; 
; \file stringhe_POL.c
; \brief POL strings set
; 


; week days
Ndz.; sDomenica_POL
Pon.; sLunedi_POL
Wt.; sMartedi_POL
Sr.; sMercoledi_POL
Czw.; sGiovedi_POL
Pt.; sVenerdi_POL
Sb.; sSabato_POL

Wyl; sOff_POL
Wl ; sOn_POL
Auto; sAuto_POL
�C; sDegC_POL
�F; sDegF_POL
H2O; sH2O_POL

; stati della stufa
ROZPALANIE; Stufa_Accensione_POL
WLACZAC; Stufa_Accesa_POL
WYLACZANIE; Stufa_Spegnimento_POL
WYLACZYC; Stufa_MCZ_POL
ALARM; Stufa_Allarme_POL
WYLACZANIE PO AWARIA; Stufa_SpeRete_POL
ROZPALANIE PO AWARIA; Stufa_AccRete_POL
NASTAWIC; Stufa_Reset_POL
KONTR. TECH.; Stufa_Collaudo_POL
 ; Stufa_NonDef_POL

; allarme
ALARM; Alarm_POL

; descrizione allarme
 ; AlarmSource00_POL
Brak rozpalenia; AlarmSource01_POL
Zgasniecie plomienia; AlarmSource02_POL
Przegrzanie zbiornika pelletu; AlarmSource03_POL
Nadmierna temperatura spalin; AlarmSource04_POL
Alarm presostatu; AlarmSource05_POL
Alarm powietrza spalaniae; AlarmSource06_POL
Otwarte drzwiczki; AlarmSource07_POL
Uszkodzenie wyrzutni spalin; AlarmSource08_POL
Uszkodzenie sondy spalin; AlarmSource09_POL
Uszkodzenie zapalnika; AlarmSource10_POL
Uszkodzenie silnika slimaka; AlarmSource11_POL
 ; AlarmSource12_POL
Uszkodzenie plyty elektronicznej; AlarmSource13_POL
 ; AlarmSource14_POL
Alarm poziomu pelletu; AlarmSource15_POL
Cisnienie wody poza zakresem; AlarmSource16_POL
Otwarte drzwiczki zbiornika pelletu; AlarmSource17_POL
Przegrzanie zbiornika wody; AlarmSource18_POL

; ripristino allarme
 ; AlarmRecovery00_POL
Wyczyscic palenisko i sprobowac ponownie; AlarmRecovery01_POL
Napelnic zbiornik pelletu; AlarmRecovery02_POL
Sprawdzic w instrukcji obslugi; AlarmRecovery03_POL
Sprawdzic w instrukcji obslugi; AlarmRecovery04_POL
Usunac ewentualna niedroznosc; AlarmRecovery05_POL
Sprawdzic czystosc paleniska/wlotu powietrza/kanalu; AlarmRecovery06_POL
Sprawdzic zamkniecie drzwiczek; AlarmRecovery07_POL
Wezwac serwis; AlarmRecovery08_POL
Wezwac serwis; AlarmRecovery09_POL
Wezwac serwis; AlarmRecovery10_POL
Wezwac serwis; AlarmRecovery11_POL
Wezwac serwis; AlarmRecovery12_POL
Wezwac serwis; AlarmRecovery13_POL
Wezwac serwis; AlarmRecovery14_POL
Sprawdzic poziomu pelletu; AlarmRecovery15_POL
Przywrocic prawidlowe cisnienie w instalacji; AlarmRecovery16_POL
Sprawdzic zamkniecie drzwiczek pelletu; AlarmRecovery17_POL
Sprawdzic w instrukcji obslugi; AlarmRecovery18_POL

; sleep
Tr. cz.; Menu_Sleep_POL

; avvio
Godz.; Settings_Time_POL
Data; Menu_DataOra_POL	
Moc; Potenza_POL		
Temperatura; Temperatura_POL
Temperatura wody; Menu_Idro_POL
Went; Fan_POL

; network
Network; Menu_Network_POL
;Wi-Fi; WiFi_POL
SSID; Ssid_POL
Slowo kl.; Keyword_POL
;TCP Port; TCPPort_POL
;WPS PIN; WPSPin_POL

; anomalie
Anomalie; Menu_Anomalie_POL
Serwis; RichiestaService_POL
Uszk. sonda temp. pow.; GuastoSondaTempAria_POL
Uszk. sonda temp. wody; GuastoSondaTempAcqua_POL
Uszk. presostat wody; GuastoPressostAcqua_POL
Cisn. wody poza zakresem; SovraPressAcqua_POL
Uszk. czujn. nat. przepl. powietrza; GuastoSensorePortata_POL
Niski poziom pelletu; PelletEsaurito_POL
Otwarte drzwiczki; PortaAperta_POL

; info
Info; Menu_Infos_POL
Kod plyty el.; CodiceScheda_POL
Kod zabezp.; CodiceSicurezza_POL
Kod wyswietl.; CodicePannello_POL
Kod parametrow; CodiceParametri_POL
L. godz. pracy; OreFunzionamento_POL
L. godz. serwisu; OreSat_POL
Serwis; Service_POL
Obr. wyrzutni 1; VentilatoreFumi_POL
Zmierz. nat. przepl. pow.; PortataAriaMisurata_POL
Tlen resztkowy; OssigenoResiduo_POL
Zuzycie pelletu; Consumo_POL
Temp. spalin; TemperaturaFumi_POL
Fotorezystor; FotoResistenza_POL
Czas slimaka 1; TempoCoclea_POL
Obroty slimaka 1; GiriCoclea_POL
Wl. went. 1; MotoreScambiatore_POL
Wl. went. 2; MotoreScambiatore2_POL
Cisnienie wody; IdroPress_POL
Liczba rozpalen; NumAccensioni_POL
Adres IP; IPAddress_POL
Historia Alarmow; StoricoAllarmi_POL	// Historia Alarm�w

; menu principale
Menu Glowne; MainMenu_POL
Ustawienia; Menu_Impostazioni_POL
Menu techniczne; Menu_Tecnico_POL

; settings
Jezyk; Lingua_POL
Eco; EcoMode_POL
Wyswietl.; Settings_Backlight_POL
�C / �F; Settings_ShowTemp_POL
Zal. slimaka; Precarica_Pellet_POL
Czyszcz.; Attiva_Pulizia_POL
Akt. pompy; Attiva_Pompa_POL
Radio ID; RFPanel_POL

; ricette
Receptury spalania; Ricette_POL
Pow.; OffsetEspulsore_POL
Pellet; RicettaPelletON_POL
Tlen; Ossigeno_POL

; temp H2O
Ust. ogrzewania; TempH2O_SetRisc_POL
Ust. cwu; TempH2O_SetSan_POL

; menu nascosto
Haslo; HidPassword_POL

;012345678901234567890 menu tecnico - livello 1					
Konfiguracja; ConfigSystem_POL
Kontrola; Controllo_POL
Hydro; MenuIdro_POL
Rozpal./wylacz.; AttuaTransitorie_POL
Moc; AttuaPotenza_POL
Obsluga alarmow; GestAllarmi_POL
Kontr. tech.; Collaudo_POL
Wyszukiwanie wg kodu; RicercaCodParam_POL

;012345678901234567890 menu tecnico - livello 2							
Ustawienia; MenuParamGen_POL
Awaria; Blackout_POL
Slimak; MenuCoclea_POL
Urz. czysz./wytrz.; MenuScuotitore_POL
Wentylator; FanAmbiente_POL
Went. 1; Fan1_POL
Went. 2; Fan2_POL
Went. 3; Fan3_POL

Funzione Eco; MenuEco_POL
Czas dzialania; TempiFunz_POL

Ustawienia; ParamTransitori_POL
Stan got. 1; Preacc1_POL
Stan got. 2; Preacc2_POL
Stan got. podgrz.; PreaccCaldo_POL
Rozpalanie A; AccA_POL
Rozpalanie B; AccB_POL
Plom WL; FireON_POL
Wylaczanie A; SpeA_POL
Wylaczanie B; SpeB_POL
Chlodzenie; Raff_POL

Parametry mocy; ParamPotenza_POL
Cykl czyszcz.; MenuPB_POL
Opozn. wyrzutni; RitardoAttuaEsp_POL
Opozn. faz mocy; RitardoStepPot_POL
Moc 1; Pot1_POL
Moc 2; Pot2_POL
Moc 3; Pot3_POL
Moc 4; Pot4_POL
Moc 5; Pot5_POL
Moc 6; Pot6_POL
Czyszcz.; Pulizia_POL
Interpolacja; Interpolazione_POL

Alarm spalin; AllarmeFumi_POL
Czujnik pelletu; SensPellet_POL
;Assenza Fiamma; AssenzaFiamma_POL
Nat. przepl. pow.; SensAria_POL

Sterowanie; Comandi_POL
Zaladunek; Carichi_POL
Uruchom.; Attuazioni_POL

; parametri generali
Czas pieca; TipoStufa_POL
Przywr. fabr. rodz. pieca; TipoStufaDef_POL
Typ silnika 1; MotoreEspulsore_POL
Typ silnika 2; MotoreEspulsore2_POL
Liczba went.; Multifan_POL
Wlaczenie Hydro; IdroMode_POL
Wysw. cwu; VisSanitari_POL
Wysw. rodz. pieca; VisTipoStufa_POL
Kontr. nat. przepl.; SensorePortataAria_POL
Kontrola obr.; SensoreHall_POL
Kontr. sondy lambda; ControlloLambda_POL
Ogranicz. obr. min.; LimGiriMinimi_POL
Termopara; AccTermocoppia_POL
Fotorezystor; AccFotoResistenza_POL
Wstrzym. rozp. B; InibAccB_POL
;Rampa Candeletta; RampaCandeletta_POL
Termostat; Termostato_Ambiente_POL
Tryb plom./temp.; GestioneIbrida_POL
;Gestione Legna/Pellet; GestioneLegna_POL
Czyszcz. mech.; FScuotitore_POL
Slimak 2/urz. czyszcz.; Coclea2_POL
Wl. wyrzutni 2; Esp2Enable_POL
Kontrola obr. wyrzutni 2; SensoreHall_2_POL
Czujnik poz. pelletu; SensorePellet_POL

; parametri blackout
Czujnik poz. pelletu; BlackoutRipristino_POL
Czas tr. przywr.; BlackoutDurata_POL

; parametri coclea
Hamow. slimaka 1; FrenataCoclea_POL
Rodz. hamow. slimaka 1; TipoFrenataCoclea_POL
Hamow. slimaka 2; FrenataCoclea_2_POL
Rodz. hamow. slimaka 2; TipoFrenataCoclea_2_POL
Kontrola obr. slimaka 1; CtrlGiriCoclea_POL
Okres slimaka 1; PeriodoCoclea_POL
Okres slimaka 2; PeriodoCoclea2_POL
Aktyw. okresu slimaka 1; AbilPeriodoCoclea_POL
Aktyw. okresu slimaka 2; AbilPeriodoCoclea2_POL

; parametri scuotitore
Czas tr. pol-cyklu; ScuotDurata_POL
Liczba cykli; ScuotNCicli_POL
;Przedzial cykli; ScuotPeriodo_POL

; parametri fan ambiente
Kontrola temp. spalin; FanCtrlTempFumi_POL
Temp. WL went.; FanTempFumiON_POL
Temp. went. min.; FanTempFumiOFF_POL

; parametri fan
Poziom 1; FanAttuaL1_POL
Poziom 2; FanAttuaL2_POL
Poziom 3; FanAttuaL3_POL
Poziom 4; FanAttuaL4_POL
Poziom 5; FanAttuaL5_POL

; parametri ricette

; parametri eco
Ecostop; EcoStop_POL
Oczek. WL; EcoAttesaAccensione_POL
Oczek. WYL; Settings_TShutdown_Eco_POL
Delta temp.; EcoDeltaTemperatura_POL

; parametri tempi funzionamento

; parametri idro
Hydro niezalezne; IdroIndipendente_POL
Wylaczanie Hydro; SpegnimentoIdro_POL
;Accumulo; FAccumulo_POL
Wstrzym. odczyt. pompy; InibSensing_POL
Pompa modulujaca; PompaModulante_POL
Presostat wody; FPressIdro_POL
;Flussostato Secondario; FlussostatoDigitale_POL
Wzm. ogrzewania; GainRisc_POL
Histereza temp. wody; IsteresiTempAcqua_POL
Delta cwu; DeltaSanitari_POL
Wzm. cwu; GainSanit_POL
Temp. aktywacji OS 2; TempAux_POL
Cisnienie maks. wody; MaxPressH2O_POL
Temp. WL Pompy; TSETON_Pompa_POL
Temp. WYL Pompy; TSETOFF_Pompa_POL

; parametri transitori
Czas tr. stanu got. 1; AccTempoPreacc_POL
Czas tr. stanu got. 2; AccTempoPreLoad_POL
Czas tr. stanu got. podgrz.; AccTempoPreacc2_POL
Temp. spalin ON; AccTempOnFumi_POL
Temp. spalin OFF; AccTempOffFumi_POL
Prog drewno; AccTempSogliaFumi_POL
Oczek. na rozpal. podgrz.; AccTempoInc_POL
Delta temp. podgrz.; AccDeltaTempCaldo_POL
Maks. czas tr. rozpal.; AccMaxTimeWarmUp_POL
Czas tr. WL plom.; AccMaxTimeFireOn_POL
Czas tr. wylaczenia; AccMaxTimeSpe_POL

; parametri di attuazione
Czas WL slimaka 1; AttuaTempoOnCoclea1_POL
Czas WYL slimaka 1; AttuaTempoOffCoclea1_POL
Nat. przepl. pow.; AttuaPortata_POL
Obr. wyrzutni 1; AttuaGiriEsp_POL
Obr. wyrzutni 2; AttuaGiriEsp2_POL
Czas WL slimaka 2; AttuaTempoOnCoclea2_POL
Czas WYL slimaka 2; AttuaTempoOffCoclea2_POL
Wyrzutnia 2; AttuaEspulsore2_POL

; parametri di potenza
Poziomy mocy; LivPotenzaMax_POL

; parametri pulizia braciere
Przedzial; AttesaPB_POL
Czas tr.; DurataPB_POL
Prog; MinPowerPB_POL

; parametri ritardo espulsore
Opozn. zwieksz.; RitardoEspInc_POL
Opozn. zmniejsz.; RitardoEspDec_POL

; parametri ritardo step potenza
Opozn. zwieksz.; RitardoPotInc_POL
Opozn. zmniejsz.; RitardoPotDec_POL

; parametri allarme fumi
Wstrzym. al. wstep.; AlmFumi_InibPreAlm_POL
Temp. spalin al. wstep.; AlmFumi_TempPreAlm_POL
Maks. czas tr. al. wstepnego; AlmFumi_DurataPreAlm_POL
Histereza al. wstep.; AlmFumi_GradientePreAlm_POL
Temp. spalin alarmu; AlmFumi_TempAlm_POL

; parametri sensore pellet
Czas tr. al. wstepnego; SensPellet_DurataPreAlm_POL

; parametri assenza fiamma
Delta spalin OFF; NoFiamma_DeltaTemp_POL

; parametri sensore portata aria
Prog otw. drzwiczek; Aria_PortataCritica_POL
Czas tr. otw. drzwiczek; Aria_TempoAlmPorta_POL
Czas tr. al. wstep. drzwicz.; Aria_TempoPreAlmPorta_POL
Wstrz. alarmu powietrza spalania; Aria_InibAlmAriaCombust_POL
Czas tr. al. wstep. pow. spalania; Aria_TempoPreAlmAriaCombust_POL
Delta nat. przepl. pow. spalania; Aria_DeltaPortAriaCombust_POL

; comandi
Kontr. tech. reczna; Test_StartStop_POL
Kontr. tech. aut.; Test_Sequenza_POL
Obejscie; Test_Bypass_POL
Kalib. termopary; Test_CalibTC_POL
Kalib. fotorezystora ON; Test_CalibFotoresOn_POL
Kalib. fotorezystora OFF; Test_CalibFotoresOff_POL
Czas tr. faz kontr. tech.; Test_DurataStep_POL

; carichi
Slimak 1; Test_Coclea_POL
Zapalnik; Test_Candeletta_POL
Went. 1; Test_Fan1_POL
Went. 2; Test_Fan2_POL
Went. 3; Test_Fan3_POL
Wyrzutnia 1; Test_Espulsore_POL
Pompa; Test_Pompa_POL
Zawor 3-drozny; Test_3Vie_POL
Os 1; Test_Aux1_POL
Os 2; Test_Aux2_POL
Os A; Test_AuxA_POL

; attuazioni
Uruch. went. 1; AttuaFan1_POL
Uruch. went. 2; AttuaFan2_POL
Uruch. went. 3; AttuaFan3_POL

; richieste di conferma
Zatwierdzic?; ReqConf_POL
Zwarcie na listwie zaciskowej JE i po Uruchomieniu; TC_ReqConf_POL

; Crono
Chrono; Menu_Crono_POL
Wlaczenie; Abilitazione_POL
Wczyt. profilu; CaricaProfilo_POL
Zerowanie; Azzera_POL

; prog. crono settimanale
Program; PrgSettimanale_POL
Wlaczenie; PrgAbilita_POL
Start; PrgStart_POL
Stop; PrgStop_POL
Temp. powietrza; PrgTempAria_POL
Temp. wody; PrgTempH2O_POL
Plom.; PrgPotenza_POL

;01 sigle fan
W1; sF1_POL,
W2; sF2_POL,

;0123 stati di attuazione
 OFF; sAttuaStat00_POL,
PSU1; sAttuaStat01_POL,
PSU2; sAttuaStat02_POL,
WPSU; sAttuaStat03_POL,
SU A; sAttuaStat04_POL,
SU B; sAttuaStat05_POL,
SU C; sAttuaStat06_POL,
FONA; sAttuaStat07_POL,
FONB; sAttuaStat08_POL,
SD A; sAttuaStat09_POL,
SD B; sAttuaStat10_POL,
SD C; sAttuaStat11_POL,
CD A; sAttuaStat12_POL,
CD B; sAttuaStat13_POL,
BCLA; sAttuaStat14_POL,
BCLB; sAttuaStat15_POL,
PL 1; sAttuaStat16_POL,
PL 2; sAttuaStat17_POL,
PL 3; sAttuaStat18_POL,
PL 4; sAttuaStat19_POL,
PL 5; sAttuaStat20_POL,
PL 6; sAttuaStat21_POL,
PL 7; sAttuaStat22_POL,
PL 8; sAttuaStat23_POL,
PL 9; sAttuaStat24_POL,
PLSA; sAttuaStat25_POL,


