; 
; \file stringhe_SLO.c
; \brief SLO strings set
; 


; week days
Nedelja; sDomenica_SLO
Ponedeljek; sLunedi_SLO
Torek; sMartedi_SLO
Sreda; sMercoledi_SLO
�etrtek; sGiovedi_SLO	// Cetrtek
Petek; sVenerdi_SLO
Sobota; sSabato_SLO

Off; sOff_SLO
On ; sOn_SLO
Avto; sAuto_SLO
�C; sDegC_SLO
�F; sDegF_SLO
H2O; sH2O_SLO

; stati della stufa
V�IG; Stufa_Accensione_SLO	// V�IG
ON; Stufa_Accesa_SLO
IZKLOP; Stufa_Spegnimento_SLO
OFF; Stufa_MCZ_SLO
ALARM; Stufa_Allarme_SLO
IZKLOP OB IZPADU ELEKTRIKE; Stufa_SpeRete_SLO
V�IG OB IZPADU ELEKTRIKE; Stufa_AccRete_SLO	// V�IG OB IZPADU ELEKTRIKE
PONASTAVI; Stufa_Reset_SLO
PREIZKU�ANJE; Stufa_Collaudo_SLO	// PREIZKU�ANJE
 ; Stufa_NonDef_SLO

; allarme
ALARM; Alarm_SLO

; descrizione allarme
 ; AlarmSource00_SLO
Neuspeli v�ig; AlarmSource01_SLO	// Neuspeli v�ig
Ugasnitev plamena; AlarmSource02_SLO
Pregrevanje rezervoarja peletov; AlarmSource03_SLO
Previsoka temperatura dimnih plinov; AlarmSource04_SLO
Alarm tla�nega stikala; AlarmSource05_SLO	// Alarm tlacnega stikala
Alarm zgorevalnega zraka; AlarmSource06_SLO
Odprta vrata; AlarmSource07_SLO
Napaka na odsesovalniku dima; AlarmSource08_SLO
Pokvarjena sonda dimnih plinov; AlarmSource09_SLO
Okvara povezana z v�igalom; AlarmSource10_SLO	// Okvara povezana z v�igalom
Pokvarjen motor pol�a; AlarmSource11_SLO	// Pokvarjen motor pol�a
 ; AlarmSource12_SLO
Pokvarjena elektronska kartica; AlarmSource13_SLO
 ; AlarmSource14_SLO
Alarm nivoja peletov; AlarmSource15_SLO
Tlak vode je presegel mejne vrednosti; AlarmSource16_SLO
Odprta vrata rezervoarja peletov; AlarmSource17_SLO
Pregrevanje vodnega rezervoarja; AlarmSource18_SLO

; ripristino allarme
 ; AlarmRecovery00_SLO
O�istite �erjavnico in poskusite znova; AlarmRecovery01_SLO	// Ocistite �erjavnico in poskusite znova
Napolnite rezervoar peletov; AlarmRecovery02_SLO
Oglejte si Navodila za uporabo; AlarmRecovery03_SLO
Oglejte si Navodila za uporabo; AlarmRecovery04_SLO
Odstranite morebitno zama�itev; AlarmRecovery05_SLO	// Odstranite morebitno zama�itev
Preverite, ali so �erjavnica/dotok zraka/dimnik �ista; AlarmRecovery06_SLO	// Preverite, ali so �erjavnica/dotok zraka/dimnik cista
Preverite, ali so vrata zaprta; AlarmRecovery07_SLO
Obrnite se na serviserja; AlarmRecovery08_SLO
Obrnite se na serviserja; AlarmRecovery09_SLO
Obrnite se na serviserja; AlarmRecovery10_SLO
Obrnite se na serviserja; AlarmRecovery11_SLO
Obrnite se na serviserja; AlarmRecovery12_SLO
Obrnite se na serviserja; AlarmRecovery13_SLO
Obrnite se na serviserja; AlarmRecovery14_SLO
Preverite nivo peletov; AlarmRecovery15_SLO
Ponovno vzpostavite pravilno vrednost tlaka v napravi; AlarmRecovery16_SLO
Preverite, da so vrata rezervoarja zaprta; AlarmRecovery17_SLO
Oglejte si Navodila za uporabo; AlarmRecovery18_SLO

; sleep
Sleep; Menu_Sleep_SLO

; avvio
Ura; Settings_Time_SLO
Datum; Menu_DataOra_SLO	
Mo�; Potenza_SLO	// Moc
Temperatura; Temperatura_SLO
Temperatura Vode; Menu_Idro_SLO
Ventilator; Fan_SLO

; network
Network; Menu_Network_SLO
;Wi-Fi; WiFi_SLO
SSID; Ssid_SLO
Keyword; Keyword_SLO
;TCP Port; TCPPort_SLO
;WPS PIN; WPSPin_SLO

; anomalie
Napake; Menu_Anomalie_SLO
Servisiranje; RichiestaService_SLO
Sonda temp. zraka ne deluje; GuastoSondaTempAria_SLO
Sonda temp. vode ne deluje; GuastoSondaTempAcqua_SLO
Stikalo za tlak vode ne deluje; GuastoPressostAcqua_SLO
Tlak vode nad mejnimi vrednostmi; SovraPressAcqua_SLO
Napaka na senzorju pretoka; GuastoSensorePortata_SLO
Pomanjkanje peletov; PelletEsaurito_SLO
Odprta vrata; PortaAperta_SLO

; info
Info; Menu_Infos_SLO
Koda kartice; CodiceScheda_SLO
Varnostna koda; CodiceSicurezza_SLO
Koda zaslona; CodicePannello_SLO
Koda parametrov; CodiceParametri_SLO
�as delovanja; OreFunzionamento_SLO	// Cas delovanja
�as servisiranja; OreSat_SLO	// Cas servisiranja
Servisiranje; Service_SLO
Vrtljaji odsesovalnika 1; VentilatoreFumi_SLO
Izmerjen zra�ni pretok; PortataAriaMisurata_SLO	// Izmerjen zracni pretok
Preostali kisik; OssigenoResiduo_SLO
Poraba peletov; Consumo_SLO
Temperatura dimov; TemperaturaFumi_SLO
Fotoupornik; FotoResistenza_SLO
�as pol�a 1; TempoCoclea_SLO	// Cas pol�a 1
Vrtiljaji pol�a 1; GiriCoclea_SLO	// Vrtiljaji pol�a 1
Aktivacija vent.1; MotoreScambiatore_SLO
Aktivacija vent.2; MotoreScambiatore2_SLO
Tlak vode; IdroPress_SLO
�tevilo v�igov; NumAccensioni_SLO	// �tevilo v�igov
IP naslov; IPAddress_SLO
Zgodovina Alarma; StoricoAllarmi_SLO

; menu principale
Glavni meni; MainMenu_SLO
Nastavitve; Menu_Impostazioni_SLO
Tehni�ni meni; Menu_Tecnico_SLO	// Tehnicni meni

; settings
Jezik; Lingua_SLO
Eco; EcoMode_SLO
Zaslona; Settings_Backlight_SLO
�C / �F; Settings_ShowTemp_SLO
Polnjenje peletov; Precarica_Pellet_SLO
�i��enje; Attiva_Pulizia_SLO	// Ci�cenje
Aktivacija �rpalke; Attiva_Pompa_SLO	// Aktivacija crpalke
Radio ID; RFPanel_SLO

; ricette
Mo�nosti kurjenja; Ricette_SLO	// Mo�nosti kurjenja
Zrak; OffsetEspulsore_SLO
Peleti; RicettaPelletON_SLO
Kisik; Ossigeno_SLO

; temp H2O
Nastav. temp. ogrevanja; TempH2O_SetRisc_SLO
Nastav. temp. sanitarne vode; TempH2O_SetSan_SLO

; menu nascosto
Geslo; HidPassword_SLO

;012345678901234567890 menu tecnico - livello 1					
Konfiguracija; ConfigSystem_SLO
Preverjanje; Controllo_SLO
Hidro; MenuIdro_SLO
V�ig/Izklop; AttuaTransitorie_SLO	// V�ig/Izklop
Mo�; AttuaPotenza_SLO	// Moc
Upravljanje alarmov; GestAllarmi_SLO
Preizku�anje; Collaudo_SLO	// Preizku�anje
Iskanje po kodi; RicercaCodParam_SLO

;012345678901234567890 menu tecnico - livello 2							
Nastavitve; MenuParamGen_SLO
Izpad elektrike; Blackout_SLO
Pol�; MenuCoclea_SLO	// Pol�
�istilec/stresalnik; MenuScuotitore_SLO	// Cistilec/stresalnik
Ventilator; FanAmbiente_SLO
Ventilator 1; Fan1_SLO
Ventilator 2; Fan2_SLO
Ventilator 3; Fan3_SLO

Funkcija Eko; MenuEco_SLO
�asi delovanja; TempiFunz_SLO	// Casi delovanja

Nastavitve; ParamTransitori_SLO
Predv�ig 1; Preacc1_SLO	// Predv�ig 1
Predv�ig 2; Preacc2_SLO	// Predv�ig 2
Toplotni predv�ig; PreaccCaldo_SLO	// Toplotni predv�ig
V�ig A; AccA_SLO	// V�ig A
V�ig B; AccB_SLO	// V�ig B
Ogenj ON; FireON_SLO
Izklop A; SpeA_SLO
Izklop B; SpeB_SLO
Ohlajanje; Raff_SLO

Parametri mo�i; ParamPotenza_SLO	// Parametri moci
Postopek �i��enja; MenuPB_SLO	// Postopek ci�cenja
Zakasnitev odsesovalnika; RitardoAttuaEsp_SLO
Zakasnitev mo�i; RitardoStepPot_SLO	// Zakasnitev moci
Mo� 1; Pot1_SLO	// Moc 1
Mo� 2; Pot2_SLO	// Moc 2
Mo� 3; Pot3_SLO	// Moc 3
Mo� 4; Pot4_SLO	// Moc 4
Mo� 5; Pot5_SLO	// Moc 5
Mo� 6; Pot6_SLO	// Moc 6
�i��enje; Pulizia_SLO	// Ci�cenje
Interpolacija; Interpolazione_SLO

Alarm dimnih plinov; AllarmeFumi_SLO
Senzor peletov; SensPellet_SLO
;Assenza Fiamma; AssenzaFiamma_SLO
Pretok zraka; SensAria_SLO

Ukazi; Comandi_SLO
Polnjenje; Carichi_SLO
Aktivacije; Attuazioni_SLO

; parametri generali
Tip pe�i; TipoStufa_SLO	// Tip peci
Ponovna vzpostavitev tovarni�kih vrednosti; TipoStufaDef_SLO	// Ponovna vzpostavitev tovarni�kih vrednosti
Tip motorja 1; MotoreEspulsore_SLO
Tip motorja 2; MotoreEspulsore2_SLO
�tevilo ventilatorjev; Multifan_SLO	// �tevilo ventilatorjev
Omogo�enje hidro; IdroMode_SLO	// Omogocenje hidro
Prikaz sanitarne vode; VisSanitari_SLO
Prikaz tipa pe�i; VisTipoStufa_SLO	// Prikaz tipa peci
Preverjanje pretoka; SensorePortataAria_SLO
Preverjanje vrtljajev; SensoreHall_SLO
Preverjanje Lambda; ControlloLambda_SLO
Omejevalnik min. vrtljajev; LimGiriMinimi_SLO
Toplotni par; AccTermocoppia_SLO
Fotoupornik; AccFotoResistenza_SLO
Blokada B v�iga; InibAccB_SLO	// Blokada B v�iga
;Rampa Candeletta; RampaCandeletta_SLO
Termostat; Termostato_Ambiente_SLO
Na�in ogenj/temp.; GestioneIbrida_SLO	// Nacin ogenj/temp.
;Gestione Legna/Pellet; GestioneLegna_SLO
Mehansko �i��enje; FScuotitore_SLO	// Mehansko ci�cenje
Pol� 2 / �istilec; Coclea2_SLO	// Pol� 2 / cistilec
Omogo�enje odsesovalnika 2; Esp2Enable_SLO	// Omogocenje odsesovalnika 2
Preverjanje vrtljajev odsesovalnika 2; SensoreHall_2_SLO
Senzor ravni peletov; SensorePellet_SLO

; parametri blackout
Omogo�enje ponovne vzpostavitve; BlackoutRipristino_SLO	// Omogocenje ponovne vzpostavitve
Trajanje ponovne vzpostavitve; BlackoutDurata_SLO

; parametri coclea
Zavora pol�a 1; FrenataCoclea_SLO	// Zavora pol�a 1
Tip zavore pol�a 1; TipoFrenataCoclea_SLO	// Tip zavore pol�a 1
Zavora pol�a 2; FrenataCoclea_2_SLO	// Zavora pol�a 2
Tip zavore pol�a 2; TipoFrenataCoclea_2_SLO	// Tip zavore pol�a 2
Preverjanje vrtljajev pol�a 1; CtrlGiriCoclea_SLO	// Preverjanje vrtljajev pol�a 1
Interval pol�a 1; PeriodoCoclea_SLO	// Interval pol�a 1
Interval pol�a 2; PeriodoCoclea2_SLO	// Interval pol�a 2
Omogocenje intervala pol�a 1; AbilPeriodoCoclea_SLO	// Omogocenje intervala pol�a 1
Omogocenje intervala pol�a 2; AbilPeriodoCoclea2_SLO	// Omogocenje intervala pol�a 2

; parametri scuotitore
Trajanje polcikla; ScuotDurata_SLO
�tevilo ciklov; ScuotNCicli_SLO	// �tevilo ciklov
;Interval ciklov; ScuotPeriodo_SLO

; parametri fan ambiente
Preverjanje temp. dimnih plinov; FanCtrlTempFumi_SLO
Temp. On ventilatorja; FanTempFumiON_SLO
Min. temp. ventilatorja; FanTempFumiOFF_SLO

; parametri fan
Raven 1; FanAttuaL1_SLO
Raven 2; FanAttuaL2_SLO
Raven 3; FanAttuaL3_SLO
Raven 4; FanAttuaL4_SLO
Raven 5; FanAttuaL5_SLO

; parametri ricette

; parametri eco
Eko stop; EcoStop_SLO
�akanje On; EcoAttesaAccensione_SLO	// Cakanje On
�akanje Off; Settings_TShutdown_Eco_SLO	// Cakanje Off
Delta Temp.; EcoDeltaTemperatura_SLO

; parametri tempi funzionamento

; parametri idro
Hidro neodvisno; IdroIndipendente_SLO
Izklop hidro; SpegnimentoIdro_SLO
;Accumulo; FAccumulo_SLO
Blokada Sensing �rpalke; InibSensing_SLO	// Blokada Sensing crpalke
Modulacijska �rpalka; PompaModulante_SLO	// Modulacijska crpalka
Stikalo za tlak vode; FPressIdro_SLO
;Flussostato Secondario; FlussostatoDigitale_SLO
Dvig temp. ogrevanja; GainRisc_SLO
Histereza temp. vode; IsteresiTempAcqua_SLO
Delta sanitarne vode; DeltaSanitari_SLO
Dvig temp. sanitarne vode; GainSanit_SLO
Temp. aktivacije AUX 2; TempAux_SLO
Maks. pritisk vode; MaxPressH2O_SLO
Temp. On �rpalke; TSETON_Pompa_SLO	// Temp. On crpalke
Temp. Off �rpalke; TSETOFF_Pompa_SLO	// Temp. Off crpalke

; parametri transitori
Trajanje predv�iga 1; AccTempoPreacc_SLO	// Trajanje predv�iga 1
Trajanje predv�iga 2; AccTempoPreLoad_SLO	// Trajanje predv�iga 2
Trajanje toplotnega predv�iga; AccTempoPreacc2_SLO	// Trajanje toplotnega predv�iga
Temp. dimnih plinov On; AccTempOnFumi_SLO
Temp. dimnih plinov Off; AccTempOffFumi_SLO
Dovoljena koli�ina lesa; AccTempSogliaFumi_SLO	// Dovoljena kolicina lesa
�akanje na toplotni v�ig; AccTempoInc_SLO	// Cakanje na toplotni v�ig
Delta temp. toplo; AccDeltaTempCaldo_SLO
Maks. trajanje v�iga; AccMaxTimeWarmUp_SLO	// Maks. trajanje v�iga
Trajanje ognja On; AccMaxTimeFireOn_SLO
Trajanje izklopa; AccMaxTimeSpe_SLO

; parametri di attuazione
�as On Pol�a 1; AttuaTempoOnCoclea1_SLO	// Cas On Pol�a 1
�as Off Pol�a 1; AttuaTempoOffCoclea1_SLO	// Cas Off Pol�a 1
Pretok zraka; AttuaPortata_SLO
Vrtljaji odsesovalnika 1; AttuaGiriEsp_SLO
Vrtljaji odsesovalnika 2; AttuaGiriEsp2_SLO
�as On Pol�a 2; AttuaTempoOnCoclea2_SLO	// Cas On Pol�a 2
�as Off Pol�a 2; AttuaTempoOffCoclea2_SLO	// Cas Off Pol�a 2
Odsesovalnik 2; AttuaEspulsore2_SLO

; parametri di potenza
Ravni mo�i; LivPotenzaMax_SLO	// Ravni moci

; parametri pulizia braciere
Interval; AttesaPB_SLO
Trajanje; DurataPB_SLO
Dovoljena koli�ina; MinPowerPB_SLO	// Dovoljena kolicina

; parametri ritardo espulsore
Zakasnitev pove�anja; RitardoEspInc_SLO	// Zakasnitev povecanja
Zakasnitev zmanj�anja; RitardoEspDec_SLO	// Zakasnitev zmanj�anja

; parametri ritardo step potenza
Zakasnitev pove�anja; RitardoPotInc_SLO	// Zakasnitev povecanja
Zakasnitev zmanj�anja; RitardoPotDec_SLO	// Zakasnitev zmanj�anja

; parametri allarme fumi
Blokada predalarma; AlmFumi_InibPreAlm_SLO
Predalarm temp. dimnih plinov; AlmFumi_TempPreAlm_SLO
Maks. trajanje predalarma; AlmFumi_DurataPreAlm_SLO
Histereza predalarma; AlmFumi_GradientePreAlm_SLO
Alarm temp. dimnih plinov; AlmFumi_TempAlm_SLO

; parametri sensore pellet
Trajanje predalarma; SensPellet_DurataPreAlm_SLO

; parametri assenza fiamma
Delta dimnih plinov Off; NoFiamma_DeltaTemp_SLO

; parametri sensore portata aria
Dovoljeni �as odprtih vrat; Aria_PortataCritica_SLO	// Dovoljeni cas odprtih vrat
Trajanje odprtih vrat; Aria_TempoAlmPorta_SLO
Trajanje predalarma vrat; Aria_TempoPreAlmPorta_SLO
Blokada alarma zgorevalnega zraka; Aria_InibAlmAriaCombust_SLO
Trajanje predalarma zgorevalnega zraka; Aria_TempoPreAlmAriaCombust_SLO
Delta pretoka zgorevalnega zraka; Aria_DeltaPortAriaCombust_SLO

; comandi
Ro�no preizku�anje; Test_StartStop_SLO	// Rocno preizku�anje
Avtomatsko preizku�anje; Test_Sequenza_SLO	// Avtomatsko preizku�anje
Obvoz; Test_Bypass_SLO
Kalibracija toplotnih parov; Test_CalibTC_SLO
Kalibracija fotoupora On; Test_CalibFotoresOn_SLO
Kalibracija fotoupora Off; Test_CalibFotoresOff_SLO
Trajanje preizku�anja; Test_DurataStep_SLO	// Trajanje preizku�anja

; carichi
Pol� 1 1; Test_Coclea_SLO	// Pol� 1
V�igalo; Test_Candeletta_SLO	// V�igalo
Ventilator 1; Test_Fan1_SLO
Ventilator 2; Test_Fan2_SLO
Ventilator 3; Test_Fan3_SLO
Odsesovalnik 1; Test_Espulsore_SLO
�rpalka; Test_Pompa_SLO	// Crpalka
3-potni ventil; Test_3Vie_SLO
Aux 1; Test_Aux1_SLO
Aux 2; Test_Aux2_SLO
Aux A; Test_AuxA_SLO

; attuazioni
Aktivacija ventilatorja 1; AttuaFan1_SLO
Aktivacija ventilatorja 2; AttuaFan2_SLO
Aktivacija ventilatorja 3; AttuaFan3_SLO

; richieste di conferma
Potrdi?; ReqConf_SLO
Vzpostavi kratki stik terminal.bloka JE, nato aktiviraj; TC_ReqConf_SLO

; Crono
�asovnik; Menu_Crono_SLO	// Casovnik
Omogo�enje; Abilitazione_SLO	// Omogocenje
Nalo�i profil; CaricaProfilo_SLO	// Nalo�i profil
Ponastavi; Azzera_SLO

; prog. crono settimanale
Program; PrgSettimanale_SLO
Omogo�enje; PrgAbilita_SLO	// Omogocenje
Zagon; PrgStart_SLO
Zaustavitev; PrgStop_SLO
Temperatura zraka; PrgTempAria_SLO
Temperatura vode; PrgTempH2O_SLO
Ogenj; PrgPotenza_SLO

;01 sigle fan
F1; sF1_SLO,
F2; sF2_SLO,

;0123 stati di attuazione
 OFF; sAttuaStat00_SLO,
PSU1; sAttuaStat01_SLO,
PSU2; sAttuaStat02_SLO,
WPSU; sAttuaStat03_SLO,
SU A; sAttuaStat04_SLO,
SU B; sAttuaStat05_SLO,
SU C; sAttuaStat06_SLO,
FONA; sAttuaStat07_SLO,
FONB; sAttuaStat08_SLO,
SD A; sAttuaStat09_SLO,
SD B; sAttuaStat10_SLO,
SD C; sAttuaStat11_SLO,
CD A; sAttuaStat12_SLO,
CD B; sAttuaStat13_SLO,
BCLA; sAttuaStat14_SLO,
BCLB; sAttuaStat15_SLO,
PL 1; sAttuaStat16_SLO,
PL 2; sAttuaStat17_SLO,
PL 3; sAttuaStat18_SLO,
PL 4; sAttuaStat19_SLO,
PL 5; sAttuaStat20_SLO,
PL 6; sAttuaStat21_SLO,
PL 7; sAttuaStat22_SLO,
PL 8; sAttuaStat23_SLO,
PL 9; sAttuaStat24_SLO,
PLSA; sAttuaStat25_SLO,


