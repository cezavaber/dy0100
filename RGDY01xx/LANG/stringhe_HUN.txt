; 
; \file stringhe_HUN.c
; \brief HUN strings set
; 


; week days
Vasarnap; sDomenica_HUN	// Vas�rnap
Hetfo; sLunedi_HUN	// H�tfo
Kedd; sMartedi_HUN
Szerda; sMercoledi_HUN
Csutortok; sGiovedi_HUN	// Cs�t�rt�k
Pentek; sVenerdi_HUN	// P�ntek
Szombat; sSabato_HUN

Ki ; sOff_HUN
Be ; sOn_HUN
Auto; sAuto_HUN
�C; sDegC_HUN
�F; sDegF_HUN
H2O; sH2O_HUN

; stati della stufa
GYUJTAS; Stufa_Accensione_HUN
BE; Stufa_Accesa_HUN
KIKAPCSOLAS; Stufa_Spegnimento_HUN
KI; Stufa_MCZ_HUN
ALARM; Stufa_Allarme_HUN
KIKAPCSOLAS UTAN VISSZAALLITASI; Stufa_SpeRete_HUN
GYUJTAS UTAN VISSZAALLITASI; Stufa_AccRete_HUN
VISSZAALLITASA; Stufa_Reset_HUN
TESZTELES; Stufa_Collaudo_HUN
 ; Stufa_NonDef_HUN

; allarme
ALARM; Alarm_HUN

; descrizione allarme
 ; AlarmSource00_HUN
Sikertelen Gyujtas; AlarmSource01_HUN	// Sikertelen gy�jt�s
Lang Kialudt; AlarmSource02_HUN	// L�ng kialudt
Pellettarolo Homerseklet Magas; AlarmSource03_HUN	// Pellett�rol� hom�rs�klet magas
Magas Fustgaz Homerseklet; AlarmSource04_HUN	// Magas fustg�z hom�rs�klet
Fustgaz Nyomaskapcsolo Riasztas; AlarmSource05_HUN	// F�stg�z nyom�skapcsol� riaszt�s
Egesi Levego Riasztas; AlarmSource06_HUN	// �g�si levego riaszt�s
Ajto Nyitva; AlarmSource07_HUN	// Ajt� nyitva
Fustgaz Ventilator Hiba; AlarmSource08_HUN	// F�stg�z ventil�tor hiba
Fustgaz Homerseklet Szenzor Hiba; AlarmSource09_HUN	// F�stg�z hom�rs�klet szenzor hiba
Pellet Gyujto Hiba; AlarmSource10_HUN	// Pellet gy�jt� hiba
Pellet Adagolo Hiba; AlarmSource11_HUN	// Pellet adagol� hiba
 ; AlarmSource12_HUN
Elektronikus Alaplap Hiba; AlarmSource13_HUN
 ; AlarmSource14_HUN
Pellet Szint Riasztas; AlarmSource15_HUN	// Pellet szint riaszt�s
Viznyomas Hatarerteken Kivul; AlarmSource16_HUN	// V�znyom�s hat�r�rt�ken k�v�l
Pellet Tarolo Ajto Nyitva; AlarmSource17_HUN	// Pellet t�rol� ajt� nyitva
Magas Futoviz Homerseklet; AlarmSource18_HUN	// Magas futov�z hom�rs�klet

; ripristino allarme
 ; AlarmRecovery00_HUN
Tisztitsa Meg Az Egot Es Inditsa Ujra; AlarmRecovery01_HUN	// Tiszt�tsa meg az �got �s ind�tsa �jra
Toltse Fel a Pellttarolot; AlarmRecovery02_HUN	// T�ltse fel a pelltt�rol�t
Nezze Meg a Kezelesi Utasitast; AlarmRecovery03_HUN	// N�zze meg a kezel�si utas�t�st
Nezze Meg a Kezelesi Utasitast; AlarmRecovery04_HUN	// N�zze meg a kezel�si utas�t�st
Szuntesse Meg a Dugulast; AlarmRecovery05_HUN	// Sz�ntesse meg a dugul�st
Ellenorizze Egot/Levegoellatast/Kemenyt; AlarmRecovery06_HUN	// Ellenorizze �got/levegoell�t�st/k�m�nyt
Ellenorizze az Ajto Zarodasat; AlarmRecovery07_HUN	// Ellenorizze az ajt� z�r�d�s�t
Hivja a Szervizt; AlarmRecovery08_HUN	// H�vja a szervizt
Hivja a Szervizt; AlarmRecovery09_HUN	// H�vja a szervizt
Hivja a Szervizt; AlarmRecovery10_HUN	// H�vja a szervizt
Hivja a Szervizt; AlarmRecovery11_HUN	// H�vja a szervizt
Hivja a Szervizt; AlarmRecovery12_HUN	// H�vja a szervizt
Hivja a Szervizt; AlarmRecovery13_HUN	// H�vja a szervizt
Hivja a Szervizt; AlarmRecovery14_HUN	// H�vja a szervizt
Ellenorizze a Pellet Szintet; AlarmRecovery15_HUN	// Ellenorizze a pellet szintet
Allitsa be a Helyes Viznyomast; AlarmRecovery16_HUN	// �ll�tsa be a helyes v�znyom�st
Ellenorizze a Pellet Tarolo Ajtajat; AlarmRecovery17_HUN	// Ellenorizze a pellet t�rol� ajtaj�t
Nezze Meg a Kezelesi Utasitast; AlarmRecovery18_HUN	// N�zze meg a kezel�si utas�t�st

; sleep
Alvas; Menu_Sleep_HUN	// Alv�s

; avvio
Ora; Settings_Time_HUN	// �ra
Nap; Menu_DataOra_HUN	
Fokozat; Potenza_HUN		
Homerseklet; Temperatura_HUN	// Hom�rs�klet
Vizhomerseklet; Menu_Idro_HUN	// V�zhom�rs�klet
Ventilator; Fan_HUN	// Ventil�tor

; network
Halozat; Menu_Network_HUN	// H�l�zat
;Wi-Fi; WiFi_HUN
SSID; Ssid_HUN
Jelszo; Keyword_HUN	// Jelsz�	//
;TCP Port; TCPPort_HUN
;WPS PIN; WPSPin_HUN

; anomalie
Riasztasok; Menu_Anomalie_HUN	// Riaszt�sok
Szerviz; RichiestaService_HUN
Leghomerseklet Erzekelo Hiba; GuastoSondaTempAria_HUN	// L�ghom�rs�klet �rz�kelo hiba
Vizhomerseklet Erzekelo Hiba; GuastoSondaTempAcqua_HUN	// V�zhom�rs�klet �rz�kelo hiba
Viznyomas Erzekelo Hiba; GuastoPressostAcqua_HUN	// V�znyom�s �rz�kelo hiba
Viznyomas Hatereteken Kivul; SovraPressAcqua_HUN	// V�znyom�s hat�r�t�ken k�v�l
Legaram Szenzor Hiba; GuastoSensorePortata_HUN	// L�g�ram szenzor hiba
Alacsony Pellet Szint; PelletEsaurito_HUN
Ajto Nyitva; PortaAperta_HUN	// Ajt� nyitva

; info
Informacio; Menu_Infos_HUN	// Inform�ci�	//
Vezerlo Panel Kod; CodiceScheda_HUN	// Vez�rlo panel k�d
Biztonsagi Kod; CodiceSicurezza_HUN	// Biztons�gi k�d
Display Kod; CodicePannello_HUN	// Display k�d
Parameter Kod; CodiceParametri_HUN	// Param�ter k�d
Mukodesi Ido; OreFunzionamento_HUN	// Muk�d�si ido
Szerviz Ido; OreSat_HUN
Szerviz; Service_HUN
Fustgaz Ventilator 1 Fordulatszam; VentilatoreFumi_HUN	// F�stg�z ventil�tor 1 fordulatsz�m
Mert Legaaram; PortataAriaMisurata_HUN	// M�rt l�ga�ram
Maradek Oxigen; OssigenoResiduo_HUN	// Marad�k oxig�n
Pellet Fogyasztas; Consumo_HUN	// Pellet fogyaszt�s
Fustgaz Homerseklet; TemperaturaFumi_HUN	// F�stg�z hom�rs�klet
Fotoelektromos Langor; FotoResistenza_HUN	// Fotoelektromos l�ngor
Pellet Adagolo 1 Ido; TempoCoclea_HUN	// Pellet adagol� 1 ido
Pellet Adagolo 1 Fordulatszam; GiriCoclea_HUN	// Pellet adagol� 1 fordulatsz�m
Ventilator 1 Aktivalas; MotoreScambiatore_HUN	// Ventil�tor 1 aktiv�l�s
Ventilator 2 Aktivalas; MotoreScambiatore2_HUN	// Ventil�tor 2 aktiv�l�s
Viznyomas; IdroPress_HUN	// V�znyom�s
Gyujtasra Szama; NumAccensioni_HUN	// Gy�jt�sra Sz�ma
IP Cim; IPAddress_HUN	// IP c�m
Riasztas Tortenete; StoricoAllarmi_HUN	// Riaszt�s T�rt�nete

; menu principale
Fomenu; MainMenu_HUN	// Fomen�	//
Beallitasok; Menu_Impostazioni_HUN	// Be�ll�t�sok
Technikai Menu; Menu_Tecnico_HUN	// Technikai Men�	//

; settings
Nyelv; Lingua_HUN
Takarekos Uzemmod (ECO); EcoMode_HUN	// Takar�kos �zemm�d (ECO)
Display; Settings_Backlight_HUN
�C / �F; Settings_ShowTemp_HUN
Pellet Toltes; Precarica_Pellet_HUN	// Pellet t�lt�s
Tisztitas; Attiva_Pulizia_HUN	// Tiszt�t�s
Szivattyu Inditas; Attiva_Pompa_HUN	// Szivatty� ind�t�s
Radio ID; RFPanel_HUN

; ricette
Egesi Beallitasok; Ricette_HUN	// �g�si be�ll�t�sok
Legaram; OffsetEspulsore_HUN	// L�g�ram
Pellet; RicettaPelletON_HUN
Oxigen; Ossigeno_HUN	// Oxig�n

; temp H2O
Futesi Viz Homerseklet Beallitas; TempH2O_SetRisc_HUN	// Fut�si v�z hom�rs�klet be�ll�t�s
Hasznalati Melegviz Homerseklet Beallitas; TempH2O_SetSan_HUN	// Haszn�lati melegv�z hom�rs�klet be�ll�t�s

; menu nascosto
Jelszo; HidPassword_HUN	// Jelsz�

;012345678901234567890 menu tecnico - livello 1					
Konfiguracio; ConfigSystem_HUN	// Konfigur�ci�	//
Vezerles; Controllo_HUN	// Vez�rl�s
Vizes Uzem; MenuIdro_HUN	// Vizes �zem
Gyujtas/Kikapcsolas; AttuaTransitorie_HUN	// Gy�jt�s/kikapcsol�s
Teljesitmeny; AttuaPotenza_HUN	// Teljes�tm�ny
Riasztas Kezeles; GestAllarmi_HUN	// Riaszt�s kezel�s
Teszt; Collaudo_HUN
Kereses Koddal; RicercaCodParam_HUN	// Keres�s k�ddal

;012345678901234567890 menu tecnico - livello 2							
Beallitasok; MenuParamGen_HUN	// Be�ll�t�sok
Aramszunet; Blackout_HUN	// �ramsz�net
Pellet Adagolo; MenuCoclea_HUN	// Pellet adagol�	//
Hamu/Ego Tisztitas; MenuScuotitore_HUN	// Hamu/�go tiszt�t�s
Ventilator; FanAmbiente_HUN	// Ventil�tor
Ventilator 1; Fan1_HUN	// Ventil�tor 1
Ventilator 2; Fan2_HUN	// Ventil�tor 2
Ventilator 3; Fan3_HUN	// Ventil�tor 3

Takarekos Uzemmod; MenuEco_HUN	// Takar�kos �zemm�d
Uzemido; TempiFunz_HUN	// �zemido

Beallitasok; ParamTransitori_HUN	// Be�ll�t�sok
Elogyujtas 1; Preacc1_HUN	// Elogy�jt�s 1
Elogyujtas 2; Preacc2_HUN	// Elogy�jt�s 2
Meleg Elogyujtas; PreaccCaldo_HUN	// Meleg elogy�jt�s
Gyujtas A; AccA_HUN	// Gy�jt�s A
Gyujtas B; AccB_HUN	// Gy�jt�s B
Langstabilizalas; FireON_HUN	// L�ngstabiliz�l�s
Kikapcsolas A; SpeA_HUN	// Kikapcsol�s A
Kikapcsolas B; SpeB_HUN	// Kikapcsol�s B
Hutes; Raff_HUN	// Hut�s

Teljesitmeny parameterek; ParamPotenza_HUN	// Teljes�tm�ny param�terek
Tisztitasi Ciklus; MenuPB_HUN	// Tiszt�t�si ciklus
Fustgaz Ventilator Kesleltetesi Ido; RitardoAttuaEsp_HUN	// F�stg�z ventil�tor k�sleltet�si ido
Fokozat Valtas Kesleltetes; RitardoStepPot_HUN	// Fokozat v�lt�s k�sleltet�s
1-es Fokozat; Pot1_HUN
2-es Fokozat; Pot2_HUN
3-es Fokozat; Pot3_HUN
4-es Fokozat; Pot4_HUN
5-es Fokozat; Pot5_HUN
6-es Fokozat; Pot6_HUN
Tisztitas; Pulizia_HUN	// Tiszt�t�s
Interpolacio; Interpolazione_HUN	// Interpol�ci�	//

Fustgaz Riasztas; AllarmeFumi_HUN	// F�stg�z riaszt�s
Pellet Erzekelo; SensPellet_HUN	// Pellet �rz�kelo
;Hianyaban Flame; AssenzaFiamma_HUN	// Hi�ny�ban Flame
Legaram; SensAria_HUN	// L�g�ram

Parancsok; Comandi_HUN
Motorok; Carichi_HUN
Aktivalasok; Attuazioni_HUN	// Aktiv�l�sok

; parametri generali
Kazan Tipus; TipoStufa_HUN	// Kaz�n t�pus
Restore Default Tuzhely Tipus; TipoStufaDef_HUN	// Restore Default Tuzhely T�pus
Motor Tipus 1; MotoreEspulsore_HUN		// Motor t�pus 1
Motor Tipus 2; MotoreEspulsore2_HUN 	// Motor t�pus 2
Ventilatorok Szama; Multifan_HUN	// Ventil�torok Sz�ma
Vizes Mod Engedelyezese; IdroMode_HUN	// Vizes m�d enged�lyez�se
Hasznalati Melegviz nezet; VisSanitari_HUN	// Haszn�lati melegv�z n�zet
Kazan Tipus Nezet; VisTipoStufa_HUN	// Kaz�n t�pus n�zet
Legtomegaram Ellenorzes; SensorePortataAria_HUN	// L�gt�meg�ram ellenorz�s
1-es Fustgazventilator Fordulatszam Ellenorzes; SensoreHall_HUN	// 1-es f�stg�zventil�tor fordulatsz�m ellenorz�s
Lambda Szonda Ellenorzes; ControlloLambda_HUN	// Lambda szonda ellenorz�s
Also Fordulatszam Hatarertek; LimGiriMinimi_HUN	// Als� fordulatsz�m hat�r�rt�k
Biztonsagi Homerseklet Erzekelo; AccTermocoppia_HUN	// Biztons�gi hom�rs�klet �rz�kelo
Fotoelektromos Langor; AccFotoResistenza_HUN	// Fotoelektromos l�ngor
Inditas B Tiltas; InibAccB_HUN	// Ind�t�s B tilt�s
;Gyujto Rampa; RampaCandeletta_HUN	// gy�jt� r�mpa
Termosztat; Termostato_Ambiente_HUN	// Termoszt�t
Teljesitmeny/Homerseklet Mod; GestioneIbrida_HUN	// Teljes�tm�ny/hom�rs�klet m�d
;Fa/Pellet Vezerles; GestioneLegna_HUN	// Fa/Pellet Vez�rl�s
Mechanikus Tisztitas; FScuotitore_HUN	// Mechanikus tiszt�t�s
Pellet Adagolo 2 / Hamu Kihordo; Coclea2_HUN	// Pellet adagol� 2 / Hamu kihord�	//
2-es Fustgazventilator Engedelyezes; Esp2Enable_HUN	// 2-es f�stg�zventil�tor enged�lyez�s
2-es Fustgazventilator Fordulatszam Ellenorzes; SensoreHall_2_HUN	// 2-es f�stg�zventil�tor fordulatsz�m ellenorz�s
Pellet Szint Erzekelo; SensorePellet_HUN	// Pellet szint �rz�kelo

; parametri blackout
Visszaallitas Engedelyezese; BlackoutRipristino_HUN	// Vissza�ll�t�s enged�lyez�se
Visszaallitasi Idotartam; BlackoutDurata_HUN	// Vissza�ll�t�si idotartam

; parametri coclea
Pellet Adagolo 1 Fekkel; FrenataCoclea_HUN	// Pellet adagol� 1 f�kkel
Pellet Adagolo 1 Fek Tipus; TipoFrenataCoclea_HUN	// Pellet adagol� 1 f�k t�pus
Pellet Adagolo 2 Fekkel; FrenataCoclea_2_HUN	// Pellet adagol� 2 f�kkel
Pellet Adagolo 2 Fek Tipus; TipoFrenataCoclea_2_HUN	// Pellet adagol� 2 f�k t�pus
Pellet Adagolo 1 Fordulatszam Ellenorzes; CtrlGiriCoclea_HUN	// Pellet adagol� 1 fordulatsz�m ellenorz�s
Pellet Adagolo 1 Periodusido; PeriodoCoclea_HUN			// Pellet adagol� 1 peri�dusido
Pellet Adagolo 2 Periodusido; PeriodoCoclea2_HUN   	// Pellet adagol� 2 peri�dusido
Pellet Adagolo 1 Periodusido Engedelyezese; AbilPeriodoCoclea_HUN	// Pellet adagol� 1 peri�dusido enged�lyez�se
Pellet Adagolo 2 Periodusido Engedelyezese; AbilPeriodoCoclea2_HUN	// Pellet adagol� 2 peri�dusido enged�lyez�se

; parametri scuotitore
Fel Ciklus Idotartam; ScuotDurata_HUN	// F�l ciklus idotartam
Ciklusok Szama; ScuotNCicli_HUN	// Ciklusok sz�ma
;Ciklus Intervallum; ScuotPeriodo_HUN

; parametri fan ambiente
Vezerles Fustgaz Homerseklettel; FanCtrlTempFumi_HUN	// Vez�rl�s f�stg�z hom�rs�klettel
Ventilator Bekapcsolasi Homerseklet; FanTempFumiON_HUN	// Ventil�tor bekapcsol�si hom�rs�klet
Ventilator Minimum Homerseklet; FanTempFumiOFF_HUN	// Ventil�tor minimum hom�rs�klet

; parametri fan
1-es Szint; FanAttuaL1_HUN
2-es Szint; FanAttuaL2_HUN
3-es Szint; FanAttuaL3_HUN
4-es Szint; FanAttuaL4_HUN
5-es Szint; FanAttuaL5_HUN

; parametri ricette

; parametri eco
Ecostop; EcoStop_HUN
Varakoztatas Bekapcsolas; EcoAttesaAccensione_HUN	// V�rakoztat�s bekapcsol�s
Varakoztatas Kikapcsolas; Settings_TShutdown_Eco_HUN	// V�rakoztat�s kikapcsol�s
Homerseklet Kulonbseg; EcoDeltaTemperatura_HUN	// Hom�rs�klet k�l�nbs�g

; parametri tempi funzionamento

; parametri idro
Fuggetlen Vizes Mod; IdroIndipendente_HUN	// F�ggetlen vizes m�d
Vizes Kikapcslas; SpegnimentoIdro_HUN	// Vizes kikapcsl�s
;Akkumulator; FAccumulo_HUN	// Akkumul�tor
Szivattyu Erzekeles Tiltas; InibSensing_HUN	// Szivatty� �rz�kel�s tilt�s
Szivattyu Modulacio; PompaModulante_HUN	// Szivatty� modul�ci�	//
Vizes Nyomaskapcsolo; FPressIdro_HUN	// Vizes nyom�skapcsol�	//
;Masodlagos Fluxmeter; FlussostatoDigitale_HUN	// M�sodlagos Fluxmeter
Futesi Hozam; GainRisc_HUN	// Fut�si hozam
Viz Homerseklet Hiszterezes; IsteresiTempAcqua_HUN	// V�z hom�rs�klet hiszter�z�s
Hasznalati Melegviz Homerseklet Kulonbseg; DeltaSanitari_HUN	// Haszn�lati melegv�z hom�rs�klet k�l�nbs�g
Hasznalati Melegviz Hozam; GainSanit_HUN	// Haszn�lati melegv�z hozam
AUX 2 Aktivalasi Homerseklet; TempAux_HUN	// Aux2 aktiv�l�si hom�rs�klet
Maximalis Futoviz Nyomas; MaxPressH2O_HUN	// Maxim�lis futov�z nyom�s
Szivattyu Bekapcsolasi Homerseklet; TSETON_Pompa_HUN	// Szivatty� bekapcsol�si hom�rs�klet
Szivattyu Kikapcsolasi Homerseklet; TSETOFF_Pompa_HUN	// Szivatty� kikapcsol�si hom�rs�klet

; parametri transitori
Elogyujtas 1 Idotartam; AccTempoPreacc_HUN	// Elogyujt�s 1 idotartam
Elogyujtas 2 Idotartam; AccTempoPreLoad_HUN	// Elogyujt�s 2 idotartam
Meleg Elogyujtas Idotartam; AccTempoPreacc2_HUN	// Meleg elogy�jt�s idotartam
Bekapcsolasi Fustgaz Homerseklet; AccTempOnFumi_HUN	// Bekapcsol�si f�stg�z hom�rs�klet
Kikapcsolasi Fustgaz Homerseklet; AccTempOffFumi_HUN	// Kikapcsol�si f�stg�z hom�rs�klet
Fa Gyulladasi Hatarertek; AccTempSogliaFumi_HUN	// Fa gyullad�si hat�r�rt�k
Meleg Elogyujtas Varakozas; AccTempoInc_HUN	// Meleg elogy�jt�s v�rakoz�s
Meleg Homerseklet Kulonbseg; AccDeltaTempCaldo_HUN	// Meleg hom�rs�klet k�l�nbs�g
Maximalis Begyujtasi Idotartam; AccMaxTimeWarmUp_HUN	// Maxim�lis begy�jt�si idotartam
Langstabilizalasi Idotartam; AccMaxTimeFireOn_HUN	// L�ngstabiliz�l�si idotartam
Kikapcsolasi Idotartam; AccMaxTimeSpe_HUN	// Kikapcsol�si idotartam

; parametri di attuazione
Pellet Adagolo 1 Mukodesi Ido; AttuaTempoOnCoclea1_HUN	// Pellet adagol� 1 muk�d�si ido
Pellet Adagolo 1 Allasi Ido; AttuaTempoOffCoclea1_HUN	// Pellet adagol� 1 �ll�si ido
Legaram; AttuaPortata_HUN	// L�g�ram
Fustgaz Ventilator 1 Fordulatszam; AttuaGiriEsp_HUN	// F�stg�z ventil�tor 1 fordulatsz�m
Fustgaz Ventilator 2 Fordulatszam; AttuaGiriEsp2_HUN	// F�stg�z ventil�tor 2 fordulatsz�m
Pellet Adagolo 2 Mukodesi Ido; AttuaTempoOnCoclea2_HUN	// Pellet adagol� 2 muk�d�si ido
Pellet Adagolo 2 Allasi Ido; AttuaTempoOffCoclea2_HUN	// Pellet adagol� 2 �ll�si ido
Fustgaz Ventilator 2; AttuaEspulsore2_HUN	// F�stg�z ventil�tor 2

; parametri di potenza
Teljesitmeny Szintek; LivPotenzaMax_HUN	// Teljes�tm�ny szintek

; parametri pulizia braciere
Intervallum; AttesaPB_HUN
Idotartam; DurataPB_HUN	// Idotartam
Hatarertek; MinPowerPB_HUN	// Hat�r�rt�k

; parametri ritardo espulsore
Felfutas Kesleltetesi Ido; RitardoEspInc_HUN	// Felfut�s k�sleltet�si ido
Lefutas Kesleltetesi Ido; RitardoEspDec_HUN	// Lefut�s k�sleltet�si ido

; parametri ritardo step potenza
Fokozat Emeles Kesleltetes; RitardoPotInc_HUN	// Fokozat emel�s k�sleltet�s
Fokozat Csokkentes Kesleltetes; RitardoPotDec_HUN	// Fokozat cs�kkent�s k�sleltet�s

; parametri allarme fumi
Elorisztas tiltas; AlmFumi_InibPreAlm_HUN	// Eloriszt�s tilt�s
Fustgaz Homerseklet Elo Risztas; AlmFumi_TempPreAlm_HUN	// F�stg�z hom�rs�klet elo riszt�s
Maximalis Fustgaz Homerseklet Elo Risztas Idotartam; AlmFumi_DurataPreAlm_HUN	// Maxim�lis f�stg�z hom�rs�klet elo riszt�s idotartam
Elorisztas Hiszterezis; AlmFumi_GradientePreAlm_HUN	// Eloriszt�s hiszter�zis
Riasztasi Fustgaz Homerseklet; AlmFumi_TempAlm_HUN	// Riaszt�si f�stg�z hom�rs�klet

; parametri sensore pellet
Elorisztas Idotartam; SensPellet_DurataPreAlm_HUN	// Eloriszt�s idotartam

; parametri assenza fiamma
Kikapcsolasi Fustgaz Homerseklet Kulonbseg; NoFiamma_DeltaTemp_HUN	// Kikapcsol�si f�stg�z hom�rs�klet k�l�nbs�g

; parametri sensore portata aria
Nyitott Ajto Hatarertek; Aria_PortataCritica_HUN	// Nyitott ajt� hat�r�rt�k
Nyitott Ajto Idotartam; Aria_TempoAlmPorta_HUN	// Nyitott ajt� idotartam
Nyitott Ajto Elorisztas Idotartam; Aria_TempoPreAlmPorta_HUN	// Nyitott ajt� eloriszt�s idotartam
Egesi Levego Risztas Tiltas; Aria_InibAlmAriaCombust_HUN	// �g�si levego riszt�s tilt�s
Egesi Levego Elorisztas Idotartam; Aria_TempoPreAlmAriaCombust_HUN	// �g�si levego eloriszt�s idotartam
Egesi Levego Kulonbseg; Aria_DeltaPortAriaCombust_HUN	// �b�si levego k�l�nbs�g

; comandi
Kezi Teszteles; Test_StartStop_HUN	// K�zi tesztel�s
Automata Teszteles; Test_Sequenza_HUN	// Automata tesztel�s
Bypass; Test_Bypass_HUN
Biztonsagi Homerseklet Erzekelo Kalibralas; Test_CalibTC_HUN	// Biztons�gi hom�rs�klet �rz�kelo kalibr�l�s
Fotoelektromos Langor Kalibralas Bekapcsolas; Test_CalibFotoresOn_HUN	// Fotoelektromos l�ngor kalibr�l�s bekapcsol�s
Fotoelektromos Langor Kalibralas Kikapcsolas; Test_CalibFotoresOff_HUN	// Fotoelektromos l�ngor kalibr�l�s kikapcsol�s
Teszteles Leptetes Idotartam; Test_DurataStep_HUN	// Tesztel�s l�ptet�s idotartam

; carichi
Pellet Adagolo; Test_Coclea_HUN	// Pellet adagol�	//
Gyujto; Test_Candeletta_HUN	// Gy�jt�	//
Ventilator 1; Test_Fan1_HUN	// Ventil�tor 1
Ventilator 2; Test_Fan2_HUN	// Ventil�tor 2
Ventilator 3; Test_Fan3_HUN	// Ventil�tor 3
Fustgaz Ventilator 1; Test_Espulsore_HUN	// F�stg�z ventil�tor 1
Szivattyu; Test_Pompa_HUN	// Szivatty�	//
3-utas Szelep; Test_3Vie_HUN
Aux 1; Test_Aux1_HUN
Aux 2; Test_Aux2_HUN
Aux A; Test_AuxA_HUN

; attuazioni
Ventilator 1 Aktivalas; AttuaFan1_HUN	// Ventil�tor 1 aktiv�l�s
Ventilator 2 Aktivalas; AttuaFan2_HUN	// Ventil�tor 2 aktiv�l�s
Ventilator 3 Aktivalas; AttuaFan3_HUN	// Ventil�tor 3 aktiv�l�s

; richieste di conferma
Jovahagyas?; ReqConf_HUN	// J�v�hagy�s?
Rovidzar Terminalok JE es Utan Engedelyezese; TC_ReqConf_HUN	// R�vidz�r termin�lok �s JE ut�n enged�lyez�se

; Crono
Idozito; Menu_Crono_HUN	// Idoz�to
Engedelyezes; Abilitazione_HUN	// Enged�lyez�s
Profil Feltoltes; CaricaProfilo_HUN	// Profil felt�lt�s
Reset; Azzera_HUN

; prog. crono settimanale
Program; PrgSettimanale_HUN
Engedelyezes; PrgAbilita_HUN	// Enged�lyez�s
Start; PrgStart_HUN
Stop; PrgStop_HUN
Leghomerseklet; PrgTempAria_HUN	// L�ghom�rs�klet
Futesi V�z Homerseklet; PrgTempH2O_HUN	// Fut�si v�z hom�rs�klet
Lang; PrgPotenza_HUN	// L�ng

;01 sigle fan
F1; sF1_HUN,
F2; sF2_HUN,

;0123 stati di attuazione
 OFF; sAttuaStat00_HUN,
PSU1; sAttuaStat01_HUN,
PSU2; sAttuaStat02_HUN,
WPSU; sAttuaStat03_HUN,
SU A; sAttuaStat04_HUN,
SU B; sAttuaStat05_HUN,
SU C; sAttuaStat06_HUN,
FONA; sAttuaStat07_HUN,
FONB; sAttuaStat08_HUN,
SD A; sAttuaStat09_HUN,
SD B; sAttuaStat10_HUN,
SD C; sAttuaStat11_HUN,
CD A; sAttuaStat12_HUN,
CD B; sAttuaStat13_HUN,
BCLA; sAttuaStat14_HUN,
BCLB; sAttuaStat15_HUN,
PL 1; sAttuaStat16_HUN,
PL 2; sAttuaStat17_HUN,
PL 3; sAttuaStat18_HUN,
PL 4; sAttuaStat19_HUN,
PL 5; sAttuaStat20_HUN,
PL 6; sAttuaStat21_HUN,
PL 7; sAttuaStat22_HUN,
PL 8; sAttuaStat23_HUN,
PL 9; sAttuaStat24_HUN,
PLSA; sAttuaStat25_HUN,


