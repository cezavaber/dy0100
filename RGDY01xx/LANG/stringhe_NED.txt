; 
; \file stringhe_NED.c
; \brief NED strings set
; 


; week days
Zondag; sDomenica_NED
Maandag; sLunedi_NED
Dinsdag; sMartedi_NED
Woensdag; sMercoledi_NED
Donderdag; sGiovedi_NED
Vrijdag; sVenerdi_NED
Zaterdag; sSabato_NED

Off; sOff_NED
On ; sOn_NED
Auto; sAuto_NED
�C; sDegC_NED
�F; sDegF_NED
H2O; sH2O_NED

; stati della stufa
ONTSTEKING; Stufa_Accensione_NED
ON; Stufa_Accesa_NED
UITSTEKING; Stufa_Spegnimento_NED
OFF; Stufa_MCZ_NED
ALARM; Stufa_Allarme_NED
UITSTEKING NA STROOM AFSNIJDEN; Stufa_SpeRete_NED
ONTSTEKING NA STROOM AFSNIJDEN; Stufa_AccRete_NED
RESET; Stufa_Reset_NED
TEST; Stufa_Collaudo_NED
 ; Stufa_NonDef_NED

; allarme
ALARM; Alarm_NED

; descrizione allarme
 ; AlarmSource00_NED
Ontsteking Gemist; AlarmSource01_NED
Vlam Uit; AlarmSource02_NED
Oververhitting Pellet Tank; AlarmSource03_NED
Overmatig Temperatuur Van de Rookgassen; AlarmSource04_NED
Alarm Drukschakelaar; AlarmSource05_NED
Alarm Verbrandingslucht; AlarmSource06_NED
Open Deur; AlarmSource07_NED
Rook Extractor Defect; AlarmSource08_NED
Rook Peiling Defect; AlarmSource09_NED
Kaars Defect; AlarmSource10_NED
Motor Pellet Defect; AlarmSource11_NED
 ; AlarmSource12_NED
Elektronische Kaart Defect; AlarmSource13_NED
 ; AlarmSource14_NED
Alarmniveau Pellet; AlarmSource15_NED
Water Druk Buiten Normen; AlarmSource16_NED
Pellet Tank Deur Open; AlarmSource17_NED
Oververhitting Water Tank; AlarmSource18_NED

; ripristino allarme
 ; AlarmRecovery00_NED
Vuurpot Kuissen en Opnieuw Starten; AlarmRecovery01_NED
Pellet Tank Vullen; AlarmRecovery02_NED
Zie Gebruiksaanwijzing; AlarmRecovery03_NED
Zie Gebruiksaanwijzing; AlarmRecovery04_NED
Verwijder Eventuele Obstructies; AlarmRecovery05_NED
Vuurpot/Luchtinlaat/Schoorsteen Controleren; AlarmRecovery06_NED
Dichting Deur Controleren; AlarmRecovery07_NED
Technieker Roepen; AlarmRecovery08_NED
Technieker Roepen; AlarmRecovery09_NED
Technieker Roepen; AlarmRecovery10_NED
Technieker Roepen; AlarmRecovery11_NED
Technieker Roepen; AlarmRecovery12_NED
Technieker Roepen; AlarmRecovery13_NED
Technieker Roepen; AlarmRecovery14_NED
Pellet Niveau Controleren; AlarmRecovery15_NED
Waterdruk Herstellen; AlarmRecovery16_NED
Dichting Pellet Tank Controleren; AlarmRecovery17_NED
Zie Gebruiksaanwijzing; AlarmRecovery18_NED

; sleep
Sleep; Menu_Sleep_NED

; avvio
Uur; Settings_Time_NED
Datum; Menu_DataOra_NED	
Vermogen; Potenza_NED		
Temperatuur; Temperatura_NED
Water Temp.; Menu_Idro_NED
Fan; Fan_NED

; network
Network; Menu_Network_NED
;Wi-Fi; WiFi_NED
SSID; Ssid_NED
Keyword; Keyword_NED
;TCP Port; TCPPort_NED
;WPS PIN; WPSPin_NED

; anomalie
Afwijkingen; Menu_Anomalie_NED
Service; RichiestaService_NED
Lucht Temp. Peiling Defect; GuastoSondaTempAria_NED
Water Temp. Peiling Defect; GuastoSondaTempAcqua_NED
Water Pressostaat Defect; GuastoPressostAcqua_NED
Water Druk Buiten Normen; SovraPressAcqua_NED
Air Flow Sensor Werkt Niet; GuastoSensorePortata_NED
Pellet Aan de Minimum; PelletEsaurito_NED
Open Deur; PortaAperta_NED

; info
Info; Menu_Infos_NED
Kaart Code; CodiceScheda_NED
Veiligheid Code; CodiceSicurezza_NED
Display Code; CodicePannello_NED
Parameters Code; CodiceParametri_NED
Werking Uren; OreFunzionamento_NED
Dienst Uren; OreSat_NED
Service; Service_NED
RPM Extractor 1; VentilatoreFumi_NED
Lucht Afvoer Gemeten; PortataAriaMisurata_NED
Restzuurstof; OssigenoResiduo_NED
Verbruik Pellet; Consumo_NED
Rook Temp.; TemperaturaFumi_NED
Photoresistor; FotoResistenza_NED
Tijd Pellet Voeding 1; TempoCoclea_NED
RPM Pellet Voeding 1; GiriCoclea_NED
Activatie Fan 1; MotoreScambiatore_NED
Activatie Fan 2; MotoreScambiatore2_NED
Water Druk; IdroPress_NED
Ontstekingen Nummer; NumAccensioni_NED
IP Address; IPAddress_NED
Alarmgeschiedenis; StoricoAllarmi_NED

; menu principale
Hoofdmenu; MainMenu_NED
Instellingen; Menu_Impostazioni_NED
Tech. Menu; Menu_Tecnico_NED

; settings
Taal; Lingua_NED
Eco; EcoMode_NED
Display; Settings_Backlight_NED
�C / �F; Settings_ShowTemp_NED
Pellet Laden; Precarica_Pellet_NED
Schoonmaak; Attiva_Pulizia_NED
Pomp Starten; Attiva_Pompa_NED
Radio ID; RFPanel_NED

; ricette
Recepten Verbrandin.; Ricette_NED
Air; OffsetEspulsore_NED
Pellet; RicettaPelletON_NED
Zuurstof; Ossigeno_NED

; temp H2O
Set Verwarming Temperatuur; TempH2O_SetRisc_NED
Set Sanitary Temperatuur; TempH2O_SetSan_NED

; menu nascosto
Password; HidPassword_NED

;012345678901234567890 menu tecnico - livello 1					
Configuratie; ConfigSystem_NED
Controle; Controllo_NED	// Contr�le
Hydro; MenuIdro_NED
Ontstek./Uitstek.; AttuaTransitorie_NED
Vermogen; AttuaPotenza_NED
Alarm Management; GestAllarmi_NED
Test; Collaudo_NED
Zoeken op Code; RicercaCodParam_NED

;012345678901234567890 menu tecnico - livello 2							
Instellingen; MenuParamGen_NED
Stroom Afsnijden; Blackout_NED
Voeding Pellets; MenuCoclea_NED
Ash/Brazier Clean.; MenuScuotitore_NED
Fan; FanAmbiente_NED
Fan 1; Fan1_NED
Fan 2; Fan2_NED
Fan 3; Fan3_NED

Eco Functie; MenuEco_NED
Time Werking; TempiFunz_NED

Instellingen; ParamTransitori_NED
PreOntsteking 1; Preacc1_NED
PreOntsteking 2; Preacc2_NED
PreOntsteking Warm; PreaccCaldo_NED
Ontsteking A; AccA_NED
Ontsteking B; AccB_NED
Brand ON; FireON_NED
Uitsteking A; SpeA_NED
Uitsteking B; SpeB_NED
Koeling; Raff_NED

Par. van Vermogen; ParamPotenza_NED
Schoonmaak Cycle; MenuPB_NED
Delay Extractor; RitardoAttuaEsp_NED
Vermogen Step Delay; RitardoStepPot_NED
Vermogen 1; Pot1_NED
Vermogen 2; Pot2_NED
Vermogen 3; Pot3_NED
Vermogen 4; Pot4_NED
Vermogen 5; Pot5_NED
Vermogen 6; Pot6_NED
Schoonmaak; Pulizia_NED
Interpolatie; Interpolazione_NED

Rook Alarm; AllarmeFumi_NED
Sensor Pellet; SensPellet_NED
;Vlamstoring; AssenzaFiamma_NED
Luchtstroom; SensAria_NED

Commands; Comandi_NED
Motors; Carichi_NED
Activaties; Attuazioni_NED

; parametri generali
Model Kachel; TipoStufa_NED
Herstellen Oorspronkelijke Model Kachel; TipoStufaDef_NED
Motor Type 1; MotoreEspulsore_NED
Motor Type 2; MotoreEspulsore2_NED
Aantal Fan; Multifan_NED
Toelaten Hydro; IdroMode_NED
Bekijkt Sanitair Water; VisSanitari_NED
Bekijkt Model Kachel; VisTipoStufa_NED
Air Flow Controle; SensorePortataAria_NED	// Air Flow Contr�le
RPM Controle; SensoreHall_NED	// RPM Contr�le
Lambda Controle; ControlloLambda_NED	// Lambda Contr�le
Min�RPM Begrenzer; LimGiriMinimi_NED
Thermokoppel; AccTermocoppia_NED
Photoresistor; AccFotoResistenza_NED
Inhibitie Ontsteking B; InibAccB_NED
;Rampa Candeletta; RampaCandeletta_NED
Thermostaat; Termostato_Ambiente_NED
Modus Brand/Temp.; GestioneIbrida_NED
;Beheer Hout/Pellet; GestioneLegna_NED
Mechanische Reiniging; FScuotitore_NED
Voeding Pellets 2 / Ash Cleaner; Coclea2_NED
Toelaten Extractor 2; Esp2Enable_NED
RPM Controle Extractor 2; SensoreHall_2_NED	// RPM Contr�le Extractor 2
Sensor Pellet Niveau; SensorePellet_NED

; parametri blackout
Enabling Recovery; BlackoutRipristino_NED
Time Restore; BlackoutDurata_NED

; parametri coclea
Rem Pellet Voeding 1 Motor; FrenataCoclea_NED
Typ Rem Pellet Voeding 1 Motor; TipoFrenataCoclea_NED
Rem Pellet Voeding 2 Motor; FrenataCoclea_2_NED
Typ Rem Pellet Voeding 2 Motor; TipoFrenataCoclea_2_NED
RPM Controle Pellet Voeding 1; CtrlGiriCoclea_NED	// RPM Contr�le Pellet Voeding 1
Tijd Pellet Voeding 1; PeriodoCoclea_NED
Tijd Pellet Voeding 2; PeriodoCoclea2_NED
Toelaten Tijd Pellet Voeding 1; AbilPeriodoCoclea_NED
Toelaten Tijd Pellet Voeding 2; AbilPeriodoCoclea2_NED

; parametri scuotitore
Tijd Half Cyclus; ScuotDurata_NED
Antaal Cycli; ScuotNCicli_NED
;Tijd Tussen Cycli; ScuotPeriodo_NED

; parametri fan ambiente
Control van Rook Temperatuur; FanCtrlTempFumi_NED
Temp. On Fan; FanTempFumiON_NED
Fan Min temp; FanTempFumiOFF_NED

; parametri fan
Niveau 1; FanAttuaL1_NED
Niveau 2; FanAttuaL2_NED
Niveau 3; FanAttuaL3_NED
Niveau 4; FanAttuaL4_NED
Niveau 5; FanAttuaL5_NED

; parametri ricette

; parametri eco
Ecostop; EcoStop_NED
Wachttijd On; EcoAttesaAccensione_NED
Wacht Off; Settings_TShutdown_Eco_NED
Delta Temp.; EcoDeltaTemperatura_NED

; parametri tempi funzionamento

; parametri idro
Onafhankelijk Hydro; IdroIndipendente_NED
Uitsteking Hydro; SpegnimentoIdro_NED
;Accumulo; FAccumulo_NED
Inhibitie Sensing Pomp; InibSensing_NED
Modulerende Pomp; PompaModulante_NED
Pressostaat Water; FPressIdro_NED
;Flussostato Secondario; FlussostatoDigitale_NED
Verwarwing Winst; GainRisc_NED
Water Temp. Hysteresis; IsteresiTempAcqua_NED
Delta Sanitair; DeltaSanitari_NED
Sanitair Winst; GainSanit_NED
Temp Activation AUX 2; TempAux_NED
Max Water Druk; MaxPressH2O_NED
Temp. On Pomp; TSETON_Pompa_NED
Temp. Off Pomp; TSETOFF_Pompa_NED

; parametri transitori
Tijd PreOnts. 1; AccTempoPreacc_NED
Tijd PreOnts. 2; AccTempoPreLoad_NED
Warme Tijd PreOntsteking; AccTempoPreacc2_NED
Rook Temp. On; AccTempOnFumi_NED
Rook Temp. Off; AccTempOffFumi_NED
Drempel Hout; AccTempSogliaFumi_NED
Warme Tijd PreOntsteking; AccTempoInc_NED
Delta Temperatuur Warm; AccDeltaTempCaldo_NED
Tijd Max. Ontsteking; AccMaxTimeWarmUp_NED
Tijd Brand On; AccMaxTimeFireOn_NED
Tijd Uitsteking; AccMaxTimeSpe_NED

; parametri di attuazione
T. On Pellet Voeding 1; AttuaTempoOnCoclea1_NED
T. Off Pellet Voeding 1; AttuaTempoOffCoclea1_NED
Luchtstroom; AttuaPortata_NED
RPM Extractor 1; AttuaGiriEsp_NED
RPM Extractor 2; AttuaGiriEsp2_NED
T. On Pellet Voeding 2; AttuaTempoOnCoclea2_NED
T. Off Pellet Voeding 2; AttuaTempoOffCoclea2_NED
Extractor 2; AttuaEspulsore2_NED

; parametri di potenza
Vermogensniveaus; LivPotenzaMax_NED

; parametri pulizia braciere
Interval; AttesaPB_NED
Tijd; DurataPB_NED
Drempel; MinPowerPB_NED

; parametri ritardo espulsore
Delay Groey; RitardoEspInc_NED
Delay Verlagen; RitardoEspDec_NED

; parametri ritardo step potenza
Delay Groey; RitardoPotInc_NED
Delay Verlagen; RitardoPotDec_NED

; parametri allarme fumi
Inhibitie Pre-Alarm; AlmFumi_InibPreAlm_NED
Roken Temp. Pre-Alarm; AlmFumi_TempPreAlm_NED
Tijd Max Pre-Alarm; AlmFumi_DurataPreAlm_NED
Pre-Alarm Hysteresis; AlmFumi_GradientePreAlm_NED
Rook Temp. Alarm; AlmFumi_TempAlm_NED

; parametri sensore pellet
Tijd Pre-Alarm; SensPellet_DurataPreAlm_NED

; parametri assenza fiamma
Delta Rook OFF; NoFiamma_DeltaTemp_NED

; parametri sensore portata aria
Drempel Open Deur; Aria_PortataCritica_NED
Tijd Open Deur; Aria_TempoAlmPorta_NED
Tijd Pre-Alarm Deur; Aria_TempoPreAlmPorta_NED
Inhibitie Alarm Verbrandingslucht; Aria_InibAlmAriaCombust_NED
Tijd Pre-Alarm Verbrandingslucht; Aria_TempoPreAlmAriaCombust_NED
Delta Luchtstroom Verbranding; Aria_DeltaPortAriaCombust_NED

; comandi
Manual Test; Test_StartStop_NED
Automatisch Test; Test_Sequenza_NED
Bypass; Test_Bypass_NED
Kalibratie Thermokoppel; Test_CalibTC_NED
Kalibratie Photoresistor On; Test_CalibFotoresOn_NED
Kalibratie Photoresistor Off; Test_CalibFotoresOff_NED
Tijd Stap Test; Test_DurataStep_NED

; carichi
Voeding Pellets 1; Test_Coclea_NED
Ontsteking Kaars; Test_Candeletta_NED
Fan 1; Test_Fan1_NED
Fan 2; Test_Fan2_NED
Fan 3; Test_Fan3_NED
Extractor 1; Test_Espulsore_NED
Pomp; Test_Pompa_NED
3-Wijze; Test_3Vie_NED
Aux 1; Test_Aux1_NED
Aux 2; Test_Aux2_NED
Aux A; Test_AuxA_NED

; attuazioni
Activatie Fan 1; AttuaFan1_NED
Activatie Fan 2; AttuaFan2_NED
Activatie Fan 3; AttuaFan3_NED

; richieste di conferma
Bevestigen?; ReqConf_NED
Kortsluiting van de Terminal JE en na Stellen; TC_ReqConf_NED

; Crono
Chrono; Menu_Crono_NED
Toelaten; Abilitazione_NED
Upload Profiel; CaricaProfilo_NED
Reset; Azzera_NED

; prog. crono settimanale
Programma; PrgSettimanale_NED
Toelaten; PrgAbilita_NED
U.Onst.; PrgStart_NED
U.Uitst.; PrgStop_NED
Luchttemp.; PrgTempAria_NED
Temp. Water; PrgTempH2O_NED
Brand; PrgPotenza_NED

;01 sigle fan
F1; sF1_NED,
F2; sF2_NED,

;0123 stati di attuazione
 OFF; sAttuaStat00_NED,
PSU1; sAttuaStat01_NED,
PSU2; sAttuaStat02_NED,
WPSU; sAttuaStat03_NED,
SU A; sAttuaStat04_NED,
SU B; sAttuaStat05_NED,
SU C; sAttuaStat06_NED,
FONA; sAttuaStat07_NED,
FONB; sAttuaStat08_NED,
SD A; sAttuaStat09_NED,
SD B; sAttuaStat10_NED,
SD C; sAttuaStat11_NED,
CD A; sAttuaStat12_NED,
CD B; sAttuaStat13_NED,
BCLA; sAttuaStat14_NED,
BCLB; sAttuaStat15_NED,
PL 1; sAttuaStat16_NED,
PL 2; sAttuaStat17_NED,
PL 3; sAttuaStat18_NED,
PL 4; sAttuaStat19_NED,
PL 5; sAttuaStat20_NED,
PL 6; sAttuaStat21_NED,
PL 7; sAttuaStat22_NED,
PL 8; sAttuaStat23_NED,
PL 9; sAttuaStat24_NED,
PLSA; sAttuaStat25_NED,


