/*!
	\file DecodInput.h
	\brief Include file del modulo DecodInput.c
*/

#ifndef _DECODINPUT_H
#define _DECODINPUT_H




/*! \var DigInput
    \brief Bitmap ingressi digitali
*/
extern TBitByte DigInput;
#define DIGIN						DigInput.BYTE
#define DIGIN_PULS					DigInput.BIT.D0	// =1 pulsante premuto
#define DIGIN_ALIM					DigInput.BIT.D1	// =1 alimentazione esterna presente



/*!
** \var VBatt_tab
** \brief Tabella dei parametri batteria
**/
typedef struct
{
	unsigned short VBattNom;	//!< livello tensione batteria nominale (mV)
	unsigned short VBattOff;	//!< livello tensione batteria scarica (mV)
	unsigned short VBattLow;	//!< basso livello tensione batteria (mV)
	unsigned short VBattFull;	//!< max livello tensione batteria carica (mV)
	unsigned short VBattADNom;	//!< valore A/D accumulato per tensione batteria nominale
} BATT_STRUCT;
extern BATT_STRUCT VBatt_tab;

extern short TempAmb;	//!< temperatura ambiente (0.01 degC)
extern unsigned short VBatt;	//!< tensione batteria (mV)
extern unsigned char BattUp;	//!< stato di ricarica batteria
extern unsigned short PercVBatt;	//!< percentuale di carica batteria (0.1%)

// variabili per stato di ricarica batteria
extern unsigned char CntSenseBat;
extern unsigned char CntChargeBat;


#define TEMP_AMB_NOM		(1800)	// temperatura ambiente nominale (18.00�C)

/*
	La tensione viene ridotta da un partitore di un fattore 1/2 e poi
	acquisita dall'ADC 12-bit con VREF = 3V.

	Tensione (mV)	= 1000 * vADC * (VREF/4095) * (2) =
							= (vADC * 3000 * 2) / (4095) =
							= (vADC * 6000) / 4095
*/
#define LITIO_VBATT_NOM			(3700)	// tensione batteria nominale (mV)
#define LITIO_VBATT_ANIN_NOM	(2525)	//(20202)	// valore A/D accumulato per tensione batteria nominale
// batteria al litio 3.7 V ricaricabile
#define LITIO_VBATT_OFF		(3500)	// batteria scarica in scarica: 3.5 V
#define LITIO_VBATT_OFF_ALIM	(3550)	// batteria scarica in carica: 3.55 V
#define LITIO_VBATT_LOW		(3410)	// soglia di forzatura low-power: 3.41 V
#define LITIO_VBATT_FULL	(4100)	// batteria completamente carica: 4.1 V

#define BATTERY_CHARGE				0	// stato batteria: in carica
#define BATTERY_DISCHARGE			1	// stato batteria: in scarica
#define BATTERY_CHARGE_ERROR		2	// stato batteria: in errore di ricarica

/*
	La tensione VREFINT (1.21 V) viene acquisita dall'ADC 12-bit con VREF = 3V.

	VREFINT (mV)	= 1000 * vADC * (VREF/4095) =
							= (vADC * 3000) / 4095
	
	Il valore VREFINT_CAL acquisito in produzione dall'ADC con VREF = 3,3V e'
	disponibile alla locazione 0x1FFF7A2A, per cui:
	
	VREFINT (mV)	= 1000 * VREFINT_CAL * (VREF/4095) =
							= (VREFINT_CAL * 3300) / 4095
	
*/
#define VREFINT_CAL			(*(unsigned short *)0x1FFF7A2A)
//#define VREFINT_NOM			(1210)	// tensione VREFINT nominale (mV)
#define VREFINT_NOM(x)			(((x) * 3300UL) / 4095UL)	// tensione VREFINT nominale (mV)
#define VREFINT_ANIN_NOM	(1652)	//(13213)	// valore A/D accumulato per tensione VREFINT


/*! \fn void inpInit( void )
    \brief Inizializza gestore ingressi
*/
void inpInit( void );

/*! \fn void inpHandler( void )
    \brief Gestore ingressi
*/
void inpHandler( void );

/*! \fn void inpMonitor( void )
    \brief Monitor degli ingressi acquisiti
*/
void inpMonitor( void );

/*!
 * \fn void DecodSenseCharge( void )
 * \brief Rileva lo stato di ricarica della batteria
 * \note La funzione presuppone di essere chiamata ogni 50ms
 */
void DecodSenseCharge(void);

/*!
** \fn short ExecTAConv( void )
** \brief Esegue una singola acquisizione TA con aggiornamento TempAmb
** \return 1: OK, 0: FAILED
** 
** \details Si considera RTOS sospeso e I2C mutex non definito
**/
short ExecTAConv( void );

/*!
 * \fn void INT1_Acc_irq( void )
 * \brief Gestione INT1_ACC interrupt
 * \return None
 */
void INT1_Acc_irq( void );

/*!
 * \fn void INT2_Acc_irq( void )
 * \brief Gestione INT1_ACC interrupt
 * \return None
 */
void INT2_Acc_irq( void );

/*!
 * \fn void inpRestart( void )
 * \brief Riavvia il gestore ingressi
 * \return None
 */
void inpRestart( void );

/*!
 * \fn void inpSuspend( void )
 * \brief Sospende il gestore ingressi
 * \return None
 */
void inpSuspend( void );



#endif	// _DECODINPUT_H
