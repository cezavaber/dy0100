/*!
** \file languages.h
** \author Alessandro Vagniluca
** \date 15/10/2015
** \brief LANGUAGES.C header file
** 
** \version 1.0
**/

#ifndef _LANGUAGES_H_
#define _LANGUAGES_H_

#include "DatiStufa.h"
#include "fonts.h"


// languages allocation in the data-flash
#define LANG_HEADER_ARRAY_ADDR		(0x003C0000)	//!< language header array start address
#define LANG_RAW_DATA_ADDR				(0x003D0000)	//!< languages raw data start address
#define LANG_MAX_ADDR					(0x003FFFEF)	//!< languages raw data end address
#define LANG_FIRST_64K_SECT		60		//!< the first 64KB sector for languages
#define LANG_NUM_64K_SECT			4		//!< 64KB sectors number for languages


#define LANG_DEF		255	// codice per lingua al default

/*!
** \enum Lang
** \brief The ROM languages' enumeration
**/
enum Lang
{
	LANG_ITA,		// Italiano - Ita
	LANG_ENG,		// Inglese - Eng
	
	NROMLANG
};
extern unsigned char NumLanguage;	// total number of languages

#define MAX_LEN_STRINGA		60		// max length for all the strings

#define cDEG				((fontMax == FONT_MAX_DEF) ? '\'' : '\200')
#define cBLANK				' '

#define LENGTH_LOGO			16
extern char pChLogo[LENGTH_LOGO];
#define LENGTH_TYPEDESC		8
extern char pChTipoDesc[NTYPE][LENGTH_TYPEDESC];
#define DISPLAY_MAX_COL		21	// max dimensione stringa generica per font 29
#define MENU_MAX_COL			26	// max dimensione stringa nei menu` per font 28
extern const char sBlank[DISPLAY_MAX_COL+1];

extern const char sBitmapLoadError[];

extern const char sOPEN[];
extern const char sCLOSE[];
extern const char sSubMenu[];
extern const char sDescParInit[];	// descrizione parametro a inizio immissione

extern const char sESM[];

typedef const char*	PCSTR;
typedef const PCSTR*	PPCSTR;
extern const PPCSTR pSetMsg[NROMLANG];

extern const char pChLogoDefault[];


/*!
** \enum STRING_TAB_INDEX
** \brief The strings' enumeration
**/
enum STRING_TAB_INDEX
{
	// week days
	sDOMENICA,
	sLUNEDI,
	sMARTEDI,
	sMERCOLEDI,
	sGIOVEDI,
	sVENERDI,
	sSABATO,
	
	sOFF,
	sON,
	sAUTO,
	sDEGC,
	sDEGF,
	sH2O,

	// stati della stufa
	STUFA_ACCENSIONE,
	STUFA_ACCESA,
	STUFA_SPEGNIMENTO,
	STUFA_SPENTA,
	STUFA_ALLARME,
	STUFA_SPERETE,
	STUFA_ACCRETE,
	STUFA_RESET,
	STUFA_COLLAUDO,
	STUFA_NONDEF,

	// allarme
	ALARM,

	// descrizione allarme
	ALARMSOURCE00,
	ALARMSOURCE01,
	ALARMSOURCE02,
	ALARMSOURCE03,
	ALARMSOURCE04,
	ALARMSOURCE05,
	ALARMSOURCE06,
	ALARMSOURCE07,
	ALARMSOURCE08,
	ALARMSOURCE09,
	ALARMSOURCE10,
	ALARMSOURCE11,
	ALARMSOURCE12,
	ALARMSOURCE13,
	ALARMSOURCE14,
	ALARMSOURCE15,
	ALARMSOURCE16,
	ALARMSOURCE17,
	ALARMSOURCE18,

	// ripristino allarme
	ALARMRECOVERY00,
	ALARMRECOVERY01,
	ALARMRECOVERY02,
	ALARMRECOVERY03,
	ALARMRECOVERY04,
	ALARMRECOVERY05,
	ALARMRECOVERY06,
	ALARMRECOVERY07,
	ALARMRECOVERY08,
	ALARMRECOVERY09,
	ALARMRECOVERY10,
	ALARMRECOVERY11,
	ALARMRECOVERY12,
	ALARMRECOVERY13,
	ALARMRECOVERY14,
	ALARMRECOVERY15,
	ALARMRECOVERY16,
	ALARMRECOVERY17,
	ALARMRECOVERY18,

	// sleep
	MENU_SLEEP,

	// avvio
	d_HOUR,
	d_DATE,
	sFIRE,
	sTEMP,
	MENU_IDRO,
	sFAN,
	
	// network
	MENU_NETWORK,
	//d_WIFI,
	d_SSID,
	d_KEYWORD,
	//d_TCP_PORT,
	//d_WPS_PIN,
	
	// anomalie
	MENU_ANOMALIE,
	RICHIESTASERVICE,
	GUASTOSONDATEMPARIA,	
	GUASTOSONDATEMPACQUA,	
	GUASTOPRESSOSTACQUA,	
	SOVRAPRESSACQUA,	
	GUASTO_SENSORE_PORTATA,
	PELLET_ESAURITO,
	PORTA_APERTA,

	// info
	MENU_INFOS,
	d_CODICE_SCHEDA,
	d_CODICE_SICUREZZA,
	d_CODICE_PANNELLO,
	d_CODICE_PARAM,
	d_OREFUNZIONAMENTO,
	d_ORESAT,
	d_SERVICE,
	d_VENTILATOREFUMI,
	d_PORTATAARIAMISURATA,
	d_OSSIGENORESIDUO,
	d_CONSUMO,
	d_TEMPERATURAFUMI,
	d_FOTORESISTENZA,
	d_TEMPOCOCLEA,
	d_GIRICOCLEA,
	d_MOTORESCAMBIATORE,
	d_MOTORESCAMBIATORE_2,
	d_PRESSIONEIDRO,
	d_NUM_ACCENSIONI,
	d_IP_ADDRESS,
	d_STORICO_ALLARMI,
	
	// menu principale
	sMAINMENU_TITLE,
	MENU_IMPOSTAZIONI,
	MENU_TECNICO,

	// settings
	d_LINGUA,
	d_ECO,
	d_BACKLIGHT,
	d_SHOWTEMP,
	d_PRECARICAPELLET,
	d_PULIZIA,
	d_ATTIVA_POMPA,
	d_RFPANEL,
	
	// ricette
	sRICETTE_MENU,
	d_OFFSETESPULSORE_ON,
	d_RICETTAPELLET_ON,
	d_OSSIGENO,
	
	// temp H2O
	d_TEMPH2O_SETRISC,
	d_TEMPH2O_SETSAN,

	// menu nascosto
	HID_PASSWORD,
	
	// menu tecnico - livello 1
	sCONFIG_SYSTEM,
	sCONTROLLO,
	d_IDRO_MENU,
	sATTUA_TRANSITORIE,
	sATTUA_POTENZA,
	sGESTIONE_ALLARMI,
	sCOLLAUDO,
	sRICERCA_CODICE_PARAM,
	
	// menu tecnico - livello 2
	d_GEN_MENU,
	sBLACKOUT,
	d_COCLEA_MENU,
	d_SCUOTITORE_MENU,
	sFANAMB_MENU,
	sFAN1_MENU,
	sFAN2_MENU,
	sFAN3_MENU,
	
	d_ECOSTOP_MENU,
	sTEMPI_FUNZ_MENU,
	
	sPARAM_TRANSITORI_MENU,
	sPREACC1_MENU,
	sPREACC2_MENU,
	sPREACCCALDO_MENU,
	sACCA_MENU,
	sACCB_MENU,
	sFIREONA_MENU,
	sSPEA_MENU,
	sSPEB_MENU,
	sRAFFA_MENU,
	
	sPARAM_POTENZA_MENU,
	d_PB_MENU,
	sRITARDO_ATTUA_ESP_MENU,
	sRITARDO_STEP_POT_MENU,
	sPOT1_MENU,
	sPOT2_MENU,
	sPOT3_MENU,
	sPOT4_MENU,
	sPOT5_MENU,
	sPOT6_MENU,
	sPULIZIA_MENU,
	sINTERPOLAZIONE_MENU,
	
	sALLARME_FUMI_MENU,
	sSENSORE_PELLET_MENU,
	//sASSENZA_FIAMMA_MENU,
	sSENSORE_ARIA_MENU,
	
	sCOMANDI_MENU,
	sCARICHI_MENU,
	sATTUA_MENU,
	
	// parametri generali
	d_TIPOSTUFA,
	d_TIPOSTUFA_DEFAULT,
	d_TIPOMOTORE,
	d_TIPOMOTORE_2,
	d_MULTIFAN,
	d_IDRO,
	d_VISSANITARI,
	d_VISTIPOSTUFA,
	d_SENSOREPORTATAARIA,
	d_SENSOREHALL,
	d_CONTROLLOLAMBDA,
	d_LIM_GIRI_MINIMI,
	d_ACC_TERMOCOPPIA,
	d_ACC_FOTORESISTENZA,
	d_INIB_ACCB,
	//d_RAMPA_CAND,
	d_TERMAMB,
	d_IBRIDA,
	//d_LEGNA,
	d_SCUOTITORE,
	d_COCLEA2,
	d_ESP2ENABLE,
	d_SENSOREHALL_2,
	d_SENSOREPELLET,

	// parametri blackout
	d_BLK_RIPRISTINO,
	d_BLK_DURATA,

	// parametri coclea
	d_FRENATACOCLEA,
	d_TIPO_FRENATACOCLEA,
	d_FRENATACOCLEA_2,
	d_TIPO_FRENATACOCLEA_2,
	d_GIRI_COCLEA,
	d_PERIODO_COCLEA,
	d_PERIODO_COCLEA_2,
	d_EN_PERIODO_COCLEA,
	d_EN_PERIODO_COCLEA_2,

	// parametri scuotitore
	d_SCUOT_DURATA, 
	d_SCUOT_NCICLI,
	//d_SCUOT_PERIODO,

	// parametri fan ambiente
	d_CTRL_TEMP_FUMI,
	d_FAN_TFUMI_ON,
	d_FAN_TFUMI_OFF,	

	// parametri fan 
	d_FAN_L1, 	
	d_FAN_L2, 	
	d_FAN_L3, 	
	d_FAN_L4, 	
	d_FAN_L5, 	

	// parametri ricette

	// parametri eco
	d_ECOSTOP,
	d_TWARMUP_ECO,
	d_TSHUTDOWN_ECO,
	d_DELTATEMP_ECO,

	// parametri tempi funzionamento

	// parametri idro
	d_IDROINDIP,
	d_SPEIDRO,
	//d_ACCUMULO,
	d_INIB_SENSING,
	d_POMPA_MODULANTE,
	d_ENPRESSIDRO,
	//d_FLUSS_DIGIT,
	d_GAIN_RISC,
	d_ISTERESI_TEMP_ACQUA,
	d_DELTA_SANITARI,
	d_GAIN_SANIT,
	d_TEMPAUX,
	d_MAXPRESSH2O,
	d_SETTONPOMPA,
	d_SETTOFFPOMPA,

	// parametri transitori
	d_TPREACC,		
	d_TPRELOAD,
	d_TPREACC2,		
	d_TEMP_ON_FUMI,	
	d_TEMP_OFF_FUMI,	
	d_TEMP_SOGLIA_FUMI,	
	d_TACCINC,
	d_DELTA_TEMP_CALDO,
	d_MAX_TWARMUP,
	d_MAX_TFIREON,
	d_MAX_TSPE,
	
	// parametri di attuazione
	d_ATTUA_TON_COCLEA_1,
	d_ATTUA_TOFF_COCLEA_1,
	d_ATTUA_PORTATA,
	d_ATTUA_GIRI,
	d_ATTUA_GIRI_2,
	d_ATTUA_TON_COCLEA_2,
	d_ATTUA_TOFF_COCLEA_2,
	d_ATTUA_ESP2,
	
	// parametri di potenza
	d_LIVPOT_MAX,
	
	// parametri pulizia braciere
	d_TWAIT_PB,
	d_TPB,
	d_MINPOWER_PB,
	
	// parametri ritardo espulsore
	d_RITARDO_ESP_INC,
	d_RITARDO_ESP_DEC,
	
	// parametri ritardo step potenza
	d_RITARDO_POT_INC,
	d_RITARDO_POT_DEC,
	
	// parametri allarme fumi
	d_ALMFUMI_INIB_PREALM,
	d_ALMFUMI_TEMP_PREALM,	
	d_ALMFUMI_TPREALM,	
	d_GRAD_PREALM_FUMI,	
	d_TEMP_ALLARME_FUMI,
	
	// parametri sensore pellet
	d_SENSPELLET_TPREALM,
	
	// parametri assenza fiamma
	d_NOFIAMMA_DELTA_TEMP,
	
	// parametri sensore portata aria
	d_PORTATA_CRITICA,
	d_TALM_PORTA,
	d_TPREALM_PORTA,
	d_INIBALM_ARIA_COMBUST,
	d_TPREALM_ARIA_COMBUST,
	d_DELTA_PORTATA_ARIA_COMBUST,
	
	// comandi
	d_TEST_START_STOP,
	d_TEST_SEQUENZA_TEMP,
	d_TEST_BYPASS,
	d_TEST_CALIB_TC,
	d_TEST_CALIB_FOTORES_ON,
	d_TEST_CALIB_FOTORES_OFF,
	d_TEST_DURATA_STEP,
	
	// carichi
	d_TEST_COCLEA,
	d_TEST_CANDELETTA,
	d_TEST_FAN1,
	d_TEST_FAN2,
	d_TEST_FAN3,
	d_TEST_ESPULSORE,
	d_TEST_POMPA,
	d_TEST_3VIE,
	d_TEST_AUX_1,
	d_TEST_AUX_2,
	d_TEST_AUX_A,
	
	// attuazioni
	d_ATTUA_FAN_1,
	d_ATTUA_FAN_2,
	d_ATTUA_FAN_3,

	// richieste di conferma
	REQCONF,
	TC_REQCONF,
	
	// Crono
	MENU_CRONO,
	ABILITAZIONE,
	CARICAPROFILO,
	AZZERA,	

	// prog. crono settimanale
	PRGSETTIMANALE,	
	PRGABILITA,
	PRGSTART,	
	PRGSTOP,	
	PRGTEMPARIA,	
	PRGTEMPH2O,	
	PRGPOTENZA,	
	
	// sigle fan
	sFAN1,
	sFAN2,

	// stati di attuazione
	sATTUASTAT00,
	sATTUASTAT01,
	sATTUASTAT02,
	sATTUASTAT03,
	sATTUASTAT04,
	sATTUASTAT05,
	sATTUASTAT06,
	sATTUASTAT07,
	sATTUASTAT08,
	sATTUASTAT09,
	sATTUASTAT10,
	sATTUASTAT11,
	sATTUASTAT12,
	sATTUASTAT13,
	sATTUASTAT14,
	sATTUASTAT15,
	sATTUASTAT16,
	sATTUASTAT17,
	sATTUASTAT18,
	sATTUASTAT19,
	sATTUASTAT20,
	sATTUASTAT21,
	sATTUASTAT22,
	sATTUASTAT23,
	sATTUASTAT24,
	sATTUASTAT25,
	
	
	NUMMSG
};

#define d_ATTUAZIONI	sATTUA_MENU

/*!
** \typedef RIF_MSG
** \brief The string reference structure.
**/
typedef struct __packed
{
	unsigned long MsgAddr;	//!< string address
	unsigned short MsgLen;	//!< string length
} RIF_MSG;

/*!
** \typedef MSG_TAB
** \brief The string set reference structure.
**/
#define MSGDESCLEN		8 
typedef struct __packed
{
	unsigned short Font_Group;			//!< font group index
	char DescMsg[NUM_FONT_GROUP][MSGDESCLEN];	//!< string set descriptor for each font group
	//RIF_MSG pStr[NUMMSG];		//!< string reference array
	RIF_MSG pStr[1];		//!< string reference dynamic array
} MSG_TAB;
#define MSG_TAB_SIZE	(sizeof(short)+NUM_FONT_GROUP*(unsigned long)MSGDESCLEN+NUMMSG*(unsigned long)sizeof(RIF_MSG))

/*!
** \var StringSet_RawData_Header
** \brief The string set reference structure pointer.
**/
extern MSG_TAB *pStringSet_RawData_Header;


#define DESC_BUL	"DOR"		// descrittore lingua bulgara per il font bulgaro (cirillico)

// giorni abbreviati della settimana in bulgaro
extern const char * const BULwday[7];


#endif	// _LANGUAGES_H_
