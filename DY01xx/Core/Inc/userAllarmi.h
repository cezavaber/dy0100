/*!
** \file userAllarmi.h
** \brief userAllarmi.c include file
**/

#ifndef __USER_ALLARMI_H__
#define __USER_ALLARMI_H__




/*!
	\typedef ALARM_RECORD
	\brief The Alarm item structure
*/
typedef struct __packed _alarm_record
{
	unsigned long timestamp;	//!< alarm timestamp (empty record if null)
	unsigned char TipoAllarme;	//!< alarm code (empty record if NO_ALARM)
} ALARM_RECORD;

/*!
	\var AlarmRecord
	\brief The Alarm item structure list
*/
#define MAX_ALARM_NUM_LIST		(10)	//!< numero max di allarmi in lista
extern ALARM_RECORD AlarmRecord[MAX_ALARM_NUM_LIST];

/*!
** \fn void AlarmInit( void )
** \brief Alarm list init
** \return None
**/
void AlarmInit( void );

/**
 *  \fn short AlarmPut( ALARM_RECORD *pAlmRec )
 *  \brief Add a new Alarm item structure in the list
 *  \param [in] pAlmRec The new Alarm item structure
 *  \return 1: OK, 0: FAILED
 */
short AlarmPut( ALARM_RECORD *pAlmRec );

/*!
** \fn ALARM_RECORD *AlarmGet( unsigned short index )
** \brief Return the Alarm item structure with the indicated index
** \param index The Alarm item structure index (0: the newest)
** \return The Alarm item structure, NULL if failed.
** \note The found Alarm is set as the current item of the list.
**/
ALARM_RECORD *AlarmGet( unsigned short index );

/*!
** \fn ALARM_RECORD *AlarmGetNext( void )
** \brief Return the next (older) Alarm item structure
** \return The Alarm item structure, NULL if failed.
** \note The current item of the list is updated.
**/
//ALARM_RECORD *AlarmGetNext( void );

/*!
** \fn short IsAlmRec( void )
** \brief Check if Alarm list not empty
** \return TRUE or FALSE
**/
short IsAlmRec( void );

/*!
	\fn unsigned char WinAlmRec( void )
	\brief Visualizza la lista dei Record Allarmi
	\return Il codice dello stato di transizione
*/
unsigned char WinAlmRec( void );

#endif	// __USER_ALLARMI_H__
