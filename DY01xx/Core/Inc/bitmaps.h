/*!
** \file bitmaps.h
** \author Alessandro Vagniluca
** \date 07/09/2015
** \brief BITMAPS.C header file
** 
** \version 1.0
**/

#ifndef _BITMAPS_H_
#define _BITMAPS_H_

#include "FT_Gpu.h"
#include "FT_Gpu_Hal.h"
#include "FT_Hal_Utils.h"
#include "FT_CoPro_Cmds.h"


// bitmap allocation in the data-flash
#define BITMAP_HEADER_ARRAY_ADDR		(0x00000000)	//!< bitmap header array start address
#define BITMAP_RAW_DATA_ADDR			(0x00002000)	//!< bitmaps raw data start address
#define BITMAP_MAX_ADDR					(0x0031FFFF)	//!< bitmaps raw data end address
#define BITMAP_FIRST_64K_SECT			0		//!< the first 64KB sector for bitmaps
#define BITMAP_NUM_64K_SECT			50		//!< 64KB sectors number for bitmaps

// bitmap allocation in the Graphic RAM
#define BITMAP_GRAM_BASE_ADDR			(RAM_G+0x10000)

/*!
** \typedef Bitmap_header_t
** \brief The bitmap property structure.
**/
typedef struct __packed Bitmap_header
{
	ft_uint8_t Format;
	ft_int16_t Width;
	ft_int16_t Height;
	ft_int16_t Stride;
	ft_int32_t Arrayoffset;
	ft_uint32_t Bitmap_RawData;
} Bitmap_header_t;

/*!
** \enum BITMAP_TAB_INDEX
** \brief The bitmaps' enumeration
**/
enum BITMAP_TAB_INDEX
{
	BITMAP_LOGO,
	BITMAP_DEG_C,
	BITMAP_DEG_F,
	BITMAP_QUIT,
	BITMAP_ESC,
	BITMAP_OK,
	BITMAP_RADIATOR,
	BITMAP_TAP,
	BITMAP_CHRONO,
	BITMAP_SLEEP,
	BITMAP_MODEM,
	BITMAP_OFF_BUTTON,
	BITMAP_ON_BUTTON,
	BITMAP_WARNING,
	BITMAP_ROOM_TEMP,
	BITMAP_WATER_TEMP,
	BITMAP_FLAMES,
	BITMAP_TSETP,
	BITMAP_FAN,
	BITMAP_FAN1,
	BITMAP_FAN2,
	BITMAP_MAN,
	BITMAP_AUTO,
	BITMAP_WATERTEMP,
	BITMAP_SETTINGS,
	BITMAP_INFO,
	BITMAP_CHECKED,
	BITMAP_UNCHECKED,
	BITMAP_PLAY,
	BITMAP_STOVE_TYPE,
	BITMAP_MUTE,
	BITMAP_SUMMER,
	BITMAP_WINTER,
	BITMAP_THERM,
	BITMAP_ALARM,
	BITMAP_CALENDAR,
	BITMAP_FOLDER,
	BITMAP_TRASH,
	BITMAP_REBOOT,
	BITMAP_CHARGE,
	BITMAP_BATTERY,
	BITMAP_CHARGE_H_4X,
	BITMAP_BATTERY_H_4X,
	BITMAP_LOGO_b0,
	BITMAP_LOGO_b1,
	BITMAP_LOGO_c0,
	BITMAP_LOGO_c1,
	
	NUM_BITMAP
};

/*!
** \var Bitmap_RawData_Header
** \brief The bitmap property structure.
**/
extern Bitmap_header_t Bitmap_RawData_Header;

/*!
** \typedef BM_HEADER
** \brief The bitmap header.
**/
typedef struct __packed
{
	Bitmap_header_t bmhdr;		//!< bitmap property structure
	unsigned long GramAddr;		//!< GRAM address
	unsigned char tag;			//!< bitmap tag
} BM_HEADER;

#define BITMAP_LOGO_MIN_WIDTH		(30)	//!< minimum width value for a valid logo bitmap
#define BITMAP_LOGO_MIN_HEIGHT	(30)	//!< minimum height value for a valid logo bitmap


#endif	// _BITMAPS_H_
