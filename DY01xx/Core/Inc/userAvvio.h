/*!
** \file userAvvio.h
** \author Alessandro Vagniluca
** \date 27/11/2015
** \brief USERAVVIO.C header file
** 
** \version 1.0
**/

#ifndef _USER_AVVIO_H_
#define _USER_AVVIO_H_



/*!
** \fn unsigned char userVisAvvio( void )
** \brief Starts the USER_STATE_AVVIO and USER_STATE_AVVIO_ALLARME
** 		User Interface states handling
** \return The next User Interface state 
**/
unsigned char userVisAvvio( void );

/*!
** \fn unsigned char userAvvio( void )
** \brief Handler function for the USER_STATE_AVVIO and USER_STATE_AVVIO_ALLARME
** 		User Interface states
** \return The next User Interface state 
**/
unsigned char userAvvio( void );

/*!
** \fn unsigned char userStandby( void )
** \brief Handler function for the USER_STATE_STANDBY User Interface states handling
** \return The next User Interface state
**/
unsigned char userStandby( void );

/*!
** \fn short WinSetPower( char *sTitle, unsigned short iPar )
** \brief SET POWER window handler
** \param sTitle The window's title string
** \param iPar Parameter index
**	\return 1: SET, 0: QUIT
**/
short WinSetPower( char *sTitle, unsigned short iPar );

/*!
** \fn short WinSetTemp( char *sTitle, unsigned short iPar )
** \brief SET TEMP window handler
** \param sTitle The window's title string
** \param iPar Parameter index
** \return 1: SET, 0: QUIT
**/
short WinSetTemp( char *sTitle, unsigned short iPar );

/*!
** \fn unsigned char userRestartOff( void )
** \brief User RESTART or SWITCH_OFF confirmation window handler
** \return The next User Interface state
**/
unsigned char userRestartOff( void );

/*!
** \fn void ReleaseAllBitmapAvvio( void )
** \brief Release all the bitmaps used in the USER_STATE_AVVIO User Interface state
** \return None
**/
void ReleaseAllBitmapAvvio( void );

/*!
** \fn short LoadAllBitmapAvvio( void )
** \brief Load all the bitmaps used in the USER_STATE_AVVIO User Interface state
** \return 1: OK, 0: FAILED
**/
short LoadAllBitmapAvvio( void );



#endif	// _USER_AVVIO_H_
