/*!
** \file user.h
** \author Alessandro Vagniluca
** \date 26/05/2015
** \brief USER.C header file
** 
** \version 1.0
**/

#ifndef _USER_H_
#define _USER_H_

#include "main.h"
#include "display.h"
#include "FT_Gpu.h"
#include "FT_Gpu_Hal.h"
#include "FT_Hal_Utils.h"
#include "FT_CoPro_Cmds.h"
#include "ctp.h"
#include "OT_Plus.h"
#include "nvram.h"
#include "rtc.h"
#include "timebase.h"
#include "res.h"
#include "crono.h"
#include "Led.h"


//#define menu_CRONO	// abilita la gestione crono a menu'
#define EN_WALLPAPER	// abilita la gestione dell'immagine di sfondo 480x272


// time defines
//
#define	USER_TIME_TICK					(100)		// USER timer tick = 100ms
#define	USER_TIMEOUT_SHOW_1			(10)		// Timeout su stati Show = 1s
#define	USER_TIMEOUT_SHOW_2			(20)		// Timeout su stati Show = 2s
#define	USER_TIMEOUT_SHOW_3			(30)		// Timeout su stati Show = 3s
#define	USER_TIMEOUT_SHOW_10			(100)		// Timeout su stati Show = 10s
//#define	USER_TIMEOUT_SHOW_20			(200)		// Timeout su stati Show = 20s
#define	USER_TIMEOUT_SHOW_60			(600)		// Timeout su stato impostazione = 60s
#define	USER_TIMEOUT_SHOW_20			USER_TIMEOUT_SHOW_60		// Timeout su stati Show = Timeout su stato impostazione

#define TIMEOUT_UNLINKED			600		// 60s timeout unlinked nello stato USER_STATE_INIT

// Timeout su stati Avvio con 100% backlight
#define USER_TIMEOUT_LIGHT_MID		(StatSec.BackLightTime*10*T1SEC)
// Timeout su stati Avvio con backlight (USER_TIMEOUT_LIGHT_MID + 5s, oppure infinito)
#define USER_TIMEOUT_LIGHT_OFF		(	StatSec.BackLightTime ? (USER_TIMEOUT_LIGHT_MID+5*T1SEC) : 0xFFFFFFFE )

#define LCD_BACKLIGHT_PWM_FREQ		(1000)	// 1000 Hz

#define LCD_BACKLIGHT_MIN			(11)	// a 8% talvolta balugina, a 7% non si vede piu' niente
#define LCD_BACKLIGHT_MID			(100)	//solo due livelli, max e min	(15)		// 15% (apprezzabile rispetto a 100%)
#define LCD_BACKLIGHT_MAX			(100)	// 100%

#define TIMEOUT_AVVIO_2SEC			(20)
#define TIMEOUT_AVVIO_3SEC			(30)
#define TIMEOUT_AVVIO_5SEC			(50)
#define TIMEOUT_AVVIO_8SEC			(80)
#define TIMEOUT_AVVIO_10SEC			(100)

#define BUZZER_ALARM_ON				(6*T1SEC)	// per 3 beep lunghi


/*! Gestione colori */
typedef struct __packed
{
	unsigned char red;
	unsigned char green;
	unsigned char blue;
	unsigned char addon;
} STRUCT_USER_COLOR;
extern STRUCT_USER_COLOR UserColor_tab[UCOL_MAX];


/*!
** \typedef userStateHandler
** \brief User Interface state function handler
**/
typedef unsigned char (*userStateHandler)(void);


/*!
** \enum stgu
** \brief The states for the User Interface task
**/
enum stgu
{
	USER_STATE_RESOURCES,
	USER_STATE_BATTERY,
	USER_STATE_INIT,
	USER_STATE_RESET,
	
	USER_STATE_AVVIO,
	USER_STATE_AVVIO_ALLARME,
	USER_STATE_STANDBY,
	USER_STATE_ALLARME,

#ifndef menu_CRONO
	USER_STATE_CRONO,
#endif
	
	USER_STATE_LOCAL_SETTINGS,
	USER_STATE_MENU,
	USER_STATE_INFO,
	USER_STATE_PARAM_CODE,
	USER_STATE_PARAM_VIS,

	USER_STATE_COLLAUDO,
	
	USER_STATE_LOW_POWER,
	USER_STATE_PAUSE,
	USER_STATE_RESTART_OFF,

	USER_MAX_STATES
};
extern unsigned char userCurrentState;

extern unsigned char userSubState;
extern unsigned long userCounterTime;
extern userStateHandler userNextPointer;

extern BYTE_UNION userParValue;

extern BYTE iCronoPrg;

extern BYTE tmpRFid;


// the touch queue
#define TOUCH_QUEUE_SIZE		16
extern osMessageQId TouchQueueHandle;

// user interface flags
typedef struct 
{
	ft_uint8_t Key_Detect	:1;
	ft_uint8_t Caps			:1;
	ft_uint8_t Numeric		:1;
	ft_uint8_t popup			:1;
	ft_uint8_t reserved		:4;	
} USER_FLAGS;
extern USER_FLAGS Flag;

#define TOUCH_TAG_INIT		(0xFFFF)
#define TAG_QUIT		(TAG_HSCROLL_BASE-2)
#define TAG_OK			(TAG_HSCROLL_BASE-1)
#define VALID_TOUCHED_KEY		(5)	//!< counter value for a valid touched key


#define MAX_NUM_EXIT4ALARM		3	//!< max number for Standby Pause exit for alarms
extern unsigned char cntExit4Alarm;	//!< Standby Pause exit counter for alarms


// waiting the setting result
extern unsigned char StDownload;
#define NSTAT_DOWNLOAD		4
extern const char charStDownload[NSTAT_DOWNLOAD]; 
typedef struct
{
	unsigned short iPar;	//!< parameter index
	char sTitle[DISPLAY_MAX_COL+1];	//!< pop-up window's title string
	char *sDesc;	//!< parameter description string
	unsigned short Time;	//!< setting result waiting timer (in USER_TIME_TICK)
	OT_RESULT result;	//!< current setting result state
} WAIT_SET_RESULT;
extern WAIT_SET_RESULT WaitSetResult;
/*!
** \fn void ShowWaitSetResultPopup( void )
** \brief Show the waiting setting result pop-up window
** \return None
** \note This function is based on the 'WaitSetResult' structure and has to be
** 		used only within a co-processor engine command list.
**/
void ShowWaitSetResultPopup( void );



ft_void_t Ft_App_WrCoCmd_Buffer(Ft_Gpu_Hal_Context_t *host,ft_uint32_t cmd);
ft_void_t Ft_App_WrDlCmd_Buffer(Ft_Gpu_Hal_Context_t *host,ft_uint32_t cmd);
ft_void_t Ft_App_WrCoStr_Buffer(Ft_Gpu_Hal_Context_t *host,const ft_char8_t *s);
ft_void_t Ft_App_Flush_DL_Buffer(Ft_Gpu_Hal_Context_t *host);
ft_void_t Ft_App_Flush_Co_Buffer(Ft_Gpu_Hal_Context_t *host);


ft_uint32_t Get_UserColorHex(unsigned char iCol);

/*!
** \fn int User_Init( void )
** \brief User interface initialization
** \return 1: OK, 0: FAILED
** \note SPI peripheral is supposed already properly initialized
**/
int User_Init( void );

/*!
** \fn void User_Task( void const * argument )
** \brief User interface task
**/
void User_Task( void const * argument );

/*!
** \fn short TagRegister( unsigned char tag, unsigned short xs, unsigned short ys,
** 						unsigned short width, unsigned short height )
** \brief Register a touch tag
** \param tag Tag value
** \param xs X coordinate of the top left corner of the object
** \param xs Y coordinate of the top left corner of the object
** \param width Object width
** \param height Object height
** \return 1: OK, 0: FAILED
**/
short TagRegister( unsigned char tag, unsigned short xs, unsigned short ys,
					unsigned short width, unsigned short height );

// flush the touch queue
void FlushTouchQueue( void );

/*!
** \fn void SetBacklight( unsigned short duty )
** \brief Set the backlight level
** \param duty The new backlight level (%)
** \return None
**/
void SetBacklight( unsigned short duty );

/*!
** \fn ft_uint8_t Read_Keypad( TAG_STRUCT *pt )
** \brief Read a key from a touch keyboard
** \param pt The touch tag structure for the key
** \return The tag of the touched key, 0: key untouched
**/
ft_uint8_t Read_Keypad( TAG_STRUCT *pt );

/*!
** \fn ft_uint8_t Ft_Gpu_Font_WH( ft_uint8_t Char, ft_uint8_t font )
** \brief Get the width of the font character
** \param Char The font character index
** \param font The font to be used with the character
** \return The width of the font character
** \note ROM fonts begin from index 32 (space) up to index 127.
** 		Custom fonts begin from index 1 (space) up to index 127.
**/
ft_uint8_t Ft_Gpu_Font_WH( ft_uint8_t Char, ft_uint8_t font );

/*!
** \fn unsigned short GetStringWidth( char *s, ft_uint8_t font )
** \brief Get the width of a string with the used font
** \param s The string
** \param font The font used to view the string
** \return The width of the string
**/
unsigned short GetStringWidth( char *s, ft_uint8_t font );

/*!
** \fn short GetFont4StringWidth( char *s, ft_uint8_t startFont, unsigned short maxWidth )
** \brief Find the right proportional font for a string width
** \param s The string
** \param startFont The start proportional font index
** \param maxWidth The max available width for the string
** \return The right proportional font index, -1 if not found
** \note startFont >= 26 => ROM font range [26, startFont]
** 		startFont >= 20 => ROM font range [20, startFont]
** 		startFont >= 0 => RAM font range [0, startFont]
**/
short GetFont4StringWidth( char *s, ft_uint8_t startFont, unsigned short maxWidth );

/*!
** \fn char Char2Font( char c, ft_uint8_t font )
** \brief Convert a character for the used font
** \param c The character
** \param font The used font
** \return The converted character (for custom RAM font only)
** \note ROM fonts begin from index 32 (space) up to index 127.
** 		Custom fonts begin from index 1 (space) up to index 127.
**/
char Char2Font( char c, ft_uint8_t font );

/*!
** \fn char *String2Font( char *s, ft_uint8_t font )
** \brief Convert a string for the used font
** \param s The string
** \param font The used font
** \return The converted string (for custom RAM font only)
** \note ROM fonts begin from index 32 (space) up to index 127.
** 		Custom fonts begin from index 1 (space) up to index 127.
**/
char *String2Font( char *s, ft_uint8_t font );

/*!
** \fn unsigned char Check4StatusChange( void )
** \brief Check for a mandatory status change event
** \return The next User Interface state, USER_MAX_STATES if no status change is required
**/
unsigned char Check4StatusChange( void );

/*!
** \fn void userSetTempUM( void )
** \brief Set the measure unit for temperatures
** \return None
**/
void userSetTempUM( void );

/*!
	\fn void userGetServicePassword( unsigned char *Lettera, unsigned char *Numero )
	\brief Ritorna la password di accesso assistenza
	\param Lettera password parte letterale
	\param Numero password parte numerica
	\return None
*/
void userGetServicePassword( unsigned char *Lettera, unsigned char *Numero );



/*!
	\fn unsigned char ExecTipoStufa( void )
	\brief Esegue il set parametro P_TIPOSTUFA
	\return Il codice dello stato di transizione
*/
unsigned char ExecTipoStufa( void );

/*!
	\fn unsigned char ExecTipoDef( void )
   \brief Esegue il Ripristino Tipo Stufa al Default
	\return Il codice dello stato di transizione
*/
unsigned char ExecTipoDef( void );

/*!
	\fn unsigned char ExecUpdateLang( void )
   \brief Esegue al set nuova lingua
	\return Il codice dello stato di transizione
*/
unsigned char ExecUpdateLang( void );

/*! \fn unsigned char ExecSetTempUM( void )
    \brief Esegue al set nuova lingua
	 \return Il codice dello stato di transizione
*/
unsigned char ExecSetTempUM( void );

/*!
	\fn unsigned char ExecBypass( void )
   \brief Esegue la procedura di Bypass
	\return Il codice dello stato di transizione
*/
unsigned char ExecBypass( void );

/*!
	\fn unsigned char ExecBypassTra( void )
   \brief Esegue la procedura di Bypass
	\return Il codice dello stato di transizione
*/
unsigned char ExecBypassTra( void );

/*!
	\fn unsigned char ExecTaraTC( void )
   \brief Esegue la procedura di taratura termocoppia
	\return Il codice dello stato di transizione
*/
unsigned char ExecTaraTC( void );

/*! \fn unsigned char ExecSetRFid( void )
    \brief Esegue il set param P_RFPANEL
	 \return Il codice dello stato di transizione
*/
unsigned char ExecSetRFid( void );

/*! \fn unsigned char ExecLocalSetRFid( void )
    \brief Esegue il set param P_RFPANEL nel menu' Local Settings
	 \return Il codice dello stato di transizione
*/
unsigned char ExecLocalSetRFid( void );



/*!	\fn void userVisShutDown( char *s, unsigned long visTime )
    \brief Visualizzazione messaggio di shut-down
    \param s Il messaggio da visualizzare (max 21 caratteri)
    \param visTime Il tempo (ms) di visualizzazione del messaggio
	\return None
*/
void userVisShutDown( char *s, unsigned long visTime );

/*! \fn unsigned char userVisLowPower( void )
    \brief Visualizzazione nello stato USER_STATE_LOW_POWER
	 \return Il codice dello stato di transizione
*/
unsigned char userVisLowPower( void );

/*!
** \fn unsigned char userVisPause( void )
** \brief Visualizzazione nello stato USER_STATE_PAUSE
** \return Il codice dello stato di transizione
**/
unsigned char userVisPause( void );

/*!
** \fn unsigned char userExitPause( void )
** \brief Uscita dallo stato USER_STATE_PAUSE
** \return Il codice dello stato di transizione
**/
unsigned char userExitPause( void );

/*!
** \fn void userStartOTLongPause( void )
** \brief Comanda la pausa lunga al processo OT/+
** \return None
**/
void userStartOTLongPause( void );

/*!
**  \fn unsigned short GetVisPercVBatt( void )
**  \brief Rileva la percentuale di carica batteria da visualizzare
**  \return Il valore in 0.1% della percentuale di carica batteria da visualizzare
**/
unsigned short GetVisPercVBatt( void );



/*!
** \fn short userIsXXX( void )
** \brief Check a PH2 parameter
** \return TRUE or FALSE
**/
short IsToView_PreCarPellet( void );
short IsToView_Pulizia( void );
short IsToView_AttivaPompa( void );
short IsServiceReq( void );
short IsSensAriaFail( void );
short IsSensH2OFail( void );
short IsPressostH2OFail( void );
short IsPressH2OAlarm( void );
short IsSensPortFail( void );
short IsFinePellet( void );
short IsPortaAperta( void );
short userIsCrono( void );
short userIsSleep( void );
short userIsIdro( void );
short IsCtrlPortata( void );
short IsCtrlLambda( void );
short IsFan1Info( void );
short IsFan2Info( void );
short IsPressIdro( void );
short IsWiFi( void );
short IsWiFiNet( void );
short IsCtrlAria( void );
short IsSensPellet( void );
short IsTermocoppia( void );
short IsTempoCoclea( void );
short IsGiriCoclea( void );
short IsCoclea2( void );
short IsTOffCoclea( void );
short IsPeriodoCoclea( void );
short IsTipoFrenCoclea( void );
short IsEsp2( void );
short IsEsp2Giri( void );
short IsTOffCoclea2( void );
short IsPeriodoCoclea2( void );
short IsTipoFrenCoclea2( void );
short IsVisSanit( void );
short IsRipristino( void );
short IsTMaxPreAlm( void );
short IsTermostato( void );
short IsDTempEco( void );
short IsTPreA06( void );
short IsPot1( void );
short IsPot2( void );
short IsPot3( void );
short IsPot4( void );
short IsPot5( void );
short IsPot6( void );
short IsConsumo( void );
short IsFotoresistenza( void );
short IsCollaudoMan( void );
short IsCollaudoAuto( void );
short IsFan1Test( void );
short IsFan2Test( void );
short IsScuotitore( void );
short IsCocleaItem( void );
short IsPreAccHotItem( void );
short IsAccBItem( void );
short IsWiFiIPvalid( void );


#ifdef SCREENSHOT
// Screenshot enumeration
enum
{
	SSHOT_Main_ON,
	SSHOT_Main_MAN,
	SSHOT_Main_AUTO,
	SSHOT_Main_THERM,
	SSHOT_Main_CMD2,
	SSHOT_SetFire,
	SSHOT_SetTemp,
	SSHOT_SetFan,
	SSHOT_SetIdro,
	SSHOT_Alarm,
	SSHOT_Standby,
	SSHOT_ESM1,
	SSHOT_ESM2,
	SSHOT_Crono,
	SSHOT_CronoProg,
	SSHOT_SetTime,
	SSHOT_SetDate,
	SSHOT_Password,
	SSHOT_SetParam,
	SSHOT_SetDisplay,
	SSHOT_Menu_Info,
	SSHOT_Menu_Main,
	SSHOT_Menu_Settings,
	SSHOT_Menu_Tecnico,
	SSHOT_ParCode,
	SSHOT_Restart_Off,
	NUM_SSHOT
};

/*!
** \fn void DoScreenshot( unsigned short iSShot )
** \brief Execute a screenshot
** \param iSShot Screenshot index
** \return None
**/
void DoScreenshot( unsigned short iSShot );

/*!
** \fn short IsGenScreeshot( void )
** \brief Check the general condition for a screenshot
** \return TRUE or FALSE
**/
short IsGenScreeshot( void );
#endif	// SCREENSHOT




#endif	// _USER_H_
