/*!
**  \file DataFlash.h
**  \author Alessandro Vagniluca
**  \date 06/10/2015
**  \brief Header file for DATAFLASH.C source module
**  
**  \version 1.0
**/

#ifndef _DATAFLASH_H
#define _DATAFLASH_H

//#define SST25VF032		//!< enable the driver for SST25VF032B memory
#define SST26VF032		//!< enable the driver for SST26VF032B memory

/*!
** \enum dfCmd
** \brief Dataflash banks enumeration
**/
enum eebank
{
	EE_BANK_0,
	//EE_BANK_1,
	NEEBANK
};

/*!
** \enum dfCmd
** \brief Dataflash command codes
**/
enum dfCmd
{
	/* Command definitions (please see datasheet for more details) */

	/* IDENTIFICATION Operations */
	SPI_FLASH_INS_RDID        			= 0x9F,	//!< read Identification

	/* READ operations */
	SPI_FLASH_INS_READ        			= 0x03,	//!< read data bytes
	SPI_FLASH_INS_FAST_READ   			= 0x0B,	//!< read data bytes at higher speed

	/* WRITE operations */
	SPI_FLASH_INS_WREN        			= 0x06,	//!< write enable
	SPI_FLASH_INS_WRDI        			= 0x04,	//!< write disable

	/* REGISTER operations */
	SPI_FLASH_INS_RDSR      			= 0x05,	//!< read status register
#if defined(SST25VF032)
	SPI_FLASH_INS_EWSR      			= 0x50,	//!< enable write status register
#elif defined(SST26VF032)	
	SPI_FLASH_INS_RBPR      			= 0x72,	//!< read Block Protection Register
	SPI_FLASH_INS_WBPR      			= 0x42,	//!< write Block Protection Register
	SPI_FLASH_INS_ULBPR      			= 0x98,	//!< Global Block Protection Unlock
#endif
	SPI_FLASH_INS_WRSR      			= 0x01,	//!< write status register

	/* PROGRAM operations */
#if defined(SST25VF032)
	SPI_FLASH_INS_BP          			= 0x02,	//!< BYTE PROGRAM
	SPI_FLASH_INS_AAI						= 0xAD,	//!< Auto Address Increment
#elif defined(SST26VF032)	
	SPI_FLASH_INS_PP          			= 0x02,	//!< PAGE PROGRAM
#endif

	/* ERASE operations */
	SPI_FLASH_INS_SSE         			= 0x20,	//!< sub sector erase
	SPI_FLASH_INS_SE          			= 0xD8,	//!< sector erase
	SPI_FLASH_INS_DE		  				= 0xC7,	//!< die erase
	
#if defined(SST26VF032)
	/* RESET operations */
	SPI_FLASH_INS_RSTEN        			= 0x66,	//!< Reset-Enable
	SPI_FLASH_INS_RST        			= 0x99,	//!< Reset
#endif
	
};

#define DF_SUBSECTOR_SIZE			(4096L)			//!< 4KB sub-sector size
#define DF_SECTOR_SIZE				(65536L)			//!< 64KB sector size
#define DF_CHIP_SIZE					(0x400000)		//!< 4MB memory chip size

#define NUM_SUBSECT_SECTOR			(DF_SECTOR_SIZE/DF_SUBSECTOR_SIZE)	//!< number of sub-sectors in one sector
#define NUM_SUBSECT_CHIP			(DF_CHIP_SIZE/DF_SUBSECTOR_SIZE)	//!< number of sub-sectors in the chip

#define DF_PAGE_SIZE				256	//!< page size

/*!
** \typedef STATUS_REG
** \brief The dataflash status register structure
**/
#if defined(SST25VF032)
typedef union
{
	unsigned char BYTE;
	struct
	{
		unsigned char Busy		:1;
		unsigned char Wel			:1;
		unsigned char Bp0			:1;
		unsigned char Bp1			:1;
		unsigned char Bp2			:1;
		unsigned char Bp3			:1;
		unsigned char Aai			:1;
		unsigned char Bpl			:1;
	} BIT;
} STATUS_REG;
#elif defined(SST26VF032)	
typedef union
{
	unsigned char BYTE;
	struct
	{
		unsigned char Busy		:1;
		unsigned char Wel			:1;
		unsigned char Wse			:1;
		unsigned char Wsp			:1;
		unsigned char Wpld		:1;
		unsigned char Sec			:1;
		unsigned char Res			:1;
		unsigned char Busy1		:1;
	} BIT;
} STATUS_REG;
#endif


/*!
**  \fn short df_Init( unsigned char bank )
**  \brief Init data-flash driver
**  \param bank The data-flash bank
**  \return 1: OK, 0: FAILED
**/
short df_Init( unsigned char bank );

/*!
**  \fn short df_Deinit( unsigned char bank )
**  \brief Deinit data-flash driver
**  \param bank The data-flash bank
**  \return 1: OK, 0: FAILED
**/
short df_Deinit( unsigned char bank );

/*!
**  \fn short df_EraseSubSector( unsigned char bank, unsigned short iSubSect )
**  \brief Erase the indicated data-flash subsector
**  \param bank The data-flash bank
**  \param iSubSect The subsector index
**  \return 1: OK, 0: FAILED
**/
short df_EraseSubSector( unsigned char bank, unsigned short iSubSect );

/*!
**  \fn short df_EraseSector( unsigned char bank, unsigned short iSect )
**  \brief Erase the indicated data-flash sector
**  \param bank The data-flash bank
**  \param iSect The sector index
**  \return 1: OK, 0: FAILED
**/
short df_EraseSector( unsigned char bank, unsigned short iSect );

/*!
**  \fn short df_EraseChip( unsigned char bank )
**  \brief Erase the all data-flash
**  \param bank data-flash bank
**  \return 1: OK, 0: FAILED
**/
short df_EraseChip( unsigned char bank );

/*!
**  \fn short df_Read( unsigned char bank, unsigned long addr,
**  							unsigned char *buf, unsigned short num )
**  \brief Read a buffer from the data-flash
**  \param bank The data-flash bank
**  \param addr The start address of the buffer
**  \param buf The buffer pointer
**  \param num The number of bytes to read
**  \return 1: OK, 0: FAILED
**/
short df_Read( unsigned char bank, unsigned long addr, 
					unsigned char *buf, unsigned short num );

/*!
** \enum wrVerifyCmd
** \brief Write verify command
**/
enum wrVerifyCmd
{
	WR_VER_NONE = 0,	//!< no write verify
	WR_VER_YES = 1,	//!< write verify
	NUM_WR_VER
};

/*!
**  \fn short df_Write( unsigned char bank, unsigned long addr, 
**  							unsigned char *buf, unsigned short num,
**  							unsigned char verify )
**  \brief Write a buffer from the data-flash
**  \param bank The data-flash bank
**  \param addr The start address of the buffer
**  \param buf The buffer pointer
**  \param num The number of bytes to write
**  \param verify The write verify command
**  \return 1: OK, 0: FAILED
**/
short df_Write( unsigned char bank, unsigned long addr, 
						unsigned char *buf, unsigned short num,
						unsigned char verify );



#endif	// _DATAFLASH_H
