/*!
** \file stringhe_ITA.h
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief STRINGHE_ITA.C header file
** 
** \version 1.0
**/

#ifndef _STRINGHE_ITA_H_
#define _STRINGHE_ITA_H_

#include "languages.h"

//!< font group for the ITA strings set
#define FONT_GROUP_ITA		(FONT_GROUP_EUROPE)

//!< string set descriptor for each font group
extern const char DescMsg_ITA[NUM_FONT_GROUP][MSGDESCLEN];

//!< ITA strings set
extern const PCSTR pStr_ITA[NUMMSG];



#endif	// _STRINGHE_ITA_H_

