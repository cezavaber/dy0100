/*!
	\file DatiStufa.h
	\brief Include file del modulo DatiStufa.c
*/

#ifndef _DATISTUFA_H
#define _DATISTUFA_H

#include "typedefine.h"


/*! \union panenab3
    \brief Struttura unione per abilitazioni 3
*/
union panenab3
{
	unsigned char BYTE;
	struct
	{
		unsigned char ModoLegna				:1;
		unsigned char EnFilPilot			:1;
		unsigned char reserved				:6;
	} BIT;
};
/*! \typedef PANENAB3
    \brief Typedef per panenab3
*/
typedef union panenab3 PANENAB3;
/*! \var PanEnab3
    \brief Bitmap dei flag PanEnab 3
*/
extern PANENAB3 PanEnab3;
#define PAN_ENAB_3			PanEnab3.BYTE
//#define EN_LEGNA				PanEnab3.BIT.ModoLegna			// =1 abilita modo legna
#define EN_FILPILOT			PanEnab4.BIT.EnFilPilot			// =1 abilita filpilot

/*! \union servnum
    \brief Struttura unione per Service Number
*/
union servnum
{
	unsigned char BYTE;
	struct
	{
		unsigned char CifraLow			:4;	//!< cifra 'L' del numero "HL" a due cifre
		unsigned char CifraHigh			:4;	//!< cifra 'H' del numero "HL" a due cifre
			//!< Le cifre 0,1,..,9 del numero sono codificate come 0x1,0x2,..,0xA
			//!< Il codice 0x0 indica lo spazio di fine numero (cifra vuota)
	} BIT;
};
/*! \typedef SERVNUM
    \brief Typedef per servnum
*/
typedef union servnum SERVNUM;
#define SERVNUM_SIZE		8
extern SERVNUM ServNumber[SERVNUM_SIZE];		//!< 16 cifre numero telefonico assistenza
	//!< le cifre piu` a sx del numero corrispondono ai membri del vettore con indice minore


//#define TEMP_AMB_PAN_0_1_DEG		// abilita l'espressione in step di 0.1�C per la temperatura ambiente misurata sul pannello
#define TEMP_AMB_PAN_0_5_DEG		// abilita l'espressione in step di 0.5�C per la temperatura ambiente misurata sul pannello
extern short iTempAmbPAN;
#define TEMP_AMB_PAN		iTempAmbPAN
extern unsigned short TBlackout;												// 
#define TBLACKOUT				TBlackout

extern TBitWord CoilBMap0;
#define SETTINGS							CoilBMap0.WORD
#define SETTINGS_LOW						CoilBMap0.BYTE.L
#define SETTINGS_HIGH					CoilBMap0.BYTE.H
#define LOGIC_ONOFF						CoilBMap0.BIT.D0
#define EN_LEGNA							CoilBMap0.BIT.D1

typedef union
{ 
	unsigned char ByVector;
	struct
	{
		// Flag di comando per il pannello all-black da scheda.
		unsigned unShowModem			:1;		// Comando per spengere il buzzer sulla scheda durante l allarme.
		unsigned unShowA15				:1;		// Flag di pressione tasto radiocomando.
		unsigned unShowSat				:1;   // Visualizzazione ore di servizio
		unsigned unShowShutDHiH2O :1;   // Visualizzazione allarme sovratemperatura H2O
		unsigned Fan1				:1;	//!< 1=collaudo scambiatore 1
		unsigned Fan2				:1;	//!< 1=collaudo scambiatore 2
		unsigned Candeletta		:1;	//!< 1=collaudo candeletta
		unsigned TestItemStatus	:1;	// 1=test item attivo
	} BIT_DATA;
} _STRUCT_CMD_MB_;
extern _STRUCT_CMD_MB_ gStrCmdMB;
#define ByCMD_MB					gStrCmdMB.ByVector
//#define	unCMD_SHOW_MODEM		gStrCmdMB.BIT_DATA.unShowModem
//#define	unCMD_SHOW_A15			gStrCmdMB.BIT_DATA.unShowA15
#define unCMD_SHOW_SAT			gStrCmdMB.BIT_DATA.unShowSat
//#define unCMD_SHOW_SDH2O		gStrCmdMB.BIT_DATA.unShowShutDHiH2O
//#define AVVIA_FAN1				gStrCmdMB.BIT_DATA.Fan1
//#define AVVIA_FAN2				gStrCmdMB.BIT_DATA.Fan2
//#define AVVIA_CANDELETTA		gStrCmdMB.BIT_DATA.Candeletta
#define TEST_ITEM_STAT			gStrCmdMB.BIT_DATA.TestItemStatus



extern unsigned char gunTemp;
#define TEMP_PAN	gunTemp

typedef union
{
	unsigned char vbbDataSPI_2;
	struct
	{
		unsigned	unMaxPower		 :4;        // Livello massimo impostato da scheda
		unsigned unPowerLvl     :4;		    // Livello settato dall'utente
	} BIT_DATA_SPI_2;
}	STRUCT_SPI_II;
extern STRUCT_SPI_II StructSPI_2;
#define POWER_BYTE			StructSPI_2.vbbDataSPI_2
#define MAX_POWER				StructSPI_2.BIT_DATA_SPI_2.unMaxPower
#define POWER_LEVEL			StructSPI_2.BIT_DATA_SPI_2.unPowerLvl

extern unsigned char NumFan;
#define F_MULTIFAN	NumFan

typedef union
{
	unsigned char BYTE;
	struct
	{
		unsigned	gunFan				:3;				// Livello del primo fan. gUnVector[0]
		unsigned UnSecondFan			:3;		    	// Livello del secondo fan.
		unsigned	unFree0				:2;				// 
	} BIT_DATA_SPI;
} DATASPI0;
extern DATASPI0 DataSPI0;
#define MODE_BYTE					DataSPI0.BYTE
#define FAN_PAN					DataSPI0.BIT_DATA_SPI.gunFan
#define FAN_PAN_2					DataSPI0.BIT_DATA_SPI.UnSecondFan

typedef union
{
	unsigned char BYTE;
	struct
	{
		unsigned UnTerzoFan			:3;		    	// Livello del terzo fan.
		unsigned	reserved				:5;					// 
	} BIT;
} DATA_FAN3;
extern DATA_FAN3 DataFan3;
#define FAN3_BYTE					DataFan3.BYTE
#define FAN_PAN_3					DataFan3.BIT.UnTerzoFan

extern unsigned char gBySchedaCod;
extern unsigned char gBySchedaVers;
extern unsigned char gBySchedaRev;
extern unsigned char gBySchedaBuild;
extern unsigned char gBySicurezzaCod;
extern unsigned char gBySicurezzaVers;
extern unsigned char gBySicurezzaRev;
extern unsigned char gBySicurezzaBuild;

#define MINSETTEMP			0
#define DEFAULTSETTEMP		18
#define MAXSETTEMP			50

#define FANOFF		0
#define FANMINLEV	0
#define FANMAXLEV	5
#define FANAUTO	6
#define FANTURBO	7

enum powlev
{
	POT1LEV = 1,
	POT2LEV,
	POT3LEV,
	POT4LEV,
	POT5LEV,
	POT6LEV,
	POT7LEV,
	POT8LEV,
	POT9LEV
};
#define POTMINLEV		POT1LEV
#define POTDEFAULT	POT3LEV
#define POTMAXLEV		POT9LEV

#define H2O_TMIN		30
#define H2O_DEFAULT	60
#define H2O_TMAX		100

#define H2OSAN_TMIN		30
#define H2OSAN_TMAX		65

enum modfz
{
	MODO_OFF = 0,
	MODO_MANU,
	MODO_AUTO,
	MODO_ECO,
	NUMMODI
};

// Definizione numero fan
enum
{
	ZERO_FAN = 0,
	SINGLE_FAN = 1,
	DOUBLE_FAN = 2,
	TRE_FAN = 3,
};

//Questa � la define della password CEZA
#define	PASSWORD_xx		90		// Corrisponde alla lettera Z.
#define PASSWORD_00		90

#define PASS_LETTER_DEFAULT 65 // Corrisponde alla lettera A.
#define PASS_CODE_DEFAULT 0


#define	PASS1_MIN			65		// Corrisponde alla lettera A.
#define	PASS1_MAX			90		// Corrisponde alla lettera Z.
#define	PASS2_MIN			 0
#define	PASS2_MAX			99

enum StStufa
{
	STUFASTAT_IN_RESET,
	STUFASTAT_SPENTA,
	STUFASTAT_IN_ACCENSIONE,
	STUFASTAT_ACCESA,
	STUFASTAT_IN_SPEGNIMENTO,
	STUFASTAT_ALLARME,
	STUFASTAT_IN_ACCENSIONE_RETE,
	STUFASTAT_IN_COLLAUDO,
	//STUFASTAT_IN_PULIZIA_RETE,
	STUFASTAT_NON_DEFINITA,
	NUMSTUFASTAT
};
extern unsigned char StatoStufa;
#define STATO_STUFA			StatoStufa
extern unsigned char StatoLogico;
extern unsigned char StatoLogicoPassato;
// Definizioni stati logiche CEZA.
enum
{
	// Stati comuni a tutti i clienti: devono essere elencati in quest'ordine.
	STAT_RESET,							// 0
	STAT_VIS_OFF,						// 1
	STAT_OFF,								// 2
	STAT_VIS_ON,						// 3
	STAT_ON,								// 4
	STAT_PULIZIA,						// 5
	STAT_VIS_ERR,						// 6
	STAT_ERRORE,						// 7
	STAT_TESTER_AUTO,					// 8
	STAT_TESTER_MANU,	// 9
	STAT_TESTER_FAST,			// 10
	STAT_TESTER_FREE,		// 11
	// Stati particolari.
	STAT_PREACC_CALDO,			// 12
	STAT_PREACC_FREDDO,			// 13
	STAT_WARM_UP_FREDDO,		// 14
	STAT_WARM_UP_INCR,			// 15
	STAT_WARM_UP_CALDO,			// 16
	STAT_FIRE_ON,						// 17
	STAT_SPE,								// 18
	STAT_SPE_DAWARMUP,			// 19
	STAT_SPE_CONINCR,				// 20
	STAT_COOLDOWN,					// 21
	STAT_SPE_ERR,						// 22
	STAT_SPE_A09,						// 23
	STAT_COOLDOWN_ERR,			// 24
	STAT_BLACKOUT,					// 25
	STAT_SPE_DAWARMUP_ERR,	// 26
	STAT_SPE_CONINCR_ERR,		// 27
	STAT_PRECARICA,					// 28
	STAT_WAITDOOR,					// 29
	STAT_MAX
};

#define NUM_ATTUA_STAT		26	// numero degli stati di attuazione
extern unsigned char StatoAttuazioni;

enum
{
	NO_ALARM = 0,
	A01,						// Mancata Accensione.
	A02,						// Spegnimento Anomalo del Fuoco.
	A03,						// Temperatura Serbatoio Troppo Elevata.
	A04,						// Temperatura Fumi Eccessiva.
	A05,						// Ostruzione Canna Fumaria.
	A06,						// Ostruzione Aspirazione Aria.
	A07,						// Apertura Porta.
	A08,						// Ventilatore Fumi Guasto.
	A09,						// Guasto Sonda Fumi.
	A10,						// Guasto Candeletta.
	A11,						// Guasto Coclea.
	A12,						// Assenza Comunicazione Telecomando.
	A13,						// Guasto Scheda Elettronica.
	A14,						// Guasto Sensore di Portata Aria.
	A15,						// Guasto Sensore di Livello Pellet.
	A16,						// Pressione Acqua oltre i Limiti.
	A17,						// Temperatura Acqua Troppo Elevata.
	A18,						// Temperatura Serbatoio Acqua Troppo Elevata.
	NALARM
};
extern unsigned char TipoAllarme;
#define TIPO_ALLARME			TipoAllarme

extern short TempAmbMB;
#define TEMP_AMB_MB			TempAmbMB

extern short H2O_Temp;
extern unsigned char H2O_TSet;
extern unsigned char H2O_Flow;
extern unsigned char TSetOnPompa;
#define H2O_TEMP			H2O_Temp
#define H2O_TSET			H2O_TSet
#define H2O_FLOW			H2O_Flow
#define SETON_POMPA  	TSetOnPompa
extern unsigned char H2O_TSetSan;
#define H2O_TSETSAN		H2O_TSetSan

// indici tipo stufa
enum
{
	NO_TYPE = 0,
	TYPE_1,
	TYPE_2,
	TYPE_3,
	TYPE_4,
	TYPE_5,
	TYPE_6,
	TYPE_7,
	TYPE_8,
	NTYPE
};
extern unsigned char TipoStufa;
#define TIPO_STUFA		TipoStufa

// Definizione stati pannello all-black.
enum
{
	/* Stato di reset: il pannello riceve tutti dati inviati dalla scheda senza inviarne alcuno;
			visualizza schermate particolari. */
	PAN_RESET,
	/* Stato di trasmissione del pannello: il pannello invia dati di funzionamento alla scheda;
			riceve da essa i dati di configurazione e i parametri da visualizzare;
			visualizzazioni particolari sono da attribuire a flag di funzionamento e non allo stato. */
	PAN_ONLINE,
	/* Stato di reset senza tipo stufa: stato analogo al reset ma non riceve i dati di configurazione;
			visualizza la schermata di richiesta tipo stufa. */
	PAN_NO_TIPO,
	/* Stato di collaudo pannello */
	PAN_COLLAUDO,
	NUM_STATI_PAN
};
extern unsigned char StatoAllBlack;
#define STATO_ALL_BLACK			StatoAllBlack

extern short RicPellet;	// ricetta pellet in potenza
extern short RicEsp;	// ricetta espulsore in potenza

extern unsigned char GradienteECO;
#define GRADIENTE_ECO 				GradienteECO
extern unsigned short TSwitchOnECO;
#define TSWITCHON_ECO 				TSwitchOnECO
extern unsigned short TShutDownECO;
#define TSHUTDOWN_ECO 				TShutDownECO
extern unsigned short TempFumi;
#define TEMP_FUMI				TempFumi
extern unsigned short VelocitaEspulsore;
#define ESP_FUMI				VelocitaEspulsore
extern unsigned short PortataAria;
#define PORTATA					PortataAria
extern unsigned long OreFunz;
extern unsigned short OreSATLimite;
extern unsigned short OreSAT;
extern short RicPelletAcc;	// ricetta pellet in accensione
extern short RicEspAcc;	// ricetta espulsore in accensione
extern unsigned char pByPassLettera;
extern unsigned char pByPassCodice;

extern unsigned char NumLanguage;
extern unsigned char iLanguage;
extern unsigned char LinguaDef;

extern unsigned short TPreAcc1;
extern unsigned short TPreAcc2;
extern unsigned short TPreAccCaldo;
extern unsigned short TWarmUp;
extern unsigned short TFireOn;
extern unsigned short TempPreAlarmFumi;
extern unsigned short TempAlarmFumi;
extern unsigned short TempOnFumi;
extern unsigned short TempOffFumi;
extern unsigned short TempSogliaFumi;
extern unsigned char GradienteAlmFumi;
extern unsigned short TempOnScamb;
extern unsigned short TempSogliaScamb;
extern unsigned char TipoMotoreEsp;
extern unsigned char TipoMotoreEsp2;
extern unsigned char ScuotNCicli;			
extern unsigned short ScuotDurataCiclo;	
extern unsigned short TWaitScuot;	
extern unsigned char PotMaxCoclea;
extern unsigned short PotMaxGiri;
extern unsigned short PotMaxPortata;
extern unsigned short TPreAlmPorta;
extern unsigned short TAlarmPorta;
extern unsigned char DeltaSanitari;
extern unsigned char IstTempAcqua;
extern short GainRisc;					
extern short GainSanitari;			
extern unsigned short TPreAlmAriaCombust;
extern unsigned char PressH2OMax;
extern unsigned char SetIdroPompaOff;
extern unsigned short TWaitPB;
extern unsigned short TPB;
extern unsigned char MinPowerPB;


extern unsigned short SetpSanitari;

extern TBitWord InpBMap21;
#define INPBMAP21						InpBMap21.WORD
#define INPBMAP21_LOW				InpBMap21.BYTE.L
#define INPBMAP21_HIGH				InpBMap21.BYTE.H
#define ALARMBUZZ_OFF				InpBMap21.BIT.D0
#define WARNING2_NOACCENSIONE		InpBMap21.BIT.D1
#define WARNING2_NOFIAMMA			InpBMap21.BIT.D2
#define WARNING1_PREALMFUMI		InpBMap21.BIT.D3
#define WARNING1_ALMFUMI			InpBMap21.BIT.D4
#define WARNING1_SONDAFUMIFAIL	InpBMap21.BIT.D5
#define WARNING1_PORTAAPERTA		InpBMap21.BIT.D6
#define WARNING2_PREALMPRESSIDRO	InpBMap21.BIT.D7
#define WARNING1_SENSPORTATA		InpBMap21.BIT.D8
#define PRESSACQUAFAIL				InpBMap21.BIT.D9

extern TBitWord InpBMap22;
#define INPBMAP22						InpBMap22.WORD
#define INPBMAP22_LOW				InpBMap22.BYTE.L
#define INPBMAP22_HIGH				InpBMap22.BYTE.H
#define WARNING2_PRESSACQUAFAIL	InpBMap22.BIT.D0
#define WARNING2_SONDAARIAFAIL	InpBMap22.BIT.D1
#define WARNING2_SONDAACQUAFAIL	InpBMap22.BIT.D2
#define WARNING1_PREALMPORTA		InpBMap22.BIT.D3
#define NOTIFICA_GUASTO_PORTATA	InpBMap22.BIT.D4
#define WARNING2_SERVICEREQ		InpBMap22.BIT.D5
#define WARNING1_PREALMPELLET		InpBMap22.BIT.D6
#define WARNING1_OSTRUZIONE		InpBMap22.BIT.D7

extern TBitWord InpBMap23;
#define INPBMAP23						InpBMap23.WORD
#define INPBMAP23_LOW				InpBMap23.BYTE.L
#define INPBMAP23_HIGH				InpBMap23.BYTE.H
#define COMANDO_ON					InpBMap23.BIT.D0
#define unCMD_SHOW_MODEM			InpBMap23.BIT.D1
#define SPE_ECO						InpBMap23.BIT.D2
#define unCMD_SHOW_SDH2O			InpBMap23.BIT.D3
#define FIAMMA_ACCESA				InpBMap23.BIT.D4
#define LEGNA_ACCESA					InpBMap23.BIT.D5
#define COMPATIBILITA				InpBMap23.BIT.D6
#define STATO5_POMPA_IDLE			InpBMap23.BIT.D7
#define STATO4_TERMAMBACCUM		InpBMap23.BIT.D8

extern TBitWord InpBMap24;
#define INPBMAP24						InpBMap24.WORD
#define INPBMAP24_LOW				InpBMap24.BYTE.L
#define INPBMAP24_HIGH				InpBMap24.BYTE.H
#define unCMD_SHOW_A15				InpBMap24.BIT.D1
#define STATO4_PULSANTE				InpBMap24.BIT.D2
#define STATO4_FLUSSOSTATO			InpBMap24.BIT.D3
#define STATO4_PELLETPRESENCE		InpBMap24.BIT.D4
#define DIPSW1							InpBMap24.BIT.D5
#define DIPSW2							InpBMap24.BIT.D6

extern TBitWord InpBMap25;
#define INPBMAP25						InpBMap25.WORD
#define INPBMAP25_LOW				InpBMap25.BYTE.L
#define INPBMAP25_HIGH				InpBMap25.BYTE.H
#define AVVIA_FAN1					InpBMap25.BIT.D0
#define AVVIA_PULIZIA				InpBMap25.BIT.D1
#define AVVIA_CANDELETTA			InpBMap25.BIT.D2
#define AVVIA_FAN2					InpBMap25.BIT.D3
#define AVVIA_FAN3					InpBMap25.BIT.D4
#define PRECARICA_PLT				InpBMap25.BIT.D5
#define EN_SCHEDA_AUXA				InpBMap25.BIT.D6
#define AVVIA_POMPA					InpBMap25.BIT.D8
#define STATO5_TREVIE				InpBMap25.BIT.D9
#define STATO5_SCUOTITORE			InpBMap25.BIT.DA
#define STATO5_DIRSCUOTITORE		InpBMap25.BIT.DB
#define EN_SCHEDA_AUX1				InpBMap25.BIT.DC
#define EN_SCHEDA_AUX2				InpBMap25.BIT.DD

extern unsigned char ContaDurata1;
extern unsigned short ContaDurata2;
extern unsigned short ContaDurataEco;
extern unsigned char ContaPID;		
extern unsigned char PotCalc;		
extern unsigned char PotIdro;		
extern unsigned char PotAttuata;	

extern unsigned short TOnFan1Set;
extern unsigned short TOffFan1Set;
extern unsigned short TOnCocleaSet;
extern unsigned short TOffCocleaSet;
extern unsigned short PortataAriaSet;
extern unsigned short GiriMinEspSet;
extern unsigned short AttEsp;
extern unsigned short Fan1Set;
extern unsigned short Fan2Set;
extern unsigned short Fan3Set;
extern unsigned short LivFotores;
extern unsigned short Giri2Set;
extern unsigned short VelocitaEspulsore2;
extern unsigned char DriverCod;
extern unsigned char DriverVers;
extern unsigned char DriverRev;
extern unsigned char DriverBuild;
extern unsigned char SecOrologio;
extern unsigned char MinOrologio;
extern unsigned short TempoOn;

extern TBitWord InpBMap115;
#define INPBMAP115					InpBMap115.WORD
#define INPBMAP115_LOW				InpBMap115.BYTE.L
#define INPBMAP115_HIGH				InpBMap115.BYTE.H
#define ERR_HILIMIT_1				InpBMap115.BIT.D0
#define ERR_APS						InpBMap115.BIT.D1
#define ERR_COCLEA					InpBMap115.BIT.D2
#define ERR_HILIMIT_2				InpBMap115.BIT.D3
#define ERR_CAND						InpBMap115.BIT.D4
#define ERR_RELE						InpBMap115.BIT.D5
#define ERR_AUX_J38					InpBMap115.BIT.D6
#define ERR_SENSHALL_1				InpBMap115.BIT.D8
#define ERR_SENSHALL_2				InpBMap115.BIT.D9
#define ERR_AFS						InpBMap115.BIT.DA


extern TBitWord HoldBMap6;
#define HOLDBMAP6						HoldBMap6.WORD
#define HOLDBMAP6_LOW				HoldBMap6.BYTE.L
#define HOLDBMAP6_HIGH				HoldBMap6.BYTE.H
#define MODE_AUTO_MANU				HoldBMap6.BIT.D0
#define ECO_ENB						HoldBMap6.BIT.D1

extern TBitWord HoldBMap23;
#define HOLDBMAP23					HoldBMap23.WORD
#define HOLDBMAP23_LOW				HoldBMap23.BYTE.L
#define HOLDBMAP23_HIGH				HoldBMap23.BYTE.H
#define F_PRESS_IDRO_PAN			HoldBMap23.BIT.D0
#define F_PUMPMODUL_PAN				HoldBMap23.BIT.D1
#define F_EN_ACCUMULO				HoldBMap23.BIT.D2
#define F_EN_FLUSS_DIGIT			HoldBMap23.BIT.D3
#define F_EN_SPEIDRO					HoldBMap23.BIT.D4
#define F_EN_IDROINDIP				HoldBMap23.BIT.D5
#define EN_SENSPOMPA					HoldBMap23.BIT.D6

extern unsigned char TOnAux;

extern TBitWord PreAcc1;
#define PREACC1_BMAP					PreAcc1.WORD
#define PREACC1_LOW					PreAcc1.BYTE.L
#define PREACC1_HIGH					PreAcc1.BYTE.H
#define PREACC1_FAN1					PreAcc1.BIT.D0
#define PREACC1_ESP					PreAcc1.BIT.D1
#define PREACC1_CAND					PreAcc1.BIT.D2
#define PREACC1_FAN2					PreAcc1.BIT.D3
#define PREACC1_FAN3					PreAcc1.BIT.D4
#define PREACC1_COCLEA				PreAcc1.BIT.D5
#define PREACC1_AUXA					PreAcc1.BIT.D6
#define PREACC1_POMPA				PreAcc1.BIT.D8
#define PREACC1_3VIE					PreAcc1.BIT.D9
#define PREACC1_PULIZMECC			PreAcc1.BIT.DA
#define PREACC1_DIRPULIZMECC		PreAcc1.BIT.DB
#define PREACC1_AUX1					PreAcc1.BIT.DC
#define PREACC1_AUX2					PreAcc1.BIT.DD
extern unsigned short PreAcc1_TOnCoclea;
extern unsigned short PreAcc1_TOffCoclea;
extern unsigned short PreAcc1_Portata;
extern unsigned short PreAcc1_Esp;
extern unsigned short PreAcc1_TOnFan1;
extern unsigned short PreAcc1_TOffFan1;
extern unsigned short PreAcc1_AttuaFan1;
extern unsigned short PreAcc1_AttuaFan2;
extern unsigned short PreAcc1_AttuaFan3;
extern unsigned short PreAcc1_Giri2;

extern TBitWord PreAcc2;
#define PREACC2_BMAP					PreAcc2.WORD
#define PREACC2_LOW					PreAcc2.BYTE.L
#define PREACC2_HIGH					PreAcc2.BYTE.H
#define PREACC2_FAN1					PreAcc2.BIT.D0
#define PREACC2_ESP					PreAcc2.BIT.D1
#define PREACC2_CAND					PreAcc2.BIT.D2
#define PREACC2_FAN2					PreAcc2.BIT.D3
#define PREACC2_FAN3					PreAcc2.BIT.D4
#define PREACC2_COCLEA				PreAcc2.BIT.D5
#define PREACC2_AUXA					PreAcc2.BIT.D6
#define PREACC2_POMPA				PreAcc2.BIT.D8
#define PREACC2_3VIE					PreAcc2.BIT.D9
#define PREACC2_PULIZMECC			PreAcc2.BIT.DA
#define PREACC2_DIRPULIZMECC		PreAcc2.BIT.DB
#define PREACC2_AUX1					PreAcc2.BIT.DC
#define PREACC2_AUX2					PreAcc2.BIT.DD
extern unsigned short PreAcc2_TOnCoclea;
extern unsigned short PreAcc2_TOffCoclea;
extern unsigned short PreAcc2_Portata;
extern unsigned short PreAcc2_Esp;
extern unsigned short PreAcc2_TOnFan1;
extern unsigned short PreAcc2_TOffFan1;
extern unsigned short PreAcc2_AttuaFan1;
extern unsigned short PreAcc2_AttuaFan2;
extern unsigned short PreAcc2_AttuaFan3;
extern unsigned short PreAcc2_Giri2;

extern TBitWord PreAccCaldo;
#define PREACCCALDO_BMAP			PreAccCaldo.WORD
#define PREACCCALDO_LOW				PreAccCaldo.BYTE.L
#define PREACCCALDO_HIGH			PreAccCaldo.BYTE.H
#define PREACCCALDO_FAN1			PreAccCaldo.BIT.D0
#define PREACCCALDO_ESP				PreAccCaldo.BIT.D1
#define PREACCCALDO_CAND			PreAccCaldo.BIT.D2
#define PREACCCALDO_FAN2			PreAccCaldo.BIT.D3
#define PREACCCALDO_FAN3			PreAccCaldo.BIT.D4
#define PREACCCALDO_COCLEA			PreAccCaldo.BIT.D5
#define PREACCCALDO_AUXA			PreAccCaldo.BIT.D6
#define PREACCCALDO_POMPA			PreAccCaldo.BIT.D8
#define PREACCCALDO_3VIE			PreAccCaldo.BIT.D9
#define PREACCCALDO_PULIZMECC		PreAccCaldo.BIT.DA
#define PREACCCALDO_DIRPULIZMECC	PreAccCaldo.BIT.DB
#define PREACCCALDO_AUX1			PreAccCaldo.BIT.DC
#define PREACCCALDO_AUX2			PreAccCaldo.BIT.DD
extern unsigned short PreAccCaldo_TOnCoclea;
extern unsigned short PreAccCaldo_TOffCoclea;
extern unsigned short PreAccCaldo_Portata;
extern unsigned short PreAccCaldo_Esp;
extern unsigned short PreAccCaldo_TOnFan1;
extern unsigned short PreAccCaldo_TOffFan1;
extern unsigned short PreAccCaldo_AttuaFan1;
extern unsigned short PreAccCaldo_AttuaFan2;
extern unsigned short PreAccCaldo_AttuaFan3;
extern unsigned short PreAccCaldo_Giri2;

extern TBitWord AccA;
#define ACCA_BMAP					AccA.WORD
#define ACCA_LOW					AccA.BYTE.L
#define ACCA_HIGH					AccA.BYTE.H
#define ACCA_FAN1					AccA.BIT.D0
#define ACCA_ESP					AccA.BIT.D1
#define ACCA_CAND					AccA.BIT.D2
#define ACCA_FAN2					AccA.BIT.D3
#define ACCA_FAN3					AccA.BIT.D4
#define ACCA_COCLEA				AccA.BIT.D5
#define ACCA_AUXA					AccA.BIT.D6
#define ACCA_POMPA				AccA.BIT.D8
#define ACCA_3VIE					AccA.BIT.D9
#define ACCA_PULIZMECC			AccA.BIT.DA
#define ACCA_DIRPULIZMECC		AccA.BIT.DB
#define ACCA_AUX1					AccA.BIT.DC
#define ACCA_AUX2					AccA.BIT.DD
extern unsigned short AccA_TOnCoclea;
extern unsigned short AccA_TOffCoclea;
extern unsigned short AccA_Portata;
extern unsigned short AccA_Esp;
extern unsigned short AccA_TOnFan1;
extern unsigned short AccA_TOffFan1;
extern unsigned short AccA_AttuaFan1;
extern unsigned short AccA_AttuaFan2;
extern unsigned short AccA_AttuaFan3;
extern unsigned short AccA_Giri2;

extern TBitWord AccB;
#define ACCB_BMAP					AccB.WORD
#define ACCB_LOW					AccB.BYTE.L
#define ACCB_HIGH					AccB.BYTE.H
#define ACCB_FAN1					AccB.BIT.D0
#define ACCB_ESP					AccB.BIT.D1
#define ACCB_CAND					AccB.BIT.D2
#define ACCB_FAN2					AccB.BIT.D3
#define ACCB_FAN3					AccB.BIT.D4
#define ACCB_COCLEA				AccB.BIT.D5
#define ACCB_AUXA					AccB.BIT.D6
#define ACCB_POMPA				AccB.BIT.D8
#define ACCB_3VIE					AccB.BIT.D9
#define ACCB_PULIZMECC			AccB.BIT.DA
#define ACCB_DIRPULIZMECC		AccB.BIT.DB
#define ACCB_AUX1					AccB.BIT.DC
#define ACCB_AUX2					AccB.BIT.DD
extern unsigned short AccB_TOnCoclea;
extern unsigned short AccB_TOffCoclea;
extern unsigned short AccB_Portata;
extern unsigned short AccB_Esp;
extern unsigned short AccB_TOnFan1;
extern unsigned short AccB_TOffFan1;
extern unsigned short AccB_AttuaFan1;
extern unsigned short AccB_AttuaFan2;
extern unsigned short AccB_AttuaFan3;
extern unsigned short AccB_Giri2;

extern TBitWord FireOnA;
#define FIREONA_BMAP					FireOnA.WORD
#define FIREONA_LOW					FireOnA.BYTE.L
#define FIREONA_HIGH					FireOnA.BYTE.H
#define FIREONA_FAN1					FireOnA.BIT.D0
#define FIREONA_ESP					FireOnA.BIT.D1
#define FIREONA_CAND					FireOnA.BIT.D2
#define FIREONA_FAN2					FireOnA.BIT.D3
#define FIREONA_FAN3					FireOnA.BIT.D4
#define FIREONA_COCLEA				FireOnA.BIT.D5
#define FIREONA_AUXA					FireOnA.BIT.D6
#define FIREONA_POMPA				FireOnA.BIT.D8
#define FIREONA_3VIE					FireOnA.BIT.D9
#define FIREONA_PULIZMECC			FireOnA.BIT.DA
#define FIREONA_DIRPULIZMECC		FireOnA.BIT.DB
#define FIREONA_AUX1					FireOnA.BIT.DC
#define FIREONA_AUX2					FireOnA.BIT.DD
extern unsigned short FireOnA_TOnCoclea;
extern unsigned short FireOnA_TOffCoclea;
extern unsigned short FireOnA_Portata;
extern unsigned short FireOnA_Esp;
extern unsigned short FireOnA_TOnFan1;
extern unsigned short FireOnA_TOffFan1;
extern unsigned short FireOnA_AttuaFan1;
extern unsigned short FireOnA_AttuaFan2;
extern unsigned short FireOnA_AttuaFan3;
extern unsigned short FireOnA_Giri2;

extern TBitWord FireOnB;
#define FIREONB_BMAP					FireOnB.WORD
#define FIREONB_LOW					FireOnB.BYTE.L
#define FIREONB_HIGH					FireOnB.BYTE.H
#define FIREONB_FAN1					FireOnB.BIT.D0
#define FIREONB_ESP					FireOnB.BIT.D1
#define FIREONB_CAND					FireOnB.BIT.D2
#define FIREONB_FAN2					FireOnB.BIT.D3
#define FIREONB_FAN3					FireOnB.BIT.D4
#define FIREONB_COCLEA				FireOnB.BIT.D5
#define FIREONB_AUXA					FireOnB.BIT.D6
#define FIREONB_POMPA				FireOnB.BIT.D8
#define FIREONB_3VIE					FireOnB.BIT.D9
#define FIREONB_PULIZMECC			FireOnB.BIT.DA
#define FIREONB_DIRPULIZMECC		FireOnB.BIT.DB
#define FIREONB_AUX1					FireOnB.BIT.DC
#define FIREONB_AUX2					FireOnB.BIT.DD
extern unsigned short FireOnB_TOnCoclea;
extern unsigned short FireOnB_TOffCoclea;
extern unsigned short FireOnB_Portata;
extern unsigned short FireOnB_Esp;
extern unsigned short FireOnB_TOnFan1;
extern unsigned short FireOnB_TOffFan1;
extern unsigned short FireOnB_AttuaFan1;
extern unsigned short FireOnB_AttuaFan2;
extern unsigned short FireOnB_AttuaFan3;
extern unsigned short FireOnB_Giri2;

extern TBitWord AccC;
#define ACCC_BMAP					AccC.WORD
#define ACCC_LOW					AccC.BYTE.L
#define ACCC_HIGH					AccC.BYTE.H
#define ACCC_FAN1					AccC.BIT.D0
#define ACCC_ESP					AccC.BIT.D1
#define ACCC_CAND					AccC.BIT.D2
#define ACCC_FAN2					AccC.BIT.D3
#define ACCC_FAN3					AccC.BIT.D4
#define ACCC_COCLEA				AccC.BIT.D5
#define ACCC_AUXA					AccC.BIT.D6
#define ACCC_POMPA				AccC.BIT.D8
#define ACCC_3VIE					AccC.BIT.D9
#define ACCC_PULIZMECC			AccC.BIT.DA
#define ACCC_DIRPULIZMECC		AccC.BIT.DB
#define ACCC_AUX1					AccC.BIT.DC
#define ACCC_AUX2					AccC.BIT.DD
extern unsigned short AccC_TOnCoclea;
extern unsigned short AccC_TOffCoclea;
extern unsigned short AccC_Portata;
extern unsigned short AccC_Esp;
extern unsigned short AccC_TOnFan1;
extern unsigned short AccC_TOffFan1;
extern unsigned short AccC_AttuaFan1;
extern unsigned short AccC_AttuaFan2;
extern unsigned short AccC_AttuaFan3;
extern unsigned short AccC_Giri2;

extern TBitWord Spenta;
#define SPENTA_BMAP					Spenta.WORD
#define SPENTA_LOW					Spenta.BYTE.L
#define SPENTA_HIGH					Spenta.BYTE.H
#define SPENTA_FAN1					Spenta.BIT.D0
#define SPENTA_ESP					Spenta.BIT.D1
#define SPENTA_CAND					Spenta.BIT.D2
#define SPENTA_FAN2					Spenta.BIT.D3
#define SPENTA_FAN3					Spenta.BIT.D4
#define SPENTA_COCLEA				Spenta.BIT.D5
#define SPENTA_AUXA					Spenta.BIT.D6
#define SPENTA_POMPA					Spenta.BIT.D8
#define SPENTA_3VIE					Spenta.BIT.D9
#define SPENTA_PULIZMECC			Spenta.BIT.DA
#define SPENTA_DIRPULIZMECC		Spenta.BIT.DB
#define SPENTA_AUX1					Spenta.BIT.DC
#define SPENTA_AUX2					Spenta.BIT.DD
extern unsigned short Spenta_TOnCoclea;
extern unsigned short Spenta_TOffCoclea;
extern unsigned short Spenta_Portata;
extern unsigned short Spenta_Esp;
extern unsigned short Spenta_TOnFan1;
extern unsigned short Spenta_TOffFan1;
extern unsigned short Spenta_AttuaFan1;
extern unsigned short Spenta_AttuaFan2;
extern unsigned short Spenta_AttuaFan3;
extern unsigned short Spenta_Giri2;

extern TBitWord SpeA;
#define SPEA_BMAP					SpeA.WORD
#define SPEA_LOW					SpeA.BYTE.L
#define SPEA_HIGH					SpeA.BYTE.H
#define SPEA_FAN1					SpeA.BIT.D0
#define SPEA_ESP					SpeA.BIT.D1
#define SPEA_CAND					SpeA.BIT.D2
#define SPEA_FAN2					SpeA.BIT.D3
#define SPEA_FAN3					SpeA.BIT.D4
#define SPEA_COCLEA				SpeA.BIT.D5
#define SPEA_AUXA					SpeA.BIT.D6
#define SPEA_POMPA				SpeA.BIT.D8
#define SPEA_3VIE					SpeA.BIT.D9
#define SPEA_PULIZMECC			SpeA.BIT.DA
#define SPEA_DIRPULIZMECC		SpeA.BIT.DB
#define SPEA_AUX1					SpeA.BIT.DC
#define SPEA_AUX2					SpeA.BIT.DD
extern unsigned short SpeA_TOnCoclea;
extern unsigned short SpeA_TOffCoclea;
extern unsigned short SpeA_Portata;
extern unsigned short SpeA_Esp;
extern unsigned short SpeA_TOnFan1;
extern unsigned short SpeA_TOffFan1;
extern unsigned short SpeA_AttuaFan1;
extern unsigned short SpeA_AttuaFan2;
extern unsigned short SpeA_AttuaFan3;
extern unsigned short SpeA_Giri2;

extern TBitWord SpeB;
#define SPEB_BMAP					SpeB.WORD
#define SPEB_LOW					SpeB.BYTE.L
#define SPEB_HIGH					SpeB.BYTE.H
#define SPEB_FAN1					SpeB.BIT.D0
#define SPEB_ESP					SpeB.BIT.D1
#define SPEB_CAND					SpeB.BIT.D2
#define SPEB_FAN2					SpeB.BIT.D3
#define SPEB_FAN3					SpeB.BIT.D4
#define SPEB_COCLEA				SpeB.BIT.D5
#define SPEB_AUXA					SpeB.BIT.D6
#define SPEB_POMPA				SpeB.BIT.D8
#define SPEB_3VIE					SpeB.BIT.D9
#define SPEB_PULIZMECC			SpeB.BIT.DA
#define SPEB_DIRPULIZMECC		SpeB.BIT.DB
#define SPEB_AUX1					SpeB.BIT.DC
#define SPEB_AUX2					SpeB.BIT.DD
extern unsigned short SpeB_TOnCoclea;
extern unsigned short SpeB_TOffCoclea;
extern unsigned short SpeB_Portata;
extern unsigned short SpeB_Esp;
extern unsigned short SpeB_TOnFan1;
extern unsigned short SpeB_TOffFan1;
extern unsigned short SpeB_AttuaFan1;
extern unsigned short SpeB_AttuaFan2;
extern unsigned short SpeB_AttuaFan3;
extern unsigned short SpeB_Giri2;

extern TBitWord RaffA;
#define RAFFA_BMAP				RaffA.WORD
#define RAFFA_LOW					RaffA.BYTE.L
#define RAFFA_HIGH				RaffA.BYTE.H
#define RAFFA_FAN1				RaffA.BIT.D0
#define RAFFA_ESP					RaffA.BIT.D1
#define RAFFA_CAND				RaffA.BIT.D2
#define RAFFA_FAN2				RaffA.BIT.D3
#define RAFFA_FAN3				RaffA.BIT.D4
#define RAFFA_COCLEA				RaffA.BIT.D5
#define RAFFA_AUXA				RaffA.BIT.D6
#define RAFFA_POMPA				RaffA.BIT.D8
#define RAFFA_3VIE				RaffA.BIT.D9
#define RAFFA_PULIZMECC			RaffA.BIT.DA
#define RAFFA_DIRPULIZMECC		RaffA.BIT.DB
#define RAFFA_AUX1				RaffA.BIT.DC
#define RAFFA_AUX2				RaffA.BIT.DD
extern unsigned short RaffA_TOnCoclea;
extern unsigned short RaffA_TOffCoclea;
extern unsigned short RaffA_Portata;
extern unsigned short RaffA_Esp;
extern unsigned short RaffA_TOnFan1;
extern unsigned short RaffA_TOffFan1;
extern unsigned short RaffA_AttuaFan1;
extern unsigned short RaffA_AttuaFan2;
extern unsigned short RaffA_AttuaFan3;
extern unsigned short RaffA_Giri2;

extern TBitWord RaffB;
#define RAFFB_BMAP				RaffB.WORD
#define RAFFB_LOW					RaffB.BYTE.L
#define RAFFB_HIGH				RaffB.BYTE.H
#define RAFFB_FAN1				RaffB.BIT.D0
#define RAFFB_ESP					RaffB.BIT.D1
#define RAFFB_CAND				RaffB.BIT.D2
#define RAFFB_FAN2				RaffB.BIT.D3
#define RAFFB_FAN3				RaffB.BIT.D4
#define RAFFB_COCLEA				RaffB.BIT.D5
#define RAFFB_AUXA				RaffB.BIT.D6
#define RAFFB_POMPA				RaffB.BIT.D8
#define RAFFB_3VIE				RaffB.BIT.D9
#define RAFFB_PULIZMECC			RaffB.BIT.DA
#define RAFFB_DIRPULIZMECC		RaffB.BIT.DB
#define RAFFB_AUX1				RaffB.BIT.DC
#define RAFFB_AUX2				RaffB.BIT.DD
extern unsigned short RaffB_TOnCoclea;
extern unsigned short RaffB_TOffCoclea;
extern unsigned short RaffB_Portata;
extern unsigned short RaffB_Esp;
extern unsigned short RaffB_TOnFan1;
extern unsigned short RaffB_TOffFan1;
extern unsigned short RaffB_AttuaFan1;
extern unsigned short RaffB_AttuaFan2;
extern unsigned short RaffB_AttuaFan3;
extern unsigned short RaffB_Giri2;

extern TBitWord SpeC;
#define SPEC_BMAP					SpeC.WORD
#define SPEC_LOW					SpeC.BYTE.L
#define SPEC_HIGH					SpeC.BYTE.H
#define SPEC_FAN1					SpeC.BIT.D0
#define SPEC_ESP					SpeC.BIT.D1
#define SPEC_CAND					SpeC.BIT.D2
#define SPEC_FAN2					SpeC.BIT.D3
#define SPEC_FAN3					SpeC.BIT.D4
#define SPEC_COCLEA				SpeC.BIT.D5
#define SPEC_AUXA					SpeC.BIT.D6
#define SPEC_POMPA				SpeC.BIT.D8
#define SPEC_3VIE					SpeC.BIT.D9
#define SPEC_PULIZMECC			SpeC.BIT.DA
#define SPEC_DIRPULIZMECC		SpeC.BIT.DB
#define SPEC_AUX1					SpeC.BIT.DC
#define SPEC_AUX2					SpeC.BIT.DD
extern unsigned short SpeC_TOnCoclea;
extern unsigned short SpeC_TOffCoclea;
extern unsigned short SpeC_Portata;
extern unsigned short SpeC_Esp;
extern unsigned short SpeC_TOnFan1;
extern unsigned short SpeC_TOffFan1;
extern unsigned short SpeC_AttuaFan1;
extern unsigned short SpeC_AttuaFan2;
extern unsigned short SpeC_AttuaFan3;
extern unsigned short SpeC_Giri2;

extern unsigned char DeltaTempCaldo;
extern unsigned short TAccB;
extern unsigned short TAccC;
extern unsigned short TFireOnB;
extern unsigned short TSpeA;
extern unsigned short TSpeB;
extern unsigned short TSpeC;
extern unsigned short TRaffA;
extern unsigned short TRaffB;
extern unsigned short PeriodoCoclea;
extern unsigned short PeriodoFan1;

extern TBitWord Pulizia;
#define PULIZIA_BMAP					Pulizia.WORD
#define PULIZIA_LOW					Pulizia.BYTE.L
#define PULIZIA_HIGH					Pulizia.BYTE.H
#define PULIZIA_FAN1					Pulizia.BIT.D0
#define PULIZIA_ESP					Pulizia.BIT.D1
#define PULIZIA_CAND					Pulizia.BIT.D2
#define PULIZIA_FAN2					Pulizia.BIT.D3
#define PULIZIA_FAN3					Pulizia.BIT.D4
#define PULIZIA_COCLEA				Pulizia.BIT.D5
#define PULIZIA_AUXA					Pulizia.BIT.D6
#define PULIZIA_POMPA				Pulizia.BIT.D8
#define PULIZIA_3VIE					Pulizia.BIT.D9
#define PULIZIA_PULIZMECC			Pulizia.BIT.DA
#define PULIZIA_DIRPULIZMECC		Pulizia.BIT.DB
#define PULIZIA_AUX1					Pulizia.BIT.DC
#define PULIZIA_AUX2					Pulizia.BIT.DD
extern unsigned short Pulizia_TOnCoclea;
extern unsigned short Pulizia_TOffCoclea;
extern unsigned short Pulizia_Portata;
extern unsigned short Pulizia_Esp;
extern unsigned short Pulizia_TOnFan1;
extern unsigned short Pulizia_TOffFan1;
extern unsigned short Pulizia_AttuaFan1;
extern unsigned short Pulizia_AttuaFan2;
extern unsigned short Pulizia_AttuaFan3;
extern unsigned short Pulizia_Giri2;

extern TBitWord Pot1;
#define POT1_BMAP					Pot1.WORD
#define POT1_LOW					Pot1.BYTE.L
#define POT1_HIGH					Pot1.BYTE.H
#define POT1_FAN1					Pot1.BIT.D0
#define POT1_ESP					Pot1.BIT.D1
#define POT1_CAND					Pot1.BIT.D2
#define POT1_FAN2					Pot1.BIT.D3
#define POT1_FAN3					Pot1.BIT.D4
#define POT1_COCLEA				Pot1.BIT.D5
#define POT1_AUXA					Pot1.BIT.D6
#define POT1_POMPA				Pot1.BIT.D8
#define POT1_3VIE					Pot1.BIT.D9
#define POT1_PULIZMECC			Pot1.BIT.DA
#define POT1_DIRPULIZMECC		Pot1.BIT.DB
#define POT1_AUX1					Pot1.BIT.DC
#define POT1_AUX2					Pot1.BIT.DD
extern unsigned short Pot1_TOnCoclea;
extern unsigned short Pot1_TOffCoclea;
extern unsigned short Pot1_Portata;
extern unsigned short Pot1_Esp;
extern unsigned short Pot1_TOnFan1;
extern unsigned short Pot1_TOffFan1;
extern unsigned short Pot1_AttuaFan1;
extern unsigned short Pot1_AttuaFan2;
extern unsigned short Pot1_AttuaFan3;
extern unsigned short Pot1_Giri2;

extern TBitWord Pot2;
#define POT2_BMAP					Pot2.WORD
#define POT2_LOW					Pot2.BYTE.L
#define POT2_HIGH					Pot2.BYTE.H
#define POT2_FAN1					Pot2.BIT.D0
#define POT2_ESP					Pot2.BIT.D1
#define POT2_CAND					Pot2.BIT.D2
#define POT2_FAN2					Pot2.BIT.D3
#define POT2_FAN3					Pot2.BIT.D4
#define POT2_COCLEA				Pot2.BIT.D5
#define POT2_AUXA					Pot2.BIT.D6
#define POT2_POMPA				Pot2.BIT.D8
#define POT2_3VIE					Pot2.BIT.D9
#define POT2_PULIZMECC			Pot2.BIT.DA
#define POT2_DIRPULIZMECC		Pot2.BIT.DB
#define POT2_AUX1					Pot2.BIT.DC
#define POT2_AUX2					Pot2.BIT.DD
extern unsigned short Pot2_TOnCoclea;
extern unsigned short Pot2_TOffCoclea;
extern unsigned short Pot2_Portata;
extern unsigned short Pot2_Esp;
extern unsigned short Pot2_TOnFan1;
extern unsigned short Pot2_TOffFan1;
extern unsigned short Pot2_AttuaFan1;
extern unsigned short Pot2_AttuaFan2;
extern unsigned short Pot2_AttuaFan3;
extern unsigned short Pot2_Giri2;

extern TBitWord Pot3;
#define POT3_BMAP					Pot3.WORD
#define POT3_LOW					Pot3.BYTE.L
#define POT3_HIGH					Pot3.BYTE.H
#define POT3_FAN1					Pot3.BIT.D0
#define POT3_ESP					Pot3.BIT.D1
#define POT3_CAND					Pot3.BIT.D2
#define POT3_FAN2					Pot3.BIT.D3
#define POT3_FAN3					Pot3.BIT.D4
#define POT3_COCLEA				Pot3.BIT.D5
#define POT3_AUXA					Pot3.BIT.D6
#define POT3_POMPA				Pot3.BIT.D8
#define POT3_3VIE					Pot3.BIT.D9
#define POT3_PULIZMECC			Pot3.BIT.DA
#define POT3_DIRPULIZMECC		Pot3.BIT.DB
#define POT3_AUX1					Pot3.BIT.DC
#define POT3_AUX2					Pot3.BIT.DD
extern unsigned short Pot3_TOnCoclea;
extern unsigned short Pot3_TOffCoclea;
extern unsigned short Pot3_Portata;
extern unsigned short Pot3_Esp;
extern unsigned short Pot3_TOnFan1;
extern unsigned short Pot3_TOffFan1;
extern unsigned short Pot3_AttuaFan1;
extern unsigned short Pot3_AttuaFan2;
extern unsigned short Pot3_AttuaFan3;
extern unsigned short Pot3_Giri2;

extern TBitWord Pot4;
#define POT4_BMAP					Pot4.WORD
#define POT4_LOW					Pot4.BYTE.L
#define POT4_HIGH					Pot4.BYTE.H
#define POT4_FAN1					Pot4.BIT.D0
#define POT4_ESP					Pot4.BIT.D1
#define POT4_CAND					Pot4.BIT.D2
#define POT4_FAN2					Pot4.BIT.D3
#define POT4_FAN3					Pot4.BIT.D4
#define POT4_COCLEA				Pot4.BIT.D5
#define POT4_AUXA					Pot4.BIT.D6
#define POT4_POMPA				Pot4.BIT.D8
#define POT4_3VIE					Pot4.BIT.D9
#define POT4_PULIZMECC			Pot4.BIT.DA
#define POT4_DIRPULIZMECC		Pot4.BIT.DB
#define POT4_AUX1					Pot4.BIT.DC
#define POT4_AUX2					Pot4.BIT.DD
extern unsigned short Pot4_TOnCoclea;
extern unsigned short Pot4_TOffCoclea;
extern unsigned short Pot4_Portata;
extern unsigned short Pot4_Esp;
extern unsigned short Pot4_TOnFan1;
extern unsigned short Pot4_TOffFan1;
extern unsigned short Pot4_AttuaFan1;
extern unsigned short Pot4_AttuaFan2;
extern unsigned short Pot4_AttuaFan3;
extern unsigned short Pot4_Giri2;

extern TBitWord Pot5;
#define POT5_BMAP					Pot5.WORD
#define POT5_LOW					Pot5.BYTE.L
#define POT5_HIGH					Pot5.BYTE.H
#define POT5_FAN1					Pot5.BIT.D0
#define POT5_ESP					Pot5.BIT.D1
#define POT5_CAND					Pot5.BIT.D2
#define POT5_FAN2					Pot5.BIT.D3
#define POT5_FAN3					Pot5.BIT.D4
#define POT5_COCLEA				Pot5.BIT.D5
#define POT5_AUXA					Pot5.BIT.D6
#define POT5_POMPA				Pot5.BIT.D8
#define POT5_3VIE					Pot5.BIT.D9
#define POT5_PULIZMECC			Pot5.BIT.DA
#define POT5_DIRPULIZMECC		Pot5.BIT.DB
#define POT5_AUX1					Pot5.BIT.DC
#define POT5_AUX2					Pot5.BIT.DD
extern unsigned short Pot5_TOnCoclea;
extern unsigned short Pot5_TOffCoclea;
extern unsigned short Pot5_Portata;
extern unsigned short Pot5_Esp;
extern unsigned short Pot5_TOnFan1;
extern unsigned short Pot5_TOffFan1;
extern unsigned short Pot5_AttuaFan1;
extern unsigned short Pot5_AttuaFan2;
extern unsigned short Pot5_AttuaFan3;
extern unsigned short Pot5_Giri2;

extern TBitWord Pot6;
#define POT6_BMAP					Pot6.WORD
#define POT6_LOW					Pot6.BYTE.L
#define POT6_HIGH					Pot6.BYTE.H
#define POT6_FAN1					Pot6.BIT.D0
#define POT6_ESP					Pot6.BIT.D1
#define POT6_CAND					Pot6.BIT.D2
#define POT6_FAN2					Pot6.BIT.D3
#define POT6_FAN3					Pot6.BIT.D4
#define POT6_COCLEA				Pot6.BIT.D5
#define POT6_AUXA					Pot6.BIT.D6
#define POT6_POMPA				Pot6.BIT.D8
#define POT6_3VIE					Pot6.BIT.D9
#define POT6_PULIZMECC			Pot6.BIT.DA
#define POT6_DIRPULIZMECC		Pot6.BIT.DB
#define POT6_AUX1					Pot6.BIT.DC
#define POT6_AUX2					Pot6.BIT.DD
extern unsigned short Pot6_TOnCoclea;
extern unsigned short Pot6_TOffCoclea;
extern unsigned short Pot6_Portata;
extern unsigned short Pot6_Esp;
extern unsigned short Pot6_TOnFan1;
extern unsigned short Pot6_TOffFan1;
extern unsigned short Pot6_AttuaFan1;
extern unsigned short Pot6_AttuaFan2;
extern unsigned short Pot6_AttuaFan3;
extern unsigned short Pot6_Giri2;

extern TBitWord Pot7;
#define POT7_BMAP					Pot7.WORD
#define POT7_LOW					Pot7.BYTE.L
#define POT7_HIGH					Pot7.BYTE.H
#define POT7_FAN1					Pot7.BIT.D0
#define POT7_ESP					Pot7.BIT.D1
#define POT7_CAND					Pot7.BIT.D2
#define POT7_FAN2					Pot7.BIT.D3
#define POT7_FAN3					Pot7.BIT.D4
#define POT7_COCLEA				Pot7.BIT.D5
#define POT7_AUXA					Pot7.BIT.D6
#define POT7_POMPA				Pot7.BIT.D8
#define POT7_3VIE					Pot7.BIT.D9
#define POT7_PULIZMECC			Pot7.BIT.DA
#define POT7_DIRPULIZMECC		Pot7.BIT.DB
#define POT7_AUX1					Pot7.BIT.DC
#define POT7_AUX2					Pot7.BIT.DD
extern unsigned short Pot7_TOnCoclea;
extern unsigned short Pot7_TOffCoclea;
extern unsigned short Pot7_Portata;
extern unsigned short Pot7_Esp;
extern unsigned short Pot7_TOnFan1;
extern unsigned short Pot7_TOffFan1;
extern unsigned short Pot7_AttuaFan1;
extern unsigned short Pot7_AttuaFan2;
extern unsigned short Pot7_AttuaFan3;
extern unsigned short Pot7_Giri2;

extern TBitWord Pot8;
#define POT8_BMAP					Pot8.WORD
#define POT8_LOW					Pot8.BYTE.L
#define POT8_HIGH					Pot8.BYTE.H
#define POT8_FAN1					Pot8.BIT.D0
#define POT8_ESP					Pot8.BIT.D1
#define POT8_CAND					Pot8.BIT.D2
#define POT8_FAN2					Pot8.BIT.D3
#define POT8_FAN3					Pot8.BIT.D4
#define POT8_COCLEA				Pot8.BIT.D5
#define POT8_AUXA					Pot8.BIT.D6
#define POT8_POMPA				Pot8.BIT.D8
#define POT8_3VIE					Pot8.BIT.D9
#define POT8_PULIZMECC			Pot8.BIT.DA
#define POT8_DIRPULIZMECC		Pot8.BIT.DB
#define POT8_AUX1					Pot8.BIT.DC
#define POT8_AUX2					Pot8.BIT.DD
extern unsigned short Pot8_TOnCoclea;
extern unsigned short Pot8_TOffCoclea;
extern unsigned short Pot8_Portata;
extern unsigned short Pot8_Esp;
extern unsigned short Pot8_TOnFan1;
extern unsigned short Pot8_TOffFan1;
extern unsigned short Pot8_AttuaFan1;
extern unsigned short Pot8_AttuaFan2;
extern unsigned short Pot8_AttuaFan3;
extern unsigned short Pot8_Giri2;

extern TBitWord PuliziaB;
#define PULIZIAB_BMAP				PuliziaB.WORD
#define PULIZIAB_LOW					PuliziaB.BYTE.L
#define PULIZIAB_HIGH				PuliziaB.BYTE.H
#define PULIZIAB_FAN1				PuliziaB.BIT.D0
#define PULIZIAB_ESP					PuliziaB.BIT.D1
#define PULIZIAB_CAND				PuliziaB.BIT.D2
#define PULIZIAB_FAN2				PuliziaB.BIT.D3
#define PULIZIAB_FAN3				PuliziaB.BIT.D4
#define PULIZIAB_COCLEA				PuliziaB.BIT.D5
#define PULIZIAB_AUXA				PuliziaB.BIT.D6
#define PULIZIAB_POMPA				PuliziaB.BIT.D8
#define PULIZIAB_3VIE				PuliziaB.BIT.D9
#define PULIZIAB_PULIZMECC			PuliziaB.BIT.DA
#define PULIZIAB_DIRPULIZMECC		PuliziaB.BIT.DB
#define PULIZIAB_AUX1				PuliziaB.BIT.DC
#define PULIZIAB_AUX2				PuliziaB.BIT.DD
extern unsigned short PuliziaB_TOnCoclea;
extern unsigned short PuliziaB_TOffCoclea;
extern unsigned short PuliziaB_Portata;
extern unsigned short PuliziaB_Esp;
extern unsigned short PuliziaB_TOnFan1;
extern unsigned short PuliziaB_TOffFan1;
extern unsigned short PuliziaB_AttuaFan1;
extern unsigned short PuliziaB_AttuaFan2;
extern unsigned short PuliziaB_AttuaFan3;
extern unsigned short PuliziaB_Giri2;

extern unsigned short EspRitardoInc;
extern unsigned short EspRitardoDec;
extern unsigned short PotRitardoInc;
extern unsigned short PotRitardoDec;
extern unsigned short TPBB;

extern TBitWord HoldBMap286;
#define HOLDBMAP286					HoldBMap286.WORD
#define HOLDBMAP286_LOW				HoldBMap286.BYTE.L
#define HOLDBMAP286_HIGH			HoldBMap286.BYTE.H
#define ALARMBUZZ						HoldBMap286.BIT.D0
#define LIMIT_SERVICE				HoldBMap286.BIT.D1
#define DISAB_CNTPALMFUMI			HoldBMap286.BIT.D4
#define DIS_A06						HoldBMap286.BIT.D6

extern unsigned short DurataMaxPreAllFumi;
extern unsigned short DurataPreAllPellet;
extern unsigned char DeltaTAssenzaFiamma;
extern unsigned short PortataCritica;
extern unsigned char DeltaPortata;

extern TBitWord HoldBMap305;
#define HOLDBMAP305					HoldBMap305.WORD
#define HOLDBMAP305_LOW				HoldBMap305.BYTE.L
#define HOLDBMAP305_HIGH			HoldBMap305.BYTE.H
#define F_IDRO_PAN					HoldBMap305.BIT.D0
#define F_PELLET_PAN					HoldBMap305.BIT.D1
#define F_EN_TERMAMB					HoldBMap305.BIT.D2
#define F_ACC_TERMOCOPPIA			HoldBMap305.BIT.D3
#define F_ACC_FOTORES				HoldBMap305.BIT.D4
#define F_EN_RIPRISTINO				HoldBMap305.BIT.D5
#define F_EN_LAMBDA					HoldBMap305.BIT.D6
#define EN_LIMGIRI					HoldBMap305.BIT.D7
#define EN_FANTEMPFUMI				HoldBMap305.BIT.D8
#define EN_ESP2						HoldBMap305.BIT.D9
#define EN_IBRIDO						HoldBMap305.BIT.DA
#define EN_NOACCACALDO				HoldBMap305.BIT.DB
#define EN_PERIODO_COCLEA			HoldBMap305.BIT.DC
#define EN_PERIODO_FAN1				HoldBMap305.BIT.DD
#define F_EN_SCUOTITORE				HoldBMap305.BIT.DF

extern TBitWord HoldBMap306;
#define HOLDBMAP306					HoldBMap306.WORD
#define HOLDBMAP306_LOW				HoldBMap306.BYTE.L
#define HOLDBMAP306_HIGH			HoldBMap306.BYTE.H
#define F_HALL_PAN					HoldBMap306.BIT.D0
#define F_PORTATA_PAN				HoldBMap306.BIT.D1
#define EN_COCLEA_HALL				HoldBMap306.BIT.D2
#define EN_PULIT_COCLEA2			HoldBMap306.BIT.D3
#define EN_FAN2_HALL					HoldBMap306.BIT.D4
#define EN_RAMPACAND					HoldBMap306.BIT.D5
#define F_FRENATA_COCLEA			HoldBMap306.BIT.D6
#define F_FRENATA_FAN1_COCLEA2	HoldBMap306.BIT.D7

extern unsigned short DurataBlkoutRecovery;
extern unsigned char TipoFrenataCoclea;
extern unsigned char TipoFrenataFan1;


extern unsigned char IdRF;
extern TBitWord HoldBMap8194;
#define HOLDBMAP8194					HoldBMap8194.WORD
#define HOLDBMAP8194_LOW			HoldBMap8194.BYTE.L
#define HOLDBMAP8194_HIGH			HoldBMap8194.BYTE.H
#define EN_IP_NETWORK				HoldBMap8194.BIT.D0	// WifiEn		// Gestione wifi
#define WIFI_ACTIVE					HoldBMap8194.BIT.D1	// WifiInfo		// Dati wifi acquisiti
#define WIFI_DATA						HoldBMap8194.BIT.D2	// WifiSet		// Cambiamento impostazioni wifi
#define WIFI_WPS						HoldBMap8194.BIT.D3	// WifiWPS		// Richiesta comando WPS
#define WIFI_CLIENT					HoldBMap8194.BIT.D4	// MbCrono		// Crono gestito dalla scheda
#define WIFI_AP_MODE					HoldBMap8194.BIT.D5	// WifiAP		// Richiesta ripristino AP

#define TCPPORT_SIZE	5
extern unsigned short PortaIP;
#define IP_SIZE		4
extern unsigned char Ip[IP_SIZE];
extern unsigned char Subnet[IP_SIZE];
extern unsigned char Gateway[IP_SIZE];
extern unsigned char Dns[IP_SIZE];
#define SSID_SIZE		32
extern char Ssid[SSID_SIZE+1];
#define KEYW_SIZE		64
extern char CryptoKey[KEYW_SIZE+1];
#define WPS_PIN_SIZE		4
extern char WPSPin[WPS_PIN_SIZE+1];


extern unsigned char Ossigeno;
extern unsigned char OssigenoResiduo;

extern unsigned short Consumo;
extern unsigned short PesataPellet;

extern unsigned short ParamCode; // codice/versione parametri (cod cliente)
extern unsigned short ParamRel; // rev/build parametri (report modifica)

extern unsigned short FotoresThresOn;
extern unsigned short FotoresThresOff;

extern unsigned char ConfigIdro;
extern unsigned char MinSetFan1;
extern unsigned char MinSetFan2;
extern unsigned char MinSetFan3;
extern unsigned char PreAcc1NumExec;
extern unsigned char PreAcc2NumExec;
extern unsigned char K_Sonda;

extern unsigned short NumAcc;

extern TBitWord HoldBMap8315;
#define HOLDBMAP8315					HoldBMap8315.WORD
#define HOLDBMAP8315_LOW			HoldBMap8315.BYTE.L
#define HOLDBMAP8315_HIGH			HoldBMap8315.BYTE.H
#define REL0							HoldBMap8315.BIT.D0
#define REL1							HoldBMap8315.BIT.D1
#define REL2							HoldBMap8315.BIT.D2
#define REL3							HoldBMap8315.BIT.D3
#define REL4							HoldBMap8315.BIT.D4
#define REL5							HoldBMap8315.BIT.D5
#define REL6							HoldBMap8315.BIT.D6
#define REL7							HoldBMap8315.BIT.D7
#define REL8							HoldBMap8315.BIT.D8
#define REL9							HoldBMap8315.BIT.D9

extern TBitWord HoldBMap8316;
#define HOLDBMAP8316					HoldBMap8316.WORD
#define HOLDBMAP8316_LOW			HoldBMap8316.BYTE.L
#define HOLDBMAP8316_HIGH			HoldBMap8316.BYTE.H
#define TERM1							HoldBMap8316.BIT.D0
#define TERM2							HoldBMap8316.BIT.D1

extern short TempP0;
extern short TempP1;
extern short TempP2;
extern short TempP3;
extern short TempP4;

extern char Local_ISO3166_1_Code[2+1];	// codice ISO3166-1 alpha 2 del Paese locale
extern unsigned char OTdelay;
#define OTDELAY_PAN			(20)	// valore OT delay forzato dal pannello


#endif	// _DATISTUFA_H
