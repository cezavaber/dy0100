/*!
** \file DynSizeVector.h
** \author Alessandro Vagniluca
** \date 18/09/2017
** \brief Include file del modulo DynSizeVector.c
** 
** 		Rif.: https://www.happybearsoftware.com/implementing-a-dynamic-array
** 
** \version 1.0
**/

#ifndef _DYNSIZEVECTOR_H
#define _DYNSIZEVECTOR_H


#define VECTOR_INITIAL_CAPACITY 16	//!< vector initial capacity

// Vector type
typedef struct 
{
	unsigned short size;		//!< slots used
	unsigned short capacity;	//!< total available slots
	unsigned short dataSize;	//!< element size
	void *data;					//!< array of elements to store
} Vector;

/*!
** \fn short vector_init(Vector *vector)
** \brief Initializes a vector struct. 'dataSize' must be defined
** \param vector An allocated Vector struct
** \return 1: OK, 0: FAILED
** \note Init values:
** 		.size = 0;
** 		.capacity = VECTOR_INITIAL_CAPACITY;
** 		.data = malloc(vector->dataSize * VECTOR_INITIAL_CAPACITY);
**/
short vector_init(Vector *vector);

/*!
** \fn short vector_append(Vector *vector, void *value)
** \brief Appends the given element to the vector
** \param vector The Vector struct
** \param value The element to append
** \return 1: OK, 0: FAILED
** \note If full, the array is expanded.
**/
short vector_append(Vector *vector, void *value);

/*!
** \fn void *vector_get(Vector *vector, unsigned short index)
** \brief Returns the element out of a vector at the given index
** \param vector The Vector struct
** \param index The element index
** \return The pointer to the element or NULL if out of bounds.
**/
void *vector_get(Vector *vector, unsigned short index);

/*!
** \fn short vector_set(Vector *vector, unsigned short index, void *value)
** \brief Sets the element at the given index to the given value
** \param vector The Vector struct
** \param index The element index
** \param value The element new value
** \return 1: OK, 0: FAILED
** \note If full, the array is expanded to the given index.
**/
short vector_set(Vector *vector, unsigned short index, void *value);

/*!
** \fn short vector_increment_capacity_if_full(Vector *vector, unsigned short newCapacity)
** \brief Increments the underlying data array capacity
** \param vector The Vector struct
** \return 1: OK, 0: FAILED
**/
short vector_increment_capacity_if_full(Vector *vector);

/*!
** \fn void vector_free(Vector *vector)
** \brief Frees the memory allocated for the data array
** \param vector The Vector struct
** \return None
**/
void vector_free(Vector *vector);


#endif	// _DYNSIZEVECTOR_H
