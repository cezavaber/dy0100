/*!
	\file OT_Plus_data_layer.h
	\brief Include file del modulo OT_Plus_data_layer.c
*/

#ifndef _OT_PLUS_DATA_LAYER_H
#define _OT_PLUS_DATA_LAYER_H


//#define DEBUG_LONG_PAUSE	// abilita il debug della pausa lunga


#include "OT_Plus_ph_layer.h"


/*!
	\struct Pkt_struct
	\brief The packet tag structure
*/
struct Pkt_struct
{
	unsigned short pktLen;						//!< packet length
	unsigned char pkt[MAX_PACKET_SIZE];		//!< packet buffer
};
/*!
	\typedef PKT_STRUCT
	\brief Typedef definition for Pkt_struct
*/
typedef struct Pkt_struct PKT_STRUCT;

/*!
	\enum SEND_MODE
	\brief Modo di invio
*/
enum SEND_MODE
{
	SEND_MODE_ONE_SHOT,		//!< invio unico
	SEND_MODE_ONE_SHOT_EXTERNAL,		//!< invio unico da client esterno
	SEND_MODE_CYCLE,			//!< invio ciclico
	NUM_SEND_MODE
};

/*!
	\def MAX_NUM_ONE_SHOT_PKT
	\brief Numero massimo di pacchetti con unico invio.
*/
#define MAX_NUM_ONE_SHOT_PKT				20

/*!
	\def MAX_NUM_ONE_SHOT_EXTERNAL_PKT
	\brief Numero massimo di pacchetti con unico invio da client esterno.
*/
#define MAX_NUM_ONE_SHOT_EXTERNAL_PKT	10

/*!
	\def MAX_NUM_CYCLE_PKT
	\brief Numero massimo di pacchetti con invio ciclico.
*/
#define MAX_NUM_CYCLE_PKT					20

/*!
	\enum OT_RX_ERROR_CODE
	\brief Codici di errore transazione tx-rx
*/
enum OT_RX_ERROR_CODE
{
	RX_ERR_NONE = 0,		//!< ok
	RX_ERR_ONE_SHOT,		//!< errore su invio unico
	RX_ERR_ONE_SHOT_EXTERNAL,		//!< errore su invio unico da client esterno
	RX_ERR_CYCLE,			//!< errore su invio ciclico
	NUM_RX_ERR
};

/*!
	\enum ot_dl_st
	\brief Stati del gestore livello dati MASTER
*/
enum ot_dl_st
{
	OT_DL_STAT_IDLE,
	OT_DL_STAT_ONE_SHOT_BUSY,
	OT_DL_STAT_ONE_SHOT_EXTERNAL_BUSY,
	OT_DL_STAT_CYCLE_BUSY,
	NUM_OT_DL_STAT
};
/*!
	\var OT_DL_Stat
	\brief Stato del gestore livello dati MASTER
*/
extern unsigned char OT_DL_Stat;

/*!
	\var OT_DL_Pausa
	\brief Pausa (in ms) tra cicli di richieste cicliche
*/
extern unsigned short OT_DL_Pausa;														
#define OT_DL_PAUSE			(T1CEN)		// 10ms pausa invio
#define OT_DL_LONG_PAUSE	(T1MIN)	// 1min pausa invio
#ifdef DEBUG_LONG_PAUSE
	#define OT_DL_LONG_PAUSE_FACT	(1)	// 1min = 1*OT_DL_LONG_PAUSE pausa invio effettiva
#else
	#define OT_DL_LONG_PAUSE_FACT	(8)	// 8min = 8*OT_DL_LONG_PAUSE pausa invio effettiva
#endif
extern unsigned char cntPausa;	//!< contatore di pausa

#ifdef TXRX_ONLY
extern unsigned long cntTxRx, cntTxRxpre;	// contatore transazioni tx-rx effettuate
extern unsigned long cntTxRxGood;	// contatore transazioni tx-rx effettuate con successo
#endif

/*!
	\var OT_DL_RFFilter
	\brief Abilitazione ricezione con filtro codice RF
*/
extern unsigned char OT_DL_RFFilter;														

/*!
	\typedef void (*OT_DL_RxPkt)( unsigned char *pkt, unsigned short pktSize,
											unsigned char ErrCode, PKT_STRUCT *Req );
	\brief Puntatore a funzione call-back per ricezione pacchetto.
	\param pkt pacchetto ricevuto
	\param pktSize dimensione pacchetto
	\param ErrCode codice di errore transazione tx-rx
	\param Req Richiesta trasmessa
*/
typedef void (*OT_DL_RxPkt)( unsigned char *pkt, unsigned short pktSize,
										unsigned char ErrCode, PKT_STRUCT *Req );

/*!
	\typedef void (*OT_DL_LimitReq)( void );
	\brief Puntatore a funzione call-back per eventuale invio richiesta Limiti/Step.
*/
typedef void (*OT_DL_LimitReq)( void );



/*!
	\fn short OT_DL_Init( OT_DL_RxPkt Rx_CallBack )
   \brief Inizializzazione data layer OT/+
	\param Rx_CallBack funzione call-back di ricezione
	\return 1: OK, 0: FAILED
*/
short OT_DL_Init( OT_DL_RxPkt Rx_CallBack );

/*!
	\fn void OT_DL_SetLimitReqFunc( OT_DL_LimitReq LimReq_CallBack )
   \brief Imposta la funzione call-back per invio richiesta Limiti/Step
	\param LimReq_CallBack funzione call-back per invio richiesta Limiti/Step
*/
void OT_DL_SetLimitReqFunc( OT_DL_LimitReq LimReq_CallBack );

/*!
	\fn short OT_DL_Send( PKT_STRUCT *sPkt, unsigned char mode )
   \brief Invio di un pacchetto
	\param pkt struttura pacchetto da inviare
	\param mode modo di invio (SEND_MODE_ONE_SHOT_xxx o SEND_MODE_CYCLE)
	\return 1: OK, 0: FAILED
	\note I pacchetti con invio unico sono prioritari rispetto a quelli
			con invio ciclico.
			La struttura del pacchetto con invio unico viene ricopiata dalla
			funzione nella propria coda e puo` quindi essere allocata in modo
			dinamico.
			La struttura del pacchetto con invio ciclico viene registrata dalla
			funzione come puntatore nella propria coda e deve quindi essere
			allocata in modo statico.
*/
short OT_DL_Send( PKT_STRUCT *sPkt, unsigned char mode );

/*!
	\fn void OT_DL_FlushOneShotQueue( void )
	\brief Svuota la coda dei pacchetti con invio unico
*/
void OT_DL_FlushOneShotQueue( void );

/*!
	\fn void OT_DL_FlushOneShotExtQueue( void )
	\brief Svuota la coda dei pacchetti con invio unico da client esterno
*/
void OT_DL_FlushOneShotExtQueue( void );

/*!
	\fn void OT_DL_FlushCycleQueue( void )
	\brief Svuota la coda dei pacchetti con invio ciclico
*/
void OT_DL_FlushCycleQueue( void );

/*!
	\fn void OT_DL_Handler( void )
	\brief Gestore del livello dati MASTER della comunicazione OT/+
*/
void OT_DL_Handler( void );

/*!
	\fn void OT_DL_Restart( void )
	\brief Riavvia il gestore del livello dati MASTER della comunicazione OT/+
*/
void OT_DL_Restart( void );

/*!
	\fn void OT_DL_SetExtRxFunc( OT_DL_RxPkt Rx_CallBack )
   \brief Imposta la funzione call-back per ricezione risposte a richieste da
			 client esterno.
	\param Rx_CallBack funzione call-back di ricezione
*/
void OT_DL_SetExtRxFunc( OT_DL_RxPkt Rx_CallBack );

/*!
	\fn PKT_STRUCT *PeekPkt( unsigned char mode )
   \brief Rileva un pacchetto dalla coda indicata, senza eliminarlo dalla coda.
	\param mode modo di invio (SEND_MODE_ONE_SHOT_xxx o SEND_MODE_CYCLE)
   \return Il puntatore al pacchetto, NULL se coda vuota.
*/
PKT_STRUCT *PeekPkt( unsigned char mode );



#endif	// _OT_PLUS_DATA_LAYER_H
