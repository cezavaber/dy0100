/*!
** \file res.h
** \author Alessandro Vagniluca
** \date 20/10/2015
** \brief Include file for the RES.C source module
** 
** \version 1.0
**/

#ifndef _RES_H_
#define _RES_H_

#include "bitmaps.h"
#include "fonts.h"
#include "languages.h"
#include "DynSizeVector.h"

/*!
** \def RES_VER
** \brief Resources version number
**/
#define RES_VER			1
/*!
** \def RES_REV
** \brief Resources revision number
**/
#define RES_REV			0
/*!
** \def RES_MAINT
** \brief Resources maintenance number
**/
#define RES_MAINT		0

#define sRES_VERSIONE		"Graphic Resources 1.0"

#define CTRL_AREA_ADDR			(0x003FFFF0)	//!< control area address
#define CTRL_AREA_4K_SECT		(1023)	//!< 4KB sector for control area

/*!
** \typedef CTRL_PARAM
** \brief The control parameters structure
**/
typedef struct __packed
{
	unsigned short BitmapNum;	//!< number of registered bitmaps
	unsigned short FontNum;	//!< number of registered fonts
	unsigned char LangNum;	//!< number of registered strings sets
	unsigned char reserved[7];
	unsigned char Version;	//!< version number
	unsigned char Revision;	//!< revision number
	unsigned char Maintenance;	//!< maintenance number
} CTRL_PARAM;
/*!
** \typedef CTRL_AREA
** \brief The control area structure
**/
typedef struct __packed
{
	CTRL_PARAM param;	//!< control parameters
	unsigned char crc;	//!< control check code
} CTRL_AREA;
/*!
** \var CtrlArea
** \brief The control area copy in ram
**/
extern CTRL_AREA CtrlArea;

extern unsigned char ResVersion;	//!< Resources version number acquired from uSD
extern unsigned char ResRevision;	//!< Resources revision number acquired from uSD
extern unsigned char ResMaintenance;	//!< Resources maintenance number acquired from uSD


#define FONT_MAX_DEF		31	// window max ROM font (height = 49, maxWidth = 36)
#define FONT_TITLE_DEF		29	// window title ROM font (height = 28, maxWidth = 22)
#define FONT_DESC_DEF		28	// parameter description ROM font (height = 25, maxWidth = 18)
#define FONT_VALUE_DEF		27 // parameter value ROM font (height = 20, maxWidth = 16)

extern ft_uint8_t fontMax;		//!< window max font
extern ft_uint8_t fontTitle;	//!< window title font
extern ft_uint8_t fontDesc;		//!< parameter description font
extern ft_uint8_t fontValue;	//!< parameter value font

/*!
** \var iFontGroup
** \brief The current font group
**/
extern unsigned char iFontGroup;

/*!
** \var SigleStati
** \brief Strings list for language selection
**/
extern Vector SigleStati;


/*!
** \fn short res_Init( void )
** \brief Init the resources handler
** \return 1: OK, 0: FAILED
**/
short res_Init( void );

/*!
** \fn unsigned long res_bm_Load( unsigned char iBitmap, unsigned long gram )
** \brief Copy a bitmap from dataflash to GRAM
** 
** If one or more bitmaps are loaded into GRAM, at the end of their use they
** must be all released to free the used GRAM.
** This allows to have a bitmaps stack loaded into GRAM: the last 'pushed'
** bitmap is the first bitmap to be 'popped'.
** 
** \param iBitmap The bitmap index
** \param gram The GRAM start address
** \return The number > 0 of the copied bytes if OK, 0 if FAILED
**/
unsigned long res_bm_Load( unsigned char iBitmap, unsigned long gram );

/*!
** \fn short res_bm_Release( unsigned char iBitmap )
** \brief Free the GRAM from the loaded bitmap
** 
** If one or more bitmaps are loaded into GRAM, at the end of their use they
** must be all released to free the used GRAM.
** This allows to have a bitmaps stack loaded into GRAM: the last 'pushed'
** bitmap is the first bitmap to be 'popped'.
** 
** \param iBitmap The bitmap index
** \return 1: OK, 0: FAILED
**/
short res_bm_Release( unsigned char iBitmap );

/*!
** \fn BM_HEADER *res_bm_GetHeader( unsigned char iBitmap )
** \brief Get the bitmap header for the loaded bitmap
** \param iBitmap The bitmap index
** \return The pointer to the bitmap header, NULL otherwise.
**/
BM_HEADER *res_bm_GetHeader( unsigned char iBitmap );

/*!
** \fn unsigned long res_bm_GetGRAM( void )
** \brief Get the available GRAM address for a new bitmap load
** 
** This function returns the next available GRAM address after the last loaded 
** bitmap.
** 
** \return The available GRAM address value
**/
unsigned long res_bm_GetGRAM( void );

/*!
** \fn void res_bm_ReleaseAll( void )
** \brief Free the GRAM from all the loaded bitmaps
** \return None
**/
void res_bm_ReleaseAll( void );

/*!
** \fn void res_bm_Reset( void )
** \brief Reset the GRAM to the starting operative state
** \return None
**/
void res_bm_Reset( void );


/*!
** \fn const char * GetMsg( unsigned char Indice_Riga, char *sDest,
** 								unsigned char Indice_Lingua,
** 								unsigned short Indice_Stringa )
** \brief Get a language string 
** \param Indice_Riga row index (1: row 1, 2: row 2, ..., NUM_ROW_BUFFER: row NUM_ROW_BUFFER)
** \param sDest Destination buffer; if NULL, the row string buffer is used.
** \param Indice_Lingua Language index
** \param Indice_Stringa String index
** \return The pointer to the string buffer, NULL if not found.
**/
#define NUM_ROW_BUFFER		(8)
const char * GetMsg( unsigned char Indice_Riga, char *sDest,
									unsigned char Indice_Lingua,
									unsigned short Indice_Stringa );

									
/*!
** \fn short LoadFonts( void )
** \brief Load the fonts to be used with the currente language
** \return The number of the fonts loaded with no errors
**/
short LoadFonts( void );



#endif	// _RES_H_

