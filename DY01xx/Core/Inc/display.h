/*!
** \file display.h
** \author Alessandro Vagniluca
** \date 25/11/2015
** \brief Header file for display's defines
** 
** \version 1.0
**/

#ifndef _DISPLAY_H_
#define _DISPLAY_H_


// LCD display parameters (RCL DISPLAY R043C002RTCV02 4.3" WQVGA - 480x272)
#define FT_DispWidth   		(480)							// Active width of LCD display
#define FT_DispHeight  		(272)							// Active height of LCD display
#define FT_DispHCycle  		(525)							// Total number of clocks per line
#define FT_DispHOffset 		(40)							// Start of active line
#define FT_DispHSync0  		(0)							// Start of horizontal sync pulse
#define FT_DispHSync1  		(1)							// End of horizontal sync pulse
#define FT_DispVCycle  		(288)							// Total number of lines per screen
#define FT_DispVOffset 		(8)							// Start of active screen
#define FT_DispVSync0  		(0)							// Start of vertical sync pulse
#define FT_DispVSync1  		(1)							// End of vertical sync pulse
#define FT_DispPCLK    		(5)							// Pixel Clock 48/5 = 9MHz
#define FT_DispSwizzle 		(0)							// Define RGB output pins
#define FT_DispPCLKPol 		(1)							// Define active edge of PCLK

// Colors - fully saturated colors defined here
#define RED					0xFF0000UL													// Red
#define GREEN				0x00FF00UL													// Green
#define BLUE				0x0000FFUL													// Blue
#define WHITE				0xFFFFFFUL													// White
#define BLACK				0x000000UL													// Black




#endif	// _DISPLAY_H_
