/*!
** \file ctp.h
** \author Alessandro Vagniluca
** \date 07/07/2015
** \brief CTP.C header file
** 
** \version 1.0
**/

#ifndef _CTP_H_
#define _CTP_H_



// CTP pin definitions
//#define CTP_RST				(RST_TOUCH_Pin)
//#define CTP_RST_PORT		(RST_TOUCH_GPIO_Port)
#define CTP_INT				(INT_FT801_Pin)	//(INT_TOUCH_Pin)
#define CTP_INT_PORT		(INT_FT801_GPIO_Port)	//(INT_TOUCH_GPIO_Port)
#define CTP_EXTI_IRQn		(EXTI1_IRQn)	//(EXTI9_5_IRQn)

/*
	The following touch tag types can be registered:
	- KEY
	  Tag for keys. The touch state for this type is updated when the touch event 
	  occurs in the (xs,ys);(xe,ye) rectangular area.
	- HSCROLL
	  Tag for horizontal slider or screen scrolling. The touch state for this type
	  is updated when the touch event occurs in the (xs,ys);(xe,ye) rectangular
	  area and |Dy| < TAG_SCROLL_MAX_DELTA from its previous touch event.
	- VSCROLL
	  Tag for vertical slider or screen scrolling. The touch state for this type
	  is updated when the touch event occurs in the (xs,ys);(xe,ye) rectangular
	  area and |Dx| < TAG_SCROLL_MAX_DELTA from its previous touch event.
*/
#define TAG_KEY_BASE			0
#define TAG_HSCROLL_BASE	128
#define TAG_VSCROLL_BASE	192
#define TAG_SCROLL_MAX_DELTA		20
typedef struct
{
	unsigned char tag;			//!< tag value and type: KEY = 0-127, HSCROLL = 128-191, VSCROLL = 192-255
	unsigned short xs;			//!< X start
	unsigned short ys;			//!< Y start
	unsigned short xe;			//!< X end
	unsigned short ye;			//!< Y end
	unsigned char enabled;		//!< 1=enabled, 0=disabled
	unsigned char status;		//!< 1=touch, 0=un-touch
	unsigned short x;				//!< X current
	unsigned short y;				//!< Y current
	short Dx;						//!< X delta (tracking value)
	short Dy;						//!< Y delta (tracking value)
} TAG_STRUCT;

/*!
** \fn CTP_TagRegister( TAG_STRUCT *pTag )
** \brief Register a touch tag
** \param pTag The tag structure pointer
** \return 1: OK, 0: FAILED
**/
short CTP_TagRegister( TAG_STRUCT *pTag );

/*!
** \fn CTP_TagDeregister( unsigned char tag )
** \brief Deregister a touch tag
** \param tag The tag to be deregistered
** \return 1: OK, 0: FAILED
**/
short CTP_TagDeregister( unsigned char tag );

/*!
** \fn void CTP_TagDeregisterAll( void )
** \brief Deregister all the current touch tags
**/
void CTP_TagDeregisterAll( void );

/*!
** \fn CTP_SetTag( unsigned char tag, unsigned char mode )
** \brief Set the mode of a registered touch tag
** \param tag The tag to be set
** \param mode Enable (1) or disable (0) the tag.
** \return 1: OK, 0: FAILED
**/
short CTP_SetTag( unsigned char tag, unsigned char mode );


/*!
** \fn int CTP_Init( void )
** \brief CTP interface initialization
** \return 1: OK, 0: FAILED
** \note I2C peripheral is supposed already properly initialized
**/
int CTP_Init( void );

/*!
** \fn void CTP_Task( void const * argument )
** \brief CTP interface task
**/
void CTP_Task( void const * argument );


#endif	// _CTP_H_
