/*!
	\file adc.h
	\brief Include file del modulo adc.c
*/

#ifndef _ADC_H
#define _ADC_H


/*! \def ACCU_NUM_LETT
    \brief Numero di letture A/D per filtraggio valore accumulato
*/
#define ACCU_NUM_LETT	1	//8

/*! \enum adch
    \brief Enumerazione canali A/D
*/
enum adch
{
	ADC_CH_VBATT,	//!< tensione batteria
	ADC_CH_VREFINT,	//!< VREF interna
	NADCCH	
};

/*! \struct anain_struct
    \brief Area RAM per valori A/D.
*/
struct anain_struct
{
	unsigned short VBatt;	//!< tensione batteria
	unsigned short VrefInt;	//!< VREF interna
};
/*! \typedef ANAIN_STRUCT
    \brief Typedef per anain_struct
*/
typedef struct anain_struct ANAIN_STRUCT;
/*! \var Anain
    \brief Struttura dati A/D converter.
*/
extern ANAIN_STRUCT Anain;



/*! \fn void adcInit( void )
    \brief Inizializzazione gestore ADC
*/
void adcInit( void );

/*! \fn void adcStart( void )
    \brief Avvio scan ingressi ADC
*/
void adcStart( void );


#endif	// _ADC_H

