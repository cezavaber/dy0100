/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <stdlib.h>
#include <string.h>
#include "cmsis_os.h"
#include "dwt_stm32_delay.h"
#include "Utils.h"
#include "printf-stdarg.h"
#include "DatiStufa.h"
#include "languages.h"
#include "DynSizeVector.h"

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

//#define DEBUG_LONG_PAUSE	// abilita il debug della pausa standby

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CHARGE_STAT_Pin GPIO_PIN_13
#define CHARGE_STAT_GPIO_Port GPIOC
#define INT1_ACC_Pin GPIO_PIN_1
#define INT1_ACC_GPIO_Port GPIOC
#define INT1_ACC_EXTI_IRQn EXTI1_IRQn
#define USART2_CTS_Pin GPIO_PIN_1
#define USART2_CTS_GPIO_Port GPIOA
#define SPI1_NSS_Pin GPIO_PIN_4
#define SPI1_NSS_GPIO_Port GPIOA
#define PD_FT801_Pin GPIO_PIN_4
#define PD_FT801_GPIO_Port GPIOC
#define INT2_ACC_Pin GPIO_PIN_5
#define INT2_ACC_GPIO_Port GPIOC
#define INT2_ACC_EXTI_IRQn EXTI9_5_IRQn
#define INT_FT801_Pin GPIO_PIN_1
#define INT_FT801_GPIO_Port GPIOB
#define BOOT1_Pin GPIO_PIN_2
#define BOOT1_GPIO_Port GPIOB
#define SPI2_NSS_Pin GPIO_PIN_12
#define SPI2_NSS_GPIO_Port GPIOB
#define BUZZER_Pin GPIO_PIN_13
#define BUZZER_GPIO_Port GPIOB
#define CONF_RF_Pin GPIO_PIN_14
#define CONF_RF_GPIO_Port GPIOB
#define PD_RF_Pin GPIO_PIN_15
#define PD_RF_GPIO_Port GPIOB
#define EN_FLASH_Pin GPIO_PIN_8
#define EN_FLASH_GPIO_Port GPIOC
#define RESET_RF_Pin GPIO_PIN_9
#define RESET_RF_GPIO_Port GPIOC
#define USART2_RTS_Pin GPIO_PIN_8
#define USART2_RTS_GPIO_Port GPIOA
#define EN_V_BCKL_Pin GPIO_PIN_10
#define EN_V_BCKL_GPIO_Port GPIOA
#define PRES_RETE_Pin GPIO_PIN_3
#define PRES_RETE_GPIO_Port GPIOB
#define PULS_Pin GPIO_PIN_4
#define PULS_GPIO_Port GPIOB
#define EN_V_BATT_Pin GPIO_PIN_5
#define EN_V_BATT_GPIO_Port GPIOB
#define EN_PWR_Pin GPIO_PIN_9
#define EN_PWR_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

// versione firmware
#define carSW		'D'	//"DY01"
#define codSW		1
#define verSW		0
#define revSW		1
#define buildSW		16
#if defined SCREENSHOT
	#define sFW_VERSIONE	"DY010001_16_S"
#elif defined DISCHARGE_BATTERY
	#define sFW_VERSIONE	"DY010001_16_D_B"
#else
	#define sFW_VERSIONE	"DY010001_16"
#endif

extern ADC_HandleTypeDef hadc1;
extern DMA_HandleTypeDef hdma_adc1;

extern I2C_HandleTypeDef hi2c1;

extern IWDG_HandleTypeDef hiwdg;

extern RTC_HandleTypeDef hrtc;

extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;

extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart6;

extern osThreadId MAINHandle;
extern osThreadId USERHandle;
extern osThreadId CTPHandle;
extern osMessageQId TouchQueueHandle;
extern osTimerId LedTimerHandle;
extern osMutexId printfMutexHandle;
extern osMutexId EVEMutexHandle;
extern osMutexId I2CMutexHandle;

enum
{
	CLK_SLOW,	//!< SPI1 clock: 1 MHz
	CLK_FAST,	//!< SPI1 clock: 21 MHz
	NUM_CLK_MODE
};

/*! \var SystemFlags
    \brief Bitmap flag di sistema
*/
extern TBitDWord SystemFlags;
#define FLAGS						SystemFlags.DWORD
#define ELAB_INP					SystemFlags.BIT.D0	//!< =1 attiva elaborazione ingressi
#define WIFI_RUN					SystemFlags.BIT.D1	//!< =1 WiFi task is running
#define TYPES_OK					SystemFlags.BIT.D2	//!< =1 ricevuto Tipi Stufa
#define ALIM_INP					SystemFlags.BIT.D3	//!< =1 external supply is present
#define LOGO_OK					SystemFlags.BIT.D4	//!< =1 ricevuto Logo
#define PULIZIA					SystemFlags.BIT.D5	//!< =1 pulizia attiva
#define RTC_TIME					SystemFlags.BIT.D6	//!< =1 avvenuto tick RTC (ogni 1s)
#define STUFA_LINK				SystemFlags.BIT.D7	//!< =1 scheda stufa in link
#define lByTogVisTH2O			SystemFlags.BIT.D8	//!< =1 visualizza temperatura ambiente, altrimenti temperatura acqua
#define BEEP_ON_CMD				SystemFlags.BIT.D9	//!< =1 abilita buzzer a comando per accensione/spegnimento
#define VIS_ACC					SystemFlags.BIT.DA	// =1 visualizza numero mancate accensioni, altrimenti numero accensioni
#define PULS_INP					SystemFlags.BIT.DB	//!< =1 pushbutton is pressed
#define TEST_STUFA				SystemFlags.BIT.DC	//!< =1 test stufa (manuale) attivo
#define TEST_AUTO					SystemFlags.BIT.DD	//!< =1 test stufa (automatico) attivo
#define SBLOCCO_PAN				SystemFlags.BIT.DE	//!< =1 comando di sblocco inviato
#define TEMPFAHRENHEIT			SystemFlags.BIT.DF	//!< =1 visualizzazione temperature in gradi Fahrenheit
//#define EPAR_MODIFIED			SystemFlags.BIT.D10	//!< =1 modificata stringa lunga valore parametro
#define INTERPOL_CMD				SystemFlags.BIT.D11	//!< =1 comanda interpolazione
#define CALIB_FOTORES_ON		SystemFlags.BIT.D12	//!< =1 calibrazione fotoresistenza ON attiva
#define CALIB_FOTORES_OFF		SystemFlags.BIT.D13	//!< =1 calibrazione fotoresistenza OFF attiva
#define VIS_CONSUMO				SystemFlags.BIT.D14	//!< =1 visualizza consumo orario, altrimenti consumo accumulato
#define PULS_IRQ					SystemFlags.BIT.D15	//!< =1 pushbutton interrupt occurred
#define ACC_IRQ					SystemFlags.BIT.D16	//!< =1 accelerometer interrupt occurred
#define PULS_LOCK				SystemFlags.BIT.D17	//!< =1 pushbutton is locked

#define SPI_LINK					(STUFA_LINK)
#define OT_LINK					(STUFA_LINK)

#define NO_INDVAR		(0xFFFF)		// codice indice per variabile non compresa nel protocollo SPI


/*!
 *  \var OpeMode
 *  \brief Modo operativo
 */
enum
{
	MODE_LOW_POWER,
	MODE_IN_CHARGE,
	MODE_CHARGE_END,
	MODE_FULL_POWER,
	MODE_STANDBY,
	NUM_MODE
};
extern unsigned char OpeMode;

/*!
	\var userReq
	\brief Richiesta da pulsante utente
*/
enum
{
	USER_REQ_NONE,
	USER_REQ_STANDBY,
	USER_REQ_WAKEUP,
	USER_REQ_OFF,
	NUM_USER_REQ
};
extern unsigned char UserRequest;



/*!
	\def STANDBY_TIME
	\brief Tempo di standby in secondi.
*/
#ifdef DEBUG_LONG_PAUSE
	#define STANDBY_TIME			60	// 1 min
	#define STANDBY_TIME_UNLINKED	60	// 1 min		//20	// 20 s
	#define STANDBY_TAIN_SAMPLE		5	// 5 s
#else
	#define STANDBY_TIME			900	// 15 min
	#define STANDBY_TIME_UNLINKED	900	// 15 min	//300	// 5 min
	#define STANDBY_TAIN_SAMPLE		600	// 10 min
#endif
#define STANDBY_TIME_MIN			15		// 15 s

#define T_PULS_ON		(3)	// 3s, minimo tempo pulsante premuto per power-on


/*!
** \def debug_out()
** \brief putchar() function (polling)
** \note The USART must be already initialized for logging
**/
#define debug_out(x)				\
	do {	\
		uint16_t tmp = (uint16_t)(x) & 0x01FFU;	\
		while( __HAL_UART_GET_FLAG(&huart6, UART_FLAG_TXE) == FALSE );	\
		huart6.Instance->DR = tmp;	\
	} while(0)

/*!
**  \def Wdog
**  \brief The refresh watchdog function
**/
#ifdef WDOG
	#define Wdog()		HAL_IWDG_Refresh(&hiwdg)
#else
	#define Wdog()		__NOP()
#endif

/*!
** \fn void MX_SPI1_Init(unsigned char mode)
** \brief SPI1 init function
** \param mode The SPI clock configuration mode (CLK_SLOW or CLK_FAST)
** \return None
**/
void MX_SPI1_Init(unsigned char mode);

/*!
** \fn void SystemReset( void )
** \brief System reset
** \return None
**/
void SystemReset( void );

/*!
** \fn void SystemRestart( void )
** \brief System restart
** \return None
**/
void SystemRestart( void );

/*!
** \fn void SystemOff( void )
** \brief System power-off
** \return None
**/
void SystemOff( void );

/*!
** \fn void SleepOnBatteryCharge( void )
** \brief System sleep on battery charging
** \return None
**/
void SleepOnBatteryCharge( void );

/*!
** \fn void CreateMutex( void )
** \brief Create all the used mutex
** \return None
**/
void CreateMutex( void );

/*!
** \fn void DeleteMutex( void )
** \brief Delete all the used mutex
** \return None
**/
void DeleteMutex( void );


#ifdef LOGGING

#define OT_LOGGING	// abilita il log della comunicazione OT/+
#ifdef OT_LOGGING
extern unsigned long TimeSec;	// contatore 0.01s
#endif

/**
  * @fn		int outbyte( int c )
  * @brief  Retargets the C library printf function (fputc) to the USART.
  * @param  c The character to print
  * @retval The printed character
  */
int outbyte( int c );

/*!
**  \fn short LOCKprintf( void )
**  \brief Lock the printf() resource
**  \return 1: OK, 0: FAILED
**/
short LOCKprintf( void );

/*!
**  \fn short UNLOCKprintf( void )
**  \brief Unlock the printf() resource
**  \return 1: OK, 0: FAILED
**/
short UNLOCKprintf( void );
#endif


/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
