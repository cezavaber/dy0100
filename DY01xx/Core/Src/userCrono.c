/*!
** \file userCrono.c
** \author Alessandro Vagniluca
** \date 02/03/2016
** \brief User Interface USER_STATE_CRONO state handler
** 
** \version 1.0
**/

#include "user.h"
#include "User_Parametri.h"
#include "userMenu.h"
#include "userAvvio.h"
#include "userCrono.h"


#ifndef menu_CRONO

/*!
** \enum TouchTags
** \brief The used touch tags values
**/
enum TouchTags
{
	TAG_VERT_SCROLL = 0,
	TAG_PROG,		// P1
	TAG_CLEAR = TAG_PROG + NPROGWEEK,	// TRASH
	TAG_DAY,		// CALENDAR
	TAG_PROFILE,	// FOLDER
	TAG_ENABLE,		// CHECKED
};

#define NUM_BM_WINCRONO		5
static const unsigned short bmWinCrono[NUM_BM_WINCRONO] =
{
	BITMAP_TRASH,			// TAG_CLEAR
	BITMAP_CALENDAR,		// TAG_DAY
	BITMAP_FOLDER,			// TAG_PROFILE
	BITMAP_CHECKED,			// TAG_ENABLE
	BITMAP_UNCHECKED,
};


static short LoadAllBitmapCrono( void );
static void ReleaseAllBitmapCrono( void );


/*!
** \fn void userPostSetCronoPar( void )
** \brief Execute the Chrono post-setting
** \return None
**/
void userPostSetCronoPar( void )
{
	short i;
	
	for( i=0; i<NPROGWEEK; i++ )
	{
		// disabilitazione del programma se OraStart e/o OraStop sono a OFF e/o nessun giorno selezionato
		if( (Crono.Crono_ProgWeek[i].OraStart == PROG_OFF) ||
			(Crono.Crono_ProgWeek[i].OraStop == PROG_OFF) ||
			!(Crono.Crono_ProgWeek[i].Enab.BYTE & 0x7F) )
		{
			userParValue.b[0] = Crono.Crono_ProgWeek[i].Enab.BYTE & 0x7F;
			ScriviCronoProgWeekAbil( i, userParValue.b[0] );
		}
	}
	// se nessun programma e` abilitato, il crono e` disabilitato
	for( i=0; i<NPROGWEEK; i++ )
	{
		if( Crono.Crono_ProgWeek[i].Enab.BIT.WeekEnab )
			break;
	}
	if( i >= NPROGWEEK )
		ScriviCrono( CRONO_OFF );

	// allineamento con eventuale profilo crono preimpostato presente
	Crono.CronoMode.BIT.iProfile = IsCronoProfile();

	AllineaStatoCrono();

	/*
		Se ho abilitato il Crono o un programma settimanale oppure
		ho caricato un profilo crono utente preimpostato,
		attua subito le impostazioni di on/off crono,
		senza aspettare il prossimo quarto d'ora.
	*/
	if( Crono.CronoMode.BIT.Enab == CRONO_ON )
	{
		// Crono abilitato con almeno un programma settimanale abilitato
		/* if(
			(iPar == P_CRONO) ||
			(iPar == P_CRONO_PROFILO) ||
			(iPar == P_CRONO_P1_SETTIM) ||
			(iPar == P_CRONO_P2_SETTIM) ||
			(iPar == P_CRONO_P3_SETTIM) ||
			(iPar == P_CRONO_P4_SETTIM) ||
			(iPar == P_CRONO_P5_SETTIM) ||
			(iPar == P_CRONO_P6_SETTIM)
			) */
		{
			// attuazione on/off crono
			AttuaStatoCrono();
		}
	}
}

/*!
** \fn unsigned char userVisCrono( void )
** \brief Starts the USER_STATE_CRONO User Interface state handling
** \return The next User Interface state 
**/
unsigned char userVisCrono( void )
{
	return USER_STATE_CRONO;
}

/*!
** \fn short LoadAllBitmapCrono( void )
** \brief Load all the bitmaps used in the USER_STATE_CRONO User Interface state
** \return 1: OK, 0: FAILED
**/
static short LoadAllBitmapCrono( void )
{
	short RetVal = 1;
	short i;

	// waiting screen while all the bitmaps are loaded
	Ft_Gpu_CoCmd_Dlstart(pHost);
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,0xFF,0));	// GREEN
	Ft_Gpu_CoCmd_Spinner(pHost, (FT_DispWidth/2),(FT_DispHeight/2),0,1);
	Ft_App_Flush_Co_Buffer( pHost );
	Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

	// push bitmaps into Graphic RAM
	for( i=0; i<NUM_BM_WINCRONO; i++ )
	{
		Wdog();
		//Ft_Gpu_Hal_Sleep( 2 );
		if( res_bm_Load( bmWinCrono[i], res_bm_GetGRAM() ) == 0 )
		{
			RetVal = 0;
		#ifdef LOGGING
			myprintf( sBitmapLoadError, bmWinCrono[i] );
		#endif
			break;
		}
	}

	// Send the stop command
	Ft_Gpu_Hal_WrCmd32(pHost, CMD_STOP);
	Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);

	return RetVal;
}

/*!
** \fn void ReleaseAllBitmapCrono( void )
** \brief Release all the bitmaps used in the USER_STATE_CRONO User Interface state
** \return None
**/
static void ReleaseAllBitmapCrono( void )
{
	short i;

	// clear screen
	Ft_Gpu_CoCmd_Dlstart(pHost);
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
	Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
	Ft_Gpu_CoCmd_Swap( pHost );
	Ft_App_Flush_Co_Buffer( pHost );
	Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
	
	// pop bitmaps from Graphic RAM
	for( i=NUM_BM_WINCRONO-1; i>=0; i-- )
	{
		Wdog();
		res_bm_Release( bmWinCrono[i] );
	}
}

/*!
** \fn unsigned char userCrono( void )
** \brief Handler function for the USER_STATE_CRONO User Interface state
** \return The next User Interface state 
**/
unsigned char userCrono( void )
{
	unsigned char RetVal = USER_STATE_CRONO;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdr;
	unsigned long key_cnt = 0;
	unsigned char loop = 1;
	char s[MAX_LEN_STRINGA+1];
	short i;
	unsigned char tm_wday = 1;  /* Day of week (0-6, 0=Sunday) */
	unsigned short bar_value, bar_size;
	CRONO_PROG_WEEK *prg;
	
	if( LoadAllBitmapCrono() )
	{
		// all the bitmaps were successfully copied

		while( loop )
		{
			if( tagval == TOUCH_TAG_INIT )
			{
				// it's the first time: tag registration
				CTP_TagDeregisterAll();
				userCounterTime = USER_TIMEOUT_SHOW_20;
			}

			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
			// draw QUIT bitmap
			p_bmhdr = res_bm_GetHeader( BITMAP_QUIT );
			if( tagval == TOUCH_TAG_INIT )
			{
				// QUIT bitmap tag
				TagRegister( TAG_QUIT, 0, (FT_DispHeight-p_bmhdr->bmhdr.Height), p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
			}
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (FT_DispHeight-p_bmhdr->bmhdr.Height)*16 ) );
			
			for( i=TAG_CLEAR; i<TAG_CLEAR+NUM_BM_WINCRONO-1; i++ )
			{
				// draw CRONO bitmaps
				if( (i == TAG_ENABLE) && (Crono.CronoMode.BIT.Enab == CRONO_OFF) )
				{
					// Crono is disabled
					p_bmhdr = res_bm_GetHeader( BITMAP_UNCHECKED );
				}
				else
				{
					p_bmhdr = res_bm_GetHeader( bmWinCrono[i-TAG_CLEAR] );
				}
				if( tagval == TOUCH_TAG_INIT )
				{
					// bitmap tag
					TagRegister( i, ((i-TAG_CLEAR+1)*(64+40)), (FT_DispHeight-p_bmhdr->bmhdr.Height), p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
				}
				if( (tagval == i) && (key_cnt >= VALID_TOUCHED_KEY) )
				{
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
				}
				// specify the starting address of the bitmap in graphics RAM
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
				// specify the bitmap format, linestride and height
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
				// set filtering, wrapping and on-screen size
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
				// start drawing bitmap
				Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( ((i-TAG_CLEAR+1)*(64+40))*16, (FT_DispHeight-p_bmhdr->bmhdr.Height)*16 ) );
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			}
			
			// Chrono profile
			// stringa vuota, se profilo crono non impostato
			if( (i = IsCronoProfile()) > 0 )
			{
				//Ft_Gpu_CoCmd_Number( pHost, (3*(64+40)+32), (FT_DispHeight-15), fontValue, OPT_CENTER, i );	it uses ASCII code 0x30-0x39 for numbers!!!
				userParValue.b[0] = i;
				userVisPar( P_CRONO_PROFILO );
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0x00));	// BLACK
				Ft_Gpu_CoCmd_Text( pHost, (3*(64+40)+32), (FT_DispHeight-32), fontValue, OPT_CENTER, String2Font(str2, fontValue) );
			}
			
			// Week Day
			GetMsg( 0, str2, iLanguage, sWeekDay_tab[tm_wday] );
			//str2[4] = '\0';	// only the first 3 characters for week day
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			Ft_Gpu_CoCmd_Text( pHost, (FT_DispWidth/2), 70, fontTitle, OPT_CENTER, String2Font(str2, fontTitle) );

			// hours' bar
			Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
			Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, 114*16 ) );
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( ((FT_DispWidth-1))*16, 123*16 ) );
			for( i=1; i<=23; i++ )
			{
				Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (20+(i-1)*20)*16, (124)*16 ) );
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (20+(i-1)*20)*16, (124+10)*16 ) );
				s[0] = Hex2Ascii( (unsigned char)(i/10) );
				s[1] = Hex2Ascii( (unsigned char)(i%10) );
				s[2] = '\0';
				Ft_Gpu_CoCmd_Text( pHost, 12+(i-1)*20, 142, 26, OPT_CENTERY, s );
			}
				
			// draw the program bar
			for( i=0; i<NPROGWEEK; i++ )
			{
				if( tagval == TOUCH_TAG_INIT )
				{
					// program Px tag
					TagRegister( (TAG_PROG+i), (8+i*77), 150, 77, 50 );
				}
				if( (tagval == (TAG_PROG+i)) && (key_cnt >= VALID_TOUCHED_KEY) )
				{
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
					Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (8+i*77)*16, (150)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (8+(i+1)*77)*16, (150+50)*16 ) );
				}
				// string "Px", x=1,..,6
				if( iFontGroup == FONT_GROUP_CYRILLIC )
				{
					s[0] = 'S';	// 'P'
				}
				else
				{
					s[0] = 'P';
				}
				s[1] = Hex2Ascii( i+1 );
				s[2] = '\0';
				if( Crono.Crono_ProgWeek[i].Enab.BIT.WeekEnab )
				{
					// program Px enabled
					if( Crono.Crono_ProgWeek[i].Enab.BYTE & (1 << tm_wday) )
					{
						// program Px enabled in this week day
						Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColor_tab[UserColorId].addon) );	// Text Color
						Ft_Gpu_CoCmd_Text( pHost, (20+i*77), 175, fontMax, OPT_CENTERY, String2Font(s, fontMax) );
						// show the program Px on the hours' bar
						prg = &Crono.Crono_ProgWeek[i];
						if( (prg->OraStart == PROG_OFF) ||
							(prg->OraStop == PROG_OFF) ||
							(prg->OraStart > prg->OraStop) )
						{
							bar_value = 0;
							bar_size = 0;
						}
						else
						{
							bar_value = prg->OraStart;
							bar_size = prg->OraStop - prg->OraStart;
						}
						Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
						Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (5*bar_value)*16, 114*16 ) );
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (5*(bar_value+bar_size))*16, 123*16 ) );
					}
					else
					{
						// program Px not enabled in this week day
						Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
						Ft_Gpu_CoCmd_Text( pHost, (20+i*77), 175, fontMax, OPT_CENTERY, String2Font(s, fontMax) );
					}
				}
				else
				{
					// program Px disabled
					Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
					Ft_Gpu_CoCmd_Text( pHost, (20+i*77), 175, fontTitle, OPT_CENTERY, String2Font(s, fontMax) );
				}
			}
			
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
			// title
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			GetMsg( 0, s, iLanguage, MENU_CRONO );
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(s, fontTitle) );

			Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
			Ft_Gpu_CoCmd_Swap( pHost );
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
					
		#ifdef SCREENSHOT
			if( IsGenScreeshot() )
			{
				// screenshot
				DoScreenshot( SSHOT_Crono );
			}
		#endif	// SCREENSHOT

			do
			{
				Wdog();
				// wait for a touch event
				event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

				// process the touch event
				if( event.status == osEventMessage )
				{
					pt = (TAG_STRUCT *)event.value.v;
				}
			} while( event.status == osOK );
				
			// event processing
			if( Check4StatusChange() != USER_MAX_STATES )
			{
				loop = 0;	// exit
			}
			else if( LoadFonts() > 0 )
			{
				// fonts updated -> restart window
				tagval = TOUCH_TAG_INIT;
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
			else if( event.status == osEventTimeout )
			{
				// tag reset
				tagval = 0;
				Flag.Key_Detect = 0;
				key_cnt = 0;

				// controllo cambio di stato a timer
				if( userCounterTime )
				{
					if( --userCounterTime == 0 )
					{
						RetVal = userVisAvvio();
						loop = 0;	// exit
					}
				}
			}
			else if( (pt->tag == TAG_QUIT) && !pt->status )
			{
				// exit touch
				
				// flush the touch queue
				FlushTouchQueue();
				
				RetVal = userVisAvvio();
				loop = 0;	// exit
			}
			else
			{
				tagval = Read_Keypad( pt );	// read the keys
				if( tagval )
				{
					// touched
					userCounterTime = USER_TIMEOUT_SHOW_20;
					if( Flag.Key_Detect )
					{
						// first touch
						Flag.Key_Detect = 0;
						key_cnt = 0;
					}
					else if( ++key_cnt >= VALID_TOUCHED_KEY )
					{
						// pressed item
						key_cnt = VALID_TOUCHED_KEY;
					}
					/* else if( ++key_cnt >= 100 )
					{
						// pressed item
						if( !(key_cnt % 5) )
						{
							// process the pressed item on continuous touch
							switch( tagval )
							{
							}
						}
					} */
				}
				else if( pt->tag )
				{
					// untouched
					if( key_cnt >= VALID_TOUCHED_KEY )
					{
						// flush the touch queue
						FlushTouchQueue();

						// process the pressed item on touch release
						switch( pt->tag )
						{
							default:
								if( (pt->tag >= TAG_PROG) && (pt->tag < (TAG_PROG + NPROGWEEK)) )
								{
									// program Px settings
									iCronoPrg = pt->tag - TAG_PROG;
									// pop bitmaps from Graphic RAM
									ReleaseAllBitmapCrono();
									// set program Px
									RetVal = EnterMenuCronoProg();
									//if( (RetVal = EnterMenuCronoProg()) == USER_STATE_MENU )
									{
										loop = 0;	// exit
									}
								}
								// restart window
								tagval = TOUCH_TAG_INIT;
							break;
						
							case TAG_ENABLE:
								// toggle enable/disable Chrono
								userToggleCronoEnable();
							break;
							
							case TAG_DAY:
								// show next week day
								if( ++tm_wday >= 7 )
								{
									tm_wday = 0;
								}
							break;
							
							case TAG_PROFILE:
								// Chrono profile setting
								userGetPar( P_CRONO_PROFILO );
								// se nessun profilo selezionato, parto dal profilo 1
								if( userParValue.b[0] == 0 )
								{
									userParValue.b[0] = 1;
								}
								userSubState = 0;	// parameter view
								// parameter code as title
								userGetParCode( P_CRONO_PROFILO );
								strcpy( s, str2 );
								if( WinSetParam( 
												P_CRONO_PROFILO,
												s,
												(char *)GetMsg( 2, NULL, iLanguage, CARICAPROFILO )
												) )
								{
									// carica il profilo selezionato
									ScriviCronoProfile( userParValue.b[0] );
									LoadCronoProfile( userParValue.b[0] );
									userPostSetCronoPar();
								}
								// restart window
								tagval = TOUCH_TAG_INIT;
							break;
							
							case TAG_CLEAR:
								// clear all Chrono programs
								if( WinConfirm(
												(char *)GetMsg( 2, NULL, iLanguage, REQCONF ),
												(char *)GetMsg( 2, NULL, iLanguage, AZZERA )
												) )
								{
									CronoDef();
									RecConfig();
								}
								// restart window
								tagval = TOUCH_TAG_INIT;
							break;
						}
					}
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
			}
		}
	}
	else
	{
		// reset bitmps GRAM
		res_bm_Reset();
	}
	
	if( RetVal != USER_STATE_MENU )
	{
		// pop bitmaps from Graphic RAM
		ReleaseAllBitmapCrono();
	}
	
	return RetVal;
}

/*!
** \fn void userToggleCronoEnable( void )
** \brief Toggle enable/disable Chrono
** \return None
**/
void userToggleCronoEnable( void )
{
	short i;
	
	if( Crono.CronoMode.BIT.Enab == CRONO_OFF )
	{
		userParValue.b[0] = CRONO_ON;
		// rifiuta l'abilitazione generale del crono, se nessun programma e` abilitato
		for( i=0; i<NPROGWEEK; i++ )
		{
			if( Crono.Crono_ProgWeek[i].Enab.BIT.WeekEnab )
				break;
		}
		if( i >= NPROGWEEK )
			userParValue.b[0] = CRONO_OFF;
	}
	else
	{
		userParValue.b[0] = CRONO_OFF;
	}
	ScriviCrono( userParValue.b[0] );
	userPostSetCronoPar();
}





#endif	// menu_CRONO

