/*!
	\file DecodInput.c
	\brief Modulo per la decodifica degli ingressi
*/

#include "main.h"
#include "timebase.h"
#include "adc.h"
#include "DatiStufa.h"
#include "DecodInput.h"
#include "user.h"
#include "i2cm_drv.h"
#include "lis2de12_reg.h"

#ifdef LOGGING
	#define MONITOR_ADC		// abilita il monitor delle acquisizioni analogiche
#endif




#define CNT_BATTERY		20	// 20x50ms=1s Tempo per filtrare la lettura del sense della batteria
#define FILTRA_1BAT		14	// soglia per batteria in scarica
#define FILTRA_0BAT		4	// soglia per batteria in carica




#define COMP_TAMB		(-250)	// -2.50°C offset di compensazione (0.01°C)
#define T_TAMB_SAMPLE	(20)	// 2s periodo di misura temperatura ambiente (0.1s)

#define T_VBATT_SAMPLE		(10)	//(12000)	// 20min, periodo di misura tensione batteria in scarica (0.1s)
#define T_VBATT_SAMPLE_ALIM	(10)	//(20)	// 2s, periodo di misura tensione batteria in carica (0.1s)


#define MCP9800_I2C_ADDRESS			0x48	//!< 7-bit device address: 0x48
#define MCP9800_TA_REG				0x00	//!< ambient temperature register (TA)
#define MCP9800_CFG_REG				0x01	//!< configuration register (CONFIG)


#define LIS2DE12_I2C_ADDRESS		0x18	//!< 7-bit device address: 0x18



/* variabili inizializzate */



/* variabili non inizializzate */

short TempAmb;	//!< temperatura ambiente (0.01 degC)
unsigned short VBatt;	//!< tensione batteria (mV)
unsigned char BattUp;	//!< stato di ricarica batteria
unsigned short PercVBatt;	//!< percentuale di carica batteria (0.1%)

// variabili per stato di ricarica batteria
unsigned char CntSenseBat;
unsigned char CntChargeBat;

static unsigned short counter;
static unsigned short TAcounter;
static unsigned short VBATTcounter;
static long TAlast;	//!< ultimo valore filtrato temperatura ambiente (0.01 degC)
static unsigned short adcVBatt;
static unsigned short adcVRefInt;
static unsigned short VrefInt;	// (mV) valore VREFINT calibrato

static unsigned char inpStat;

/*! \var DigInput
    \brief Bitmap ingressi digitali
*/
TBitByte DigInput;

static unsigned short filtPuls;	// filtro pulsante
static unsigned char filtAlim;	// filtro alimentazione esterna

/*! MCP9800 CONFIG register */
typedef union
{
	unsigned char b;
	struct
	{
		unsigned char Shutdown:1;	// 1: enabled
		unsigned char CompInt:1;
		unsigned char AlertPolarity:1;
		unsigned char FaultQueue:2;
		unsigned char Resolution:2;	// 0: 0.5°C, 1: 0.25°C, 2: 0.125°C, 3: 0.0625°C
		unsigned char OneShot:1;	// 1: enabled, 0: disabled (end one-shot conversion)
	} bit;
} MCP9800_CFG_REG_STRUCT;
static MCP9800_CFG_REG_STRUCT cfgMCP9800;


/*! LIS2DE12 driver interface */
static lis2de12_ctx_t dev_ctx;


/* costanti */


/*!
** \var VBatt_tab
** \brief Tabella dei parametri batteria
**/
BATT_STRUCT VBatt_tab =
{
	LITIO_VBATT_NOM,
	LITIO_VBATT_OFF,
	LITIO_VBATT_LOW,
	LITIO_VBATT_FULL,
	LITIO_VBATT_ANIN_NOM
};




/* codice */



/*!
** \fn short MCP9800_SetConfig( MCP9800_CFG_REG_STRUCT cfg )
** \brief Imposta il registro CONFIG del sensore MCP9800
** \param cfg Nuovo valore del registro CONFIG
** \return 1: OK, 0: FAILED
**/
static short MCP9800_SetConfig( MCP9800_CFG_REG_STRUCT cfg )
{
	short RetVal = 0;
	unsigned char data[2];	// device select byte + register CONFIG address
	unsigned char esito;
	
	if( osKernelRunning()
		&& (I2CMutexHandle != NULL)
		)
	{
		if( osMutexWait(I2CMutexHandle, 0 ) != osOK )
		{
			return RetVal;
		}
	}

	data[0] = MCP9800_I2C_ADDRESS;
	data[1] = MCP9800_CFG_REG;
	Wdog();
	esito = i2cWriteBuffer( data, 2, &cfg.b, 1 );
	Wdog();

	if( esito == I2C_OK )
	{
		RetVal = 1;
	}
	
	if( osKernelRunning()
		&& (I2CMutexHandle != NULL)
		)
	{
		osMutexRelease(I2CMutexHandle);
	}

	return RetVal;
}

/*!
** \fn short MCP9800_GetConfig( MCP9800_CFG_REG_STRUCT *cfg )
** \brief Legge il registro CONFIG del sensore MCP9800
** \param cfg Buffer del registro CONFIG
** \return 1: OK, 0: FAILED
**/
static short MCP9800_GetConfig( MCP9800_CFG_REG_STRUCT *cfg )
{
	short RetVal = 0;
	unsigned char data[2];	// device select byte + register CONFIG address
	unsigned char esito;
	
	if( osKernelRunning()
		&& (I2CMutexHandle != NULL)
		)
	{
		if( osMutexWait(I2CMutexHandle, 0 ) != osOK )
		{
			return RetVal;
		}
	}

	data[0] = MCP9800_I2C_ADDRESS;
	data[1] = MCP9800_CFG_REG;
	Wdog();
	esito = i2cWriteReadBuffer( data, 2, &cfg->b, 1 );
	Wdog();

	if( esito == I2C_OK )
	{
		RetVal = 1;
	}
	
	if( osKernelRunning()
		&& (I2CMutexHandle != NULL)
		)
	{
		osMutexRelease(I2CMutexHandle);
	}

	return RetVal;
}

/*!
** \fn long TAFilter( long ta )
** \brief Filtraggio valore acquisito della temperatura ambiente
** \param [in] ta Ultimo valore acquisito TA
** \return Il valore TA filtrato
**/
static long TAFilter( long ta )
{
	#define NF		5		// grado del filtraggio (0: disabilitato)

	TAlast *= NF;
	TAlast += ta;
	TAlast /= (NF + 1);
	
	return TAlast;
}


/*!
** \fn short GetTempAmb( void )
** \brief Ritorna il nuovo valore della temperatura ambiente, lanciando una
** 			nuova acquisizione
** \return 1: OK (TempAmb aggiornata), 0: FAILED (TempAmb invariata)
** 
** \details La temperatura ambiente viene acquisita dal sensore MCP9800 in
**			Shutdown e One-Shot Mode con risoluzione 0.25°C (10-bit) e
**			Tconv = 60ms.
**/
static short GetTempAmb( void )
{
	short RetVal = 0;
	unsigned char data[2];	// device select byte + register TA address
	unsigned char b[2];
	unsigned char esito;
	long TAval;	// new TA value in 0.0001°C

	if( osKernelRunning()
		&& (I2CMutexHandle != NULL)
		)
	{
		if( osMutexWait(I2CMutexHandle, 0 ) != osOK )
		{
			return RetVal;
		}
	}
	
	data[0] = MCP9800_I2C_ADDRESS;
	data[1] = MCP9800_TA_REG;
	Wdog();
	esito = i2cWriteReadBuffer( data, 2, b, 2 );
	Wdog();

	if( esito == I2C_OK )
	{
		RetVal = 1;
		// calcola valore temperatura ambiente
		TAval = (long)((signed char)b[0]) * 10000;	// signed integer part in 0.0001°C
		if( b[1] & BIT7 )
		{
			TAval += 5000;	// add 0.5°C
		}
		if( b[1] & BIT6 )
		{
			TAval += 2500;	// add 0.25°C
		}
		if( b[1] & BIT5 )
		{
			TAval += 1250;	// add 0.125°C
		}
		if( b[1] & BIT4 )
		{
			TAval += 625;	// add 0.0625°C
		}
		// arrotonda a 0.01°C
		if( TAval < 0 )
		{
			TAval -= 50;
		}
		else
		{
			TAval += 50;
		}
		TAval /= 100;
		// filtra e aggiorna temperatura ambiente
		TempAmb = (short)TAFilter( TAval );
	}
	
	if( osKernelRunning()
		&& (I2CMutexHandle != NULL)
		)
	{
		osMutexRelease(I2CMutexHandle);
	}
	
	return RetVal;
}

/*
 * @brief  Write generic LIS2DE12 register
 *
 * @param  handle    customizable argument. In this examples is used in
 *                   order to select the correct sensor bus handler.
 * @param  reg       register to write
 * @param  bufp      pointer to data to write in register reg
 * @param  len       number of consecutive register to write
 *
 * @retval 0: OK, otherwise FAILED (error code)
 *
 */
static int32_t LIS2DE12_write(void *handle, uint8_t reg, uint8_t *bufp,
                              uint16_t len)
{
	int32_t RetVal = -1;
	unsigned char data[2];	// device select byte + first register address
	unsigned char esito;
	
	if( handle != NULL )
	{
		if( osKernelRunning()
			&& (I2CMutexHandle != NULL)
			)
		{
			if( osMutexWait(I2CMutexHandle, 0 ) != osOK )
			{
				return RetVal;
			}
		}

		data[0] = LIS2DE12_I2C_ADDRESS;
		data[1] = reg;
		Wdog();
		esito = i2cWriteBuffer( data, 2, bufp, len );
		Wdog();

		if( esito == I2C_OK )
		{
			RetVal = 0;
		}
		
		if( osKernelRunning()
			&& (I2CMutexHandle != NULL)
			)
		{
			osMutexRelease(I2CMutexHandle);
		}
	}
	
	return RetVal;
}

/*
 * @brief  Read generic LIS2DE12 register
 *
 * @param  handle    customizable argument. In this examples is used in
 *                   order to select the correct sensor bus handler.
 * @param  reg       register to read
 * @param  bufp      pointer to buffer that store the data read
 * @param  len       number of consecutive register to read
 *
 * @retval 0: OK, otherwise FAILED (error code)
 *
 */
static int32_t LIS2DE12_read(void *handle, uint8_t reg, uint8_t *bufp,
                             uint16_t len)
{
	int32_t RetVal = -1;
	unsigned char data[2];	// device select byte + first register address
	unsigned char esito;
	
	if( handle != NULL )
	{
		if( osKernelRunning()
			&& (I2CMutexHandle != NULL)
			)
		{
			if( osMutexWait(I2CMutexHandle, 0 ) != osOK )
			{
				return RetVal;
			}
		}

		data[0] = LIS2DE12_I2C_ADDRESS;
		data[1] = reg;
		Wdog();
		esito = i2cWriteReadBuffer( data, 2, bufp, len );
		Wdog();

		if( esito == I2C_OK )
		{
			RetVal = 0;
		}
		
		if( osKernelRunning()
			&& (I2CMutexHandle != NULL)
			)
		{
			osMutexRelease(I2CMutexHandle);
		}
	}
	
	return RetVal;
}


/*! \fn void inpInit( void )
    \brief Inizializza gestore ingressi
*/
void inpInit( void )
{
	BATT_STRUCT *pt;
	uint8_t b;
	
	adcInit();

	DIGIN = 0;
	filtPuls = 0;
	filtAlim = 0;
	counter = 0;
	TAcounter = T_TAMB_SAMPLE - 2;	// forza una nuova acquisizione della temperatura ambiente
	VBATTcounter = 10;	//20;	// forza un aggiornamento della tensione batteria dopo un ciclo sufficiente di filtraggio analogico
	
	CntSenseBat = 0;
	CntChargeBat = 0;
	BattUp = BATTERY_DISCHARGE;
	/*
		Inizializza le misure con i valori nominali,
		per compensare l'azione del filtraggio sulle prime letture
		che altrimenti fornirebbero un valore molto basso.
	*/
	TempAmb = TEMP_AMB_NOM;
	TAlast = TEMP_AMB_NOM;
	pt = &VBatt_tab;
	VBatt = pt->VBattNom;
	Anain.VBatt = pt->VBattADNom;
	PercVBatt = 1000;
	Anain.VrefInt = VREFINT_ANIN_NOM;
	
	// valore VREFINT calibrato
	VrefInt = VREFINT_NOM( VREFINT_CAL );
#ifdef MONITOR_ADC
	myprintf( "VREFINT: %4u - %u.%03uV\r\n",
				VREFINT_CAL,
				(VrefInt/1000),
				(VrefInt%1000)
			);
#endif

	inpStat = 0;
	
	// sensore MCP9800 in shutdown mode
	cfgMCP9800.b = 0x00;	// CONFIG default value
	cfgMCP9800.bit.Shutdown = 1;	// enable shutdown
	cfgMCP9800.bit.Resolution = 1;	// 10-bit resolution (0.25°C, TAconv=60ms)
	MCP9800_SetConfig( cfgMCP9800 );

	// Initialize LIS2DE12 driver interface
	dev_ctx.write_reg = LIS2DE12_write;
	dev_ctx.read_reg = LIS2DE12_read;
	dev_ctx.handle = &hi2c1; 

	// Check LIS2DE12 ID
	b = ~((uint8_t)LIS2DE12_ID);
	lis2de12_device_id_get(&dev_ctx, &b);
	if (b == LIS2DE12_ID)
	{
		// config LIS2DE12 
		/*!
			Rif.: AN5005
			Inertial wake-up feature performed on high-pass filtered data.
			The device is configured to recognize when the high-frequency
			component of the acceleration applied along either the X, Y, or Z-axis
			exceeds a preset threshold.
			The event which triggers the interrupt is latched inside the device 
			and its occurrence is signaled through the use of the INTx pin.
		**/
		// Turn on the sensor, enable X, Y, and Z, set Output Data Rate to 1Hz
		b = 0x1F;
		lis2de12_write_reg( &dev_ctx, LIS2DE12_CTRL_REG1, &b, 1 );
		/* // High-pass filter enabled on interrupt activity 1
		b = 0x09; */
		// High-pass filter enabled on interrupt activity 2
		b = 0x0A;
		lis2de12_write_reg( &dev_ctx, LIS2DE12_CTRL_REG2, &b, 1 );
		/* // Interrupt activity IA1 driven to INT1 pad
		b = 0x40; */
		// No interrupt driven to INT1 pad
		b = 0x00;
		lis2de12_write_reg( &dev_ctx, LIS2DE12_CTRL_REG3, &b, 1 );
		// BDU enabled, FS = ±2g
		b = 0x80;
		lis2de12_write_reg( &dev_ctx, LIS2DE12_CTRL_REG4, &b, 1 );
		/* // Interrupt 1 pin latched
		b = 0x08; */
		// Interrupt 2 pin latched
		b = 0x02;
		lis2de12_write_reg( &dev_ctx, LIS2DE12_CTRL_REG5, &b, 1 );
		/* // Interrupt activity IA1 driven to INT2 pad
		b = 0x40; */
		// Interrupt activity IA2 driven to INT2 pad
		b = 0x20;
		lis2de12_write_reg( &dev_ctx, LIS2DE12_CTRL_REG6, &b, 1 );
		// Threshold = 256mg
		b = 0x10;
		//lis2de12_write_reg( &dev_ctx, LIS2DE12_INT1_THS, &b, 1 );
		lis2de12_write_reg( &dev_ctx, LIS2DE12_INT2_THS, &b, 1 );
		// Duration = 0
		b = 0x00;
		//lis2de12_write_reg( &dev_ctx, LIS2DE12_INT1_DURATION, &b, 1 );
		lis2de12_write_reg( &dev_ctx, LIS2DE12_INT2_DURATION, &b, 1 );
		// Dummy read to force the HP filter to current acceleration value
		// (i.e. set reference acceleration/tilt value)
		lis2de12_filter_reference_get( &dev_ctx, &b );
		// Configure desired wake-up event
		b = 0x2A;
		//lis2de12_write_reg( &dev_ctx, LIS2DE12_INT1_CFG, &b, 1 );
		lis2de12_write_reg( &dev_ctx, LIS2DE12_INT2_CFG, &b, 1 );
		
		__HAL_GPIO_EXTI_CLEAR_IT(INT1_ACC_Pin);
		//HAL_NVIC_EnableIRQ(INT1_ACC_EXTI_IRQn);	// enable interrupt from INT1_ACC
		__HAL_GPIO_EXTI_CLEAR_IT(INT2_ACC_Pin);
		HAL_NVIC_EnableIRQ(INT2_ACC_EXTI_IRQn);	// enable interrupt from INT2_ACC
	}
	else
	{
		// disabilita ogni ulteriore accesso a LIS2DE12
		dev_ctx.handle = NULL;
	}
	
}

/*! \fn void inpHandler( void )
    \brief Gestore ingressi
*/
void inpHandler( void )
{
	BATT_STRUCT *pt = &VBatt_tab;
	unsigned long d;
	
	switch( inpStat )
	{
		case 0:
			// check for start ADC scanning conversion every 5ms
			if( checktimer( T_ADC ) != INCORSO )
			{
				starttimer( T_ADC, 5 );
				
				// campionamento ingressi digitali
				filtPuls <<= 1;
				if( HAL_GPIO_ReadPin( PULS_GPIO_Port, PULS_Pin ) == GPIO_PIN_SET )
				{
					filtPuls |= BIT0;
				}
				if( filtPuls == 0xFFFF )
				{
					// pulsante premuto
					DIGIN_PULS = 1;
				}
				else if( filtPuls == 0x00 )
				{
					// pulsante rilasciato
					DIGIN_PULS = 0;
					PULS_LOCK = 0;
				}
				filtAlim <<= 1;
				if( HAL_GPIO_ReadPin( PRES_RETE_GPIO_Port, PRES_RETE_Pin ) == GPIO_PIN_SET )
				{
					filtAlim |= BIT0;
				}
				if( filtAlim == 0xFF )
				{
					// alimentazione esterna presente
					DIGIN_ALIM = 1;
				}
				else if( filtAlim == 0x00 )
				{
					// alimentazione esterna assente
					DIGIN_ALIM = 0;
				}
				
				// gestione ingressi ogni 100ms
				if( ++counter >= 20 )
				{
					counter = 0;

					/*! TEMPERATURA AMBIENTE */
					if( ++TAcounter >= T_TAMB_SAMPLE )	// TA acquisita in 60ms ogni (T_TAMB_SAMPLE x 0.1s)
					{
						TAcounter = 0;
						// decodifica temperatura ambiente
						cfgMCP9800.bit.OneShot = 0;	// disable one-shot conversion
						if( GetTempAmb() )
						{
							TempAmb += COMP_TAMB;	// offset compensazione
							// se la misura e' fuori del range -30/+80°C, imposto il valore 100°C come segnalazione di errore
					#if defined( TEMP_AMB_PAN_0_1_DEG )
							// codifico temperatura ambiente in step di 0.1°C per la stufa
							if( (TempAmb < -3000) || (TempAmb > 8000) )
								TEMP_AMB_PAN = 1000;
							else if( TempAmb >= 0 )
								TEMP_AMB_PAN = (TempAmb + 5)/10;
							else
								TEMP_AMB_PAN = (TempAmb - 5)/10;
					#elif defined( TEMP_AMB_PAN_0_5_DEG )
							// codifico temperatura ambiente in step di 0.5°C per la stufa
							if( (TempAmb < -3000) || (TempAmb > 8000) )
								TEMP_AMB_PAN = 200;
							if( TempAmb >= 0 )
								TEMP_AMB_PAN = (TempAmb + 25)/50;
							else
								TEMP_AMB_PAN = (TempAmb - 25)/50;
					#else
							#error Codifica temperatura ambiente non definita.
					#endif
						}
					}
					else if( TAcounter == (T_TAMB_SAMPLE - 1) )
					{
						// avvia una nuova conversione TA
						//cfgMCP9800.bit.Shutdown = 1;	// enable shutdown
						//cfgMCP9800.bit.Resolution = 1;	// 10-bit resolution (0.25°C, TAconv=60ms)
						cfgMCP9800.bit.OneShot = 1;	// start a new one-shot conversion
						MCP9800_SetConfig( cfgMCP9800 );
					}

					/*! TENSIONE BATTERIA */
					if( VBATTcounter )
					{
						VBATTcounter--;
					}
					if( VBATTcounter == 0 )
					{
						if( DIGIN_ALIM )
						{
							pt->VBattOff = LITIO_VBATT_OFF_ALIM;
							// riavvia timer aggiornamento batteria in carica
							VBATTcounter = T_VBATT_SAMPLE_ALIM;
						}
						else
						{
							pt->VBattOff = LITIO_VBATT_OFF;
							// riavvia timer aggiornamento batteria in scarica
							VBATTcounter = T_VBATT_SAMPLE;
						}
						/*
							La tensione viene ridotta da un partitore di un fattore 1/2 e poi
							acquisita dall'ADC 12-bit con VREF = 3V.

							Tensione (mV)	= 1000 * vADC * (VREF/4095) * (2) =
													= (vADC * 3000 * 2) / (4095) =
													= (vADC * 6000) / 4095

							La VREF e' variabile, percio' si rapporta il valore acquisito
							con il valore acquisito della VREFINT.

							Vbatt = 2*(VREF/4095) * vADC1
							VREFINT = (VREF/4095) * vADC2
							------------------------------
							Vbatt = (2 * vADC1 * VREFINT) / vADC2	indipendente dalla VREF
						*/
						adcVRefInt = Anain.VrefInt / ACCU_NUM_LETT;
						d = Anain.VBatt / ACCU_NUM_LETT;
						adcVBatt = d;
						d *= 20;	// calcolo in 0.1mV
						d *= VrefInt;
						d /= adcVRefInt;
						VBatt = (d + 5)/10;	// arrotondo a mV
						// percentuale di carica batteria
						if( VBatt > pt->VBattOff )
						{
							d = 10000*(VBatt - pt->VBattOff);	// calcolo in 0.01%
							/* if( DIGIN_ALIM
									//&& (BattUp == BATTERY_CHARGE)
								)
							{
								// dinamica tensione in carica
								d /= (pt->VBattFull - pt->VBattOff);
							}
							else
							{
								// dinamica tensione in scarica
								d /= (pt->VBattNom - pt->VBattOff);
							} */
							d /= (pt->VBattFull - pt->VBattOff);
							if( d > 10000 )
							{
								d = 10000;
							}
						}
						else
						{
							d = 0;
						}
						PercVBatt = (d + 5)/10;	// arrotondo a 0.1%
					}
				}

				ELAB_INP = 0;
				adcStart();
				inpStat = 1;
			}
		break;

		default:
		case 1:
			// a fine conversione disabilita DMA per ADC
			if( ELAB_INP )
			{
				ELAB_INP = 0;
				HAL_ADC_Stop_DMA( &hadc1 );
				inpStat = 0;
			}
		break;
		
		case 200:
			// sospensione
		break;
	}
}

/*! \fn void inpMonitor( void )
    \brief Monitor degli ingressi acquisiti
*/
void inpMonitor( void )
{
#ifdef MONITOR_ADC
	char s[30];
    /* lis2de12_reg_t reg;
    axis3bit16_t data_raw_acceleration;
    float acceleration_mg[3];	// mg
    short acceleration_g[3];	// 0.01g
    int i; */

	myprintf( "\r\n\nTamb: %c%d.%02u C (offset: %c%d.%02u C)\r\n",
                ((-100<TempAmb)&&(TempAmb<0))?'-':' ',
				(TempAmb/100),
				(abs(TempAmb)%100),
                ((-100<COMP_TAMB)&&(COMP_TAMB<0))?'-':' ',
				(COMP_TAMB/100),
				(abs(COMP_TAMB)%100)
			);
	myprintf( "Batt: %4u - %u.%03uV [%u.%01u%% - in %s] OFF=%u.%03uV\r\n",
				adcVBatt,
				(VBatt/1000),
				(VBatt%1000),
				(PercVBatt/10),
				(PercVBatt%10),
				(BattUp == BATTERY_CHARGE) ? "charge" : ((BattUp == BATTERY_DISCHARGE) ? "discharge" : "charge error"),
				(VBatt_tab.VBattOff/1000),
				(VBatt_tab.VBattOff%1000)
			);
	ScriviData( s );
	ScriviOra( &s[12] );
	myprintf("Data-Ora: %s %s [%u]\r\n", s, &s[12], SecCounter);

    /* if( !lis2de12_xl_data_ready_get( &dev_ctx, &reg.byte ) )
    {
		if (reg.byte)
		{
			// Read accelerometer data
			memset(data_raw_acceleration.u8bit, 0x00, 3*sizeof(int16_t));
			if( !lis2de12_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit) )
			{
				for( i=0; i<3; i++ )
				{
					acceleration_mg[i] = lis2de12_from_fs2_to_mg(data_raw_acceleration.i16bit[i]);
					if( acceleration_mg[i] < 0 )
					{
						acceleration_g[i] = (short)(acceleration_mg[i] - 5)/10;
					}
					else
					{
						acceleration_g[i] = (short)(acceleration_mg[i] + 5)/10;
					}
				}
				myprintf( "Acceleration [g]: X=%c%d.%02u  Y=%c%d.%02u  Z=%c%d.%02u\r\n\n",
						((-100<acceleration_g[0])&&(acceleration_g[0]<0))?'-':' ',
						(acceleration_g[0]/100),
						(abs(acceleration_g[0])%100),
						((-100<acceleration_g[1])&&(acceleration_g[1]<0))?'-':' ',
						(acceleration_g[1]/100),
						(abs(acceleration_g[1])%100),
						((-100<acceleration_g[2])&&(acceleration_g[2]<0))?'-':' ',
						(acceleration_g[2]/100),
						(abs(acceleration_g[2])%100)
						);
			}
		}
    } */
#endif	// MONITOR_ADC
}

/*!
 * \fn void DecodSenseCharge( void )
 * \brief Rileva lo stato di ricarica della batteria
 * \note La funzione presuppone di essere chiamata ogni 50ms
 */
void DecodSenseCharge(void)
{
	CntSenseBat ++;
	// Conto quante volte leggo il segnale alto del caricatore.
	if (HAL_GPIO_ReadPin(CHARGE_STAT_GPIO_Port,CHARGE_STAT_Pin) == GPIO_PIN_SET)
		CntChargeBat ++;
	
	// Ogni secondo effettuo la decisione.	
	if (CntSenseBat >= CNT_BATTERY)
	{
		if (CntChargeBat > FILTRA_1BAT)
		{
			// Segnale alto: batteria in scarica.
			BattUp = BATTERY_DISCHARGE;
		}
		else if (CntChargeBat > FILTRA_0BAT)
		{
			// Segnale frequenza: errore di ricarica.
			BattUp = BATTERY_CHARGE_ERROR;
		}
		else
		{
			// Segnale basso: batteria in carica.
			BattUp = BATTERY_CHARGE;
		}
		
		CntChargeBat = 0;
		CntSenseBat = 0;
	}

}

/*!
** \fn short ExecTAConv( void )
** \brief Esegue una singola acquisizione TA con aggiornamento TempAmb
** \return 1: OK, 0: FAILED
** 
** \details Si considera RTOS sospeso e I2C mutex non definito
**/
short ExecTAConv( void )
{
	short RetVal = 0;
	unsigned short timeout = 0;
	
	// avvia una nuova conversione TA
	//cfgMCP9800.bit.Shutdown = 1;	// enable shutdown
	//cfgMCP9800.bit.Resolution = 1;	// 10-bit resolution (0.25°C, TAconv=60ms)
	cfgMCP9800.bit.OneShot = 1;	// start a new one-shot conversion
	MCP9800_SetConfig( cfgMCP9800 );

	// attesa fine conversione
	while( cfgMCP9800.bit.OneShot )
	{
		DWT_Delay_us( 15000 );	// check every 15ms (SysTick may be suspended too)
		if( !MCP9800_GetConfig( &cfgMCP9800 ) )
		{
			return RetVal;
		}
		if( cfgMCP9800.bit.OneShot
			&& (++timeout >= 20)	// wait max 300ms
			)
		{
			return RetVal;
		}
	}
	
	// decodifica temperatura ambiente
	if( GetTempAmb() )
	{
		RetVal = 1;
		
		TempAmb += COMP_TAMB;	// offset compensazione
		// se la misura e' fuori del range -30/+80°C, imposto il valore 100°C come segnalazione di errore
	#if defined( TEMP_AMB_PAN_0_1_DEG )
		// codifico temperatura ambiente in step di 0.1°C per la stufa
		if( (TempAmb < -3000) || (TempAmb > 8000) )
			TEMP_AMB_PAN = 1000;
		else if( TempAmb >= 0 )
			TEMP_AMB_PAN = (TempAmb + 5)/10;
		else
			TEMP_AMB_PAN = (TempAmb - 5)/10;
	#elif defined( TEMP_AMB_PAN_0_5_DEG )
		// codifico temperatura ambiente in step di 0.5°C per la stufa
		if( (TempAmb < -3000) || (TempAmb > 8000) )
			TEMP_AMB_PAN = 200;
		if( TempAmb >= 0 )
			TEMP_AMB_PAN = (TempAmb + 25)/50;
		else
			TEMP_AMB_PAN = (TempAmb - 25)/50;
	#else
		#error Codifica temperatura ambiente non definita.
	#endif
	}
	
	return RetVal;
}

/*!
 * \fn void INT1_Acc_irq( void )
 * \brief Gestione INT1_ACC interrupt
 * \return None
 */
void INT1_Acc_irq( void )
{
	unsigned char b;
	lis2de12_int1_src_t int1_src;
	
	// Return the event that has triggered the interrupt and clear interrupt
	int1_src.ia = 0;
	lis2de12_int1_gen_source_get( &dev_ctx, &int1_src );
	if( int1_src.ia )
	{
		// avvenuto interrupt IA
	}
	// Configure desired wake-up event
	b = 0x2A;
	lis2de12_write_reg( &dev_ctx, LIS2DE12_INT1_CFG, &b, 1 );
	
}

/*!
 * \fn void INT2_Acc_irq( void )
 * \brief Gestione INT1_ACC interrupt
 * \return None
 */
void INT2_Acc_irq( void )
{
	unsigned char b;
	lis2de12_int2_src_t int2_src;
	
	// Return the event that has triggered the interrupt and clear interrupt
	int2_src.ia = 0;
	lis2de12_int2_gen_source_get( &dev_ctx, &int2_src );
	if( int2_src.ia )
	{
		// avvenuto interrupt IA
	}
	// Configure desired wake-up event
	b = 0x2A;
	lis2de12_write_reg( &dev_ctx, LIS2DE12_INT2_CFG, &b, 1 );
	
}

/*!
 * \fn void inpRestart( void )
 * \brief Riavvia il gestore ingressi
 * \return None
 */
void inpRestart( void )
{
	adcInit();
	
	counter = 0;
	TAcounter = T_TAMB_SAMPLE - 2;	// forza una nuova acquisizione della temperatura ambiente
	VBATTcounter = 10;	//20;	// forza un aggiornamento della tensione batteria dopo un ciclo sufficiente di filtraggio analogico
	ELAB_INP = 0;
	HAL_ADC_Stop_DMA( &hadc1 );
	HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
	
	// allinea filtri ingressi sull'ultimo stato acquisito
	/*if( DIGIN_PULS )
	{
		filtPuls = 0xFFFF;
	}
	else
	{
		filtPuls = 0x00;
	}*/
	filtPuls = 0x0001;	// forza un filtraggio completo del pulsante per la successiva decisione sul suo stato
	if( DIGIN_ALIM )
	{
		filtAlim = 0xFF;
	}
	else
	{
		filtAlim = 0x00;
	}
	
	inpStat = 0;
	starttimer( T_ADC, 25 );	// compensa l'eventuale transitorio sul power-on VBATT OPA
}

/*!
 * \fn void inpSuspend( void )
 * \brief Sospende il gestore ingressi
 * \return None
 */
void inpSuspend( void )
{
	inpStat = 200;
    HAL_NVIC_DisableIRQ(DMA2_Stream0_IRQn);
	ELAB_INP = 0;
	HAL_ADC_Stop_DMA( &hadc1 );
	// power off VBATT OPA
	HAL_GPIO_WritePin(EN_V_BATT_GPIO_Port, EN_V_BATT_Pin, GPIO_PIN_RESET);
}




