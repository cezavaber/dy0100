/*!
** \file userMenu.c
** \author Alessandro Vagniluca
** \date 26/11/2015
** \brief User Interface USER_STATE_MENU state handler
** 
** \version 1.0
**/

#include "user.h"
#include "User_Parametri.h"
#include "userMenu.h"
#include "userAvvio.h"
#include "userParamCode.h"
#include "userInit.h"
#include "userAllarmi.h"
#include "userCrono.h"


unsigned char iLivMenu;	//!< livello corrente di menu`
unsigned char iVoceMenu[NLIVMNENU];	//!< indice item di menu`
unsigned char iPageMenu[NLIVMNENU];	//!< indice pagina di menu`
char strTab[NUM_MAX_ITEM+4][DISPLAY_MAX_COL+1];	//!< stringhe voci di menu`
unsigned char maxLen;	//!< max lunghezza stringa lunga valore parametro

unsigned char IndexVisItem[NUM_MAX_ITEM];	// indici in tabella EXTREC_STRUCT delle voci visualizzate

char sDescPar[NDIGIT+1];	// codice descrizione parametro corrente


/*!
** \enum TouchTags
** \brief The used touch tags values
**/
enum TouchTags
{
	TAG_VERT_SCROLL = 0,
};



// Menu events definitions
#define MENU_EVENT_IDLE    0xFF	//!< Idle. Nothing to report.
#define MENU_EVENT_EXIT    0xFE	//!< Exit. User has pressed the back button.
// Menu Keyboard codes
#define MENU_KEYCODE_DOWN  40		//!< Down
#define MENU_KEYCODE_UP    38		//!< Up
#define MENU_KEYCODE_BACK  8		//!< Back/exit
#define MENU_KEYCODE_ENTER 13		//!< Enter/select

// struttura di descrizione menu`
enum
{
	MENU_TYPE_LIST,				//!< lista semplice
	MENU_TYPE_USER_PARAM,		//!< lista parametri utente
	NUM_MENU_TYPE
};
typedef struct 
{
	unsigned char MenuType;				//!< tipo menu`
	const char *title;						//!< stringa del titolo
	const EXTREC_STRUCT *ExtRec;		//!< tabella dei dati di gestione degli elementi
	unsigned char num_elements;		//!< numero degli elementi
	unsigned char current_selection;	//!< indice del primo elemento visualizzato
	unsigned char current_page;		//!< indice della pagina corrente
} SETUP_MENU;
static SETUP_MENU Setup_Menu[NLIVMNENU];

static unsigned char nMenuPage;	// numero di pagine per menu` 
// numero di voci menu` per pagina
static const unsigned char nItemPage[NUM_MENU_TYPE] = 
{
	5,	// lista semplice di stringhe
	5,	// lista parametri utente (descrizione + valore)
};
// stringhe titolo menu` (di max DISPLAY_MAX_COL caratteri!!!)
static char sMenuTitle[NLIVMNENU][DISPLAY_MAX_COL+1];

unsigned char LivAccesso;	//!< livello di accesso al menu`

static unsigned char iItemReq;	//!< indice item per richieste valore/limiti


static ft_uint16_t m_tagval;	//!< tag value
BM_HEADER * m_bmhdrQUIT = NULL;	//!< QUIT bitmap header pointer
BM_HEADER * m_bmhdrCHECKED = NULL;	//!< CHECKED bitmap header pointer
BM_HEADER * m_bmhdrUNCHECKED = NULL;	//!< UNCHECKED bitmap  header pointer
static unsigned char m_scrollON;	//!< scrolling status
static unsigned long m_key_cnt;	//!< key counter
static unsigned short m_preYscroll;	//!< previous Y value for vertical scrolling
static short m_redraw;	//!< menu redraw 

// Macros and variables for WinSetString() funcrion
// {
#define ON          1
#define OFF         0						 
#define Font        27					// Font Size for string input
#define FontNumber  29					// Font Size for number input
#define MAX_LINES   4					// Max Lines allows to Display

#define NUM_SIGN			27				// sign of number
#define BACK_SPACE		28				// Backspace
#define CAPS_LOCK			29				// Caps Lock
#define NUMBER_LOCK		30				// Number Lock
#define CLEARALL			31				// Clear
#define SPECIAL_FUN     (NUM_SIGN)

#define LINE_STARTPOS	FT_DispWidth/50			// Start of Line
#define LINE_ENDPOS	FT_DispWidth			// max length of the line

static struct Notepad_buffer
{
	ft_char8_t *temp;
	ft_char8_t  notepad[MAX_LINES][80];
} Buffer;

static const char keyChar[3][26] =
{
	{ 'Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M' },
	{ 'q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m' },
	{ '1','2','3','4','5','6','7','8','9','0','-','@','#','$','%','^','&','*','(',')','_','+','[',']','{','}' },
};
// }


static short GetRealParCode( short code );
static unsigned char userVisHidPass( void );
static unsigned char ConfTipoDef( void );
static unsigned char ConfBypass( void );
static unsigned char ConfBypassTra( void );
static unsigned char ConfTaraTC( void );
static unsigned char ConfOreSAT( void );
static void VisMenu( unsigned char MenuType, const char *Title,
							const EXTREC_STRUCT *ExtRec, unsigned char nItem );
static void setup_menu_init( SETUP_MENU *menu );
//static short setup_menu_process_key( SETUP_MENU *menu, unsigned char keycode );
static void setup_menu_draw( SETUP_MENU *menu, short redraw );
static short EnterMenu( unsigned char MenuType, unsigned short iStrTitle,
								const EXTREC_STRUCT *ExtRec, unsigned char nItem );
static void RetMenu( unsigned char MenuType,
							const EXTREC_STRUCT *ExtRec, unsigned char nItem );
static unsigned char RetMainMenu( void );
static unsigned char EnterMenuSettings( void );
static unsigned char RetMenuSettings( void );
static unsigned char EnterMenuRicette( void );
static unsigned char EnterMenuTec( void );
static unsigned char EnterMenuSysCfg( void );
static unsigned char RetMenuSysCfg( void );
static unsigned char EnterMenuCtrl( void );
static unsigned char RetMenuCtrl( void );
static unsigned char EnterMenuAttuaTra( void );
static unsigned char RetMenuAttuaTra( void );
static unsigned char EnterMenuAttuaPot( void );
static unsigned char RetMenuAttuaPot( void );
static unsigned char EnterMenuAlarms( void );
static unsigned char RetMenuAlarms( void );
static unsigned char EnterMenuTest( void );
static unsigned char RetMenuTest( void );
static unsigned char EnterMenuGen( void );
//static unsigned char RetMenuGen( void );
static unsigned char EnterMenuBlkout( void );
//static unsigned char RetMenuBlkout( void );
static unsigned char EnterMenuCoclea( void );
//static unsigned char RetMenuCoclea( void );
static unsigned char EnterMenuScuot( void );
//static unsigned char RetMenuScuot( void );
static unsigned char EnterMenuFanAmb( void );
//static unsigned char RetMenuFanAmb( void );
static unsigned char EnterMenuFan1( void );
//static unsigned char RetMenuFan1( void );
static unsigned char EnterMenuFan2( void );
//static unsigned char RetMenuFan2( void );
static unsigned char EnterMenuFan3( void );
//static unsigned char RetMenuFan3( void );
static unsigned char EnterMenuRicTra( void );
//static unsigned char RetMenuRicTra( void );
static unsigned char EnterMenuEco( void );
//static unsigned char RetMenuEco( void );
static unsigned char EnterMenuTFunz( void );
//static unsigned char RetMenuTFunz( void );
static unsigned char EnterMenuIdro( void );
//static unsigned char RetMenuIdro( void );
static unsigned char EnterMenuParTra( void );
//static unsigned char RetMenuParTra( void );
static unsigned char EnterMenuPreAcc1( void );
//static unsigned char RetMenuPreAcc1( void );
static unsigned char EnterMenuPreAcc2( void );
//static unsigned char RetMenuPreAcc2( void );
static unsigned char EnterMenuPreAccHot( void );
//static unsigned char RetMenuPreAccHot( void );
static unsigned char EnterMenuAccA( void );
//static unsigned char RetMenuAccA( void );
static unsigned char EnterMenuAccB( void );
//static unsigned char RetMenuAccB( void );
static unsigned char EnterMenuFireONA( void );
//static unsigned char RetMenuFireONA( void );
static unsigned char EnterMenuSpeA( void );
//static unsigned char RetMenuSpeA( void );
static unsigned char EnterMenuSpeB( void );
//static unsigned char RetMenuSpeB( void );
static unsigned char EnterMenuRaffA( void );
//static unsigned char RetMenuRaffA( void );
static unsigned char EnterMenuParPot( void );
//static unsigned char RetMenuParPot( void );
static unsigned char EnterMenuPB( void );
//static unsigned char RetMenuPB( void );
static unsigned char EnterMenuRitEsp( void );
//static unsigned char RetMenuRitEsp( void );
static unsigned char EnterMenuRitPot( void );
//static unsigned char RetMenuRitPot( void );
static unsigned char EnterMenuPot1( void );
//static unsigned char RetMenuPot1( void );
static unsigned char EnterMenuPot2( void );
//static unsigned char RetMenuPot2( void );
static unsigned char EnterMenuPot3( void );
//static unsigned char RetMenuPot3( void );
static unsigned char EnterMenuPot4( void );
//static unsigned char RetMenuPot4( void );
static unsigned char EnterMenuPot5( void );
//static unsigned char RetMenuPot5( void );
static unsigned char EnterMenuPot6( void );
//static unsigned char RetMenuPot6( void );
static unsigned char EnterMenuPuliz( void );
//static unsigned char RetMenuPuliz( void );
static unsigned char EnterMenuInterp( void );
//static unsigned char RetMenuInterp( void );
static unsigned char EnterMenuAlmFumi( void );
//static unsigned char RetMenuAlmFumi( void );
static unsigned char EnterMenuSensPlt( void );
//static unsigned char RetMenuSensPlt( void );
//static unsigned char EnterMenuNoFiam( void );
//static unsigned char RetMenuNoFiam( void );
static unsigned char EnterMenuSensAria( void );
//static unsigned char RetMenuSensAria( void );
static unsigned char EnterMenuCmd( void );
//static unsigned char RetMenuCmd( void );
static unsigned char EnterMenuLoads( void );
//static unsigned char RetMenuLoads( void );
static unsigned char EnterMenuAttua( void );
//static unsigned char RetMenuAttua( void );
static unsigned char EnterVisParCod( void );


// --------------------------------------------------------------------------------
// Tabelle di gestione menu
//

// parametri per MENU ANOMALIE
enum warn
{
	WARN_ITEM_SERVICEREQ,
	WARN_ITEM_A14_SENS_PORTATA,
	WARN_ITEM_A15_FINE_PELLET,
	WARN_ITEM_PREALL_PORTA,
	WARN_ITEM_GUASTOSONDATEMPARIA,
	WARN_ITEM_GUASTOSONDATEMPACQUA,
	WARN_ITEM_GUASTOPRESSOSTACQUA,
	WARN_ITEM_SOVRAPRESSACQUA,
	NUMWARNITEM
};
static const EXTREC_STRUCT WarnItem[NUMWARNITEM] =
{
//  code,						IsToView,				VoceExtIndex,  PreFunc,		ExecFunc,    EscModFunc,	EscFunc,		wDispId						LivAccesso
	{P_SERVICEREQ,				IsServiceReq,			NULL,         /* NULL,*/	NULL,        NULL,        	userExitMenu,	RICHIESTASERVICE,			LOGIN_FREE },
	{P_GUASTO_SENS_PORTATA,		IsSensPortFail,			NULL,         /* NULL,*/	NULL,        NULL,        	userExitMenu,	GUASTO_SENSORE_PORTATA,		LOGIN_FREE },
	{P_FINE_PELLET,				IsFinePellet,			NULL,         /* NULL,*/	NULL,        NULL,        	userExitMenu,	PELLET_ESAURITO,			LOGIN_FREE },
	{P_PREALL_PORTA,			IsPortaAperta,			NULL,         /* NULL,*/	NULL,        NULL,        	userExitMenu,	PORTA_APERTA,				LOGIN_FREE },
	{P_GUASTOSONDATEMPARIA,		IsSensAriaFail,			NULL,         /* NULL,*/	NULL,        NULL,        	userExitMenu,	GUASTOSONDATEMPARIA,		LOGIN_FREE },
	{P_GUASTOSONDATEMPACQUA,	IsSensH2OFail,			NULL,         /* NULL,*/	NULL,        NULL,        	userExitMenu,	GUASTOSONDATEMPACQUA,		LOGIN_FREE },
	{P_GUASTOPRESSOSTACQUA,		IsPressostH2OFail,		NULL,         /* NULL,*/	NULL,        NULL,        	userExitMenu,	GUASTOPRESSOSTACQUA,		LOGIN_FREE },
	{P_SOVRAPRESSACQUA,			IsPressH2OAlarm,		NULL,         /* NULL,*/	NULL,        NULL,        	userExitMenu,	SOVRAPRESSACQUA,			LOGIN_FREE },
};

// parametri per MENU INFO
enum infos
{
	INFO_ITEM_CODICE_SCHEDA,
	INFO_ITEM_CODICE_SICUREZZA,
	INFO_ITEM_CODICE_PANNELLO,
	INFO_ITEM_CODICE_PARAMETRI,
	INFO_ITEM_OREFUNZIONAMENTO,
	INFO_ITEM_ORESAT,
	INFO_ITEM_SERVICE,
	INFO_ITEM_ATTUAZIONI,
	INFO_ITEM_VENTILATOREFUMI,
	INFO_ITEM_PORTATAARIAMISURATA,
	INFO_ITEM_OSSIGENO_RESIDUO,
	//INFO_ITEM_CONSUMO,
	INFO_ITEM_TEMPERATURAFUMI,
	INFO_ITEM_FOTORESISTENZA,
	INFO_ITEM_TEMPOCOCLEA,
	INFO_ITEM_GIRICOCLEA,
	INFO_ITEM_MOTORESCAMBIATORE,
	INFO_ITEM_MOTORESCAMBIATORE_2,
	INFO_ITEM_IDROFLOW,
	INFO_ITEM_NUM_ACC,
	INFO_ITEM_IPADDRESS,
	INFO_ITEM_STORICO_ALLARMI,
	NUMINFOITEM
};
static const EXTREC_STRUCT InfoItem[NUMINFOITEM] =
{
//  code,						IsToView,				VoceExtIndex,  PreFunc,		ExecFunc,	EscModFunc,		EscFunc,    	wDispId						LivAccesso
	{P_CODICE_SCHEDA,			NULL,					NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_CODICE_SCHEDA,			LOGIN_FREE },
	{P_CODICE_SICUREZZA,		NULL,					NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_CODICE_SICUREZZA,			LOGIN_FREE },
	{P_CODICE_PANNELLO,			NULL,					NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_CODICE_PANNELLO,			LOGIN_FREE },
	{P_CODICE_PARAM,			NULL,					NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_CODICE_PARAM,				LOGIN_FREE },
	{P_OREFUNZIONAMENTO,		NULL,					NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_OREFUNZIONAMENTO,			LOGIN_FREE },
	{P_ORESAT,					NULL,					NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_ORESAT,					LOGIN_FREE },
	{P_SERVICE,					NULL,					NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_SERVICE,					LOGIN_FREE },
	{P_STATO_ATTUA,				NULL,					NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_ATTUAZIONI,				LOGIN_FREE },
	{P_VENTILATOREFUMI,			NULL,					NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_VENTILATOREFUMI,			LOGIN_FREE },
	{P_PORTATAARIAMISURATA,		IsCtrlPortata,			NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_PORTATAARIAMISURATA,		LOGIN_FREE },
	{P_OSSIGENO_RESIDUO,		IsCtrlLambda,			NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_OSSIGENORESIDUO,			LOGIN_FREE },
	//{P_ACC_CONSUMO,				IsConsumo,				NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_CONSUMO,					LOGIN_FREE },
	{P_TEMPERATURAFUMI,			NULL,					NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_TEMPERATURAFUMI,			LOGIN_FREE },
	{P_FOTORESISTENZA,			IsFotoresistenza,		NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_FOTORESISTENZA,			LOGIN_FREE },
	{P_TEMPOCOCLEAON_SET,		IsTempoCoclea,			NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_TEMPOCOCLEA,				LOGIN_FREE },
	{P_GIRI2,					IsGiriCoclea,			NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_GIRICOCLEA,				LOGIN_FREE },
	{P_FAN1_SET,				IsFan1Info,				NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_MOTORESCAMBIATORE,		LOGIN_FREE },
	{P_FAN2_SET,				IsFan2Info,				NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_MOTORESCAMBIATORE_2,		LOGIN_FREE },
	{P_PRESSIONEIDRO,			IsPressIdro,			NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_PRESSIONEIDRO,			LOGIN_FREE },
	{P_NUM_ACC,					NULL,					NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_NUM_ACCENSIONI,			LOGIN_FREE },
	{P_WIFI_IP,					IsWiFi,					NULL,         /* NULL,*/	NULL,    	NULL,        	userExitMenu,	d_IP_ADDRESS,				LOGIN_FREE },
	{SPECIALF_TYPE,				IsAlmRec,				NULL,         /* NULL,*/	WinAlmRec,	NULL,        	userExitMenu,	d_STORICO_ALLARMI,			LOGIN_FREE },
};

// parametri per MENU NETWORK
enum network
{
	// NETWORK_ITEM_EN_WIFI,
	NETWORK_ITEM_SSID,
	NETWORK_ITEM_KEYWORD,
	// NETWORK_ITEM_TCP_PORT,
	// NETWORK_ITEM_WPS_PIN,
	NUMNETWORKITEM
};
static const EXTREC_STRUCT NetworkItem[NUMNETWORKITEM] =
{
//  code,							IsToView,		VoceExtIndex,		PreFunc,		ExecFunc,		EscModFunc,	EscFunc,    	wDispId					LivAccesso
	// {P_EN_WIFI,						NULL,			NULL,			 	/* NULL,*/		NULL,			NULL,       userExitMenu,	d_WIFI,					LOGIN_FREE },
	{SPECIAL_WIFI_SSID_TYPE, 		NULL,			NULL,          		/* NULL,*/		NULL,			NULL,		userExitMenu,	d_SSID,					LOGIN_FREE },
	{SPECIAL_WIFI_KEYW_TYPE, 		NULL,			NULL,			 	/* NULL,*/		NULL,			NULL,       userExitMenu,	d_KEYWORD,				LOGIN_FREE },
	// {SPECIAL_WIFI_PORT_TYPE,		NULL,			NULL,			 	/* NULL,*/		NULL,   		NULL,       userExitMenu,	d_TCP_PORT,				LOGIN_FREE },
	// {SPECIAL_WPS_PIN_TYPE, 		NULL,			NULL,			 	/* NULL,*/		NULL,			NULL,       userExitMenu,	d_WPS_PIN,				LOGIN_FREE },
};

// parametri per MENU RICETTE
enum 
{
	RICETTE_ITEM_PELLET,
	RICETTE_ITEM_ARIA,
	RICETTE_ITEM_OSSIGENO,
	NUMRICETTEITEM
};
static const EXTREC_STRUCT RicetteItem[NUMRICETTEITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,			EscFunc,			wDispId					LivAccesso
	{P_RICETTA_PELLET_P,	NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,	    		RetMenuSettings,	d_RICETTAPELLET_ON,		LOGIN_FREE },
	{P_RICETTA_ARIA_P,		IsCtrlAria,		NULL,			/* NULL,*/		NULL,	    	NULL,	    		RetMenuSettings,	d_OFFSETESPULSORE_ON,	LOGIN_FREE },
	{P_OSSIGENO,			IsCtrlLambda,	NULL,			/* NULL,*/		NULL,	    	NULL,	    		RetMenuSettings,	d_OSSIGENO,				LOGIN_FREE },
};

// parametri per MENU IMPOSTAZIONI
enum settings
{
	SETTINGS_ITEM_LINGUA,
	SETTINGS_ITEM_ECO,
	SETTINGS_ITEM_BACKLIGHT,
	//SETTINGS_ITEM_USER_COLOR_ID,
	//SETTINGS_ITEM_TONE,
	SETTINGS_ITEM_RFID,
	SETTINGS_ITEM_SHOWTEMP,
	SETTINGS_ITEM_RICETTE_MENU,
	SETTINGS_PRECARICAPELLET,
	SETTINGS_PULIZIA,
	SETTINGS_ATTIVA_POMPA,
	NUMSETTINGSITEM
};
static const EXTREC_STRUCT SettingsItem[NUMSETTINGSITEM] =
{
//  code,								IsToView,		 		VoceExtIndex,	PreFunc,		ExecFunc,        	EscModFunc,	EscFunc,    	wDispId					LivAccesso
	{P_LINGUA,							NULL,          			NULL,			/* NULL,*/		ExecUpdateLang,  	NULL,       RetMainMenu,	d_LINGUA,				LOGIN_FREE },
	{P_ECO,								NULL,         			NULL,			/* NULL,*/		NULL,	    		NULL,       RetMainMenu,	d_ECO,					LOGIN_FREE },
	{P_BACKLIGHT,						NULL,          			NULL,			/* NULL,*/		NULL,			   	NULL,       RetMainMenu,	d_BACKLIGHT,			LOGIN_FREE },
	//{P_USER_COLOR_ID,					NULL,          			NULL,			/* NULL,*/		NULL,			   	NULL,       RetMainMenu,	d_BACKLIGHT,			LOGIN_FREE },
	// {P_TONE,								NULL,          			NULL,			/* NULL,*/     NULL,			   	NULL,       RetMainMenu,	d_TONE,					LOGIN_FREE },
	{P_RFPANEL,							NULL,          			NULL,			/* NULL,*/     NULL,			   	NULL,       RetMainMenu,	d_RFPANEL,				LOGIN_FREE },
	{P_SHOWTEMP,	 					NULL,          			NULL,			 /* NULL,*/		ExecSetTempUM,   	NULL,		RetMainMenu,	d_SHOWTEMP,				LOGIN_FREE },
	{SUBMENU_P_TYPE,  					NULL,          			RicetteItem,	/* NULL,*/		EnterMenuRicette,	NULL,       RetMainMenu, 	sRICETTE_MENU,			LOGIN_FREE },
	{FUNCTION_ATTIVA_COCLEA_TYPE, 		IsToView_PreCarPellet,	NULL,			/* NULL,*/		NULL,			   	NULL,		RetMainMenu,	d_PRECARICAPELLET,		LOGIN_FREE },
	{FUNCTION_ATTIVA_ESP_TYPE, 			IsToView_Pulizia,		NULL,			/* NULL,*/		NULL,			   	NULL,       RetMainMenu,	d_PULIZIA,				LOGIN_FREE },
	{FUNCTION_ATTIVA_POMPA_TYPE, 		IsToView_AttivaPompa,	NULL,			/* NULL,*/		NULL,			   	NULL,		RetMainMenu,	d_ATTIVA_POMPA,			LOGIN_FREE },
};

#ifdef menu_CRONO

// parametri per MENU Crono parametri settimanali prg1 abilitazione giorni
enum cronoPrgGiorniSett
{
	CRONO_ITEM_SETTIM_WEEK_DOM,
	CRONO_ITEM_SETTIM_WEEK_LUN,
	CRONO_ITEM_SETTIM_WEEK_MAR,
	CRONO_ITEM_SETTIM_WEEK_MER,
	CRONO_ITEM_SETTIM_WEEK_GIO,
	CRONO_ITEM_SETTIM_WEEK_VEN,
	CRONO_ITEM_SETTIM_WEEK_SAB,
	NUMCRONOGIORNISETTITEM,
};
static const EXTREC_STRUCT CronoSettim1WeekItem[NUMCRONOGIORNISETTITEM] =
{
//  code,							IsToView,	 VoceExtIndex,	PreFunc,		ExecFunc,				EscModFunc,	EscFunc,			wDispId			LivAccesso
	{P_CRONO_P1_SETTIM_WEEK_DOM,	NULL,        NULL,			/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sDOMENICA,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_LUN,	NULL,        NULL,			/* NULL,*/		userPostSetCronoPar,	NULL,		RetMenuCronoPrg,	sLUNEDI,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_MAR,	NULL,        NULL,			/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sMARTEDI,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_MER,	NULL,        NULL,			/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sMERCOLEDI,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_GIO,	NULL,        NULL,			/* NULL,*/		userPostSetCronoPar,	NULL,		RetMenuCronoPrg,	sGIOVEDI,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_VEN,	NULL,        NULL,			/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sVENERDI,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_SAB,	NULL,        NULL,			/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sSABATO,		LOGIN_FREE },
};

// parametri per MENU Crono parametri settimanali prg1
enum cronoPrgParamSett
{
	CRONO_ITEM_PARAM_SETT_PRGABILITA, 
	CRONO_ITEM_PARAM_SETT_PRGSTART,
	CRONO_ITEM_PARAM_SETT_PRGSTOP,
	CRONO_ITEM_PARAM_SETT_PRGTEMPARIA,
	CRONO_ITEM_PARAM_SETT_PRGTEMPH2O,
	CRONO_ITEM_PARAM_SETT_PRGPOTENZA,
	CRONO_ITEM_PARAM_SETT_PRGGIORNISETT,
	NUMCRONOPARAMSETTITEM,
};
static const EXTREC_STRUCT CronoSettim1Item[NUMCRONOPARAMSETTITEM] =
{
//  code,			              	IsToView,	VoceExtIndex, 			PreFunc,			ExecFunc,					EscModFunc,			EscFunc,			wDispId			LivAccesso
	{P_CRONO_P1_SETTIM,          	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGABILITA,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_START,    	NULL,       NULL,			 		/* NULL,*/			userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGSTART,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_STOP,     	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGSTOP,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_TEMP_ARIA,	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGTEMPARIA,	LOGIN_FREE },
	{P_CRONO_P1_SETTIM_TEMP_H2O, 	userIsIdro,	NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGTEMPH2O,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_POTENZA,  	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGPOTENZA,		LOGIN_FREE },
	{SUBMENU_P_TYPE,       			NULL,       CronoSettim1WeekItem,	/* NULL,*/       	EnterMenuCronoPrgDay,		NULL,   			RetMenuCrono, 		PRGGIORNISETT,	LOGIN_FREE },
};                                                                                                                                                                 
                                                                                                                                                                   
// parametri per MENU Crono parametri settimanali prg2 abilitazione giorni
static const EXTREC_STRUCT CronoSettim2WeekItem[NUMCRONOGIORNISETTITEM] =
{
//  code,							IsToView,	 VoceExtIndex,		PreFunc,		ExecFunc,				EscModFunc,	EscFunc,			wDispId			LivAccesso
	{P_CRONO_P2_SETTIM_WEEK_DOM,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sDOMENICA,		LOGIN_FREE },
	{P_CRONO_P2_SETTIM_WEEK_LUN,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,		RetMenuCronoPrg,	sLUNEDI,		LOGIN_FREE },
	{P_CRONO_P2_SETTIM_WEEK_MAR,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sMARTEDI,		LOGIN_FREE },
	{P_CRONO_P2_SETTIM_WEEK_MER,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sMERCOLEDI,		LOGIN_FREE },
	{P_CRONO_P2_SETTIM_WEEK_GIO,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,		RetMenuCronoPrg,	sGIOVEDI,		LOGIN_FREE },
	{P_CRONO_P2_SETTIM_WEEK_VEN,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sVENERDI,		LOGIN_FREE },
	{P_CRONO_P2_SETTIM_WEEK_SAB,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sSABATO,		LOGIN_FREE },
};

// parametri per MENU Crono parametri settimanali prg2
static const EXTREC_STRUCT CronoSettim2Item[NUMCRONOPARAMSETTITEM] =
{
//  code,			              	IsToView,	VoceExtIndex, 			PreFunc,			ExecFunc,					EscModFunc,			EscFunc,			wDispId			LivAccesso
	{P_CRONO_P2_SETTIM,          	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGABILITA,		LOGIN_FREE },
	{P_CRONO_P2_SETTIM_START,    	NULL,       NULL,			 		/* NULL,*/			userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGSTART,		LOGIN_FREE },
	{P_CRONO_P2_SETTIM_STOP,     	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGSTOP,		LOGIN_FREE },
	{P_CRONO_P2_SETTIM_TEMP_ARIA,	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGTEMPARIA,	LOGIN_FREE },
	{P_CRONO_P2_SETTIM_TEMP_H2O, 	userIsIdro,	NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGTEMPH2O,		LOGIN_FREE },
	{P_CRONO_P2_SETTIM_POTENZA,  	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGPOTENZA,		LOGIN_FREE },
	{SUBMENU_P_TYPE,       			NULL,       CronoSettim2WeekItem,	/* NULL,*/       	EnterMenuCronoPrgDay,		NULL,   			RetMenuCrono, 		PRGGIORNISETT,	LOGIN_FREE },
};

// parametri per MENU Crono parametri settimanali prg3 abilitazione giorni
static const EXTREC_STRUCT CronoSettim3WeekItem[NUMCRONOGIORNISETTITEM] =
{
//  code,							IsToView,	 VoceExtIndex,		PreFunc,		ExecFunc,				EscModFunc,	EscFunc,			wDispId			LivAccesso
	{P_CRONO_P3_SETTIM_WEEK_DOM,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sDOMENICA,		LOGIN_FREE },
	{P_CRONO_P3_SETTIM_WEEK_LUN,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,		RetMenuCronoPrg,	sLUNEDI,		LOGIN_FREE },
	{P_CRONO_P3_SETTIM_WEEK_MAR,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sMARTEDI,		LOGIN_FREE },
	{P_CRONO_P3_SETTIM_WEEK_MER,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sMERCOLEDI,		LOGIN_FREE },
	{P_CRONO_P3_SETTIM_WEEK_GIO,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,		RetMenuCronoPrg,	sGIOVEDI,		LOGIN_FREE },
	{P_CRONO_P3_SETTIM_WEEK_VEN,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sVENERDI,		LOGIN_FREE },
	{P_CRONO_P3_SETTIM_WEEK_SAB,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sSABATO,		LOGIN_FREE },
};

// parametri per MENU Crono parametri settimanali prg3
static const EXTREC_STRUCT CronoSettim3Item[NUMCRONOPARAMSETTITEM] =
{
//  code,			              	IsToView,	VoceExtIndex, 			PreFunc,			ExecFunc,					EscModFunc,			EscFunc,			wDispId			LivAccesso
	{P_CRONO_P3_SETTIM,          	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGABILITA,		LOGIN_FREE },
	{P_CRONO_P3_SETTIM_START,    	NULL,       NULL,			 		/* NULL,*/			userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGSTART,		LOGIN_FREE },
	{P_CRONO_P3_SETTIM_STOP,     	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGSTOP,		LOGIN_FREE },
	{P_CRONO_P3_SETTIM_TEMP_ARIA,	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGTEMPARIA,	LOGIN_FREE },
	{P_CRONO_P3_SETTIM_TEMP_H2O, 	userIsIdro,	NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGTEMPH2O,		LOGIN_FREE },
	{P_CRONO_P3_SETTIM_POTENZA,  	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGPOTENZA,		LOGIN_FREE },
	{SUBMENU_P_TYPE,       			NULL,       CronoSettim3WeekItem,	/* NULL,*/       	EnterMenuCronoPrgDay,		NULL,   			RetMenuCrono, 		PRGGIORNISETT,	LOGIN_FREE },
};

// parametri per MENU Crono parametri settimanali prg4 abilitazione giorni
static const EXTREC_STRUCT CronoSettim4WeekItem[NUMCRONOGIORNISETTITEM] =
{
//  code,							IsToView,	 VoceExtIndex,		PreFunc,		ExecFunc,				EscModFunc,	EscFunc,			wDispId			LivAccesso
	{P_CRONO_P4_SETTIM_WEEK_DOM,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sDOMENICA,		LOGIN_FREE },
	{P_CRONO_P4_SETTIM_WEEK_LUN,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,		RetMenuCronoPrg,	sLUNEDI,		LOGIN_FREE },
	{P_CRONO_P4_SETTIM_WEEK_MAR,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sMARTEDI,		LOGIN_FREE },
	{P_CRONO_P4_SETTIM_WEEK_MER,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sMERCOLEDI,		LOGIN_FREE },
	{P_CRONO_P4_SETTIM_WEEK_GIO,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,		RetMenuCronoPrg,	sGIOVEDI,		LOGIN_FREE },
	{P_CRONO_P4_SETTIM_WEEK_VEN,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sVENERDI,		LOGIN_FREE },
	{P_CRONO_P4_SETTIM_WEEK_SAB,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sSABATO,		LOGIN_FREE },
};

// parametri per MENU Crono parametri settimanali prg4
static const EXTREC_STRUCT CronoSettim4Item[NUMCRONOPARAMSETTITEM] =
{
//  code,			              	IsToView,	VoceExtIndex, 			PreFunc,			ExecFunc,					EscModFunc,			EscFunc,			wDispId			LivAccesso
	{P_CRONO_P4_SETTIM,          	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGABILITA,		LOGIN_FREE },
	{P_CRONO_P4_SETTIM_START,    	NULL,       NULL,			 		/* NULL,*/			userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGSTART,		LOGIN_FREE },
	{P_CRONO_P4_SETTIM_STOP,     	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGSTOP,		LOGIN_FREE },
	{P_CRONO_P4_SETTIM_TEMP_ARIA,	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGTEMPARIA,	LOGIN_FREE },
	{P_CRONO_P4_SETTIM_TEMP_H2O, 	userIsIdro,	NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGTEMPH2O,		LOGIN_FREE },
	{P_CRONO_P4_SETTIM_POTENZA,  	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGPOTENZA,		LOGIN_FREE },
	{SUBMENU_P_TYPE,       			NULL,       CronoSettim4WeekItem,	/* NULL,*/       	EnterMenuCronoPrgDay,		NULL,   			RetMenuCrono, 		PRGGIORNISETT,	LOGIN_FREE },
};

// parametri per MENU Crono parametri settimanali prg5 abilitazione giorni
static const EXTREC_STRUCT CronoSettim5WeekItem[NUMCRONOGIORNISETTITEM] =
{
//  code,							IsToView,	 VoceExtIndex,		PreFunc,		ExecFunc,				EscModFunc,	EscFunc,			wDispId			LivAccesso
	{P_CRONO_P5_SETTIM_WEEK_DOM,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sDOMENICA,		LOGIN_FREE },
	{P_CRONO_P5_SETTIM_WEEK_LUN,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,		RetMenuCronoPrg,	sLUNEDI,		LOGIN_FREE },
	{P_CRONO_P5_SETTIM_WEEK_MAR,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sMARTEDI,		LOGIN_FREE },
	{P_CRONO_P5_SETTIM_WEEK_MER,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sMERCOLEDI,		LOGIN_FREE },
	{P_CRONO_P5_SETTIM_WEEK_GIO,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,		RetMenuCronoPrg,	sGIOVEDI,		LOGIN_FREE },
	{P_CRONO_P5_SETTIM_WEEK_VEN,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sVENERDI,		LOGIN_FREE },
	{P_CRONO_P5_SETTIM_WEEK_SAB,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sSABATO,		LOGIN_FREE },
};

// parametri per MENU Crono parametri settimanali prg5
static const EXTREC_STRUCT CronoSettim5Item[NUMCRONOPARAMSETTITEM] =
{
//  code,			              	IsToView,	VoceExtIndex, 			PreFunc,			ExecFunc,					EscModFunc,			EscFunc,			wDispId			LivAccesso
	{P_CRONO_P5_SETTIM,          	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGABILITA,		LOGIN_FREE },
	{P_CRONO_P5_SETTIM_START,    	NULL,       NULL,			 		/* NULL,*/			userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGSTART,		LOGIN_FREE },
	{P_CRONO_P5_SETTIM_STOP,     	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGSTOP,		LOGIN_FREE },
	{P_CRONO_P5_SETTIM_TEMP_ARIA,	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGTEMPARIA,	LOGIN_FREE },
	{P_CRONO_P5_SETTIM_TEMP_H2O, 	userIsIdro,	NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGTEMPH2O,		LOGIN_FREE },
	{P_CRONO_P5_SETTIM_POTENZA,  	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGPOTENZA,		LOGIN_FREE },
	{SUBMENU_P_TYPE,       			NULL,       CronoSettim5WeekItem,	/* NULL,*/       	EnterMenuCronoPrgDay,		NULL,   			RetMenuCrono, 		PRGGIORNISETT,	LOGIN_FREE },
};

// parametri per MENU Crono parametri settimanali prg6 abilitazione giorni
static const EXTREC_STRUCT CronoSettim6WeekItem[NUMCRONOGIORNISETTITEM] =
{
//  code,							IsToView,	 VoceExtIndex, 		PreFunc,		ExecFunc,				EscModFunc,	EscFunc,			wDispId			LivAccesso
	{P_CRONO_P6_SETTIM_WEEK_DOM,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sDOMENICA,		LOGIN_FREE },
	{P_CRONO_P6_SETTIM_WEEK_LUN,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,		RetMenuCronoPrg,	sLUNEDI,		LOGIN_FREE },
	{P_CRONO_P6_SETTIM_WEEK_MAR,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sMARTEDI,		LOGIN_FREE },
	{P_CRONO_P6_SETTIM_WEEK_MER,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sMERCOLEDI,		LOGIN_FREE },
	{P_CRONO_P6_SETTIM_WEEK_GIO,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,		RetMenuCronoPrg,	sGIOVEDI,		LOGIN_FREE },
	{P_CRONO_P6_SETTIM_WEEK_VEN,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sVENERDI,		LOGIN_FREE },
	{P_CRONO_P6_SETTIM_WEEK_SAB,	NULL,        NULL,			 	/* NULL,*/		userPostSetCronoPar,	NULL,       RetMenuCronoPrg,	sSABATO,		LOGIN_FREE },
};

// parametri per MENU Crono parametri settimanali prg6
static const EXTREC_STRUCT CronoSettim6Item[NUMCRONOPARAMSETTITEM] =
{
//  code,			              	IsToView,	VoceExtIndex, 			PreFunc,			ExecFunc,					EscModFunc,			EscFunc,			wDispId			LivAccesso
	{P_CRONO_P6_SETTIM,          	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGABILITA,		LOGIN_FREE },
	{P_CRONO_P6_SETTIM_START,    	NULL,       NULL,			 		/* NULL,*/			userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGSTART,		LOGIN_FREE },
	{P_CRONO_P6_SETTIM_STOP,     	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,	  			RetMenuCrono, 		PRGSTOP,		LOGIN_FREE },
	{P_CRONO_P6_SETTIM_TEMP_ARIA,	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGTEMPARIA,	LOGIN_FREE },
	{P_CRONO_P6_SETTIM_TEMP_H2O, 	userIsIdro,	NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGTEMPH2O,		LOGIN_FREE },
	{P_CRONO_P6_SETTIM_POTENZA,  	NULL,       NULL,			 		/* NULL,*/       	userPostSetCronoPar,		NULL,       		RetMenuCrono, 		PRGPOTENZA,		LOGIN_FREE },
	{SUBMENU_P_TYPE,       			NULL,       CronoSettim6WeekItem,	/* NULL,*/       	EnterMenuCronoPrgDay,		NULL,   			RetMenuCrono, 		PRGGIORNISETT,	LOGIN_FREE },
};

// parametri per MENU Crono main
enum crono
{
	CRONO_ITEM_ABILITA,
	CRONO_ITEM_PROFILO,
	CRONO_ITEM_AZZERA,
	CRONO_ITEM_PROGSETTIMANALE1,
	CRONO_ITEM_PROGSETTIMANALE2,
	CRONO_ITEM_PROGSETTIMANALE3,
	CRONO_ITEM_PROGSETTIMANALE4,
	CRONO_ITEM_PROGSETTIMANALE5,
	CRONO_ITEM_PROGSETTIMANALE6,
	NUMCRONO1ITEM
};
static const EXTREC_STRUCT MenuCronoItem[NUMCRONO1ITEM] =
{
//  code,			 	IsToView,	 VoceExtIndex, 		PreFunc,		ExecFunc,       		EscModFunc,	EscFunc,     wDispId			LivAccesso
	{P_CRONO,       	NULL,        NULL,				/* NULL,*/		userPostSetCronoPar,	NULL,       RetMainMenu, ABILITAZIONE,		LOGIN_FREE },
	{P_CRONO_PROFILO,	NULL,        NULL,				/* NULL,*/		userPostSetCronoPar,	NULL,       RetMainMenu, CARICAPROFILO,		LOGIN_FREE },
	{SPECIALF_TYPE, 	NULL,        NULL,				/* NULL,*/		ConfCronoAzz,   		NULL,		RetMainMenu, AZZERA,			LOGIN_FREE },
	{SUBMENU_P_TYPE,	NULL,        CronoSettim1Item,	/* NULL,*/		EnterMenuCronoPrg,  	NULL,       RetMainMenu, PRGSETTIMANALE1,	LOGIN_FREE },
	{SUBMENU_P_TYPE,	NULL,        CronoSettim2Item,	/* NULL,*/		EnterMenuCronoPrg,  	NULL,       RetMainMenu, PRGSETTIMANALE2,	LOGIN_FREE },
	{SUBMENU_P_TYPE,	NULL,        CronoSettim3Item,	/* NULL,*/		EnterMenuCronoPrg,  	NULL,       RetMainMenu, PRGSETTIMANALE3,	LOGIN_FREE },
	{SUBMENU_P_TYPE,	NULL,        CronoSettim4Item,	/* NULL,*/		EnterMenuCronoPrg,  	NULL,       RetMainMenu, PRGSETTIMANALE4,	LOGIN_FREE },
	{SUBMENU_P_TYPE,	NULL,        CronoSettim5Item,	/* NULL,*/		EnterMenuCronoPrg,  	NULL,       RetMainMenu, PRGSETTIMANALE5,	LOGIN_FREE },
	{SUBMENU_P_TYPE,	NULL,        CronoSettim6Item,	/* NULL,*/		EnterMenuCronoPrg,  	NULL,       RetMainMenu, PRGSETTIMANALE6,	LOGIN_FREE },
};

#else	// menu_CRONO

// parametri per MENU Crono programma settimanale
enum cronoPrgParamSett
{
	CRONO_ITEM_PARAM_SETT_PRGABILITA, 
	CRONO_ITEM_PARAM_SETT_PRGSTART,
	CRONO_ITEM_PARAM_SETT_PRGSTOP,
	CRONO_ITEM_PARAM_SETT_PRGPOTENZA,
	CRONO_ITEM_PARAM_SETT_PRGTEMPARIA,
	CRONO_ITEM_PARAM_SETT_PRGTEMPH2O,
	CRONO_ITEM_SETTIM_WEEK_LUN,
	CRONO_ITEM_SETTIM_WEEK_MAR,
	CRONO_ITEM_SETTIM_WEEK_MER,
	CRONO_ITEM_SETTIM_WEEK_GIO,
	CRONO_ITEM_SETTIM_WEEK_VEN,
	CRONO_ITEM_SETTIM_WEEK_SAB,
	CRONO_ITEM_SETTIM_WEEK_DOM,
	NUMCRONOPARAMSETTITEM,
};
static const EXTREC_STRUCT CronoSettimItem[NUMCRONOPARAMSETTITEM] =
{
//  code,			              	IsToView,	VoceExtIndex, 			PreFunc,			ExecFunc,					EscModFunc,			EscFunc,				wDispId			LivAccesso
	{P_CRONO_P1_SETTIM,          	NULL,       NULL,			 		/* NULL,*/       	NULL,						NULL,	  			ExitMenuCronoProg, 		PRGABILITA,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_START,    	NULL,       NULL,			 		/* NULL,*/			NULL,						NULL,	  			ExitMenuCronoProg, 		PRGSTART,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_STOP,     	NULL,       NULL,			 		/* NULL,*/       	NULL,						NULL,	  			ExitMenuCronoProg, 		PRGSTOP,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_POTENZA,  	NULL,       NULL,			 		/* NULL,*/       	NULL,						NULL,       		ExitMenuCronoProg, 		PRGPOTENZA,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_TEMP_ARIA,	NULL,       NULL,			 		/* NULL,*/       	NULL,						NULL,       		ExitMenuCronoProg, 		PRGTEMPARIA,	LOGIN_FREE },
	{P_CRONO_P1_SETTIM_TEMP_H2O, 	userIsIdro,	NULL,			 		/* NULL,*/       	NULL,						NULL,       		ExitMenuCronoProg, 		PRGTEMPH2O,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_LUN,	NULL,		NULL,					/* NULL,*/			NULL,						NULL,				ExitMenuCronoProg,		sLUNEDI,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_MAR,	NULL,		NULL,					/* NULL,*/			NULL,						NULL,       		ExitMenuCronoProg,		sMARTEDI,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_MER,	NULL,		NULL,					/* NULL,*/			NULL,						NULL,       		ExitMenuCronoProg,		sMERCOLEDI,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_GIO,	NULL,		NULL,					/* NULL,*/			NULL,						NULL,				ExitMenuCronoProg,		sGIOVEDI,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_VEN,	NULL,		NULL,					/* NULL,*/			NULL,						NULL,       		ExitMenuCronoProg,		sVENERDI,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_SAB,	NULL,		NULL,					/* NULL,*/			NULL,						NULL,       		ExitMenuCronoProg,		sSABATO,		LOGIN_FREE },
	{P_CRONO_P1_SETTIM_WEEK_DOM,	NULL,		NULL,					/* NULL,*/			NULL,						NULL,       		ExitMenuCronoProg,		sDOMENICA,		LOGIN_FREE },
};                                                                                                                                                                 

#endif	// menu_CRONO

// parametri per MENU GENERICO
enum generic
{
	GEN_ITEM_TIPOSTUFA,
	GEN_ITEM_TIPOSTUFA_DEFAULT,
	GEN_ITEM_TIPOMOTORE,
	GEN_ITEM_TIPOMOTORE_2,
	GEN_ITEM_MULTIFAN,
	GEN_ITEM_IDRO,
	GEN_ITEM_VISSANITARI,
	GEN_ITEM_VISTIPOSTUFA,
	GEN_ITEM_SENSOREPORTATAARIA,
	GEN_ITEM_SENSOREHALL,
	GEN_ITEM_SONDALAMBDA,
	GEN_ITEM_LIM_GIRI_MINIMI,
	GEN_ITEM_INIB_ACCB,
	//GEN_ITEM_RAMPA_CAND,
	GEN_ITEM_TERMAMB,
	GEN_ITEM_IBRIDA,
	//GEN_ITEM_LEGNA,
	GEN_ITEM_SCUOTITORE,
	GEN_ITEM_COCLEA2,
	GEN_ITEM_ESP2ENABLE,
	GEN_ITEM_SENSOREHALL_2,
	GEN_ITEM_SENSOREPELLET,
	GEN_ITEM_ACC_TERMOCOPPIA,
	GEN_ITEM_ACC_FOTORESISTENZA,
	NUMGENITEM
};
static const EXTREC_STRUCT GenItem[NUMGENITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_TIPOSTUFA,     		NULL,			NULL,			 /* NULL,*/     ExecTipoStufa,	NULL,			RetMenuSysCfg,		d_TIPOSTUFA,				LOGIN_SERVICE },
	{SPECIALF_TYPE,			NULL,         	NULL,			 /* NULL,*/     ConfTipoDef,	NULL,			RetMenuSysCfg,		d_TIPOSTUFA_DEFAULT,		LOGIN_SERVICE },
	{P_TIPOMOTORE,			NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_TIPOMOTORE,				LOGIN_SERVICE },
	{P_TIPOMOTORE_2,		NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_TIPOMOTORE_2,				LOGIN_MANUFACTURER },
	{P_MULTIFAN,			NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_MULTIFAN,					LOGIN_MANUFACTURER },
	{P_IDRO,				NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_IDRO,						LOGIN_SERVICE },
	{P_VIS_SANITARI,		NULL,			NULL,			 /* NULL,*/		NULL,			NULL,			RetMenuSysCfg,		d_VISSANITARI,				LOGIN_SERVICE },
	{P_VIS_TIPOSTUFA,		NULL,			NULL,			 /* NULL,*/		NULL,			NULL,			RetMenuSysCfg,		d_VISTIPOSTUFA,				LOGIN_SERVICE },
	{P_SENSOREPORTATAARIA,	NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_SENSOREPORTATAARIA,		LOGIN_SERVICE },
	{P_SENSOREHALL,			NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_SENSOREHALL,				LOGIN_SERVICE },
	{P_SONDA_LAMBDA,		NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_CONTROLLOLAMBDA,			LOGIN_MANUFACTURER },
	{P_LIM_GIRI_MINIMI,		NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_LIM_GIRI_MINIMI,			LOGIN_MANUFACTURER },
	{P_INIB_ACC_CALDO,		NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_INIB_ACCB,				LOGIN_MANUFACTURER },
	//{P_RAMPA_CAND,			NULL,			NULL,			/* NULL,*/     NULL,	    	NULL,			RetMenuSysCfg,		d_RAMPA_CAND,				LOGIN_SERVICE },
	{P_TERM_AMB,			NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_TERMAMB,					LOGIN_SERVICE },
	{P_IBRIDA,				NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_IBRIDA,					LOGIN_MANUFACTURER },
	//{P_EN_LEGNA,				NULL,			NULL,			/* NULL,*/     NULL,	    	NULL,			RetMenuSysCfg,		d_LEGNA,						LOGIN_SERVICE },
	{P_SCUOTITORE,			NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_SCUOTITORE,				LOGIN_MANUFACTURER },
	{P_PULIT_COCLEA2,		NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_COCLEA2,					LOGIN_MANUFACTURER },
	{P_ESP2_ENABLE,			NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_ESP2ENABLE,				LOGIN_MANUFACTURER },
	{P_EN_FAN2_HALL,		NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_SENSOREHALL_2,			LOGIN_MANUFACTURER },
	{P_SENSOREPELLET,		NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_SENSOREPELLET,			LOGIN_SERVICE },
	{P_ACC_TC,				NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_ACC_TERMOCOPPIA,			LOGIN_MANUFACTURER },
	{P_ACC_FR,				NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_ACC_FOTORESISTENZA,		LOGIN_MANUFACTURER },
};

// parametri per MENU BLACKOUT
enum blkout
{
	BLKOUT_ITEM_BLK_RIPRISTINO,
	BLKOUT_ITEM_BLK_DURATA,
	NUMBLKOUTITEM
};
static const EXTREC_STRUCT BlkoutItem[NUMBLKOUTITEM] =
{
//  code,			   		IsToView,	  	VoceExtIndex,	PreFunc,		ExecFunc,		EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_RIPRISTINO_BLACKOUT,	NULL,         	NULL,			/* NULL,*/		NULL,			NULL,			RetMenuSysCfg,		d_BLK_RIPRISTINO,			LOGIN_MANUFACTURER },
	{P_TRIPRI_BLACKOUT,		IsRipristino,	NULL,			/* NULL,*/		NULL,			NULL,			RetMenuSysCfg,		d_BLK_DURATA,				LOGIN_MANUFACTURER },
};

// parametri per MENU COCLEA
enum cocl
{
	COCLEA_ITEM_FRENATACOCLEA,
	COCLEA_ITEM_TIPO_FRENATACOCLEA,
	COCLEA_ITEM_EN_PERIODO_COCLEA,
	COCLEA_ITEM_PERIODO_COCLEA,
	COCLEA_ITEM_GIRI_COCLEA,
	COCLEA_ITEM_FRENATACOCLEA_2,
	COCLEA_ITEM_TIPO_FRENATACOCLEA_2,
	COCLEA_ITEM_EN_PERIODO_COCLEA_2,
	COCLEA_ITEM_PERIODO_COCLEA_2,
	NUMCOCLEAITEM
};
static const EXTREC_STRUCT CocleaItem[NUMCOCLEAITEM] =
{
//  code,			   			IsToView,	  			VoceExtIndex,	PreFunc,		ExecFunc,		EscModFunc,		EscFunc,			wDispId					LivAccesso
	{P_FRENATACOCLEA,			IsTempoCoclea,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FRENATACOCLEA,		LOGIN_SERVICE },
	{P_TIPO_FRENATA_COCLEA,		IsTipoFrenCoclea,		NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_TIPO_FRENATACOCLEA,	LOGIN_MANUFACTURER },
	{P_EN_PERIODO_COCLEA,		IsTempoCoclea,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_EN_PERIODO_COCLEA,	LOGIN_MANUFACTURER },
	{P_PERIODO_COCLEA,			IsPeriodoCoclea,		NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_PERIODO_COCLEA,		LOGIN_SERVICE },
	{P_EN_COCLEA_HALL,			NULL,         			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_GIRI_COCLEA,			LOGIN_MANUFACTURER },
	{P_FRENATA_FAN1_COCLEA2,	IsCoclea2,				NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FRENATACOCLEA_2,		LOGIN_MANUFACTURER },
	{P_TIPO_FRENATA_FAN1,		IsTipoFrenCoclea2,		NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_TIPO_FRENATACOCLEA_2,	LOGIN_MANUFACTURER },
	{P_EN_PERIODO_FAN1,			IsCoclea2,				NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_EN_PERIODO_COCLEA_2,	LOGIN_MANUFACTURER },
	{P_PERIODO_FAN1,			IsPeriodoCoclea2,		NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_PERIODO_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU SCUOTITORE
enum scuot
{
	SCUOT_ITEM_DURATA_CICLO,
	SCUOT_ITEM_NUMCICLI,
	//SCUOT_ITEM_PERIODO,
	NUMSCUOTITEM
};
static const EXTREC_STRUCT ScuotItem[NUMSCUOTITEM] =
{
//	code,				   		IsToView,	  VoceExtIndex,		PreFunc,		ExecFunc,		EscModFunc,		EscFunc,			wDispId				LivAccesso
	{P_SCUOT_DURATA,			NULL,         NULL,			 	/* NULL,*/		NULL,	    	NULL,			RetMenuCtrl,		d_SCUOT_DURATA, 	LOGIN_MANUFACTURER },
	{P_SCUOT_NCICLI,			NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuCtrl,		d_SCUOT_NCICLI,		LOGIN_MANUFACTURER },
	//{P_SCUOT_PERIODO,			NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuCtrl,		d_SCUOT_PERIODO,	LOGIN_MANUFACTURER },
};

// parametri per MENU FAN AMBIENTE
enum 
{
	FANAMB_ITEM_CTRL_TEMP_FUMI,
	FANAMB_ITEM_FAN_TFUMI_ON,
	FANAMB_ITEM_FAN_TFUMI_OFF,	
	NUMFANAMBITEM
};
static const EXTREC_STRUCT FanAmb[NUMFANAMBITEM] =
{
//  code,			   		IsToView,	  VoceExtIndex,		PreFunc,		ExecFunc,     	EscModFunc,		EscFunc,			wDispId				LivAccesso
	{P_EN_FANTEMPFUMI,		NULL,         NULL,			 	/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_CTRL_TEMP_FUMI,	LOGIN_MANUFACTURER },
	{P_TFUMI_FAN_ON,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_TFUMI_ON,		LOGIN_SERVICE },
	{P_TFUMI_FAN_MIN,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_TFUMI_OFF,	LOGIN_MANUFACTURER },
};

// parametri per MENU FAN 1
enum fan
{
	FAN_ITEM_FAN1_L1,
	FAN_ITEM_FAN1_L2,
	FAN_ITEM_FAN2_L3,
	FAN_ITEM_FAN2_L4,
	FAN_ITEM_FAN2_L5,
	FAN_ITEM_INTERPOL,
	NUMFANITEM
};
static const EXTREC_STRUCT Fan1Item[NUMFANITEM] =
{
//  code,			   		IsToView,	  VoceExtIndex,		PreFunc,		ExecFunc,		EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_POT1_ATTUA_FAN1,		NULL,         NULL,			 	/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L1,					LOGIN_SERVICE },
	{P_POT2_ATTUA_FAN1,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L2,					LOGIN_SERVICE },
	{P_POT3_ATTUA_FAN1,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L3,					LOGIN_SERVICE },
	{P_POT4_ATTUA_FAN1,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L4,					LOGIN_SERVICE },
	{P_POT5_ATTUA_FAN1,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L5,					LOGIN_SERVICE },
	{P_FAN1_INTERPOL,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		sINTERPOLAZIONE_MENU,		LOGIN_SERVICE },
};

// parametri per MENU FAN 2
static const EXTREC_STRUCT Fan2Item[NUMFANITEM] =
{
//  code,			   		IsToView,	  VoceExtIndex,		PreFunc,		ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_POT1_ATTUA_FAN2,		NULL,         NULL,			 	/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L1,					LOGIN_SERVICE },
	{P_POT2_ATTUA_FAN2,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L2,					LOGIN_SERVICE },
	{P_POT3_ATTUA_FAN2,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L3,					LOGIN_SERVICE },
	{P_POT4_ATTUA_FAN2,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L4,					LOGIN_SERVICE },
	{P_POT5_ATTUA_FAN2,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L5,					LOGIN_SERVICE },
	{P_FAN2_INTERPOL,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		sINTERPOLAZIONE_MENU,		LOGIN_SERVICE },
};

// parametri per MENU FAN 3
static const EXTREC_STRUCT Fan3Item[NUMFANITEM] =
{
//  code,			   		IsToView,	  VoceExtIndex,		PreFunc,		ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_POT1_ATTUA_FAN3,		NULL,         NULL,			 	/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L1,					LOGIN_MANUFACTURER },
	{P_POT2_ATTUA_FAN3,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L2,					LOGIN_MANUFACTURER },
	{P_POT3_ATTUA_FAN3,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L3,					LOGIN_MANUFACTURER },
	{P_POT4_ATTUA_FAN3,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L4,					LOGIN_MANUFACTURER },
	{P_POT5_ATTUA_FAN3,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		d_FAN_L5,					LOGIN_MANUFACTURER },
	{P_FAN3_INTERPOL,		NULL,         NULL,				/* NULL,*/		NULL,	    	NULL,			RetMenuSysCfg,		sINTERPOLAZIONE_MENU,		LOGIN_MANUFACTURER },
};

// parametri per MENU CONFIG SYSTEM
enum sysconfig
{
	SYSCFG_ITEM_GEN_MENU,
	SYSCFG_ITEM_BLACKOUT,
	SYSCFG_ITEM_COCLEA_MENU,
	// SYSCFG_ITEM_SCUOTITORE_MENU,
	SYSCFG_ITEM_FANAMB_MENU,
	SYSCFG_ITEM_FAN1_MENU,
	SYSCFG_ITEM_FAN2_MENU,
	SYSCFG_ITEM_FAN3_MENU,
	NUMSYSCFGITEM
};
static const EXTREC_STRUCT SysCfgItem[NUMSYSCFGITEM] =
{
//  code,				IsToView,		VoceExtIndex,  PreFunc,			ExecFunc,    		EscModFunc,		EscFunc,		wDispId					LivAccesso
	{SUBMENU_P_TYPE,  	NULL,			GenItem,		/* NULL,*/		EnterMenuGen,		NULL,       	RetMenuTec, 	d_GEN_MENU,				LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,			BlkoutItem,		/* NULL,*/		EnterMenuBlkout,	NULL,			RetMenuTec, 	sBLACKOUT,				LOGIN_MANUFACTURER },
	{SUBMENU_P_TYPE,	IsCocleaItem,	CocleaItem,		/* NULL,*/		EnterMenuCoclea,	NULL,			RetMenuTec, 	d_COCLEA_MENU,			LOGIN_SERVICE },
	// {SUBMENU_P_TYPE,	IsScuotitore,	ScuotItem,		/* NULL,*/		EnterMenuScuot,		NULL,			RetMenuTec, 	d_SCUOTITORE_MENU,		LOGIN_MANUFACTURER },
	{SUBMENU_P_TYPE,	NULL,			FanAmb,			/* NULL,*/		EnterMenuFanAmb,	NULL,			RetMenuTec, 	sFANAMB_MENU,			LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	IsFan1Test,		Fan1Item,		/* NULL,*/		EnterMenuFan1,		NULL,			RetMenuTec, 	sFAN1_MENU,				LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	IsFan2Test,		Fan2Item,		/* NULL,*/		EnterMenuFan2,		NULL,			RetMenuTec, 	sFAN2_MENU,				LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,			Fan3Item,		/* NULL,*/		EnterMenuFan3,		NULL,			RetMenuTec, 	sFAN3_MENU,				LOGIN_MANUFACTURER },
};

// parametri per MENU RICETTE TRANSITORIE
enum 
{
	RICTRA_ITEM_PELLET,
	RICTRA_ITEM_ARIA,
	NUMRICTRAITEM
};
static const EXTREC_STRUCT RicTraItem[NUMRICTRAITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,			EscFunc,			wDispId					LivAccesso
	{P_RICETTA_PELLET_T,	NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuCtrl,		d_RICETTAPELLET_ON,		LOGIN_SERVICE },
	{P_RICETTA_ARIA_T,		IsCtrlAria,		NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuCtrl,		d_OFFSETESPULSORE_ON,	LOGIN_SERVICE },
};

// parametri per MENU ECO
enum ecostop
{
	ECOSTOP_ITEM_ECO,
	ECOSTOP_ITEM_TWARMUP_ECO,
	ECOSTOP_ITEM_TSHUTDOWN_ECO,
	ECOSTOP_ITEM_DELTATEMP_ECO,
	NUMECOSTOPITEM
};
static const EXTREC_STRUCT EcoItem[NUMECOSTOPITEM] =
{
//  code,			   		IsToView,	  	VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,		EscFunc,			wDispId				LivAccesso
	{P_ECO,					NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuCtrl,		d_ECOSTOP,			LOGIN_SERVICE },
	{P_TSWITCHON_ECO,		NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuCtrl,		d_TWARMUP_ECO,		LOGIN_SERVICE },
	{P_TSHUTDOWN_ECO,		NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuCtrl,		d_TSHUTDOWN_ECO,	LOGIN_SERVICE },
	{P_DELTATEMP,			IsDTempEco,		NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuCtrl,		d_DELTATEMP_ECO,	LOGIN_SERVICE },
};

// parametri per MENU TEMPI DI FUNZIONAMENTO
enum 
{
	TFUNZ_ITEM_OREFUNZIONAMENTO,
	TFUNZ_ITEM_ORESAT,
	NUMTFUNZITEM
};
static const EXTREC_STRUCT TFunzItem[NUMTFUNZITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,		EscFunc,			wDispId					LivAccesso
	{P_OREFUNZIONAMENTO,	NULL,			NULL,			/* NULL,*/		NULL,			NULL,			RetMenuAttuaPot,	d_OREFUNZIONAMENTO,		LOGIN_SERVICE },
	{P_ORESAT,				NULL,			NULL,			/* NULL,*/		NULL,			NULL,			RetMenuAttuaPot,	d_ORESAT,				LOGIN_SERVICE },
};

// parametri per MENU IDRO
enum 
{
	IDRO_ITEM_IDROINDIP,
	IDRO_ITEM_SPEIDRO,
	//IDRO_ITEM_ACCUMULO,
	IDRO_ITEM_INIB_SENSING,
	IDRO_ITEM_POMPA_MODULANTE,
	IDRO_ITEM_ENPRESSIDRO,
	//IDRO_ITEM_FLUSS_DIGIT,
	IDRO_ITEM_SETRISC,
	IDRO_ITEM_GAIN_RISC,
	IDRO_ITEM_ISTERESI_TEMP_ACQUA,
	IDRO_ITEM_SETSAN,
	IDRO_ITEM_DELTA_SANITARI,
	IDRO_ITEM_GAIN_SANIT,
	IDRO_ITEM_TEMPAUX,
	IDRO_ITEM_MAXPRESSH2O,
	IDRO_ITEM_SETTONPOMPA,
	IDRO_ITEM_SETTOFFPOMPA,
	NUMIDROITEM
};
static const EXTREC_STRUCT IdroItem[NUMIDROITEM] =
{
//  code,			   			IsToView,	  	VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_INDIPENDENTE,			NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_IDROINDIP,				LOGIN_SERVICE },
	{P_SPEGNIMENTO_IDRO,		NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_SPEIDRO,					LOGIN_SERVICE },
	//{P_ACCUMULO,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_ACCUMULO,					LOGIN_MANUFACTURER },
	{P_INIB_SENSING_H2O,		NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_INIB_SENSING,				LOGIN_SERVICE },
	{P_POMPA_MODULANTE,			NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_POMPA_MODULANTE,			LOGIN_MANUFACTURER },
	{P_FPRESSIDRO,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_ENPRESSIDRO,				LOGIN_SERVICE },
	//{P_FLUSS_DIGIT,			NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_FLUSS_DIGIT,				LOGIN_MANUFACTURER },
	{P_SET_TRISC,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_TEMPH2O_SETRISC,			LOGIN_SERVICE },
	{P_GAIN_RISC,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_GAIN_RISC,				LOGIN_MANUFACTURER },
	{P_ISTERESI_TEMP_ACQUA,		NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_ISTERESI_TEMP_ACQUA,		LOGIN_SERVICE },
	{P_SET_TSANIT,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_TEMPH2O_SETSAN,			LOGIN_SERVICE },
	{P_DELTA_SANITARI,			NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_DELTA_SANITARI,			LOGIN_MANUFACTURER },
	{P_GAIN_SANIT,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_GAIN_SANIT,				LOGIN_MANUFACTURER },
	{P_TEMPAUX,					NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_TEMPAUX,					LOGIN_SERVICE },
	{P_MAXPRESSH2O,				IsPressIdro,	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_MAXPRESSH2O,				LOGIN_SERVICE },
	{P_TSETONPOMPA,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_SETTONPOMPA,				LOGIN_SERVICE },
	{P_TSETOFFPOMPA,			NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTec,			d_SETTOFFPOMPA,				LOGIN_SERVICE },
};                                                       
                                                         
// parametri per MENU CONTROLLO
enum
{
	CTRL_ITEM_RICTRA_MENU,
	CTRL_ITEM_ECOSTOP_MENU,
	// CTRL_ITEM_IDRO_MENU,
	CTRL_ITEM_SCUOTITORE_MENU,
	NUMCTRLITEM
};
static const EXTREC_STRUCT CtrlItem[NUMCTRLITEM] =
{
//  code,				IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,    		EscModFunc,		EscFunc,		wDispId					LivAccesso
	{SUBMENU_P_TYPE,	NULL,			RicTraItem,		/* NULL,*/		EnterMenuRicTra,	NULL,       	RetMenuTec, 	sRICETTE_MENU,			LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,			EcoItem,		/* NULL,*/		EnterMenuEco,		NULL,			RetMenuTec, 	d_ECOSTOP_MENU,			LOGIN_SERVICE },
	// {SUBMENU_P_TYPE,	userIsIdro,		IdroItem,		/* NULL,*/		EnterMenuIdro,		NULL,			RetMenuTec, 	d_IDRO_MENU,			LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	IsScuotitore,	ScuotItem,		/* NULL,*/		EnterMenuScuot,		NULL,			RetMenuTec, 	d_SCUOTITORE_MENU,		LOGIN_MANUFACTURER },
};

// parametri per MENU PARAMETRI TRANSITORI
enum 
{
	PARTRA_ITEM_BYPASS,
	PARTRA_ITEM_TPREACC,		
	PARTRA_ITEM_TPRELOAD,
	PARTRA_ITEM_TPREACC2,		
	PARTRA_ITEM_TEMP_ON_FUMI,	
	PARTRA_ITEM_TEMP_OFF_FUMI,	
	PARTRA_ITEM_DELTATEMP_NO_FIAMMA,
	PARTRA_ITEM_TEMP_SOGLIA_FUMI,
	PARTRA_ITEM_TACCINC,
	PARTRA_ITEM_DELTA_TEMP_CALDO,
	PARTRA_ITEM_MAX_TWARMUP,
	PARTRA_ITEM_MAX_TFIREON,
	PARTRA_ITEM_MAX_TSPE,
	NUMPARTRAITEM
};
static const EXTREC_STRUCT ParTraItem[NUMPARTRAITEM] =
{
//  code,			   			IsToView,	  	VoceExtIndex,	PreFunc,		ExecFunc,		EscModFunc,		EscFunc,			wDispId						LivAccesso
	{SPECIALF_TYPE,				NULL,         	NULL,			/* NULL,*/		ConfBypassTra,	NULL,			RetMenuAttuaTra,	d_TEST_BYPASS,				LOGIN_SERVICE },
	{P_TPREACC,					NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_TPREACC,					LOGIN_SERVICE },
	{P_TPREACC2,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_TPRELOAD,					LOGIN_SERVICE },
	{P_TPRELOAD,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_TPREACC2,					LOGIN_MANUFACTURER },
	{P_TFUMI_ON,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_TEMP_ON_FUMI,				LOGIN_SERVICE },
	{P_TFUMI_OFF,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_TEMP_OFF_FUMI,			LOGIN_SERVICE },
	{P_DELTATEMP_NO_FIAMMA,		NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_NOFIAMMA_DELTA_TEMP,		LOGIN_MANUFACTURER },
	{P_TEMP_SOGLIA_FUMI_LEGNA,	NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_TEMP_SOGLIA_FUMI,			LOGIN_MANUFACTURER },
	{P_TEMPO_RAFFA,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_TACCINC,					LOGIN_MANUFACTURER },
	{P_DELTA_TACC_CALDO,		NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_DELTA_TEMP_CALDO,			LOGIN_MANUFACTURER },
	{P_MAX_TWARMUP,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_MAX_TWARMUP,				LOGIN_SERVICE },
	{P_MAX_TFIREON_A,			NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_MAX_TFIREON,				LOGIN_SERVICE },
	{P_TIME_SPEA,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_MAX_TSPE,					LOGIN_MANUFACTURER },
};                                                          
                                                            
// parametri per MENU PREACCENSIONE 1                       
enum                                                        
{
	PREACC1_ITEM_GIRI_COCLEA,
	PREACC1_ITEM_TON_COCLEA_1,
	PREACC1_ITEM_TOFF_COCLEA_1,
	PREACC1_ITEM_PORTATA,
	PREACC1_ITEM_GIRI,
	PREACC1_ITEM_ESPULSORE_2,
	PREACC1_ITEM_GIRI_2,
	PREACC1_ITEM_TON_COCLEA_2,
	PREACC1_ITEM_TOFF_COCLEA_2,
	NUMPREACC1ITEM
};
static const EXTREC_STRUCT PreAcc1Item[NUMPREACC1ITEM] =
{
//  code,			   			IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,			EscFunc,			wDispId						LivAccesso
	{P_PREACC1_GIRI_COCLEA,		IsGiriCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_PREACC1_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_PREACC1_TOFF_COCLEA,		IsTOffCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_PREACC1_PORTATA,			IsCtrlPortata,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_PREACC1_GIRI,			NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_PREACC1_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_PREACC1_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_PREACC1_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_PREACC1_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU PREACCENSIONE 2
enum 
{
	PREACC2_ITEM_GIRI_COCLEA,
	PREACC2_ITEM_TON_COCLEA_1,
	PREACC2_ITEM_TOFF_COCLEA_1,
	PREACC2_ITEM_PORTATA,
	PREACC2_ITEM_GIRI,
	PREACC2_ITEM_ESPULSORE_2,
	PREACC2_ITEM_GIRI_2,
	PREACC2_ITEM_TON_COCLEA_2,
	PREACC2_ITEM_TOFF_COCLEA_2,
	NUMPREACC2ITEM
};
static const EXTREC_STRUCT PreAcc2Item[NUMPREACC2ITEM] =
{
//  code,			   			IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,			EscFunc,			wDispId						LivAccesso
	{P_PREACC2_GIRI_COCLEA,		IsGiriCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_PREACC2_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_PREACC2_TOFF_COCLEA,		IsTOffCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_PREACC2_PORTATA,			IsCtrlPortata,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_PREACC2_GIRI,			NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_PREACC2_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_PREACC2_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_PREACC2_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_PREACC2_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU PREACCENSIONE A CALDO
enum 
{
	PREACCCALDO_ITEM_GIRI_COCLEA,
	PREACCCALDO_ITEM_TON_COCLEA_1,
	PREACCCALDO_ITEM_TOFF_COCLEA_1,
	PREACCCALDO_ITEM_PORTATA,
	PREACCCALDO_ITEM_GIRI,
	PREACCCALDO_ITEM_ESPULSORE_2,
	PREACCCALDO_ITEM_GIRI_2,
	PREACCCALDO_ITEM_TON_COCLEA_2,
	PREACCCALDO_ITEM_TOFF_COCLEA_2,
	NUMPREACCCALDOITEM
};
static const EXTREC_STRUCT PreAccHotItem[NUMPREACCCALDOITEM] =
{
//  code,			   				IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,			EscFunc,			wDispId						LivAccesso
	{P_PREACCCALDO_GIRI_COCLEA,		IsGiriCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_PREACCCALDO_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_PREACCCALDO_TOFF_COCLEA,		IsTOffCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_PREACCCALDO_PORTATA,			IsCtrlPortata,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_PREACCCALDO_GIRI,			NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_PREACCCALDO_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_PREACCCALDO_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_PREACCCALDO_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_PREACCCALDO_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU ACCENSIONE A
enum 
{
	ACCA_ITEM_GIRI_COCLEA,
	ACCA_ITEM_TON_COCLEA_1,
	ACCA_ITEM_TOFF_COCLEA_1,
	ACCA_ITEM_PORTATA,
	ACCA_ITEM_GIRI,
	ACCA_ITEM_ESPULSORE_2,
	ACCA_ITEM_GIRI_2,
	ACCA_ITEM_TON_COCLEA_2,
	ACCA_ITEM_TOFF_COCLEA_2,
	NUMACCAITEM
};
static const EXTREC_STRUCT AccAItem[NUMACCAITEM] =
{
//  code,			   			IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_ACCA_GIRI_COCLEA,		IsGiriCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_ACCA_TON_COCLEA,			IsTempoCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_ACCA_TOFF_COCLEA,		IsTOffCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_ACCA_PORTATA,			IsCtrlPortata,	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_ACCA_GIRI,				NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_ACCA_ATTUA_ESP2,			IsEsp2,			NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_ACCA_GIRI2,				IsEsp2Giri,		NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_ACCA_TON_FAN1,			IsCoclea2,		NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_ACCA_TOFF_FAN1,			IsTOffCoclea2,	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU ACCENSIONE B
enum 
{
	ACCB_ITEM_GIRI_COCLEA,
	ACCB_ITEM_TON_COCLEA_1,
	ACCB_ITEM_TOFF_COCLEA_1,
	ACCB_ITEM_PORTATA,
	ACCB_ITEM_GIRI,
	ACCB_ITEM_ESPULSORE_2,
	ACCB_ITEM_GIRI_2,
	ACCB_ITEM_TON_COCLEA_2,
	ACCB_ITEM_TOFF_COCLEA_2,
	NUMACCBITEM
};
static const EXTREC_STRUCT AccBItem[NUMACCBITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_ACCB_GIRI_COCLEA,	IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_ACCB_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_ACCB_TOFF_COCLEA,	IsTOffCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_ACCB_PORTATA,		IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_ACCB_GIRI,			NULL,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_ACCB_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_ACCB_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_ACCB_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_ACCB_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU FIRE ON
enum 
{
	FIREONA_ITEM_GIRI_COCLEA,
	FIREONA_ITEM_TON_COCLEA_1,
	FIREONA_ITEM_TOFF_COCLEA_1,
	FIREONA_ITEM_PORTATA,
	FIREONA_ITEM_GIRI,
	FIREONA_ITEM_ESPULSORE_2,
	FIREONA_ITEM_GIRI_2,
	FIREONA_ITEM_TON_COCLEA_2,
	FIREONA_ITEM_TOFF_COCLEA_2,
	NUMFIREONAITEM
};
static const EXTREC_STRUCT FireONAItem[NUMFIREONAITEM] =
{
//  code,			   			IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,			EscFunc,			wDispId						LivAccesso
	{P_FIREONA_GIRI_COCLEA,		IsGiriCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_FIREONA_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_FIREONA_TOFF_COCLEA,		IsTOffCoclea,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_FIREONA_PORTATA,			IsCtrlPortata,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_FIREONA_GIRI,			NULL,			NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_FIREONA_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_FIREONA_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_FIREONA_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_FIREONA_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU SPEGNIMENTO A
enum 
{
	SPEA_ITEM_GIRI_COCLEA,
	SPEA_ITEM_TON_COCLEA_1,
	SPEA_ITEM_TOFF_COCLEA_1,
	SPEA_ITEM_PORTATA,
	SPEA_ITEM_GIRI,
	SPEA_ITEM_ESPULSORE_2,
	SPEA_ITEM_GIRI_2,
	SPEA_ITEM_TON_COCLEA_2,
	SPEA_ITEM_TOFF_COCLEA_2,
	NUMSPEAITEM
};
static const EXTREC_STRUCT SpeAItem[NUMSPEAITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_SPEA_GIRI_COCLEA,	IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_SPEA_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_SPEA_TOFF_COCLEA,	IsTOffCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_SPEA_PORTATA,		IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_PORTATA,			LOGIN_MANUFACTURER },
	{P_SPEA_GIRI,			NULL,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_GIRI,				LOGIN_MANUFACTURER },
	{P_SPEA_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_SPEA_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_SPEA_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_SPEA_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU SPEGNIMENTO B
enum 
{
	SPEB_ITEM_GIRI_COCLEA,
	SPEB_ITEM_TON_COCLEA_1,
	SPEB_ITEM_TOFF_COCLEA_1,
	SPEB_ITEM_PORTATA,
	SPEB_ITEM_GIRI,
	SPEB_ITEM_ESPULSORE_2,
	SPEB_ITEM_GIRI_2,
	SPEB_ITEM_TON_COCLEA_2,
	SPEB_ITEM_TOFF_COCLEA_2,
	NUMSPEBITEM
};
static const EXTREC_STRUCT SpeBItem[NUMSPEBITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_SPEB_GIRI_COCLEA,	IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_SPEB_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_SPEB_TOFF_COCLEA,	IsTOffCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_SPEB_PORTATA,		IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_PORTATA,			LOGIN_MANUFACTURER },
	{P_SPEB_GIRI,			NULL,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_GIRI,				LOGIN_MANUFACTURER },
	{P_SPEB_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_SPEB_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_SPEB_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_SPEB_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU RAFFREDDAMENTO A
enum 
{
	RAFFA_ITEM_GIRI_COCLEA,
	RAFFA_ITEM_TON_COCLEA_1,
	RAFFA_ITEM_TOFF_COCLEA_1,
	RAFFA_ITEM_PORTATA,
	RAFFA_ITEM_GIRI,
	RAFFA_ITEM_ESPULSORE_2,
	RAFFA_ITEM_GIRI_2,
	RAFFA_ITEM_TON_COCLEA_2,
	RAFFA_ITEM_TOFF_COCLEA_2,
	NUMRAFFAITEM
};
static const EXTREC_STRUCT RaffAItem[NUMRAFFAITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_RAFFA_GIRI_COCLEA,	IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_RAFFA_TON_COCLEA,	IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_RAFFA_TOFF_COCLEA,	IsTOffCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_RAFFA_PORTATA,		IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_PORTATA,			LOGIN_MANUFACTURER },
	{P_RAFFA_GIRI,			NULL,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_GIRI,				LOGIN_MANUFACTURER },
	{P_RAFFA_ATTUA_ESP2,	IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_RAFFA_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_RAFFA_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_RAFFA_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaTra,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU ATTUAZIONI TRANSITORIE
enum
{
	ATTUATRA_ITEM_PARAM_TRANSITORI_MENU,
	ATTUATRA_ITEM_PREACC1_MENU,
	ATTUATRA_ITEM_PREACC2_MENU,
	ATTUATRA_ITEM_PREACCCALDO_MENU,
	ATTUATRA_ITEM_ACCA_MENU,
	ATTUATRA_ITEM_ACCB_MENU,
	ATTUATRA_ITEM_FIREONA_MENU,
	ATTUATRA_ITEM_SPEA_MENU,
	ATTUATRA_ITEM_SPEB_MENU,
	ATTUATRA_ITEM_RAFF_MENU,
	NUMATTUATRAITEM
};
static const EXTREC_STRUCT AttuaTraItem[NUMATTUATRAITEM] =
{
//  code,				IsToView,			VoceExtIndex,	PreFunc,			ExecFunc,    			EscModFunc,	EscFunc,		wDispId							LivAccesso
	{SUBMENU_P_TYPE,	NULL,          		ParTraItem,		/* NULL,*/			EnterMenuParTra,		NULL,       RetMenuTec, 	sPARAM_TRANSITORI_MENU,			LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,          		PreAcc1Item,	/* NULL,*/			EnterMenuPreAcc1,		NULL,       RetMenuTec, 	sPREACC1_MENU,					LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,          		PreAcc2Item,	/* NULL,*/			EnterMenuPreAcc2,		NULL,       RetMenuTec, 	sPREACC2_MENU,					LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	IsPreAccHotItem,	PreAccHotItem,	/* NULL,*/			EnterMenuPreAccHot,		NULL,       RetMenuTec, 	sPREACCCALDO_MENU,				LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,          		AccAItem,		/* NULL,*/			EnterMenuAccA,			NULL,       RetMenuTec, 	sACCA_MENU,						LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	IsAccBItem,      	AccBItem,		/* NULL,*/			EnterMenuAccB,			NULL,       RetMenuTec, 	sACCB_MENU,						LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,          		FireONAItem,	/* NULL,*/			EnterMenuFireONA,		NULL,       RetMenuTec, 	sFIREONA_MENU,					LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,          		SpeAItem,		/* NULL,*/			EnterMenuSpeA,			NULL,       RetMenuTec, 	sSPEA_MENU,						LOGIN_MANUFACTURER },
	{SUBMENU_P_TYPE,	NULL,          		SpeBItem,		/* NULL,*/			EnterMenuSpeB,			NULL,       RetMenuTec, 	sSPEB_MENU,						LOGIN_MANUFACTURER },
	{SUBMENU_P_TYPE,	NULL,          		RaffAItem,		/* NULL,*/			EnterMenuRaffA,			NULL,       RetMenuTec, 	sRAFFA_MENU,					LOGIN_MANUFACTURER },
};

// parametri per MENU PARAMETRI DI POTENZA
enum 
{
	PARPOT_ITEM_LIVPOT_MAX,
	NUMPARPOTITEM
};
static const EXTREC_STRUCT ParPotItem[NUMPARPOTITEM] =
{
//  code,			   		IsToView,	  VoceExtIndex,		PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId					LivAccesso
	{P_MAXPOWER,			NULL,         NULL,				/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_LIVPOT_MAX,			LOGIN_MANUFACTURER },
};

// parametri per MENU PULIZIA BRACIERE
enum 
{
	PB_ITEM_TWAIT_PB,
	PB_ITEM_TPB,
	PB_ITEM_MINPOWER_PB,
	NUMPBITEM
};
static const EXTREC_STRUCT PBItem[NUMPBITEM] =
{
//  code,			   		IsToView,	  VoceExtIndex,		PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId				LivAccesso
	{P_TWAIT_PB,			NULL,         NULL,			 	/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_TWAIT_PB,			LOGIN_SERVICE },
	{P_TPB,					NULL,         NULL,			 	/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_TPB,				LOGIN_SERVICE },
	{P_MINPOWER_PB,			NULL,         NULL,			 	/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_MINPOWER_PB,		LOGIN_SERVICE },
};

// parametri per MENU RITARDO ATTUAZIONE ESPULSORE
enum 
{
	RITESP_ITEM_RITARDO_INC,
	RITESP_ITEM_RITARDO_DEC,
	NUMRITESPITEM
};
static const EXTREC_STRUCT RitEspItem[NUMRITESPITEM] =
{
//  code,			   		IsToView,	  VoceExtIndex,		PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId				LivAccesso
	{P_DELAY_INC_ESP,		NULL,         NULL,			 	/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_RITARDO_ESP_INC,	LOGIN_MANUFACTURER },
	{P_DELAY_DEC_ESP,		NULL,         NULL,			 	/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_RITARDO_ESP_DEC,	LOGIN_MANUFACTURER },
};

// parametri per MENU RITARDO STEP POTENZA
enum 
{
	RITPOT_ITEM_RITARDO_INC,
	RITPOT_ITEM_RITARDO_DEC,
	NUMRITPOTITEM
};
static const EXTREC_STRUCT RitPotItem[NUMRITPOTITEM] =
{
//  code,			   			IsToView,	  VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId				LivAccesso
	{P_DELAY_INC_STEP_POTENZA,	NULL,         NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_RITARDO_POT_INC,	LOGIN_MANUFACTURER },
	{P_DELAY_DEC_STEP_POTENZA,	NULL,         NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_RITARDO_POT_DEC,	LOGIN_MANUFACTURER },
};

// parametri per MENU POTENZA 1
enum 
{
	POT1_ITEM_GIRI_COCLEA,
	POT1_ITEM_TON_COCLEA_1,
	POT1_ITEM_TOFF_COCLEA_1,
	POT1_ITEM_PORTATA,
	POT1_ITEM_GIRI,
	POT1_ITEM_ESPULSORE_2,
	POT1_ITEM_GIRI_2,
	POT1_ITEM_TON_COCLEA_2,
	POT1_ITEM_TOFF_COCLEA_2,
	NUMPOT1ITEM
};
static const EXTREC_STRUCT Pot1Item[NUMPOT1ITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_POT1_GIRI_COCLEA,	IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_POT1_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_POT1_TOFF_COCLEA,	IsTOffCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_POT1_PORTATA,		IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_POT1_GIRI,			NULL,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_POT1_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_POT1_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_POT1_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_POT1_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU POTENZA 2
enum 
{
	POT2_ITEM_GIRI_COCLEA,
	POT2_ITEM_TON_COCLEA_1,
	POT2_ITEM_TOFF_COCLEA_1,
	POT2_ITEM_PORTATA,
	POT2_ITEM_GIRI,
	POT2_ITEM_ESPULSORE_2,
	POT2_ITEM_GIRI_2,
	POT2_ITEM_TON_COCLEA_2,
	POT2_ITEM_TOFF_COCLEA_2,
	NUMPOT2ITEM
};
static const EXTREC_STRUCT Pot2Item[NUMPOT2ITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_POT2_GIRI_COCLEA,	IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_POT2_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_POT2_TOFF_COCLEA,	IsTOffCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_POT2_PORTATA,		IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_POT2_GIRI,			NULL,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_POT2_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_POT2_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_POT2_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_POT2_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU POTENZA 3
enum 
{
	POT3_ITEM_GIRI_COCLEA,
	POT3_ITEM_TON_COCLEA_1,
	POT3_ITEM_TOFF_COCLEA_1,
	POT3_ITEM_PORTATA,
	POT3_ITEM_GIRI,
	POT3_ITEM_ESPULSORE_2,
	POT3_ITEM_GIRI_2,
	POT3_ITEM_TON_COCLEA_2,
	POT3_ITEM_TOFF_COCLEA_2,
	NUMPOT3ITEM
};
static const EXTREC_STRUCT Pot3Item[NUMPOT3ITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_POT3_GIRI_COCLEA,	IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_POT3_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_POT3_TOFF_COCLEA,	IsTOffCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_POT3_PORTATA,		IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_POT3_GIRI,			NULL,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_POT3_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_POT3_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_POT3_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_POT3_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU POTENZA 4
enum 
{
	POT4_ITEM_GIRI_COCLEA,
	POT4_ITEM_TON_COCLEA_1,
	POT4_ITEM_TOFF_COCLEA_1,
	POT4_ITEM_PORTATA,
	POT4_ITEM_GIRI,
	POT4_ITEM_ESPULSORE_2,
	POT4_ITEM_GIRI_2,
	POT4_ITEM_TON_COCLEA_2,
	POT4_ITEM_TOFF_COCLEA_2,
	NUMPOT4ITEM
};
static const EXTREC_STRUCT Pot4Item[NUMPOT4ITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_POT4_GIRI_COCLEA,	IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_POT4_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_POT4_TOFF_COCLEA,	IsTOffCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_POT4_PORTATA,		IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_POT4_GIRI,			NULL,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_POT4_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_POT4_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_POT4_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_POT4_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU POTENZA 5
enum 
{
	POT5_ITEM_GIRI_COCLEA,
	POT5_ITEM_TON_COCLEA_1,
	POT5_ITEM_TOFF_COCLEA_1,
	POT5_ITEM_PORTATA,
	POT5_ITEM_GIRI,
	POT5_ITEM_ESPULSORE_2,
	POT5_ITEM_GIRI_2,
	POT5_ITEM_TON_COCLEA_2,
	POT5_ITEM_TOFF_COCLEA_2,
	NUMPOT5ITEM
};
static const EXTREC_STRUCT Pot5Item[NUMPOT5ITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_POT5_GIRI_COCLEA,	IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_POT5_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_POT5_TOFF_COCLEA,	IsTOffCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_POT5_PORTATA,		IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_POT5_GIRI,			NULL,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_POT5_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_POT5_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_POT5_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_POT5_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU POTENZA 6
enum 
{
	POT6_ITEM_GIRI_COCLEA,
	POT6_ITEM_TON_COCLEA_1,
	POT6_ITEM_TOFF_COCLEA_1,
	POT6_ITEM_PORTATA,
	POT6_ITEM_GIRI,
	POT6_ITEM_ESPULSORE_2,
	POT6_ITEM_GIRI_2,
	POT6_ITEM_TON_COCLEA_2,
	POT6_ITEM_TOFF_COCLEA_2,
	NUMPOT6ITEM
};
static const EXTREC_STRUCT Pot6Item[NUMPOT6ITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_POT6_GIRI_COCLEA,	IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_POT6_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_POT6_TOFF_COCLEA,	IsTOffCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_POT6_PORTATA,		IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_POT6_GIRI,			NULL,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_POT6_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_POT6_GIRI2,			IsEsp2Giri,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_POT6_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_POT6_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU PULIZIA
enum 
{
	PULIZIA_ITEM_GIRI_COCLEA,
	PULIZIA_ITEM_TON_COCLEA_1,
	PULIZIA_ITEM_TOFF_COCLEA_1,
	PULIZIA_ITEM_PORTATA,
	PULIZIA_ITEM_GIRI,
	PULIZIA_ITEM_ESPULSORE_2,
	PULIZIA_ITEM_GIRI_2,
	PULIZIA_ITEM_TON_COCLEA_2,
	PULIZIA_ITEM_TOFF_COCLEA_2,
	NUMPULIZIAITEM
};
static const EXTREC_STRUCT PulizItem[NUMPULIZIAITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_PULIZIA_GIRI_COCLEA,	IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_PULIZIA_TON_COCLEA,	IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_PULIZIA_TOFF_COCLEA,	IsTOffCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_PULIZIA_PORTATA,		IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_PULIZIA_GIRI,		NULL,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_PULIZIA_ATTUA_ESP2,	IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_PULIZIA_GIRI2,		IsEsp2Giri,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_PULIZIA_TON_FAN1,	IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_PULIZIA_TOFF_FAN1,	IsTOffCoclea2,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU INTERPOLAZIONE
enum 
{
	INTERP_ITEM_GIRI_COCLEA,
	INTERP_ITEM_TON_COCLEA_1,
	INTERP_ITEM_PORTATA,
	INTERP_ITEM_GIRI,
	INTERP_ITEM_ESPULSORE_2,
	INTERP_ITEM_GIRI_2,
	INTERP_ITEM_TON_COCLEA_2,
	NUMINTERPITEM
};
static const EXTREC_STRUCT InterpItem[NUMINTERPITEM] =
{
//  code,			   		IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_RPM2_INTERPOL,		IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_TON_COCLEA_INTERPOL,	IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_1,		LOGIN_SERVICE },
	{P_LPM_INTERPOL,		IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_PORTATA,			LOGIN_SERVICE },
	{P_RPM1_INTERPOL,		NULL,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI,				LOGIN_SERVICE },
	{P_ESP2_INTERPOL,		IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_RPM2_INTERPOL,		IsEsp2Giri,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_TON_FAN1_INTERPOL,	IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuAttuaPot,	d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
};

// parametri per MENU ATTUAZIONI DI POTENZA
enum
{
	ATTUAPOT_ITEM_PARAM_POTENZA_MENU,
	ATTUAPOT_ITEM_TEMPI_FUNZIONAMENTO,
	ATTUAPOT_ITEM_RITARDO_ATTUA_ESP_MENU,
	ATTUAPOT_ITEM_RITARDO_STEP_POT_MENU,
	ATTUAPOT_ITEM_PB_MENU,
	ATTUAPOT_ITEM_POT1_MENU,
	ATTUAPOT_ITEM_POT2_MENU,
	ATTUAPOT_ITEM_POT3_MENU,
	ATTUAPOT_ITEM_POT4_MENU,
	ATTUAPOT_ITEM_POT5_MENU,
	ATTUAPOT_ITEM_POT6_MENU,
	ATTUAPOT_ITEM_PULIZIA_MENU,
	ATTUAPOT_ITEM_INTERPOL_MENU,
	NUMATTUAPOTITEM
};
static const EXTREC_STRUCT AttuaPotItem[NUMATTUAPOTITEM] =
{
//  code,				IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,    			EscModFunc,	EscFunc,		wDispId							LivAccesso
	{SUBMENU_P_TYPE,	NULL,			ParPotItem,		/* NULL,*/		EnterMenuParPot,		NULL,       RetMenuTec, 	sPARAM_POTENZA_MENU,			LOGIN_MANUFACTURER },
	{SUBMENU_P_TYPE,	NULL,			TFunzItem,		/* NULL,*/		EnterMenuTFunz,			NULL,		RetMenuTec, 	sTEMPI_FUNZ_MENU,				LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,			RitEspItem,		/* NULL,*/		EnterMenuRitEsp,		NULL,       RetMenuTec, 	sRITARDO_ATTUA_ESP_MENU,		LOGIN_MANUFACTURER },
	{SUBMENU_P_TYPE,	NULL,			RitPotItem,		/* NULL,*/		EnterMenuRitPot,		NULL,       RetMenuTec, 	sRITARDO_STEP_POT_MENU,			LOGIN_MANUFACTURER },
	{SUBMENU_P_TYPE,	NULL,			PBItem,			/* NULL,*/		EnterMenuPB,			NULL,       RetMenuTec, 	d_PB_MENU,						LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	IsPot1,			Pot1Item,		/* NULL,*/		EnterMenuPot1,			NULL,       RetMenuTec, 	sPOT1_MENU,						LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	IsPot2,			Pot2Item,		/* NULL,*/		EnterMenuPot2,			NULL,       RetMenuTec, 	sPOT2_MENU,						LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	IsPot3,			Pot3Item,		/* NULL,*/		EnterMenuPot3,			NULL,       RetMenuTec, 	sPOT3_MENU,						LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	IsPot4,			Pot4Item,		/* NULL,*/		EnterMenuPot4,			NULL,       RetMenuTec, 	sPOT4_MENU,						LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	IsPot5,			Pot5Item,		/* NULL,*/		EnterMenuPot5,			NULL,       RetMenuTec, 	sPOT5_MENU,						LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	IsPot6,			Pot6Item,		/* NULL,*/		EnterMenuPot6,			NULL,       RetMenuTec, 	sPOT6_MENU,						LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,			PulizItem,		/* NULL,*/		EnterMenuPuliz,			NULL,       RetMenuTec, 	sPULIZIA_MENU,					LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,			InterpItem,		/* NULL,*/		EnterMenuInterp,		NULL,       RetMenuTec, 	sINTERPOLAZIONE_MENU,			LOGIN_SERVICE },
};

// parametri per MENU ALLARME FUMI
enum 
{
	ALMFUMI_ITEM_ALMFUMI_INIB_PREALM,
	ALMFUMI_ITEM_ALMFUMI_TEMP_PREALM,
	ALMFUMI_ITEM_ALMFUMI_TPREALM,	
	ALMFUMI_ITEM_GRAD_PREALM_FUMI,	
	ALMFUMI_ITEM_TEMP_ALLARME_FUMI,
	NUMALMFUMIITEM
};
static const EXTREC_STRUCT AlmFumiItem[NUMALMFUMIITEM] =
{
//  code,			   			IsToView,	  	VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,			EscFunc,			wDispId					LivAccesso
	{P_INIB_ALM_PREALM_FUMI,	NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAlarms,		d_ALMFUMI_INIB_PREALM,	LOGIN_MANUFACTURER },
	{P_TEMP_PREALM_FUMI,		NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAlarms,		d_ALMFUMI_TEMP_PREALM,	LOGIN_SERVICE },
	{P_MAX_TIME_PREALM_FUMI,	IsTMaxPreAlm,	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAlarms,		d_ALMFUMI_TPREALM,		LOGIN_MANUFACTURER },
	{P_GRAD_PREALM_FUMI,		NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAlarms,		d_GRAD_PREALM_FUMI,		LOGIN_MANUFACTURER },
	{P_TEMP_ALLARME_FUMI,		NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAlarms,		d_TEMP_ALLARME_FUMI,	LOGIN_SERVICE },
};

// parametri per MENU SENSORE PELLET
enum 
{
	SENSPLT_ITEM_SENSPELLET_TPREALM,
	NUMSENSPLTITEM
};
static const EXTREC_STRUCT SensPltItem[NUMSENSPLTITEM] =
{
//  code,			   				IsToView,	  VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,			EscFunc,			wDispId						LivAccesso
	{P_TIME_PREALM_SENS_PELLET,		NULL,         NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAlarms,		d_SENSPELLET_TPREALM,		LOGIN_SERVICE },
};

// parametri per MENU ASSENZA FIAMMA
// enum 
// {
	// NOFIAM_ITEM_NOFIAMMA_DELTA_TEMP,
	// NUMNOFIAMITEM
// };
// static const EXTREC_STRUCT NoFiamItem[NUMNOFIAMITEM] =
// {
// //  code,			   				IsToView,	  VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,				wDispId						LivAccesso
	// {P_DELTATEMP_NO_FIAMMA,			NULL,         NULL,			 	/* NULL,*/     NULL,	    		NULL,				RetMenuAlarms,		d_NOFIAMMA_DELTA_TEMP,	LOGIN_MANUFACTURER },
// };

// parametri per MENU SENSORE PORTATA ARIA
enum 
{
	SENSARIA_ITEM_PORTATA_CRITICA,
	SENSARIA_ITEM_TALM_PORTA,
	SENSARIA_ITEM_TPREALM_PORTA,
	SENSARIA_ITEM_INIBALM_ARIA_COMBUST,
	SENSARIA_ITEM_TPREALM_ARIA_COMBUST,
	SENSARIA_ITEM_DELTA_PORTATA_ARIA_COMBUST,
	NUMSENSARIAITEM
};
static const EXTREC_STRUCT SensAriaItem[NUMSENSARIAITEM] =
{
//  code,			   					IsToView,	  	VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,			EscFunc,			wDispId								LivAccesso
	{P_PORTATA_CRITICA,					NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAlarms,		d_PORTATA_CRITICA,					LOGIN_SERVICE },
	{P_TEMPO_ALLARME_PORTA,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAlarms,		d_TALM_PORTA,						LOGIN_MANUFACTURER },
	{P_TEMPO_PREALM_PORTA,				NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAlarms,		d_TPREALM_PORTA,					LOGIN_MANUFACTURER },
	{P_INIB_ALM_ARIACOBUST_INSUFF,		NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAlarms,		d_INIBALM_ARIA_COMBUST,				LOGIN_SERVICE },
	{P_TEMPO_PREALM_ARIA_COMBUST,		IsTPreA06,		NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAlarms,		d_TPREALM_ARIA_COMBUST,				LOGIN_MANUFACTURER },
	{P_DELTA_PORTATA,					NULL,         	NULL,			/* NULL,*/		NULL,	    	NULL,				RetMenuAlarms,		d_DELTA_PORTATA_ARIA_COMBUST,		LOGIN_MANUFACTURER },
};

// parametri per MENU GESTIONE ALLARMI
enum
{
	ALARMS_ITEM_ALLARME_FUMI_MENU,
	ALARMS_ITEM_SENSORE_PELLET_MENU,
	//ALARMS_ITEM_ASSENZA_FIAMMA_MENU,
	ALARMS_ITEM_SENSORE_ARIA_MENU,
	NUMALARMSITEM
};
static const EXTREC_STRUCT AlarmsItem[NUMALARMSITEM] =
{
//  code,				IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,    			EscModFunc,	EscFunc,		wDispId					LivAccesso
	{SUBMENU_P_TYPE,	NULL,			AlmFumiItem,	/* NULL,*/		EnterMenuAlmFumi,		NULL,       RetMenuTec, 	sALLARME_FUMI_MENU,		LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	IsSensPellet,	SensPltItem,	/* NULL,*/		EnterMenuSensPlt,		NULL,       RetMenuTec, 	sSENSORE_PELLET_MENU,	LOGIN_SERVICE },
	//{SUBMENU_P_TYPE,  NULL,			NoFiamItem,		/* NULL,*/		EnterMenuNoFiam,		NULL,       RetMenuTec, 	sASSENZA_FIAMMA_MENU,	LOGIN_MANUFACTURER },
	{SUBMENU_P_TYPE,	IsCtrlPortata,	SensAriaItem,	/* NULL,*/		EnterMenuSensAria,		NULL,       RetMenuTec, 	sSENSORE_ARIA_MENU,		LOGIN_SERVICE },
};

// parametri per MENU TEST COMANDI
enum 
{
	CMD_ITEM_TEST_MANUALE,
	CMD_ITEM_TEST_AUTOMATICO,
	CMD_ITEM_TEST_BYPASS,
	CMD_ITEM_TEST_CALIB_TC,
	CMD_ITEM_TEST_CALIB_FOTORES_ON,
	CMD_ITEM_TEST_CALIB_FOTORES_OFF,
	CMD_ITEM_TEST_DURATA_STEP,
	NUMCMDITEM
};
static const EXTREC_STRUCT CmdItem[NUMCMDITEM] =
{
//  code,			   							IsToView,	  		VoceExtIndex,	PreFunc,		ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{FUNCTION_TEST_TYPE,						IsCollaudoMan,		NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTest,		d_TEST_START_STOP,			LOGIN_SERVICE },
	{FUNCTION_TEST_AUTO_TYPE,					IsCollaudoAuto,		NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTest,		d_TEST_SEQUENZA_TEMP,		LOGIN_SERVICE },
	{SPECIALF_TYPE,								NULL,         		NULL,			/* NULL,*/		ConfBypass,		NULL,			RetMenuTest,		d_TEST_BYPASS,				LOGIN_SERVICE },
	{SPECIALF_TYPE,								IsTermocoppia,		NULL,			/* NULL,*/		ConfTaraTC,		NULL,			RetMenuTest,		d_TEST_CALIB_TC,			LOGIN_SERVICE },
	{FUNCTION_TEST_CALIB_FOTORES_ON_TYPE,		IsFotoresistenza,	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTest,		d_TEST_CALIB_FOTORES_ON,	LOGIN_SERVICE },
	{FUNCTION_TEST_CALIB_FOTORES_OFF_TYPE,		IsFotoresistenza,	NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTest,		d_TEST_CALIB_FOTORES_OFF,	LOGIN_SERVICE },
	{P_TRAFF_B,									NULL,         		NULL,			/* NULL,*/		NULL,	    	NULL,			RetMenuTest,		d_TEST_DURATA_STEP,			LOGIN_SERVICE },
};

// parametri per MENU TEST CARICHI
enum 
{
	LOADS_ITEM_TEST_COCLEA,
	LOADS_ITEM_TEST_CANDELETTA,
	LOADS_ITEM_TEST_FAN1,
	LOADS_ITEM_TEST_FAN2,
	LOADS_ITEM_TEST_FAN3,
	LOADS_ITEM_TEST_ESPULSORE,
	LOADS_ITEM_TEST_POMPA,
	LOADS_ITEM_TEST_3VIE,
	LOADS_ITEM_TEST_AUX_1,
	LOADS_ITEM_TEST_AUX_2,
	LOADS_ITEM_TEST_AUX_A,
	NUMLOADSITEM
};
static const EXTREC_STRUCT LoadsItem[NUMLOADSITEM] =
{
//  code,			   		IsToView,	  	VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_SPE_COCLEA,			NULL,         	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_TEST_COCLEA,				LOGIN_SERVICE },
	{P_SPE_CAND,			NULL,         	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_TEST_CANDELETTA,			LOGIN_SERVICE },
	{P_SPE_FAN1,			NULL,         	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_TEST_FAN1,				LOGIN_SERVICE },
	{P_SPE_FAN2,			NULL,         	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_TEST_FAN2,				LOGIN_SERVICE },
	{P_SPE_FAN3,			NULL,         	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_TEST_FAN3,				LOGIN_SERVICE },
	{P_SPE_ESP,				NULL,         	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_TEST_ESPULSORE,			LOGIN_SERVICE },
	{P_SPE_POMPA,			userIsIdro,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_TEST_POMPA,				LOGIN_SERVICE },
	{P_SPE_3VIE,			userIsIdro,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_TEST_3VIE,				LOGIN_SERVICE },
	{P_SPE_AUX1,			NULL,         	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_TEST_AUX_1,				LOGIN_SERVICE },
	{P_SPE_AUX2,			userIsIdro,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_TEST_AUX_2,				LOGIN_SERVICE },
	{P_SPE_AUXA,			NULL,         	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_TEST_AUX_A,				LOGIN_SERVICE },
};

// parametri per MENU TEST ATTUAZIONI
enum 
{
	ATTUA_ITEM_TEST_GIRI_COCLEA,
	ATTUA_ITEM_TEST_TON_COCLEA_1,
	ATTUA_ITEM_TEST_TOFF_COCLEA_1,
	ATTUA_ITEM_TEST_PORTATA,
	ATTUA_ITEM_TEST_GIRI,
	ATTUA_ITEM_TEST_ESPULSORE_2,
	ATTUA_ITEM_TEST_GIRI_2,
	ATTUA_ITEM_TEST_TON_COCLEA_2,
	ATTUA_ITEM_TEST_TOFF_COCLEA_2,
	ATTUA_ITEM_TEST_FAN_1,
	ATTUA_ITEM_TEST_FAN_2,
	ATTUA_ITEM_TEST_FAN_3,
	NUMATTUAITEM
};
static const EXTREC_STRUCT AttuaItem[NUMATTUAITEM] =
{
//  code,			   		IsToView,	  	VoceExtIndex,	PreFunc,			ExecFunc,     	EscModFunc,		EscFunc,			wDispId						LivAccesso
	{P_SPE_GIRI_COCLEA,		IsGiriCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_GIRICOCLEA,				LOGIN_MANUFACTURER },
	{P_SPE_TON_COCLEA,		IsTempoCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_ATTUA_TON_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_SPE_TOFF_COCLEA,		IsTOffCoclea,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_ATTUA_TOFF_COCLEA_1,		LOGIN_MANUFACTURER },
	{P_SPE_PORTATA,			IsCtrlPortata,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_ATTUA_PORTATA,			LOGIN_MANUFACTURER },
	{P_SPE_GIRI,			NULL,         	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_ATTUA_GIRI,				LOGIN_MANUFACTURER },
	{P_SPE_ATTUA_ESP2,		IsEsp2,			NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_ATTUA_ESP2,				LOGIN_MANUFACTURER },
	{P_SPE_GIRI2,			IsEsp2Giri,   	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_ATTUA_GIRI_2,				LOGIN_MANUFACTURER },
	{P_SPE_TON_FAN1,		IsCoclea2,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_ATTUA_TON_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_SPE_TOFF_FAN1,		IsTOffCoclea2,	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_ATTUA_TOFF_COCLEA_2,		LOGIN_MANUFACTURER },
	{P_SPE_ATTUA_FAN1,		IsFan1Test,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_ATTUA_FAN_1,				LOGIN_MANUFACTURER },
	{P_SPE_ATTUA_FAN2,		IsFan2Test,		NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_ATTUA_FAN_2,				LOGIN_MANUFACTURER },
	{P_SPE_ATTUA_FAN3,		NULL,         	NULL,			/* NULL,*/			NULL,	    	NULL,			RetMenuTest,		d_ATTUA_FAN_3,				LOGIN_MANUFACTURER },
};

// parametri per MENU COLLAUDO
enum
{
	TEST_ITEM_COMANDI_MENU,
	TEST_ITEM_CARICHI_MENU,
	TEST_ITEM_ATTUA_MENU,
	NUMTESTITEM
};
static const EXTREC_STRUCT TestItem[NUMTESTITEM] =
{
//  code,				IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,    			EscModFunc,	EscFunc,		wDispId				LivAccesso
	{SUBMENU_P_TYPE,	NULL,			CmdItem,		/* NULL,*/		EnterMenuCmd,			NULL,       RetMenuTec, 	sCOMANDI_MENU,		LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,			LoadsItem,		/* NULL,*/		EnterMenuLoads,			NULL,       RetMenuTec, 	sCARICHI_MENU,		LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	NULL,			AttuaItem,		/* NULL,*/		EnterMenuAttua,			NULL,       RetMenuTec, 	sATTUA_MENU,		LOGIN_MANUFACTURER },
};

// parametri per MENU TECNICO
enum tecnico
{
	TEC_ITEM_CONFIG_SYSTEM,
	TEC_ITEM_CONTROLLO,
	TEC_ITEM_IDRO_MENU,
	TEC_ITEM_ATTUA_TRANSITORIE,
	TEC_ITEM_ATTUA_POTENZA,
	TEC_ITEM_GESTIONE_ALLARMI,
	TEC_ITEM_COLLAUDO,
	TEC_ITEM_RICERCA_CODICE_PARAM,
	NUMTECITEM
};
static const EXTREC_STRUCT TecnicoItem[NUMTECITEM] =
{
//  code,				IsToView,		VoceExtIndex,		PreFunc,			ExecFunc,			EscModFunc,	EscFunc,     wDispId						LivAccesso
	{SUBMENU_M_TYPE,  	NULL,			SysCfgItem,			/* NULL,*/			EnterMenuSysCfg,  	NULL,     	RetMainMenu, sCONFIG_SYSTEM,			LOGIN_SERVICE },
	{SUBMENU_M_TYPE,	NULL,			CtrlItem,			/* NULL,*/			EnterMenuCtrl,		NULL,		RetMainMenu, sCONTROLLO,				LOGIN_SERVICE },
	{SUBMENU_P_TYPE,	userIsIdro,		IdroItem,			/* NULL,*/			EnterMenuIdro,		NULL,		RetMainMenu, d_IDRO_MENU,				LOGIN_SERVICE },
	{SUBMENU_M_TYPE,	NULL,			AttuaTraItem,		/* NULL,*/			EnterMenuAttuaTra,	NULL,		RetMainMenu, sATTUA_TRANSITORIE,		LOGIN_SERVICE },
	{SUBMENU_M_TYPE,	NULL,			AttuaPotItem,		/* NULL,*/			EnterMenuAttuaPot,	NULL,		RetMainMenu, sATTUA_POTENZA,			LOGIN_SERVICE },
	{SUBMENU_M_TYPE,	NULL,			AlarmsItem,			/* NULL,*/			EnterMenuAlarms,	NULL,		RetMainMenu, sGESTIONE_ALLARMI,			LOGIN_SERVICE },
	{SUBMENU_M_TYPE,	NULL,			TestItem,			/* NULL,*/			EnterMenuTest,		NULL,		RetMainMenu, sCOLLAUDO,					LOGIN_SERVICE },
	{SPECIALF_TYPE,		NULL,			NULL,				/* NULL,*/			EnterVisParCod,		NULL,		RetMainMenu, sRICERCA_CODICE_PARAM,		LOGIN_SERVICE },
};

// parametri per MENU PRINCIPALE
enum main_menu
{
	MENU_ITEM_IMPOSTAZIONI,
	MENU_ITEM_TECNICO,
	NUM_MENU_ITEM
};
static const EXTREC_STRUCT MenuItem[NUM_MENU_ITEM] =
{
//  code,					IsToView,		VoceExtIndex,	PreFunc,			ExecFunc,			EscModFunc,	EscFunc,		wDispId,			LivAccesso
	{SUBMENU_P_TYPE,		NULL,			SettingsItem,	/* NULL,*/			EnterMenuSettings,	NULL,       userVisAvvio,	MENU_IMPOSTAZIONI,	LOGIN_FREE },
	{SUBMENU_M_TYPE,		NULL,			TecnicoItem,	/* NULL,*/			userVisHidPass,		NULL,       userVisAvvio,	MENU_TECNICO,		LOGIN_FREE },
};

// parametri per MENU IMPOSTAZIONI LOCALI
enum localsettings
{
	LOCALSETTINGS_ITEM_CODICE_PANNELLO,
	LOCALSETTINGS_ITEM_LINGUA,
	LOCALSETTINGS_ITEM_DATA,
	LOCALSETTINGS_ITEM_ORA,
	LOCALSETTINGS_ITEM_BACKLIGHT,
	//LOCALSETTINGS_ITEM_USER_COLOR_ID,
	//LOCALSETTINGS_ITEM_TONE,
	LOCALSETTINGS_ITEM_RFID,
	LOCALSETTINGS_ITEM_SHOWTEMP,
	NUMLOCALSETTINGSITEM
};
static const EXTREC_STRUCT LocalSettingsItem[NUMLOCALSETTINGSITEM] =
{
//  code,					IsToView,		VoceExtIndex,	PreFunc,		ExecFunc,			EscModFunc,		EscFunc,		wDispId,				LivAccesso
	{P_CODICE_PANNELLO,		NULL,			NULL,			/* NULL,*/      NULL,				NULL,        	userVisInit,	d_CODICE_PANNELLO,		LOGIN_FREE },
	{P_LINGUA,				NULL,          	NULL,			/* NULL,*/		ExecUpdateLang,		NULL,			userVisInit,	d_LINGUA,				LOGIN_FREE },
	{SPECIAL_DATE_TYPE,		NULL,			NULL,			/* NULL,*/		NULL,				NULL,       	userVisInit,	d_DATE,					LOGIN_FREE },
	{SPECIAL_TIME_TYPE,		NULL,			NULL,			/* NULL,*/		NULL,				NULL,       	userVisInit,	d_HOUR,					LOGIN_FREE },
	{P_BACKLIGHT,			NULL,          	NULL,			/* NULL,*/		NULL,			   	NULL,       	userVisInit,	d_BACKLIGHT,			LOGIN_FREE },
	//{P_USER_COLOR_ID,		NULL,          	NULL,			/* NULL,*/		NULL,			   	NULL,       	userVisInit,,	d_BACKLIGHT,			LOGIN_FREE },
	// {P_TONE,				NULL,          	NULL,			/* NULL,*/		NULL,			   	NULL,       	userVisInit,	d_TONE,					LOGIN_FREE },
	{P_RFPANEL,				NULL,          	NULL,			/* NULL,*/		ExecLocalSetRFid,  	NULL,       	userVisInit,	d_RFPANEL,				LOGIN_FREE },
	{P_SHOWTEMP,	 		NULL,          	NULL,			/* NULL,*/		ExecSetTempUM,   	NULL,			userVisInit,	d_SHOWTEMP,				LOGIN_FREE },
};


/*!
** \fn short GetRealParCode( short code )
** \brief Eventuale recupero vero codice parametro da codice voce menu'
** \param code Il codice voce menu'
** \return Il vero codice del parametro
**/
static short GetRealParCode( short code )
{
	switch( code )
	{
		case FUNCTION_ATTIVA_COCLEA_TYPE:
			code = P_ATTIVA_COCLEA;
		break;
		
		case FUNCTION_ATTIVA_ESP_TYPE:
			code = P_ATTIVA_ESP;
		break;

		case FUNCTION_ATTIVA_POMPA_TYPE:
			code = P_ATTIVA_POMPA;
		break;

		case FUNCTION_TEST_TYPE:
			code = P_TEST;
		break;

		case FUNCTION_TEST_AUTO_TYPE:
			code = P_TEST_AUTO;
		break;

		case FUNCTION_TEST_CALIB_FOTORES_ON_TYPE:
			code = P_TEST_CALIB_FOTORES_ON;
		break;

		case FUNCTION_TEST_CALIB_FOTORES_OFF_TYPE:
			code = P_TEST_CALIB_FOTORES_OFF;
		break;
		
		case SPECIAL_WIFI_SSID_TYPE:
			code = P_WIFI_SSID;
		break;
		
		case SPECIAL_WIFI_KEYW_TYPE:
			code = P_WIFI_KEYW;
		break;
		
		case SPECIAL_WPS_PIN_TYPE:
			code = P_WPS_PIN;
		break;
		
		case SPECIAL_WIFI_PORT_TYPE:
			code = P_WIFI_PORT;
		break;
	}
	
	return code;
}

/*! 
	\fn short CheckMenuAccess( const EXTREC_STRUCT *ExtRec, unsigned char nItem )
	\brief Verifica l'accessibilita` di un menu`
	\param ExtRec Tabella dei dati di gestione degli elementi
	\param nItem Numero degli elementi nella tabella
	\return 1: menu` accessibile, 0: menu` non accessibile
*/
short CheckMenuAccess( const EXTREC_STRUCT *ExtRec, unsigned char nItem )
{
	short RetVal = 0;
	unsigned char i;
	
	for( i=0; i<nItem; i++ )
	{
		if( LivAccesso >= ExtRec[i].LivAccesso )
		{
			if( ExtRec[i].IsToView != NULL )
			{
				if( ExtRec[i].IsToView() )
				{
					RetVal = 1;
					break;
				}
			}
			else
			{
				RetVal = 1;
				break;
			}
		}
	}
	
	return RetVal;
}

/*! 
	\fn short userIsWarning( void )
	\brief Verifica la presenza di anomalie da visualizzare
	\return 0: nessuna anomalia, 1: una o piu' anomalie presenti
*/
short userIsWarning( void )
{
	short RetVal = 0;

	RetVal = CheckMenuAccess( WarnItem, NUMWARNITEM );

	return RetVal;
}

/*! 
	\fn unsigned char userVisHidPass( void )
   \brief Visualizzazione della richiesta password per Menu` Tecnico
	\return Il codice dello stato di transizione
*/
static unsigned char userVisHidPass( void )
{
	unsigned char RetVal = userCurrentState;
	unsigned char ByHidPW_xx;
	unsigned char ByHidPW_00;
	unsigned char loop = 1;

	while( loop )
	{
		ByHidPW_xx = PASS1_MIN;
		ByHidPW_00 = PASS2_MIN;
		if( WinPassword( 
							(char *)GetMsg( 2, NULL, iLanguage, HID_PASSWORD ),
							&ByHidPW_xx, 
							&ByHidPW_00 
							) )
		{
			// password inserita
			userGetServicePassword( &userParValue.b[2], &userParValue.b[3] );	// determina la password di accesso assistenza
			if( ((ByHidPW_xx == PASSWORD_xx) && (ByHidPW_00 == PASSWORD_00)) ||
				((ByHidPW_xx == pByPassLettera) && (ByHidPW_00 == pByPassCodice)) 
				)
			{
				// password corretta: passo al menu nascosto.
				LivAccesso = LOGIN_MANUFACTURER;	// login come costruttore
				RetVal = EnterMenuTec();
				loop = 0;	// exit
			}
			else if( (ByHidPW_xx == userParValue.b[2]) && (ByHidPW_00 == userParValue.b[3]) )
			{
				// password corretta: passo al menu nascosto.
				LivAccesso = LOGIN_SERVICE;	// login come assistenza
				RetVal = EnterMenuTec();
				loop = 0;	// exit
			}
			else
			{
				// password non corretta: ripeto richiesta password.
			}
		}
		else
		{
			// ritorno al menu livello superiore
			RetVal = RetMainMenu();
			loop = 0;	// exit
		}
	}
	
	return RetVal;
}

/*!
	\fn unsigned char ConfTipoDef( void )
   \brief Attiva la procedura di conferma Ripristino Tipo Stufa al Default
	\return Il codice dello stato di transizione
*/
static unsigned char ConfTipoDef( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( WinConfirm(
						(char *)GetMsg( 2, NULL, iLanguage, REQCONF ),
						NULL
						) )
	{
		RetVal = ExecTipoDef();
	}
	else
	{
		//RetVal = RetMenuGen();
	}
	
	return RetVal;
}

/*!
	\fn unsigned char ConfBypass( void )
	\brief Attiva la procedura di conferma Bypass
	\return Il codice dello stato di transizione
*/
static unsigned char ConfBypass( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( WinConfirm(
						(char *)GetMsg( 2, NULL, iLanguage, REQCONF ),
						NULL
						) )
	{
		RetVal = ExecBypass();
	}
	else
	{
		//RetVal = RetMenuCmd();
	}
	
	return RetVal;
}

/*!
	\fn unsigned char ConfBypassTra( void )
   \brief Attiva la procedura di conferma Bypass
	\return Il codice dello stato di transizione
*/
static unsigned char ConfBypassTra( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( WinConfirm(
						(char *)GetMsg( 2, NULL, iLanguage, REQCONF ),
						NULL
						) )
	{
		RetVal = ExecBypassTra();
	}
	else
	{
		//RetVal = RetMenuParTra();
	}
	
	return RetVal;
}

/*!
	\fn unsigned char ConfTaraTC( void )
   \brief Attiva la procedura di taratura termocoppia
	\return Il codice dello stato di transizione
*/
static unsigned char ConfTaraTC( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( WinConfirm(
						(char *)GetMsg( 2, NULL, iLanguage, REQCONF ),
						(char *)GetMsg( 3, NULL, iLanguage, TC_REQCONF )
						) )
	{
		RetVal = ExecTaraTC();
	}
	else
	{
		//RetVal = RetMenuCmd();
	}
	
	return RetVal;
}

/*!
	\fn unsigned char ConfOreSAT( void )
   \brief Attiva la procedura di conferma azzeramento ore service
	\return Il codice dello stato di transizione
*/
static unsigned char ConfOreSAT( void )
{
	unsigned char RetVal = userCurrentState;
	char s[DISPLAY_MAX_COL+1];
	
	if( WinConfirm(
						(char *)GetMsg( 2, NULL, iLanguage, REQCONF ),
						(char *)GetMsg( 3, NULL, iLanguage, AZZERA )
						) )
	{
		// ore service solo azzerabili
		userParValue.w[0] = 0;
		// parameter code as title
		userGetParCode( P_ORESAT );
		strcpy( s, str2 );
		if( WinSetParWaitResult( 
										P_ORESAT,
										s,
										(char *)GetMsg( 2, NULL, iLanguage, d_ORESAT )
									) == 1 )
		{
		}
		//RetVal = RetMenuAttuaPot();
	}
	else
	{
		//RetVal = RetMenuAttuaPot();
	}
	
	return RetVal;
}

/*! 
	\fn void VisMenu( unsigned char MenuType, const char *Title,
							const EXTREC_STRUCT *ExtRec, unsigned char nItem )
	\brief Visualizzazione di un menu`
	\param MenuType Tipo menu`
	\param Title Stringa del titolo
	\param ExtRec Tabella dei dati di gestione degli elementi
	\param nItem Numero degli elementi nella tabella
*/
static void VisMenu( unsigned char MenuType, const char *Title,
							const EXTREC_STRUCT *ExtRec, unsigned char nItem )
{
	SETUP_MENU *menu = &Setup_Menu[iLivMenu];
	unsigned char i, n;
	
	userSubState = 0;		// stato selezione
	
	// inizializza struttura menu`
	menu->MenuType = MenuType;
	if( Title != NULL )
		menu->title = Title;
	menu->ExtRec = ExtRec;
	// il menu` si suppone avere gia` almeno un elemento accessibile
	for( i=0,n=0; i<nItem; i++ )
	{
		if( LivAccesso >= ExtRec[i].LivAccesso )
		{
			if( ExtRec[i].IsToView != NULL )
			{
				if( ExtRec[i].IsToView() )
				{
					IndexVisItem[n++] = i;
				}
			}
			else
			{
				IndexVisItem[n++] = i;
			}
		}
	}
	menu->num_elements = n;
	
	nMenuPage = menu->num_elements / nItemPage[MenuType];
	if( menu->num_elements % nItemPage[MenuType] )
		nMenuPage++;
		
	// check selezione corrente
	if(
		(menu->current_page >= nMenuPage) ||
		(menu->current_selection >= menu->num_elements)
		)
	{
		// reset selezione corrente
		menu->current_page = 0;
		menu->current_selection = 0;
	}

	setup_menu_init( menu );

}

/*! 
	\fn short EnterMenu( unsigned char MenuType, unsigned short iStrTitle,
								const EXTREC_STRUCT *ExtRec, unsigned char nItem )
	\brief Procedura di ingresso in un menu`
	\param MenuType Tipo menu`
	\param iStrTitle Indice stringa del titolo
	\param ExtRec Tabella dei dati di gestione degli elementi
	\param nItem Numero degli elementi nella tabella
	\return 1: ingresso effettuato con successo, 0: altrimenti
*/
static short EnterMenu( unsigned char MenuType, unsigned short iStrTitle,
								const EXTREC_STRUCT *ExtRec, unsigned char nItem )
{
	short RetVal = 0;
	char *sTitle;
	short i;
	
	if( CheckMenuAccess( ExtRec, nItem ) )
	{
		iLivMenu++;

		// reset Setup Menu struct
		Setup_Menu[iLivMenu].current_page = 0;
		Setup_Menu[iLivMenu].current_selection = 0;
		
		sTitle = sMenuTitle[iLivMenu];
		strncpy( sTitle, GetMsg( 1, NULL, iLanguage, iStrTitle ), DISPLAY_MAX_COL );
		if( iStrTitle == PRGSETTIMANALE )
		{
			i = strlen( sTitle );
			sTitle[i++] = cBLANK;
			sTitle[i++] = Hex2Ascii( iCronoPrg+1 );
			sTitle[i] = '\0';
		}
		sTitle[DISPLAY_MAX_COL] = '\0';
		VisMenu( MenuType, sTitle, ExtRec, nItem );
		
		userCounterTime = USER_TIMEOUT_SHOW_20;
		userNextPointer = userVisAvvio;	// visualizzazione a livello superiore
		RetVal = 1;
	}
	
	return RetVal;
}

/*! 
	\fn void RetMenu( unsigned char MenuType,
							const EXTREC_STRUCT *ExtRec, unsigned char nItem )
	\brief Procedura di ritorno ad un menu`
	\param MenuType Tipo menu`
	\param ExtRec Tabella dei dati di gestione degli elementi
	\param nItem Numero degli elementi nella tabella
*/
static void RetMenu( unsigned char MenuType,
							const EXTREC_STRUCT *ExtRec, unsigned char nItem )
{
	if( iLivMenu )
		iLivMenu--;
	
	VisMenu( MenuType, NULL, ExtRec, nItem );
	
	userCounterTime = USER_TIMEOUT_SHOW_20;
	userNextPointer = userVisAvvio;	// visualizzazione a livello superiore
}

/*! 
	\fn void setup_menu_init( SETUP_MENU *menu )
	\brief Inizializza il menu' di setup
	\param menu Struttura menu' setup
*/
static void setup_menu_init( SETUP_MENU *menu )
{
	if( m_bmhdrQUIT == NULL )
	{
		// push bitmap ESC into Graphic RAM
		if( res_bm_Load( BITMAP_ESC, res_bm_GetGRAM() ) == 0 )
		{
		#ifdef LOGGING
			myprintf( sBitmapLoadError, BITMAP_ESC );
		#endif
			return;
		}
		m_bmhdrQUIT = res_bm_GetHeader( BITMAP_ESC );
	}
	if( m_bmhdrCHECKED == NULL )
	{
		// push bitmap CHECKED into Graphic RAM
		if( res_bm_Load( BITMAP_CHECKED, res_bm_GetGRAM() ) == 0 )
		{
		#ifdef LOGGING
			myprintf( sBitmapLoadError, BITMAP_CHECKED );
		#endif
			return;
		}
		m_bmhdrCHECKED = res_bm_GetHeader( BITMAP_CHECKED );
	}
	if( m_bmhdrUNCHECKED == NULL )
	{
		// push bitmap UNCHECKED into Graphic RAM
		if( res_bm_Load( BITMAP_UNCHECKED, res_bm_GetGRAM() ) == 0 )
		{
		#ifdef LOGGING
			myprintf( sBitmapLoadError, BITMAP_UNCHECKED );
		#endif
			return;
		}
		m_bmhdrUNCHECKED = res_bm_GetHeader( BITMAP_UNCHECKED );
	}
	
	m_tagval = TOUCH_TAG_INIT;
	m_scrollON = 0;
	m_key_cnt = 0;
	Flag.Key_Detect = 0;
	
	// Draw menu options below
	setup_menu_draw( menu, 0 );
	m_redraw = 1;
}

/*! 
	\fn short setup_menu_process_key( SETUP_MENU *menu, unsigned char keycode )
	\brief Aggiorna il menu' setup secondo il tasto premuto dall'utente
	\param menu Struttura menu' setup
	\param keycode Codice tasto premuto
	\return L'indice della voce del men� selezionata oppure il codice di stato del menu'.
*
static short setup_menu_process_key( SETUP_MENU *menu, unsigned char keycode )
{
	switch (keycode) 
	{
		case MENU_KEYCODE_DOWN:
			if( menu->current_selection < (menu->num_elements - nItemPage[menu->MenuType]) )
			{
				menu->current_selection++;
				// Update menu on display
				setup_menu_draw( menu, 0 );
			}				
			// Nothing selected yet
			return MENU_EVENT_IDLE;
			
		case MENU_KEYCODE_UP:
			if( menu->current_selection )
			{
				menu->current_selection--;
				// Update menu on display
				setup_menu_draw( menu, 0 );
			}				
			// Nothing selected yet
			return MENU_EVENT_IDLE;
			
		case MENU_KEYCODE_ENTER:
			// Got what we want. Return selection.
			return menu->current_selection;
			
		case MENU_KEYCODE_BACK:
			// User pressed "back" key, inform user
			return MENU_EVENT_EXIT;
			
		default:
			// Unknown key event
			return MENU_EVENT_IDLE;
	}
}

/*! 
	\fn void setup_menu_draw( SETUP_MENU *menu, short redraw )
	\brief Riscrive il menu' setup secondo la selezione corrente
	\param menu Struttura menu' setup
	\param redraw	0: riscrive tutta la pagina
						-1: selezione item precedente
						+1: selezione item successivo
*/
static void setup_menu_draw( SETUP_MENU *menu, short redraw )
{
	short i,j;
	ft_uint16_t But_opt;
	char s[MAX_LEN_STRINGA+1];
	short l;
	short code;
	unsigned char flag = 0;
	ft_uint8_t font;
	
	if( redraw == 0 )
	{
		// clear screen if we have changed the page or menu and prepare redraw
		//m_tagval = TOUCH_TAG_INIT;
		// reset al primo item di pagina dell'indice item per richieste valore/limiti
		iItemReq = menu->current_selection;
		// reset visualizzazione consumo
		VIS_CONSUMO = 0;	// visualizza consumo accumulato
		// reset visualizzazione accensioni
		VIS_ACC = 0;	// visualizza numero accensioni
	}

	if( m_tagval == TOUCH_TAG_INIT )
	{
		// it's the first time: tag registration
		CTP_TagDeregisterAll();
	}

	if( !((m_tagval == TAG_VERT_SCROLL) && (m_scrollON == 1)) )
	{
		switch( menu->MenuType )
		{
			case MENU_TYPE_LIST:
				Ft_Gpu_CoCmd_Dlstart(pHost);
				Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
				Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
				// menu item
				for( i=0; (i<nItemPage[menu->MenuType])&&(menu->current_selection+i<menu->num_elements); i++ )
				{
					l = IndexVisItem[menu->current_selection+i];	// indice voce in tabella;
					if( m_tagval == TOUCH_TAG_INIT )
					{
						TagRegister( i+1, 0, 31+i*(26+21+1), 469, (26+21+1) );
					}
					if( (m_tagval == i+1) && (m_key_cnt >= VALID_TOUCHED_KEY) )
					{
						// stringa evidenziata WHITE
						Ft_App_WrCoCmd_Buffer(pHost, COLOR_RGB(0xFF,0xFF,0xFF) );
						Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
						Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (31+i*(26+21+1))*16 ) );
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 469*16, (78+i*(26+21+1))*16 ) );
						// colore stringa evidenziata
						Ft_App_WrCoCmd_Buffer(pHost, Get_UserColorHex(UserColorId) );	// USER COLOR
					}
					else
					{
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
					}
					/* stringa (troncato a MENU_MAX_COL caratteri) *
					strncpy( s, 
								GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId ),
								MENU_MAX_COL );
					s[MENU_MAX_COL] = '\0';	**/
					/* stringa */
					GetMsg( 0, s, iLanguage, menu->ExtRec[l].wDispId );
					j = strlen( s );
					for( ; GetStringWidth( s, fontDesc ) > 470; )
					{
						// troncamento stringa
						j--;
						s[j] = '\0';
					}
					Ft_Gpu_CoCmd_Text( pHost, 0, 31+13+i*(26+21+1), fontDesc, 0, String2Font(s, fontDesc) );
					if( i != 4 )
					{
						// separator
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(16,16,16));	// GREY-BLACK
						Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (78+i*(26+21+1))*16 ) );
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 469*16, (78+i*(26+21+1))*16 ) );
					}
				}
			break;
			
			case MENU_TYPE_USER_PARAM:
				Ft_Gpu_CoCmd_Dlstart(pHost);
				Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
				Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
				// menu item
				for( i=0; (i<nItemPage[menu->MenuType])&&(menu->current_selection+i<menu->num_elements); i++ )
				{
					l = IndexVisItem[menu->current_selection+i];	// indice voce in tabella;
					code = menu->ExtRec[l].code;	// indice parametro
					if( m_tagval == TOUCH_TAG_INIT )
					{
						TagRegister( i+1, 0, 31+i*(26+21+1), 469, (26+21+1) );
					}
					if( (m_tagval == i+1) && (m_key_cnt >= VALID_TOUCHED_KEY) )
					{
						// stringa evidenziata WHITE
						Ft_App_WrCoCmd_Buffer(pHost, COLOR_RGB(0xFF,0xFF,0xFF) );
						Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
						Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (31+i*(26+21+1))*16 ) );
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 469*16, (78+i*(26+21+1))*16 ) );
						// colore stringa evidenziata
						Ft_App_WrCoCmd_Buffer(pHost, Get_UserColorHex(UserColorId) );	// USER COLOR
					}
					else
					{
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
					}
					/* descrizione parametro (troncato a MENU_MAX_COL caratteri) *
					strncpy( s, 
								GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId ),
								MENU_MAX_COL );
					s[MENU_MAX_COL] = '\0';	**/
					/* descrizione parametro */
					if(
						(menu->ExtRec == InfoItem) &&
						(code == P_NUM_ACC)
						)
					{
						// allinea la stringa descrittiva numero accensioni / numero mancate accensioni
						if( VIS_ACC )
						{
							// numero mancate accensioni
							GetMsg( 0, s, iLanguage, ALARMSOURCE01 );
						}
						else
						{
							// numero accensioni
							GetMsg( 0, s, iLanguage, menu->ExtRec[l].wDispId );
						}
					}
					else
					{
						GetMsg( 0, s, iLanguage, menu->ExtRec[l].wDispId );
					}
					j = strlen( s );
					/*!
					** Some parameters must be viewed only with latin alphabet
					**/
					font = fontDesc;
					if( iFontGroup == FONT_GROUP_CYRILLIC )
					{
						if( (code == SPECIAL_WIFI_KEYW_TYPE) ||
							(code == SPECIAL_WPS_PIN_TYPE) ||
							(code == SPECIAL_WIFI_SSID_TYPE) ||
							(code == SPECIAL_WIFI_PORT_TYPE) ||
							(code == P_EN_WIFI) )
						{
							font = FONT_DESC_DEF;
						}
					}
					for( ; GetStringWidth( s, font ) > 470; )
					{
						// troncamento descrizione parametro
						j--;
						s[j] = '\0';
					}
					Ft_Gpu_CoCmd_Text( pHost, 0, 31+i*(26+21+1), font, 0, String2Font(s, font) );
					// valore parametro
					if(
							(menu->ExtRec == InfoItem) &&
							(code == P_ACC_CONSUMO) &&
							VIS_CONSUMO
						)
					{
						// sto visualizzando il consumo orario
						code = P_CONSUMO;
					}
					else if(
							(menu->ExtRec == InfoItem) &&
							(code == P_NUM_ACC) &&
							VIS_ACC
						)
					{
						// sto visualizzando il numero mancate accensioni
						code = P_NUM_NO_ACC;
					}
					switch( code )
					{
						case SUBMENU_P_TYPE:		// la voce e' un sub menu param
						case SUBMENU_M_TYPE:		// la voce e' un sub menu menu
						case SPECIALF_TYPE:		// la voce e' di tipo special function
						case SPECIAL_WIFI_KEYW_TYPE:	// non si visualizza il valore a menu'
						case SPECIAL_WPS_PIN_TYPE:	// non si visualizza il valore a menu'
							strcpy( str2, sSubMenu );
						break;
						
						case SPECIAL_WIFI_SSID_TYPE:
						case SPECIAL_WIFI_PORT_TYPE:
							// valore come stringa lunga (troncato a MENU_MAX_COL caratteri)
							userVisEPar( code );
							strncpy( str2, strPar, MENU_MAX_COL );
							str2[MENU_MAX_COL] = '\0';
						break;
						
						case SPECIAL_DATE_TYPE:
							GetMsg( 0, str2, iLanguage, sWeekDay_tab[DateTime.tm_wday] );
							//str2[4] = '\0';	// only the first 3 characters for week day
							strcat( str2, " " );
							ScriviData( &str2[strlen(str2)] );
						break;
						
						case SPECIAL_TIME_TYPE:
							ScriviOra( str2 );
							str2[strlen(str2)-3] = '\0';	// do not display seconds
						break;
						
						default:
							if( (code != FUNCTION_ATTIVA_COCLEA_TYPE) &&
								(code != FUNCTION_ATTIVA_ESP_TYPE) &&
								(code != FUNCTION_ATTIVA_POMPA_TYPE) &&
								(code != FUNCTION_TEST_TYPE) &&
								(code != FUNCTION_TEST_AUTO_TYPE) &&
								(code != FUNCTION_TEST_CALIB_FOTORES_ON_TYPE) &&
								(code != FUNCTION_TEST_CALIB_FOTORES_OFF_TYPE) 
								)
							{
								userGetPar( code );
								userVisPar( code );
							}
						break;
					}
					if( (code == FUNCTION_ATTIVA_COCLEA_TYPE) ||
						(code == FUNCTION_ATTIVA_ESP_TYPE) ||
						(code == FUNCTION_ATTIVA_POMPA_TYPE) ||
						(code == FUNCTION_TEST_TYPE) ||
						(code == FUNCTION_TEST_AUTO_TYPE) ||
						(code == FUNCTION_TEST_CALIB_FOTORES_ON_TYPE) ||
						(code == FUNCTION_TEST_CALIB_FOTORES_OFF_TYPE) 
						)
					{
						// function
						switch( code )
						{
							case FUNCTION_ATTIVA_COCLEA_TYPE:
								flag = PRECARICA_PLT;
							break;
							
							case FUNCTION_ATTIVA_ESP_TYPE:
								//flag = AVVIA_PULIZIA;	con la precarica-pellet si attiva anche l'espulsore
								flag = PULIZIA;
							break;
							
							case FUNCTION_ATTIVA_POMPA_TYPE:
								flag = AVVIA_POMPA;
							break;
							
							case FUNCTION_TEST_TYPE:
								flag = TEST_STUFA;
							break;
							
							case FUNCTION_TEST_AUTO_TYPE:
								flag = TEST_AUTO;
							break;
							
							case FUNCTION_TEST_CALIB_FOTORES_ON_TYPE:
								flag = CALIB_FOTORES_ON;
							break;
							
							case FUNCTION_TEST_CALIB_FOTORES_OFF_TYPE:
								flag = CALIB_FOTORES_OFF;
							break;
						}
						/* Draw toggle bar with 3d effect *
						Ft_Gpu_CoCmd_FgColor(pHost,0xffff00);
						if( flag )
							Ft_Gpu_CoCmd_BgColor(pHost, 0x00C000);
						else
							Ft_Gpu_CoCmd_BgColor(pHost, 0x808080);
						Ft_Gpu_CoCmd_Toggle(pHost, (469-50), (31+10+i*(26+21+1)), 30, FONT_DESC_DEF, 0, 65535*(1-flag)," I ""\xFF"" O "); */
						// start drawing bitmap
						Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
						if( flag )
						{
							// checked
							// specify the starting address of the bitmap in graphics RAM
							Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(m_bmhdrCHECKED->GramAddr));
							// specify the bitmap format, linestride and height
							Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(m_bmhdrCHECKED->bmhdr.Format, m_bmhdrCHECKED->bmhdr.Stride, m_bmhdrCHECKED->bmhdr.Height));
							// set filtering, wrapping and on-screen size
							Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, 30, 30));	//scale to 30x30
							Ft_Gpu_CoCmd_LoadIdentity(pHost);
							Ft_Gpu_CoCmd_Scale(pHost, 65536*30L/m_bmhdrCHECKED->bmhdr.Width, 65536*30L/m_bmhdrCHECKED->bmhdr.Height);	//scale to 30x30
							Ft_Gpu_CoCmd_SetMatrix(pHost );
						}
						else
						{
							// unchecked
							// specify the starting address of the bitmap in graphics RAM
							Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(m_bmhdrUNCHECKED->GramAddr));
							// specify the bitmap format, linestride and height
							Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(m_bmhdrUNCHECKED->bmhdr.Format, m_bmhdrUNCHECKED->bmhdr.Stride, m_bmhdrUNCHECKED->bmhdr.Height));
							// set filtering, wrapping and on-screen size
							Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, 30, 30));	//scale to 30x30
							Ft_Gpu_CoCmd_LoadIdentity(pHost);
							Ft_Gpu_CoCmd_Scale(pHost, 65536*30L/m_bmhdrUNCHECKED->bmhdr.Width, 65536*30L/m_bmhdrUNCHECKED->bmhdr.Height);	//scale to 30x30
							Ft_Gpu_CoCmd_SetMatrix(pHost );
						}
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (469-50)*16, (31+9+i*(26+21+1))*16 ) );
						Ft_Gpu_CoCmd_LoadIdentity(pHost);
						Ft_Gpu_CoCmd_Scale(pHost, 65536, 65536);	//scale to normal
						Ft_Gpu_CoCmd_SetMatrix(pHost );
					}
					else if( (code >= 0) && userIsFlagVar( code ) )
					{
						// flag
						// start drawing bitmap
						Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
						if( userParValue.b[0] )
						{
							// checked
							// specify the starting address of the bitmap in graphics RAM
							Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(m_bmhdrCHECKED->GramAddr));
							// specify the bitmap format, linestride and height
							Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(m_bmhdrCHECKED->bmhdr.Format, m_bmhdrCHECKED->bmhdr.Stride, m_bmhdrCHECKED->bmhdr.Height));
							// set filtering, wrapping and on-screen size
							Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, 30, 30));	//scale to 30x30
							Ft_Gpu_CoCmd_LoadIdentity(pHost);
							Ft_Gpu_CoCmd_Scale(pHost, 65536*30L/m_bmhdrCHECKED->bmhdr.Width, 65536*30L/m_bmhdrCHECKED->bmhdr.Height);	//scale to 30x30
							Ft_Gpu_CoCmd_SetMatrix(pHost );
						}
						else
						{
							// unchecked
							// specify the starting address of the bitmap in graphics RAM
							Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(m_bmhdrUNCHECKED->GramAddr));
							// specify the bitmap format, linestride and height
							Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(m_bmhdrUNCHECKED->bmhdr.Format, m_bmhdrUNCHECKED->bmhdr.Stride, m_bmhdrUNCHECKED->bmhdr.Height));
							// set filtering, wrapping and on-screen size
							Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, 30, 30));	//scale to 30x30
							Ft_Gpu_CoCmd_LoadIdentity(pHost);
							Ft_Gpu_CoCmd_Scale(pHost, 65536*30L/m_bmhdrUNCHECKED->bmhdr.Width, 65536*30L/m_bmhdrUNCHECKED->bmhdr.Height);	//scale to 30x30
							Ft_Gpu_CoCmd_SetMatrix(pHost );
						}
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (469-50)*16, (31+9+i*(26+21+1))*16 ) );
						Ft_Gpu_CoCmd_LoadIdentity(pHost);
						Ft_Gpu_CoCmd_Scale(pHost, 65536, 65536);	//scale to normal
						Ft_Gpu_CoCmd_SetMatrix(pHost );
					}
					else
					{
						// normal value
						if( (m_tagval == i+1) && (m_key_cnt >= VALID_TOUCHED_KEY) )
						{
							// colore stringa evidenziata
							Ft_App_WrCoCmd_Buffer(pHost, Get_UserColorHex(UserColorId) );	// USER COLOR
						}
						else
						{
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(240,240,240));	// GREY
						}
						j = strlen( str2 );
						/*!
						** Some parameters must be viewed only with latin alphabet
						**/
						font = fontValue;
						if( iFontGroup == FONT_GROUP_CYRILLIC )
						{
							if( (code == SPECIAL_WIFI_KEYW_TYPE) ||
								(code == SPECIAL_WPS_PIN_TYPE) ||
								(code == SPECIAL_WIFI_SSID_TYPE) ||
								(code == P_STATO_ATTUA) )
							{
								font = FONT_VALUE_DEF;
							}
						}
						for( ; GetStringWidth( str2, font ) > 470; )
						{
							// troncamento valore parametro
							j--;
							str2[j] = '\0';
						}
						Ft_Gpu_CoCmd_Text( pHost, 0, 57+i*(26+21+1), font, 0, String2Font(str2, font) );
					}
					if( i != (nItemPage[menu->MenuType]-1) )
					{
						// separator
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(16,16,16));	// GREY-BLACK
						Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (78+i*(26+21+1))*16 ) );
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 469*16, (78+i*(26+21+1))*16 ) );
					}
				}
			break;
		}
		
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
		if( nMenuPage > 1 )
		{
			// vertical scroll bar
			Ft_Gpu_CoCmd_FgColor(pHost, Get_UserColorHex(UserColor_tab[UserColorId].addon));
			Ft_Gpu_CoCmd_BgColor(pHost, 0xFFFFFF);
			if( m_tagval == TOUCH_TAG_INIT )
			{
				TagRegister( TAG_VSCROLL_BASE, 472, 30, 8, (FT_DispHeight-30) );
			}
			But_opt = ( (m_tagval == TAG_VSCROLL_BASE) /*&& pt->status*/ ) ? OPT_FLAT : 0;
			Ft_Gpu_CoCmd_Scrollbar( pHost,
											(FT_DispWidth-8), (30+8/2), // consider radius of bar
											8, (FT_DispHeight-(30+8/2)-8/2), // consider radius of bar
											But_opt,
											menu->current_selection,
											nItemPage[menu->MenuType],
											menu->num_elements);
				
			if( m_tagval == TOUCH_TAG_INIT )
			{
				// screen scroll tag
				TagRegister( TAG_VERT_SCROLL, 0, 31, FT_DispWidth-8, FT_DispHeight-30 );
			}
		}
		
		// draw QUIT bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(m_bmhdrQUIT->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(m_bmhdrQUIT->bmhdr.Format, m_bmhdrQUIT->bmhdr.Stride, m_bmhdrQUIT->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, m_bmhdrQUIT->bmhdr.Width, m_bmhdrQUIT->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, 0*16 ) );
		if( m_tagval == TOUCH_TAG_INIT )
		{
			// QUIT bitmap tag
			TagRegister( TAG_QUIT, 0, 0, 30, 30 );
		}
		
		// waiting pop-up window for the setting result
		if( Flag.popup && (menu->MenuType == MENU_TYPE_USER_PARAM) )
		{
			ShowWaitSetResultPopup();
		}
		
		// title
		/* Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_XY( 30, 0 ) );
		Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_SIZE( FT_DispWidth-30, 30 ) );
		Ft_Gpu_CoCmd_Gradient( pHost, 30, 0, 0x0000FF, FT_DispWidth-1, 0, 0x000000 ); */
		Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
		Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 30*16, (0)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font((char *)menu->title, fontTitle) );
		
		Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
		Ft_Gpu_CoCmd_Swap( pHost );
		Ft_App_Flush_Co_Buffer( pHost );
		Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() )
		{
			// make the BGRA file name
			i = NUM_SSHOT;
			if( menu->ExtRec == InfoItem )
			{
				i = SSHOT_Menu_Info;
			}
			else if( menu->ExtRec == MenuItem )
			{
				i = SSHOT_Menu_Main;
			}
			else if( menu->ExtRec == SettingsItem )
			{
				i = SSHOT_Menu_Settings;
			}
			else if( menu->ExtRec == TecnicoItem )
			{
				i = SSHOT_Menu_Tecnico;
			}
			
			// screenshot
			DoScreenshot( i );
		}
	#endif	// SCREENSHOT
	}		
				
}

/*!
** \fn unsigned char userMenu( void )
** \brief Handler function for the USER_STATE_MENU and USER_STATE_INFO
** 		User Interface states
** \return The next User Interface state 
**/
unsigned char userMenu( void )
{
	unsigned char RetVal = userCurrentState;
	char s[DISPLAY_MAX_COL+1];
	osEvent event;
	TAG_STRUCT *pt = NULL;
	short i;
	short Dy = 0;
	short l;
	short code;
	const PARTAB_STRUCT *pParTab;
	SETUP_MENU *menu = &Setup_Menu[iLivMenu];
	char *sTitle;
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;
	unsigned char ret;
	
	// Draw menu
	setup_menu_draw( menu, m_redraw );
	m_redraw = 1;
		
	do
	{
		Wdog();
		if( Flag.popup	)
		{
			// wait for a setting result
			Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
		
			// get the current setting result state
			i = OT_GetResult();
			if( (i == OT_RESULT_OK) || (i == OT_RESULT_FAILED) )
			{
				// received setting result
				WaitSetResult.result = i;
				WaitSetResult.Time = 2000/USER_TIME_TICK;	// 2s for viewing result

				if( (WaitSetResult.iPar == P_RFPANEL)
					&& (WaitSetResult.result == OT_RESULT_OK )
					)
				{
					// registra il nuovo RFID solo se confermato dalla scheda
					ExecSetRFid();
				}
			}
			
			// check for pop-up window timeout
			if( WaitSetResult.Time )
			{
				WaitSetResult.Time--;
			}
			if( WaitSetResult.Time == 0 )
			{
				// disable pop-up window
				Flag.popup = 0;
			}
			
			// flush the touch queue
			FlushTouchQueue();
			
			// simulate a touch timeout event
			if( userCounterTime <= 1 )
			{
				userCounterTime = USER_TIMEOUT_SHOW_20;
			}
			event.status = osEventTimeout;
		}
		else
		{
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );
			
			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
				m_tagval = pt->tag;
			}
		}
	} while( event.status == osOK );
			
	// event processing
	ret = Check4StatusChange();	// status changed?
	if( (ret != USER_MAX_STATES)
		&& (ret != USER_STATE_INIT)
		&& (menu->ExtRec == LocalSettingsItem)
		)
	{
		// exit from local settings menu
		RetVal = userVisInit();	// exit
	}
	else if( (ret != USER_MAX_STATES)
			&& (menu->ExtRec != LocalSettingsItem)
			)
	{
		// exit from any other menu
		RetVal = userExitMenu();	// exit
	}
	else if( LoadFonts() > 0 )
	{
		// fonts updated -> restart window
		m_tagval = TOUCH_TAG_INIT;
		m_scrollON = 0;
		Flag.Key_Detect = 0;
		m_key_cnt = 0;
		Flag.popup = 0;
	}
	else if( event.status == osEventTimeout )
	{
		// tag reset
		m_tagval = TAG_VERT_SCROLL;
		m_scrollON = 0;
		Flag.Key_Detect = 0;
		m_key_cnt = 0;

		/* Con Precarica Pellet o Pulizia o Pompa attivi si rimane nel 
			Menu' Impostazioni fino al comando utente di uscita */
		if( (menu->ExtRec == SettingsItem) &&
			(PRECARICA_PLT || (AVVIA_PULIZIA || PULIZIA) || AVVIA_POMPA) )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
		}
		
		// controllo cambio di stato a timer
		if( userCounterTime )
		{
			if( --userCounterTime == 0 )
			{
				if( menu->ExtRec == LocalSettingsItem )
				{
					RetVal = userVisInit();	// exit
				}
				else
				{
					RetVal = userVisAvvio();	// exit
				}
			}
			else
			{
				if( !(userCounterTime % (100/USER_TIME_TICK)) && 
					(menu->MenuType == MENU_TYPE_USER_PARAM) )
				{
					//Richiesta a ricircolo valore/limiti dei parametri visualizzati.
					i = iItemReq;
					do
					{
						i++;
						if( (i >= menu->num_elements) ||
							(i >= (menu->current_selection + nItemPage[menu->MenuType])) )
						{
							i = menu->current_selection ;
						}
						l = IndexVisItem[i];	// indice voce in tabella
						code = menu->ExtRec[l].code;
						if(
							(
								(code >= 0) ||
								(code == FUNCTION_ATTIVA_COCLEA_TYPE ) ||
								(code == FUNCTION_ATTIVA_ESP_TYPE ) ||
								(code == FUNCTION_ATTIVA_POMPA_TYPE ) ||
								(code == FUNCTION_TEST_TYPE ) ||
								(code == FUNCTION_TEST_AUTO_TYPE ) ||
								(code == FUNCTION_TEST_CALIB_FOTORES_ON_TYPE ) ||
								(code == FUNCTION_TEST_CALIB_FOTORES_OFF_TYPE ) ||
								(code == SPECIAL_WIFI_SSID_TYPE ) ||
								(code == SPECIAL_WIFI_KEYW_TYPE ) ||
								(code == SPECIAL_WPS_PIN_TYPE ) ||
								(code == SPECIAL_WIFI_PORT_TYPE )
							)
							&& (code != P_CONSUMO)	// valore parametro read-only richiesto da un altro processo
							&& (code != P_NUM_NO_ACC)	// valore parametro read-only richiesto da un altro processo
							)
						{
							// eventuale recupero vero codice parametro
							code = GetRealParCode( code );
							// recupera valore e limiti parametro
							if( !userReqPar( code ) )
							{
								// invio fallito per coda satura -> ritento al prossimo tick
								if( i > menu->current_selection )
								{
									// prossima richiesta sul parametro corrente
									i--;
								}
								else
								{
									// prossima richiesta sul primo parametro
									i = menu->num_elements;
								}
							}
							break;
						}
					} while( i != iItemReq );
					iItemReq = i;
				}
			}
		}
	}
	else if( (pt->tag == TAG_QUIT) && !pt->status )
	{
		// exit touch
		Flag.popup = 0;
		
		// flush the touch queue
		FlushTouchQueue();
		
		/* Mi assicuro che Precarica Pellet, Pulizia e Pompa siano disattivate
			all'uscita dal Menu' Impostazioni */
		if( menu->ExtRec == SettingsItem )
		{
			if( PRECARICA_PLT )
			{
				// disattiva
				userParValue.b[0] = 0;
				userSetPar( P_ATTIVA_COCLEA );
			}
			if( AVVIA_PULIZIA || PULIZIA )
			{
				// disattiva
				PULIZIA = 0;
				userParValue.b[0] = 0;
				userSetPar( P_ATTIVA_ESP );
			}
			if( AVVIA_POMPA )
			{
				// disattiva
				userParValue.b[0] = 0;
				userSetPar( P_ATTIVA_POMPA );
			}
		}

		l = IndexVisItem[menu->current_selection];	// indice voce in tabella;
		if( menu->ExtRec[l].EscFunc )
		{
			// ritorno al menu` superiore
			RetVal = menu->ExtRec[l].EscFunc();
		}
	}
	else if( (m_tagval == TAG_VSCROLL_BASE) && pt->status )
	{
		userCounterTime = USER_TIMEOUT_SHOW_20;
		// scroll bar touch
	}
	else if( m_tagval == TAG_VERT_SCROLL )
	{
		Flag.popup = 0;
		userCounterTime = USER_TIMEOUT_SHOW_20;
		// screen vertical scroll
		if( pt->status )
		{
			// touched
			switch( m_scrollON )
			{
			default:
			case 0:
				m_preYscroll = pt->y;
				m_scrollON = 1;
				break;

			case 1:
			case 2:
				if( m_key_cnt < 5 )
				{
					Dy = pt->y - m_preYscroll;
					if( Dy > 5 )
					{
						m_scrollON = 2;
						Flag.Key_Detect = 0;
						m_key_cnt = 0;
						// scroll up
						m_preYscroll = pt->y;
						//setup_menu_process_key(menu, MENU_KEYCODE_UP);
						if( menu->current_selection )
						{
							menu->current_selection--;
							m_redraw = 0;
						}				
					}
					else if( Dy < -5 )
					{
						m_scrollON = 2;
						Flag.Key_Detect = 0;
						m_key_cnt = 0;
						// scroll down
						m_preYscroll = pt->y;
						//setup_menu_process_key(menu, MENU_KEYCODE_DOWN);
						if( menu->current_selection < (menu->num_elements - nItemPage[menu->MenuType]) )
						{
							menu->current_selection++;
							m_redraw = 0;
						}				
					}
				}
				break;
			}
		}
		else
		{
			// untouched
			m_scrollON = 0;
		}
	}
	else if( m_scrollON != 2 )	// tagval > 0 without scroll
	{
		Flag.popup = 0;
		userCounterTime = USER_TIMEOUT_SHOW_20;
		// item
		i = menu->current_selection + (pt->tag - 1);
		l = IndexVisItem[i];	// indice voce in tabella;
		code = menu->ExtRec[l].code;
		pParTab = &Param_tab[code];
		
		m_tagval = Read_Keypad( pt );	// read the keys
		if( m_tagval )
		{
			// touched
			if( Flag.Key_Detect )
			{
				// first touch
				Flag.Key_Detect = 0;
				m_key_cnt = 0;
			}
			else if( ++m_key_cnt >= 600 )	// about 10s
			{
				// pressed item
				if(
						(menu->ExtRec == InfoItem) &&
						(code == P_ACC_CONSUMO) &&
						VIS_CONSUMO
					)
				{
					// sto visualizzando il consumo orario
					code = P_CONSUMO;
				}
				else if(
						(menu->ExtRec == InfoItem) &&
						(code == P_NUM_ACC) &&
						VIS_ACC
					)
				{
					// sto visualizzando il numero mancate accensioni
					code = P_NUM_NO_ACC;
				}
				
				if(
					(menu->ExtRec == InfoItem) &&
					(code == P_PORTATAARIAMISURATA)
					)
				{
					// inibizione A06
					userParValue.b[0] = 1;
					userSetPar( P_INIB_ALM_ARIACOBUST_INSUFF );
					sPkt.pktLen = 0;
					sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
					sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
					sPkt.pkt[sPkt.pktLen++] = MB_FUNC_SUONO_RC;	// cmd
					sPkt.pkt[sPkt.pktLen++] = BUZ_1LONG;
					OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
				}
				else if(
							(menu->ExtRec == InfoItem) &&
							(code == P_ACC_CONSUMO)
						)
				{
					// azzeramento consumo accumulato
					StatSec.AccConsumo = 0;
				}
				else if(
							(menu->ExtRec == InfoItem) &&
							((code == P_NUM_ACC) || (code == P_NUM_NO_ACC))
						)
				{
					// azzeramento contatori accensioni e mancate accensioni
					StatSec.NumNoAcc = 0;
					userParValue.w[0] = 0;
					userSetPar( P_NUM_ACC );
					sPkt.pktLen = 0;
					sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
					sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
					sPkt.pkt[sPkt.pktLen++] = MB_FUNC_SUONO_RC;	// cmd
					sPkt.pkt[sPkt.pktLen++] = BUZ_1LONG;
					OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
				}
				else if(
							(menu->ExtRec == InfoItem) &&
							(code == P_WIFI_IP)
						)
				{
					// richiesta AP mode
					WIFI_AP_MODE = 1;
					userSetNetworkFlags();
					sPkt.pktLen = 0;
					sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
					sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
					sPkt.pkt[sPkt.pktLen++] = MB_FUNC_SUONO_RC;	// cmd
					sPkt.pkt[sPkt.pktLen++] = BUZ_1LONG;
					OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
				}
				else if(
							((menu->ExtRec == CmdItem) && (code == P_TRAFF_B))
							|| ((menu->ExtRec == LocalSettingsItem) && (code == P_CODICE_PANNELLO))
						)
				{
					// ingresso in bootloader
					ENABLE_BOOT = 1;
					userVisShutDown( "Reboot...", 2000 );
					SystemRestart();
					// ------> NON PASSA MAI DI QUI
				}
				
				// restart window
				m_tagval = TOUCH_TAG_INIT;
				m_scrollON = 0;
				Flag.Key_Detect = 0;
				m_key_cnt = 0;
			}
			/* else if( m_key_cnt >= VALID_TOUCHED_KEY )
			{
				// pressed item
			} */

		}
		else if( pt->tag )
		{
			// untouched
			if(
					(menu->ExtRec == InfoItem) &&
					(code == P_ACC_CONSUMO)
				)
			{
				// toggle visualizzazione consumo orario/accumulato
				VIS_CONSUMO ^= 1;
			}
			else if(
					(menu->ExtRec == InfoItem) &&
					(code == P_NUM_ACC)
				)
			{
				// toggle visualizzazione numero accensioni / numero mancate accensioni
				VIS_ACC ^= 1;
			}
			
			if( m_key_cnt >= VALID_TOUCHED_KEY )
			{
				// flush the touch queue
				FlushTouchQueue();

				// process the pressed item on touch release
				switch( code )
				{
					case SUBMENU_P_TYPE :	// la voce e' un sub menu param
					case SUBMENU_M_TYPE :	// la voce e' un sub menu menu
					case SPECIALF_TYPE:		// la voce e' di tipo special function
						if( menu->ExtRec[l].ExecFunc != NULL )
						{
							// ingresso nel menu` inferiore o nella special function
							RetVal = menu->ExtRec[l].ExecFunc();
							// restart menu
							m_tagval = TOUCH_TAG_INIT;
							userCounterTime = USER_TIMEOUT_SHOW_20;
						}
						m_scrollON = 0;
					break;
					
					case FUNCTION_ATTIVA_COCLEA_TYPE:
						userParValue.b[0] = PRECARICA_PLT ^ 1;
						// parameter code as title
						userGetParCode( P_ATTIVA_COCLEA );
						strcpy( s, str2 );
						//userSetPar( P_ATTIVA_COCLEA );
						WinSetParWaitResult( 
											P_ATTIVA_COCLEA,
											s,
											(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
											);
						// restart menu
						m_tagval = TOUCH_TAG_INIT;
						userCounterTime = USER_TIMEOUT_SHOW_20;
						// redraw menu
						m_scrollON = 0;
					break;
					
					case FUNCTION_ATTIVA_ESP_TYPE:
						//userParValue.b[0] = AVVIA_PULIZIA ^ 1;	con la precarica-pellet si attiva anche l'espulsore
						userParValue.b[0] = PULIZIA ^ 1;
						// parameter code as title
						userGetParCode( P_ATTIVA_ESP );
						strcpy( s, str2 );
						//userSetPar( P_ATTIVA_ESP );
						WinSetParWaitResult(
											P_ATTIVA_ESP,
											s,
											(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
											);
						// restart menu
						m_tagval = TOUCH_TAG_INIT;
						userCounterTime = USER_TIMEOUT_SHOW_20;
						// redraw menu
						m_scrollON = 0;
					break;
					
					case FUNCTION_ATTIVA_POMPA_TYPE:
						userParValue.b[0] = AVVIA_POMPA ^ 1;
						// parameter code as title
						userGetParCode( P_ATTIVA_POMPA );
						strcpy( s, str2 );
						//userSetPar( P_ATTIVA_POMPA );
						WinSetParWaitResult( 
											P_ATTIVA_POMPA,
											s,
											(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
											);
						// restart menu
						m_tagval = TOUCH_TAG_INIT;
						userCounterTime = USER_TIMEOUT_SHOW_20;
						// redraw menu
						m_scrollON = 0;
					break;
					
					case FUNCTION_TEST_TYPE:
						userParValue.b[0] = TEST_STUFA ^ 1;
						// parameter code as title
						userGetParCode( P_TEST );
						strcpy( s, str2 );
						//userSetPar( P_TEST );
						WinSetParWaitResult( 
											P_TEST,
											s,
											(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
											);
						// restart menu
						m_tagval = TOUCH_TAG_INIT;
						userCounterTime = USER_TIMEOUT_SHOW_20;
						// redraw menu
						m_scrollON = 0;
					break;
					
					case FUNCTION_TEST_AUTO_TYPE:
						userParValue.b[0] = TEST_AUTO ^ 1;
						// parameter code as title
						userGetParCode( P_TEST_AUTO );
						strcpy( s, str2 );
						//userSetPar( P_TEST_AUTO );
						WinSetParWaitResult( 
											P_TEST_AUTO,
											s,
											(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
											);
						// restart menu
						m_tagval = TOUCH_TAG_INIT;
						userCounterTime = USER_TIMEOUT_SHOW_20;
						// redraw menu
						m_scrollON = 0;
					break;
					
					case FUNCTION_TEST_CALIB_FOTORES_ON_TYPE:
						userParValue.b[0] = CALIB_FOTORES_ON ^ 1;
						// parameter code as title
						userGetParCode( P_TEST_CALIB_FOTORES_ON );
						strcpy( s, str2 );
						//userSetPar( P_TEST_CALIB_FOTORES_ON );
						WinSetParWaitResult( 
											P_TEST_CALIB_FOTORES_ON,
											s,
											(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
											);
						// restart menu
						m_tagval = TOUCH_TAG_INIT;
						userCounterTime = USER_TIMEOUT_SHOW_20;
						// redraw menu
						m_scrollON = 0;
					break;
					
					case FUNCTION_TEST_CALIB_FOTORES_OFF_TYPE:
						userParValue.b[0] = CALIB_FOTORES_OFF ^ 1;
						// parameter code as title
						userGetParCode( P_TEST_CALIB_FOTORES_OFF );
						strcpy( s, str2 );
						//userSetPar( P_TEST_CALIB_FOTORES_OFF );
						WinSetParWaitResult( 
											P_TEST_CALIB_FOTORES_OFF,
											s,
											(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
											);
						// restart menu
						m_tagval = TOUCH_TAG_INIT;
						userCounterTime = USER_TIMEOUT_SHOW_20;
						// redraw menu
						m_scrollON = 0;
					break;
						
					case SPECIAL_WIFI_SSID_TYPE:
					case SPECIAL_WIFI_KEYW_TYPE:
					case SPECIAL_WPS_PIN_TYPE:
					case SPECIAL_WIFI_PORT_TYPE:
						// recupero vero codice parametro
						code = GetRealParCode( code );
						if( (menu->ExtRec != InfoItem) &&
							!userIsReadOnlyVar( code ) )
						{
							// modifica valore stringa lunga
							userVisEPar( code );
							userSubState = 0;	// parameter view
							if( WinSetString( 
											(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId ),
											strPar,
											maxLen
											) )
							{
								// esegui funzione associata, se richiesta
								if( menu->ExtRec[l].ExecFunc != NULL )
								{
									RetVal = menu->ExtRec[l].ExecFunc();
								}
								else
								{
									// parameter code as title
									userGetParCode( code );
									strcpy( s, str2 );
									//userSetPar( code );
									WinSetParWaitResult( 
														code,
														s,
														(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
														);
								}
							}
							else
							{
								// esegui funzione associata, se richiesta
								if( menu->ExtRec[l].EscModFunc != NULL )
								{
									RetVal = menu->ExtRec[l].EscModFunc();
								}
								else
								{
									userEscPar( code );
								}
							}
							// restart menu
							m_tagval = TOUCH_TAG_INIT;
							userCounterTime = USER_TIMEOUT_SHOW_20;
						}
						m_scrollON = 0;
					break;
					
					case SPECIAL_DATE_TYPE:
						// set date
						userSubState = 0;	// parameter view
						WinSetDate( (char *)GetMsg( 1, NULL, iLanguage, menu->ExtRec[l].wDispId ) );
						// restart menu
						m_tagval = TOUCH_TAG_INIT;
						userCounterTime = USER_TIMEOUT_SHOW_20;
						// redraw menu
						m_scrollON = 0;
					break;
					
					case SPECIAL_TIME_TYPE:
						// set time
						userSubState = 0;	// parameter view
						WinSetTime( (char *)GetMsg( 1, NULL, iLanguage, menu->ExtRec[l].wDispId ) );
						// restart menu
						m_tagval = TOUCH_TAG_INIT;
						userCounterTime = USER_TIMEOUT_SHOW_20;
						// redraw menu
						m_scrollON = 0;
					break;
					
					case P_BACKLIGHT:
						// modifica display
						userSubState = 0;	// parameter view
						// parameter code as title
						userGetParCode( code );
						strcpy( s, str2 );
						if( WinSetDisplay( 
										code,
										s,
										(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
										) )
						{
							/* // esegui funzione associata, se richiesta
							if( menu->ExtRec[l].ExecFunc != NULL )
							{
								RetVal = menu->ExtRec[l].ExecFunc();
							}
							else
							{
								// parameter code as title
								userGetParCode( code );
								strcpy( s, str2 );
								//userSetPar( code );
								WinSetParWaitResult( 
													code,
													s,
													(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
													);
							} */
						}
						else
						{
							/* // esegui funzione associata, se richiesta
							if( menu->ExtRec[l].EscModFunc != NULL )
							{
								RetVal = menu->ExtRec[l].EscModFunc();
							}
							else
							{
								userEscPar( code );
							} */
						}
						// restart menu
						m_tagval = TOUCH_TAG_INIT;
						userCounterTime = USER_TIMEOUT_SHOW_20;
						// redraw menu
						m_scrollON = 0;
					break;
					
					default:	// code >= 0: parametro
						if( (menu->ExtRec != InfoItem) &&
							!userIsReadOnlyVar( code ) )
						{
							userGetPar( code );
							if( userIsFlagVar( code ) )
							{
								// flag toggle
								userParValue.b[0] ^= 1;
								// esegui funzione associata, se richiesta
								if( menu->ExtRec[l].ExecFunc != NULL )
								{
									RetVal = menu->ExtRec[l].ExecFunc();
								}
								else
								{
									// parameter code as title, if not a local 
									userGetParCode( code );
									strcpy( s, str2 );
									//userSetPar( code );
									WinSetParWaitResult( 
														code,
														s,
														(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
														);
									userCounterTime = USER_TIMEOUT_SHOW_20;
								}
							}
							else
							{
								if(
									(code == P_CRONO_P1_SETTIM_START) || (code == P_CRONO_P1_SETTIM_STOP)
							#ifdef menu_CRONO
									|| (code == P_CRONO_P2_SETTIM_START) || (code == P_CRONO_P2_SETTIM_STOP)
									|| (code == P_CRONO_P3_SETTIM_START) || (code == P_CRONO_P3_SETTIM_STOP)
									|| (code == P_CRONO_P4_SETTIM_START) || (code == P_CRONO_P4_SETTIM_STOP)
									|| (code == P_CRONO_P5_SETTIM_START) || (code == P_CRONO_P5_SETTIM_STOP)
									|| (code == P_CRONO_P6_SETTIM_START) || (code == P_CRONO_P6_SETTIM_STOP)
							#endif	// menu_CRONO
									 )
								{
									/* In modifica un'ora di start/stop pari a PROG_OFF viene riportata
										uguale al limite inferiore */
									if( userParValue.b[0] == PROG_OFF )
									{
										userParValue.b[0] = pParTab->uMin.Min.b[0];
									}
									if( code == P_CRONO_P1_SETTIM_STOP )
									{
										/* In modifica parto dal valore corrente dell'ora di start
											del programma crono, se necessario */
										if( userParValue.b[0] < Crono.Crono_ProgWeek[iCronoPrg].OraStart )
										{
											userParValue.b[0] = Crono.Crono_ProgWeek[iCronoPrg].OraStart;
										}
									}
								}
								else if( code == P_CRONO_PROFILO )
								{
									// se nessun profilo selezionato, parto dal profilo 1
									if( userParValue.b[0] == 0 )
										userParValue.b[0] = 1;
								}
								userSubState = 0;	// parameter view
								// parameter code as title
								userGetParCode( code );
								strcpy( s, str2 );
								if( code == P_ORESAT )
								{
									// ore service solo azzerabili
									ConfOreSAT();
								}
								else if( WinSetParam( 
													code,
													s,
													(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
													) )
								{
									// esegui funzione associata, se richiesta
									if( menu->ExtRec[l].ExecFunc != NULL )
									{
										RetVal = menu->ExtRec[l].ExecFunc();
									}
									else
									{
										//userSetPar( code );
										if( WinSetParWaitResult( 
																code,
																s,
																(char *)GetMsg( 2, NULL, iLanguage, menu->ExtRec[l].wDispId )
																) == 1 )
										{
											if( menu->ExtRec == RicetteItem )	// solo nel Menu` RICETTE 
											{
												if( code == P_RICETTA_PELLET_P )
												{
													// imposto con lo stesso valore la ricetta attuazioni transitorie
													userSetPar( P_RICETTA_PELLET_T );
												}
												else if( code == P_RICETTA_ARIA_P )
												{
													// imposto con lo stesso valore l'offset attuazioni transitorie
													userSetPar( P_RICETTA_ARIA_T );
												}
											}
										}
									}
									if( code == P_LINGUA )
									{
										// aggiorna il titolo del Menu` Utente a livello corrente
										sTitle = sMenuTitle[iLivMenu];
										strncpy( sTitle, GetMsg( 1, NULL, iLanguage, MENU_IMPOSTAZIONI ), DISPLAY_MAX_COL );
										sTitle[DISPLAY_MAX_COL] = '\0';
										if( menu->ExtRec == SettingsItem )
										{
											// aggiorna il titolo del Menu` Principale a livello superiore
											sTitle = sMenuTitle[iLivMenu-1];
											strncpy( sTitle, GetMsg( 1, NULL, iLanguage, sMAINMENU_TITLE ), DISPLAY_MAX_COL );
											sTitle[DISPLAY_MAX_COL] = '\0';
										}
									}
								}
								else
								{
									// esegui funzione associata, se richiesta
									if( menu->ExtRec[l].EscModFunc != NULL )
									{
										RetVal = menu->ExtRec[l].EscModFunc();
									}
									else
									{
										userEscPar( code );
									}
								}
								userCounterTime = USER_TIMEOUT_SHOW_20;
							}
							// restart menu
							m_tagval = TOUCH_TAG_INIT;
						}
						m_scrollON = 0;
					break;
				}
			}
			Flag.Key_Detect = 0;
			m_key_cnt = 0;
		}
	}
	else
	{
		Flag.popup = 0;
		userCounterTime = USER_TIMEOUT_SHOW_20;
		// tagval > 0 with scroll
		Flag.Key_Detect = 0;
		m_key_cnt = 0;
	}
	
	return RetVal;	
}

/*!
** \fn short WinSetString( char *sTitle, char *String, unsigned short MaxLen )
** \brief Set string window handler
** \param sTitle The window's title string
** \param String The string to be set
** \param MaxLen The string maximum lenght
** \return 1: SET, 0: QUIT
**/
short WinSetString( char *sTitle, char *String, unsigned short MaxLen )
{
	short RetVal = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	ft_uint8_t Line = 0;
	ft_uint16_t Disp_pos = 0, But_opt;
	ft_uint8_t Read_sfk = 255, tval;
	ft_uint16_t noofchars = 0, line2disp = 0, nextline = 0;
	ft_uint8_t font = FONT_VALUE_DEF;	//fontValue; per ora solo caratteri latini
	unsigned short i;
	unsigned short len = strlen(String);
	unsigned char loop = 1;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );

	// reset all tags
	CTP_TagDeregisterAll();

	// Init Linebuffer
	for( tval=0; tval<MAX_LINES; tval++ )
	{
		memset( &Buffer.notepad[tval], '\0', sizeof( Buffer.notepad[tval] ) );
	}
	Line = 0;						  // Starting line
	Disp_pos = LINE_STARTPOS;	  // starting pos                                                      
	Flag.Caps = OFF;			  		// Disable the capital charaters
	Flag.Numeric = OFF;			  // Disable the numbers and spcial charaters
	memset( ( Buffer.notepad[Line] + 0 ), '_', 1 );	// For Cursor  
	Disp_pos += Ft_Gpu_Font_WH( Buffer.notepad[Line][0], Font );	// Update the Disp_Pos
	noofchars += 1;				  // for cursor
	/*enter */
	//Flag.Exit = 0;
	Flag.Key_Detect = 0;
	
	// init the displayed string
	for( i=0; i<len; i++ )
	{
		Disp_pos += Ft_Gpu_Font_WH( String[i], Font );	// update dispos
		Buffer.temp = Buffer.notepad[Line] + strlen( Buffer.notepad[Line] );	// load into temporary buffer 
		Buffer.temp[-1] = String[i];	// update the string
		Buffer.temp[0] = '_';
		Buffer.temp[1] = '\0';
		noofchars = strlen( Buffer.notepad[Line] );	// get the string len
		//if( Disp_pos > LINE_ENDPOS )	// check if there is place to put a character in a specific line
		if( (Disp_pos > LINE_ENDPOS) && (Line < MAX_LINES-1) )	// check if there is place to put a character in a specific line
		{
			Buffer.temp =
				Buffer.notepad[Line] + ( strlen( Buffer.notepad[Line] ) - 1 );
			Buffer.temp[0] = '\0';
			noofchars -= 1;
			Disp_pos = LINE_STARTPOS;
			Line++;
			memset( ( Buffer.notepad[Line] ), '\0', sizeof( Buffer.notepad[Line] ) );	// Clear the line buffer
			noofchars = strlen( Buffer.notepad[Line] );
			Buffer.temp = Buffer.notepad[Line] + noofchars;
			Buffer.temp[0] = '_';
			Buffer.temp[1] = '\0';
			for( tval = 0; tval < noofchars; tval++ )
				Disp_pos += Ft_Gpu_Font_WH( Buffer.notepad[Line][tval], Font );	// Update the Disp_Pos
		}
	}

	while( loop )
	{
		if( Flag.Key_Detect )
		{								  // check if key is pressed
			Flag.Key_Detect = 0;	  // clear it  
			if( (Read_sfk >= SPECIAL_FUN) && (Read_sfk < ' ') )
			{
				// check if any special function keys are pressed 
				switch ( Read_sfk )
				{
					case BACK_SPACE:
						if( len )
							len--;
						if( noofchars > 1 )	// check if any characters are present in the line, cursor not include
						{
							noofchars -= 1;	// clear the character in the buffer
							Disp_pos -= Ft_Gpu_Font_WH( *( Buffer.notepad[Line] + noofchars - 1 ), Font );	// Update the Disp_Pos                             
						}
						else
						{
							// no characters are present in the line
							/*
							if( Line >= ( MAX_LINES - 1 ) )
								Line--;
							else
								Line = 0;	// check the lines
							*/
							if( Line )
								Line--;
							noofchars = strlen( Buffer.notepad[Line] );	// Read the len of the line 
							for( tval = 0; tval < noofchars; tval++ )	// Compute the length of the Line
								Disp_pos += Ft_Gpu_Font_WH( Buffer.notepad[Line][tval], Font );	// Update the Disp_Pos
						}
						Buffer.temp = ( Buffer.notepad[Line] + noofchars );	// load into temporary buffer                                   
						Buffer.temp[-1] = '_';	// update the string                       
						Buffer.temp[0] = '\0';
						break;

					case CAPS_LOCK:
						Flag.Caps ^= 1;	// toggle the caps lock on when the key detect
						break;

					case NUMBER_LOCK:
						Flag.Numeric ^= 1;	// toggle the number lock on when the key detect
						break;

					case CLEARALL:
						len = 0;
						for( tval = 0; tval < MAX_LINES; tval++ )
							memset( &Buffer.notepad[tval], '\0',
								sizeof( Buffer.notepad[tval] ) );
						Line = 0;	  // Starting line
						Disp_pos = LINE_STARTPOS;	// starting pos                                                      
						//       Flag.Numeric = OFF;                             // Disable the numbers and spcial charaters
						memset( ( Buffer.notepad[Line] + 0 ), '_', 1 );	// For Cursor  
						Disp_pos += Ft_Gpu_Font_WH( Buffer.notepad[Line][0], Font );	// Update the Disp_Pos
						noofchars += 1;
						break;
				}
			}
			else if( len < MaxLen-1 )
			{
				len++;
				Disp_pos += Ft_Gpu_Font_WH( Read_sfk, Font );	// update dispos
				Buffer.temp = Buffer.notepad[Line] + strlen( Buffer.notepad[Line] );	// load into temporary buffer 
				Buffer.temp[-1] = Read_sfk;	// update the string                           
				Buffer.temp[0] = '_';
				Buffer.temp[1] = '\0';
				noofchars = strlen( Buffer.notepad[Line] );	// get the string len
				//if( Disp_pos > LINE_ENDPOS )	// check if there is place to put a character in a specific line
				if( (Disp_pos > LINE_ENDPOS) && (Line < MAX_LINES-1) )	// check if there is place to put a character in a specific line
				{
					Buffer.temp =
						Buffer.notepad[Line] + ( strlen( Buffer.notepad[Line] ) - 1 );
					Buffer.temp[0] = '\0';
					noofchars -= 1;
					Disp_pos = LINE_STARTPOS;
					Line++;
					memset( ( Buffer.notepad[Line] ), '\0', sizeof( Buffer.notepad[Line] ) );	// Clear the line buffer
					noofchars = strlen( Buffer.notepad[Line] );
					Buffer.temp = Buffer.notepad[Line] + noofchars;
					Buffer.temp[0] = '_';
					Buffer.temp[1] = '\0';
					for( tval = 0; tval < noofchars; tval++ )
						Disp_pos += Ft_Gpu_Font_WH( Buffer.notepad[Line][tval], Font );	// Update the Disp_Pos
				}
			}
		}
		
		// Display List start 
		Ft_Gpu_CoCmd_Dlstart( pHost );
		Ft_App_WrCoCmd_Buffer( pHost, CLEAR_COLOR_RGB( 100, 100, 100 ) );
		Ft_App_WrCoCmd_Buffer( pHost, CLEAR( 1, 1, 1 ) );
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );
		
		// draw QUIT bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, 30, 30));	//scale to 30x30
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_Gpu_CoCmd_LoadIdentity(pHost);
		Ft_Gpu_CoCmd_Scale(pHost, 65536*30L/p_bmhdrQUIT->bmhdr.Width, 65536*30L/p_bmhdrQUIT->bmhdr.Height);	//scale to 30x30
		Ft_Gpu_CoCmd_SetMatrix(pHost );
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, 0*16 ) );
		if( Read_sfk == 255 )
		{
			// QUIT bitmap tag
			TagRegister( TAG_QUIT, 0, 0, 30, 30 );
		}
		
		// draw OK bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, 30, 30));	//scale to 30x30
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_Gpu_CoCmd_LoadIdentity(pHost);
		Ft_Gpu_CoCmd_Scale(pHost, 65536*30L/p_bmhdrOK->bmhdr.Width, 65536*30L/p_bmhdrOK->bmhdr.Height);	//scale to 30x30
		Ft_Gpu_CoCmd_SetMatrix(pHost );
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-30)*16, 0*16 ) );
		if( Read_sfk == 255 )
		{
			// OK bitmap tag
			TagRegister( TAG_OK, FT_DispWidth-30, 0, 30, 30 );
		}

		Ft_Gpu_CoCmd_LoadIdentity(pHost);
		Ft_Gpu_CoCmd_Scale(pHost, 65536, 65536);	//scale to normal
		Ft_Gpu_CoCmd_SetMatrix(pHost );
		
		Ft_Gpu_CoCmd_FgColor( pHost, 0x703800 );
		Ft_Gpu_CoCmd_BgColor( pHost, 0x703800 );
		
		But_opt = ( Read_sfk == CLEARALL ) ? OPT_FLAT : 0;	// button color change if the button during press
		// Clear all
		if( Read_sfk == 255 )
		{
			TagRegister( CLEARALL, 410, 225, 70, 30 );
		}
		Ft_Gpu_CoCmd_Button( pHost, 410, 225, 70, 30, font, But_opt, String2Font("DEL", font) );
			
		But_opt = ( Read_sfk == BACK_SPACE ) ? OPT_FLAT : 0;
		// BackSpace
		if( Read_sfk == 255 )
		{
			TagRegister( BACK_SPACE, 420, 190, 60, 30 );
		}
		Ft_Gpu_CoCmd_Button( pHost, 420, 190, 60, 30, font, But_opt, String2Font("<-", font) );
			
		But_opt = ( Read_sfk == cBLANK ) ? OPT_FLAT : 0;
		// Space
		if( Read_sfk == 255 )
		{
			TagRegister( cBLANK, 55, 225, 350, 30 );
		}
		Ft_Gpu_CoCmd_Button( pHost, 55, 225, 350, 30, font, But_opt, String2Font(" ", font) );

		if( Flag.Numeric == OFF )
		{
			Ft_Gpu_CoCmd_Keys( pHost, 0, 120, FT_DispWidth, 30,
									font, Read_sfk,
									( Flag.Caps == ON ? String2Font("QWERTYUIOP", font) : String2Font("qwertyuiop", font) ) );	// 10 keys 45x30, gap 3
			if( Read_sfk == 255 )
			{
				TagRegister( 'Q', 0*48, 120, 45, 30 );
				TagRegister( 'W', 1*48, 120, 45, 30 );
				TagRegister( 'E', 2*48, 120, 45, 30 );
				TagRegister( 'R', 3*48, 120, 45, 30 );
				TagRegister( 'T', 4*48, 120, 45, 30 );
				TagRegister( 'Y', 5*48, 120, 45, 30 );
				TagRegister( 'U', 6*48, 120, 45, 30 );
				TagRegister( 'I', 7*48, 120, 45, 30 );
				TagRegister( 'O', 8*48, 120, 45, 30 );
				TagRegister( 'P', 9*48, 120, 45, 30 );
			}
			
			Ft_Gpu_CoCmd_Keys( pHost, 20, 155, 460, 30,
									font, Read_sfk,
									( Flag.Caps == ON ? String2Font("ASDFGHJKL", font) : String2Font("asdfghjkl", font) ) );	// 9 keys 48x30, gap 3
			if( Read_sfk == 255 )
			{
				TagRegister( 'A', 0*51+20, 155, 48, 30 );
				TagRegister( 'S', 1*51+20, 155, 48, 30 );
				TagRegister( 'D', 2*51+20, 155, 48, 30 );
				TagRegister( 'F', 3*51+20, 155, 48, 30 );
				TagRegister( 'G', 4*51+20, 155, 48, 30 );
				TagRegister( 'H', 5*51+20, 155, 48, 30 );
				TagRegister( 'J', 6*51+20, 155, 48, 30 );
				TagRegister( 'K', 7*51+20, 155, 48, 30 );
				TagRegister( 'L', 8*51+20, 155, 48, 30 );
			}
									
			Ft_Gpu_CoCmd_Keys( pHost, 60, 190, 350, 30,
									font, Read_sfk,
									( Flag.Caps == ON ? String2Font("ZXCVBNM", font) : String2Font("zxcvbnm", font) ) );	// 7 keys 47x30, gap 3
			if( Read_sfk == 255 )
			{
				TagRegister( 'Z', 0*50+60, 190, 47, 30 );
				TagRegister( 'X', 1*50+60, 190, 47, 30 );
				TagRegister( 'C', 2*50+60, 190, 47, 30 );
				TagRegister( 'V', 3*50+60, 190, 47, 30 );
				TagRegister( 'B', 4*50+60, 190, 47, 30 );
				TagRegister( 'N', 5*50+60, 190, 47, 30 );
				TagRegister( 'M', 6*50+60, 190, 47, 30 );
			}

			But_opt = ( Read_sfk == CAPS_LOCK ) ? OPT_FLAT : 0;
			// Capslock
			Ft_Gpu_CoCmd_Button( pHost, 0, 190, 48, 30,
										font, But_opt, String2Font("a^", font) );
			if( Read_sfk == 255 )
			{
				TagRegister( CAPS_LOCK, 0, 190, 48, 30 );
			}
				
			But_opt = ( Read_sfk == NUMBER_LOCK ) ? OPT_FLAT : 0;
			// Numberlock
			Ft_Gpu_CoCmd_Button( pHost, 0, 225, 48, 30,
										font, But_opt, String2Font("12*", font) );
			if( Read_sfk == 255 )
			{
				TagRegister( NUMBER_LOCK, 0, 225, 48, 30 );
			}
		}
		else	// Flag.Numeric == ON
		{
			Ft_Gpu_CoCmd_Keys( pHost, 0, 120, FT_DispWidth, 30,
									font, Read_sfk, String2Font("1234567890", font) );	// 10 keys 45x30, gap 3
									
			Ft_Gpu_CoCmd_Keys( pHost, 20, 155, 460, 30,
									font, Read_sfk, String2Font("-@#$%^&*(", font) );	// 9 keys 48x30, gap 3
									
			Ft_Gpu_CoCmd_Keys( pHost, 60, 190, 350, 30,
									font, Read_sfk, String2Font(")_+[]{}", font) );	// 7 keys 47x30, gap 3
									
			But_opt = ( Read_sfk == NUMBER_LOCK ) ? OPT_FLAT : 0;
			// Numberlock
			Ft_Gpu_CoCmd_Button( pHost, 0, 225, 48, 30,
										font, But_opt,	String2Font("AB*", font) );
		}
			
		// title
		/* Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_XY( 30, 0 ) );
		Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_SIZE( FT_DispWidth-30-30, 30 ) );
		Ft_Gpu_CoCmd_Gradient( pHost, 30, 0, 0x0000FF, FT_DispWidth-30-1, 0, 0x000000 ); */
		Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
		Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 30*16, (0)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-30-1)*16, (30)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );
		
		Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_XY( 0, 30 ) );
		Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_SIZE( 480, 80 ) );
		Ft_App_WrCoCmd_Buffer( pHost, CLEAR_COLOR_RGB( 0, 0, 0 ) );
		Ft_App_WrCoCmd_Buffer( pHost, CLEAR( 1, 1, 1 ) );
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		
		line2disp = 0;
		while( line2disp <= Line )
		{
			nextline = 30 + 3 + ( line2disp * 20 );
			Ft_Gpu_CoCmd_Text( pHost, 0, nextline, font, 0,
				String2Font((char *)&Buffer.notepad[line2disp], font) );
			line2disp++;
		}
		Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
		Ft_Gpu_CoCmd_Swap( pHost );
		Ft_App_Flush_Co_Buffer( pHost );
		Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
		
		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );
			
			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
			}
		} while( event.status == osOK );
		
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			Read_sfk = 255;
			Flag.Key_Detect = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			Flag.Key_Detect = 0;
			Read_sfk = 0;
		}
		else if( ((pt->tag == TAG_QUIT) || (pt->tag == TAG_OK)) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
				
			if( (pt->tag == TAG_OK) && (userSubState == 1) )
			{
				String[0] = '\0';
				if( (Line != 0) || (noofchars > 1) )
				{
					Buffer.notepad[Line][noofchars-1] = '\0';
					for( tval=0; tval<MAX_LINES; tval++ )
					{
						strcat( String, Buffer.notepad[tval] );
					}
				}
				RetVal = 1;
			}
				
			loop = 0;	// exit
		}
		else
		{
			Read_sfk = Read_Keypad( pt );	// read the keys   
			if( Read_sfk && (!Flag.Caps || Flag.Numeric) )
			{
				// touched
				userCounterTime = USER_TIMEOUT_SHOW_60;
				for( i=0; i<26; i++ )
				{
					if( Read_sfk == keyChar[0][i] )
					{
						if( userSubState == 0 )
						{
							userSubState = 1;	// parameter modify
						}
						if( Flag.Numeric )
							Read_sfk = keyChar[2][i];
						else if( !Flag.Caps )
							Read_sfk = keyChar[1][i];
						break;
					}
				}
			}
		}
	}

	return RetVal;
}

/*!
**  \fn short WinSetParam( unsigned short iPar, char *sDesc, char *sTitle )
**  \brief Set parameter value window handler
**  \param iPar The parameter index
**  \param sDesc The parameter description string
**  \param sTitle The window's title string
**  \return 1: SET, 0: QUIT
**/
short WinSetParam( unsigned short iPar, char *sDesc, char *sTitle )
{
	short RetVal = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	unsigned long key_cnt = 0;
	unsigned char loop = 1;
	short i;
	unsigned char scrollON[1] = { 0 };	//!< scrolling status
	unsigned short preYscroll[1] = { 0 };	//!< previous Y value for vertical scrolling
	short Dy = 0;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );

	// inizializza valore per scroll DOWN (decremento)
	userDecPar( iPar, 1 );
	userVisPar( iPar );
	strcpy( str3, str2 );
	userIncPar( iPar, 1 );
	
	// inizializza valore per scroll UP (incremento)
	userIncPar( iPar, 1 );
	userVisPar( iPar );
	strcpy( str4, str2 );
	userDecPar( iPar, 1 );
	
	
	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
			// recupera valore e limiti parametro
			userReqPar( iPar );
		}

		if( !((tagval == TAG_VSCROLL_BASE + 0) && (scrollON[0] == 1)) )
		{
			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			
			// draw QUIT bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// QUIT bitmap tag
				TagRegister( TAG_QUIT, 0, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height), p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
			}
			
			// draw OK bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-p_bmhdrOK->bmhdr.Width)*16, (FT_DispHeight-p_bmhdrOK->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// OK bitmap tag
				TagRegister( TAG_OK, (FT_DispWidth-p_bmhdrOK->bmhdr.Width), (FT_DispHeight-p_bmhdrOK->bmhdr.Height), p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height );
			}

			// description
			if( sDesc != NULL )
			{
				if( (i = GetFont4StringWidth( (char *)sDesc, fontTitle, FT_DispWidth )) > 0 )
				{
					Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 50, i, OPT_CENTER, String2Font(sDesc, i) );
				}
			}

			// draw the centered parameter value
			userVisPar( iPar );
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColor_tab[UserColorId].addon) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 160, fontMax, OPT_CENTER, String2Font(str2, fontMax) );

			// draw the scroll DOWN parameter value
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 160-25-5-14, fontTitle, OPT_CENTER, String2Font(str3, fontTitle) );

			// draw the scroll UP parameter value
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 160+25+5+14, fontTitle, OPT_CENTER, String2Font(str4, fontTitle) );

			if( tagval == TOUCH_TAG_INIT )
			{
				// digit scroll tag
				TagRegister( TAG_VSCROLL_BASE+0, (FT_DispWidth/2-20), 31, 40, FT_DispHeight-30 );
			}

			// title
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );

			Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
			Ft_Gpu_CoCmd_Swap( pHost );
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
		}
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() )
		{
			// screenshot
			DoScreenshot( SSHOT_SetParam );
		}
	#endif	// SCREENSHOT

		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
			}
		} while( event.status == osOK );
			
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			scrollON[0] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			scrollON[0] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					if( userSubState == 1 )
					{
						// modifica
						// ???
					}
					loop = 0;	// exit
				}
				else
				{
					if( userSubState == 1 )
					{
						// modifica
						// ???
					}
					else
					{
						// visualizzazione
						if( !(userCounterTime % USER_TIMEOUT_SHOW_1) )
						{
							// recupera valore e limiti parametro
							userReqPar( iPar );
							//userGetPar( iPar ); userParValue.b[0] gia' inizializzato
						}
					}
				}
			}
		}
		else if( ((pt->tag == TAG_QUIT) || (pt->tag == TAG_OK)) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			if( (pt->tag == TAG_OK) && (userSubState == 1) )
			{
				RetVal = 1;
			}
			
			loop = 0;	// exit
		}
		else if( tagval == TAG_VSCROLL_BASE+0 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// digit vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[0] )
				{
				default:
				case 0:
					preYscroll[0] = pt->y;
					scrollON[0] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[0];
						if( Dy > 5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[0] = pt->y;
							
							if( userSubState == 0 )
							{
								userSubState = 1;	// parameter modify
								//userGetPar( iPar ); userParValue.b[0] gia' inizializzato
							}
							// update the scroll UP parameter value
							strcpy( str4, str2 );
							// down 
							userDecPar( iPar, 1 );
							// update the scroll DOWN parameter value
							userDecPar( iPar, 1 );
							userVisPar( iPar );
							strcpy( str3, str2 );
							userIncPar( iPar, 1 );
						}
						else if( Dy < -5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[0] = pt->y;
							
							if( userSubState == 0 )
							{
								userSubState = 1;	// parameter modify
								//userGetPar( iPar ); userParValue.b[0] gia' inizializzato
							}
							// update the scroll DOWN parameter value
							strcpy( str3, str2 );
							// up 
							userIncPar( iPar, 1 );
							// update the scroll UP parameter value
							userIncPar( iPar, 1 );
							userVisPar( iPar );
							strcpy( str4, str2 );
							userDecPar( iPar, 1 );
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[0] = 0;
			}
		}
		else if( scrollON[0] != 2 )
		{
			tagval = Read_Keypad( pt );	// read the keys
			if( tagval )
			{
				// touched
				userCounterTime = USER_TIMEOUT_SHOW_60;
				if( Flag.Key_Detect )
				{
					// first touch
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
				/*else if( ++key_cnt >= VALID_TOUCHED_KEY )
				{
					// pressed item
					key_cnt = VALID_TOUCHED_KEY;
				}*/
				/* else if( ++key_cnt >= 100 )
				{
					// pressed item
					if( !(key_cnt % 5) )
					{
						// process the pressed item on continuous touch
						switch( tagval )
						{
						}
					}
				} */
			}
			else if( pt->tag )
			{
				// untouched
				if( key_cnt >= VALID_TOUCHED_KEY )
				{
					// flush the touch queue
					FlushTouchQueue();

					// process the pressed item on touch release
					switch( pt->tag )
					{
					}
				}
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}
		else
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		
	}

	return RetVal;
	
}

/*!
**  \fn short WinSetTime( char *sTitle )
**  \brief Set time window handler
**  \param sTitle The window's title string
**  \return 1: SET, 0: QUIT
**/
short WinSetTime( char *sTitle )
{
	short RetVal = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	TM TimeDate;
	unsigned char tm_min2;   /* Minutes (0-59) */
	unsigned char tm_hour2;  /* Hour (0-23) */
	unsigned long key_cnt = 0;
	unsigned char loop = 1;
	// format: hh.mm
	const char sFmt[] = "%02u . %02u";
	const char sFmt2[] = "%02u     %02u";
	unsigned char scrollON[2] = { 0 };	//!< scrolling status
	unsigned short preYscroll[2] = { 0 };	//!< previous Y value for vertical scrolling
	short Dy = 0;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );
	
	// read the current time
	rtcRd( &TimeDate );

	// inizializza valori per scroll DOWN (decremento)
	tm_hour2 = TimeDate.tm_hour;
	if( tm_hour2 > 0 )
	{
		tm_hour2--;
	}
	else
	{
		tm_hour2 = 23;
	}
	tm_min2 = TimeDate.tm_min;
	if( tm_min2 > 0 )
	{
		tm_min2--;
	}
	else
	{
		tm_min2 = 59;
	}
	mysprintf( str3, sFmt2, tm_hour2, tm_min2 );
	
	// inizializza valori per scroll UP (incremento)
	tm_hour2 = TimeDate.tm_hour;
	if( tm_hour2 < 23 )
	{
		tm_hour2++;
	}
	else
	{
		tm_hour2 = 0;
	}
	tm_min2 = TimeDate.tm_min;
	if( tm_min2 < 59 )
	{
		tm_min2++;
	}
	else
	{
		tm_min2 = 0;
	}
	mysprintf( str4, sFmt2, tm_hour2, tm_min2 );
	
	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
		}

		if( !((tagval == TAG_VSCROLL_BASE + 0) && (scrollON[0] == 1))
			&& !((tagval == TAG_VSCROLL_BASE + 1) && (scrollON[1] == 1))
			)
		{
			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			
			// draw QUIT bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// QUIT bitmap tag
				TagRegister( TAG_QUIT, 0, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height), p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
			}
			
			// draw OK bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-p_bmhdrOK->bmhdr.Width)*16, (FT_DispHeight-p_bmhdrOK->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// OK bitmap tag
				TagRegister( TAG_OK, (FT_DispWidth-p_bmhdrOK->bmhdr.Width), (FT_DispHeight-p_bmhdrOK->bmhdr.Height), p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height );
			}
			
			// draw the centered time
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColor_tab[UserColorId].addon) );	// Text Color
			mysprintf( str2, sFmt, TimeDate.tm_hour, TimeDate.tm_min );
			Ft_Gpu_CoCmd_Text( pHost, (FT_DispWidth/2), 130, fontMax, OPT_CENTER, String2Font(str2, fontMax) );

			// draw the scroll DOWN parameter value
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 130-25-5-14, fontTitle, OPT_CENTER, String2Font(str3, fontTitle) );

			// draw the scroll UP parameter value
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 130+25+5+14, fontTitle, OPT_CENTER, String2Font(str4, fontTitle) );

			if( tagval == TOUCH_TAG_INIT )
			{
				// hour scroll tag
				TagRegister( TAG_VSCROLL_BASE+0, (FT_DispWidth/2-10-50), 31, 40, FT_DispHeight-30 );
				// minute scroll tag
				TagRegister( TAG_VSCROLL_BASE+1, (FT_DispWidth/2+10), 31, 40, FT_DispHeight-30 );
			}
				
			// title
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );

			Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
			Ft_Gpu_CoCmd_Swap( pHost );
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
		}
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() )
		{
			// screenshot
			DoScreenshot( SSHOT_SetTime );
		}
	#endif	// SCREENSHOT

		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
			}
		} while( event.status == osOK );
		
			
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			scrollON[0] = 0;
			scrollON[1] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			scrollON[0] = 0;
			scrollON[1] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					loop = 0;	// exit
				}
			}
		}
		else if( ((pt->tag == TAG_QUIT) || (pt->tag == TAG_OK)) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			if( (pt->tag == TAG_OK) && (userSubState == 1) )
			{
				// set time
				tm_hour2 = TimeDate.tm_hour;
				tm_min2 = TimeDate.tm_min;
				rtcRd( &TimeDate );	// read the current date
				TimeDate.tm_hour = tm_hour2;
				TimeDate.tm_min = tm_min2;
				TimeDate.tm_sec = 0;
				if( !rtcWr( &TimeDate ) )
					RetVal = 1;
			}
			
			loop = 0;	// exit
		}
		else if( tagval == TAG_VSCROLL_BASE+0 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset minute vertical scroll
			scrollON[1] = 0;
			// hour vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[0] )
				{
				default:
				case 0:
					preYscroll[0] = pt->y;
					scrollON[0] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[0];
						if( Dy > 5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[0] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP hour value
							tm_hour2 = TimeDate.tm_hour;
							tm_min2 = TimeDate.tm_min;
							if( tm_min2 < 59 )
							{
								tm_min2++;
							}
							else
							{
								tm_min2 = 0;
							}
							mysprintf( str4, sFmt2, tm_hour2, tm_min2 );
							// down hour
							if( TimeDate.tm_hour > 0 )
							{
								TimeDate.tm_hour--;
							}
							else
							{
								TimeDate.tm_hour = 23;
							}
							// update the scroll DOWN hour value
							tm_hour2 = TimeDate.tm_hour;
							if( tm_hour2 > 0 )
							{
								tm_hour2--;
							}
							else
							{
								tm_hour2 = 23;
							}
							tm_min2 = TimeDate.tm_min;
							if( tm_min2 > 0 )
							{
								tm_min2--;
							}
							else
							{
								tm_min2 = 59;
							}
							mysprintf( str3, sFmt2, tm_hour2, tm_min2 );
						}
						else if( Dy < -5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[0] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN hour value
							tm_hour2 = TimeDate.tm_hour;
							tm_min2 = TimeDate.tm_min;
							if( tm_min2 > 0 )
							{
								tm_min2--;
							}
							else
							{
								tm_min2 = 59;
							}
							mysprintf( str3, sFmt2, tm_hour2, tm_min2 );
							// up hour
							if( TimeDate.tm_hour < 23 )
							{
								TimeDate.tm_hour++;
							}
							else
							{
								TimeDate.tm_hour = 0;
							}
							// update the scroll UP hour value
							tm_hour2 = TimeDate.tm_hour;
							if( tm_hour2 < 23 )
							{
								tm_hour2++;
							}
							else
							{
								tm_hour2 = 0;
							}
							tm_min2 = TimeDate.tm_min;
							if( tm_min2 < 59 )
							{
								tm_min2++;
							}
							else
							{
								tm_min2 = 0;
							}
							mysprintf( str4, sFmt2, tm_hour2, tm_min2 );
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[0] = 0;
			}
		}
		else if( tagval == TAG_VSCROLL_BASE+1 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset hour vertical scroll
			scrollON[0] = 0;
			// minute vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[1] )
				{
				default:
				case 0:
					preYscroll[1] = pt->y;
					scrollON[1] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[1];
						if( Dy > 5 )
						{
							scrollON[1] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[1] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP minute value
							tm_hour2 = TimeDate.tm_hour;
							tm_min2 = TimeDate.tm_min;
							if( tm_hour2 < 23 )
							{
								tm_hour2++;
							}
							else
							{
								tm_hour2 = 0;
							}
							mysprintf( str4, sFmt2, tm_hour2, tm_min2 );
							// down minute
							if( TimeDate.tm_min > 0 )
							{
								TimeDate.tm_min--;
							}
							else
							{
								TimeDate.tm_min = 59;
							}
							// update the scroll DOWN minute value
							tm_hour2 = TimeDate.tm_hour;
							if( tm_hour2 > 0 )
							{
								tm_hour2--;
							}
							else
							{
								tm_hour2 = 23;
							}
							tm_min2 = TimeDate.tm_min;
							if( tm_min2 > 0 )
							{
								tm_min2--;
							}
							else
							{
								tm_min2 = 59;
							}
							mysprintf( str3, sFmt2, tm_hour2, tm_min2 );
						}
						else if( Dy < -5 )
						{
							scrollON[1] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[1] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN minute value
							tm_hour2 = TimeDate.tm_hour;
							tm_min2 = TimeDate.tm_min;
							if( tm_hour2 > 0 )
							{
								tm_hour2--;
							}
							else
							{
								tm_hour2 = 23;
							}
							mysprintf( str3, sFmt2, tm_hour2, tm_min2 );
							// up minute
							if( TimeDate.tm_min < 59 )
							{
								TimeDate.tm_min++;
							}
							else
							{
								TimeDate.tm_min = 0;
							}
							// update the scroll UP minute value
							tm_hour2 = TimeDate.tm_hour;
							if( tm_hour2 < 23 )
							{
								tm_hour2++;
							}
							else
							{
								tm_hour2 = 0;
							}
							tm_min2 = TimeDate.tm_min;
							if( tm_min2 < 59 )
							{
								tm_min2++;
							}
							else
							{
								tm_min2 = 0;
							}
							mysprintf( str4, sFmt2, tm_hour2, tm_min2 );
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[1] = 0;
			}
		}
		else if( (scrollON[0] != 2)
				&& (scrollON[1] != 2)
				)
		{
			tagval = Read_Keypad( pt );	// read the keys
			if( tagval )
			{
				// touched
				userCounterTime = USER_TIMEOUT_SHOW_60;
				if( Flag.Key_Detect )
				{
					// first touch
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
				/*else if( ++key_cnt >= VALID_TOUCHED_KEY )
				{
					// pressed item
					key_cnt = VALID_TOUCHED_KEY;
				}*/
				/* else if( ++key_cnt >= 100 )
				{
					// pressed item
					if( !(key_cnt % 5) )
					{
						// process the pressed item on continuous touch
						switch( tagval )
						{
						}
					}
				} */
			}
			else if( pt->tag )
			{
				// untouched
				if( key_cnt >= VALID_TOUCHED_KEY )
				{
					// flush the touch queue
					FlushTouchQueue();

					// process the pressed item on touch release
					switch( pt->tag )
					{
					}
				}
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}
		else
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		
	}

	return RetVal;
	
}

/*!
**  \fn short WinSetDate( char *sTitle )
**  \brief Set date window handler
**  \param sTitle The window's title string
**  \return 1: SET, 0: QUIT
**/
short WinSetDate( char *sTitle )
{
	short RetVal = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	TM TimeDate;
	unsigned char tm_wday2;  /* Day of week (0-6, 0=Sunday) */
	unsigned char tm_mday2;  /* Day of month (1-31) */
	unsigned char tm_mon2;   /* Month (1-12) */
	unsigned short tm_year2;  /* Year */
	unsigned long key_cnt = 0;
	unsigned char loop = 1;
	char *s3 = NULL;
	char *s4 = NULL;
	const char sFmt[] = "%02u / %02u / %04u";
	const char sFmt2[] = "%02u         %02u           %04u";
	unsigned char scrollON[3] = { 0 };	//!< scrolling status
	unsigned short preYscroll[3] = { 0 };	//!< previous Y value for vertical scrolling
	short Dy = 0;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );
	
	if( (s3 = (char *)malloc(MAX_LEN_STRINGA+1)) == NULL )
	{
		return RetVal;
	}
	else if( (s4 = (char *)malloc(MAX_LEN_STRINGA+1)) == NULL )
	{
		free( s3 );
		return RetVal;
	}

	// read the current time
	rtcRd( &TimeDate );

	// inizializza valori per scroll DOWN (decremento)
	tm_mday2 = TimeDate.tm_mday;
	if( tm_mday2 > 1 )
	{
		tm_mday2--;
	}
	else
	{
		tm_mday2 = 31;
	}
	tm_mon2 = TimeDate.tm_mon;
	if( tm_mon2 > 1 )
	{
		tm_mon2--;
	}
	else
	{
		tm_mon2 = 12;
	}
	tm_year2 = TimeDate.tm_year;
	if( tm_year2 > MIN_YEAR_RTC )
	{
		tm_year2--;
	}
	else
	{
		tm_year2 = MAX_YEAR;
	}
	mysprintf( s3, sFmt2, tm_mday2, tm_mon2, tm_year2 );
	
	// inizializza valori per scroll UP (incremento)
	tm_mday2 = TimeDate.tm_mday;
	if( tm_mday2 < 31 )
	{
		tm_mday2++;
	}
	else
	{
		tm_mday2 = 1;
	}
	tm_mon2 = TimeDate.tm_mon;
	if( tm_mon2 < 12 )
	{
		tm_mon2++;
	}
	else
	{
		tm_mon2 = 1;
	}
	tm_year2 = TimeDate.tm_year;
	if( tm_year2 < MAX_YEAR )
	{
		tm_year2++;
	}
	else
	{
		tm_year2 = MIN_YEAR_RTC;
	}
	mysprintf( s4, sFmt2, tm_mday2, tm_mon2, tm_year2 );
	
	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
		}

		if( !((tagval == TAG_VSCROLL_BASE + 0) && (scrollON[0] == 1))
			&& !((tagval == TAG_VSCROLL_BASE + 1) && (scrollON[1] == 1))
			&& !((tagval == TAG_VSCROLL_BASE + 2) && (scrollON[2] == 1))
			)
		{
			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			
			// draw QUIT bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// QUIT bitmap tag
				TagRegister( TAG_QUIT, 0, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height), p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
			}
			
			// draw OK bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-p_bmhdrOK->bmhdr.Width)*16, (FT_DispHeight-p_bmhdrOK->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// OK bitmap tag
				TagRegister( TAG_OK, (FT_DispWidth-p_bmhdrOK->bmhdr.Width), (FT_DispHeight-p_bmhdrOK->bmhdr.Height), p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height );
			}
			
			// draw the centered date
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColor_tab[UserColorId].addon) );	// Text Color
			GetMsg( 0, str2, iLanguage, sWeekDay_tab[TimeDate.tm_wday] );
			//str2[4] = '\0';	// only the first 3 characters for week day
			Ft_Gpu_CoCmd_Text( pHost, 0, 130, fontMax, OPT_CENTERY, String2Font(str2, fontMax) );
			mysprintf( str2, sFmt, TimeDate.tm_mday, TimeDate.tm_mon, TimeDate.tm_year );
			Ft_Gpu_CoCmd_Text( pHost, (FT_DispWidth-1), 130, fontMax, OPT_CENTERY|OPT_RIGHTX, String2Font(str2, fontMax) );

			// draw the scroll DOWN parameter value
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, (FT_DispWidth-1), 130-25-5-14, fontTitle, OPT_CENTERY|OPT_RIGHTX, String2Font(s3, fontTitle) );

			// draw the scroll UP parameter value
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, (FT_DispWidth-1), 130+25+5+14, fontTitle, OPT_CENTERY|OPT_RIGHTX, String2Font(s4, fontTitle) );

			if( tagval == TOUCH_TAG_INIT )
			{
				// format: "## / ## / ####"
				/* // '#' width: 24, '/' width: 17, ' ' width: 10
				// day scroll tag
				TagRegister( TAG_VSCROLL_BASE+0, (FT_DispWidth-1-2*24-10-17-10-2*24-10-17-10-4*24), 31, 2*24, FT_DispHeight-30 );
				// month scroll tag
				TagRegister( TAG_VSCROLL_BASE+1, (FT_DispWidth-1-2*24-10-17-10-4*24), 31, 2*24, FT_DispHeight-30 );
				// year scroll tag
				TagRegister( TAG_VSCROLL_BASE+2, (FT_DispWidth-1-4*24), 31, 4*24, FT_DispHeight-30 ); */
				// format: "##         ##           ####"
				// '#' width: 14, '/' width: 10, ' ' width: 6
				// day scroll tag
				TagRegister( TAG_VSCROLL_BASE+0, (FT_DispWidth-1-2*14-9*6-2*14-11*6-4*14), 31, 2*24, FT_DispHeight-30 );
				// month scroll tag
				TagRegister( TAG_VSCROLL_BASE+1, (FT_DispWidth-1-2*14-11*6-4*14), 31, 2*24, FT_DispHeight-30 );
				// year scroll tag
				TagRegister( TAG_VSCROLL_BASE+2, (FT_DispWidth-1-4*24), 31, 4*24, FT_DispHeight-30 );
			}
			
				
			// title
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );

			Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
			Ft_Gpu_CoCmd_Swap( pHost );
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
		}
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() )
		{
			// screenshot
			DoScreenshot( SSHOT_SetDate );
		}
	#endif	// SCREENSHOT

		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
			}
		} while( event.status == osOK );
		
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			scrollON[0] = 0;
			scrollON[1] = 0;
			scrollON[2] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			scrollON[0] = 0;
			scrollON[1] = 0;
			scrollON[2] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					loop = 0;	// exit
				}
			}
		}
		else if( ((pt->tag == TAG_QUIT) || (pt->tag == TAG_OK)) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			if( (pt->tag == TAG_OK) && (userSubState == 1) )
			{
				// set date
				tm_wday2 = TimeDate.tm_wday;
				tm_mday2 = TimeDate.tm_mday;
				tm_mon2 = TimeDate.tm_mon;
				tm_year2 = TimeDate.tm_year;
				rtcRd( &TimeDate );	// read the current time
				TimeDate.tm_wday = tm_wday2;
				TimeDate.tm_mday = tm_mday2;
				TimeDate.tm_mon = tm_mon2;
				TimeDate.tm_year = tm_year2;
				if( !rtcWr( &TimeDate ) )
					RetVal = 1;
			}
			
			loop = 0;	// exit
		}
		else if( tagval == TAG_VSCROLL_BASE+0 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset month vertical scroll
			scrollON[1] = 0;
			// reset year vertical scroll
			scrollON[2] = 0;
			// day vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[0] )
				{
				default:
				case 0:
					preYscroll[0] = pt->y;
					scrollON[0] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[0];
						if( Dy > 5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[0] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP day value
							tm_mday2 = TimeDate.tm_mday;
							tm_mon2 = TimeDate.tm_mon;
							if( tm_mon2 < 12 )
							{
								tm_mon2++;
							}
							else
							{
								tm_mon2 = 1;
							}
							tm_year2 = TimeDate.tm_year;
							if( tm_year2 < MAX_YEAR )
							{
								tm_year2++;
							}
							else
							{
								tm_year2 = MIN_YEAR_RTC;
							}
							mysprintf( s4, sFmt2, tm_mday2, tm_mon2, tm_year2 );
							// down day
							if( TimeDate.tm_mday > 1 )
							{
								TimeDate.tm_mday--;
							}
							else
							{
								TimeDate.tm_mday = 31;
							}
							TimeDate.tm_wday = rtcGetWeekDay( TimeDate.tm_mday, TimeDate.tm_mon, TimeDate.tm_year );
							// update the scroll DOWN day value
							tm_mday2 = TimeDate.tm_mday;
							if( tm_mday2 > 1 )
							{
								tm_mday2--;
							}
							else
							{
								tm_mday2 = 31;
							}
							tm_mon2 = TimeDate.tm_mon;
							if( tm_mon2 > 1 )
							{
								tm_mon2--;
							}
							else
							{
								tm_mon2 = 12;
							}
							tm_year2 = TimeDate.tm_year;
							if( tm_year2 > MIN_YEAR_RTC )
							{
								tm_year2--;
							}
							else
							{
								tm_year2 = MAX_YEAR;
							}
							mysprintf( s3, sFmt2, tm_mday2, tm_mon2, tm_year2 );
						}
						else if( Dy < -5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[0] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN day value
							tm_mday2 = TimeDate.tm_mday;
							tm_mon2 = TimeDate.tm_mon;
							if( tm_mon2 > 1 )
							{
								tm_mon2--;
							}
							else
							{
								tm_mon2 = 12;
							}
							tm_year2 = TimeDate.tm_year;
							if( tm_year2 > MIN_YEAR_RTC )
							{
								tm_year2--;
							}
							else
							{
								tm_year2 = MAX_YEAR;
							}
							mysprintf( s3, sFmt2, tm_mday2, tm_mon2, tm_year2 );
							// up day
							if( TimeDate.tm_mday < 31 )
							{
								TimeDate.tm_mday++;
							}
							else
							{
								TimeDate.tm_mday = 1;
							}
							TimeDate.tm_wday = rtcGetWeekDay( TimeDate.tm_mday, TimeDate.tm_mon, TimeDate.tm_year );
							// update the scroll UP day value
							tm_mday2 = TimeDate.tm_mday;
							if( tm_mday2 < 31 )
							{
								tm_mday2++;
							}
							else
							{
								tm_mday2 = 1;
							}
							tm_mon2 = TimeDate.tm_mon;
							if( tm_mon2 < 12 )
							{
								tm_mon2++;
							}
							else
							{
								tm_mon2 = 1;
							}
							tm_year2 = TimeDate.tm_year;
							if( tm_year2 < MAX_YEAR )
							{
								tm_year2++;
							}
							else
							{
								tm_year2 = MIN_YEAR_RTC;
							}
							mysprintf( s4, sFmt2, tm_mday2, tm_mon2, tm_year2 );
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[0] = 0;
			}
		}
		else if( tagval == TAG_VSCROLL_BASE+1 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset day vertical scroll
			scrollON[0] = 0;
			// reset year vertical scroll
			scrollON[2] = 0;
			// month vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[1] )
				{
				default:
				case 0:
					preYscroll[1] = pt->y;
					scrollON[1] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[1];
						if( Dy > 5 )
						{
							scrollON[1] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[1] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP month value
							tm_mon2 = TimeDate.tm_mon;
							tm_mday2 = TimeDate.tm_mday;
							if( tm_mday2 < 31 )
							{
								tm_mday2++;
							}
							else
							{
								tm_mday2 = 1;
							}
							tm_year2 = TimeDate.tm_year;
							if( tm_year2 < MAX_YEAR )
							{
								tm_year2++;
							}
							else
							{
								tm_year2 = MIN_YEAR_RTC;
							}
							mysprintf( s4, sFmt2, tm_mday2, tm_mon2, tm_year2 );
							// down month
							if( TimeDate.tm_mon > 1 )
							{
								TimeDate.tm_mon--;
							}
							else
							{
								TimeDate.tm_mon = 12;
							}
							TimeDate.tm_wday = rtcGetWeekDay( TimeDate.tm_mday, TimeDate.tm_mon, TimeDate.tm_year );
							// update the scroll DOWN month value
							tm_mday2 = TimeDate.tm_mday;
							if( tm_mday2 > 1 )
							{
								tm_mday2--;
							}
							else
							{
								tm_mday2 = 31;
							}
							tm_mon2 = TimeDate.tm_mon;
							if( tm_mon2 > 1 )
							{
								tm_mon2--;
							}
							else
							{
								tm_mon2 = 12;
							}
							tm_year2 = TimeDate.tm_year;
							if( tm_year2 > MIN_YEAR_RTC )
							{
								tm_year2--;
							}
							else
							{
								tm_year2 = MAX_YEAR;
							}
							mysprintf( s3, sFmt2, tm_mday2, tm_mon2, tm_year2 );
						}
						else if( Dy < -5 )
						{
							scrollON[1] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[1] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN month value
							tm_mon2 = TimeDate.tm_mon;
							tm_mday2 = TimeDate.tm_mday;
							if( tm_mday2 > 1 )
							{
								tm_mday2--;
							}
							else
							{
								tm_mday2 = 31;
							}
							tm_year2 = TimeDate.tm_year;
							if( tm_year2 > MIN_YEAR_RTC )
							{
								tm_year2--;
							}
							else
							{
								tm_year2 = MAX_YEAR;
							}
							mysprintf( s3, sFmt2, tm_mday2, tm_mon2, tm_year2 );
							// up month
							if( TimeDate.tm_mon < 12 )
							{
								TimeDate.tm_mon++;
							}
							else
							{
								TimeDate.tm_mon = 1;
							}
							TimeDate.tm_wday = rtcGetWeekDay( TimeDate.tm_mday, TimeDate.tm_mon, TimeDate.tm_year );
							// update the scroll UP month value
							tm_mday2 = TimeDate.tm_mday;
							if( tm_mday2 < 31 )
							{
								tm_mday2++;
							}
							else
							{
								tm_mday2 = 1;
							}
							tm_mon2 = TimeDate.tm_mon;
							if( tm_mon2 < 12 )
							{
								tm_mon2++;
							}
							else
							{
								tm_mon2 = 1;
							}
							tm_year2 = TimeDate.tm_year;
							if( tm_year2 < MAX_YEAR )
							{
								tm_year2++;
							}
							else
							{
								tm_year2 = MIN_YEAR_RTC;
							}
							mysprintf( s4, sFmt2, tm_mday2, tm_mon2, tm_year2 );
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[1] = 0;
			}
		}
		else if( tagval == TAG_VSCROLL_BASE+2 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset day vertical scroll
			scrollON[0] = 0;
			// reset month vertical scroll
			scrollON[1] = 0;
			// year vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[2] )
				{
				default:
				case 0:
					preYscroll[2] = pt->y;
					scrollON[2] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[2];
						if( Dy > 5 )
						{
							scrollON[2] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[2] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP year value
							tm_mday2 = TimeDate.tm_mday;
							if( tm_mday2 < 31 )
							{
								tm_mday2++;
							}
							else
							{
								tm_mday2 = 1;
							}
							tm_mon2 = TimeDate.tm_mon;
							if( tm_mon2 < 12 )
							{
								tm_mon2++;
							}
							else
							{
								tm_mon2 = 1;
							}
							tm_year2 = TimeDate.tm_year;
							mysprintf( s4, sFmt2, tm_mday2, tm_mon2, tm_year2 );
							// down year
							if( TimeDate.tm_year > MIN_YEAR_RTC )
							{
								TimeDate.tm_year--;
							}
							else
							{
								TimeDate.tm_year = MAX_YEAR;
							}
							TimeDate.tm_wday = rtcGetWeekDay( TimeDate.tm_mday, TimeDate.tm_mon, TimeDate.tm_year );
							// update the scroll DOWN year value
							tm_mday2 = TimeDate.tm_mday;
							if( tm_mday2 > 1 )
							{
								tm_mday2--;
							}
							else
							{
								tm_mday2 = 31;
							}
							tm_mon2 = TimeDate.tm_mon;
							if( tm_mon2 > 1 )
							{
								tm_mon2--;
							}
							else
							{
								tm_mon2 = 12;
							}
							tm_year2 = TimeDate.tm_year;
							if( tm_year2 > MIN_YEAR_RTC )
							{
								tm_year2--;
							}
							else
							{
								tm_year2 = MAX_YEAR;
							}
							mysprintf( s3, sFmt2, tm_mday2, tm_mon2, tm_year2 );
						}
						else if( Dy < -5 )
						{
							scrollON[2] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[2] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN year value
							tm_mday2 = TimeDate.tm_mday;
							if( tm_mday2 > 1 )
							{
								tm_mday2--;
							}
							else
							{
								tm_mday2 = 31;
							}
							tm_mon2 = TimeDate.tm_mon;
							if( tm_mon2 > 1 )
							{
								tm_mon2--;
							}
							else
							{
								tm_mon2 = 12;
							}
							tm_year2 = TimeDate.tm_year;
							mysprintf( s3, sFmt2, tm_mday2, tm_mon2, tm_year2 );
							// up year
							if( TimeDate.tm_year < MAX_YEAR )
							{
								TimeDate.tm_year++;
							}
							else
							{
								TimeDate.tm_year = MIN_YEAR_RTC;
							}
							TimeDate.tm_wday = rtcGetWeekDay( TimeDate.tm_mday, TimeDate.tm_mon, TimeDate.tm_year );
							// update the scroll UP year value
							tm_mday2 = TimeDate.tm_mday;
							if( tm_mday2 < 31 )
							{
								tm_mday2++;
							}
							else
							{
								tm_mday2 = 1;
							}
							tm_mon2 = TimeDate.tm_mon;
							if( tm_mon2 < 12 )
							{
								tm_mon2++;
							}
							else
							{
								tm_mon2 = 1;
							}
							tm_year2 = TimeDate.tm_year;
							if( tm_year2 < MAX_YEAR )
							{
								tm_year2++;
							}
							else
							{
								tm_year2 = MIN_YEAR_RTC;
							}
							mysprintf( s4, sFmt2, tm_mday2, tm_mon2, tm_year2 );
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[2] = 0;
			}
		}
		else if( (scrollON[0] != 2)
				&& (scrollON[1] != 2)
				&& (scrollON[2] != 2)
				)
		{
			tagval = Read_Keypad( pt );	// read the keys
			if( tagval )
			{
				// touched
				userCounterTime = USER_TIMEOUT_SHOW_60;
				if( Flag.Key_Detect )
				{
					// first touch
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
				/*else if( ++key_cnt >= VALID_TOUCHED_KEY )
				{
					// pressed item
					key_cnt = VALID_TOUCHED_KEY;
				}*/
				/* else if( ++key_cnt >= 100 )
				{
					// pressed item
					if( !(key_cnt % 5) )
					{
						// process the pressed item on continuous touch
						switch( tagval )
						{
						}
					}
				} */
			}
			else if( pt->tag )
			{
				// untouched
				if( key_cnt >= VALID_TOUCHED_KEY )
				{
					// flush the touch queue
					FlushTouchQueue();

					// process the pressed item on touch release
					switch( pt->tag )
					{
					}
				}
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}
		else
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		
	}

	if( s4 != NULL )
	{
		free( s4 );
	}
	if( s3 != NULL )
	{
		free( s3 );
	}

	return RetVal;
	
}

/*!
** \fn short WinPassword( char *title, unsigned char *Char, unsigned char *Number )
** \brief PASSWORD window handler
** \param sTitle The window's title string
** \param Char The pointer to the password character part 
** \param Number The pointer to the password numerical part 
** \return 1: OK, 0: quit
**/
short WinPassword( char *sTitle, unsigned char *Char, unsigned char *Number )
{
	short RetVal = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	unsigned char vChar = *Char;
	unsigned char vNumber = *Number;
	unsigned char vChar2;
	unsigned char vNumber2;
	unsigned long key_cnt = 0;
	unsigned char loop = 1;
	unsigned char scrollON[2] = { 0 };	//!< scrolling status
	unsigned short preYscroll[2] = { 0 };	//!< previous Y value for vertical scrolling
	short Dy = 0;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );

	// inizializza valori per scroll DOWN (decremento)
	vChar2 = vChar;
	if( vChar2 > PASS1_MIN )
	{
		vChar2--;
	}
	else
	{
		vChar2 = PASS1_MAX;
	}
	str3[0] = vChar2;
	str3[1] = '\0';
	vNumber2 = vNumber;
	if( vNumber2 > PASS2_MIN )
	{
		vNumber2--;
	}
	else
	{
		vNumber2 = PASS2_MAX;
	}
	str3[2] = Hex2Ascii( vNumber2/10 );
	str3[3] = Hex2Ascii( vNumber2%10 );
	str3[4] = '\0';
	
	// inizializza valori per scroll UP (incremento)
	vChar2 = vChar;
	if( ++vChar2 > PASS1_MAX )
	{
		vChar2 = PASS1_MIN;
	}
	str4[0] = vChar2;
	str4[1] = '\0';
	vNumber2 = vNumber;
	if( ++vNumber2 > PASS2_MAX )
	{
		vNumber2 = PASS2_MIN;
	}
	str4[2] = Hex2Ascii( vNumber2/10 );
	str4[3] = Hex2Ascii( vNumber2%10 );
	str4[4] = '\0';
	

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
		}

		if( !((tagval == TAG_VSCROLL_BASE + 0) && (scrollON[0] == 1))
			&& !((tagval == TAG_VSCROLL_BASE + 1) && (scrollON[1] == 1))
			)
		{
			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			
			// draw QUIT bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// QUIT bitmap tag
				TagRegister( TAG_QUIT, 0, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height), p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
			}
			
			// draw OK bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-p_bmhdrOK->bmhdr.Width)*16, (FT_DispHeight-p_bmhdrOK->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// OK bitmap tag
				TagRegister( TAG_OK, (FT_DispWidth-p_bmhdrOK->bmhdr.Width), (FT_DispHeight-p_bmhdrOK->bmhdr.Height), p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height );
			}
			
			// draw the centered password
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColor_tab[UserColorId].addon) );	// Text Color
			str2[0] = vChar;
			str2[1] = '\0';
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2-60, 160, FONT_MAX_DEF, OPT_CENTERY, str2 );
			str2[2] = Hex2Ascii( vNumber/10 );
			str2[3] = Hex2Ascii( vNumber%10 );
			str2[4] = '\0';
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2+60, 160, FONT_MAX_DEF, OPT_CENTERY|OPT_RIGHTX, &str2[2] );

			// draw the scroll DOWN parameter value
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2-60, 160-25-5-14, FONT_TITLE_DEF, OPT_CENTERY, str3 );
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2+60, 160-25-5-14, FONT_TITLE_DEF, OPT_CENTERY|OPT_RIGHTX, &str3[2] );

			// draw the scroll UP parameter value
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2-60, 160+25+5+14, FONT_TITLE_DEF, OPT_CENTERY, str4 );
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2+60, 160+25+5+14, FONT_TITLE_DEF, OPT_CENTERY|OPT_RIGHTX, &str4[2] );

			if( tagval == TOUCH_TAG_INIT )
			{
				// CHAR scroll tag
				TagRegister( TAG_VSCROLL_BASE+0, (FT_DispWidth/2-55), 31, 40, FT_DispHeight-30 );
				// NUMBER scroll tag
				TagRegister( TAG_VSCROLL_BASE+1, (FT_DispWidth/2+60-45), 31, 40, FT_DispHeight-30 );
			}
			
				
			// title
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );

			Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
			Ft_Gpu_CoCmd_Swap( pHost );
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
		}
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() )
		{
			// screenshot
			DoScreenshot( SSHOT_Password );
		}
	#endif	// SCREENSHOT

		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
			}
		} while( event.status == osOK );
		
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			scrollON[0] = 0;
			scrollON[1] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			scrollON[0] = 0;
			scrollON[1] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					loop = 0;	// exit
				}
			}
		}
		else if( ((pt->tag == TAG_QUIT) || (pt->tag == TAG_OK)) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			if( pt->tag == TAG_OK )
			{
				*Char = vChar;
				*Number = vNumber;
				RetVal = 1;
			}
			
			loop = 0;	// exit
		}
		else if( tagval == TAG_VSCROLL_BASE+0 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset NUMBER vertical scroll
			scrollON[1] = 0;
			// CHAR vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[0] )
				{
				default:
				case 0:
					preYscroll[0] = pt->y;
					scrollON[0] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[0];
						if( Dy > 5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[0] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP CHAR value
							vChar2 = vChar;
							str4[0] = vChar2;
							str4[1] = '\0';
							vNumber2 = vNumber;
							if( ++vNumber2 > PASS2_MAX )
							{
								vNumber2 = PASS2_MIN;
							}
							str4[2] = Hex2Ascii( vNumber2/10 );
							str4[3] = Hex2Ascii( vNumber2%10 );
							str4[4] = '\0';
							// down CHAR
							if( vChar > PASS1_MIN )
							{
								vChar--;
							}
							else
							{
								vChar = PASS1_MAX;
							}
							// update the scroll DOWN CHAR value
							vChar2 = vChar;
							if( vChar2 > PASS1_MIN )
							{
								vChar2--;
							}
							else
							{
								vChar2 = PASS1_MAX;
							}
							str3[0] = vChar2;
							str3[1] = '\0';
							vNumber2 = vNumber;
							if( vNumber2 > PASS2_MIN )
							{
								vNumber2--;
							}
							else
							{
								vNumber2 = PASS2_MAX;
							}
							str3[2] = Hex2Ascii( vNumber2/10 );
							str3[3] = Hex2Ascii( vNumber2%10 );
							str3[4] = '\0';
						}
						else if( Dy < -5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[0] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN CHAR value
							vChar2 = vChar;
							str3[0] = vChar2;
							str3[1] = '\0';
							vNumber2 = vNumber;
							if( vNumber2 > PASS2_MIN )
							{
								vNumber2--;
							}
							else
							{
								vNumber2 = PASS2_MAX;
							}
							str3[2] = Hex2Ascii( vNumber2/10 );
							str3[3] = Hex2Ascii( vNumber2%10 );
							str3[4] = '\0';
							// up CHAR
							if( ++vChar > PASS1_MAX )
							{
								vChar = PASS1_MIN;
							}
							// update the scroll UP CHAR value
							vChar2 = vChar;
							if( ++vChar2 > PASS1_MAX )
							{
								vChar2 = PASS1_MIN;
							}
							str4[0] = vChar2;
							str4[1] = '\0';
							vNumber2 = vNumber;
							if( ++vNumber2 > PASS2_MAX )
							{
								vNumber2 = PASS2_MIN;
							}
							str4[2] = Hex2Ascii( vNumber2/10 );
							str4[3] = Hex2Ascii( vNumber2%10 );
							str4[4] = '\0';
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[0] = 0;
			}
		}
		else if( tagval == TAG_VSCROLL_BASE+1 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset CHAR vertical scroll
			scrollON[0] = 0;
			// NUMBER vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[1] )
				{
				default:
				case 0:
					preYscroll[1] = pt->y;
					scrollON[1] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[1];
						if( Dy > 5 )
						{
							scrollON[1] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[1] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP NUMBER value
							vChar2 = vChar;
							if( ++vChar2 > PASS1_MAX )
							{
								vChar2 = PASS1_MIN;
							}
							str4[0] = vChar2;
							str4[1] = '\0';
							vNumber2 = vNumber;
							str4[2] = Hex2Ascii( vNumber2/10 );
							str4[3] = Hex2Ascii( vNumber2%10 );
							str4[4] = '\0';
							// down NUMBER
							if( vNumber > PASS2_MIN )
							{
								vNumber--;
							}
							else
							{
								vNumber = PASS2_MAX;
							}
							// update the scroll DOWN NUMBER value
							vChar2 = vChar;
							if( vChar2 > PASS1_MIN )
							{
								vChar2--;
							}
							else
							{
								vChar2 = PASS1_MAX;
							}
							str3[0] = vChar2;
							str3[1] = '\0';
							vNumber2 = vNumber;
							if( vNumber2 > PASS2_MIN )
							{
								vNumber2--;
							}
							else
							{
								vNumber2 = PASS2_MAX;
							}
							str3[2] = Hex2Ascii( vNumber2/10 );
							str3[3] = Hex2Ascii( vNumber2%10 );
							str3[4] = '\0';
						}
						else if( Dy < -5 )
						{
							scrollON[1] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[1] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN NUMBER value
							vChar2 = vChar;
							if( vChar2 > PASS1_MIN )
							{
								vChar2--;
							}
							else
							{
								vChar2 = PASS1_MAX;
							}
							str3[0] = vChar2;
							str3[1] = '\0';
							vNumber2 = vNumber;
							str3[2] = Hex2Ascii( vNumber2/10 );
							str3[3] = Hex2Ascii( vNumber2%10 );
							str3[4] = '\0';
							// up NUMBER
							if( ++vNumber > PASS2_MAX )
							{
								vNumber = PASS2_MIN;
							}
							// update the scroll UP NUMBER value
							vChar2 = vChar;
							if( ++vChar2 > PASS1_MAX )
							{
								vChar2 = PASS1_MIN;
							}
							str4[0] = vChar2;
							str4[1] = '\0';
							vNumber2 = vNumber;
							if( ++vNumber2 > PASS2_MAX )
							{
								vNumber2 = PASS2_MIN;
							}
							str4[2] = Hex2Ascii( vNumber2/10 );
							str4[3] = Hex2Ascii( vNumber2%10 );
							str4[4] = '\0';
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[1] = 0;
			}
		}
		else if( (scrollON[0] != 2)
				&& (scrollON[1] != 2)
				)
		{
			tagval = Read_Keypad( pt );	// read the keys
			if( tagval )
			{
				// touched
				userCounterTime = USER_TIMEOUT_SHOW_60;
				if( Flag.Key_Detect )
				{
					// first touch
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
				/*else if( ++key_cnt >= VALID_TOUCHED_KEY )
				{
					// pressed item
					key_cnt = VALID_TOUCHED_KEY;
				}*/
				/* else if( ++key_cnt >= 100 )
				{
					// pressed item
					if( !(key_cnt % 5) )
					{
						// process the pressed item on continuous touch
						switch( tagval )
						{
						}
					}
				} */
			}
			else if( pt->tag )
			{
				// untouched
				if( key_cnt >= VALID_TOUCHED_KEY )
				{
					// flush the touch queue
					FlushTouchQueue();

					// process the pressed item on touch release
					switch( pt->tag )
					{
					}
				}
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}
		else
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		
	}

	return RetVal;
	
}

/*!
** \fn short WinConfirm( char *sTitle, char *msg )
** \brief CONFIRM window handler
** \param sTitle The window's title string
** \param msg The description string of what to confirm
** \return 1: OK, 0: quit
**/
short WinConfirm( char *sTitle, char *msg )
{
	short RetVal = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	//unsigned long key_cnt = 0;
	unsigned char loop = 1;
	short i;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );
	

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
		}

		Ft_Gpu_CoCmd_Dlstart(pHost);
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		// description string
		if( msg != NULL )
		{
			if( (i = GetFont4StringWidth( msg, fontTitle, FT_DispWidth )) > 0 )
			{
				Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 100, i, OPT_CENTER, String2Font(msg, i) );
			}
		}
		
		// draw QUIT bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 100*16, 150*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// QUIT bitmap tag
			TagRegister( TAG_QUIT, 100, 150, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
		}
		
		// draw OK bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 316*16, 150*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// OK bitmap tag
			TagRegister( TAG_OK, 316, 150, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height );
		}
			
		// title
		Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
		Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );

		Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
		Ft_Gpu_CoCmd_Swap( pHost );
		Ft_App_Flush_Co_Buffer( pHost );
		Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
			}
		} while( event.status == osOK );
		
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			Flag.Key_Detect = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			Flag.Key_Detect = 0;
			//key_cnt = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					loop = 0;	// exit
				}
			}
		}
		else if( ((pt->tag == TAG_QUIT) || (pt->tag == TAG_OK)) && !pt->status )
		{
			// flush the touch queue
			FlushTouchQueue();
			
			if( pt->tag == TAG_OK )
			{
				RetVal = 1;
			}
			
			loop = 0;	// exit
		}
	}

	return RetVal;
}

/*!
** \fn short WinSetParWaitResult( unsigned short iPar, char *sTitle, char *sDesc )
** \brief Send the setting command for the parameter and wait the setting result
** \param iPar The parameter index
** \param sTitle The window's title string
** \param sDesc The parameter description string
** \return 1: SUCCESSFUL, 0: FAILED, -1: TIMEOUT
**/
short WinSetParWaitResult( unsigned short iPar, char *sTitle, char *sDesc )
{
	short RetVal = 0;
	// short i;
	// OT_RESULT result = OT_RESULT_RUN;
	// BM_HEADER * p_bmhdrQUIT;
	// BM_HEADER * p_bmhdrOK;
	// BM_HEADER * p_bmhdr = NULL;
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];
	
	// // get bitmap QUIT into Graphic RAM
	// p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// // get bitmap OK into Graphic RAM
	// p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );
	
	OT_GetResult();	// reset setting result
	userSetPar( iPar );	// send the setting command
	
	// wait the setting result, if it is a PH2 parameter
	if( (pParTab->IndVar != NO_INDVAR) ||
		(iPar == P_ATTIVA_ESP) ||
		(iPar == P_TEST) ||
		(iPar == P_TEST_AUTO) ||
		(iPar == P_TEST_CALIB_FOTORES_ON) ||
		(iPar == P_TEST_CALIB_FOTORES_OFF) ||
		(iPar == P_FAN1_INTERPOL) ||
		(iPar == P_FAN2_INTERPOL) ||
		(iPar == P_FAN3_INTERPOL) ||
		(iPar == P_TON_COCLEA_INTERPOL) ||
		(iPar == P_TON_FAN1_INTERPOL) ||
		(iPar == P_RPM1_INTERPOL) ||
		(iPar == P_RPM2_INTERPOL) ||
		(iPar == P_LPM_INTERPOL) ||
		(iPar == P_ESP2_INTERPOL) ||
		(iPar == P_RFPANEL)
		)
	{
		/* // run the spinner for the waiting time
		Ft_Gpu_CoCmd_Dlstart(pHost);
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,0xFF,0));	// GREEN
		Ft_Gpu_CoCmd_Spinner(pHost, (FT_DispWidth/2),(FT_DispHeight/2),0,1);
		Ft_App_Flush_Co_Buffer( pHost );
		Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

		// wait for the setting result
		RetVal = -1;
		for( i=0; i<(3000/2); i++ )	// result timeout: 3s
		{
			Wdog();
			Ft_Gpu_Hal_Sleep(2);
			switch( result = OT_GetResult() )
			{
				case OT_RESULT_RUN:
				break;

				case OT_RESULT_OK:
					p_bmhdr = p_bmhdrOK;
					RetVal = 1;
					i = 3000;	// exit
				break;
				
				case OT_RESULT_FAILED:
					p_bmhdr = p_bmhdrQUIT;
					RetVal = 0;
					i = 3000;	// exit
				break;
				
				default:
					i = 3000;	// exit
				break;
			}
		}

		// stop spinner
		Ft_Gpu_Hal_WrCmd32(pHost, CMD_STOP);
		Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);

		if( p_bmhdr != NULL )
		{
			// draw OK/QUIT bitmap
			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( ((FT_DispWidth-p_bmhdr->bmhdr.Width)/2)*16, ((FT_DispHeight-p_bmhdr->bmhdr.Height)/2)*16 ) );

			// description
			if( sDesc != NULL )
			{
				if( (i = GetFont4StringWidth( (char *)sDesc, fontTitle, FT_DispWidth )) > 0 )
				{
					Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 50, i, OPT_CENTER, String2Font(sDesc, i) );
				}
			}
			
			// title
			if( sTitle != NULL )
			{
				/* Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_XY( 0, 0 ) );
				Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_SIZE( FT_DispWidth, 30 ) );
				Ft_Gpu_CoCmd_Gradient( pHost, 0, 0, 0x0000FF, FT_DispWidth-1, 0, 0x000000 ); *
				Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
				Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
				Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
				Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 30*16, (0)*16 ) );
				Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
				Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
				Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );
			}
			
			Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
			Ft_Gpu_CoCmd_Swap( pHost );
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

			Wdog();
			Ft_Gpu_Hal_Sleep(2000);
			Wdog();
		} */
		
		// enable the pop-up window for waiting the setting result
		RetVal = 1;
		WaitSetResult.iPar = iPar;
		strcpy( WaitSetResult.sTitle, sTitle );
		WaitSetResult.sDesc = sDesc;
		WaitSetResult.Time = 10000/USER_TIME_TICK;	// result timeout: 10s
		WaitSetResult.result = OT_RESULT_RUN;
		StDownload = 0;
		Flag.popup = 1;
		
		// flush the touch queue
		FlushTouchQueue();
	}
	else
	{
		RetVal = 1;
	}
	
	return RetVal;
}

/*!
** \fn short WinSetDisplay( unsigned short iPar, char *sDesc, char *sTitle )
**  \brief Set display (user color ID and LCD backlight time) window handler
**  \param iPar The parameter index (P_BACKLIGHT)
**  \param sDesc The parameter description string
**  \param sTitle The window's title string
**  \return 1: SET, 0: QUIT
**/
short WinSetDisplay( unsigned short iPar, char *sDesc, char *sTitle )
{
	short RetVal = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	unsigned char ColorID3;
	unsigned char ColorID;
	unsigned char ColorID4;
	unsigned long key_cnt = 0;
	unsigned char loop = 1;
	short i;
	unsigned char scrollON[2] = { 0 };	//!< scrolling status
	unsigned short preYscroll[2] = { 0 };	//!< previous Y value for vertical scrolling
	short Dy = 0;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );

	ColorID = UserColorId;
	userGetPar( P_BACKLIGHT );
	
	// inizializza valori per scroll DOWN (decremento)
	ColorID3 = ColorID;
	if( ColorID3 > 0 )
	{
		ColorID3--;
	}
	else
	{
		ColorID3 = UCOL_MAX-1;
	}
	userDecPar( P_BACKLIGHT, 1 );
	userVisPar( P_BACKLIGHT );
	strcpy( str3, str2 );
	userIncPar( P_BACKLIGHT, 1 );
	
	// inizializza valori per scroll UP (incremento)
	ColorID4 = ColorID;
	if( ++ColorID4 >= UCOL_MAX )
	{
		ColorID4 = 0;
	}
	userIncPar( P_BACKLIGHT, 1 );
	userVisPar( P_BACKLIGHT );
	strcpy( str4, str2 );
	userDecPar( P_BACKLIGHT, 1 );
	

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
		}

		if( !((tagval == TAG_VSCROLL_BASE + 0) && (scrollON[0] == 1))
			&& !((tagval == TAG_VSCROLL_BASE + 1) && (scrollON[1] == 1))
			)
		{
			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			
			// draw QUIT bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// QUIT bitmap tag
				TagRegister( TAG_QUIT, 0, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height), p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
			}
			
			// draw OK bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-p_bmhdrOK->bmhdr.Width)*16, (FT_DispHeight-p_bmhdrOK->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// OK bitmap tag
				TagRegister( TAG_OK, (FT_DispWidth-p_bmhdrOK->bmhdr.Width), (FT_DispHeight-p_bmhdrOK->bmhdr.Height), p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height );
			}
			
			// description
			if( sDesc != NULL )
			{
				if( (i = GetFont4StringWidth( (char *)sDesc, fontTitle, FT_DispWidth )) > 0 )
				{
					Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 50, i, OPT_CENTER, String2Font(sDesc, i) );
				}
			}

			// draw the user color ID value
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(ColorID) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth/2-10-50)*16, (160-25)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth/2-10)*16, (160+25)*16 ) );
			// draw the LCD backlight time value
			userVisPar( P_BACKLIGHT );
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColor_tab[UserColorId].addon) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2+10, 160, fontMax, OPT_CENTERY, String2Font(str2, fontMax) );

			// draw the scroll DOWN parameter value
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(ColorID3) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth/2-10-50)*16, (160-25-5-28)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth/2-10-22)*16, (160-25-5)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2+10, 160-25-5-14, fontTitle, OPT_CENTERY, String2Font(str3, fontTitle) );

			// draw the scroll UP parameter value
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(ColorID4) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth/2-10-50)*16, (160+25+5)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth/2-10-22)*16, (160+25+5+28)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2+10, 160+25+5+14, fontTitle, OPT_CENTERY, String2Font(str4, fontTitle) );

			if( tagval == TOUCH_TAG_INIT )
			{
				// user color ID scroll tag
				TagRegister( TAG_VSCROLL_BASE+0, (FT_DispWidth/2-10-50), 31, 40, FT_DispHeight-30 );
				// LCD backlight time scroll tag
				TagRegister( TAG_VSCROLL_BASE+1, (FT_DispWidth/2+10), 31, 40, FT_DispHeight-30 );
			}
				
			// title
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );

			Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
			Ft_Gpu_CoCmd_Swap( pHost );
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
		}
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() )
		{
			// screenshot
			DoScreenshot( SSHOT_SetDisplay );
		}
	#endif	// SCREENSHOT

		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
			}
		} while( event.status == osOK );
		
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			scrollON[0] = 0;
			scrollON[1] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			scrollON[0] = 0;
			scrollON[1] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					loop = 0;	// exit
				}
			}
		}
		else if( ((pt->tag == TAG_QUIT) || (pt->tag == TAG_OK)) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			if( pt->tag == TAG_OK )
			{
				userSetPar( P_BACKLIGHT );
				userParValue.b[0] = ColorID;
				userSetPar( P_USER_COLOR_ID );
				RetVal = 1;
			}
			
			loop = 0;	// exit
		}
		else if( tagval == TAG_VSCROLL_BASE+0 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset LCD backlight time vertical scroll
			scrollON[1] = 0;
			// user color ID vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[0] )
				{
				default:
				case 0:
					preYscroll[0] = pt->y;
					scrollON[0] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[0];
						if( Dy > 5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[0] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP parameter value
							ColorID4 = ColorID;
							// down 
							if( ColorID > 0 )
							{
								ColorID--;
							}
							else
							{
								ColorID = UCOL_MAX-1;
							}
							// update the scroll DOWN parameter value
							ColorID3 = ColorID;
							if( ColorID3 > 0 )
							{
								ColorID3--;
							}
							else
							{
								ColorID3 = UCOL_MAX-1;
							}
						}
						else if( Dy < -5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[0] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN parameter value
							ColorID3 = ColorID;
							// up 
							if( ++ColorID >= UCOL_MAX )
							{
								ColorID = 0;
							}
							// update the scroll UP parameter value
							ColorID4 = ColorID;
							if( ++ColorID4 >= UCOL_MAX )
							{
								ColorID4 = 0;
							}
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[0] = 0;
			}
		}
		else if( tagval == TAG_VSCROLL_BASE+1 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset user color ID vertical scroll
			scrollON[0] = 0;
			// LCD backlight time vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[1] )
				{
				default:
				case 0:
					preYscroll[1] = pt->y;
					scrollON[1] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[1];
						if( Dy > 5 )
						{
							scrollON[1] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[1] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP parameter value
							strcpy( str4, str2 );
							// down 
							userDecPar( P_BACKLIGHT, 1 );
							// update the scroll DOWN parameter value
							userDecPar( P_BACKLIGHT, 1 );
							userVisPar( P_BACKLIGHT );
							strcpy( str3, str2 );
							userIncPar( P_BACKLIGHT, 1 );
						}
						else if( Dy < -5 )
						{
							scrollON[1] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[1] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN parameter value
							strcpy( str3, str2 );
							// up 
							userIncPar( P_BACKLIGHT, 1 );
							// update the scroll UP parameter value
							userIncPar( P_BACKLIGHT, 1 );
							userVisPar( P_BACKLIGHT );
							strcpy( str4, str2 );
							userDecPar( P_BACKLIGHT, 1 );
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[1] = 0;
			}
		}
		else if( (scrollON[0] != 2)
				&& (scrollON[1] != 2)
				)
		{
			tagval = Read_Keypad( pt );	// read the keys
			if( tagval )
			{
				// touched
				userCounterTime = USER_TIMEOUT_SHOW_60;
				if( Flag.Key_Detect )
				{
					// first touch
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
				/*else if( ++key_cnt >= VALID_TOUCHED_KEY )
				{
					// pressed item
					key_cnt = VALID_TOUCHED_KEY;
				}*/
				/* else if( ++key_cnt >= 100 )
				{
					// pressed item
					if( !(key_cnt % 5) )
					{
						// process the pressed item on continuous touch
						switch( tagval )
						{
						}
					}
				} */
			}
			else if( pt->tag )
			{
				// untouched
				if( key_cnt >= VALID_TOUCHED_KEY )
				{
					// flush the touch queue
					FlushTouchQueue();

					// process the pressed item on touch release
					switch( pt->tag )
					{
					}
				}
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}
		else
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		
	}

	return RetVal;
	
}

/*!
** \fn unsigned char userExitMenu( void )
** \brief Return to Main Window from Menu Window
** \return The next User Interface state 
**/
unsigned char userExitMenu( void )
{
	// pop bitmap BITMAP_UNCHECKED from Graphic RAM
	res_bm_Release( BITMAP_UNCHECKED );
	m_bmhdrUNCHECKED = NULL;
	// pop bitmap BITMAP_CHECKED from Graphic RAM
	res_bm_Release( BITMAP_CHECKED );
	m_bmhdrCHECKED = NULL;
	// pop bitmap BITMAP_ESC from Graphic RAM
	res_bm_Release( BITMAP_ESC );
	m_bmhdrQUIT = NULL;
	Flag.popup = 0;
	
	return userVisAvvio();
}

/*! 
	\fn unsigned char EnterMenuInfo( void )
	\brief Ingresso nel MENU INFO
	\return Il codice dello stato di transizione
*/
unsigned char EnterMenuInfo( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, MENU_INFOS, InfoItem, NUMINFOITEM ) )
	{
		RetVal = USER_STATE_INFO;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuInfo( void )
	\brief Ritorno al MENU INFO
	\return Il codice dello stato di transizione
*/
unsigned char RetMenuInfo( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, InfoItem, NUMINFOITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuNetwork( void )
	\brief Ingresso nel MENU NETWORK
	\return Il codice dello stato di transizione
*/
unsigned char EnterMenuNetwork( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, MENU_NETWORK, NetworkItem, NUMNETWORKITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char EnterMenuWarn( void )
	\brief Ingresso nel MENU ANOMALIE
	\return Il codice dello stato di transizione
*/
unsigned char EnterMenuWarn( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_LIST, MENU_ANOMALIE, WarnItem, NUMWARNITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char EnterMainMenu( void )
	\brief Ingresso nel MAIN MENU
	\return Il codice dello stato di transizione
*/
unsigned char EnterMainMenu( void )
{
	char *sTitle;
	
	LivAccesso = LOGIN_FREE;	// reset login
	iLivMenu = 0;		// solo per Main Menu`

	// reset Setup Menu struct
	Setup_Menu[iLivMenu].current_page = 0;
	Setup_Menu[iLivMenu].current_selection = 0;
	
	sTitle = sMenuTitle[iLivMenu];
	strncpy( sTitle, GetMsg( 1, NULL, iLanguage, sMAINMENU_TITLE ), DISPLAY_MAX_COL );
	sTitle[DISPLAY_MAX_COL] = '\0';
	VisMenu( MENU_TYPE_LIST, sTitle, MenuItem, NUM_MENU_ITEM );
	
	userCounterTime = USER_TIMEOUT_SHOW_20;
	userNextPointer = userVisAvvio;	// visualizzazione a livello superiore
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char RetMainMenu( void )
	\brief Ritorno al MAIN MENU
	\return Il codice dello stato di transizione
*/
static unsigned char RetMainMenu( void )
{
	LivAccesso = LOGIN_FREE;	// reset login
	iLivMenu = 0;		// solo per Main Menu`
	
	VisMenu( MENU_TYPE_LIST, NULL, MenuItem, NUM_MENU_ITEM );
	
	userCounterTime = USER_TIMEOUT_SHOW_20;
	userNextPointer = userVisAvvio;	// visualizzazione a livello superiore
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuSettings( void )
	\brief Ingresso nel MENU IMPOSTAZIONI
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuSettings( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, MENU_IMPOSTAZIONI, SettingsItem, NUMSETTINGSITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuSettings( void )
	\brief Ritorno al MENU IMPOSTAZIONI
	\return Il codice dello stato di transizione
*/
static unsigned char RetMenuSettings( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, SettingsItem, NUMSETTINGSITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuRicette( void )
	\brief Ingresso nel MENU RICETTE
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuRicette( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sRICETTE_MENU, RicetteItem, NUMRICETTEITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char EnterMenuTec( void )
	\brief Ingresso nel MENU TECNICO
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuTec( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_LIST, MENU_TECNICO, TecnicoItem, NUMTECITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuTec( void )
	\brief Ritorno al MENU TECNICO
	\return Il codice dello stato di transizione
*/
unsigned char RetMenuTec( void )
{
	RetMenu( MENU_TYPE_LIST, TecnicoItem, NUMTECITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuSysCfg( void )
	\brief Ingresso nel MENU CONFIGURAZIONE SISTEMA
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuSysCfg( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_LIST, sCONFIG_SYSTEM, SysCfgItem, NUMSYSCFGITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuSysCfg( void )
	\brief Ritorno al MENU TECNICO
	\return Il codice dello stato di transizione
*/
static unsigned char RetMenuSysCfg( void )
{
	RetMenu( MENU_TYPE_LIST, SysCfgItem, NUMSYSCFGITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuCtrl( void )
	\brief Ingresso nel MENU CONTROLLO
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuCtrl( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_LIST, sCONTROLLO, CtrlItem, NUMCTRLITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuCtrl( void )
	\brief Ritorno al MENU CONTROLLO
	\return Il codice dello stato di transizione
*/
static unsigned char RetMenuCtrl( void )
{
	RetMenu( MENU_TYPE_LIST, CtrlItem, NUMCTRLITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuAttuaTra( void )
	\brief Ingresso nel MENU ATTUAZIONE TRANSITORIE
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuAttuaTra( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_LIST, sATTUA_TRANSITORIE, AttuaTraItem, NUMATTUATRAITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuAttuaTra( void )
	\brief Ritorno al MENU ATTUAZIONE TRANSITORIE
	\return Il codice dello stato di transizione
*/
static unsigned char RetMenuAttuaTra( void )
{
	RetMenu( MENU_TYPE_LIST, AttuaTraItem, NUMATTUATRAITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuAttuaPot( void )
	\brief Ingresso nel MENU ATTUAZIONE DI POTENZA
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuAttuaPot( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_LIST, sATTUA_POTENZA, AttuaPotItem, NUMATTUAPOTITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuAttuaPot( void )
	\brief Ritorno al MENU ATTUAZIONE DI POTENZA
	\return Il codice dello stato di transizione
*/
static unsigned char RetMenuAttuaPot( void )
{
	RetMenu( MENU_TYPE_LIST, AttuaPotItem, NUMATTUAPOTITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuAlarms( void )
	\brief Ingresso nel MENU ALLARMI
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuAlarms( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_LIST, sGESTIONE_ALLARMI, AlarmsItem, NUMALARMSITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuAlarms( void )
	\brief Ritorno al MENU ALLARMI
	\return Il codice dello stato di transizione
*/
static unsigned char RetMenuAlarms( void )
{
	RetMenu( MENU_TYPE_LIST, AlarmsItem, NUMALARMSITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuTest( void )
	\brief Ingresso nel MENU COLLAUDO
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuTest( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_LIST, sCOLLAUDO, TestItem, NUMTESTITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuTest( void )
	\brief Ritorno al MENU COLLAUDO
	\return Il codice dello stato di transizione
*/
static unsigned char RetMenuTest( void )
{
	RetMenu( MENU_TYPE_LIST, TestItem, NUMTESTITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuGen( void )
	\brief Ingresso nel MENU PARAMETRI GENERALI
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuGen( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, d_GEN_MENU, GenItem, NUMGENITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuGen( void )
	\brief Ritorno al MENU PARAMETRI GENERALI
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuGen( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, GenItem, NUMGENITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuBlkout( void )
	\brief Ingresso nel MENU BLACKOUT
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuBlkout( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sBLACKOUT, BlkoutItem, NUMBLKOUTITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuBlkout( void )
	\brief Ritorno al MENU BLACKOUT
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuBlkout( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, BlkoutItem, NUMBLKOUTITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuCoclea( void )
	\brief Ingresso nel MENU COCLEA
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuCoclea( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, d_COCLEA_MENU, CocleaItem, NUMCOCLEAITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuCoclea( void )
	\brief Ritorno al MENU COCLEA
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuCoclea( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, CocleaItem, NUMCOCLEAITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuScuot( void )
	\brief Ingresso nel MENU SCUOTITORE
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuScuot( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, d_SCUOTITORE_MENU, ScuotItem, NUMSCUOTITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuScuot( void )
	\brief Ritorno al MENU SCUOTITORE
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuScuot( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, ScuotItem, NUMSCUOTITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuFanAmb( void )
	\brief Ingresso nel MENU FAN AMBIENTE
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuFanAmb( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sFANAMB_MENU, FanAmb, NUMFANAMBITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuFanAmb( void )
	\brief Ritorno al MENU FAN AMBIENTE
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuFanAmb( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, FanAmb, NUMFANAMBITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuFan1( void )
	\brief Ingresso nel MENU FAN 1
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuFan1( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sFAN1_MENU, Fan1Item, NUMFANITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuFan1( void )
	\brief Ritorno al MENU FAN 1
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuFan1( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, Fan1Item, NUMFANITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuFan2( void )
	\brief Ingresso nel MENU FAN 2
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuFan2( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sFAN2_MENU, Fan2Item, NUMFANITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuFan2( void )
	\brief Ritorno al MENU FAN 2
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuFan2( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, Fan2Item, NUMFANITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuFan3( void )
	\brief Ingresso nel MENU FAN 3
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuFan3( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sFAN3_MENU, Fan3Item, NUMFANITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuFan3( void )
	\brief Ritorno al MENU FAN 3
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuFan3( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, Fan3Item, NUMFANITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuRicTra( void )
	\brief Ingresso nel MENU RICETTE TRANSITORIE
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuRicTra( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sRICETTE_MENU, RicTraItem, NUMRICTRAITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuRicTra( void )
	\brief Ritorno al MENU RICETTE TRANSITORIE
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuRicTra( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, RicTraItem, NUMRICTRAITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuEco( void )
	\brief Ingresso nel MENU ECO
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuEco( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, d_ECOSTOP_MENU, EcoItem, NUMECOSTOPITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuEco( void )
	\brief Ritorno al MENU ECO
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuEco( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, EcoItem, NUMECOSTOPITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuTFunz( void )
	\brief Ingresso nel MENU TEMPI DI FUNZIONAMENTO
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuTFunz( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sTEMPI_FUNZ_MENU, TFunzItem, NUMTFUNZITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuTFunz( void )
	\brief Ritorno al MENU TEMPI DI FUNZIONAMENTO
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuTFunz( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, TFunzItem, NUMTFUNZITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuIdro( void )
	\brief Ingresso nel MENU IDRO
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuIdro( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, d_IDRO_MENU, IdroItem, NUMIDROITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuIdro( void )
	\brief Ritorno al MENU IDRO
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuIdro( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, IdroItem, NUMIDROITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuParTra( void )
	\brief Ingresso nel MENU PARAMETRI TRANSITORI
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuParTra( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sPARAM_TRANSITORI_MENU, ParTraItem, NUMPARTRAITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuParTra( void )
	\brief Ritorno al MENU PARAMETRI TRANSITORI
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuParTra( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, ParTraItem, NUMPARTRAITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuPreAcc1( void )
	\brief Ingresso nel MENU PREACCENSIONE 1
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuPreAcc1( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sPREACC1_MENU, PreAcc1Item, NUMPREACC1ITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuPreAcc1( void )
	\brief Ritorno al MENU PREACCENSIONE 1
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuPreAcc1( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, PreAcc1Item, NUMPREACC1ITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuPreAcc2( void )
	\brief Ingresso nel MENU PREACCENSIONE 2
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuPreAcc2( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sPREACC2_MENU, PreAcc2Item, NUMPREACC2ITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuPreAcc2( void )
	\brief Ritorno al MENU PREACCENSIONE 2
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuPreAcc2( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, PreAcc2Item, NUMPREACC2ITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuPreAccHot( void )
	\brief Ingresso nel MENU PREACCENSIONE A CALDO
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuPreAccHot( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sPREACCCALDO_MENU, PreAccHotItem, NUMPREACCCALDOITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuPreAccHot( void )
	\brief Ritorno al MENU PREACCENSIONE A CALDO
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuPreAccHot( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, PreAccHotItem, NUMPREACCCALDOITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuAccA( void )
	\brief Ingresso nel MENU ACCENSIONE A
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuAccA( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sACCA_MENU, AccAItem, NUMACCAITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuAccA( void )
	\brief Ritorno al MENU ACCENSIONE A
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuAccA( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, AccAItem, NUMACCAITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuAccB( void )
	\brief Ingresso nel MENU ACCENSIONE B
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuAccB( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sACCB_MENU, AccBItem, NUMACCBITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuAccB( void )
	\brief Ritorno al MENU ACCENSIONE B
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuAccB( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, AccBItem, NUMACCBITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuFireONA( void )
	\brief Ingresso nel MENU FIRE-ON A
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuFireONA( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sFIREONA_MENU, FireONAItem, NUMFIREONAITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuFireONA( void )
	\brief Ritorno al MENU FIRE-ON A
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuFireONA( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, FireONAItem, NUMFIREONAITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuSpeA( void )
	\brief Ingresso nel MENU SPEGNIMENTO A
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuSpeA( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sSPEA_MENU, SpeAItem, NUMSPEAITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuSpeA( void )
	\brief Ritorno al MENU SPEGNIMENTO A
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuSpeA( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, SpeAItem, NUMSPEAITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuSpeB( void )
	\brief Ingresso nel MENU SPEGNIMENTO B
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuSpeB( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sSPEB_MENU, SpeBItem, NUMSPEBITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuSpeB( void )
	\brief Ritorno al MENU SPEGNIMENTO B
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuSpeB( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, SpeBItem, NUMSPEBITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuRaffA( void )
	\brief Ingresso nel MENU RAFFREDDAMENTO A
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuRaffA( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sRAFFA_MENU, RaffAItem, NUMRAFFAITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuRaffA( void )
	\brief Ritorno al MENU RAFFREDDAMENTO A
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuRaffA( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, RaffAItem, NUMRAFFAITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuParPot( void )
	\brief Ingresso nel MENU PARAMETRI DI POTENZA
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuParPot( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sPARAM_POTENZA_MENU, ParPotItem, NUMPARPOTITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuParPot( void )
	\brief Ritorno al MENU PARAMETRI DI POTENZA
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuParPot( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, ParPotItem, NUMPARPOTITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuPB( void )
	\brief Ingresso nel MENU PULIZIA BRACIERE
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuPB( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, d_PB_MENU, PBItem, NUMPBITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuPB( void )
	\brief Ritorno al MENU PULIZIA BRACIERE
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuPB( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, PBItem, NUMPBITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuRitEsp( void )
	\brief Ingresso nel MENU RITARDO ATTUAZIONE ESPULSORE
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuRitEsp( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sRITARDO_ATTUA_ESP_MENU, RitEspItem, NUMRITESPITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuRitEsp( void )
	\brief Ritorno al MENU RITARDO ATTUAZIONE ESPULSORE
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuRitEsp( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, RitEspItem, NUMRITESPITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuRitPot( void )
	\brief Ingresso nel MENU RITARDO STEP POTENZA
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuRitPot( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sRITARDO_STEP_POT_MENU, RitPotItem, NUMRITPOTITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuRitPot( void )
	\brief Ritorno al MENU RITARDO STEP POTENZA
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuRitPot( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, RitPotItem, NUMRITPOTITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuPot1( void )
	\brief Ingresso nel MENU POTENZA 1
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuPot1( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sPOT1_MENU, Pot1Item, NUMPOT1ITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuPot1( void )
	\brief Ritorno al MENU POTENZA 1
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuPot1( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, Pot1Item, NUMPOT1ITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuPot2( void )
	\brief Ingresso nel MENU POTENZA 2
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuPot2( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sPOT2_MENU, Pot2Item, NUMPOT2ITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuPot2( void )
	\brief Ritorno al MENU POTENZA 2
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuPot2( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, Pot2Item, NUMPOT2ITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuPot3( void )
	\brief Ingresso nel MENU POTENZA 3
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuPot3( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sPOT3_MENU, Pot3Item, NUMPOT3ITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuPot3( void )
	\brief Ritorno al MENU POTENZA 3
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuPot3( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, Pot3Item, NUMPOT3ITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuPot4( void )
	\brief Ingresso nel MENU POTENZA 4
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuPot4( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sPOT4_MENU, Pot4Item, NUMPOT4ITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuPot4( void )
	\brief Ritorno al MENU POTENZA 4
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuPot4( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, Pot4Item, NUMPOT4ITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuPot5( void )
	\brief Ingresso nel MENU POTENZA 5
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuPot5( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sPOT5_MENU, Pot5Item, NUMPOT5ITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuPot5( void )
	\brief Ritorno al MENU POTENZA 5
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuPot5( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, Pot5Item, NUMPOT5ITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuPot6( void )
	\brief Ingresso nel MENU POTENZA 6
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuPot6( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sPOT6_MENU, Pot6Item, NUMPOT6ITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuPot6( void )
	\brief Ritorno al MENU POTENZA 6
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuPot6( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, Pot6Item, NUMPOT6ITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuPuliz( void )
	\brief Ingresso nel MENU PULIZIA
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuPuliz( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sPULIZIA_MENU, PulizItem, NUMPULIZIAITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuPuliz( void )
	\brief Ritorno al MENU PULIZIA
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuPuliz( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, PulizItem, NUMPULIZIAITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuInterp( void )
	\brief Ingresso nel MENU INTERPOLAZIONE
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuInterp( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sINTERPOLAZIONE_MENU, InterpItem, NUMINTERPITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuInterp( void )
	\brief Ritorno al MENU INTERPOLAZIONE
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuInterp( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, InterpItem, NUMINTERPITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuAlmFumi( void )
	\brief Ingresso nel MENU ALLARME FUMI
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuAlmFumi( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sALLARME_FUMI_MENU, AlmFumiItem, NUMALMFUMIITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuAlmFumi( void )
	\brief Ritorno al MENU ALLARME FUMI
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuAlmFumi( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, AlmFumiItem, NUMALMFUMIITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuSensPlt( void )
	\brief Ingresso nel MENU SENSORE PELLET
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuSensPlt( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sSENSORE_PELLET_MENU, SensPltItem, NUMSENSPLTITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuSensPlt( void )
	\brief Ritorno al MENU SENSORE PELLET
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuSensPlt( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, SensPltItem, NUMSENSPLTITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuNoFiam( void )
	\brief Ingresso nel MENU ASSENZA FIAMMA
	\return Il codice dello stato di transizione
*
static unsigned char EnterMenuNoFiam( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sASSENZA_FIAMMA_MENU, NoFiamItem, NUMNOFIAMITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuNoFiam( void )
	\brief Ritorno al MENU ASSENZA FIAMMA
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuNoFiam( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, NoFiamItem, NUMNOFIAMITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuSensAria( void )
	\brief Ingresso nel MENU SENSORE PORTATA ARIA
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuSensAria( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sSENSORE_ARIA_MENU, SensAriaItem, NUMSENSARIAITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuSensAria( void )
	\brief Ritorno al MENU SENSORE PORTATA ARIA
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuSensAria( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, SensAriaItem, NUMSENSARIAITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuCmd( void )
	\brief Ingresso nel MENU TEST COMANDI
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuCmd( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sCOMANDI_MENU, CmdItem, NUMCMDITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuCmd( void )
	\brief Ritorno al MENU TEST COMANDI
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuCmd( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, CmdItem, NUMCMDITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuLoads( void )
	\brief Ingresso nel MENU TEST CARICHI
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuLoads( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sCARICHI_MENU, LoadsItem, NUMLOADSITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuLoads( void )
	\brief Ritorno al MENU TEST CARICHI
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuLoads( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, LoadsItem, NUMLOADSITEM );
	
	return USER_STATE_MENU;
}

/*! 
	\fn unsigned char EnterMenuAttua( void )
	\brief Ingresso nel MENU TEST ATTUAZIONI
	\return Il codice dello stato di transizione
*/
static unsigned char EnterMenuAttua( void )
{
	unsigned char RetVal = userCurrentState;
	
	if( EnterMenu( MENU_TYPE_USER_PARAM, sATTUA_MENU, AttuaItem, NUMATTUAITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*! 
	\fn unsigned char RetMenuAttua( void )
	\brief Ritorno al MENU TEST ATTUAZIONI
	\return Il codice dello stato di transizione
*
static unsigned char RetMenuAttua( void )
{
	RetMenu( MENU_TYPE_USER_PARAM, AttuaItem, NUMATTUAITEM );
	
	return USER_STATE_MENU;
}

/*! \fn unsigned char EnterVisParCod( void )
    \brief Ingresso nella schermata di richiesta codice parametro
	 \return Il codice dello stato di transizione
*/
static unsigned char EnterVisParCod( void )
{
	unsigned char RetVal = userCurrentState;

	if( LivAccesso >= TecnicoItem[TEC_ITEM_RICERCA_CODICE_PARAM].LivAccesso )
	{
		iLivMenu++;

		// entro nello stato di gestione codice parametro
		RetVal = userVisReqParCod();
	}

	return RetVal;
}

/*! 
	\fn unsigned char EnterLocalSettings( void )
	\brief Ingresso nel LOCAL SETTINGS MENU
	\return Il codice dello stato di transizione
*/
unsigned char EnterLocalSettings( void )
{
	char *sTitle;
	
	LivAccesso = LOGIN_FREE;	// reset login
	iLivMenu = 0;		// solo per Main Menu`

	// reset Setup Menu struct
	Setup_Menu[iLivMenu].current_page = 0;
	Setup_Menu[iLivMenu].current_selection = 0;
	
	sTitle = sMenuTitle[iLivMenu];
	strncpy( sTitle, GetMsg( 1, NULL, iLanguage, MENU_IMPOSTAZIONI ), DISPLAY_MAX_COL );
	sTitle[DISPLAY_MAX_COL] = '\0';
	VisMenu( MENU_TYPE_USER_PARAM, sTitle, LocalSettingsItem, NUMLOCALSETTINGSITEM );
	
	userCounterTime = USER_TIMEOUT_SHOW_20;
	userNextPointer = userVisInit;	// visualizzazione a livello superiore
	return USER_STATE_LOCAL_SETTINGS;
}

/*! 
	\fn unsigned char EnterMenuCronoProg( void )
	\brief Ingresso nel MENU CRONO PROGRAMMA SETTIMANALE
	\return Il codice dello stato di transizione
*/
unsigned char EnterMenuCronoProg( void )
{
	unsigned char RetVal = userCurrentState;
	
	// titolo menu variabile
	if( EnterMenu( MENU_TYPE_USER_PARAM, PRGSETTIMANALE, CronoSettimItem, NUMCRONOPARAMSETTITEM ) )
	{
		RetVal = USER_STATE_MENU;
	}
	
	return RetVal;
}

/*!
** \fn unsigned char ExitMenuCronoProg( void )
** \brief Return to Crono Window from Crono Prog Window
** \return The next User Interface state 
**/
unsigned char ExitMenuCronoProg( void )
{
	CRONO_PROG_WEEK *prg = &Crono.Crono_ProgWeek[iCronoPrg];
			
	// rifiuta l'abilitazione del programma se OraStart e/o OraStop sono a OFF
	if( (prg->OraStart == PROG_OFF) ||
		(prg->OraStop == PROG_OFF) )
	{
		prg->Enab.BIT.WeekEnab = 0;
	}
	
	// save the new settings for the Chrono program
	// Crono.Crono_ProgWeek[iCronoPrg] = prg;
	ScriviCronoProgWeekAbil( iCronoPrg, prg->Enab.BYTE );
	ScriviCronoProgWeekStart( iCronoPrg, prg->OraStart );
	ScriviCronoProgWeekStop( iCronoPrg, prg->OraStop );
	ScriviCronoProgWeekTempAria( iCronoPrg, prg->TempAria );
	ScriviCronoProgWeekTempH2O( iCronoPrg, prg->TempH2O );
	ScriviCronoProgWeekFire( iCronoPrg, prg->Fire );
	
	userPostSetCronoPar();
	
	LivAccesso = LOGIN_FREE;	// reset login
	iLivMenu = 0;		// solo per Main Menu`

	// reset Setup Menu struct
	Setup_Menu[iLivMenu].current_page = 0;
	Setup_Menu[iLivMenu].current_selection = 0;
	
	// pop bitmap BITMAP_UNCHECKED from Graphic RAM
	res_bm_Release( BITMAP_UNCHECKED );
	m_bmhdrUNCHECKED = NULL;
	// pop bitmap BITMAP_CHECKED from Graphic RAM
	res_bm_Release( BITMAP_CHECKED );
	m_bmhdrCHECKED = NULL;
	// pop bitmap BITMAP_ESC from Graphic RAM
	res_bm_Release( BITMAP_ESC );
	m_bmhdrQUIT = NULL;
	Flag.popup = 0;
	
	return userVisCrono();
}




