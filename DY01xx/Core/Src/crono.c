/*!
** \file crono.c
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief Chrono-Sleep handler
** 
** \version 1.0
**/

#include "main.h"
#include "timebase.h"
#include "DecodInput.h"
#include "OT_Plus.h"
#include "nvram.h"
#include "rtc.h"
#include "user.h"
#include "crono.h"

/************************** Locals Variables  *****************************/

SLEEPSTRUCT Sleep;

static unsigned char iProgCrono,iLastProgCrono;	// indice programma attivo
static unsigned char TempAria,LastTempAria;	// setpoint temperatura aria del programma attivo
static unsigned char TempH2O,LastTempH2O;		// setpoint temperatura acqua del programma attivo
static unsigned char Fire,LastFire;			// livello di potenza del programma attivo

#define NMAX_RETRY		3
static unsigned char cntRetry;	// contatore tentativi di invio attuazione Crono
static unsigned char cntRetrySleep;	// contatore tentativi di invio attuazione Sleep

// profili crono utente preimpostati
const CRONOPROFILE CronoProfile[NUMCRONOPROFILE] =
{
	// Profilo 1
	24, 92, 0xBE,		// fascia 1
	28, 96, 0xC1,		// fascia 2
	255, 255, 0x00,		// fascia 3
	255, 255, 0x00,		// fascia 4
	255, 255, 0x00,		// fascia 5
	255, 255, 0x00,		// fascia 6

	// Profilo 2
	24, 36, 0xBE,		// fascia 1
	68, 92, 0xBE,		// fascia 2
	28, 96, 0xC1,		// fascia 3
	255, 255, 0x00,		// fascia 4
	255, 255, 0x00,		// fascia 5
	255, 255, 0x00,		// fascia 6

	// Profilo 3
	24, 36, 0xBE,		// fascia 1
	48, 60, 0xBE,		// fascia 2
	68, 92, 0xBE,		// fascia 3
	28, 96, 0xC1,		// fascia 4
	255, 255, 0x00,		// fascia 5
	255, 255, 0x00,		// fascia 6

	// Profilo 4
	16, 32, 0xBE,		// fascia 1
	44, 56, 0xBE,		// fascia 2
	68, 88, 0xBE,		// fascia 3
	24, 92, 0xC1,		// fascia 4
	255, 255, 0x00,		// fascia 5
	255, 255, 0x00,		// fascia 6

	// Profilo 5
	20, 32, 0xFE,		// fascia 1
	48, 56, 0xFE,		// fascia 2
	68, 92, 0xFE,		// fascia 3
	20, 44, 0x81,		// fascia 4
	64, 96, 0x81,		// fascia 5
	255, 255, 0x00,		// fascia 6

	// Profilo 6
	20, 28, 0xBE,		// fascia 1
	52, 92, 0xBE,		// fascia 2
	32, 96, 0xC1,		// fascia 3
	255, 255, 0x00,		// fascia 4
	255, 255, 0x00,		// fascia 5
	255, 255, 0x00,		// fascia 6

	// Profilo 7
	28, 40, 0xBE,		// fascia 1
	52, 60, 0xBE,		// fascia 2
	72, 92, 0xBE,		// fascia 3
	28, 92, 0xC1,		// fascia 4
	255, 255, 0x00,		// fascia 5
	255, 255, 0x00,		// fascia 6

	// Profilo 8
	28, 40, 0xBE,		// fascia 1
	72, 92, 0xBE,		// fascia 2
	28, 92, 0xC1,		// fascia 3
	255, 255, 0x00,		// fascia 4
	255, 255, 0x00,		// fascia 5
	255, 255, 0x00,		// fascia 6

	// Profilo 9
	32, 48, 0xBE,		// fascia 1
	60, 76, 0xBE,		// fascia 2
	255, 255, 0x00,		// fascia 3
	255, 255, 0x00,		// fascia 4
	255, 255, 0x00,		// fascia 5
	255, 255, 0x00,		// fascia 6

	// Profilo 10
	72, 96, 0xA0,		// fascia 1
	24, 92, 0xC1,		// fascia 2
	255, 255, 0x00,		// fascia 3
	255, 255, 0x00,		// fascia 4
	255, 255, 0x00,		// fascia 5
	255, 255, 0x00,		// fascia 6

};


/************************** Locals Functions        ***********************/
static void GestSleep( void );
static BYTE IsCronoON( void );


/***************************************************************************
* FUNZIONE  : void InitCrono( void )
* SCOPO     : Inizializza gestore CRONO
* ARGOMENTI	:
* RETURN		:
***************************************************************************/
void InitCrono( void )
{
	// nessun programma settimanale attivo
	iProgCrono = NPROGWEEK;
	
	//STATO_CRONO = 0;	e' un flag non volatile
	AllineaStatoCrono();

	Sleep.Ore = SLEEP_OFF;
	Sleep.Minuti = 0;
}

/***************************************************************************
* FUNZIONE  : void GestCronoSleep( void )
* SCOPO     : Gestore CRONO e SLEEP
* ARGOMENTI	:
* RETURN		:
* NOTA		: Funzione da richiamare ogni 1 sec
***************************************************************************/
void GestCronoSleep( void )
{
	LIMITS_STRUCT *pLim;
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[16];		//!< packet buffer
	} sPkt;

	// Se il crono � abilitato aggiorno il suo stato ogni quarto d'ora.
	if( Crono.CronoMode.BIT.Enab != CRONO_OFF )
	{
		// attuazione crono
		if( IsCronoON() )
		{
			// Attuo solo se lo sleep � disabilitato e all'inizio di ogni quarto d'ora.
			if ((Sleep.Ore == SLEEP_OFF) && (DateTime.tm_sec == 0) &&
				((DateTime.tm_min == 0) || (DateTime.tm_min == 15) || (DateTime.tm_min == 30) || (DateTime.tm_min == 45)) )
			{
				// determino il tipo di attuazione crono
				if( !LOGIC_ONOFF  && !STATO_CRONO )
				{
					// Si invia il comando crono di on con impostazioni del programma
					// abilito invio impostazioni crono
					cntRetry = 0;
				}
				else if( !LOGIC_ONOFF && STATO_CRONO )
				{
					/*
						Non si invia il comando crono di on,
						rimangono le impostazioni utente
						(il crono mander� un comando di on solo dopo una sua fase di off).
					*/
					// disabilito invio impostazioni crono
					cntRetry = NMAX_RETRY;
				}
				else if( LOGIC_ONOFF && !STATO_CRONO )
				{
					/*
						Non si invia il comando crono di on,
						rimangono le impostazioni utente
						(il crono interverr� solo per spegnimento).
					*/
					// disabilito invio impostazioni crono
					cntRetry = NMAX_RETRY;
				}
				else	// ( LOGIC_ONOFF && STATO_CRONO )
				{
					/*
						Il crono e` gia` attivo e la stufa e` gia` accesa.
						Se l'utente (locale o remoto) ha impostato uno o pi� parametri
						operativi diversi da quelli previsti dal programma nell'intervallo
						precedente, le impostazioni utente vanno mantenute.
					*/
					if( iLastProgCrono < NPROGWEEK )
					{
						if( iLastProgCrono != iProgCrono )
						{
							// programmi diversi e contigui
							if(
								(TEMP_PAN != LastTempAria) ||
								(POWER_LEVEL != LastFire) ||
								(H2O_TSET != LastTempH2O)
								)
							{
								// avvenuta impostazione utente sui parametri operativi
								// disabilito invio impostazioni crono
								cntRetry = NMAX_RETRY;
							}
							else
							{
								// abilito invio impostazioni crono
								cntRetry = 0;
							}
						}
						else
						{
							// stesso programma
							// disabilito invio impostazioni crono
							cntRetry = NMAX_RETRY;
						}
					}
					else
					{
						// NON DOVREBBE MAI PASSARE DI QUA!!!
						// abilito invio impostazioni crono
						cntRetry = 0;
					}
				}
			}
			
			/*
				All'inizio del quarto d'ora, ogni 10 sec
				gestisce gli eventuali tentativi di reinvio impostazioni.
				Alla fine assume il cambio di stato crono.
			*/
			if( !(DateTime.tm_sec % 10) && (cntRetry < NMAX_RETRY) )
			{
				/*
					La presenza di un client WiFi connesso alla scheda
					inibisce l'attuazione del crono.
				*/
				if( !WIFI_CLIENT )
				{
					/*
						Sfrutto il flag di stato crono per riconoscere un cambio di stato
						ed attuare l'accensione. L'invio delle impostazioni dipende dallo
						stato corrente della stufa.
					*/
					if( !LOGIC_ONOFF )
					{
						LOGIC_ONOFF = 1;
						sPkt.pktLen = 0;
						sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
						sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
						sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_SINGLE_COIL;	// cmd
						sPkt.pkt[sPkt.pktLen++] = 0x00;	// addr H
						sPkt.pkt[sPkt.pktLen++] = 0x00;	// addr L
						sPkt.pkt[sPkt.pktLen++] = LOGIC_ONOFF;		// bit value
						OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
						
						//SetBacklight( LCD_BACKLIGHT_MAX );
						//starttimersec( TUSER100, USER_TIMEOUT_LIGHT_OFF );
						OT_DL_Pausa = OT_DL_PAUSE;
					}
					if( TEMP_PAN != TempAria )
					{
						TEMP_PAN = TempAria;
						sPkt.pktLen = 0;
						sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
						sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
						sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
						sPkt.pkt[sPkt.pktLen++] = HIBYTE( ADDR_TEMP_PAN );	// addr H
						sPkt.pkt[sPkt.pktLen++] = LOBYTE( ADDR_TEMP_PAN );	// addr L
						sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
						sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
						sPkt.pkt[sPkt.pktLen++] = 2;		// Nbyte
						sPkt.pkt[sPkt.pktLen++] = 0x00;		// byte H
						sPkt.pkt[sPkt.pktLen++] = TEMP_PAN;		// byte L
						OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
						OT_DL_Pausa = OT_DL_PAUSE;
					}
					if( POWER_LEVEL != Fire )
					{
						// check limiti variabili potenza
						pLim = (LIMITS_STRUCT *)&Limits2_tab[ADDR_POWER_LEVEL-THOLREG2_FIRST_ADDR];
						if( Fire > pLim->Max )
							Fire = pLim->Max;
						else if( Fire < pLim->Min )
							Fire = pLim->Min;
						POWER_LEVEL = Fire;
						sPkt.pktLen = 0;
						sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
						sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
						sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
						sPkt.pkt[sPkt.pktLen++] = HIBYTE( ADDR_POWER_LEVEL );	// addr H
						sPkt.pkt[sPkt.pktLen++] = LOBYTE( ADDR_POWER_LEVEL );	// addr L
						sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
						sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
						sPkt.pkt[sPkt.pktLen++] = 2;		// Nbyte
						sPkt.pkt[sPkt.pktLen++] = 0x00;		// byte H
						sPkt.pkt[sPkt.pktLen++] = POWER_LEVEL;		// byte L
						OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
						OT_DL_Pausa = OT_DL_PAUSE;
					}
					if( H2O_TSET != TempH2O )
					{
						H2O_TSET = TempH2O;
						if( userIsIdro() )
						{
							sPkt.pktLen = 0;
							sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
							sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
							sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
							sPkt.pkt[sPkt.pktLen++] = HIBYTE( ADDR_H2O_TSET );	// addr H
							sPkt.pkt[sPkt.pktLen++] = LOBYTE( ADDR_H2O_TSET );	// addr L
							sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
							sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
							sPkt.pkt[sPkt.pktLen++] = 2;		// Nbyte
							sPkt.pkt[sPkt.pktLen++] = 0x00;		// byte H
							sPkt.pkt[sPkt.pktLen++] = H2O_TSET;		// byte L
							OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
							OT_DL_Pausa = OT_DL_PAUSE;
						}
					}
				}
				
				cntRetry++;
			}
			if( cntRetry >= NMAX_RETRY )
			{
				// Riconosco di essere nel quarto d'ora con stufa accesa.
				if( !STATO_CRONO )
				{
					STATO_CRONO = 1;
				}
				LastTempAria = TempAria;
				LastFire = Fire;
				LastTempH2O = TempH2O;
			}
		}
		else
		{
			// Attuo solo se lo sleep � disabilitato e all'inizio di ogni quarto d'ora.
			if ((Sleep.Ore == SLEEP_OFF) && (DateTime.tm_sec == 0) &&
				((DateTime.tm_min == 0) || (DateTime.tm_min == 15) || (DateTime.tm_min == 30) || (DateTime.tm_min == 45)) )
			{
				// Sfrutto il flag di stato crono per riconoscere un cambio di stato ed attuare.
				if( STATO_CRONO )
				{
					// abilito invio impostazioni crono
					cntRetry = 0;
				}
				else
				{
					// disabilito invio impostazioni crono
					cntRetry = NMAX_RETRY;
				}
			}
			
			/*
				All'inizio del quarto d'ora, ogni 10 sec
				gestisce gli eventuali tentativi di reinvio impostazioni.
				Alla fine assume il cambio di stato crono.
			*/
			if( !(DateTime.tm_sec % 10) && (cntRetry < NMAX_RETRY) )
			{
				/*
					La presenza di un client WiFi connesso alla scheda
					inibisce l'attuazione del crono.
				*/
				if( !WIFI_CLIENT )
				{
					if( LOGIC_ONOFF )
					{
						// imposta stufa OFF da Crono
						LOGIC_ONOFF = 0;
						sPkt.pktLen = 0;
						sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
						sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
						sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_SINGLE_COIL;	// cmd
						sPkt.pkt[sPkt.pktLen++] = 0x00;	// addr H
						sPkt.pkt[sPkt.pktLen++] = 0x00;	// addr L
						sPkt.pkt[sPkt.pktLen++] = LOGIC_ONOFF;		// bit value
						OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
						OT_DL_Pausa = OT_DL_PAUSE;
					}
				}
				
				cntRetry++;
			}
			if( cntRetry >= NMAX_RETRY )
			{
				// Riconosco di essere nel quarto d'ora con stufa spenta.
				if( STATO_CRONO )
				{
					STATO_CRONO = 0;
				}
			}
		}
	}
	// Se il crono � disabilitato resetto il suo stato.
	else if (STATO_CRONO)
	{
		STATO_CRONO = 0;
	}

#ifndef DEBUG_SLEEP
	if (!LOGIC_ONOFF)
	{
		Sleep.Ore = SLEEP_OFF;
	}
	else
#endif
	if( Sleep.Ore != SLEEP_OFF )
	{
		GestSleep();
	}
	else
	{
		// predispongo prossimo invio OFF da Sleep
		cntRetrySleep = 0;
	}
}

/***************************************************************************
* FUNZIONE  : void GestSleep( void )
* SCOPO     : Gestore SLEEP
* ARGOMENTI	:
* RETURN		:
* NOTA		: Funzione da richiamare almeno ogni 1 min e con SLEEP abilitato.
***************************************************************************/
static void GestSleep( void )
{
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;
	BYTE timeNow;
	BYTE timeSleep;
	BYTE lbbApp;

	lbbApp = DateTime.tm_min / 10;
	timeNow = 6 * DateTime.tm_hour + lbbApp;	// ora corrente in decine di minuti
	lbbApp = Sleep.Minuti / 10;
	timeSleep = 6 * Sleep.Ore + lbbApp;	// ora Sleep in decine di minuti
	if (timeSleep == timeNow)
	{
		/*
			All'inizio della decina di minuti, ogni 10 sec
			gestisce gli eventuali tentativi di reinvio OFF da Sleep.
			Alla fine disabilita Sleep.
		*/
		if( !(DateTime.tm_sec % 10) && (cntRetrySleep < NMAX_RETRY) )
		{

			if( LOGIC_ONOFF )
			{
				// imposta stufa OFF da Sleep
				LOGIC_ONOFF = 0;
				sPkt.pktLen = 0;
				sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
				sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
				sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_SINGLE_COIL;	// cmd
				sPkt.pkt[sPkt.pktLen++] = 0x00;	// addr H
				sPkt.pkt[sPkt.pktLen++] = 0x00;	// addr L
				sPkt.pkt[sPkt.pktLen++] = LOGIC_ONOFF;		// bit value
				OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
				OT_DL_Pausa = OT_DL_PAUSE;
			}

			if( ++cntRetrySleep >= NMAX_RETRY )
			{
				// disabilito SLEEP
				Sleep.Ore = SLEEP_OFF;
				Sleep.Minuti = 0;
			}
		}
	}
}

/***************************************************************************
* FUNZIONE  : void AllineaStatoCrono( void )
* SCOPO     : Allinea lo stato CRONO all'ultima impostazione utente.
*				  Il cambio di stato avverr� all'inizio del successivo quarto d'ora.
* ARGOMENTI	:
* RETURN		:
***************************************************************************/
void AllineaStatoCrono( void )
{
	if( Crono.CronoMode.BIT.Enab != CRONO_OFF )
	{
		if( IsCronoON() )
		{
			// Riconosco di essere nel quarto d'ora con stufa accesa.
			if (LOGIC_ONOFF)
			{
				// la stufa e� gia� accesa
				// allineo lo stato Crono
				// come se fosse stato lui ad accenderla
				if (!STATO_CRONO)
				{
					STATO_CRONO = 1;
				}
				LastTempAria = TempAria;
				LastFire = Fire;
				LastTempH2O = TempH2O;
			}
			else
			{
				// la stufa e� spenta
				// allineo lo stato Crono
				// per accenderla al prossimo quarto d'ora
				if (STATO_CRONO)
				{
					STATO_CRONO = 0;
				}
			}
		}
		else
		{
			// Riconosco di essere nel quarto d'ora con stufa spenta.
			if (STATO_CRONO)
			{
				STATO_CRONO = 0;
			}
		}
	}
	// Se il crono � disabilitato resetto il suo stato.
	else if (STATO_CRONO)
	{
		STATO_CRONO = 0;
	}
}

/***************************************************************************
* FUNZIONE  : void AttuaStatoCrono( void )
* SCOPO     : Attua lo stato CRONO all'ultima impostazione utente.
* ARGOMENTI	:
* RETURN		:
***************************************************************************/
void AttuaStatoCrono( void )
{
	LIMITS_STRUCT *pLim;
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;

	if( Crono.CronoMode.BIT.Enab != CRONO_OFF )
	{
		if( IsCronoON() )
		{
			// Riconosco di essere nel quarto d'ora con stufa accesa.
			STATO_CRONO = 1;
			OT_DL_Pausa = OT_DL_PAUSE;
			// imposta stufa ON da Crono
			LOGIC_ONOFF = 1;
			sPkt.pktLen = 0;
			sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
			sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
			sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_SINGLE_COIL;	// cmd
			sPkt.pkt[sPkt.pktLen++] = 0x00;	// addr H
			sPkt.pkt[sPkt.pktLen++] = 0x00;	// addr L
			sPkt.pkt[sPkt.pktLen++] = LOGIC_ONOFF;		// bit value
			OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
			/* Le impostazioni per TempAria, Fire e TempH2O
				vanno sempre aggiornate secondo il programma corrente attivo. */
			TEMP_PAN = TempAria;
			sPkt.pktLen = 0;
			sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
			sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
			sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
			sPkt.pkt[sPkt.pktLen++] = HIBYTE( ADDR_TEMP_PAN );	// addr H
			sPkt.pkt[sPkt.pktLen++] = LOBYTE( ADDR_TEMP_PAN );	// addr L
			sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
			sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
			sPkt.pkt[sPkt.pktLen++] = 2;		// Nbyte
			sPkt.pkt[sPkt.pktLen++] = 0x00;		// byte H
			sPkt.pkt[sPkt.pktLen++] = TEMP_PAN;		// byte L
			OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
			LastTempAria = TempAria;
			
			// check limiti variabili potenza
			pLim = (LIMITS_STRUCT *)&Limits2_tab[ADDR_POWER_LEVEL-THOLREG2_FIRST_ADDR];
			if( Fire > pLim->Max )
				Fire = pLim->Max;
			else if( Fire < pLim->Min )
				Fire = pLim->Min;
			POWER_LEVEL = Fire;
			sPkt.pktLen = 0;
			sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
			sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
			sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
			sPkt.pkt[sPkt.pktLen++] = HIBYTE( ADDR_POWER_LEVEL );	// addr H
			sPkt.pkt[sPkt.pktLen++] = LOBYTE( ADDR_POWER_LEVEL );	// addr L
			sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
			sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
			sPkt.pkt[sPkt.pktLen++] = 2;		// Nbyte
			sPkt.pkt[sPkt.pktLen++] = 0x00;		// byte H
			sPkt.pkt[sPkt.pktLen++] = POWER_LEVEL;		// byte L
			OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
			LastFire = Fire;
			
			H2O_TSET = TempH2O;
			if( userIsIdro() )
			{
				sPkt.pktLen = 0;
				sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
				sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
				sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
				sPkt.pkt[sPkt.pktLen++] = HIBYTE( ADDR_H2O_TSET );	// addr H
				sPkt.pkt[sPkt.pktLen++] = LOBYTE( ADDR_H2O_TSET );	// addr L
				sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
				sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
				sPkt.pkt[sPkt.pktLen++] = 2;		// Nbyte
				sPkt.pkt[sPkt.pktLen++] = 0x00;		// byte H
				sPkt.pkt[sPkt.pktLen++] = H2O_TSET;		// byte L
				OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
			}
			LastTempH2O = TempH2O;
		}
		else
		{
			// Riconosco di essere nel quarto d'ora con stufa spenta.
			STATO_CRONO = 0;
			OT_DL_Pausa = OT_DL_PAUSE;
			// imposta stufa OFF da Crono
			LOGIC_ONOFF = 0;
			sPkt.pktLen = 0;
			sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
			sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
			sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_SINGLE_COIL;	// cmd
			sPkt.pkt[sPkt.pktLen++] = 0x00;	// addr H
			sPkt.pkt[sPkt.pktLen++] = 0x00;	// addr L
			sPkt.pkt[sPkt.pktLen++] = LOGIC_ONOFF;		// bit value
			OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
		}
	}
	// Se il crono � disabilitato resetto il suo stato.
	else if (STATO_CRONO)
	{
		STATO_CRONO = 0;
	}
}

/***************************************************************************
* FUNZIONE  : BYTE IsCronoON( void )
* SCOPO     : Check accensione stufa da comando CRONO
* ARGOMENTI	:
* RETURN		: 1: richiesta accensione, 0: richiesta spegnimento
***************************************************************************/
static BYTE IsCronoON( void )
{
	BYTE i, qOra;
	CRONO_PROG_WEEK *prw;
	BYTE ComandoON = 0;

	// salva ultimo programma settimanale
	iLastProgCrono = iProgCrono;
	
	// imposto stato stufa all'inizio di ogni quarto d'ora
	if( Crono.CronoMode.BIT.Enab == CRONO_ON )
	{
		/*
			La stufa si accende se ALMENO UNO dei programmi abilitati lo richiede.
			La stufa si spenge se TUTTI i programmi abilitati lo richiedono oppure
			se NON CI SONO programmi abilitati.
		*/
		for( i=0; i<NPROGWEEK; i++ )
		{
			prw = &Crono.Crono_ProgWeek[i];
			// se il programma e` abilitato ...
			if( prw->Enab.BIT.WeekEnab)
			{
				// ... anche nel giorno odierno della settimana e ...
				if( GetBitBMap(&prw->Enab.BYTE, DateTime.tm_wday) )
				{
					// ... i tempi di Start e di Stop sono impostati correttamente ...
					if( (prw->OraStart != PROG_OFF) && (prw->OraStop != PROG_OFF) &&
						(prw->OraStart < prw->OraStop) )
					{
						// determino il quarto d'ora corrente
						qOra = (DateTime.tm_hour * 4) + (DateTime.tm_min / 15);
						// confronto con START e STOP del programma
						if( (qOra >= prw->OraStart) && (qOra < prw->OraStop) )
						{
							// il programma settimanale richiede accensione
							ComandoON = 1;
							// ricopio i parametri operativi del programma settimanale
							TempAria = prw->TempAria;
							TempH2O = prw->TempH2O;
							Fire = prw->Fire;
							// si assumono i parametri del primo programma settimanale attivo
							break;
						}
					}
				}
			}
		}
		// programma settimanale attivo o nessuno
		iProgCrono = i;
	}
	else
	{
		// nessun programma settimanale attivo
		iProgCrono = NPROGWEEK;
	}

	return ComandoON;
}

/***************************************************************************
* FUNZIONE  : BYTE IsCronoProfile( void )
* SCOPO     : Check presenza profilo nella configurazione crono
* ARGOMENTI	:
* RETURN		: 0: profilo assente, >=1: indice profilo presente
***************************************************************************/
BYTE IsCronoProfile( void )
{
	BYTE RetVal = 0;
	BYTE i;
	CRONOPROFILE cp;
	CRONO_PROG_WEEK *prw;
	CRONO_PROFILE_PROG_WEEK *ppr;

	// copia la configurazione crono corrente come profilo
	for( i=0; i<NPROGWEEK; i++ )
	{
		prw = &Crono.Crono_ProgWeek[i];
		ppr = &cp.CronoProfile_ProgWeek[i];
		ppr->OraStart = prw->OraStart;
		ppr->OraStop = prw->OraStop;
		ppr->Enab.BYTE = prw->Enab.BYTE;
	}

	// confronta con i profili preimpostati
	for( i=0; i<NUMCRONOPROFILE; i++ )
	{
		if( !memcmp( (BYTE *)&cp, (BYTE *)&CronoProfile[i], sizeof(CRONOPROFILE) ) )
		{
			RetVal = i + 1;
			break;
		}
	}

	return RetVal;
}

/***************************************************************************
* FUNZIONE  : void LoadCronoProfile( BYTE iProfile )
* SCOPO     : Carica il profilo crono preimpostato indicato
* ARGOMENTI	: iProfile - indice profilo crono preimpostato (1..NUMCRONOPROFILE)
* RETURN		:
***************************************************************************/
void LoadCronoProfile( BYTE iProfile )
{
	BYTE i;
	CRONO_PROG_WEEK *prw;
	const CRONO_PROFILE_PROG_WEEK *ppr;

	iProfile--;		// 0..(NUMCRONOPROFILE-1)
	for( i=0; i<NPROGWEEK; i++ )
	{
		prw = &Crono.Crono_ProgWeek[i];
		ppr = &CronoProfile[iProfile].CronoProfile_ProgWeek[i];
		prw->OraStart = ppr->OraStart;
		prw->OraStop = ppr->OraStop;
		prw->Enab.BYTE = ppr->Enab.BYTE;
		/* Le impostazioni per TempAria, Fire e TempH2O
			sono uguali a quelle correnti per ogni fascia abilitata. */
		if( prw->Enab.BIT.WeekEnab )
		{
			prw->TempAria = TEMP_PAN;
			prw->TempH2O = H2O_TSET;
			prw->Fire = POWER_LEVEL;
		}
		else
		{
			prw->TempAria = DEFAULTSETTEMP;
			prw->TempH2O = H2O_DEFAULT;
			prw->Fire = POTDEFAULT;
		}
	}

}


