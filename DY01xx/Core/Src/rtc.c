/*!
**  \file rtc.c
**  \author Alessandro Vagniluca
**  \date 20/05/2015
**  \brief RTC handler
**  
**  \version 1.0
**/

#include "main.h"
#include "nvram.h"
#include "rtc.h"


/* defines */



/* variabili */
//static const char sDate_fmt[] = "%02u/%02u/%04u";
//static const char sTime_fmt[] = "%02u.%02u.%02u";

unsigned long SecCounter;

//static _near const unsigned char DayMonth[12] =
static const unsigned char DayMonth[12] =
{
	31,
	28,
	31,
	30,
	31,
	30,
	31,
	31,
	30,
	31,
	30,
	31
};
//static _near const unsigned long SecMonth[12] =
static const unsigned long SecMonth[12] =
{
	2678400L,
	2419200L,
	2678400L,
	2592000L,
	2678400L,
	2592000L,
	2678400L,
	2678400L,
	2592000L,
	2678400L,
	2592000L,
	2678400L
};

/*
	Secondo il calendario gregoriano un anno e` bisestile se il suo numero � divisibile per 4,
	con l'eccezione che gli anni secolari (quelli divisibili per 100)
	sono bisestili solo se divisibili per 400.
*/
#define IS_LEAP(y) ((((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0))?1:0)
static const unsigned long ys[2] = { 31536000L, 31622400L };
static const unsigned short ds[2] = { 365, 366 };


TM DateTime;


/* prototipi */





/***************************************************************************
* FUNZIONE  : short rtcInit( void )
* SCOPO     : Inizializza RTC
* ARGOMENTI :
* RETURN    : 1: OK, 0: ripristinato al default
***************************************************************************/
short rtcInit( void )
{
	short RetVal = 0;
	unsigned long NSec;
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;
	
	HAL_NVIC_DisableIRQ(RTC_WKUP_IRQn);	// Interrupt disable
	if( !nvramInit() )
	{
		// start from 01/01/MIN_YEAR_RTC 00.00.00
		DateTime.tm_wday = FIRST_WDAY_RTC;
		DateTime.tm_mday = 1;
		DateTime.tm_mon = 1;
		DateTime.tm_year = MIN_YEAR_RTC;
		DateTime.tm_hour = 0;
		DateTime.tm_min = 0;
		DateTime.tm_sec = 0;
		
		// numero di secondi trascorsi dal 01/01/MIN_YEAR 00.00.00
		NSec = NUM_SEC_START_RTC;

		// Configure the Date on RTC
		sDate.WeekDay = (FIRST_WDAY == 0) ? RTC_WEEKDAY_SUNDAY : FIRST_WDAY;
		sDate.Month = RTC_MONTH_JANUARY;
		sDate.Date = 1;
		sDate.Year = 0;	// base year: MIN_YEAR_RTC
		Wdog();
		HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
		Wdog();
		
		// Configure the TIME on RTC
		sTime.Hours = 0;
		sTime.Minutes = 0;
		sTime.Seconds = 0;
		sTime.SubSeconds = 0;
		sTime.TimeFormat = RTC_HOURFORMAT12_AM;
		sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
		sTime.StoreOperation = RTC_STOREOPERATION_RESET;
		Wdog();
		HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
		Wdog();
	}
	else
	{
		/* Get the RTC current Date */
		HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
		DateTime.tm_wday = (sDate.WeekDay == RTC_WEEKDAY_SUNDAY) ? 0 : sDate.WeekDay;
		DateTime.tm_mday = sDate.Date;
		DateTime.tm_mon = sDate.Month;
		DateTime.tm_year = (sDate.Year >= (MIN_YEAR % 100)) ? (sDate.Year+2000) : (sDate.Year+2100);
		DateTime.tm_year = sDate.Year + MIN_YEAR_RTC;
		
		/* Get the RTC current Time */
		HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
		DateTime.tm_hour = sTime.Hours;
		DateTime.tm_min = sTime.Minutes;
		DateTime.tm_sec = sTime.Seconds;
		
		// numero di secondi trascorsi dal 01/01/MIN_YEAR 00.00.00
		NSec = DateTime2Sec( &DateTime );

		RetVal = 1;
	}
	
	SecCounter = NSec;
	
    /* Clear the WAKEUPTIMER interrupt pending bit */
    __HAL_RTC_WAKEUPTIMER_CLEAR_FLAG(&hrtc, RTC_FLAG_WUTF);

	HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);	// Interrupt enable

	// calcolo il numero di secondi trascorsi dall'ultimo reset
	TBLACKOUT = 0;
	if( NSec > StatSec.LastNumSec )
	{
		NSec -= StatSec.LastNumSec;
		if( NSec > 65535 )
			TBLACKOUT = 65535;
		else
			TBLACKOUT = NSec;
	}

	RTC_TIME = 0;

	return RetVal;
}

/**
  * @brief  Wake Up Timer callback.
  * @param  hrtc: pointer to a RTC_HandleTypeDef structure that contains
  *                the configuration information for RTC.
  * @retval None
  */
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc)
{
	RTC_TIME = 1;
	
	SecCounter++;
}

/***************************************************************************
* FUNZIONE  : short rtcWr( TM *pTm )
* SCOPO     : Set RTC
* ARGOMENTI : pTm - new date/time structure
* RETURN    : 0: success, -1: error
***************************************************************************/
short rtcWr( TM *pTm )
{
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;

	if( !rtcCheckDateTime( pTm ) )
	{
		HAL_NVIC_DisableIRQ(RTC_WKUP_IRQn);	// Interrupt disable

		DateTime = *pTm;
		/* Adjust the counter value */
		SecCounter = DateTime2Sec( &DateTime );

		// Configure the Date on RTC
		sDate.WeekDay = (DateTime.tm_wday == 0) ? RTC_WEEKDAY_SUNDAY : DateTime.tm_wday;
		sDate.Month = DateTime.tm_mon;
		sDate.Date = DateTime.tm_mday;
		sDate.Year = DateTime.tm_year - MIN_YEAR_RTC;
		Wdog();
		HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
		Wdog();
		
		// Configure the Time on RTC
		sTime.Hours = DateTime.tm_hour;
		sTime.Minutes = DateTime.tm_min;
		sTime.Seconds = DateTime.tm_sec;
		sTime.SubSeconds = 0;
		sTime.TimeFormat = RTC_HOURFORMAT12_AM;
		sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
		sTime.StoreOperation = RTC_STOREOPERATION_RESET;
		Wdog();
		HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
		Wdog();

		HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);	// Interrupt enable

		return 0;
	}
	else
		return -1;
}

/***************************************************************************
* FUNZIONE  : unsigned long rtcRd( TM *pTm )
* SCOPO     : Read RTC
* ARGOMENTI : pTm - puntatore alla struttura data/ora oppure NULL
* RETURN    : Il valore corrente del contatore RTC
***************************************************************************/
unsigned long rtcRd( TM *pTm )
{
	unsigned long RetVal;
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;

	HAL_NVIC_DisableIRQ(RTC_WKUP_IRQn);	// Interrupt disable

	/*
		We must call HAL_RTC_GetDate() after HAL_RTC_GetTime() to unlock the values 
		in the higher-order calendar shadow registers to ensure consistency between the time and date values.
		Reading RTC current time locks the values in calendar shadow registers until Current date is read.
	*/
	
	/* Get the RTC current Time */
	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	DateTime.tm_hour = sTime.Hours;
	DateTime.tm_min = sTime.Minutes;
	DateTime.tm_sec = sTime.Seconds;

	/* Get the RTC current Date */
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
	DateTime.tm_wday = (sDate.WeekDay == RTC_WEEKDAY_SUNDAY) ? 0 : sDate.WeekDay;
	DateTime.tm_mday = sDate.Date;
	DateTime.tm_mon = sDate.Month;
	DateTime.tm_year = sDate.Year + MIN_YEAR_RTC;

	if( pTm != NULL )
		*pTm = DateTime;

	RetVal = SecCounter;

	HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);	// Interrupt enable

	return RetVal;
}

/***************************************************************************
* FUNZIONE  : short rtcCheckDateTime( TM *pTm )
* SCOPO     : Check a date/time structure
* ARGOMENTI : pTm - date/time structure to check
* RETURN    : 0: success, -1: error
***************************************************************************/
short rtcCheckDateTime( TM *pTm )
{
	unsigned char leap_year = IS_LEAP( pTm->tm_year );

	if( (pTm->tm_year < MIN_YEAR_RTC) || (pTm->tm_year > MAX_YEAR) )
		return -1;
	if( (pTm->tm_sec > 59) || (pTm->tm_min > 59) || (pTm->tm_hour > 23) ||
		(pTm->tm_mon < 1) || (pTm->tm_mon > 12) || (pTm->tm_mday < 1) )
		return -1;
	if( pTm->tm_wday > 6 )
		return -1;
	if( !leap_year && (pTm->tm_mday > DayMonth[pTm->tm_mon-1]) )
		return -1;
	if( leap_year )
	{
		// leap year
		if( (pTm->tm_mon == 2) && (pTm->tm_mday > 29) )
			return -1;
		else if( (pTm->tm_mon != 2) && (pTm->tm_mday > DayMonth[pTm->tm_mon-1]) )
			return -1;
	}

	return 0;
}

/***************************************************************************
* FUNZIONE  : unsigned long DateTime2Sec( TM *pTm )
* SCOPO     : Conversione di una data in numero di secondi
* ARGOMENTI : pTm - puntatore alla struttura data/ora
* RETURN    : Il numero di secondi dal 01/01/MIN_YEAR 00.00.00
***************************************************************************/
unsigned long DateTime2Sec( TM *pTm )
{
	unsigned long Nsec = 0;
	unsigned short k;

	if( pTm->tm_year > MIN_YEAR )
	{
		for( k=MIN_YEAR; k<pTm->tm_year; k++ )
		{
			Nsec += ys[IS_LEAP(k)];
		}
	}
	if( pTm->tm_mon > 1 )
	{
		for( k=1; k<pTm->tm_mon; k++ )
			Nsec += SecMonth[k-1];
		// check if leap year
		if( (pTm->tm_mon > 2) && IS_LEAP(pTm->tm_year) )
			Nsec += 86400L;
	}
	if( pTm->tm_mday > 1 )
	{
		for( k=1; k<pTm->tm_mday; k++ )
			Nsec += 86400L;
	}
	if( pTm->tm_hour > 0 )
	{
		for( k=0; k<pTm->tm_hour; k++ )
			Nsec += 3600;
	}
	if( pTm->tm_min > 0 )
	{
		for( k=0; k<pTm->tm_min; k++ )
			Nsec += 60;
	}
	Nsec += (unsigned long)pTm->tm_sec;

	return Nsec;
}

/***************************************************************************
* FUNZIONE  : void Sec2DateTime( unsigned long Nsec, TM *pTm )
* SCOPO     : Conversione del numero di secondi in una data
* ARGOMENTI : Nsec - il numero di secondi da 01/01/MIN_YEAR 00.00.00
*				  pTm - puntatore alla struttura data e ora
***************************************************************************/
void Sec2DateTime( unsigned long Nsec, TM *pTm )
{
	unsigned long nDay = 0;
	unsigned char localDayMonth[12];
	unsigned long localSecMonth[12];
	short leap;
	
	// start from 01/01/MIN_YEAR 00.00.00
	pTm->tm_wday = FIRST_WDAY;
	pTm->tm_mday = 1;
	pTm->tm_mon = 1;
	pTm->tm_year = MIN_YEAR;
	pTm->tm_hour = 0;
	pTm->tm_min = 0;
	pTm->tm_sec = 0;

	// copio in ram il numero di secondi nei vari mesi
	memcpy( localSecMonth, SecMonth, sizeof(SecMonth) );
	// copio in ram il numero di giorni nei vari mesi
	memcpy( localDayMonth, DayMonth, sizeof(DayMonth) );

	leap = IS_LEAP(pTm->tm_year);
	while( Nsec >= ys[leap] )
	{
		Nsec -= ys[leap];
		nDay += ds[leap];
		pTm->tm_year++;
		leap = IS_LEAP(pTm->tm_year);
	}

	// check if leap year
	if( leap )
	{
		// se bisestile, aggiungo i secondi del giorno 29 Febbraio
		localSecMonth[1] += 86400L;
		// se bisestile, aggiungo il giorno 29 Febbraio
		localDayMonth[1]++;
	}
	while( Nsec >= localSecMonth[pTm->tm_mon-1] )
	{
		Nsec -= localSecMonth[pTm->tm_mon-1];
		nDay += localDayMonth[pTm->tm_mon-1];
		pTm->tm_mon++;
	}
	
	// giorno della settimana al primo del mese
	nDay %= 7;
	nDay += pTm->tm_wday;
	pTm->tm_wday = nDay % 7;

	while( Nsec >= 86400L )
	{
		Nsec -= 86400L;
		pTm->tm_mday++;
		if( ++pTm->tm_wday > 6 )
			pTm->tm_wday = 0;
	}

	while( Nsec >= 3600 )
	{
		Nsec -= 3600;
		pTm->tm_hour++;
	}

	while( Nsec >= 60 )
	{
		Nsec -= 60;
		pTm->tm_min++;
	}

	pTm->tm_sec = (unsigned char)Nsec;
}

/***************************************************************************
* FUNZIONE  : void rtcStrMakeDate( char *s, TM *pTm )
* SCOPO     : Formatta una stringa data
* ARGOMENTI : s - stringa di destinazione data
*				  pTm - puntatore alla struttura data e ora
* RETURN    :
***************************************************************************/
void rtcStrMakeDate( char *s, TM *pTm )
{
	//sprintf( s, sDate_fmt,
	//			pTm->tm_mday, pTm->tm_mon, pTm->tm_year );
	
	s[0] = Hex2Ascii( pTm->tm_mday / 10 );
	s[1] = Hex2Ascii( pTm->tm_mday % 10 );
	s[2] = '/';
	s[3] = Hex2Ascii( pTm->tm_mon / 10 );
	s[4] = Hex2Ascii( pTm->tm_mon % 10 );
	s[5] = '/';
	itoa( pTm->tm_year, &s[6], 10 );
	
}

/***************************************************************************
* FUNZIONE  : void rtcStrMakeTime( char *s, TM *pTm )
* SCOPO     : Formatta una stringa ora
* ARGOMENTI : s - stringa di destinazione ora
*				  pTm - puntatore alla struttura data e ora
* RETURN    :
***************************************************************************/
void rtcStrMakeTime( char *s, TM *pTm )
{
	//sprintf( s, sTime_fmt,
	//			pTm->tm_hour, pTm->tm_min, pTm->tm_sec );
	
	s[0] = Hex2Ascii( pTm->tm_hour / 10 );
	s[1] = Hex2Ascii( pTm->tm_hour % 10 );
	s[2] = '.';
	s[3] = Hex2Ascii( pTm->tm_min / 10 );
	s[4] = Hex2Ascii( pTm->tm_min % 10 );
	s[5] = '.';
	s[6] = Hex2Ascii( pTm->tm_sec / 10 );
	s[7] = Hex2Ascii( pTm->tm_sec % 10 );
	s[8] = '\0';
	
}

/****************************************************************************
* FUNZIONE : void ScriviData( char *s )
****************************************************************************/
void ScriviData( char *s )
{
	rtcStrMakeDate( s, &DateTime );
}

/****************************************************************************
* FUNZIONE : void ScriviOra( char *s )
****************************************************************************/
void ScriviOra( char *s )
{
	rtcStrMakeTime( s, &DateTime );
}

/***************************************************************************
* FUNZIONE  : unsigned char rtcGetWeekDay( unsigned char day,
*												unsigned char month, unsigned short year )
* SCOPO     : Calcola il giorno della settimana corrispondente alla data indicata
* ARGOMENTI : day - giorno della data
*				  month - mese della data
*				  year - anno della data
* RETURN    : Il giorno della settimana corrispondente alla data indicata (0-6, 0=Sunday)
***************************************************************************/
unsigned char rtcGetWeekDay( unsigned char day, unsigned char month, unsigned short year )
{
	unsigned char wday;
	unsigned short a, y, m;
	
	a = ( 14 - (unsigned short)month ) / 12;
	y = year - a;
	m = (unsigned short)month + ( 12 * a ) - 2;
	wday = (unsigned char)( ( (unsigned short)day + y + ( y / 4 ) - ( y / 100 ) + ( y / 400 ) + ( ( 31 * m ) / 12 ) ) % 7 );
	
	return wday;
}

