/*!
** \file userResources.c
** \author Alessandro Vagniluca
** \date 07/12/2015
** \brief User Interface USER_STATE_RESOURCES state handler
** 
** \version 1.0
**/

#include "user.h"
#include "User_Parametri.h"
#include "userMenu.h"
#include "userResources.h"
#include "userInit.h"
#include "DecodInput.h"


#define TDEC_PULS_ON	(10*T_PULS_ON)	// (0.1s) minimo tempo pulsante premuto per power-on

enum
{
	USER_RES_WAIT_UPDATE,
	USER_RES_NO_RES,
};

/*!
** \def VBATT_X
** \brief Calculate the battery level X coordinate
** \param xs X start value (left: 0% battery level)
** \param xe X end value (right: 100% battery level)
** \param vb battery level (0.1%)
**/
#define VBATT_X( xs, xe, vb )	( ((xe - xs)*vb)/1000 + xs )


/*!
** \fn void GoToSleepOnBatteryCharge( void )
** \brief Go to sleep in the USER_STATE_BATTERY User Interface state handling
** \return The next User Interface state
**/
static void GoToSleepOnBatteryCharge( void )
{
	 // black screen
	SetBacklight( 0 );
	userVisShutDown( "", 0 );

	// switch FT801 from Active to Sleep mode (clocks disabled, all registers retained)
	//Play mute sound to avoid pop sound
	Ft_Gpu_Hal_Wr16( pHost, REG_SOUND, 0x0060 );
	Ft_Gpu_Hal_Wr8( pHost, REG_PLAY, 0x01 );
	Ft_Gpu_PowerModeSwitch( pHost, FT_GPU_SLEEP_M );

	MX_SPI1_Init( CLK_SLOW );

	Wdog();
	SleepOnBatteryCharge();
	Wdog();

	// switch FT801 from Sleep to Active mode
	Ft_Gpu_PowerModeSwitch( pHost, FT_GPU_ACTIVE_M );
	Ft_Gpu_Hal_Sleep(40);

	// Now FT800 can accept commands at up to 30MHz clock on SPI bus
	MX_SPI1_Init( CLK_FAST );
}

/*!
** \fn unsigned char userVisBattery( void )
** \brief Starts the USER_STATE_BATTERY User Interface state handling
** \return The next User Interface state 
**/
unsigned char userVisBattery( void )
{
	// full LCD backlight
	SetBacklight( LCD_BACKLIGHT_MAX );
	
	// flush the touch queue
	CTP_TagDeregisterAll();
	FlushTouchQueue();

	userSubState = 0;
	userCounterTime = TIMEOUT_AVVIO_10SEC;
	userNextPointer = NULL;
	
	return USER_STATE_BATTERY;
}

/*!
** \fn void VisVersion( char *s )
** \brief Print the SW version, the local current measures and a string.
** \param s The string to be printed
** \return None
**/
static void VisVersion( char *s )
{
	Ft_Gpu_CoCmd_Dlstart(pHost);
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
	
	// SW version
	Ft_Gpu_CoCmd_Text( pHost, 0, 0, 26, 0, sFW_VERSIONE );
	// graphic resources version expected
	Ft_Gpu_CoCmd_Text( pHost, 0, 16, 26, 0, sRES_VERSIONE );
	// ambient temperature
	mysprintf( str1, "Temp: %c%d.%02u C", 
                ((-100<TempAmb)&&(TempAmb<0))?'-':' ',
				(TempAmb/100),
				(abs(TempAmb)%100)
			);
	Ft_Gpu_CoCmd_Text( pHost, 0, 32, 26, 0, str1 );
	// battery voltage
	mysprintf( str1, "VBatt: %u.%03u V", (VBatt/1000), (VBatt%1000) );
	Ft_Gpu_CoCmd_Text( pHost, 0, 48, 26, 0, str1 );
	
	// string to be print
	Ft_Gpu_CoCmd_Text( pHost, 0, 80, 26, 0, s );
	
	Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
	Ft_Gpu_CoCmd_Swap( pHost );
	Ft_App_Flush_Co_Buffer( pHost );
	Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

	// force a measures update
	//inpRestart();
}

/*!
** \fn unsigned char userVisResources( void )
** \brief Starts the USER_STATE_RESOURCES User Interface state handling
** \return The next User Interface state 
**/
unsigned char userVisResources( void )
{
	// full LCD backlight
	SetBacklight( LCD_BACKLIGHT_MAX );
	
	VisVersion( "Check for resources..." );
	
	// flush the touch queue
	CTP_TagDeregisterAll();
	FlushTouchQueue();

	userSubState = USER_RES_WAIT_UPDATE;
	userCounterTime = TIMEOUT_AVVIO_3SEC;
	userNextPointer = NULL;
	
	return USER_STATE_RESOURCES;
}

/*!
** \fn unsigned char userResources( void )
** \brief Handler function for the USER_STATE_RESOURCES User Interface state
** \return The next User Interface state 
**/
unsigned char userResources( void )
{
	unsigned char RetVal = USER_STATE_RESOURCES;
	unsigned char prePuls = DIGIN_PULS;
	// wakeup da sola alimentazione esterna?
	int WakeupAlimOnly = (HAL_GPIO_ReadPin( PRES_RETE_GPIO_Port, PRES_RETE_Pin ) == GPIO_PIN_SET)
							&& (HAL_GPIO_ReadPin( PULS_GPIO_Port, PULS_Pin ) == GPIO_PIN_RESET);
	
	for( ; RetVal == USER_STATE_RESOURCES; Wdog() )
	{
		prePuls = DIGIN_PULS;

		Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
		
		if( Check4StatusChange() == USER_STATE_RESTART_OFF )
		{
			/*
			We cannot display the Restart/Off screen
			with the graphic resources unavailable.
			Power off in any case.
			*/
			userVisShutDown( "Power off...", 2000 );
			/*
			 * If no resources are present, enable bootloader on the next restart.
			 */
			if( userSubState == USER_RES_NO_RES )
			{
				ENABLE_BOOT = 1;
			}
			SystemOff();
			// ------> NON PASSA MAI DI QUI
		}
		
		/*
		L'azione utente sul pulsante annulla l'eventuale condizione di wake-up
		da sola alimentazione esterna e termina lo stato USER_STATE_RESOURCES,
		se le risorse grafiche sono presenti.
		*/
		if( prePuls && !DIGIN_PULS) // pushbutton release event )
		{
			WakeupAlimOnly = 0;
			userCounterTime = 1;
		}
		/*
		 * Se alimentazione assente, annulla l'eventuale condizione di wake-up
		 * da sola alimentazione esterna.
		 */
		else if( !DIGIN_ALIM )
		{
			WakeupAlimOnly = 0;
		}

		switch( userSubState )
		{
			case USER_RES_WAIT_UPDATE:
				// wait for resources update
				if( userCounterTime )
				{
					if( --userCounterTime == 0 )
					{
						// check resources
						if( res_Init() )
						{
							// push bitmap QUIT into Graphic RAM
							if( res_bm_Load( BITMAP_QUIT, res_bm_GetGRAM() ) == 0 )
							{
							#ifdef LOGGING
								myprintf( sBitmapLoadError, BITMAP_QUIT );
							#endif
							}
							// push bitmap OK into Graphic RAM
							if( res_bm_Load( BITMAP_OK, res_bm_GetGRAM() ) == 0 )
							{
							#ifdef LOGGING
								myprintf( sBitmapLoadError, BITMAP_OK );
							#endif
							}
							if( WakeupAlimOnly )
							{
								RetVal = userVisBattery();
							}
							else
							{
								RetVal = userVisInit();
							}
						}
						else
						{
							VisVersion( "ERROR: no resources." );
							userSubState = USER_RES_NO_RES;
							userCounterTime = TIMEOUT_AVVIO_3SEC;
						}
					}
					else //if( !(userCounterTime % (500/USER_TIME_TICK)) )
					{
						// update local measures
						VisVersion( "Check for resources..." );
					}
				}
			break;
			
			case USER_RES_NO_RES:
				// notify no resources 
				if( userCounterTime )
				{
					if( --userCounterTime == 0 )
					{
						userCounterTime = TIMEOUT_AVVIO_3SEC;
					}
					//if( !(userCounterTime % (500/USER_TIME_TICK)) )
					{
						// update local measures
						VisVersion( "ERROR: no resources." );
					}
				}
			break;
		}
		
	}
	
	return RetVal;
}

/*!
** \fn unsigned char userBattery( void )
** \brief Handler function for the USER_STATE_BATTERY User Interface state
** \return The next User Interface state 
**/
unsigned char userBattery( void )
{
	unsigned char RetVal = USER_STATE_BATTERY;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrBATTERY;
	BM_HEADER * p_bmhdrCHARGE;
	unsigned char key_cnt = 0;
	unsigned char loop = 1;
	//char s[DISPLAY_MAX_COL];
	unsigned char prePuls;
	unsigned short visPercVBatt;

	// push bitmap BATTERY_H_4X into Graphic RAM
	if( res_bm_Load( BITMAP_BATTERY_H_4X, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_BATTERY_H_4X );
	#endif
		return RetVal;
	}
	p_bmhdrBATTERY = res_bm_GetHeader( BITMAP_BATTERY_H_4X );
	
	// push bitmap CHARGE_H_4X into Graphic RAM
	if( res_bm_Load( BITMAP_CHARGE_H_4X, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_CHARGE_H_4X );
	#endif
		return RetVal;
	}
	p_bmhdrCHARGE = res_bm_GetHeader( BITMAP_CHARGE_H_4X );
	
	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// reset all tags
			CTP_TagDeregisterAll();
			userCounterTime = TIMEOUT_AVVIO_10SEC;
			// full LCD backlight
			SetBacklight( LCD_BACKLIGHT_MAX );

			// screen tag
			//TagRegister( 0, 0, 0, FT_DispWidth, FT_DispHeight );	no touch in questo stato
		}
		
		Ft_Gpu_CoCmd_Dlstart(pHost);
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE

		// show battery in charge
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrBATTERY->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrBATTERY->bmhdr.Format, p_bmhdrBATTERY->bmhdr.Stride, p_bmhdrBATTERY->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrBATTERY->bmhdr.Width, p_bmhdrBATTERY->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 128*16, 68*16 ) );
				
		// draw BATTERY level
		visPercVBatt = GetVisPercVBatt();
		//if( DIGIN_ALIM /*&& (BattUp == BATTERY_CHARGE)*/ )	// GREY color for charging battery
		{
			// charge battery level
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(207, 216, 220));
		}
		/* else if( visPercVBatt <= 100 )	// RED color for PercVBatt <= 10%
		{
			// alarm battery level
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(200,0,0));
		}
		else if( visPercVBatt <= 200 )	// ORANGE color for 10% < PercVBatt <= 20%
		{
			// warning battery level
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(200,128,0));
		}
		else	// GREEN color for 20% < PercVBatt <= 100%
		{
			// info battery level
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,200,0));
		} */
		Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(8 * 16) );
		Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (128+11)*16, (68+11)*16 ) );
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( VBATT_X( (128+11), (128+200), visPercVBatt )*16, (68+124)*16 ) );

		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		// draw BATTERY charge state
		//if( DIGIN_ALIM /*&& (BattUp == BATTERY_CHARGE)*/ )
		{
			// battery charging
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(255));
			Ft_App_WrCoCmd_Buffer(pHost, BLEND_FUNC( SRC_ALPHA, SRC_ALPHA ) );	// overlay on BATTERY_H_4X bitmap 224x160
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrCHARGE->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrCHARGE->bmhdr.Format, p_bmhdrCHARGE->bmhdr.Stride, p_bmhdrCHARGE->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrCHARGE->bmhdr.Width, p_bmhdrCHARGE->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (128+(224-90)/2)*16, (68+(135-54)/2)*16 ) );
			
			Ft_App_WrCoCmd_Buffer(pHost, BLEND_FUNC( SRC_ALPHA, ONE_MINUS_SRC_ALPHA ) );	// default blend
		}
		
		/* mysprintf( s, "%u%%", (visPercVBatt+5)/10 );
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, FT_DispHeight-40, FONT_DESC_DEF, OPT_CENTER, s ); */
		
		Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
		Ft_Gpu_CoCmd_Swap( pHost );
		Ft_App_Flush_Co_Buffer( pHost );
		Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
		
		prePuls = DIGIN_PULS;
		
		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			pt = NULL;
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
				tagval = pt->tag;
			}
		} while( event.status == osOK );
	
		// reset eventuale richiesta utente
		UserRequest = USER_REQ_NONE;

		/* if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;
		}
		else */ if(
					/*((pt != NULL) && (!pt->status)) // touch release event
					||*/ (prePuls && !DIGIN_PULS /*&& !PULS_LOCK*/) // pushbutton release event
				)
		{
			// flush the touch queue
			FlushTouchQueue();

			/*!
			 * SLEEP and wait wakeup from user or power-off
			 */
			GoToSleepOnBatteryCharge();

			// restart window handler
			RetVal = userVisBattery();
			loop = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;

			/*
			 * Se alimentazione assente, spegnimento.
			 */
			if( !DIGIN_ALIM )
			{
				// black screen
				SetBacklight( 0 );
				userVisShutDown( "", 0 );
				SystemOff();
				// ------> NON PASSA MAI DI QUI
			}

			/*
			 * Se pulsante premuto per 10s, avvio.
			 */
			if( DIGIN_PULS /*&& !PULS_LOCK*/)
			{
				// pulsante premuto
				userCounterTime = TIMEOUT_AVVIO_10SEC;
				// full LCD backlight
				SetBacklight( LCD_BACKLIGHT_MAX );
				if( ++key_cnt >= TDEC_PULS_ON )
				{
					// flush the touch queue
					FlushTouchQueue();

					// link waiting screen
					Ft_Gpu_CoCmd_Dlstart(pHost);
					Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
					Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,0xFF,0));	// GREEN
					Ft_Gpu_CoCmd_Spinner(pHost, (FT_DispWidth/2),(FT_DispHeight/2),0,1);
					Ft_App_Flush_Co_Buffer( pHost );
					Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

					// attesa rilascio pulsante
					for( ; prePuls || DIGIN_PULS; )
					{
						Wdog();
						prePuls = DIGIN_PULS;
						Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
					}

					// Send the stop command
					Ft_Gpu_Hal_WrCmd32(pHost, CMD_STOP);
					Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);
					// reset eventuale richiesta utente
					UserRequest = USER_REQ_NONE;
					// avvio
					RetVal = userVisInit();
					loop = 0;
				}
			}
			else
			{
				// pulsante non premuto
				key_cnt = 0;

				// LCD backlight
				if( --userCounterTime == 0 )
				{
					/*!
					 * SLEEP and wait wakeup from user or power-off
					 */
					GoToSleepOnBatteryCharge();

					// restart window handler
					RetVal = userVisBattery();
					loop = 0;
				}
				/* else if( userCounterTime <= TIMEOUT_AVVIO_2SEC )
				{
					SetBacklight( LCD_BACKLIGHT_MIN );
				} */
			}
		}
	}
	
	// pop bitmap CHARGE from Graphic RAM
	res_bm_Release( BITMAP_CHARGE_H_4X );
	// pop bitmap BATTERY from Graphic RAM
	res_bm_Release( BITMAP_BATTERY_H_4X );
	
	return RetVal;
}




