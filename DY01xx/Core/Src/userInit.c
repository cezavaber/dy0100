/*!
** \file userInit.c
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief User Interface USER_STATE_INIT state handler
** 
** \version 1.0
**/

#include "user.h"
#include "User_Parametri.h"
#include "userMenu.h"
#include "userInit.h"
#include "userReset.h"
#include "DecodInput.h"



/*!
** \fn unsigned char userVisInit( void )
** \brief Starts the USER_STATE_INIT User Interface state handling
** \return The next User Interface state 
**/
unsigned char userVisInit( void )
{
	// init OT/+ communication 
	OT_Init();
	OpeMode = MODE_FULL_POWER;

	// start viewing room temperature
	lByTogVisTH2O = 1;

	STATO_ALL_BLACK = PAN_RESET;

	STATO_STUFA = STUFASTAT_IN_RESET;
	TIPO_ALLARME = NO_ALARM;

	SBLOCCO_PAN = 0;

	TEMP_PAN = DEFAULTSETTEMP;
	FAN_PAN = FANAUTO;
	FAN_PAN_2 = FANAUTO;
	POWER_LEVEL = POTDEFAULT;

	MAX_POWER = POTMAXLEV;

	TEMP_AMB_MB = 255;

	Sleep.Ore = SLEEP_OFF;
	Sleep.Minuti = 0;

	H2O_TSET = H2O_DEFAULT;

	InitPassword();
	InitLogo();
	InitTipoDesc();
	TIPO_STUFA = NO_TYPE;
	OTdelay = 255;
	starttimer( TDISCONNECTED, 10*T1SEC );

	/* bRipetizione = 0;
	TargetFAN = TARGET_FAN_1;
	CommutaFan = 0; */

	userSubState = 0;
	userCounterTime = TIMEOUT_AVVIO_8SEC;
	userNextPointer = NULL;
	
	return USER_STATE_INIT;
}

/*!
** \fn unsigned char userInit( void )
** \brief Handler function for the USER_STATE_INIT User Interface state
** \return The next User Interface state 
**/
unsigned char userInit( void )
{
	unsigned char RetVal = USER_STATE_INIT;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	unsigned long key_cnt = 0;
	unsigned long timer;
	unsigned char loop = 1;
	unsigned long logoTimeout;
	
	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// reset all tags
			CTP_TagDeregisterAll();
			Flag.Key_Detect = 0;
			key_cnt = 0;
			// full LCD backlight
			SetBacklight( LCD_BACKLIGHT_MAX );
			starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
			OT_DL_Pausa = OT_DL_PAUSE;
			logoTimeout = TIMEOUT_UNLINKED;

			// screen tag
			TagRegister( 0, 0, 0, FT_DispWidth, FT_DispHeight );

			// link waiting screen
			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,0xFF,0));	// GREEN
			Ft_Gpu_CoCmd_Spinner(pHost, (FT_DispWidth/2),(FT_DispHeight/2),0,1);
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
		}
		
		// init window loop
		do
		{
			Wdog();
			ALIM_INP = DIGIN_ALIM;
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
				tagval = pt->tag;
			}
		} while( event.status == osOK );

		// connessione alimentazione esterna
		if( !ALIM_INP && DIGIN_ALIM )
		{
			// full LCD backlight
			SetBacklight( LCD_BACKLIGHT_MAX );
			starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
		}
	
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			// Send the stop command
			Ft_Gpu_Hal_WrCmd32(pHost, CMD_STOP);
			Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);
			loop = 0;
		}
		else if( /*LOGO_OK ||*/ userCollectParam() )
		{
			// logo and Tipo Stufa descriptions were received
			// Send the stop command
			Ft_Gpu_Hal_WrCmd32(pHost, CMD_STOP);
			Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);
			LoadFonts();	// load fonts related to the current language
			RetVal = userVisReset();
			loop = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;

			// LCD backlight
			if( StatSec.BackLightTime )
			{
				timer = readtimer( TUSER100 );
				if( timer == NOTIMER )
				{
					SetBacklight( LCD_BACKLIGHT_MIN );
				#if !defined(TX_ONLY) && !defined(RX_ONLY) && !defined(TXRX_ONLY)
					// comando la pausa lunga
					userStartOTLongPause();
				#endif
				}
				else if( timer >= USER_TIMEOUT_LIGHT_MID )
				{
					SetBacklight( LCD_BACKLIGHT_MID );
				}
			}
			
			if( logoTimeout )
			{
				if( --logoTimeout == 0 )
				{
					// logo and Tipo Stufa descriptions reception timeout
			#if !defined(TX_ONLY) && !defined(RX_ONLY) && !defined(TXRX_ONLY)
				#ifdef LOGGING
					myprintf( "Pausa in standby per timeout ricezione logo e tipi stufa\r\n" );
				#endif
					// comando la pausa lunga
					userStartOTLongPause();
			#endif
					/* if( OpeMode != MODE_STANDBY )
					{
					#ifdef LOGGING
						myprintf( "Pausa in standby per timeout ricezione logo e tipi stufa\r\n" );
					#endif
						// Send the stop command
						Ft_Gpu_Hal_WrCmd32(pHost, CMD_STOP);
						Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);
						RetVal = userVisPause();
						loop = 0;
					} */
				}
			}
		}
		else if( !pt->status )
		{
			// touch release event
			
			// full LCD backlight
			SetBacklight( LCD_BACKLIGHT_MAX );
			starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
			
			// flush the touch queue
			FlushTouchQueue();
			
			// Send the stop command
			Ft_Gpu_Hal_WrCmd32(pHost, CMD_STOP);
			Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);

			// local settings
			RetVal = EnterLocalSettings();
			//if( (RetVal = EnterLocalSettings() ) == USER_STATE_LOCAL_SETTINGS )
			{
				loop = 0;	// exit
			}
			// restart window
			tagval = TOUCH_TAG_INIT;
		}
	}
	
	return RetVal;
}

/*!
** \fn void InitPassword( void )
** \brief Password initialization
** \return None
**/
void InitPassword( void )
{
	pByPassLettera = PASS_LETTER_DEFAULT;
	pByPassCodice = PASS_CODE_DEFAULT;
}

/*!
** \fn void InitLogo( void )
** \brief Logo initialization
** \return None
**/
void InitLogo( void )
{
	memcpy(pChLogo , pChLogoDefault, LENGTH_LOGO);
	LOGO_OK = 0;
}

/*!
** \fn void InitLogo( void )
** \brief Tipo Stufa description strings initialization
** \return None
**/
void InitTipoDesc (void)
{
	BYTE i;

	memcpy( pChTipoDesc[NO_TYPE], sBlank, LENGTH_TYPEDESC );
	for( i=TYPE_1; i<NTYPE; i++ )
	{
		memcpy( pChTipoDesc[i], sBlank, LENGTH_TYPEDESC );
	}
	TYPES_OK = 0;

}

/*!
** \fn short userCollectParam( void )
** \brief Check for the reception of all the initial required parameters
** \return 0: recetion is running, 1: reception completed
**/
short userCollectParam( void )
{
	short RetVal = 0;
	BYTE i;
	char sTipo[LENGTH_TYPEDESC];

	// logo
	if( memcmp( pChLogo, pChLogoDefault, LENGTH_LOGO ) )
	{
		if( !LOGO_OK )
		{
		#ifdef LOGGING
			myprintf( "Received LOGO\r\n" );
		#endif
		}
		LOGO_OK = 1;
		// tipo stufa
		if( !(((TIPO_STUFA == NO_TYPE) || (TIPO_STUFA >= NTYPE)) && (STATO_ALL_BLACK != PAN_NO_TIPO)) )
		{
			// descrizione tipo
			memcpy( sTipo, sBlank, LENGTH_TYPEDESC );
			for( i=TYPE_1; i<NTYPE; i++ )
			{
				if( !memcmp( pChTipoDesc[i], sTipo, LENGTH_TYPEDESC ) )
				{
					break;
				}
			}
			if( i == NTYPE )
			{
				if( !TYPES_OK )
				{
				#ifdef LOGGING
					myprintf( "Received STOVE TYPES\r\n" );
				#endif
				}
				TYPES_OK = 1;
				RetVal = 1;		// ricezione completata
			}
		}
	}

	return RetVal;
}




