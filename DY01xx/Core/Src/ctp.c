/*!
** \file ctp.c
** \author Alessandro Vagniluca
** \date 07/07/2015
** \brief FT801 CTP interface
** 
** \version 1.0
**/

/* Includes ------------------------------------------------------------------*/
#include "user.h"
#include "DecodInput.h"

//#define CTP_INTERRUPT	// abilita la gestione a interrupt tramite FT801 NON GENERA INTERRUPT DI FINE CONVERSIONE TOUCH


/* #define CTP_I2C_ADDRESS			0x82	//!< 7-bit device address: 0x41

// i2c command for ilitek touch screen
#define ILITEK_TP_CMD_READ_DATA				0x10
#define ILITEK_TP_CMD_READ_SUB_DATA			0x11
#define ILITEK_TP_CMD_READ_DATA2			0x14
#define ILITEK_TP_CMD_GET_RESOLUTION		0x20
#define ILITEK_TP_CMD_SLEEP                 0x30
#define ILITEK_TP_CMD_GET_FIRMWARE_VERSION	0x40
#define ILITEK_TP_CMD_GET_PROTOCOL_VERSION	0x42

#define TOUCH_STATUS			0x80	// 1=touch, 0=un-touch
#define TOUCH_REPORT			0x40	// 0=info, 1=report
#define TOUCH_KEY				0x00
#define SCROLLING_BAR			0x01


// CTP device data
typedef struct dev_data
{
    // maximum x
    unsigned short max_x;
    // maximum y
    unsigned short max_y;
	// maximum touch point
    unsigned short max_tp;
	// maximum key button
    unsigned short max_btn;
    // the total number of x channel
    unsigned short x_ch;
    // the total number of y channel
    unsigned short y_ch;
    // the touch key number / the scrolling bar low value
    unsigned char tk_num;
    // the scrolling bar high value
    unsigned char sb_high;
	// protocol version
    unsigned char protocol_ver[2];
	// firmware version
	unsigned char FW_ver[3];

} DEV_DATA;
static DEV_DATA dev; */

#ifdef CTP_INTERRUPT
static EventGroupHandle_t TouchEventGroup;
#endif
#define TOUCH_EVENT			0x0001	//!< received touch event

typedef struct
{
	unsigned char Reported_ID;
	unsigned char Status;	//!< 1=touch, 0=un-touch
	unsigned short x;
	unsigned short y;
} TOUCHPOINT;
//static TOUCHPOINT TouchPoint[10];
//static unsigned short TouchPointIndex;
static TOUCHPOINT TouchPoint;

/*typedef struct
{
	unsigned char Reported_ID;
	unsigned char Status;	//!< 1=touch, 0=un-touch
	unsigned char TouchKey_ID;
} TOUCHKEY;
static TOUCHKEY TouchKey[10];
static unsigned short TouchKeyIndex;

typedef struct
{
	unsigned char Reported_ID;
	unsigned char Status;	//!< 1=touch, 0=un-touch
	unsigned char ScrollingBar_ID;
	unsigned short Position;
} SCROLLBAR;
static SCROLLBAR ScrollBar[10];
static unsigned short ScrollBarIndex;*/

/*!
	\typedef TAG_ITEM
	\brief Touch tag buffers item of a linked list
*/
typedef struct tag_item
{
	TAG_STRUCT TagBuf;
	struct tag_item *next;
} TAG_ITEM;
static struct tag_item *head = NULL;
static struct tag_item *curr = NULL;


static struct tag_item* create_list( TAG_STRUCT *pTag );
static struct tag_item* add_to_list_end( TAG_STRUCT *pTag );
static struct tag_item* search_in_list( unsigned char tag, struct tag_item **prev );
static short delete_from_list( unsigned char tag );
static void delete_list( void );




/******************************************************************************
LINKED LIST HANDLING FUNCTIONS
*******************************************************************************/

// create the first item of the linked list
static struct tag_item* create_list( TAG_STRUCT *pTag )
{
	struct tag_item *ptr = (struct tag_item *)malloc(sizeof(struct tag_item));
	
	if( NULL != ptr )
	{
		ptr->TagBuf = *pTag;
		ptr->next = NULL;

		head = curr = ptr;
	}
	
	return ptr;
}

// append an item to the linked list
static struct tag_item* add_to_list_end( TAG_STRUCT *pTag )
{
	struct tag_item *ptr = NULL;
	
	if( NULL == head )
	{
		// cretae the linked list with the item
		ptr = create_list(pTag);
	}
	else
	{
		// create the new item
		ptr = (struct tag_item *)malloc(sizeof(struct tag_item));
		if( NULL != ptr )
		{
			ptr->TagBuf = *pTag;
			ptr->next = NULL;
		
			// add the item to the linked list end
			curr->next = ptr;
			curr = ptr;
		}
	}
	
	return ptr;
}

// search an item and the previous one in the linked list
// return NULL if not found
static struct tag_item* search_in_list( unsigned char tag, struct tag_item **prev )
{
	struct tag_item *ptr = head;
	struct tag_item *tmp = NULL;
	unsigned char found = 0;
	
	// search the item
	while( ptr != NULL )
	{
		if( ptr->TagBuf.tag == tag )
		{
			// found
			found = 1;
			break;
		}
		else
		{
			// save the current item and continue to the next item
			tmp = ptr;
			ptr = ptr->next;
		}
	}
	
	if( found )
	{
		// 'ptr' is the searched item
		if( prev )
		{
			// pass also the previous item
			*prev = tmp;
		}
	}
	else
	{
		ptr = NULL;
	}
	
	return ptr;
}

// delete an item from the linked list
// return: 1: OK, 0: FAILED
static short delete_from_list( unsigned char tag )
{
	short RetVal = 0;
	struct tag_item *prev = NULL;
	struct tag_item *del = NULL;
	
	del = search_in_list( tag, &prev );
	if( del != NULL )
	{
		if( prev != NULL )	// link the previous to the next
		{
			prev->next = del->next;
		}

		if( del == curr )	// is it the last item?
		{ 
			curr = prev;
		}
 		/* --else--
 		 * fix-bug cancellazione linked-list 20180529 - A. BERTINI
 		 */
		if( del == head )	// is it the first item?
		{
			head = del->next;
		}

		free( del );
		
		RetVal = 1;
	}
	
	return RetVal;
}

// delete the entire linked list
static void delete_list( void )
{
	struct tag_item *next;
	
	// delete the linked list from the beginning
	while( head != NULL )
	{
		next = head->next;
		free( head );
		head = next;
	}
}




/*!
** \fn int CTP_Init( void )
** \brief CTP interface initialization
** \return 1: OK, 0: FAILED
** \note I2C peripheral is supposed already properly initialized
**/
int CTP_Init( void )
{
	int RetVal = 1;
	// unsigned char data[16];

	/*
#ifdef LOGGING
	myprintf( "CTP reset... " );
#endif
	// reset touch controller
	HAL_GPIO_WritePin(CTP_RST_PORT, CTP_RST, GPIO_PIN_RESET);
	osDelay( 2 );	//HAL_Delay( 2 );
	HAL_GPIO_WritePin(CTP_RST_PORT, CTP_RST, GPIO_PIN_SET);
	osDelay( 120 );	//HAL_Delay( 120 );	// Wait at least 100ms
#ifdef LOGGING
	myprintf( "OK\r\n" );
#endif
	*/

	/*
#ifdef LOGGING
	myprintf( "CTP parameters:\r\n" );
#endif
	// read firmware version
	if(
		HAL_I2C_Mem_Read(
						&hi2c1,
						CTP_I2C_ADDRESS,
						ILITEK_TP_CMD_GET_FIRMWARE_VERSION,
						I2C_MEMADD_SIZE_8BIT,
						dev.FW_ver,
						3,
						1000
						) == HAL_OK
		)
	{
	#ifdef LOGGING
		myprintf( "\tFW_Ver: %u.%u.%u\r\n", dev.FW_ver[0], dev.FW_ver[1], dev.FW_ver[2] );
	#endif
		// read protocol version
		if(
			HAL_I2C_Mem_Read(
						&hi2c1,
						CTP_I2C_ADDRESS,
						ILITEK_TP_CMD_GET_PROTOCOL_VERSION,
						I2C_MEMADD_SIZE_8BIT,
						dev.protocol_ver,
						2,
						1000
						) == HAL_OK
			)
		{
		#ifdef LOGGING
			myprintf( "\tPROT_Ver: %u.%u\r\n", dev.protocol_ver[0], dev.protocol_ver[1] );
		#endif
		    // read touch resolution
			if(
				HAL_I2C_Mem_Read(
							&hi2c1,
							CTP_I2C_ADDRESS,
							ILITEK_TP_CMD_GET_RESOLUTION,
							I2C_MEMADD_SIZE_8BIT,
							data,
							10,
							1000
							) == HAL_OK
				)
			{
				// maximum touch point
				dev.max_tp = data[6];
			#ifdef LOGGING
				myprintf( "\tMax touch points: %u\r\n", dev.max_tp );
			#endif
				// maximum button number
				dev.max_btn = data[7];
			#ifdef LOGGING
				myprintf( "\tMax touch buttons: %u\r\n", dev.max_btn );
			#endif

				// calculate the resolution for x and y direction
				dev.max_x = MAKEWORD( data[0], data[1] );
			#ifdef LOGGING
				myprintf( "\tMax X: %u\r\n", dev.max_x );
			#endif
				dev.max_y = MAKEWORD( data[2], data[3] );
			#ifdef LOGGING
				myprintf( "\tMax Y: %u\r\n", dev.max_y );
			#endif
				dev.x_ch = data[4];
			#ifdef LOGGING
				myprintf( "\tNum X chan: %u\r\n", dev.x_ch );
			#endif
				dev.y_ch = data[5];
			#ifdef LOGGING
				myprintf( "\tNum Y chan: %u\r\n", dev.y_ch );
			#endif

				dev.tk_num = data[8];
			#ifdef LOGGING
				myprintf( "\tNum touch key: %u\r\n", dev.tk_num );
			#endif
				dev.sb_high = data[9];
			#ifdef LOGGING
				myprintf( "\tScroll high: %u\r\n", dev.sb_high );
			#endif

				RetVal = 1;
			}
		}
	} */

	//volatile unsigned char b[4];	// DEBUG
	
	Ft_Gpu_Hal_Wr8(pHost, REG_CTOUCH_EXTENDED, CTOUCH_MODE_COMPATABILITY);
	//Ft_Gpu_Hal_Wr8(pHost, REG_CTOUCH_EXTENDED, CTOUCH_MODE_EXTENDED);
	Ft_Gpu_Hal_Wr8(pHost, REG_TOUCH_MODE, TOUCHMODE_CONTINUOUS);	// continuous touch acquisitions
#ifdef CTP_INTERRUPT
	Ft_Gpu_Hal_Wr8(pHost, REG_INT_MASK, INT_CONVCOMPLETE);	// enable touch conversion completed interrupt source
	Ft_Gpu_Hal_Wr8(pHost, REG_INT_EN, 1);	// enable FT800 interrupts
#endif
	
	/* b[0] = Ft_Gpu_Hal_Rd8(pHost, REG_CTOUCH_EXTENDED);
	b[1] = Ft_Gpu_Hal_Rd8(pHost, REG_TOUCH_MODE);
	b[2] = Ft_Gpu_Hal_Rd8(pHost, REG_INT_MASK);
	b[3] = Ft_Gpu_Hal_Rd8(pHost, REG_INT_EN); */

	return RetVal;
}

/**
  * @brief  EXTI line detection callbacks.
  * @param  GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

#ifdef CTP_INTERRUPT
	if(GPIO_Pin == CTP_INT)
	{
		/* Set bit TOUCH_EVENT in TouchEventGroup. */
		xEventGroupSetBitsFromISR(
						TouchEventGroup,    /* The event group being updated. */
						TOUCH_EVENT,   /* The bits being set. */
						&xHigherPriorityTaskWoken
						);

		/* If xHigherPriorityTaskWoken is now set to pdTRUE then a context
		switch should be requested. */
		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	}
#endif

	if( GPIO_Pin == PULS_Pin )
	{
		// pushbutton interrupt
		PULS_IRQ = 1;
	}
	else if( GPIO_Pin == INT1_ACC_Pin )
	{
		// INT1_ACC interrupt
		//INT1_Acc_irq();
		ACC_IRQ = 1;
	}
	else if( GPIO_Pin == INT2_ACC_Pin )
	{
		// INT2_ACC interrupt
		//INT2_Acc_irq();
		ACC_IRQ = 1;
	}
}

/*!
** \fn void CTP_Task( void const * argument )
** \brief CTP interface task
**/
void CTP_Task( void const * argument )
{	
	EventBits_t uxBits;
	// unsigned char data[16];
	// unsigned char nTouchPoints;
	short i,j;
	// unsigned char mult_tp_id;
	struct tag_item *ptr = NULL;
	//struct tag_item *prev = NULL;
	ft_uint32_t ReadWord;
	ft_int16_t xvalue[5],yvalue[5];
	
	// disable touch interrupt
	HAL_NVIC_DisableIRQ(CTP_EXTI_IRQn);

#ifdef CTP_INTERRUPT
	/* create the touch event group. */
	TouchEventGroup = xEventGroupCreate();
#endif

	/* definition and creation of TouchQueue */
	osMessageQDef(TouchQueue, TOUCH_QUEUE_SIZE, sizeof(TAG_STRUCT *));
	TouchQueueHandle = osMessageCreate(osMessageQ(TouchQueue), NULL);

	//CTP_Init(); se eseguita qui, I2C va in tilt!!!

	/*
	memset( &TouchPoint, 0, sizeof(TouchPoint) );
	//memset( TouchPoint, 0, sizeof(TouchPoint) );

	// read for reset touch reported
#ifdef LOGGING
	myprintf( "CTP clear touch reported " );
#endif
	if(
		HAL_I2C_Mem_Read(
						&hi2c1,
						CTP_I2C_ADDRESS,
						ILITEK_TP_CMD_READ_DATA,
						I2C_MEMADD_SIZE_8BIT,
						data,
						1,
						1000
						) == HAL_OK
		)
	{
		nTouchPoints = data[0];
	#ifdef LOGGING
		myprintf( "[%u] ", nTouchPoints );
	#endif
		// read touch point
		for( i=0; i<nTouchPoints; i++ )
		{
			if( HAL_I2C_Mem_Read(
									&hi2c1,
									CTP_I2C_ADDRESS,
									ILITEK_TP_CMD_READ_SUB_DATA,
									I2C_MEMADD_SIZE_8BIT,
									data,
									5,
									1000
									) == HAL_OK
				)
			{
			#ifdef LOGGING
				myprintf( "." );
			#endif
			}
			else
			{
				break;
			}
		}
	}
#ifdef LOGGING
	myprintf( "\r\n" );
#endif
	*/

#ifdef CTP_INTERRUPT
	// enable touch interrupt
	__HAL_GPIO_EXTI_CLEAR_IT(CTP_INT);
	HAL_NVIC_EnableIRQ(CTP_EXTI_IRQn);
#endif

	memset( &TouchPoint, 0, sizeof(TouchPoint) );
	TouchPoint.x = -32768;
	TouchPoint.y = -32768;
	/*memset( TouchPoint, 0, sizeof(TouchPoint) );
	TouchPointIndex = 0;
	memset( TouchKey, 0, sizeof(TouchKey) );
	TouchKeyIndex = 0;
	memset( ScrollBar, 0, sizeof(ScrollBar) );
	ScrollBarIndex = 0;*/

	for( ; ; Wdog() )
	{
#ifdef CTP_INTERRUPT
		/* Wait a maximum of 100ms for bit TOUCH_EVENT to be set within
		the event group.  Clear the bits before exiting. */
		uxBits = xEventGroupWaitBits(
							TouchEventGroup,   /* The event group being tested. */
							TOUCH_EVENT, /* The bits within the event group to wait for. */
							pdTRUE,        /* The bits should be cleared before returning. */
							pdFALSE,       /* Don't wait for all the bits, either bit will do. */
							(100/portTICK_PERIOD_MS)	/* Wait a maximum of 100ms for either bit to be set. */
							);

		if( uxBits & TOUCH_EVENT )
#else
		// polling touch conversions
		Ft_Gpu_Hal_Sleep(10);
		uxBits = 0;
#endif
		{
			// process the touch event
			/* if(
				HAL_I2C_Mem_Read(
								&hi2c1,
								CTP_I2C_ADDRESS,
								ILITEK_TP_CMD_READ_DATA,
								I2C_MEMADD_SIZE_8BIT,
								data,
								1,
								1000
								) == HAL_OK
				) */
			{
				/* nTouchPoints = data[0];
				// read touch point
				for( i=0; i<nTouchPoints; i++ ) */
				{
					// parse point
					/* if(
						HAL_I2C_Mem_Read(
										&hi2c1,
										CTP_I2C_ADDRESS,
										ILITEK_TP_CMD_READ_SUB_DATA,
										I2C_MEMADD_SIZE_8BIT,
										data,
										5,
										1000
										) == HAL_OK
						) */
					{
						/* mult_tp_id = data[0];
						if( mult_tp_id & TOUCH_REPORT ) */
						{
							// touch report
							// if( data[2] == SCROLLING_BAR )
							{
								// scrolling bar report
								/*ScrollBar[ScrollBarIndex].Reported_ID = mult_tp_id & 0x3F;
								ScrollBar[ScrollBarIndex].Status = (mult_tp_id & TOUCH_STATUS)? 1 : 0;
								ScrollBar[ScrollBarIndex].ScrollingBar_ID = data[1];
								ScrollBar[ScrollBarIndex].Position = MAKEWORD( data[3], data[4] );
								if( ++ScrollBarIndex > 10)
								{
									ScrollBarIndex = 0;
								}*/
							}
							// else
							{
								// touch key report
								/*TouchKey[TouchKeyIndex].Reported_ID = mult_tp_id & 0x3F;
								TouchKey[TouchKeyIndex].Status = (mult_tp_id & TOUCH_STATUS)? 1 : 0;
								TouchKey[TouchKeyIndex].TouchKey_ID = data[1];
								if( ++TouchKeyIndex > 10)
								{
									TouchKeyIndex = 0;
								}*/
							}
						}
						// else
						{
							// touch info
							/*TouchPoint[TouchPointIndex].Reported_ID = mult_tp_id & 0x3F;
							TouchPoint[TouchPointIndex].Status = (mult_tp_id & TOUCH_STATUS)? 1 : 0;
							TouchPoint[TouchPointIndex].x = MAKEWORD( data[2], data[1] );
							TouchPoint[TouchPointIndex].y = MAKEWORD( data[4], data[3] );
							if( ++TouchPointIndex > 10)
							{
								TouchPointIndex = 0;
							}*/
							// TouchPoint.Reported_ID = mult_tp_id & 0x3F;
							// TouchPoint.Status = (mult_tp_id & TOUCH_STATUS)? 1 : 0;
							// TouchPoint.x = MAKEWORD( data[2], data[1] );
							// TouchPoint.y = MAKEWORD( data[4], data[3] );
							
							ReadWord = Ft_Gpu_Hal_Rd32(pHost, REG_CTOUCH_TOUCH0_XY);
							yvalue[0] = LOWORD( ReadWord );
							xvalue[0] = HIWORD( ReadWord );
							/* ReadWord = Ft_Gpu_Hal_Rd32(pHost, REG_CTOUCH_TOUCH1_XY);
							yvalue[1] = LOWORD( ReadWord );
							xvalue[1] = HIWORD( ReadWord );
							ReadWord = Ft_Gpu_Hal_Rd32(pHost, REG_CTOUCH_TOUCH2_XY);
							yvalue[2] = LOWORD( ReadWord );
							xvalue[2] = HIWORD( ReadWord );
							ReadWord = Ft_Gpu_Hal_Rd32(pHost, REG_CTOUCH_TOUCH3_XY);
							yvalue[3] = LOWORD( ReadWord );
							xvalue[3] = HIWORD( ReadWord );
							xvalue[4] = Ft_Gpu_Hal_Rd16(pHost, REG_CTOUCH_TOUCH4_X);
							yvalue[4] = Ft_Gpu_Hal_Rd16(pHost, REG_CTOUCH_TOUCH4_Y); */
							
							if( (xvalue[0] == -32768) && (yvalue[0] == -32768) )
							{
								// untouch
								if( TouchPoint.Status )
								{
									// untouch event on the last (x,y) point
									uxBits |= TOUCH_EVENT;
								}
								TouchPoint.Status = 0;
							}
							else if( (xvalue[0] >= 0) && (yvalue[0] >= 0) )
							{
								// touch event
								uxBits |= TOUCH_EVENT;
								TouchPoint.Status = 1;
								TouchPoint.x = xvalue[0];
								TouchPoint.y = yvalue[0];
							#ifdef LCD_INVERTED_LANDSCAPE
								// LCD 180� rotated
								/*
								 * Commentato per calibrazione touch effettuata su LCD gi� ruotato
								 */
								//TouchPoint.x = FT_DispWidth - 1 - TouchPoint.x;
								//TouchPoint.y = FT_DispHeight - 1 - TouchPoint.y;
							#endif
							}

							if( uxBits & TOUCH_EVENT )
							{
								// check for the registered tags
								for( ptr=head; ptr!=NULL; ptr=ptr->next )
								{
									if(
										ptr->TagBuf.enabled &&
										(ptr->TagBuf.xs <= TouchPoint.x) && (TouchPoint.x <= ptr->TagBuf.xe) &&
										(ptr->TagBuf.ys <= TouchPoint.y) && (TouchPoint.y <= ptr->TagBuf.ye)
										)
									{
										// found a registered enabled tag for the touch event
										j = 1;	// enable the touch event notification
										if( TouchPoint.Status && !ptr->TagBuf.status )
										{
											// first touch event for the registered enabled tag
											ptr->TagBuf.Dx = 0;
											ptr->TagBuf.Dy = 0;
											j = 0;	// disable the touch event notification
										}
										else if( TouchPoint.Status && ptr->TagBuf.status )
										{
											// the registered enabled tag keeps to be touched
											if( ptr->TagBuf.tag >= TAG_VSCROLL_BASE )
											{
												// touch event for a vertical scrolling tag
												ptr->TagBuf.Dy = ptr->TagBuf.ys - TouchPoint.y;	// the tracking value
												ptr->TagBuf.Dx = abs( (int)TouchPoint.x - (int)ptr->TagBuf.x );
												TouchPoint.x = ptr->TagBuf.x;	// keep the X reference
												if(
													(ptr->TagBuf.xs == 0) &&
													(ptr->TagBuf.xe == FT_DispWidth-1) &&
													(ptr->TagBuf.Dx > TAG_SCROLL_MAX_DELTA)
													)
												{
													// it is no more a vertical screen scrolling,
													// abort scroll tracking
													TouchPoint.Status = 0;
												}
											}
											else if( ptr->TagBuf.tag >= TAG_HSCROLL_BASE )
											{
												// touch event for a horizontal scrolling tag
												ptr->TagBuf.Dx = ptr->TagBuf.xs - TouchPoint.x;	// the tracking value
												ptr->TagBuf.Dy = abs( (int)TouchPoint.y - (int)ptr->TagBuf.y );
												TouchPoint.y = ptr->TagBuf.y;	// keep the Y reference
												if(
													(ptr->TagBuf.ys == 0) &&
													(ptr->TagBuf.ye == FT_DispHeight-1) &&
													(ptr->TagBuf.Dy > TAG_SCROLL_MAX_DELTA)
													)
												{
													// it is no more a horizontal screen scrolling,
													// abort scroll tracking
													TouchPoint.Status = 0;
												}
											}
											else
											{
												// touch event for a key tag
												j = 1;	// enable the touch event notification
											}
										}
										else if( !TouchPoint.Status && ptr->TagBuf.status )
										{
											// the registered enabled tag is released
											j = 1;	// enable the touch event notification
										}

										// tag status update
										ptr->TagBuf.status = TouchPoint.Status;
										ptr->TagBuf.x = TouchPoint.x;
										ptr->TagBuf.y = TouchPoint.y;

										// notify the touch event for the registered enabled tag
										if( j )
										{
											osMessagePut( TouchQueueHandle, (uint32_t)&ptr->TagBuf, 0 );
											//break;
										}
									}
								}

								/*// check for the registered tags in touched state and no more touched (single-touch)
								for( ptr=head; ptr!=NULL; ptr=ptr->next )
								{
									if(
										ptr->TagBuf.status &&
										(
											(ptr->TagBuf.xs > TouchPoint.x) || (TouchPoint.x > ptr->TagBuf.xe) ||
											(ptr->TagBuf.ys > TouchPoint.y) || (TouchPoint.y > ptr->TagBuf.ye)
										)
										)
									{
										ptr->TagBuf.status = 0;
										osMessagePut( TouchQueueHandle, (uint32_t)&ptr->TagBuf, 0 );
									}
								}*/
							}
						}
					}
					/* else
					{
						mult_tp_id = 0;
					} */
				}

				/* if( !nTouchPoints )
				{
					mult_tp_id = 0;
				} */
			}
		}
	}
}

/*!
** \fn CTP_TagRegister( TAG_STRUCT *pTag )
** \brief Register a touch tag
** \param pTag The tag structure pointer
** \return 1: OK, 0: FAILED
**/
short CTP_TagRegister( TAG_STRUCT *pTag )
{
	short RetVal = 0;
	struct tag_item *ptr = add_to_list_end( pTag );
	
	if( ptr != NULL )
	{
		ptr->TagBuf.status = 0;	// start with an untouched state
		ptr->TagBuf.x = 0;
		ptr->TagBuf.y = 0;
		ptr->TagBuf.Dx = 0;
		ptr->TagBuf.Dy = 0;
		RetVal = 1;
	}
	
	return RetVal;
}

/*!
** \fn CTP_TagDeregister( unsigned char tag )
** \brief Deregister a touch tag
** \param tag The tag to be deregistered
** \return 1: OK, 0: FAILED
**/
short CTP_TagDeregister( unsigned char tag )
{
	short RetVal = 0;
	
	if( delete_from_list( tag ) )
	{
		RetVal = 1;
	}
	
	return RetVal;
}

/*!
** \fn void CTP_TagDeregisterAll( void )
** \brief Deregister all the current touch tags
**/
void CTP_TagDeregisterAll( void )
{
	delete_list();
}

/*!
** \fn CTP_SetTag( unsigned char tag, unsigned char mode )
** \brief Set the mode of a registered touch tag
** \param tag The tag to be set
** \param mode Enable (1) or disable (0) the tag.
** \return 1: OK, 0: FAILED
**/
short CTP_SetTag( unsigned char tag, unsigned char mode )
{
	short RetVal = 0;
	struct tag_item *ptr = NULL;
	struct tag_item *prev = NULL;
	
	ptr = search_in_list( tag, &prev );
	if( ptr != NULL )
	{
		// set the entry mode
		if( ptr->TagBuf.enabled != mode )
		{
			ptr->TagBuf.status = 0;	// default: untouched
			ptr->TagBuf.enabled = mode;
		}
		RetVal = 1;
	}
	
	return RetVal;
}

