/*!
	\file OT_Plus.c
	\brief Modulo OT_Plus.c

	Gestione della comunicazione MASTER su canale OT/+ .

*/

#include "main.h"
#include "timebase.h"
#include "nvram.h"
#include "rtc.h"
#include "OT_Plus.h"
#include "userAllarmi.h"



/* defines */

#define T_ANTI_DEAD_LOCK	(20*T1SEC)	// 20s: tempo massimo di attesa senza risposta dal basso livello
#define TMAX_TEMP_AMB		(8*T1MIN)	// 8min: intervallo massimo tra due invii della temperatura ambiente
							/* TMAX_TEMP_AMB � usato anche per la richiesta del
								codice ISO3166-1 alpha 2 del Paese locale. */
#define TMAX_OT_UNLINKED	(30*T1SEC)		// 30s: in OT_STAT_LINKED massimo tempo di assenza risposte prima di passare in OT_STAT_UNLINKED
#define TMAX_CONSUMO		(T1MIN)	// 1min intervallo massimo tra due richieste di consumo pellet

/* variabili inizializzate */



/* variabili non inizializzate */

/*!
	\var OT_Stat
	\brief Stato gestore comunicazione
*/
OT_STAT OT_Stat;
OT_STAT OT_NextStat;	// stato gestore in uscita dalla pausa standby


/*!
	\var OT_LimitStep_Requested
	\brief Bitmap di flag limiti/step richiesti per i Holding Registers
*/
unsigned char OT_LimitStep_Requested[LIMSTEP_REQ_BMAP_SIZE];

unsigned char iLimReq;	//!< indice richiesta Limiti/Step
short PreTempAmbPAN;	//!< valore precedente temperatura ambiente in 0.1�C

/*!
	\var OT_Result
	\brief Esito richiesta
*/
OT_RESULT OT_Result;

static unsigned short cntErrOneShot;	//!< contatore errori su invio unico
static unsigned short cntErrCycle;	//!< contatore errori su invio ciclico


static PKT_STRUCT saveReq;	//!< richiesta di cui si vuole l'esito dell'invio


/* costanti */

/*!
	\var reqXXX
	\brief Richieste Fisse in formato OTPKT_STRUCT corrispondente a PKT_STRUCT
*/
const REQ_STATO reqVerFW =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_INPUT_REGISTER, 0x00, 0x00, 0x00, 0x08 }		//!< packet buffer
};
const REQ_STATO reqLinguaDef =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_INPUT_REGISTER, 0x00, 0x6D, 0x00, 0x01 }		//!< packet buffer
};
const REQ_STATO reqTipoStufa =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_INPUT_REGISTER, 0x00, 0x6C, 0x00, 0x01 }		//!< packet buffer
};
const REQ_STATO reqCoils =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_COILS, 0x00, 0x00, 0x00, 0x02 }		//!< packet buffer
};
const REQ_STATO reqInputStato_Misure_1 =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_INPUT_REGISTER, 0x00, 0x0F, 0x00, 0x0B }		//!< packet buffer
};
const REQ_STATO reqInputStato_Misure_2 =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_INPUT_REGISTER, 0x00, 0x25, 0x00, 0x05 }		//!< packet buffer
};
const REQ_STATO reqPassword =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_INPUT_REGISTER, 0x00, 0x3E, 0x00, 0x02 }		//!< packet buffer
};
const REQ_STATO reqLogo =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_INPUT_REGISTER, 0x00, 0x44, 0x00, 0x08 }		//!< packet buffer
};
const REQ_STATO reqTipoDesc_1_2 =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_INPUT_REGISTER, 0x00, 0x4C, 0x00, 0x08 }		//!< packet buffer
};
const REQ_STATO reqTipoDesc_3_4 =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_INPUT_REGISTER, 0x00, 0x54, 0x00, 0x08 }		//!< packet buffer
};
const REQ_STATO reqTipoDesc_5_6 =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_INPUT_REGISTER, 0x00, 0x5C, 0x00, 0x08 }		//!< packet buffer
};
const REQ_STATO reqTipoDesc_7_8 =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_INPUT_REGISTER, 0x00, 0x64, 0x00, 0x08 }		//!< packet buffer
};
const REQ_STATO reqEcoBmap_NFan =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_HOLDING_REGISTER, 0x00, 0x06, 0x00, 0x06 }		//!< packet buffer
};
const REQ_STATO reqTermAmbBmap =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_HOLDING_REGISTER, 0x01, 0x31, 0x00, 0x07 }		//!< packet buffer
};
const REQ_STATO reqAccumBmap =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_HOLDING_REGISTER, 0x00, 0x17, 0x00, 0x01 }		//!< packet buffer
};
const REQ_STATO reqFireFan =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_HOLDING_REGISTER, 0x04, 0x30, 0x00, 0x04 }		//!< packet buffer
};
const REQ_LIMITS reqLimitsFireFan =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_LIMITS, 0x04, 0x30, 0x00, 0x04 }		//!< packet buffer
};
const REQ_STATO reqSoundAlarmBmap =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_HOLDING_REGISTER, 0x01, 0x1E, 0x00, 0x01 }		//!< packet buffer
};
const REQ_STATO reqWiFiBmap =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_HOLDING_REGISTER, 0x20, 0x02, 0x00, 0x01 }		//!< packet buffer
};
const REQ_STATO reqMaxPower =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_HOLDING_REGISTER, 0x01, 0x0D, 0x00, 0x01 }		//!< packet buffer
};
const REQ_STATO reqConsumo =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_INPUT_REGISTER, 0x00, 0x34, 0x00, 0x01 }		//!< packet buffer
};
const REQ_STATO reqReleTermBmap =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_HOLDING_REGISTER, 0x20, 0x7B, 0x00, 0x02 }		//!< packet buffer
};
const REQ_STATO reqTempPx =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_HOLDING_REGISTER, 0x20, 0x7D, 0x00, 0x05 }		//!< packet buffer
};
const REQ_STATO reqConfigIdro =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_HOLDING_REGISTER, 0x01, 0x17, 0x00, 0x01 }		//!< packet buffer
};
const REQ_STATO reqISOCode =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_HOLDING_REGISTER, 0x20, 0x56, 0x00, 0x01 }		//!< packet buffer
};
const REQ_STATO reqIPaddr =
{
	7,			//!< packet length
	{ RFID_DEF, MB_ADDR, MB_FUNC_READ_HOLDING_REGISTER, 0x20, 0x04, 0x00, 0x08 }		//!< packet buffer
};



/*!
	\var reqSet_XXXX
	\brief Gruppi di richieste fisse a invio ciclico
*/
#define NUM_REQ_SET_STATUS		11	//12
static const PCREQ reqSet_Status[NUM_REQ_SET_STATUS] =
{
	//(PCREQ)&reqCoils,	la bitmap SETTINGS e' letta con la richiesta reqEcoBmap_NFan
	(PCREQ)&reqInputStato_Misure_1,
	(PCREQ)&reqInputStato_Misure_2,
	(PCREQ)&reqEcoBmap_NFan,
	(PCREQ)&reqTermAmbBmap,
	(PCREQ)&reqAccumBmap,
	(PCREQ)&reqFireFan,
	(PCREQ)&reqLimitsFireFan,
	(PCREQ)&reqSoundAlarmBmap,
	(PCREQ)&reqWiFiBmap,
	(PCREQ)&reqMaxPower,
	(PCREQ)&reqConfigIdro,
};

/*!
	\struct reqSet_tab_struct
	\brief Struttura per gruppo di richieste fisse a invio ciclico
*/
struct reqSet_tab_struct
{
	unsigned char nReq;		//!< requests' number
	PPCREQ reqSet;				//!< requests' set pointer
};
/*!
	\typedef REQSET_TAB_STRUCT
	\brief Typedef definition for reqSet_tab_struct
*/
typedef struct reqSet_tab_struct REQSET_TAB_STRUCT;
static const REQSET_TAB_STRUCT reqSet_tab[NUM_SEND_REQ_SET] =
{
	{ NUM_REQ_SET_STATUS, reqSet_Status },		//!< SEND_REQ_SET_STATUS
};

#if defined(TX_ONLY) || defined(RX_ONLY)
unsigned long cnt;
#endif

/*!
	\struct RadioCfg
	\brief Struttura di configurazione radio
*/
static RCQ2_868_STRUCT RadioCfg;
#define RF_DEST_ADDRESS		(0x7E7E7E00L)
#define RF_MY_ADDRESS		(0x007E7E7EL)

/*!
	\var rfChanMaxPower
	\brief Massima potenza per canale (CEPT/ERC Rec 70-03)
*/
static const unsigned char rfChanMaxPower[NUM_RF_CHAN] =
{
	RF_TXPOWER_14_DBM,	// RF_CHAN_868_2_MHZ <= 25 mW
	RF_TXPOWER_14_DBM,	// RF_CHAN_868_4_MHZ <= 25 mW
	RF_TXPOWER_08_DBM,	// RF_CHAN_868_6_MHZ <= 10 mW
	RF_TXPOWER_14_DBM,	// RF_CHAN_868_8_MHZ <= 25 mW
	RF_TXPOWER_14_DBM,	// RF_CHAN_869_0_MHZ <= 25 mW
	RF_TXPOWER_08_DBM,	// RF_CHAN_869_2_MHZ <= 10 mW
	RF_TXPOWER_20_DBM,	// RF_CHAN_869_4_MHZ <= 500 mW
	RF_TXPOWER_14_DBM,	// RF_CHAN_869_6_MHZ <= 25 mW
	RF_TXPOWER_05_DBM,	// RF_CHAN_869_8_MHZ <= 5 mW
	RF_TXPOWER_05_DBM,	// RF_CHAN_870_0_MHZ <= 5 mW
};

const unsigned char rfChannel = RF_CHAN_868_6_MHZ;
const unsigned char rfPower = RF_TXPOWER_08_DBM;



/* codice */

static void OT_RxPkt( unsigned char *pkt, unsigned short pktSize,
						unsigned char ErrCode, PKT_STRUCT *Req );
static short SetCoil( unsigned short addr, unsigned short value );
static short SetInputReg( unsigned short addr, unsigned short value );
static short SetHoldingReg( unsigned short addr, unsigned short value );
static short SetLimits( unsigned short addr, unsigned char *pb );
static void ReqLimits( void );
static short SendTembAmb( void );


/*!
	\fn short OT_Init( void )
   \brief Inizializzazione gestore comunicazione OT/+
	\return 1: OK, 0: FAILED
*/
short OT_Init( void )
{
	short RetVal = 0;

	cntErrOneShot = 0;
	cntErrCycle = 0;
	PreTempAmbPAN = -32000;

#if defined(TX_ONLY) || defined(RX_ONLY)
	cnt = 0;
#else
	starttimer( T_OT, T_ANTI_DEAD_LOCK );	// start dead-lock protection timer
#endif

	OT_Stat = OT_STAT_UNLINKED;
	OT_Result = OT_RESULT_IDLE;
	
	HAL_GPIO_WritePin(RESET_RF_GPIO_Port, RESET_RF_Pin, GPIO_PIN_RESET);	// RADIO reset on
	
	RetVal = OT_DL_Init( OT_RxPkt );
	rfID = RFid;

	return RetVal;
}

/*!
	\fn void OT_Handler( void )
	\brief Gestore comunicazione OT/+
*/
void OT_Handler( void )
{
	// richiama gestore data-link layer
	OT_DL_Handler();

#if !defined(TX_ONLY) && !defined(RX_ONLY)
	if( (OT_STAT_UNLINKED_READ_VERFW <= OT_Stat)
		&& (OT_Stat <= OT_STAT_LINKED)
		)
	{
		if( cntPausa )
		{
			// pausa lunga di invio
			starttimer( T_OT, T_ANTI_DEAD_LOCK );	// restart dead-lock protection timer
		}
		// check dead-lock protection timer
		else if( checktimer( T_OT ) != INCORSO )
		{
			OT_Stat = OT_STAT_UNLINKED;
		}
	}
#endif

	switch( OT_Stat )
	{
		case OT_STAT_UNLINKED:
			OT_DL_RFFilter = 0;
		//case OT_STAT_UNLINKED_STBY:
			OT_DL_Pausa = OT_DL_PAUSE;
#if defined(TX_ONLY) || defined(RX_ONLY)
			// set configurazione radio
			RadioCfg.DestAddr = 0x7E7E7E7EL;
			RadioCfg.RCQ2Addr = 0x7E7E7E7EL;
			RadioCfg.RFChan = rfChannel;
			RadioCfg.TXPower = rfPower;
			RadioCfg.TxDataPacketSize = 30;
			RadioCfg.RxDataPacketSize = 30;

			OT_PH_SetRCQ2Cfg( &RadioCfg );
			starttimer( T_OT, T1SEC );
			OT_Stat = OT_STAT_UNLINKED_RADIO_SET_CONFIG;
#else
			// get configurazione radio
			memset( &RadioCfg, 0, sizeof(RCQ2_868_STRUCT) );
			OT_PH_GetRCQ2Cfg( &RadioCfg );
			starttimer( T_OT, T1SEC );
			/* if( OT_Stat == OT_STAT_UNLINKED_STBY )
				OT_Stat = OT_STAT_UNLINKED_STBY_RADIO_GET_CONFIG;
			else */
				OT_Stat = OT_STAT_UNLINKED_RADIO_GET_CONFIG;
#endif
		break;

		case OT_STAT_UNLINKED_RADIO_GET_CONFIG:
		//case OT_STAT_UNLINKED_STBY_RADIO_GET_CONFIG:
			// attesa fine acquisizione configurazione radio
			switch( OT_PH_GetRCQ2Cfg( &RadioCfg ) )
			{
				case 1:
					// configurazione radio acquisita con successo
					RadioCfg.DestAddr = DWordLittle2BigEndian( RadioCfg.DestAddr );
					RadioCfg.RCQ2Addr = DWordLittle2BigEndian( RadioCfg.RCQ2Addr );
				#ifdef OT_LOGGING
					myprintf( "%u RX Radio cfg:\r\n", TimeSec );
					myprintf( "\tDestAddr=%08X\r\n", RadioCfg.DestAddr );
					myprintf( "\tRCQ2Addr=%08X\r\n", RadioCfg.RCQ2Addr );
					myprintf( "\tRFChan=%u\r\n", RadioCfg.RFChan );
					myprintf( "\tTXPower=%u\r\n", RadioCfg.TXPower );
					myprintf( "\tTxDataPacketSize=%u\r\n", RadioCfg.TxDataPacketSize );
					myprintf( "\tRxDataPacketSize=%u\r\n", RadioCfg.RxDataPacketSize );
				#endif
					// check configurazione radio
					if(
						(RadioCfg.DestAddr != RF_DEST_ADDRESS) ||
						(RadioCfg.RCQ2Addr != RF_MY_ADDRESS) ||
						(RadioCfg.RFChan != rfChannel) ||
						(RadioCfg.TXPower != rfPower) ||
						(RadioCfg.TxDataPacketSize != 30) ||
						(RadioCfg.RxDataPacketSize != 30)
						)
					{
						// set configurazione radio
						RadioCfg.DestAddr = DWordLittle2BigEndian( RF_DEST_ADDRESS );
						RadioCfg.RCQ2Addr = DWordLittle2BigEndian( RF_MY_ADDRESS );
						RadioCfg.RFChan = rfChannel;
						RadioCfg.TXPower = rfPower;
						RadioCfg.TxDataPacketSize = 30;
						RadioCfg.RxDataPacketSize = 30;
					#ifdef OT_LOGGING
						myprintf( "%u TX Radio cfg:\r\n", TimeSec );
						myprintf( "\tDestAddr=%08X\r\n", DWordLittle2BigEndian( RadioCfg.DestAddr ) );
						myprintf( "\tRCQ2Addr=%08X\r\n", DWordLittle2BigEndian( RadioCfg.RCQ2Addr ) );
						myprintf( "\tRFChan=%u\r\n", RadioCfg.RFChan );
						myprintf( "\tTXPower=%u\r\n", RadioCfg.TXPower );
						myprintf( "\tTxDataPacketSize=%u\r\n", RadioCfg.TxDataPacketSize );
						myprintf( "\tRxDataPacketSize=%u\r\n", RadioCfg.RxDataPacketSize );
					#endif

						OT_PH_SetRCQ2Cfg( &RadioCfg );
						starttimer( T_OT, 2*T1SEC );
						/* if( OT_Stat == OT_STAT_UNLINKED_STBY_RADIO_GET_CONFIG )
							OT_Stat = OT_STAT_UNLINKED_STBY_RADIO_SET_CONFIG;
						else */
							OT_Stat = OT_STAT_UNLINKED_RADIO_SET_CONFIG;
					}
					else
					{
						// radio configurata con successo
					#ifdef OT_LOGGING
						myprintf( "%u Radio cfg OK\r\n", TimeSec );
					#endif
						Consumo = 0;	// azzero consumo orario
						// riavvio gestore data-link layer
						OT_DL_Restart();
						/* if( OT_Stat == OT_STAT_UNLINKED_STBY_RADIO_GET_CONFIG )
						{
							// ripristino richieste Limiti/Step
							OT_DL_SetLimitReqFunc( ReqLimits );
							// reset dell'invio unico corrente
							OT_DL_FlushOneShotQueue();
							// avvio richieste cicliche di stato
							OT_Send_SetReq( SEND_REQ_SET_STATUS );
							starttimer( T_OT_UNLINKED, TMAX_OT_UNLINKED );
							OT_Stat = OT_STAT_LINKED;
							// invio valore misurato temperatura ambiente
							if( SendTembAmb() )
							{
								PreTempAmbPAN = TEMP_AMB_PAN;
							}
						}
						else */
						{
							// disabilito richieste Limiti/Step
							OT_DL_SetLimitReqFunc( NULL );
							// reset dell'invio unico corrente
							OT_DL_FlushOneShotQueue();
							// reset dell'invio ciclico corrente
							OT_DL_FlushCycleQueue();
							// invia richiesta #1 di link
							if( OT_Send( (PKT_STRUCT *)&reqVerFW, SEND_MODE_ONE_SHOT, 0 ) )
							{
								OT_Stat = OT_STAT_UNLINKED_READ_VERFW;
								starttimer( T_OT, T_ANTI_DEAD_LOCK );	// start dead-lock protection timer
							}
							else
							{
								OT_Stat = OT_STAT_UNLINKED;
							}
						}
					}
				break;

				case 0:
					// acquisizione configurazione radio in corso
					if( checktimer( T_OT ) != INCORSO )
					{
						// timeout: abort procedura
					#ifdef OT_LOGGING
						myprintf( "%u RX Radio cfg FAILED (timeout)\r\n", TimeSec );
					#endif
						OT_PH_GetRCQ2Cfg( NULL );
						OT_Stat = OT_STAT_UNLINKED;
					}
				break;

				default:
				case -1:
					// acquisizione configurazione radio fallita
				#ifdef OT_LOGGING
					myprintf( "%u RX Radio cfg FAILED\r\n", TimeSec );
				#endif
					/* if( OT_Stat == OT_STAT_UNLINKED_STBY_RADIO_GET_CONFIG )
					{
						starttimer( T_OT, T1DEC );
						OT_Stat = OT_STAT_UNLINKED_STBY_WAIT;
					}
					else */
					{
						OT_Stat = OT_STAT_UNLINKED;
					}
				break;
			}
		break;

		case OT_STAT_UNLINKED_RADIO_SET_CONFIG:
		//case OT_STAT_UNLINKED_STBY_RADIO_SET_CONFIG:
			// attesa fine configurazione radio
			switch( OT_PH_SetRCQ2Cfg( &RadioCfg ) )
			{
				case 1:
					// radio configurata con successo
				#ifdef OT_LOGGING
					myprintf( "%u TX Radio cfg OK\r\n", TimeSec );
				#endif
					Consumo = 0;	// azzero consumo orario
#if defined(TX_ONLY) || defined(RX_ONLY)
					starttimer( T_OT, 5*T1DEC );
					OT_Stat = OT_STAT_UNLINKED_READ_VERFW;
#else
					// riavvio gestore data-link layer
					OT_DL_Restart();
					/* if( OT_Stat == OT_STAT_UNLINKED_RADIO_SET_CONFIG )
					{
						// ripristino richieste Limiti/Step
						OT_DL_SetLimitReqFunc( ReqLimits );
						// reset dell'invio unico corrente
						OT_DL_FlushOneShotQueue();
						// avvio richieste cicliche di stato
						OT_Send_SetReq( SEND_REQ_SET_STATUS );
						starttimer( T_OT_UNLINKED, TMAX_OT_UNLINKED );
						OT_Stat = OT_STAT_LINKED;
						// invio valore misurato temperatura ambiente
						if( SendTembAmb() )
						{
							PreTempAmbPAN = TEMP_AMB_PAN;
						}
					}
					else */
					{
						// disabilito richieste Limiti/Step
						OT_DL_SetLimitReqFunc( NULL );
						// reset dell'invio unico corrente
						OT_DL_FlushOneShotQueue();
						// reset dell'invio ciclico corrente
						OT_DL_FlushCycleQueue();
						// invia richiesta #1 di link
						if( OT_Send( (PKT_STRUCT *)&reqVerFW, SEND_MODE_ONE_SHOT, 0 ) )
						{
							OT_Stat = OT_STAT_UNLINKED_READ_VERFW;
							starttimer( T_OT, T_ANTI_DEAD_LOCK );	// start dead-lock protection timer
						}
						else
						{
							OT_Stat = OT_STAT_UNLINKED;
						}
					}
#endif
				break;

				case 0:
					// configurazione radio in corso
					if( checktimer( T_OT ) != INCORSO )
					{
						// timeout: abort procedura
					#ifdef OT_LOGGING
						myprintf( "%u TX Radio cfg FAILED (timeout)\r\n", TimeSec );
					#endif
						OT_PH_SetRCQ2Cfg( NULL );
						OT_Stat = OT_STAT_UNLINKED;
					}
				break;

				default:
				case -1:
					// configurazione radio fallita
				#ifdef OT_LOGGING
					myprintf( "%u TX Radio cfg FAILED\r\n", TimeSec );
				#endif
					/* if( OT_Stat == OT_STAT_UNLINKED_RADIO_SET_CONFIG )
					{
						starttimer( T_OT, T1DEC );
						OT_Stat = OT_STAT_UNLINKED_STBY_WAIT;
					}
					else */
					{
						OT_Stat = OT_STAT_UNLINKED;
					}
				break;
			}
		break;
		
		case OT_STAT_UNLINKED_STBY_WAIT:
			// attesa assestamento alimentazione radio
			if( checktimer( T_OT ) != INCORSO )
			{
				starttimer( T_OT, T_ANTI_DEAD_LOCK );	// start dead-lock protection timer
				rfID = RFid;
				//OT_Stat = OT_STAT_UNLINKED_STBY;
				/*
					All'uscita dal power-down si assume che la radio
					sia rimasta configurata.
				*/
				if( OT_NextStat == OT_STAT_UNLINKED )
				{
					OT_Stat = OT_STAT_UNLINKED;
				}
				else
				{
					Consumo = 0;	// azzero consumo orario
					// riavvio gestore data-link layer
					OT_DL_Restart();
					if( iLimReq < LIMITSREQ_NUM )
					{
						// ripristino richieste Limiti/Step
						OT_DL_SetLimitReqFunc( ReqLimits );
					}
					else
					{
						// disabilito richieste Limiti/Step
						OT_DL_SetLimitReqFunc( NULL );
					}
					// reset dell'invio unico corrente
					OT_DL_FlushOneShotQueue();
					// avvio richieste cicliche di stato
					OT_Send_SetReq( SEND_REQ_SET_STATUS );
					starttimer( T_OT_UNLINKED, TMAX_OT_UNLINKED );
					OT_Stat = OT_STAT_LINKED;
					// invio valore misurato temperatura ambiente
					if( SendTembAmb() )
					{
						PreTempAmbPAN = TEMP_AMB_PAN;
					}
				}
			}
		break;

		case OT_STAT_UNLINKED_READ_VERFW:
		case OT_STAT_UNLINKED_WRITE_VERFW:
		case OT_STAT_LINKED_WRITE_RFID:
#if defined(TX_ONLY)
			// invio unica richiesta ogni 500ms
			if( checktimer( T_OT ) != INCORSO )
			{
				starttimer( T_OT, 5*T1DEC );
				OT_PH_Send( reqVerFW.pkt, reqVerFW.pktLen, 1 );
				cnt++;
			}
#endif
		break;

		case OT_STAT_LINKED:
			OT_DL_RFFilter = 1;
			// invio valore misurato temperatura ambiente ad ogni COV
			if( PreTempAmbPAN != TEMP_AMB_PAN )
			{
				if( SendTembAmb() )
				{
					PreTempAmbPAN = TEMP_AMB_PAN;
				}
			}
			// altrimenti alla scadenza del timer
			else if( checktimer( T_TEMP_AMB ) != INCORSO )
			{
				if( SendTembAmb() )
				{
					PreTempAmbPAN = TEMP_AMB_PAN;
				}
			}
			
			/* // aggiornamento consumi alla scadenza del timer
			if( PesataPellet > 0 )
			{
				if( checktimer( T_CONSUMO ) != INCORSO )
				{
					if( OT_Send( (PKT_STRUCT *)&reqConsumo, SEND_MODE_ONE_SHOT, 0 ) )
					{
						starttimer( T_CONSUMO, TMAX_CONSUMO );
					}
				}
				else if(
							(STATO_STUFA < STUFASTAT_IN_ACCENSIONE) ||
							(STATO_STUFA > STUFASTAT_IN_ACCENSIONE_RETE)
							)
				{
					Consumo = 0;	// azzero consumo orario
				}
			}
			else
			{
				Consumo = 0;	// azzero consumo orario
			} */
		break;
	}
}

/*!
	\fn void OT_RxPkt( unsigned char *pkt, unsigned short pktSize,
							unsigned char ErrCode, PKT_STRUCT *Req );
	\brief Funzione call-back per ricezione pacchetto.
	\param pkt pacchetto ricevuto
	\param pktSize dimensione pacchetto
	\param ErrCode codice di errore transazione tx-rx
	\param Req Richiesta trasmessa
	\note Il gestore data-link layer al rientro da questa call-back esegue un
			NextPkt() sulla coda cui appartiene la richiesta trasmessa,
			ritornando in stato idle. Se la coda e` la CYCLE e la ricezione e`
			avvenuta con successo, puo` inserire nella coda ONE_SHOT una eventuale
			richiesta Limiti/Step.
*/
static void OT_RxPkt( unsigned char *pkt, unsigned short pktSize,
							unsigned char ErrCode, PKT_STRUCT *Req )
{
#ifdef RX_ONLY
	if( ErrCode == RX_ERR_NONE )
	{
		// ricevuto pacchetto
		cnt++;
	}
#else
	unsigned char SlaveRFid = pkt[0];	// radio ID slave
	unsigned char SlaveAddr = Req->pkt[1];	// indirizzo slave
	unsigned char cmd = Req->pkt[2];	// comando inviato
	short i;
	unsigned short n;
	unsigned short addr;
	unsigned short w;
	unsigned char *pb;
	const LIMITSREQ_STRUCT *ps = &LimitsReq_tab[iLimReq];
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[28];		//!< packet buffer
	} sPkt;

#ifndef TX_ONLY
	starttimer( T_OT, T_ANTI_DEAD_LOCK );	// restart dead-lock protection timer
#endif
	
	if( ErrCode == RX_ERR_ONE_SHOT )
		cntErrOneShot++;
	else if( ErrCode == RX_ERR_CYCLE )
		cntErrCycle++;

	// richiesta con esito?
	i = 0;
	if( OT_Result == OT_RESULT_RUN )
	{
		if( saveReq.pktLen == Req->pktLen )
		{
			if( !memcmp( saveReq.pkt, Req->pkt, saveReq.pktLen ) )
			{
				// segnalo di comunicare l'esito
				i = 1;
			}
		}
	}
	
	if( ErrCode != RX_ERR_NONE )
	{
		// risposta non ricevuta -> slave disconnesso
		if( i )
		{
			// esito richiesta
			OT_Result = OT_RESULT_FAILED;
		}
		if( OT_Stat == OT_STAT_LINKED )
		{
			// filtraggio della perdita di connessione
			if( checktimer( T_OT_UNLINKED ) != INCORSO )
			{
				OT_Stat = OT_STAT_UNLINKED;
			}
		}
		else
		{
			// slave definitivamente disconnesso
			if( OT_Stat == OT_STAT_UNLINKED_READ_VERFW )
			{
				// alterna RFID con il jolly
				if( rfID == RFid )
					rfID = RFID_JOLLY;
				else
					rfID = RFid;
			}
			OT_Stat = OT_STAT_UNLINKED;
		}
	}
	else if( (pktSize >= 3) && (pkt[1] == SlaveAddr) )
	{
		// risposta da slave indirizzato dalla richiesta
		starttimer( T_OT_UNLINKED, TMAX_OT_UNLINKED );
		if( pkt[2] == cmd + MB_FUNC_ERROR )
		{
			// richiesta rifiutata
			if( pkt[2] == MB_EX_SLAVE_BUSY )
			{
				// slave busy -> riaccodo la richiesta
				OT_Send( Req, SEND_MODE_ONE_SHOT, i );
				return;
			}
			else if( i )
			{
				// esito richiesta
				OT_Result = OT_RESULT_FAILED;
			}
			switch( cmd )
			{
				case MB_FUNC_WRITE_SINGLE_COIL:
					if(
						(OT_Stat == OT_STAT_LINKED) &&
						((pkt[3] == MB_EX_ILLEGAL_DATA_VALUE) || (pkt[3] == MB_EX_SLAVE_DEVICE_FAILURE))
						)
					{
						// rileggo il singolo coil (allineamento)
						sPkt.pktLen = 0;
						sPkt.pkt[sPkt.pktLen++] = SlaveRFid;	// radio ID
						sPkt.pkt[sPkt.pktLen++] = SlaveAddr;	// addr
						sPkt.pkt[sPkt.pktLen++] = MB_FUNC_READ_COILS;	// cmd
						sPkt.pkt[sPkt.pktLen++] = Req->pkt[3];	// addr H
						sPkt.pkt[sPkt.pktLen++] = Req->pkt[4];	// addr L
						sPkt.pkt[sPkt.pktLen++] = 0x00;	// Nbit H
						sPkt.pkt[sPkt.pktLen++] = 0x01;	// Nbit L
						OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
					}
				break;

				case MB_FUNC_WRITE_MULTIPLE_COILS:
					if(
						(OT_Stat == OT_STAT_LINKED) &&
						((pkt[3] == MB_EX_ILLEGAL_DATA_VALUE) || (pkt[3] == MB_EX_SLAVE_DEVICE_FAILURE))
						)
					{
						// rileggo i coil (allineamento)
						sPkt.pktLen = 0;
						sPkt.pkt[sPkt.pktLen++] = SlaveRFid;	// radio ID
						sPkt.pkt[sPkt.pktLen++] = SlaveAddr;	// addr
						sPkt.pkt[sPkt.pktLen++] = MB_FUNC_READ_COILS;	// cmd
						sPkt.pkt[sPkt.pktLen++] = Req->pkt[3];	// addr H
						sPkt.pkt[sPkt.pktLen++] = Req->pkt[4];	// addr L
						sPkt.pkt[sPkt.pktLen++] = Req->pkt[5];	// Nbit H
						sPkt.pkt[sPkt.pktLen++] = Req->pkt[6];	// Nbit L
						OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
					}
				break;

				case MB_FUNC_WRITE_MULTIPLE_REGISTERS:
					if( (OT_Stat == OT_STAT_UNLINKED_WRITE_VERFW) &&
						(Req->pkt[3] == 0x01) && (Req->pkt[4] == 0x42)
						)
					{
						// fallimento comunicazione ver FW pannello
						OT_Stat = OT_STAT_UNLINKED;
					}
					else if( (OT_Stat == OT_STAT_LINKED_WRITE_RFID) &&
						(Req->pkt[3] == HIBYTE( ADDR_RFID )) && (Req->pkt[4] == LOBYTE( ADDR_RFID ))
						)
					{
						// fallimento impostazione RFID
						OT_Stat = OT_STAT_UNLINKED;
					}
					else if(
								(OT_Stat == OT_STAT_LINKED) &&
								((pkt[3] == MB_EX_ILLEGAL_DATA_VALUE) || (pkt[3] == MB_EX_SLAVE_DEVICE_FAILURE))
							)
					{
						// rileggo gli holding register (allineamento)
						sPkt.pktLen = 0;
						sPkt.pkt[sPkt.pktLen++] = SlaveRFid;	// radio ID
						sPkt.pkt[sPkt.pktLen++] = SlaveAddr;	// addr
						sPkt.pkt[sPkt.pktLen++] = MB_FUNC_READ_HOLDING_REGISTER;	// cmd
						sPkt.pkt[sPkt.pktLen++] = Req->pkt[3];	// addr H
						sPkt.pkt[sPkt.pktLen++] = Req->pkt[4];	// addr L
						sPkt.pkt[sPkt.pktLen++] = Req->pkt[5];	// Nword H
						sPkt.pkt[sPkt.pktLen++] = Req->pkt[6];	// Nword L
						OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
					}
				break;

				case MB_FUNC_READ_INPUT_REGISTER:
					if( OT_Stat == OT_STAT_UNLINKED_READ_VERFW )
					{
						// fallimento richiesta ver FW scheda
						OT_Stat = OT_STAT_UNLINKED;
					}
					/*
						Verifico se la richiesta rifiutata e` una di quelle ONE_SHOT
						che vengono fatte solamente a connessione avvenuta.
						Nel caso, riaccodo la richiesta.
					*/
					else if( !memcmp( &Req->pkt[1], &reqLinguaDef.pkt[1], reqLinguaDef.pktLen-1 ) )
					{
						// richiedo lingua di default
						OT_Send( (PKT_STRUCT *)&reqLinguaDef, SEND_MODE_ONE_SHOT, 0 );
					}
					else if( !memcmp( &Req->pkt[1], &reqTipoStufa.pkt[1], reqTipoStufa.pktLen-1 ) )
					{
						// richiedo tipo stufa
						OT_Send( (PKT_STRUCT *)&reqTipoStufa, SEND_MODE_ONE_SHOT, 0 );
					}
					else if( !memcmp( &Req->pkt[1], &reqPassword.pkt[1], reqPassword.pktLen-1 ) )
					{
						// richiedo password
						OT_Send( (PKT_STRUCT *)&reqPassword, SEND_MODE_ONE_SHOT, 0 );
					}
					else if( !memcmp( &Req->pkt[1], &reqLogo.pkt[1], reqLogo.pktLen-1 ) )
					{
						// richiedo Logo
						OT_Send( (PKT_STRUCT *)&reqLogo, SEND_MODE_ONE_SHOT, 0 );
					}
					else if( !memcmp( &Req->pkt[1], &reqTipoDesc_1_2.pkt[1], reqTipoDesc_1_2.pktLen-1 ) )
					{
						// richiedo descrizione tipo 1-2
						OT_Send( (PKT_STRUCT *)&reqTipoDesc_1_2, SEND_MODE_ONE_SHOT, 0 );
					}
					else if( !memcmp( &Req->pkt[1], &reqTipoDesc_3_4.pkt[1], reqTipoDesc_3_4.pktLen-1 ) )
					{
						// richiedo descrizione tipo 3-4
						OT_Send( (PKT_STRUCT *)&reqTipoDesc_3_4, SEND_MODE_ONE_SHOT, 0 );
					}
					else if( !memcmp( &Req->pkt[1], &reqTipoDesc_5_6.pkt[1], reqTipoDesc_5_6.pktLen-1 ) )
					{
						// richiedo descrizione tipo 5-6
						OT_Send( (PKT_STRUCT *)&reqTipoDesc_5_6, SEND_MODE_ONE_SHOT, 0 );
					}
					else if( !memcmp( &Req->pkt[1], &reqTipoDesc_7_8.pkt[1], reqTipoDesc_7_8.pktLen-1 ) )
					{
						// richiedo descrizione tipo 7-8
						OT_Send( (PKT_STRUCT *)&reqTipoDesc_7_8, SEND_MODE_ONE_SHOT, 0 );
					}
					// altrimenti si suppone trattarsi di una lettura ciclica
				break;

				case MB_FUNC_TYPE_SET:
					if(
						(OT_Stat == OT_STAT_LINKED) &&
						((pkt[3] == MB_EX_ILLEGAL_DATA_VALUE) || (pkt[3] == MB_EX_SLAVE_DEVICE_FAILURE))
						)
					{
						// rileggo tipo stufa (allineamento)
						OT_Send( (PKT_STRUCT *)&reqTipoStufa, SEND_MODE_ONE_SHOT, 0 );
					}
				break;

				case MB_FUNC_UNLOCK:
				case MB_FUNC_CMD_DEBUG:
				break;
			}
		}
		else if( pkt[2] == cmd )
		{
			// richiesta accettata
			if( i )
			{
				// esito richiesta
				OT_Result = OT_RESULT_OK;
			}
			switch( cmd )
			{
				case MB_FUNC_READ_COILS:
					// aggiorna i coil
					n = MAKEWORD( Req->pkt[6], Req->pkt[5] );
					if( pkt[3] &&
						((!(n % 8) && (pkt[3] >= n/8)) ||
						 ((n % 8) && (pkt[3] >= 1 + n/8)))
						)
					{
						addr = MAKEWORD( Req->pkt[4], Req->pkt[3] );
						for( i=0; i<n; i++ )
						{
							if( GetBitBMap( &pkt[4], i ) )
								SetCoil( addr, 1 );
							else
								SetCoil( addr, 0 );
							addr++;
						}
					}
					else
					{
						// risposta non corretta
						w = 1;
					}
				break;

				case MB_FUNC_READ_INPUT_REGISTER:
					// aggiorna gli input register
					n = MAKEWORD( Req->pkt[6], Req->pkt[5] );
					if( pkt[3] >= 2*n )
					{
						addr = MAKEWORD( Req->pkt[4], Req->pkt[3] );
						pb = &pkt[4];
						for( i=0; i<n; i++ )
						{
							w = MAKEWORD( pb[1], pb[0] );
							SetInputReg( addr, w );
							addr++;
							pb = (unsigned char *)((unsigned long)pb + 2*sizeof(char));
						}
						if( OT_Stat == OT_STAT_UNLINKED_READ_VERFW )
						{
							// invia richiesta #2 di link
							sPkt.pktLen = 0;
							sPkt.pkt[sPkt.pktLen++] = SlaveRFid;	// radio ID
							sPkt.pkt[sPkt.pktLen++] = SlaveAddr;	// addr
							sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
							sPkt.pkt[sPkt.pktLen++] = 0x01;	// addr H
							sPkt.pkt[sPkt.pktLen++] = 0x42;	// addr L
							sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
							sPkt.pkt[sPkt.pktLen++] = 0x07;		// Nword L
							sPkt.pkt[sPkt.pktLen++] = 14;		// Nbyte
							sPkt.pkt[sPkt.pktLen++] = HIBYTE( TBLACKOUT );		// TBLACKOUT H
							sPkt.pkt[sPkt.pktLen++] = LOBYTE( TBLACKOUT );		// TBLACKOUT L
							sPkt.pkt[sPkt.pktLen++] = 0x00;		// StatSec.LastStatoFunz H
							sPkt.pkt[sPkt.pktLen++] = StatSec.LastStatoFunz;		// StatSec.LastStatoFunz L
							sPkt.pkt[sPkt.pktLen++] = HIBYTE( TEMP_AMB_PAN );		// TEMP_AMB_PAN H
							sPkt.pkt[sPkt.pktLen++] = LOBYTE( TEMP_AMB_PAN );		// TEMP_AMB_PAN L
							sPkt.pkt[sPkt.pktLen++] = 0x00;		// codSW H
							sPkt.pkt[sPkt.pktLen++] = codSW;		// codSW L
							sPkt.pkt[sPkt.pktLen++] = 0x00;		// verSW H
							sPkt.pkt[sPkt.pktLen++] = verSW;		// verSW L
							sPkt.pkt[sPkt.pktLen++] = 0x00;		// revSW H
							sPkt.pkt[sPkt.pktLen++] = revSW;		// revSW L
							sPkt.pkt[sPkt.pktLen++] = 0x00;		// buildSW H
							sPkt.pkt[sPkt.pktLen++] = buildSW;		// buildSW L
							OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
							OT_Stat = OT_STAT_UNLINKED_WRITE_VERFW;
							PreTempAmbPAN = TEMP_AMB_PAN;
							starttimer( T_TEMP_AMB, TMAX_TEMP_AMB );
							starttimer( T_CONSUMO, TMAX_CONSUMO );
						}
					}
					else
					{
						// risposta non corretta
						w = 2;
					}
				break;

				case MB_FUNC_READ_HOLDING_REGISTER:
					// aggiorna gli holding register
					n = MAKEWORD( Req->pkt[6], Req->pkt[5] );
					if( pkt[3] >= 2*n )
					{
						addr = MAKEWORD( Req->pkt[4], Req->pkt[3] );
						pb = &pkt[4];
						for( i=0; i<n; i++ )
						{
							w = MAKEWORD( pb[1], pb[0] );
							SetHoldingReg( addr, w );
							addr++;
							pb = (unsigned char *)((unsigned long)pb + 2*sizeof(char));
						}
					}
					else
					{
						// risposta non corretta
						w = 3;
					}
				break;

				case MB_FUNC_READ_LIMITS:
					// aggiorna i limiti degli holding register
					n = MAKEWORD( Req->pkt[6], Req->pkt[5] );
					if( pkt[3] >= 5*n )
					{
						addr = MAKEWORD( Req->pkt[4], Req->pkt[3] );
						pb = &pkt[4];
						for( i=0; i<n; i++ )
						{
							SetLimits( addr, pb );
							addr++;
							pb = (unsigned char *)((unsigned long)pb + 5*sizeof(char));
						}
						// eventuale aggiornamento procedura di richieste Limits/Step
						addr -= n;
						if( (iLimReq < LIMITSREQ_NUM) &&
							(addr == ps->HoldRegAddr) &&
							(n == ps->nHoldAddr) )
						{
							if( ++iLimReq >= LIMITSREQ_NUM )
							{
								// termine procedura
								OT_DL_SetLimitReqFunc( NULL );
							}
						}
					}
					else
					{
						// risposta non corretta
						w = 4;
					}
				break;

				case MB_FUNC_WRITE_MULTIPLE_REGISTERS:
					if( OT_Stat == OT_STAT_UNLINKED_WRITE_VERFW )
					{
						// link ok
						if( SlaveRFid != RFid )
						{
							// associo la scheda impostando RFID
							sPkt.pktLen = 0;
							sPkt.pkt[sPkt.pktLen++] = SlaveRFid;	// radio ID
							sPkt.pkt[sPkt.pktLen++] = SlaveAddr;	// addr
							sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
							sPkt.pkt[sPkt.pktLen++] = HIBYTE( ADDR_RFID );	// addr H
							sPkt.pkt[sPkt.pktLen++] = LOBYTE( ADDR_RFID );	// addr L
							sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
							sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
							sPkt.pkt[sPkt.pktLen++] = 2;		// Nbyte
							sPkt.pkt[sPkt.pktLen++] = 0x00;		// RFid H
							sPkt.pkt[sPkt.pktLen++] = RFid;		// RFid L
							OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
							OT_Stat = OT_STAT_LINKED_WRITE_RFID;
						}
						else
						{
							// scheda gia` associata
							rfID = RFid;
							OT_Stat = OT_STAT_LINKED;
							// reset procedura richieste limiti/step
							memset( OT_LimitStep_Requested, 0, sizeof(OT_LimitStep_Requested) );
							iLimReq = 0;
							OT_DL_SetLimitReqFunc( ReqLimits );
							// richiedo lingua di default
							OT_Send( (PKT_STRUCT *)&reqLinguaDef, SEND_MODE_ONE_SHOT, 0 );
							// richiedo tipo stufa
							OT_Send( (PKT_STRUCT *)&reqTipoStufa, SEND_MODE_ONE_SHOT, 0 );
							// richiedo password
							OT_Send( (PKT_STRUCT *)&reqPassword, SEND_MODE_ONE_SHOT, 0 );
							// richiedo Logo
							OT_Send( (PKT_STRUCT *)&reqLogo, SEND_MODE_ONE_SHOT, 0 );
							// richiedo descrizione tipo 1-2
							OT_Send( (PKT_STRUCT *)&reqTipoDesc_1_2, SEND_MODE_ONE_SHOT, 0 );
							// richiedo descrizione tipo 3-4
							OT_Send( (PKT_STRUCT *)&reqTipoDesc_3_4, SEND_MODE_ONE_SHOT, 0 );
							// richiedo descrizione tipo 5-6
							OT_Send( (PKT_STRUCT *)&reqTipoDesc_5_6, SEND_MODE_ONE_SHOT, 0 );
							// richiedo descrizione tipo 7-8
							OT_Send( (PKT_STRUCT *)&reqTipoDesc_7_8, SEND_MODE_ONE_SHOT, 0 );
							// avvio richieste cicliche di stato
							OT_Send_SetReq( SEND_REQ_SET_STATUS );
						}
					}
					else if( OT_Stat == OT_STAT_LINKED_WRITE_RFID )
					{
						// rfid ok
						rfID = RFid;
						OT_Stat = OT_STAT_LINKED;
						// reset procedura richieste limiti/step
						memset( OT_LimitStep_Requested, 0, sizeof(OT_LimitStep_Requested) );
						iLimReq = 0;
						OT_DL_SetLimitReqFunc( ReqLimits );
						// richiedo lingua di default
						OT_Send( (PKT_STRUCT *)&reqLinguaDef, SEND_MODE_ONE_SHOT, 0 );
						// richiedo tipo stufa
						OT_Send( (PKT_STRUCT *)&reqTipoStufa, SEND_MODE_ONE_SHOT, 0 );
						// richiedo password
						OT_Send( (PKT_STRUCT *)&reqPassword, SEND_MODE_ONE_SHOT, 0 );
						// richiedo Logo
						OT_Send( (PKT_STRUCT *)&reqLogo, SEND_MODE_ONE_SHOT, 0 );
						// richiedo descrizione tipo 1-2
						OT_Send( (PKT_STRUCT *)&reqTipoDesc_1_2, SEND_MODE_ONE_SHOT, 0 );
						// richiedo descrizione tipo 3-4
						OT_Send( (PKT_STRUCT *)&reqTipoDesc_3_4, SEND_MODE_ONE_SHOT, 0 );
						// richiedo descrizione tipo 5-6
						OT_Send( (PKT_STRUCT *)&reqTipoDesc_5_6, SEND_MODE_ONE_SHOT, 0 );
						// richiedo descrizione tipo 7-8
						OT_Send( (PKT_STRUCT *)&reqTipoDesc_7_8, SEND_MODE_ONE_SHOT, 0 );
						// avvio richieste cicliche di stato
						OT_Send_SetReq( SEND_REQ_SET_STATUS );
					}
				break;
			}
		}
		else
		{
			// risposta errata
			if( i )
			{
				// esito richiesta
				OT_Result = OT_RESULT_FAILED;
			}
			if( OT_Stat != OT_STAT_LINKED )	// se connesso, mantengo la connessione
			{
				if( OT_Stat == OT_STAT_UNLINKED_READ_VERFW )
				{
					// alterna RFID con il jolly
					if( rfID == RFid )
						rfID = RFID_JOLLY;
					else
						rfID = RFid;
				}
				OT_Stat = OT_STAT_UNLINKED;
			}
		}
	}
	else
	{
		// per sicurezza ...
		if( i )
		{
			// esito richiesta
			OT_Result = OT_RESULT_FAILED;
		}
		if( OT_Stat != OT_STAT_LINKED )	// se connesso, mantengo la connessione
		{
			if( OT_Stat == OT_STAT_UNLINKED_READ_VERFW )
			{
				// alterna RFID con il jolly
				if( rfID == RFid )
					rfID = RFID_JOLLY;
				else
					rfID = RFid;
			}
			OT_Stat = OT_STAT_UNLINKED;
		}
	}
#endif	// RX_ONLY
}

/*!
	\fn short OT_Send_SetReq( unsigned short iSetReq )
   \brief Predispone l'invio ciclico del gruppo di richieste fisse indicato.
	\param iSetReq indice del gruppo di richieste fisse
	\return 1: OK, 0: FAILED
*/
short OT_Send_SetReq( unsigned short iSetReq )
{
	short RetVal = 0;
	unsigned short i;
	const REQSET_TAB_STRUCT *ppReqSet;
	PCREQ pReqSet;

	if( iSetReq < NUM_SEND_REQ_SET )
	{
		// reset dell'invio ciclico corrente
		OT_DL_FlushCycleQueue();
		// carica il gruppo di richieste indicato
		ppReqSet = &reqSet_tab[iSetReq];
		for( i=0; i<ppReqSet->nReq; i++ )
		{
			pReqSet = ppReqSet->reqSet[i];
			if( !(RetVal = OT_Send( (PKT_STRUCT *)pReqSet, SEND_MODE_CYCLE, 0 )) )
				break;
		}
	}

	return RetVal;
}

/*!
	\fn short SetCoil( unsigned short addr, unsigned short value )
   \brief Imposta il coil indicato sul valore indicato
	\param addr indirizzo del coil
	\param value nuovo valore
	\return 1: OK, 0: FAILED
*/
static short SetCoil( unsigned short addr, unsigned short value )
{
	short RetVal = 0;
	const MBDATA *pt = &Coil_tab[addr];	// puntatore al registro in tabella
	unsigned short *pw = pt->pw;	// puntatore al valore del registro
	unsigned short Mask;

	value &= 0x0001;
	switch( pt->Tipo )
	{
		case MBTYPE_BOOL:
			*pw = value;
			RetVal = 1;
		break;

		case MBTYPE_BITMAP:
			if( value )
				SetBitBMap( (unsigned char *)pw, pt->Pos );
			else
				ResBitBMap( (unsigned char *)pw, pt->Pos );
			RetVal = 1;
		break;

		case MBTYPE_BYTE_MASK:
			Mask = pt->Pos;
			while( !(Mask & 0x01) )
			{
				Mask >>= 1;
				value <<= 1;
			}
			*(unsigned char *)pw &= (~pt->Pos);
			*(unsigned char *)pw |= (unsigned char)value;
			RetVal = 1;
		break;
	}

	return RetVal;
}

/*!
	\fn short SetInputReg( unsigned short addr, unsigned short value )
   \brief Imposta l'Input Register indicato sul valore indicato
	\param addr indirizzo dell'Input Register
	\param value nuovo valore
	\return 1: OK, 0: FAILED
*/
static short SetInputReg( unsigned short addr, unsigned short value )
{
	short RetVal = 0;
	const MBDATA *pt = &InpReg_tab[addr];	// puntatore al registro in tabella
	unsigned short *pw = pt->pw;	// puntatore al valore del registro
	unsigned short Mask;
	unsigned long d;
	ALARM_RECORD AlmRec;

	switch( pt->Tipo )
	{
		case MBTYPE_CHAR:
		case MBTYPE_BYTE:
			if( addr == 0x0014 )
			{
				// Eccezione: codice allarme
				if( (TIPO_ALLARME == NO_ALARM) && (value != NO_ALARM) )
				{
					// registra il nuovo allarme
					AlmRec.timestamp = rtcRd( NULL );
					AlmRec.TipoAllarme = (unsigned char)value;
					AlarmPut( &AlmRec );
					if( value == A01 )
					{
						// incrementa contatore mancate accensioni
						StatSec.NumNoAcc++;
					}
				}
				*((unsigned char *)pw) = (unsigned char)value;
			}
			else if( addr == 0x0029 )
			{
				// Eccezione: stato logico stufa
				*((unsigned char *)pw) = (unsigned char)value;
				
				if( (value != STAT_RESET) &&
					(value != STAT_TESTER_AUTO) &&
					(value != STAT_TESTER_MANU)
					)
				{
					// salva l'ultimo stato logico stufa
					StatSec.LastStatoFunz = (unsigned char)value;
				}
					
				if( value == STAT_TESTER_AUTO )
				{
					TEST_STUFA = 0;
					TEST_AUTO = 1;
				}
				else if( value == STAT_TESTER_MANU )
				{
					TEST_STUFA = 1;
					TEST_AUTO = 0;
				}
				else
				{
					TEST_STUFA = 0;
					TEST_AUTO = 0;
				}
			}
			else if( addr == 0x006C )
			{
				// Eccezione: tipo stufa
				*((unsigned char *)pw) = (unsigned char)value;
				// azioni per tipo stufa
				if( (TIPO_STUFA == NO_TYPE) || (TIPO_STUFA >= NTYPE) )
				{
					// Se non ho un tipo stufa valido entro nello stato per la sua impostazione.
					STATO_ALL_BLACK = PAN_NO_TIPO;
				}
			}
 			else if( addr == 0x006D )
			{
				// Eccezione: lingua di default
				*((unsigned char *)pw) = (unsigned char)value;
				// solo se la lingua e' gia' al default, cambio il default (se gestito)
				if( (StatSec.Language == LANG_DEF) && (value < NumLanguage) )
				{
					iLanguage = value;
				}
			}
			else if( pt->Pos == 0 )
			{
				*((unsigned char *)pw) = (unsigned char)value;
			}
			else
			{
				*pw = value;
			}
		break;

		case MBTYPE_BYTE_MASK:
			Mask = pt->Pos;
			while( !(Mask & 0x01) )
			{
				Mask >>= 1;
				value <<= 1;
			}
			*(unsigned char *)pw &= (~pt->Pos);
			*(unsigned char *)pw |= (unsigned char)value;
		break;

		case MBTYPE_WORD:
			*pw = value;
			if( addr == 0x0034 )
			{
				// Consumo orario espresso in 0.05 kg/h
				d = (500L * Consumo)/6;	// arrotondo a 0.1 g/min
				d = (d + 5)/10;
				// accumulo il consumo dell'ultimo minuto
				StatSec.AccConsumo += d;
			}
		break;
		
		case MBTYPE_BITMAP:
		case MBTYPE_INT:
		case MBTYPE_DWORD:
		case MBTYPE_LONG:
		case MBTYPE_FLOAT:
			*pw = value;
		break;
	}

	return RetVal;
}

/*!
	\fn short SetHoldingReg( unsigned short addr, unsigned short value )
   \brief Imposta il Holding Register indicato sul valore indicato
	\param addr indirizzo del Holding Register
	\param value nuovo valore
	\return 1: OK, 0: FAILED
*/
static short SetHoldingReg( unsigned short addr, unsigned short value )
{
	short RetVal = 0;
	const MBDATA *pt = &HoldReg_tab[addr];	// puntatore al registro in tabella
	unsigned short *pw = pt->pw;	// puntatore al valore del registro
	unsigned short Mask;

	if( addr >= THOLREG3_FIRST_ADDR )
	{
		// Holding Register 3
		pt = &HoldReg3_tab[addr-THOLREG3_FIRST_ADDR];	// puntatore al registro in tabella
		pw = pt->pw;	// puntatore al valore del registro
	}
	else if( addr >= THOLREG2_FIRST_ADDR )
	{
		// Holding Register 2
		pt = &HoldReg2_tab[addr-THOLREG2_FIRST_ADDR];	// puntatore al registro in tabella
		pw = pt->pw;	// puntatore al valore del registro
	}

	switch( pt->Tipo )
	{
		case MBTYPE_CHAR:
		case MBTYPE_BYTE:
			if( pt->Pos == 0 )
			{
				*((unsigned char *)pw) = (unsigned char)value;
			}
			else
			{
				*pw = value;
			}
		break;

		case MBTYPE_BYTE_MASK:
			Mask = pt->Pos;
			while( !(Mask & 0x01) )
			{
				Mask >>= 1;
				value <<= 1;
			}
			*(unsigned char *)pw &= (~pt->Pos);
			*(unsigned char *)pw |= (unsigned char)value;
		break;

		case MBTYPE_WORD:
			*pw = value;
		break;

		case MBTYPE_BITMAP:
		case MBTYPE_INT:
		case MBTYPE_DWORD:
		case MBTYPE_LONG:
		case MBTYPE_FLOAT:
			*pw = value;
		break;
	}

	return RetVal;
}

/*!
	\fn short SetLimits( unsigned short addr, unsigned char *pb )
   \brief Imposta i limiti del Holding Register indicato
	\param addr indirizzo del Holding Register
	\param pb record dei limiti
	\return 1: OK, 0: FAILED
*/
static short SetLimits( unsigned short addr, unsigned char *pb )
{
	short RetVal = 1;
	unsigned short w;
	LIMITS_STRUCT *pLim;

	if( addr >= THOLREG3_FIRST_ADDR )
	{
		// errore: limiti gia` noti
		return 0;
	}
	else if( addr >= THOLREG2_FIRST_ADDR )
	{
		pLim = &Limits2_tab[addr-THOLREG2_FIRST_ADDR];
	}
	else
	{
		pLim = &Limits_tab[addr];
		// registro avvenuta richiesta
		SetBitBMap( OT_LimitStep_Requested, addr );
	}

	w = MAKEWORD( pb[1], pb[0] );
	pLim->Min = w;
	w = MAKEWORD( pb[3], pb[2] );
	pLim->Max = w;
	pLim->Step = pb[4];

	return RetVal;
}

/*!
	\fn void ReqLimits( void )
   \brief Procedura di richieste Limits/Step
*/
static void ReqLimits( void )
{
	const LIMITSREQ_STRUCT *ps;
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;

	if( (OT_Stat == OT_STAT_LINKED) && (iLimReq < LIMITSREQ_NUM) )
	{
	#ifdef OT_LOGGING
		myprintf( "Req LIMITI: [%u]\r\n", iLimReq );
	#endif
		ps = &LimitsReq_tab[iLimReq];
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_READ_LIMITS;	// cmd
		sPkt.pkt[sPkt.pktLen++] = HIBYTE( ps->HoldRegAddr );	// addr H
		sPkt.pkt[sPkt.pktLen++] = LOBYTE( ps->HoldRegAddr );	// addr L
		sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
		sPkt.pkt[sPkt.pktLen++] = ps->nHoldAddr;		// Nword L
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
	}
}

/*!
	\fn void OT_ReqSound( unsigned char SoundCode )
   \brief Invio comando di generazione suono
	\param SoundCode Codice suono
*/
void OT_ReqSound( unsigned char SoundCode )
{
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;

	if( OT_Stat == OT_STAT_LINKED )
	{
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_SUONO_RC;	// cmd
		sPkt.pkt[sPkt.pktLen++] = SoundCode;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
	}
}

/*!
	\fn short SendTembAmb( void )
   \brief Invio valore misurato temperatura ambiente
	\return 1: OK, 0: FAILED
	\note Richiede anche il codice ISO3166-1 alpha 2 del Paese locale
*/
static short SendTembAmb( void )
{
	short RetVal = 0;
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[16];		//!< packet buffer
	} sPkt;

	if( OT_Stat == OT_STAT_LINKED )
	{
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
		sPkt.pkt[sPkt.pktLen++] = 0x01;	// addr H
		sPkt.pkt[sPkt.pktLen++] = 0x44;	// addr L
		sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
		sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
		sPkt.pkt[sPkt.pktLen++] = 2;		// Nbyte
		sPkt.pkt[sPkt.pktLen++] = HIBYTE( TEMP_AMB_PAN );		// TEMP_AMB_PAN H
		sPkt.pkt[sPkt.pktLen++] = LOBYTE( TEMP_AMB_PAN );		// TEMP_AMB_PAN L
		if( OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 ) )
		{
			OT_DL_Pausa = OT_DL_PAUSE;
			starttimer( T_TEMP_AMB, TMAX_TEMP_AMB );
			RetVal = 1;
			// richiesta anche del codice ISO3166-1 alpha 2 del Paese locale
			OT_Send( (PKT_STRUCT *)&reqISOCode, SEND_MODE_ONE_SHOT, 0 );
		}
	}
	
	return RetVal;
}

/*!
	\fn short OT_Send( PKT_STRUCT *sPkt, unsigned char mode, unsigned char esito )
   \brief Invio di un pacchetto
	\param pkt struttura pacchetto da inviare
	\param mode modo di invio (SEND_MODE_ONE_SHOT o SEND_MODE_CYCLE)
	\param esito 1: richiede l'esito dell'invio (solo per SEND_MODE_ONE_SHOT)
	\return 1: OK, 0: FAILED
	\note I pacchetti con invio unico sono prioritari rispetto a quelli
			con invio ciclico.
			La struttura del pacchetto con invio unico viene ricopiata dalla
			funzione nella propria coda e puo` quindi essere allocata in modo
			dinamico.
			La struttura del pacchetto con invio ciclico viene registrata dalla
			funzione come puntatore nella propria coda e deve quindi essere
			allocata in modo statico.
			E` possibile richiedere l'esito di invio solo per un pacchetto alla
			volta: prima di richiedere un nuovo esito, bisogna attendere l'esito
			richiesto precedentemente; si ottiene sempre l'ultimo esito richiesto.
*/
short OT_Send( PKT_STRUCT *sPkt, unsigned char mode, unsigned char esito )
{
	short RetVal = OT_DL_Send( sPkt, mode );
	
	if( RetVal )
	{
		if( esito && (mode == SEND_MODE_ONE_SHOT) )
		{
			OT_Result = OT_RESULT_RUN;
			// registro il pacchetto di cui si vuole l'esito dell'invio
			saveReq = *sPkt;
			// mi assicuro di registrarlo con RFID corrente
			saveReq.pkt[0] = rfID;
			/*
				Se era gia` OT_Result = OT_RESULT_RUN, la richiesta di esito
				precedente viene sovrascritta.
			*/
		}
	}
	else if( esito && (mode == SEND_MODE_ONE_SHOT) )
	{
		OT_Result = OT_RESULT_FAILED;
	}
	
	return RetVal;
}

/*!
	\fn OT_RESULT OT_GetResult( void )
	\brief Ritorna l'esito dell'invio registrato
	\return Il codice di esito
*/
OT_RESULT OT_GetResult( void )
{
	OT_RESULT RetVal = OT_Result;
	
	if( OT_Result != OT_RESULT_RUN )
	{
		// reset esito
		OT_Result = OT_RESULT_IDLE;
	}
	
	return RetVal;
}

/*!
	\fn void OT_RestartRequests( void )
   \brief Riavvia la sequenza delle richieste come alla connessione
*/
void OT_RestartRequests( void )
{
	if( OT_Stat == OT_STAT_LINKED )
	{
		// riavvio gestore data-link layer
		rfID = RFid;
		OT_DL_Restart();
		// disabilito richieste Limiti/Step
		OT_DL_SetLimitReqFunc( NULL );
		// reset dell'invio unico corrente
		OT_DL_FlushOneShotQueue();
		// reset dell'invio ciclico corrente
		OT_DL_FlushCycleQueue();
		
		// reset procedura richieste limiti/step
		memset( OT_LimitStep_Requested, 0, sizeof(OT_LimitStep_Requested) );
		iLimReq = 0;
		OT_DL_SetLimitReqFunc( ReqLimits );
		// richiedo lingua di default
		OT_Send( (PKT_STRUCT *)&reqLinguaDef, SEND_MODE_ONE_SHOT, 0 );
		// richiedo tipo stufa
		OT_Send( (PKT_STRUCT *)&reqTipoStufa, SEND_MODE_ONE_SHOT, 0 );
		// richiedo password
		OT_Send( (PKT_STRUCT *)&reqPassword, SEND_MODE_ONE_SHOT, 0 );
		// richiedo Logo
		OT_Send( (PKT_STRUCT *)&reqLogo, SEND_MODE_ONE_SHOT, 0 );
		// richiedo descrizione tipo 1-2
		OT_Send( (PKT_STRUCT *)&reqTipoDesc_1_2, SEND_MODE_ONE_SHOT, 0 );
		// richiedo descrizione tipo 3-4
		OT_Send( (PKT_STRUCT *)&reqTipoDesc_3_4, SEND_MODE_ONE_SHOT, 0 );
		// richiedo descrizione tipo 5-6
		OT_Send( (PKT_STRUCT *)&reqTipoDesc_5_6, SEND_MODE_ONE_SHOT, 0 );
		// richiedo descrizione tipo 7-8
		OT_Send( (PKT_STRUCT *)&reqTipoDesc_7_8, SEND_MODE_ONE_SHOT, 0 );
		// avvio richieste cicliche di stato
		OT_Send_SetReq( SEND_REQ_SET_STATUS );
	}
}

/*!
	\fn short SendVerFW( void )
   \brief Invio versione FW
*/
short SendVerFW( void )
{
	short RetVal = 0;
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[28];		//!< packet buffer
	} sPkt;

	if( OT_Stat == OT_STAT_LINKED )
	{
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
		sPkt.pkt[sPkt.pktLen++] = 0x01;	// addr H
		sPkt.pkt[sPkt.pktLen++] = 0x42;	// addr L
		sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
		sPkt.pkt[sPkt.pktLen++] = 0x07;		// Nword L
		sPkt.pkt[sPkt.pktLen++] = 14;		// Nbyte
		sPkt.pkt[sPkt.pktLen++] = HIBYTE( TBLACKOUT );		// TBLACKOUT H
		sPkt.pkt[sPkt.pktLen++] = LOBYTE( TBLACKOUT );		// TBLACKOUT L
		sPkt.pkt[sPkt.pktLen++] = 0x00;		// StatSec.LastStatoFunz H
		sPkt.pkt[sPkt.pktLen++] = StatSec.LastStatoFunz;		// StatSec.LastStatoFunz L
		sPkt.pkt[sPkt.pktLen++] = HIBYTE( TEMP_AMB_PAN );		// TEMP_AMB_PAN H
		sPkt.pkt[sPkt.pktLen++] = LOBYTE( TEMP_AMB_PAN );		// TEMP_AMB_PAN L
		sPkt.pkt[sPkt.pktLen++] = 0x00;		// codSW H
		sPkt.pkt[sPkt.pktLen++] = codSW;		// codSW L
		sPkt.pkt[sPkt.pktLen++] = 0x00;		// verSW H
		sPkt.pkt[sPkt.pktLen++] = verSW;		// verSW L
		sPkt.pkt[sPkt.pktLen++] = 0x00;		// revSW H
		sPkt.pkt[sPkt.pktLen++] = revSW;		// revSW L
		sPkt.pkt[sPkt.pktLen++] = 0x00;		// buildSW H
		sPkt.pkt[sPkt.pktLen++] = buildSW;		// buildSW L
		if( OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 ) )
		{
			starttimer( T_TEMP_AMB, TMAX_TEMP_AMB );
			RetVal = 1;
		}
	}
	
	return RetVal;
}

/*!
	\fn void OT_SetETime( unsigned long ETime )
   \brief Invio comando di impostazione Epoch Time
	\param ETime Valore Epoch Time
*/
void OT_SetETime( unsigned long ETime )
{
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;
	TBitDWord u;

	if( OT_Stat == OT_STAT_LINKED )
	{
		u.DWORD = ETime;
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
		sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_NETWORK;
		sPkt.pkt[sPkt.pktLen++] = 10;		// set Epoch Time
		sPkt.pkt[sPkt.pktLen++] = u.BYTE.HH;
		sPkt.pkt[sPkt.pktLen++] = u.BYTE.HL;
		sPkt.pkt[sPkt.pktLen++] = u.BYTE.LH;
		sPkt.pkt[sPkt.pktLen++] = u.BYTE.LL;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
	}
}

/*!
	\fn void OT_ExitStandby( void )
	\brief Uscita dallo stato di standby
*/
void OT_ExitStandby( void )
{
	cntErrOneShot = 0;
	cntErrCycle = 0;
	
	if( OT_Stat == OT_STAT_LINKED )
	{
		OT_NextStat = OT_STAT_LINKED;
	}
	else
	{
		OT_NextStat = OT_STAT_UNLINKED;
	}

	starttimer( T_OT, 3*T1DEC );
	OT_Stat = OT_STAT_UNLINKED_STBY_WAIT;
	OT_Result = OT_RESULT_IDLE;

	OT_DL_Init( OT_RxPkt );
	rfID = RFid;
}


