/*!
** \file user.c
** \author Alessandro Vagniluca
** \date 26/05/2015
** \brief User Interface handler
** 
** \version 1.0
**/

//#define BITMAP_TEST	// enable the image test
//#define FONT_TEST		// enable the font test
//#define TOUCH_TEST		// enable the touch test
//#define DEBUG_CONTINUE_IF_NO_LINK	// continue even if no link
#define TOUCH_CALIBRATE	// enable the touch calibration
#ifdef TOUCH_CALIBRATE
	//#define TOUCH_CALIBRATION_AND_READ	// enable the read of the touch calibration
#endif
//#define SIMUL_WIFI		// WiFi simulation
#ifdef SIMUL_WIFI
	//#define WIFI_IP_VALID	// WiFi IP valid
#endif


/* Includes ------------------------------------------------------------------*/
#include "user.h"
#include "User_Parametri.h"
#include "userMenu.h"
#include "userResources.h"
#include "userInit.h"
#include "userReset.h"
#include "userAvvio.h"
#include "userParamCode.h"
#include "userCrono.h"
#include "DecodInput.h"
#include "userAllarmi.h"


#define BUFFER_OPTIMIZATION


/* Private variables ---------------------------------------------------------*/

#define FT800_LOW_FREQ_BOUND		(47040000)	// 47.04MHz minimum PCLK = 48MHz - 2% (must be 48MHZ +/- 3%)

// Colors - fully saturated colors defined here
#define RED					0xFF0000UL													// Red
#define GREEN				0x00FF00UL													// Green
#define BLUE				0x0000FFUL													// Blue
#define WHITE				0xFFFFFFUL													// White
#define BLACK				0x000000UL													// Black

// Global Variables

unsigned char userCurrentState;
unsigned char userSubState;
unsigned long userCounterTime;
userStateHandler userNextPointer;

BYTE_UNION userParValue;

BYTE iCronoPrg;

BYTE tmpRFid;

Ft_Gpu_Hal_Context_t host;	//!< GPU HAL Context
static ft_uint32_t Ft_CmdBuffer_Index;
static ft_uint32_t Ft_DlBuffer_Index;

#ifdef BUFFER_OPTIMIZATION
static ft_uint8_t  Ft_DlBuffer[FT_DL_SIZE];
static ft_uint8_t  Ft_CmdBuffer[FT_CMD_FIFO_SIZE];
#endif

osMessageQId TouchQueueHandle;	//!< the touch queue

USER_FLAGS Flag;


// waiting the setting result
unsigned char StDownload;
const char charStDownload[NSTAT_DOWNLOAD] = { '|', '/', '-', '\\' }; 
WAIT_SET_RESULT WaitSetResult;

unsigned char cntExit4Alarm;	//!< Standby Pause exit counter for alarms



/* Private function prototypes -----------------------------------------------*/
static unsigned long ft800MeasureFreq( void );
static void ft800TuneFreq( void );
static unsigned char userPause( void );


/*!
** \var userStateTable
** \brief The table of the User Interface state handlers
**/
static const userStateHandler userStateTable[USER_MAX_STATES] =
{
	userResources,		// USER_STATE_RESOURCES,
	userBattery,		// USER_STATE_BATTERY,
	userInit,			// USER_STATE_INIT,
	userReset,			// USER_STATE_RESET,
			 
	userAvvio,			// USER_STATE_AVVIO,
	NULL,			// USER_STATE_AVVIO_ALLARME,
	userStandby,			// USER_STATE_STANDBY,
	NULL,			// USER_STATE_ALLARME,
			 
#ifndef menu_CRONO
	userCrono,			// USER_STATE_CRONO,
#endif
			 
	userMenu,			// USER_STATE_LOCAL_SETTINGS,
	userMenu,			// USER_STATE_MENU,
	userMenu,			// USER_STATE_INFO,
	userParamCode,			// USER_STATE_PARAM_CODE,
	userParamVis,			// USER_STATE_PARAM_VIS,
			 
	NULL,			// USER_STATE_COLLAUDO,
			 
	(userStateHandler)SystemOff,			// USER_STATE_LOW_POWER,
	userPause,			// USER_STATE_PAUSE,
	userRestartOff,			// USER_STATE_RESTART_OFF,
};


STRUCT_USER_COLOR UserColor_tab[UCOL_MAX] = 
{
	{ 0, 130, 203, UCOL_BLUE },			// UCOL_BLUE,
	{ 155, 255, 0, UCOL_GREEN },		// UCOL_GREEN,
	{ 225, 225, 0, UCOL_YELLOW },		// UCOL_YELLOW,
	{ 240, 140, 0, UCOL_ORANGE },		// UCOL_ORANGE,
	{ 240, 30, 65, UCOL_SCARLET },		// UCOL_SCARLET,
	{ 150, 50, 255, UCOL_PURPLE },		// UCOL_PURPLE,
	{ 255, 180, 255, UCOL_PINK },		// UCOL_PINK,
	{ 115, 115, 115, UCOL_GREY },		// UCOL_GREY,
	{ 0, 0, 0, UCOL_GREY },				// UCOL_BLACK,
};



/*!
** \fn void SetBacklight( unsigned short duty )
** \brief Set the backlight level
** \param duty The new backlight level (%)
** \return None
**/
void SetBacklight( unsigned short duty )
{
#ifdef DISCHARGE_BATTERY
	duty = 100;	// sempre al max
#endif
	if( duty )
	{
		HAL_GPIO_WritePin(EN_V_BCKL_GPIO_Port, EN_V_BCKL_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(EN_V_BCKL_GPIO_Port, EN_V_BCKL_Pin, GPIO_PIN_RESET);
	}
	Ft_Gpu_Hal_Wr8( pHost, REG_PWM_DUTY, (duty*128)/100 );
}

/*!
** \fn unsigned long ft800MeasureFreq( void )
** \brief Measures the FT80x working frequency
** \return The measured frequency in Hz
**/
static unsigned long ft800MeasureFreq( void )
{
	unsigned long t1;
	unsigned long t0 = Ft_Gpu_Hal_Rd32( pHost, REG_CLOCK );

	Wdog();
	// delay 15625 us (= 1/64)
	HAL_Delay(15);
	DWT_Delay_us(625);
	t1 = Ft_Gpu_Hal_Rd32( pHost, REG_CLOCK );
	t1 = (unsigned long)(t1 - t0);
	t1 <<= 6;	// x64
	
	return t1;	// freq in Hz
}

/*!
** \fn void ft800TuneFreq( void )
** \brief Internal clock trimming sequence
**/
static void ft800TuneFreq( void )
{
	unsigned char i;
	unsigned long freq;
	
	// start trimming from the minimum frequency
	Ft_Gpu_Hal_Wr8( pHost, REG_TRIM, 0 );
	HAL_Delay(2);
	for( i=1; (i<32) && ((freq = ft800MeasureFreq()) < FT800_LOW_FREQ_BOUND); i++ )
	{
		Wdog();
		// increase REG_TRIM value
		Ft_Gpu_Hal_Wr8( pHost, REG_TRIM, i );
	}
	Ft_Gpu_Hal_Wr32( pHost, REG_FREQUENCY, freq );
}

ft_void_t Ft_App_WrCoCmd_Buffer(Ft_Gpu_Hal_Context_t *host,ft_uint32_t cmd)
{
#ifdef  BUFFER_OPTIMIZATION
   /* Copy the command instruction into buffer */
   ft_uint32_t *pBuffcmd;
   pBuffcmd =(ft_uint32_t*)&Ft_CmdBuffer[Ft_CmdBuffer_Index];
   *pBuffcmd = cmd;
#else
   Ft_Gpu_Hal_WrCmd32(host,cmd);
#endif
   /* Increment the command index */
   Ft_CmdBuffer_Index += FT_CMD_SIZE;  
}

ft_void_t Ft_App_WrDlCmd_Buffer(Ft_Gpu_Hal_Context_t *host,ft_uint32_t cmd)
{
#ifdef BUFFER_OPTIMIZATION  
   /* Copy the command instruction into buffer */
   ft_uint32_t *pBuffcmd;
   pBuffcmd =(ft_uint32_t*)&Ft_DlBuffer[Ft_DlBuffer_Index];
   *pBuffcmd = cmd;
#else
   Ft_Gpu_Hal_Wr32(host,(RAM_DL+Ft_DlBuffer_Index),cmd);
#endif
   /* Increment the command index */
   Ft_DlBuffer_Index += FT_CMD_SIZE;  
}

ft_void_t Ft_App_WrCoStr_Buffer(Ft_Gpu_Hal_Context_t *host,const ft_char8_t *s)
{
  ft_uint16_t length = 0;
  length = strlen(s) + 1;//last for the null termination
  
#ifdef  BUFFER_OPTIMIZATION  
  strcpy((char *)&Ft_CmdBuffer[Ft_CmdBuffer_Index],s);

  /* increment the length and align it by 4 bytes */
  Ft_CmdBuffer_Index += ((length + 3) & ~3);  
#else	
	Ft_Gpu_Hal_WrCmdBuf(host,(ft_uint8_t*)s,length);
#endif  
}

ft_void_t Ft_App_Flush_DL_Buffer(Ft_Gpu_Hal_Context_t *host)
{
#ifdef  BUFFER_OPTIMIZATION    
   if (Ft_DlBuffer_Index > 0)
     Ft_Gpu_Hal_WrMem(host,RAM_DL,Ft_DlBuffer,Ft_DlBuffer_Index);
#endif     
   Ft_DlBuffer_Index = 0;
}

ft_void_t Ft_App_Flush_Co_Buffer(Ft_Gpu_Hal_Context_t *host)
{
#ifdef  BUFFER_OPTIMIZATION    
   if (Ft_CmdBuffer_Index > 0)
     Ft_Gpu_Hal_WrCmdBuf(host,Ft_CmdBuffer,Ft_CmdBuffer_Index);
#endif     
   Ft_CmdBuffer_Index = 0;
}

ft_uint32_t Get_UserColorHex(unsigned char iCol)
{
	if (iCol >= UCOL_MAX)
	{
		iCol = UCOL_BLUE;
	}
	STRUCT_USER_COLOR * color = &UserColor_tab[iCol];
	return COLOR_RGB(color->red, color->green, color->blue);
}


/*!
** \fn int User_Init( void )
** \brief User interface initialization
** \return 1: OK, 0: FAILED
** \note SPI peripheral is supposed already properly initialized
**/
int User_Init( void )
{
	int i;
	Ft_Gpu_HalInit_t halinit;
	unsigned char ft800Gpio;
	
	halinit.TotalChannelNum = 1;
	Ft_Gpu_Hal_Init(&halinit);
	
	host.hal_config.channel_no = 0;
	host.hal_config.spi_clockrate_khz = 12000; //in KHz
	Ft_Gpu_Hal_Open(pHost);

	Wdog();
	HAL_Delay(20);																				// Wait 20ms
	Wdog();
	
	/* Do a power cycle for safer side */
	Ft_Gpu_Hal_Powercycle(pHost,FT_TRUE);

	/* Access address 0 to wake up the FT800 */
	Ft_Gpu_HostCommand(pHost,FT_GPU_ACTIVE_M);
	Wdog();
	HAL_Delay(20);																					// Give some time to process
	Wdog();

	/* Do a core reset for safer side */
	Ft_Gpu_CoreReset( pHost );	// reset all the internal registers and state machines

	/* Set the clock to external clock */
	// Ft_Gpu_ClockSelect( pHost, FT_GPU_EXTERNAL_OSC );
	// HAL_Delay(10);																					// Give some time to process

	/* Switch PLL output to 48MHz */
	Ft_Gpu_PLL_FreqSelect( pHost, FT_GPU_PLL_48M );
	Wdog();
	HAL_Delay(10);																					// Give some time to process
	Wdog();

	ft800TuneFreq();	// internal clock trimming

  	// Now FT800 can accept commands at up to 30MHz clock on SPI bus
	MX_SPI1_Init( CLK_FAST );
						
	//Read Register ID to check if FT800 is ready. 
	for( i=0; i<100; i++ )	// max 100 retries
	{
		if(Ft_Gpu_Hal_Rd8(pHost, REG_ID) == 0x7C )											// Read ID register - is it 0x7C?
			break;
	}
	if( i >= 100 )
	{
		return 0;	// If we don't get 0x7C, the interface isn't working
	}  
  
	// Set PCLK to zero - don't clock the LCD until later
	Ft_Gpu_Hal_Wr8(pHost, REG_PCLK, ZERO);
	// Turn off backlight
	Ft_Gpu_Hal_Wr8(pHost, REG_PWM_DUTY, ZERO);

	// End of Wake-up FT800
	
	//***************************************
	// Initialize Display
	Ft_Gpu_Hal_Wr16(pHost, REG_HSIZE,   FT_DispWidth);								// active display width
	Ft_Gpu_Hal_Wr16(pHost, REG_HCYCLE,  FT_DispHCycle);							// total number of clocks per line, incl front/back porch
	Ft_Gpu_Hal_Wr16(pHost, REG_HOFFSET, FT_DispHOffset);							// start of active line
	Ft_Gpu_Hal_Wr16(pHost, REG_HSYNC0,  FT_DispHSync0);							// start of horizontal sync pulse
	Ft_Gpu_Hal_Wr16(pHost, REG_HSYNC1,  FT_DispHSync1);							// end of horizontal sync pulse
	Ft_Gpu_Hal_Wr16(pHost, REG_VSIZE,   FT_DispHeight);							// active display height
	Ft_Gpu_Hal_Wr16(pHost, REG_VCYCLE,  FT_DispVCycle);							// total number of lines per screen, incl pre/post
	Ft_Gpu_Hal_Wr16(pHost, REG_VOFFSET, FT_DispVOffset);							// start of active screen
	Ft_Gpu_Hal_Wr16(pHost, REG_VSYNC0,  FT_DispVSync0);							// start of vertical sync pulse
	Ft_Gpu_Hal_Wr16(pHost, REG_VSYNC1,  FT_DispVSync1);							// end of vertical sync pulse
	Ft_Gpu_Hal_Wr8(pHost, REG_SWIZZLE,  FT_DispSwizzle);							// FT800 output to LCD - pin order
	Ft_Gpu_Hal_Wr8(pHost, REG_PCLK_POL, FT_DispPCLKPol);							// LCD data is clocked in on this PCLK edge
  
							// Don't set PCLK yet - wait for just after the first display list
	// End of Initialize Display
	//***************************************

	//***************************************
	// Configure Touch, Interrupts and Audio 
	//Ft_Gpu_Hal_Wr8(pHost, REG_TOUCH_MODE, TOUCHMODE_OFF);		// Disable touch
	//Ft_Gpu_Hal_Wr8(pHost, REG_TOUCH_MODE, TOUCHMODE_CONTINUOUS);	// continuous touch acquisitions
	/* Touch configuration - configure the resistance value to 1200 - this value is specific to customer requirement and derived by experiment */
    //Ft_Gpu_Hal_Wr16(pHost, REG_TOUCH_RZTHRESH,1200);
	Ft_Gpu_Hal_Wr16(pHost, REG_TOUCH_RZTHRESH, ZERO);						// Eliminate any false touches

	Ft_Gpu_Hal_Wr8(pHost, REG_INT_EN, 0);	// disable FT800 interrupts
	Ft_Gpu_Hal_Rd8(pHost, REG_INT_FLAGS);	// clear all FT800 interrupts
	
	Ft_Gpu_Hal_Wr8(pHost, REG_VOL_PB, ZERO);											// turn recorded audio volume down
	Ft_Gpu_Hal_Wr8(pHost, REG_VOL_SOUND, ZERO);									// turn synthesizer volume down
	Ft_Gpu_Hal_Wr16(pHost, REG_SOUND, 0x6000);										// set synthesizer to mute
	  
	// End of Configure Touch, Interrupts and Audio
	//***************************************

	Wdog();

	//***************************************
	// Write Initial Display List & Enable Display
	Ft_CmdBuffer_Index = 0;
	Ft_DlBuffer_Index = 0;
	
	Ft_App_WrDlCmd_Buffer( pHost, (DL_CLEAR_RGB) ); 				// Clear Color RGB to black
	Ft_App_WrDlCmd_Buffer( pHost, (DL_CLEAR | CLR_COL | CLR_STN | CLR_TAG) );
	Ft_App_WrDlCmd_Buffer( pHost, DISPLAY() );
	/* Download the DL into DL RAM */
	Ft_App_Flush_DL_Buffer( pHost );
	/* Do a swap */
	Ft_Gpu_Hal_Wr8(pHost, REG_DLSWAP, DLSWAP_FRAME);
	
	ft800Gpio = Ft_Gpu_Hal_Rd8(pHost, REG_GPIO_DIR);									// Read the FT800 GPIO_DIR register
	ft800Gpio = ft800Gpio | 0x83;													// set bit 7 and 1-0 of FT800 GPIO as outputs
	Ft_Gpu_Hal_Wr8(pHost, REG_GPIO_DIR, ft800Gpio);									// Set GPIO direction
	ft800Gpio = Ft_Gpu_Hal_Rd8(pHost, REG_GPIO);									// Read the FT800 GPIO register for a read/modify/write operation
	ft800Gpio = ft800Gpio | 0x83;													// set bit 7 and bit 1-0 of FT800 GPIO register (DISP)
	Ft_Gpu_Hal_Wr8(pHost, REG_GPIO, ft800Gpio);									// Enable the DISP signal to the LCD panel
	Ft_Gpu_Hal_Wr16(pHost, REG_PWM_HZ, LCD_BACKLIGHT_PWM_FREQ);										// set backlight pwm frequency
	//for(i = 0; i <= 128; i++)									// Turn on backlight - ramp up slowly to full brightness
	for(i = 0; i <= LCD_BACKLIGHT_MAX; i++)
	{
		Wdog();
		//Ft_Gpu_Hal_Wr8(pHost, REG_PWM_DUTY, i);
		SetBacklight( (unsigned short)i );
		HAL_Delay(10);
	}
	Ft_Gpu_Hal_Wr8(pHost, REG_PCLK, FT_DispPCLK);										// Now start clocking data to the LCD panel
	
#ifdef LCD_INVERTED_LANDSCAPE
	Ft_Gpu_Hal_Wr32(pHost, REG_ROTATE, 1);	// set LCD 180� rotated
#endif

	// End of Write Initial Display List & Enable Display
	//***************************************	
  	
	cntExit4Alarm = 0;
	
	return 1;
}

/*!
** \fn short TagRegister( unsigned char tag, unsigned short xs, unsigned short ys,
** 						unsigned short width, unsigned short height )
** \brief Register a touch tag
** \param tag Tag value
** \param xs X coordinate of the top left corner of the object
** \param xs Y coordinate of the top left corner of the object
** \param width Object width
** \param height Object height
** \return 1: OK, 0: FAILED
**/
short TagRegister( unsigned char tag, unsigned short xs, unsigned short ys,
					unsigned short width, unsigned short height )
{
	TAG_STRUCT Tag;	//!< the touch tag structure

	Tag.tag = tag;
	Tag.status = 0;
	Tag.enabled = 1;
	Tag.xs = xs;
	Tag.ys = ys;
	Tag.xe = Tag.xs + width - 1;
	Tag.ye = Tag.ys + height -1;

	return CTP_TagRegister( &Tag );
}

// flush the touch queue
void FlushTouchQueue( void )
{
	osEvent event;
	
	do
	{
		Wdog();
		// wait for a touch event
		event = osMessageGet( TouchQueueHandle, 0 );
	} while( event.status == osEventMessage );
}

/*!
** \fn ft_uint8_t Read_Keypad( TAG_STRUCT *pt )
** \brief Read a key from a touch keyboard
** \param pt The touch tag structure for the key
** \return The tag of the touched key, 0: key untouched
**/
ft_uint8_t Read_Keypad( TAG_STRUCT *pt )
{
	static ft_uint8_t Read_tag = 0;	// no tag value
	ft_uint8_t RetVal;
  
	if( pt->status )
	{
		if( pt->tag != Read_tag )
		{
			Read_tag = pt->tag;
			Flag.Key_Detect = 1;
		}
		RetVal = pt->tag;
	}
	else
	{
		Read_tag = 0;
		RetVal = 0;	// 0: key untouched
	}

	return RetVal;
}

/*!
** \fn ft_uint8_t Ft_Gpu_Font_WH( ft_uint8_t Char, ft_uint8_t font )
** \brief Get the width of the font character
** \param Char The font character index
** \param font The font to be used with the character
** \return The width of the font character
** \note ROM fonts begin from index 32 (space) up to index 127.
** 		Custom fonts begin from index 1 (space) up to index 127.
**/
ft_uint8_t Ft_Gpu_Font_WH( ft_uint8_t Char, ft_uint8_t font )
{
	ft_uint32_t ptr,Wptr;
	ft_uint8_t Width = 0;
	
	if( font >= 16 )
	{
		// ROM font
		ptr = Ft_Gpu_Hal_Rd32(pHost,0xffffc);
		// read Width of the character
		Wptr = (ptr + (148 * (font - 16)))+Char;	// (table starts at font 16)
	}
	else
	{
		// RAM font
		ptr = RAMFontTableAddr[font];
		// read Width of the character
		Wptr = ptr + Char2Font(Char, font);
	}
	
	Width = Ft_Gpu_Hal_Rd8(pHost,Wptr);
	
	return Width;
}

/*!
** \fn unsigned short GetStringWidth( char *s, ft_uint8_t font )
** \brief Get the width of a string with the used font
** \param s The string
** \param font The font used to view the string
** \return The width of the string
**/
unsigned short GetStringWidth( char *s, ft_uint8_t font )
{
	unsigned short len = 0;
	// ft_uint32_t ptr,Wptr;

	/* if( font >= 16 )
	{
		// ROM font
		ptr = Ft_Gpu_Hal_Rd32(pHost,0xffffc);
		Wptr = ptr + (148 * (font - 16));	// (table starts at font 16)
	}
	else
	{
		// RAM font
		ptr = RAMFontTableAddr[font];
		// read Width of the character
		Wptr = ptr;
	} */
	
	while( *s )
	{
		// len += Ft_Gpu_Hal_Rd8(pHost, Wptr+(*s) );
		len += Ft_Gpu_Font_WH( *s, font );
		s++;
	}
	
	return len;
}

/*!
** \fn short GetFont4StringWidth( char *s, ft_uint8_t startFont, unsigned short maxWidth )
** \brief Find the right proportional font for a string width
** \param s The string
** \param startFont The start proportional font index
** \param maxWidth The max available width for the string
** \return The right proportional font index, -1 if not found
** \note startFont >= 26 => ROM font range [26, startFont]
** 		startFont >= 20 => ROM font range [20, startFont]
** 		startFont >= 0 => RAM font range [0, startFont]
**/
short GetFont4StringWidth( char *s, ft_uint8_t startFont, unsigned short maxWidth )
{
	short RetVal = -1;
	short i;
	ft_uint8_t endFont = 0;	// RAM font
	
	if( startFont >= 26 ) endFont = 26;	// ROM font
	else if( startFont >= 20 ) endFont = 20;	// ROM font
	
	for( i=startFont; i>=endFont; i-- )
	{
		if( GetStringWidth( s, i ) <= maxWidth )
		{
			RetVal = i;
			break;
		}
	}
	
	return RetVal;
}

/*!
** \fn char Char2Font( char c, ft_uint8_t font )
** \brief Convert a character for the used font
** \param c The character
** \param font The used font
** \return The converted character (for custom RAM font only)
** \note ROM fonts begin from index 32 (space) up to index 127.
** 		Custom fonts begin from index 1 (space) up to index 127.
**/
char Char2Font( char c, ft_uint8_t font )
{
	char retChar = c;
	
	if( font < 16 )
	{
		retChar = retChar - cBLANK + 1;
	}
	
	return retChar;
}

/*!
** \fn char *String2Font( char *s, ft_uint8_t font )
** \brief Convert a string for the used font
** \param s The string
** \param font The used font
** \return The converted string (for custom RAM font only)
** \note ROM fonts begin from index 32 (space) up to index 127.
** 		Custom fonts begin from index 1 (space) up to index 127.
**/
char *String2Font( char *s, ft_uint8_t font )
{
	static char destStr[MAX_LEN_STRINGA+1];	// used for a ROM string
	char *p = s;
	
	if( font < 16 )
	{
		p = destStr;
		while( *s )
		{
			*p++ = Char2Font( *s++, font );
		}
		*p = '\0';
		p = destStr;
	}
	
	return p;
}

#ifdef BITMAP_TEST

static const ft_uint8_t Bitmap_RawData[] =
{
		/*('file properties: ', 'resolution ', 480, 'x', 95, 'format ', 'ARGB1555', 'stride ', 960, ' total size ', 91200)*/
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,210,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,
		181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,181,214,149,210,90,235,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,218,247,
		41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,254,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,217,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,146,231,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,229,249,229,249,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,228,249,74,250,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,90,235,215,218,247,222,247,222,247,222,247,222,247,222,247,222,247,222,24,227,57,227,25,227,25,227,
		25,227,25,227,25,227,25,227,25,227,25,227,24,227,247,222,25,227,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,25,227,24,227,247,222,182,214,182,214,214,218,214,218,215,218,247,222,247,222,247,222,24,227,57,227,25,227,
		25,227,25,227,25,227,25,227,25,227,25,227,25,227,25,227,25,227,25,227,25,227,25,227,25,227,24,227,91,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,90,235,83,202,116,206,116,206,116,206,116,206,116,206,116,206,116,206,116,206,116,206,116,206,116,206,116,206,116,206,115,206,181,214,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,214,218,207,185,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,
		240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,239,189,207,185,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,183,239,
		42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,113,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,113,231,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,182,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,218,247,111,223,41,215,42,215,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,20,255,195,249,5,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,229,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,115,202,207,185,239,189,239,189,239,189,239,189,239,189,239,189,239,189,239,189,239,189,239,189,239,189,239,189,
		239,189,239,189,239,189,239,189,207,185,174,181,49,194,58,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,189,247,123,239,248,222,148,210,16,194,174,181,206,185,207,185,207,185,239,189,239,189,
		239,189,239,189,239,189,239,189,239,189,239,189,239,189,239,189,239,189,239,189,239,189,239,189,239,189,174,181,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,207,185,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,240,189,239,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,149,210,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,146,231,
		42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,41,215,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,147,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,183,239,76,219,41,215,42,215,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,108,250,195,249,5,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,229,249,195,249,87,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,239,185,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,190,207,185,82,202,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,214,218,49,198,207,185,239,189,
		16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,16,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,115,206,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,
		189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,111,227,
		42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,147,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,112,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,147,231,43,215,42,215,75,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,141,250,195,249,229,249,5,250,6,250,6,250,6,250,6,250,6,250,5,250,228,249,195,249,87,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,247,222,207,185,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,240,189,239,185,58,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,215,218,
		240,189,207,185,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,181,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,
		49,198,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,76,219,
		75,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,78,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,218,247,78,223,41,211,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,86,255,7,250,195,249,228,249,229,249,229,249,228,249,196,249,195,249,107,250,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,148,210,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		239,185,82,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,91,239,82,202,207,185,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,49,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		207,185,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,148,235,42,215,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,114,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,209,250,108,250,74,250,174,250,20,255,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,115,206,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,
		181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,123,239,16,194,239,185,16,194,16,194,16,194,16,194,16,194,16,194,239,189,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,190,16,190,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,42,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,42,215,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,82,202,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,247,222,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,247,222,207,185,240,189,16,194,16,194,16,194,16,190,16,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,239,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,190,240,189,239,189,239,185,239,185,239,185,239,185,239,185,239,185,239,185,207,185,239,189,239,189,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,239,189,215,218,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,219,247,41,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,149,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,235,42,215,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,220,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,115,206,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,247,222,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,189,247,240,189,240,189,16,194,16,194,240,189,148,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,210,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,190,239,189,207,185,16,194,115,206,247,218,58,231,156,243,189,247,222,251,222,251,222,251,156,243,123,239,24,227,181,210,50,198,207,185,239,185,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,206,185,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,183,239,42,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,185,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,210,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,181,214,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,49,198,239,189,16,194,239,189,247,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,239,185,
		240,189,214,218,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,223,251,58,231,82,202,207,185,239,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,239,189,115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,148,235,42,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,149,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,181,235,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,181,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,83,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,16,194,240,189,239,185,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,49,198,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,207,185,82,202,91,235,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,214,218,240,189,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,112,227,42,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,215,75,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,146,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,239,189,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,239,185,173,177,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,58,231,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,49,194,123,239,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,181,214,207,185,240,189,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,240,189,16,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,78,223,43,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,182,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,182,239,42,215,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,110,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,16,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,190,240,189,16,194,16,194,16,190,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,16,190,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,207,185,24,223,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,50,198,207,185,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,207,185,24,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,43,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,75,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,43,215,75,215,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,181,214,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,148,210,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,190,49,198,50,198,240,189,16,194,49,194,16,190,16,190,17,194,148,210,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,214,218,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,17,194,189,247,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,247,222,206,185,16,190,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,190,239,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,183,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,182,239,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,189,247,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,156,243,206,185,149,210,255,255,239,189,239,189,215,218,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,90,235,207,185,240,189,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,189,149,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,220,251,41,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,43,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,253,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,82,202,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,214,218,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,156,243,206,185,148,210,255,255,239,189,239,189,214,218,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,115,206,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,148,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,239,185,240,189,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,206,185,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,184,243,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,183,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,183,239,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,217,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,124,239,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,239,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,124,239,174,181,148,210,255,255,207,185,207,185,182,214,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,148,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,157,243,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,207,185,16,190,
		16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,148,235,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,149,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,50,198,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,182,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,222,251,222,251,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,16,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,83,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,115,206,206,185,206,181,174,181,207,185,215,218,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,124,239,206,185,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,113,227,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,184,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,182,239,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,114,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,254,255,220,251,252,251,252,251,252,251,252,251,252,251,254,255,114,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,78,223,254,255,252,251,
		252,251,252,251,252,251,252,251,252,251,252,251,220,251,253,251,255,255,255,255,219,247,253,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,220,251,253,251,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,156,243,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,247,222,116,206,17,194,240,189,240,189,239,189,239,189,239,189,239,189,240,189,240,189,50,198,149,210,248,222,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,240,189,82,202,116,206,116,206,116,206,115,206,116,206,116,206,115,206,181,214,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,185,16,194,16,194,16,194,16,194,16,194,16,194,240,189,16,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,223,251,
		239,189,240,189,57,231,189,247,189,247,215,218,173,181,83,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,
		207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,190,240,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,75,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,78,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,40,211,41,215,75,219,77,223,111,223,112,227,
		113,231,146,231,146,231,146,231,112,227,111,223,77,223,76,219,42,215,7,211,218,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,7,207,42,215,
		75,219,77,223,110,223,112,227,113,231,146,231,146,231,146,231,112,227,111,223,77,223,76,219,42,215,7,211,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,149,235,40,211,41,215,41,215,41,215,41,215,41,215,41,215,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,41,215,41,215,
		41,215,41,215,41,215,41,215,41,215,41,215,40,211,113,227,255,255,255,255,76,219,40,211,41,215,75,219,77,219,78,223,111,227,112,227,112,227,112,227,111,223,78,223,76,219,42,215,41,215,40,211,147,231,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,148,206,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,24,227,83,202,207,185,207,185,239,189,240,189,240,189,16,190,16,194,16,194,16,194,16,194,16,194,16,190,16,190,240,189,239,189,239,189,207,185,240,189,148,210,123,239,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,240,189,240,189,240,189,240,189,240,189,240,189,240,189,239,189,50,198,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,239,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,207,185,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,82,198,
		16,190,255,255,255,255,255,255,255,255,255,255,156,243,173,181,24,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		115,206,239,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,214,218,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,215,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,184,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,182,239,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,215,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,215,43,215,42,215,
		42,215,42,215,42,215,42,215,42,215,43,215,43,215,75,215,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,215,43,215,42,215,42,215,42,215,42,215,42,215,42,215,43,215,43,215,75,215,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,184,243,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,42,215,149,235,255,255,255,255,77,223,43,215,75,219,75,219,75,215,43,215,42,215,42,215,42,215,42,215,43,215,43,215,75,215,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,90,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,222,251,148,210,239,189,239,185,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,239,189,207,185,16,194,57,231,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,50,198,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,148,210,239,189,16,194,16,194,16,194,16,194,16,194,16,194,239,185,215,218,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,173,177,
		124,239,255,255,255,255,255,255,255,255,255,255,255,255,181,214,207,185,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,215,75,215,75,219,75,219,75,219,75,219,75,219,42,215,78,223,75,215,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,218,247,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,42,215,182,239,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,24,227,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,148,210,207,185,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,207,185,49,198,156,243,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,240,189,82,202,239,189,240,189,16,194,16,194,16,194,240,189,50,198,214,218,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,206,185,16,194,16,194,16,194,16,194,16,194,16,194,240,189,16,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,57,231,206,185,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,174,181,57,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,25,227,207,185,16,194,16,194,16,194,16,194,16,194,16,194,239,189,115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,220,251,42,215,75,219,75,219,
		75,219,75,219,75,219,75,219,76,219,75,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,183,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,182,239,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,219,251,42,215,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,218,247,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,42,215,182,239,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,50,198,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,90,235,
		239,189,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,207,185,207,185,207,185,239,189,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,206,185,247,222,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,255,255,115,206,207,185,240,189,16,194,239,189,215,218,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,82,202,240,189,16,194,16,194,16,194,16,194,16,194,16,194,207,185,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,58,231,206,185,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,115,206,17,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,17,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,206,185,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,184,243,42,215,75,219,75,219,
		75,219,75,219,75,219,42,215,185,243,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,43,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,42,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,182,239,254,255,41,215,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,183,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,218,247,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,42,215,182,239,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,24,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,182,214,206,185,
		16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,207,185,182,214,123,239,157,243,157,243,57,227,17,194,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,
		115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,24,227,255,255,255,255,156,243,49,198,207,185,239,189,214,218,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,90,231,207,185,16,194,16,194,16,194,16,194,16,194,16,194,240,189,17,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,173,181,
		123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,173,177,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,58,231,207,185,16,194,16,194,16,194,16,194,16,194,16,194,240,189,49,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,235,42,215,75,219,75,219,
		75,219,75,219,75,219,42,215,254,255,147,235,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,184,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,148,235,42,215,75,219,75,219,75,219,75,219,75,219,75,219,43,215,255,255,255,255,41,211,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,147,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,185,243,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,42,215,149,235,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,90,235,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,82,202,239,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,148,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,210,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		239,189,83,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,123,239,239,189,123,239,255,255,90,231,207,185,181,214,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,239,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,239,185,24,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,50,198,
		50,198,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,82,198,49,198,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,239,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,207,185,24,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,113,231,42,215,75,219,75,219,
		75,219,75,219,75,219,42,215,255,255,255,255,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,77,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,42,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,181,239,255,255,255,255,42,215,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,111,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,149,235,40,211,41,215,41,215,41,215,41,215,41,215,41,215,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,41,215,41,215,
		41,215,41,215,41,215,41,215,41,215,41,215,40,211,113,227,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,181,214,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,239,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,83,202,239,189,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,115,206,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,239,185,181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,156,243,207,185,206,185,83,202,222,251,255,255,123,239,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,214,218,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,190,240,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,
		173,181,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,173,181,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,214,218,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,190,239,185,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,78,223,43,215,75,219,75,219,
		75,219,75,219,75,219,43,215,255,255,255,255,146,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,184,243,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,148,235,42,215,75,219,75,219,75,219,75,219,75,219,75,215,75,215,255,255,255,255,255,255,76,219,75,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,220,251,217,243,217,243,217,243,217,243,217,243,217,243,218,247,112,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,78,223,219,247,217,243,
		217,243,217,243,217,243,217,243,217,243,217,243,217,243,219,247,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,50,198,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,115,202,239,189,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,206,185,57,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,156,239,239,185,16,194,240,189,206,185,181,214,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,222,251,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		240,189,148,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,16,194,116,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,189,247,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,76,219,75,215,75,219,75,219,
		75,219,75,219,43,215,78,223,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,75,219,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,42,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,182,239,255,255,255,255,255,255,78,223,43,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,247,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,247,222,207,185,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,206,185,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,214,218,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,190,239,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,16,194,16,194,16,194,239,189,239,185,24,227,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,83,202,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,58,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		57,231,173,181,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,58,231,173,181,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,49,198,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,215,75,219,75,219,75,219,
		75,219,75,219,42,215,113,227,255,255,255,255,255,255,146,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,184,243,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,215,75,219,255,255,255,255,255,255,255,255,112,227,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,189,247,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,156,243,207,185,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,148,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,239,189,148,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,16,194,16,194,16,194,16,194,16,194,240,189,50,198,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,156,239,206,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,206,185,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,239,185,181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,239,189,181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,149,210,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,220,251,41,215,75,219,75,219,75,219,
		75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,42,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,182,239,255,255,255,255,255,255,255,255,146,231,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,183,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,43,215,42,215,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,90,235,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,16,194,240,189,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,82,202,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,207,185,222,251,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,240,189,239,189,239,189,239,189,239,189,239,189,239,189,207,185,50,198,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,17,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,16,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,247,222,206,181,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,50,198,173,181,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,57,231,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,184,243,42,215,75,219,75,219,75,219,
		75,219,75,219,42,215,183,243,255,255,255,255,255,255,255,255,113,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,184,243,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,146,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,255,255,255,255,255,255,255,255,255,255,148,235,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,147,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,42,215,220,251,183,239,42,215,42,215,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,247,222,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,247,222,239,185,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,240,189,17,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,248,222,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,239,189,116,206,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,240,189,148,210,24,227,24,223,24,223,24,223,24,223,247,222,247,222,57,231,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,24,227,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,206,185,247,222,255,255,255,255,255,255,255,255,90,235,50,198,173,181,16,194,57,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,156,243,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,16,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,235,42,215,75,219,75,219,75,219,
		75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,253,251,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,75,219,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,42,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,149,235,255,255,255,255,255,255,255,255,255,255,182,239,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,111,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,42,215,112,227,255,255,255,255,146,231,41,215,42,215,42,215,41,211,147,235,255,255,255,255,255,255,255,255,255,255,
		255,255,181,214,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,17,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,240,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,239,189,181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,189,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,247,218,189,247,189,243,189,243,189,243,123,239,255,255,255,255,214,218,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,182,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,182,214,239,185,255,255,255,255,25,227,207,185,173,181,83,202,124,239,255,255,255,255,119,255,120,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,215,218,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,113,231,42,215,75,219,75,219,75,219,
		75,219,75,219,41,215,254,255,255,255,255,255,255,255,255,255,255,255,113,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,183,239,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,112,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,43,215,255,255,255,255,255,255,255,255,255,255,255,255,217,243,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,41,215,147,231,255,255,253,251,149,235,182,239,149,235,218,247,255,255,255,255,255,255,255,255,255,255,
		255,255,115,206,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,50,198,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,239,185,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,185,24,227,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,207,185,207,185,207,185,173,181,24,223,255,255,82,202,49,198,255,255,255,255,255,255,
		255,255,255,255,255,255,181,214,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,248,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,222,251,174,181,116,206,239,185,173,181,148,210,190,247,255,255,255,255,53,255,40,250,194,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,255,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,78,223,43,215,75,219,75,219,75,219,
		75,219,75,219,42,215,255,255,255,255,255,255,255,255,255,255,255,255,254,255,41,211,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,41,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,149,235,255,255,255,255,255,255,255,255,255,255,255,255,219,247,41,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,215,41,215,77,223,254,255,255,255,219,247,220,251,219,247,253,251,255,255,255,255,255,255,255,255,255,255,
		255,255,50,198,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,83,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,16,194,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,206,185,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,240,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,240,189,115,206,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,206,185,123,239,255,255,82,202,207,185,82,202,255,255,255,255,255,255,
		255,255,255,255,156,243,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,57,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,116,206,207,185,215,218,255,255,255,255,255,255,242,254,228,249,195,249,229,249,5,250,195,249,153,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,215,75,219,75,219,75,219,75,219,
		75,219,75,215,77,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,146,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,183,239,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,112,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,43,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,220,251,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,215,77,219,220,251,255,255,183,243,42,215,41,215,41,215,40,211,147,231,255,255,255,255,255,255,255,255,255,255,
		255,255,49,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,156,243,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,207,185,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,82,202,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,239,189,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,207,185,123,239,255,255,49,198,239,189,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,50,198,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,25,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,222,251,255,255,255,255,254,255,174,250,228,249,196,249,229,249,6,250,6,250,6,250,229,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,206,185,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,
		75,219,43,215,110,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,43,215,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,41,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,181,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,183,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,41,215,220,251,220,251,77,223,41,215,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,240,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,148,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,181,214,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,240,189,49,198,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,210,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,207,185,156,243,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,240,189,207,185,156,243,255,255,17,194,207,185,239,189,239,185,50,198,255,255,255,255,255,255,
		255,255,255,255,24,223,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,25,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,153,255,107,250,194,249,228,249,229,249,6,250,6,250,6,250,6,250,6,250,5,250,195,249,188,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,16,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,219,247,41,215,75,219,75,219,75,219,75,219,
		75,219,42,215,113,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,113,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,183,239,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,110,223,42,215,75,219,75,219,75,219,75,219,75,219,75,219,43,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,146,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,215,77,223,41,215,43,215,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,240,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,17,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,240,189,148,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,214,218,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,239,189,24,227,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,190,240,189,189,247,255,255,248,222,182,214,214,218,214,218,214,218,248,222,255,255,255,255,255,255,
		255,255,255,255,255,255,17,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,57,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,73,250,196,249,5,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,228,249,173,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,90,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,184,243,42,215,75,219,75,219,75,219,75,219,
		75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,41,211,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,220,251,41,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,77,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,215,75,219,75,219,75,219,75,219,75,219,75,219,41,215,147,235,255,255,255,255,255,255,255,255,255,255,
		255,255,240,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,189,182,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,247,222,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,239,189,181,214,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,240,189,90,235,189,247,157,243,189,247,190,247,190,247,190,247,190,247,222,251,255,255,255,255,255,255,
		255,255,255,255,255,255,156,239,206,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,214,218,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,187,255,195,249,5,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,5,250,195,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,50,198,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,148,235,42,215,75,219,75,219,75,219,75,219,
		75,219,42,215,183,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,113,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,183,239,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,110,223,42,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,76,219,75,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,185,243,255,255,255,255,255,255,255,255,255,255,
		255,255,240,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,57,231,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,189,247,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,57,231,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,207,185,207,185,207,185,207,185,207,185,207,185,206,185,49,198,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,83,202,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,149,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,73,250,229,249,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,5,250,228,249,240,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,189,247,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,157,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,113,227,42,215,75,219,75,219,75,219,75,219,
		75,219,41,215,218,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,43,215,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,218,247,41,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,149,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,78,223,43,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,75,215,75,215,75,215,75,215,41,215,183,239,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,17,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,148,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,156,243,148,210,215,218,
		24,227,90,235,124,239,157,243,222,251,254,251,254,251,255,251,222,251,157,243,124,239,90,235,24,227,215,218,148,210,156,243,255,255,255,255,255,255,255,255,255,255,214,218,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,189,57,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,57,231,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,190,16,194,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,222,251,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,50,198,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,120,255,195,249,5,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,229,249,229,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,123,235,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,149,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,
		75,219,42,215,252,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,113,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,
		182,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,219,43,215,75,219,75,219,75,219,75,219,75,219,75,219,43,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,112,227,42,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,217,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,76,219,76,219,76,219,76,219,76,219,76,219,42,215,183,239,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,50,198,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,206,185,239,189,
		239,189,239,185,207,185,207,185,207,185,206,185,206,185,206,185,207,185,207,185,207,185,239,185,239,189,239,189,206,185,123,239,255,255,255,255,255,255,255,255,255,255,115,206,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,185,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,90,231,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,190,240,189,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,239,189,16,194,240,189,239,189,16,194,16,194,239,189,49,198,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,214,218,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,7,250,229,249,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,195,249,52,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,247,218,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,207,185,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,42,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,
		75,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,218,247,41,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,146,231,42,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,149,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,42,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,115,206,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,49,198,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,189,215,218,123,239,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,123,239,247,222,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,247,222,57,231,207,185,115,206,90,235,240,189,239,189,181,210,189,247,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,239,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,53,255,195,249,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,229,249,229,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,82,202,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,24,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,75,219,75,219,75,219,75,219,
		75,219,42,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,113,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		41,215,182,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,76,219,43,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,148,235,42,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,112,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,215,77,219,77,219,77,219,77,219,77,219,77,219,77,219,76,219,149,235,255,255,255,255,255,255,255,255,255,255,
		255,255,181,214,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,50,198,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,240,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,239,189,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,156,243,206,185,149,210,255,255,239,189,239,189,214,218,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,57,231,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,214,218,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,229,249,229,249,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,5,250,195,249,86,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,17,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,219,247,41,215,75,219,75,219,75,219,75,219,75,219,
		75,215,76,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,220,251,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,42,215,255,255,255,255,255,255,255,255,255,255,255,255,185,243,41,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,182,239,42,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,77,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,215,75,215,75,215,75,215,75,215,75,215,75,215,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,248,222,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,16,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,206,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,123,239,173,181,148,206,255,255,206,185,206,185,182,214,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,82,202,240,189,16,194,16,194,16,194,16,194,16,194,16,194,240,189,17,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,242,254,228,249,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,229,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,57,231,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,206,185,156,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,183,239,42,215,75,219,75,219,75,219,75,219,75,219,
		42,215,111,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,112,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,76,219,75,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,184,243,42,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,254,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,147,235,255,255,255,255,255,255,255,255,255,255,
		255,255,90,235,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,206,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,222,251,214,218,90,235,255,255,247,222,247,222,123,239,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,156,243,206,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,228,249,229,249,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,5,250,228,249,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,82,198,240,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,
		42,215,146,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,251,41,211,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,215,255,255,255,255,255,255,255,255,183,239,42,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,147,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,218,247,41,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,220,251,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,41,215,43,215,217,243,255,255,255,255,255,255,255,255,255,255,
		255,255,222,251,206,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,222,251,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,214,218,156,243,157,243,156,243,156,239,157,243,157,243,156,243,156,243,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,148,210,239,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,148,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,175,254,228,249,6,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,5,250,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,189,247,206,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,111,223,42,215,75,219,75,219,75,219,75,219,75,219,
		42,215,149,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,112,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,42,215,148,235,255,255,255,255,255,255,75,219,75,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,219,247,41,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,183,239,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,41,215,78,223,185,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,16,194,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,57,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,222,251,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,239,189,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,
		207,185,207,185,207,185,207,185,207,185,207,185,207,185,206,185,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,207,185,207,185,207,185,207,185,207,185,207,185,206,185,49,198,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,239,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,207,185,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,195,249,5,250,6,250,6,250,6,250,6,250,6,250,6,250,6,250,229,249,229,249,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,148,210,239,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,214,218,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,215,75,219,75,219,75,219,75,219,75,219,
		42,215,183,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,42,215,255,255,255,255,183,239,41,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,147,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,41,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,146,231,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,41,215,112,227,220,251,255,255,254,255,78,223,147,231,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,115,206,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,206,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,189,24,227,156,243,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,
		123,239,123,239,123,239,123,239,123,239,123,239,123,239,123,239,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,185,16,194,16,194,16,194,16,194,16,194,16,194,239,189,148,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,140,250,228,249,6,250,6,250,6,250,6,250,6,250,6,250,6,250,228,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,189,247,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,190,239,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,42,215,75,219,75,219,75,219,75,219,75,219,75,219,
		41,215,218,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,113,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,42,215,148,235,255,255,43,215,75,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,78,223,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,43,215,147,231,254,251,255,255,182,239,217,243,217,243,40,211,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,17,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,206,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,207,185,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,239,189,239,189,16,190,16,194,16,194,239,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,16,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,207,185,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,221,255,228,249,229,249,6,250,6,250,6,250,6,250,6,250,229,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,82,202,240,189,16,194,16,194,16,194,16,194,16,194,16,194,207,185,90,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,220,251,42,215,75,219,75,219,75,219,75,219,75,219,75,219,
		41,215,220,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,41,211,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,76,219,149,235,42,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,146,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,42,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,255,255,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,43,215,185,243,255,255,255,255,77,223,7,207,183,239,218,247,40,211,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,189,247,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,254,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,240,189,17,194,24,227,247,222,240,189,240,189,207,185,116,206,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,16,194,16,194,16,194,16,194,16,194,240,189,17,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,153,255,195,249,228,249,229,249,229,249,229,249,228,249,228,249,120,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		57,231,207,185,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,198,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,217,243,41,215,75,219,75,219,75,219,75,219,75,219,75,219,
		41,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,112,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,253,251,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,41,215,77,219,183,239,255,255,220,251,219,247,217,243,39,211,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,16,194,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,17,194,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,185,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,240,189,17,194,255,255,222,251,222,251,255,251,207,185,90,235,255,255,57,231,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,115,206,240,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,208,254,39,250,229,249,7,250,207,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,
		239,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,206,185,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,235,42,215,75,219,75,219,75,219,75,219,75,219,75,219,
		43,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,220,251,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,113,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,219,
		75,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,217,243,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,42,215,43,215,182,239,255,255,255,255,146,231,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,247,222,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,148,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,82,202,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,189,57,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,247,222,255,255,173,181,174,181,222,251,255,255,255,255,49,198,17,194,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,206,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,57,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,17,194,
		240,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,149,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,113,227,42,215,75,219,75,219,75,219,75,219,75,219,75,215,
		76,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,112,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,253,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,78,223,
		43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,211,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,42,215,42,215,148,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,222,251,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,239,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,149,210,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,189,247,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,156,243,16,194,16,194,157,243,156,239,240,189,16,194,115,206,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,214,218,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,115,206,239,189,
		16,194,16,194,16,194,16,194,16,194,16,194,16,190,239,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,78,223,43,215,75,219,75,219,75,219,75,219,75,219,43,215,
		78,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,41,211,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,113,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,111,223,
		42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,111,227,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,41,215,183,239,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,115,206,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,57,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,248,222,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,239,189,181,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,24,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,240,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,190,207,185,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,83,202,239,189,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,207,185,24,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,
		111,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,112,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,211,253,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,113,227,
		42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,41,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,124,239,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,17,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,190,17,194,17,194,17,194,17,194,17,194,17,194,17,194,16,194,83,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,57,231,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,115,202,239,189,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,240,189,16,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,41,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,
		113,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,252,251,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,112,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,
		42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,254,255,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,184,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,219,251,41,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,76,219,76,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,82,202,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,222,251,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,207,185,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,190,16,190,16,190,16,190,16,190,16,190,16,190,239,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,49,198,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,207,185,24,227,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,17,194,239,189,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,218,247,41,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,
		147,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,111,227,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,220,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,148,235,
		42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,149,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,217,243,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,41,215,220,251,184,243,41,215,42,215,42,215,42,215,42,215,41,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,156,243,206,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,50,198,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,17,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,207,185,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,240,189,239,189,239,189,239,189,239,189,239,189,239,189,239,189,50,198,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,115,206,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,58,231,239,185,240,189,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,240,189,115,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,182,239,42,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,
		182,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,41,211,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,111,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,182,239,
		42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,183,239,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,114,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,235,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,41,215,218,247,185,243,111,223,112,227,112,227,112,227,112,227,111,223,182,239,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,149,210,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,58,231,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,181,214,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,239,189,24,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,240,189,115,202,181,214,181,214,181,214,181,214,181,214,181,214,149,210,215,218,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,148,210,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,240,189,57,231,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,115,206,207,185,16,190,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,206,185,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,146,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,
		183,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,111,223,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,211,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,184,243,
		42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,146,231,255,255,255,255,255,255,255,255,254,255,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,77,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,112,227,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,146,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,239,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,239,189,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,24,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,207,185,17,194,90,235,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,181,214,206,185,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,239,189,214,218,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,78,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,41,215,
		218,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,252,251,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,110,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,217,243,
		41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,77,223,255,255,255,255,255,255,255,255,255,255,76,219,75,215,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,235,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,41,215,219,247,182,239,40,211,41,215,41,215,41,215,41,215,40,211,147,231,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,235,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,116,206,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,240,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,190,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,239,189,207,185,207,185,207,185,207,185,207,185,207,185,206,185,49,198,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,247,222,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,207,185,49,194,
		24,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,148,210,239,185,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,190,240,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,
		219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,110,223,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,219,247,
		41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,255,255,255,255,255,255,255,255,255,255,77,219,75,215,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,181,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,219,247,41,215,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,235,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,42,215,185,243,149,235,42,215,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,181,214,239,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,24,227,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,214,218,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,214,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,190,16,190,16,194,16,194,16,190,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,17,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,
		239,189,206,185,50,198,247,222,123,239,255,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,57,231,148,210,239,189,207,185,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,207,185,90,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,251,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,
		252,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,220,251,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,77,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,252,251,
		42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,254,255,255,255,255,255,255,255,255,255,78,223,43,215,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,75,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,112,227,42,215,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,183,239,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,42,215,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,82,202,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,206,185,123,239,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,206,185,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,190,17,194,17,194,16,190,16,194,16,194,16,190,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,240,189,239,189,207,185,239,189,17,194,82,198,83,202,148,210,148,210,116,206,82,202,49,198,16,190,207,185,239,185,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,240,189,49,198,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,219,247,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,
		254,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,78,223,42,215,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,185,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,
		42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,217,243,255,255,255,255,255,255,255,255,114,231,42,215,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,41,215,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,217,247,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,111,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,16,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,207,185,123,239,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,116,206,239,189,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,116,206,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,156,243,206,185,148,210,255,251,239,189,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,83,202,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,190,240,189,240,189,240,189,240,189,240,189,240,189,240,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,207,185,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,182,239,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,219,247,41,215,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,
		43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,149,235,255,255,255,255,255,255,255,255,183,243,42,215,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,76,219,43,215,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,220,251,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,77,223,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,42,215,42,215,42,215,42,215,42,215,42,215,41,211,147,231,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,16,194,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,206,185,90,235,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,206,185,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,24,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,156,243,174,181,148,210,255,255,239,185,240,189,239,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		239,189,148,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,146,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,78,223,43,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,41,215,184,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,112,227,255,255,255,255,255,255,255,255,253,251,41,215,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,78,223,42,215,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,42,215,149,235,185,243,184,243,184,243,184,243,184,243,184,243,184,243,219,247,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,223,255,240,189,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,214,218,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,214,218,239,185,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,206,185,156,243,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,83,202,174,181,214,218,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,57,231,156,243,16,194,214,218,255,255,49,198,50,198,50,198,148,206,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,214,218,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		207,185,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,111,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,219,247,41,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		75,219,75,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,77,219,255,255,255,255,255,255,255,255,255,255,76,219,75,215,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,211,147,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,183,243,42,215,42,215,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,76,219,75,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,254,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,42,215,149,235,185,243,184,243,184,243,184,243,184,243,184,243,184,243,219,247,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,49,198,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,185,
		49,198,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,207,185,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,49,194,240,189,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,206,185,57,231,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,25,227,207,185,240,189,239,185,247,222,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,239,189,24,227,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,239,189,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,
		24,223,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,76,219,75,215,75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,76,219,43,215,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,42,215,182,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		76,219,75,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,255,255,255,255,255,255,255,255,255,255,181,239,42,215,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,42,215,112,227,149,235,184,243,183,239,148,235,112,227,75,219,41,215,75,219,75,219,75,219,75,219,75,215,42,215,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,146,231,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,184,243,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,42,215,42,215,42,215,42,215,42,215,42,215,42,215,41,211,147,231,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,116,206,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		240,189,207,185,182,214,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,247,222,239,189,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,206,185,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,149,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,90,235,
		49,194,207,185,16,194,16,194,207,185,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,58,231,207,185,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,17,194,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,77,223,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,218,247,41,215,75,219,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,42,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		78,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,253,251,255,255,255,255,255,255,255,255,255,255,43,215,75,215,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,42,215,42,215,42,215,42,215,42,215,75,219,75,219,75,219,75,219,75,219,42,215,77,223,182,239,42,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,218,247,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,215,76,219,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,77,223,43,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,207,185,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,239,189,206,185,182,214,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,57,231,16,194,239,185,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,90,235,206,185,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,207,185,215,218,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,247,218,240,189,207,185,
		240,189,16,194,16,194,239,189,148,210,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,190,16,190,16,190,16,190,16,190,16,190,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,50,198,240,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,206,185,123,239,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,218,247,41,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,43,215,78,223,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,76,219,75,215,75,219,75,219,75,219,
		75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		111,223,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,217,243,255,255,255,255,255,255,255,255,255,255,182,239,41,215,75,219,75,219,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,76,219,255,255,183,239,42,215,
		75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,41,215,218,247,255,255,255,255,255,255,255,255,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,
		75,219,75,219,41,215,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,75,215,75,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,111,227,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,219,247,255,255,77,223,42,215,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,75,219,42,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,49,198,207,185,16,190,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,239,189,207,185,50,198,247,222,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,123,239,181,214,16,190,207,185,240,189,16,194,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,207,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,57,231,206,185,16,194,16,194,
		16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,239,189,239,185,49,198,182,214,57,231,156,239,189,247,189,247,189,247,123,239,24,227,181,210,49,194,207,185,239,189,16,190,16,194,
		16,194,16,194,16,194,239,185,123,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,16,194,240,189,82,202,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,157,243,174,181,206,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,
		207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,206,181,83,202,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,235,40,211,41,215,41,215,41,215,41,215,41,215,41,215,41,215,40,211,110,223,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,185,243,39,211,41,215,41,215,41,215,
		41,215,41,215,41,215,41,215,41,215,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		113,227,42,215,42,215,42,215,42,215,42,215,42,215,42,215,42,215,42,215,42,215,42,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,40,211,147,231,255,255,255,255,255,255,255,255,255,255,255,255,111,227,40,211,41,215,
		41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,211,76,219,255,255,255,255,181,235,40,211,
		41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,39,211,218,247,255,255,255,255,255,255,255,255,42,215,41,211,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,
		41,215,41,215,7,211,219,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,149,235,40,211,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,41,215,40,211,
		7,211,78,223,113,231,146,231,112,227,77,219,41,211,6,207,113,227,255,255,77,219,42,215,42,215,42,215,42,215,42,215,42,215,42,215,42,215,42,215,42,215,42,215,42,215,42,215,42,215,41,215,148,235,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,239,189,206,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,
		207,185,207,185,207,185,207,185,207,185,206,185,206,185,174,181,173,177,141,177,16,194,115,206,214,218,24,227,57,231,123,239,123,239,123,239,57,231,25,227,214,218,116,206,16,194,141,177,173,181,174,181,206,185,207,185,207,185,207,185,207,185,207,185,
		207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,173,181,91,235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,57,231,206,185,207,185,
		207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,207,185,206,185,206,181,174,181,173,181,173,181,173,181,173,181,173,181,174,181,206,185,206,185,207,185,207,185,207,185,207,185,
		207,185,207,185,206,185,240,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,24,227,174,181,239,185,239,185,239,185,239,185,239,185,239,185,239,185,239,185,239,185,239,185,239,185,239,185,239,185,207,185,49,198,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,
		222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,251,254,255,254,255,254,255,
		254,255,254,255,254,255,254,255,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,251,254,255,
		254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,255,255,255,255,255,255,255,255,254,251,
		254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,253,251,255,255,255,255,255,255,255,255,255,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,
		254,255,254,255,253,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,251,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,254,255,
		254,255,253,251,253,251,253,251,253,251,253,251,254,251,254,255,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,
		222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,189,247,189,247,189,247,189,247,189,247,189,247,189,247,189,247,189,247,189,247,189,247,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,
		222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,222,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,156,243,
		156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,156,243,
		156,243,156,243,156,243,189,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,189,247,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,90,235,123,239,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,188,255,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,153,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,154,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,242,254,19,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,241,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,20,255,241,254,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,187,255,174,250,40,250,162,249,194,249,229,249,107,250,87,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,255,242,254,40,250,195,249,194,249,195,249,73,250,
		19,255,255,255,255,255,255,255,255,255,20,255,140,250,175,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,140,250,140,250,153,255,255,255,255,255,255,255,53,255,140,250,174,250,255,255,255,255,255,255,221,255,7,250,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,188,255,108,250,140,250,188,255,255,255,255,255,255,255,242,254,141,250,141,250,141,250,141,250,141,250,141,250,141,250,140,250,154,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,120,255,140,250,141,250,141,250,141,250,241,250,87,255,255,255,255,255,255,255,255,255,255,255,208,254,141,250,141,250,141,250,141,250,141,250,141,250,141,250,108,250,188,255,255,255,255,255,154,255,140,250,141,250,141,250,141,250,19,255,121,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,187,255,108,250,108,250,221,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,6,250,19,255,255,255,255,255,255,255,20,255,140,250,175,250,
		255,255,255,255,255,255,255,255,39,250,187,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,175,250,140,250,52,255,255,255,255,255,255,255,241,254,141,250,141,250,141,250,141,250,175,250,53,255,187,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,242,254,140,250,208,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,107,250,140,250,120,255,255,255,255,255,255,255,255,255,255,255,86,255,73,250,194,249,162,249,39,250,20,255,255,255,255,255,
		255,255,254,255,140,250,141,250,141,250,141,250,141,250,141,250,141,250,141,250,141,250,175,250,255,255,255,255,141,250,141,250,141,250,141,250,208,254,86,255,222,255,255,255,255,255,255,255,255,255,255,255,255,255,242,254,140,250,209,254,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,207,250,86,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,140,250,141,250,141,250,141,250,141,250,141,250,141,250,
		209,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,140,250,121,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,174,250,141,250,141,250,141,250,207,250,53,255,222,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,221,255,40,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,39,250,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,155,255,174,250,40,250,162,249,194,249,229,249,107,250,119,255,255,255,255,255,255,255,155,255,140,250,141,250,141,250,141,250,141,250,141,250,141,250,141,250,241,254,255,255,255,255,255,255,
		175,250,140,250,52,255,255,255,255,255,255,255,255,255,255,255,255,255,188,255,108,250,108,250,220,255,255,255,86,255,140,250,141,250,141,250,141,250,141,250,141,250,141,250,141,250,140,250,119,255,255,255,255,255,141,250,140,250,87,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,155,255,174,250,40,250,162,249,194,249,229,249,107,250,119,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,188,255,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,174,250,229,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,40,250,140,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,5,250,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,141,250,195,249,228,249,195,249,195,249,195,249,196,249,228,249,194,249,208,250,255,255,255,255,255,255,255,255,255,255,20,255,195,249,196,249,196,249,195,249,195,249,196,249,228,249,
		195,249,40,250,255,255,255,255,255,255,174,250,195,249,6,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,195,249,194,249,86,255,255,255,255,255,255,255,208,250,195,249,229,249,255,255,255,255,255,255,222,255,161,249,107,250,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,155,255,162,249,194,249,154,255,255,255,255,255,255,255,108,250,195,249,228,249,196,249,196,249,196,249,196,249,196,249,161,249,86,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,195,249,228,249,196,249,196,249,196,249,195,249,7,250,221,255,255,255,255,255,255,255,73,250,196,249,228,249,196,249,196,249,196,249,196,249,196,249,161,249,154,255,255,255,255,255,87,255,194,249,228,249,196,249,196,249,195,249,195,249,
		6,250,187,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,153,255,194,249,162,249,188,255,255,255,255,255,255,255,255,255,255,255,255,255,140,250,195,249,74,250,255,255,255,255,255,255,174,250,195,249,6,250,
		255,255,255,255,255,255,255,255,73,250,228,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,195,249,175,250,255,255,255,255,255,255,74,250,196,249,228,249,196,249,196,249,196,249,195,249,195,249,40,250,187,255,
		255,255,255,255,255,255,255,255,255,255,255,255,108,250,195,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,161,249,194,249,53,255,255,255,255,255,255,255,255,255,208,254,195,249,228,249,196,249,195,249,228,249,195,249,141,250,255,255,
		255,255,222,255,194,249,195,249,196,249,228,249,228,249,228,249,196,249,196,249,195,249,6,250,255,255,255,255,228,249,228,249,228,249,196,249,195,249,195,249,228,249,242,254,255,255,255,255,255,255,255,255,255,255,107,250,195,249,73,250,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,195,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,194,249,228,249,196,249,196,249,196,249,196,249,195,249,
		41,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,195,249,106,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,229,249,228,249,228,249,196,249,195,249,195,249,196,249,209,254,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,154,255,128,249,187,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,20,255,196,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,242,254,161,249,153,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,141,250,195,249,228,249,195,249,195,249,195,249,196,249,228,249,194,249,242,254,255,255,255,255,120,255,194,249,228,249,196,249,196,249,196,249,196,249,196,249,195,249,73,250,255,255,255,255,255,255,
		7,250,195,249,175,250,255,255,255,255,255,255,255,255,255,255,255,255,155,255,162,249,162,249,187,255,255,255,209,254,162,249,196,249,196,249,228,249,228,249,228,249,196,249,196,249,162,249,19,255,255,255,255,255,196,249,194,249,242,254,255,255,255,255,
		255,255,255,255,255,255,255,255,141,250,195,249,228,249,195,249,195,249,195,249,196,249,228,249,194,249,242,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,175,250,161,249,188,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,40,250,194,249,153,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,153,255,194,249,40,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,87,255,195,249,195,249,87,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,73,250,195,249,196,249,6,250,241,254,120,255,120,255,19,255,73,250,161,249,208,250,255,255,255,255,255,255,255,255,207,250,195,249,228,249,228,249,175,250,87,255,120,255,53,255,107,250,
		195,249,39,250,255,255,255,255,255,255,174,250,228,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,228,249,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,228,249,74,250,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,187,255,195,249,196,249,154,255,255,255,255,255,255,255,141,250,228,249,6,250,108,250,107,250,107,250,107,250,107,250,74,250,121,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,229,249,108,250,108,250,7,250,195,249,228,249,229,249,255,255,255,255,255,255,74,250,228,249,39,250,108,250,107,250,107,250,107,250,107,250,73,250,187,255,255,255,255,255,120,255,195,249,229,249,107,250,108,250,40,250,195,249,
		229,249,196,249,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,194,249,221,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,228,249,228,249,220,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,39,250,228,249,207,250,255,255,255,255,255,255,107,250,228,249,7,250,108,250,107,250,73,250,228,249,195,249,228,249,194,249,
		53,255,255,255,255,255,255,255,255,255,255,255,141,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,196,249,53,255,255,255,255,255,255,255,120,255,194,249,229,249,40,250,86,255,121,255,141,250,196,249,161,249,53,255,
		255,255,254,255,74,250,107,250,107,250,74,250,229,249,229,249,107,250,107,250,107,250,141,250,255,255,255,255,229,249,229,249,40,250,108,250,74,250,228,249,228,249,194,249,20,255,255,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,20,255,195,249,195,249,154,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,229,249,108,250,107,250,107,250,107,250,106,250,
		175,250,255,255,255,255,255,255,255,255,255,255,255,255,209,254,228,249,195,249,188,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,229,249,40,250,108,250,74,250,228,249,228,249,194,249,19,255,255,255,255,255,255,255,255,255,
		255,255,255,255,20,255,195,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,195,249,188,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,228,249,228,249,74,250,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,40,250,196,249,196,249,6,250,242,254,120,255,120,255,18,255,73,250,161,249,241,254,255,255,255,255,121,255,195,249,229,249,107,250,107,250,107,250,107,250,107,250,106,250,207,250,255,255,255,255,255,255,
		40,250,228,249,207,250,255,255,255,255,255,255,255,255,255,255,255,255,187,255,195,249,195,249,187,255,255,255,53,255,74,250,107,250,108,250,6,250,229,249,7,250,108,250,107,250,74,250,86,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		255,255,255,255,255,255,40,250,196,249,196,249,6,250,242,254,120,255,120,255,18,255,73,250,161,249,209,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,195,249,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,195,249,229,249,74,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,74,250,228,249,5,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,250,229,249,229,249,40,250,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,141,250,196,249,195,249,207,250,255,255,255,255,255,255,255,255,255,255,255,255,86,255,208,250,255,255,255,255,255,255,53,255,195,249,228,249,74,250,255,255,255,255,255,255,255,255,255,255,255,255,
		188,255,74,250,255,255,255,255,255,255,174,250,228,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,228,249,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,6,250,196,249,107,250,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,187,255,195,249,196,249,154,255,255,255,255,255,255,255,141,250,228,249,74,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,194,249,255,255,255,255,255,255,53,255,228,249,195,249,20,255,255,255,255,255,74,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,255,255,255,255,255,255,120,255,
		228,249,196,249,209,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,242,254,195,249,140,250,255,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,229,249,229,249,229,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,39,250,228,249,207,250,255,255,255,255,255,255,107,250,228,249,140,250,255,255,255,255,255,255,255,255,52,255,228,249,229,249,
		195,249,120,255,255,255,255,255,255,255,255,255,141,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,196,249,53,255,255,255,255,255,255,255,141,250,228,249,6,250,255,255,255,255,255,255,255,255,141,250,53,255,255,255,
		255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,255,255,40,250,228,249,229,249,255,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,6,250,229,249,228,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,228,249,229,249,228,249,174,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,228,249,242,254,255,255,255,255,255,255,73,250,229,249,228,249,255,255,255,255,255,255,255,255,
		255,255,255,255,141,250,228,249,228,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,86,255,195,249,195,249,86,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,20,255,196,249,229,249,196,249,187,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,140,250,228,249,195,249,208,250,255,255,255,255,255,255,255,255,255,255,255,255,53,255,241,254,255,255,255,255,121,255,195,249,195,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		40,250,228,249,207,250,255,255,255,255,255,255,255,255,255,255,255,255,187,255,195,249,195,249,187,255,255,255,255,255,255,255,255,255,255,255,107,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		255,255,255,255,140,250,228,249,195,249,208,250,255,255,255,255,255,255,255,255,255,255,255,255,53,255,241,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,209,250,228,249,229,249,228,249,222,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,187,255,195,249,5,250,196,249,187,255,255,255,255,255,255,255,255,255,255,255,188,255,195,249,5,250,228,249,188,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,153,255,195,249,229,249,229,249,195,249,121,255,
		255,255,255,255,255,255,255,255,255,255,255,255,153,255,194,249,228,249,241,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,228,249,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,228,249,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,6,250,229,249,196,249,
		107,250,255,255,255,255,255,255,255,255,255,255,255,255,187,255,195,249,196,249,154,255,255,255,255,255,255,255,141,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,74,250,228,249,108,250,255,255,255,255,74,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,255,255,255,255,255,255,255,255,
		140,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,6,250,195,249,220,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,229,249,229,249,229,249,228,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,39,250,228,249,207,250,255,255,255,255,255,255,107,250,228,249,107,250,255,255,255,255,255,255,255,255,255,255,222,255,229,249,
		229,249,228,249,255,255,255,255,255,255,255,255,141,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,196,249,53,255,255,255,255,255,255,255,107,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,242,254,255,255,255,255,255,255,154,255,195,249,228,249,188,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,54,255,195,249,229,249,229,249,195,249,188,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,242,254,196,249,229,249,229,249,196,249,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,228,249,241,254,255,255,255,255,255,255,155,255,195,249,195,249,188,255,255,255,255,255,255,255,
		255,255,255,255,39,250,229,249,228,249,208,250,255,255,255,255,255,255,255,255,255,255,255,255,7,250,229,249,228,249,209,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,229,249,229,249,228,249,141,250,255,255,255,255,
		255,255,255,255,255,255,255,255,120,255,195,249,196,249,242,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,195,249,195,249,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		40,250,228,249,207,250,255,255,255,255,255,255,255,255,255,255,255,255,187,255,195,249,195,249,187,255,255,255,255,255,255,255,255,255,255,255,106,250,228,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		255,255,120,255,195,249,196,249,242,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,229,249,229,249,229,249,228,249,208,250,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,53,255,196,249,5,250,228,249,174,250,255,255,255,255,255,255,255,255,255,255,141,250,228,249,5,250,195,249,87,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,73,250,196,249,175,250,175,250,195,249,107,250,
		255,255,255,255,255,255,255,255,255,255,255,255,73,250,228,249,6,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,242,254,195,249,228,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,174,250,228,249,40,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,228,249,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,229,249,140,250,228,249,
		196,249,107,250,255,255,255,255,255,255,255,255,255,255,187,255,195,249,196,249,154,255,255,255,255,255,255,255,141,250,228,249,74,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,174,250,196,249,106,250,255,255,255,255,74,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,255,255,255,255,255,255,255,255,
		175,250,196,249,40,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,86,255,160,249,141,250,255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,228,249,106,250,6,250,228,249,229,249,221,255,255,255,255,255,255,255,255,255,255,255,39,250,228,249,207,250,255,255,255,255,255,255,107,250,228,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,120,255,
		195,249,195,249,53,255,255,255,255,255,255,255,141,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,196,249,53,255,255,255,255,255,255,255,241,254,195,249,228,249,87,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,242,254,255,255,255,255,255,255,188,255,195,249,228,249,187,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,6,250,196,249,242,254,107,250,195,249,173,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,229,249,195,249,53,255,40,250,195,249,209,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,228,249,241,254,255,255,255,255,255,255,221,255,195,249,195,249,187,255,255,255,255,255,255,255,
		255,255,255,255,194,249,5,250,229,249,195,249,255,255,255,255,255,255,255,255,255,255,153,255,195,249,5,250,229,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,54,255,195,249,6,250,119,255,196,249,195,249,254,255,255,255,
		255,255,255,255,255,255,255,255,40,250,228,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		40,250,228,249,207,250,255,255,255,255,255,255,255,255,255,255,255,255,187,255,195,249,195,249,187,255,255,255,255,255,255,255,255,255,255,255,106,250,228,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		255,255,40,250,228,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,242,254,195,249,40,250,86,255,195,249,195,249,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,175,250,228,249,229,249,229,249,195,249,255,255,255,255,255,255,255,255,222,255,195,249,229,249,229,249,228,249,241,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,221,255,187,255,195,249,195,249,
		188,255,255,255,255,255,255,255,255,255,221,255,228,249,196,249,52,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,39,250,228,249,108,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,187,255,121,255,121,255,121,255,121,255,153,255,120,255,228,249,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,195,249,187,255,19,255,
		195,249,195,249,140,250,255,255,255,255,255,255,255,255,187,255,195,249,196,249,154,255,255,255,255,255,255,255,141,250,228,249,41,250,187,255,121,255,121,255,121,255,121,255,120,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,6,250,228,249,174,250,255,255,255,255,74,250,228,249,107,250,155,255,121,255,121,255,121,255,121,255,120,255,255,255,255,255,255,255,120,255,195,249,195,249,255,255,255,255,255,255,255,255,
		40,250,196,249,174,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,221,255,241,250,188,255,255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,228,249,175,250,222,255,228,249,228,249,229,249,222,255,255,255,255,255,255,255,255,255,39,250,228,249,207,250,255,255,255,255,255,255,107,250,228,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		7,250,228,249,140,250,255,255,255,255,255,255,141,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,196,249,53,255,255,255,255,255,255,255,255,255,229,249,228,249,195,249,107,250,121,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,242,254,255,255,255,255,255,255,19,255,228,249,196,249,255,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		255,255,255,255,255,255,255,255,87,255,195,249,195,249,255,255,120,255,195,249,195,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,195,249,154,255,153,255,121,255,121,255,121,255,
		188,255,255,255,255,255,255,255,255,255,20,255,195,249,229,249,255,255,52,255,195,249,229,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,228,249,241,254,255,255,255,255,255,255,20,255,228,249,195,249,255,255,255,255,255,255,255,255,
		255,255,155,255,195,249,229,249,229,249,196,249,20,255,255,255,255,255,255,255,255,255,74,250,229,249,229,249,229,249,6,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,196,249,208,250,255,255,6,250,196,249,208,250,255,255,
		255,255,255,255,255,255,221,255,228,249,196,249,53,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,195,249,196,249,120,255,153,255,121,255,121,255,121,255,121,255,188,255,255,255,255,255,255,255,
		40,250,228,249,207,250,255,255,255,255,255,255,255,255,255,255,255,255,187,255,195,249,195,249,187,255,255,255,255,255,255,255,255,255,255,255,106,250,228,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		221,255,228,249,196,249,53,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,228,249,195,249,19,255,255,255,228,249,195,249,19,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,73,250,228,249,208,250,5,250,228,249,241,254,255,255,255,255,255,255,175,250,196,249,40,250,173,250,228,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,108,250,195,249,107,250,255,255,255,255,41,250,228,249,
		141,250,255,255,255,255,255,255,255,255,153,255,195,249,195,249,188,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,228,249,19,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,174,250,228,249,229,249,195,249,195,249,195,249,195,249,195,249,195,249,196,249,5,250,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,195,249,121,255,255,255,
		242,254,195,249,196,249,108,250,255,255,255,255,255,255,187,255,195,249,196,249,154,255,255,255,255,255,255,255,141,250,228,249,229,249,195,249,195,249,195,249,195,249,195,249,162,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,195,249,255,255,255,255,154,255,74,250,228,249,194,249,153,255,255,255,255,255,74,250,229,249,229,249,195,249,195,249,195,249,195,249,195,249,162,249,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,121,255,74,250,
		228,249,195,249,187,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,228,249,141,250,255,255,187,255,228,249,228,249,229,249,221,255,255,255,255,255,255,255,39,250,228,249,207,250,255,255,255,255,255,255,107,250,228,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		174,250,228,249,40,250,255,255,255,255,255,255,141,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,196,249,53,255,255,255,255,255,255,255,255,255,221,255,7,250,195,249,228,249,195,249,6,250,86,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,229,249,228,249,242,254,255,255,187,255,209,254,228,249,195,249,175,250,255,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		255,255,255,255,255,255,255,255,72,250,196,249,174,250,255,255,255,255,229,249,196,249,209,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,6,250,195,249,195,249,195,249,195,249,162,249,
		175,250,255,255,255,255,255,255,255,255,6,250,195,249,241,254,255,255,255,255,229,249,195,249,20,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,228,249,241,254,255,255,188,255,241,254,228,249,195,249,174,250,255,255,255,255,255,255,255,255,
		255,255,20,255,196,249,6,250,208,250,228,249,39,250,255,255,255,255,255,255,187,255,195,249,228,249,241,254,228,249,194,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,87,255,195,249,195,249,255,255,255,255,53,255,195,249,229,249,255,255,
		255,255,255,255,255,255,120,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,195,249,6,250,196,249,195,249,195,249,195,249,195,249,162,249,209,254,255,255,255,255,255,255,
		40,250,228,249,207,250,255,255,255,255,255,255,255,255,255,255,255,255,187,255,195,249,195,249,187,255,255,255,255,255,255,255,255,255,255,255,106,250,228,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		120,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,52,255,195,249,228,249,255,255,255,255,241,254,195,249,6,250,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,6,250,195,249,120,255,241,254,196,249,6,250,255,255,255,255,255,255,228,249,195,249,53,255,20,255,195,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,221,255,195,249,195,249,153,255,255,255,255,255,87,255,195,249,
		195,249,254,255,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,196,249,86,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,174,250,228,249,229,249,195,249,195,249,195,249,195,249,195,249,195,249,195,249,5,250,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,195,249,120,255,255,255,
		255,255,241,254,195,249,195,249,141,250,255,255,255,255,187,255,195,249,196,249,154,255,255,255,255,255,255,255,141,250,228,249,229,249,195,249,195,249,195,249,195,249,194,249,161,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,6,250,195,249,195,249,195,249,228,249,195,249,174,250,255,255,255,255,255,255,74,250,229,249,229,249,195,249,195,249,195,249,195,249,194,249,161,249,255,255,255,255,255,255,120,255,195,249,6,250,195,249,195,249,195,249,228,249,
		194,249,20,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,228,249,141,250,255,255,255,255,187,255,195,249,228,249,5,250,254,255,255,255,255,255,39,250,228,249,207,250,255,255,255,255,255,255,107,250,228,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		208,250,228,249,39,250,255,255,255,255,255,255,141,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,196,249,53,255,255,255,255,255,255,255,255,255,255,255,255,255,87,255,40,250,195,249,228,249,194,249,174,250,255,255,
		255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,229,249,229,249,228,249,195,249,195,249,228,249,195,249,73,250,255,255,255,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		255,255,255,255,255,255,153,255,195,249,196,249,188,255,255,255,255,255,19,255,195,249,229,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,6,250,195,249,195,249,195,249,195,249,161,249,
		175,250,255,255,255,255,255,255,86,255,195,249,228,249,255,255,255,255,255,255,208,250,195,249,6,250,255,255,255,255,255,255,255,255,255,255,255,255,6,250,229,249,228,249,195,249,195,249,228,249,195,249,40,250,255,255,255,255,255,255,255,255,255,255,
		255,255,174,250,196,249,106,250,254,255,195,249,195,249,120,255,255,255,255,255,140,250,228,249,41,250,255,255,195,249,195,249,154,255,255,255,255,255,255,255,255,255,255,255,255,255,73,250,195,249,174,250,255,255,255,255,255,255,229,249,195,249,19,255,
		255,255,255,255,255,255,87,255,195,249,195,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,195,249,6,250,195,249,195,249,195,249,195,249,195,249,161,249,208,254,255,255,255,255,255,255,
		40,250,228,249,207,250,255,255,255,255,255,255,255,255,255,255,255,255,187,255,195,249,195,249,187,255,255,255,255,255,255,255,255,255,255,255,106,250,228,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		87,255,195,249,195,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,195,249,209,254,255,255,255,255,222,255,228,249,195,249,86,255,255,255,255,255,255,255,255,255,
		255,255,255,255,188,255,195,249,195,249,188,255,254,255,228,249,195,249,53,255,255,255,242,254,196,249,228,249,255,255,120,255,195,249,195,249,255,255,255,255,255,255,255,255,255,255,255,255,140,250,228,249,74,250,255,255,255,255,255,255,255,255,39,250,
		196,249,208,250,255,255,255,255,255,255,121,255,195,249,195,249,188,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,228,249,20,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,174,250,228,249,40,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,228,249,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,195,249,120,255,255,255,
		255,255,255,255,209,254,195,249,195,249,140,250,255,255,187,255,195,249,196,249,154,255,255,255,255,255,255,255,141,250,228,249,74,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,6,250,195,249,195,249,228,249,5,250,20,255,255,255,255,255,255,255,255,255,74,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,195,249,5,250,229,249,229,249,229,249,141,250,
		221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,228,249,141,250,255,255,255,255,255,255,155,255,195,249,228,249,5,250,254,255,255,255,39,250,228,249,207,250,255,255,255,255,255,255,107,250,228,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		174,250,228,249,40,250,255,255,255,255,255,255,141,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,196,249,53,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,187,255,40,250,228,249,195,249,20,255,
		255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,229,249,229,249,229,249,229,249,229,249,39,250,87,255,255,255,255,255,255,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		255,255,255,255,255,255,73,250,228,249,174,250,255,255,255,255,255,255,255,255,6,250,195,249,20,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,195,249,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,39,250,196,249,241,254,255,255,255,255,255,255,255,255,229,249,195,249,87,255,255,255,255,255,255,255,255,255,255,255,6,250,229,249,229,249,229,249,229,249,7,250,86,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,40,250,196,249,208,250,255,255,74,250,228,249,73,250,255,255,254,255,194,249,195,249,86,255,255,255,195,249,196,249,20,255,255,255,255,255,255,255,255,255,255,255,153,255,195,249,228,249,254,255,255,255,255,255,255,255,19,255,228,249,6,250,
		255,255,255,255,255,255,120,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,195,249,195,249,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		40,250,228,249,207,250,255,255,255,255,255,255,255,255,255,255,255,255,187,255,195,249,195,249,187,255,255,255,255,255,255,255,255,255,255,255,106,250,228,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		120,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,86,255,195,249,229,249,255,255,255,255,255,255,255,255,175,250,228,249,7,250,255,255,255,255,255,255,255,255,
		255,255,255,255,54,255,195,249,5,250,255,255,255,255,140,250,228,249,39,250,255,255,6,250,195,249,208,250,255,255,188,255,228,249,195,249,155,255,255,255,255,255,255,255,255,255,222,255,196,249,229,249,73,250,140,250,107,250,107,250,140,250,40,250,
		229,249,228,249,255,255,255,255,255,255,220,255,228,249,196,249,53,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,228,249,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,195,249,120,255,255,255,
		255,255,255,255,255,255,208,250,195,249,195,249,174,250,221,255,195,249,196,249,154,255,255,255,255,255,255,255,141,250,228,249,74,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,195,249,187,255,153,255,188,255,255,255,255,255,255,255,255,255,255,255,255,255,74,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,188,255,228,249,228,249,140,250,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,228,249,141,250,255,255,255,255,255,255,255,255,153,255,195,249,228,249,5,250,255,255,40,250,228,249,207,250,255,255,255,255,255,255,107,250,228,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		7,250,228,249,141,250,255,255,255,255,255,255,140,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,196,249,53,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,40,250,228,249,39,250,
		255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,229,249,228,249,19,255,141,250,228,249,229,249,222,255,255,255,255,255,255,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		255,255,255,255,188,255,195,249,229,249,74,250,108,250,107,250,107,250,140,250,7,250,229,249,229,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,120,255,195,249,229,249,106,250,108,250,107,250,107,250,141,250,6,250,229,249,40,250,255,255,255,255,255,255,255,255,255,255,6,250,228,249,242,254,174,250,228,249,229,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,229,249,195,249,54,255,255,255,120,255,195,249,194,249,255,255,208,250,228,249,229,249,255,255,255,255,40,250,228,249,175,250,255,255,255,255,255,255,255,255,255,255,74,250,229,249,6,250,140,250,107,250,107,250,108,250,107,250,229,249,195,249,
		86,255,255,255,255,255,188,255,228,249,195,249,86,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		40,250,228,249,207,250,255,255,255,255,255,255,255,255,255,255,255,255,187,255,195,249,195,249,187,255,255,255,255,255,255,255,255,255,255,255,106,250,228,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		188,255,196,249,195,249,86,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,40,250,229,249,7,250,140,250,107,250,107,250,108,250,74,250,229,249,195,249,121,255,255,255,255,255,255,255,
		255,255,255,255,207,250,228,249,40,250,255,255,255,255,187,255,195,249,228,249,174,250,228,249,228,249,254,255,255,255,255,255,228,249,196,249,53,255,255,255,255,255,255,255,255,255,175,250,228,249,229,249,228,249,196,249,196,249,196,249,196,249,228,249,
		229,249,196,249,19,255,255,255,255,255,255,255,40,250,228,249,40,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,208,250,228,249,228,249,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,228,249,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,195,249,120,255,255,255,
		255,255,255,255,255,255,255,255,208,250,195,249,228,249,74,250,229,249,196,249,154,255,255,255,255,255,255,255,141,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,74,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,255,255,87,255,195,249,195,249,
		242,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,154,255,195,249,228,249,7,250,39,250,228,249,207,250,255,255,255,255,255,255,107,250,228,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,86,255,
		195,249,195,249,54,255,255,255,255,255,255,255,141,250,228,249,40,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,195,249,195,249,86,255,255,255,255,255,255,255,255,255,154,255,255,255,255,255,255,255,255,255,255,255,19,255,228,249,196,249,
		255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,5,250,228,249,7,250,255,255,255,255,255,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		255,255,255,255,107,250,229,249,229,249,228,249,196,249,196,249,196,249,196,249,228,249,229,249,195,249,86,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,40,250,229,249,229,249,228,249,196,249,196,249,196,249,196,249,228,249,229,249,195,249,153,255,255,255,255,255,255,255,255,255,6,250,228,249,242,254,255,255,6,250,228,249,6,250,255,255,255,255,255,255,255,255,255,255,255,255,
		187,255,195,249,195,249,187,255,255,255,255,255,6,250,228,249,107,250,6,250,195,249,20,255,255,255,255,255,141,250,228,249,73,250,255,255,255,255,255,255,255,255,187,255,195,249,229,249,228,249,196,249,196,249,196,249,196,249,228,249,229,249,229,249,
		39,250,255,255,255,255,255,255,7,250,228,249,41,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,195,249,195,249,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		40,250,228,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,121,255,195,249,195,249,188,255,255,255,255,255,255,255,255,255,255,255,106,250,228,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		255,255,7,250,228,249,40,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,195,249,229,249,228,249,196,249,196,249,196,249,196,249,228,249,229,249,229,249,74,250,255,255,255,255,255,255,
		255,255,255,255,73,250,228,249,141,250,255,255,255,255,255,255,40,250,229,249,229,249,228,249,141,250,255,255,255,255,255,255,7,250,228,249,207,250,255,255,255,255,255,255,254,255,228,249,228,249,74,250,141,250,141,250,141,250,141,250,141,250,174,250,
		40,250,229,249,6,250,255,255,255,255,255,255,53,255,195,249,228,249,241,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,220,255,195,249,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,228,249,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,195,249,120,255,255,255,
		255,255,255,255,255,255,255,255,255,255,175,250,195,249,229,249,5,250,196,249,154,255,255,255,255,255,255,255,141,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,74,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,255,255,255,255,141,250,228,249,
		195,249,187,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,121,255,195,249,229,249,229,249,228,249,207,250,255,255,255,255,255,255,107,250,228,249,107,250,255,255,255,255,255,255,255,255,255,255,188,255,228,249,
		229,249,229,249,255,255,255,255,255,255,255,255,242,254,196,249,228,249,255,255,255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,187,255,255,255,255,255,87,255,195,249,128,249,220,255,255,255,255,255,255,255,255,255,18,255,228,249,228,249,
		255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,242,254,255,255,154,255,194,249,228,249,175,250,255,255,255,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		255,255,187,255,196,249,228,249,107,250,141,250,141,250,141,250,141,250,141,250,174,250,39,250,228,249,73,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,153,255,195,249,228,249,140,250,141,250,141,250,141,250,141,250,141,250,174,250,7,250,228,249,107,250,255,255,255,255,255,255,255,255,6,250,228,249,241,254,255,255,154,255,194,249,228,249,174,250,255,255,255,255,255,255,255,255,255,255,
		53,255,195,249,229,249,255,255,255,255,255,255,53,255,195,249,229,249,229,249,229,249,255,255,255,255,255,255,19,255,195,249,6,250,255,255,255,255,255,255,255,255,107,250,228,249,6,250,174,250,141,250,141,250,141,250,141,250,141,250,140,250,228,249,
		195,249,121,255,255,255,255,255,52,255,195,249,196,249,242,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,195,249,195,249,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		140,250,228,249,229,249,255,255,255,255,255,255,255,255,255,255,255,255,242,254,228,249,229,249,254,255,255,255,255,255,255,255,255,255,255,255,106,250,228,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		255,255,52,255,195,249,196,249,242,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,41,250,228,249,39,250,174,250,141,250,141,250,141,250,141,250,141,250,107,250,228,249,195,249,188,255,255,255,255,255,
		255,255,255,255,195,249,228,249,242,254,255,255,255,255,255,255,120,255,195,249,5,250,195,249,221,255,255,255,255,255,255,255,140,250,228,249,73,250,255,255,255,255,255,255,209,250,196,249,229,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		221,255,195,249,195,249,53,255,255,255,255,255,255,255,74,250,228,249,195,249,208,250,255,255,255,255,255,255,255,255,255,255,255,255,52,255,175,250,255,255,255,255,255,255,242,254,195,249,228,249,74,250,255,255,255,255,255,255,255,255,255,255,255,255,
		187,255,41,250,255,255,255,255,255,255,174,250,228,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,228,249,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,195,249,120,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,207,250,195,249,5,250,196,249,154,255,255,255,255,255,255,255,141,250,228,249,74,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,74,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,255,255,255,255,255,255,5,250,
		228,249,6,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,229,249,228,249,207,250,255,255,255,255,255,255,107,250,228,249,140,250,255,255,255,255,255,255,255,255,19,255,196,249,229,249,
		195,249,153,255,255,255,255,255,255,255,255,255,188,255,195,249,228,249,140,250,255,255,255,255,255,255,255,255,255,255,7,250,229,249,229,249,255,255,255,255,255,255,254,255,194,249,228,249,207,250,255,255,255,255,255,255,255,255,39,250,228,249,74,250,
		255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,242,254,255,255,255,255,208,250,228,249,194,249,120,255,255,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		255,255,141,250,228,249,6,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,153,255,195,249,195,249,153,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,107,250,228,249,40,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,86,255,195,249,195,249,188,255,255,255,255,255,255,255,6,250,228,249,241,254,255,255,255,255,209,254,196,249,195,249,87,255,255,255,255,255,255,255,255,255,
		174,250,228,249,40,250,255,255,255,255,255,255,255,255,6,250,229,249,228,249,209,250,255,255,255,255,255,255,120,255,195,249,228,249,221,255,255,255,255,255,188,255,195,249,195,249,54,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,40,250,
		228,249,74,250,255,255,255,255,255,255,73,250,228,249,195,249,209,254,255,255,255,255,255,255,255,255,255,255,255,255,19,255,208,250,255,255,255,255,121,255,195,249,195,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		87,255,195,249,196,249,242,254,255,255,255,255,255,255,255,255,221,255,195,249,228,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,106,250,228,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		255,255,255,255,73,250,228,249,195,249,209,254,255,255,255,255,255,255,255,255,255,255,255,255,20,255,208,250,255,255,255,255,153,255,195,249,195,249,121,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,6,250,228,249,140,250,255,255,255,255,
		255,255,188,255,195,249,195,249,120,255,255,255,255,255,255,255,255,255,6,250,228,249,107,250,255,255,255,255,255,255,255,255,241,254,228,249,6,250,255,255,255,255,255,255,228,249,228,249,175,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,74,250,228,249,40,250,255,255,255,255,255,255,255,255,7,250,228,249,228,249,6,250,209,250,120,255,120,255,19,255,73,250,161,249,208,250,255,255,255,255,255,255,255,255,140,250,195,249,228,249,228,249,175,250,86,255,120,255,53,255,108,250,
		195,249,39,250,255,255,255,255,255,255,174,250,228,249,39,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,228,249,195,249,87,255,255,255,255,255,255,255,208,250,228,249,6,250,255,255,255,255,255,255,222,255,195,249,195,249,120,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,174,250,195,249,196,249,154,255,255,255,255,255,255,255,141,250,228,249,6,250,107,250,107,250,107,250,107,250,107,250,73,250,121,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,74,250,228,249,7,250,107,250,107,250,107,250,107,250,107,250,73,250,187,255,255,255,255,255,120,255,195,249,195,249,255,255,255,255,255,255,153,255,
		195,249,228,249,108,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,154,255,195,249,229,249,74,250,107,250,107,250,107,250,40,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,174,250,228,249,39,250,
		255,255,255,255,255,255,255,255,73,250,228,249,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,195,249,228,249,207,250,255,255,255,255,255,255,107,250,228,249,7,250,107,250,107,250,73,250,228,249,196,249,228,249,195,249,
		87,255,255,255,255,255,255,255,255,255,255,255,255,255,175,250,195,249,228,249,73,250,53,255,120,255,19,255,6,250,228,249,195,249,87,255,255,255,255,255,255,255,255,255,107,250,228,249,228,249,141,250,120,255,86,255,40,250,228,249,195,249,120,255,
		255,255,255,255,255,255,255,255,255,255,120,255,195,249,195,249,254,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,242,254,255,255,255,255,255,255,7,250,228,249,228,249,254,255,255,255,255,255,255,255,140,250,228,249,106,250,255,255,255,255,
		254,255,195,249,196,249,19,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,7,250,228,249,74,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,196,249,194,249,255,255,255,255,255,255,255,255,255,255,
		255,255,187,255,195,249,195,249,86,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,229,249,228,249,141,250,255,255,255,255,255,255,6,250,228,249,241,254,255,255,255,255,255,255,39,250,228,249,196,249,222,255,255,255,255,255,255,255,
		40,250,228,249,140,250,255,255,255,255,255,255,255,255,208,250,228,249,194,249,255,255,255,255,255,255,255,255,221,255,195,249,195,249,120,255,255,255,255,255,141,250,228,249,6,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,
		195,249,195,249,188,255,255,255,255,255,255,255,6,250,228,249,196,249,6,250,241,254,120,255,120,255,19,255,73,250,161,249,241,254,255,255,255,255,121,255,195,249,229,249,106,250,107,250,107,250,107,250,107,250,74,250,175,250,255,255,255,255,255,255,
		255,255,73,250,228,249,196,249,108,250,86,255,120,255,208,250,228,249,229,249,194,249,221,255,255,255,255,255,255,255,255,255,255,255,255,255,106,250,228,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,229,249,196,249,19,255,255,255,255,255,
		255,255,255,255,255,255,6,250,228,249,196,249,6,250,241,254,120,255,120,255,19,255,73,250,161,249,209,254,255,255,255,255,106,250,228,249,40,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,242,254,228,249,194,249,255,255,255,255,
		255,255,53,255,194,249,162,249,254,255,255,255,255,255,255,255,255,255,20,255,161,249,154,255,255,255,255,255,255,255,255,255,86,255,194,249,195,249,220,255,255,255,241,254,194,249,194,249,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,87,255,195,249,162,249,87,255,255,255,255,255,255,255,255,255,140,250,195,249,228,249,196,249,195,249,195,249,195,249,228,249,194,249,209,254,255,255,255,255,255,255,255,255,255,255,242,254,195,249,228,249,228,249,195,249,195,249,195,249,228,249,
		195,249,40,250,255,255,255,255,255,255,174,250,195,249,6,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,195,249,194,249,86,255,255,255,255,255,255,255,208,250,195,249,229,249,255,255,255,255,255,255,222,255,162,249,194,249,120,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,174,250,161,249,154,255,255,255,255,255,255,255,108,250,195,249,228,249,196,249,196,249,196,249,196,249,196,249,161,249,86,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,53,255,194,249,161,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,73,250,196,249,228,249,196,249,196,249,196,249,196,249,196,249,161,249,154,255,255,255,255,255,87,255,194,249,161,249,255,255,255,255,255,255,255,255,
		208,250,195,249,162,249,20,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,153,255,162,249,228,249,228,249,196,249,196,249,196,249,128,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,174,250,195,249,6,250,
		255,255,255,255,255,255,255,255,72,250,195,249,140,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,162,249,175,250,255,255,255,255,255,255,74,250,195,249,228,249,196,249,196,249,196,249,195,249,195,249,74,250,221,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,141,250,195,249,228,249,196,249,195,249,196,249,228,249,195,249,53,255,255,255,255,255,255,255,255,255,255,255,255,255,7,250,195,249,228,249,195,249,196,249,228,249,195,249,241,250,255,255,
		255,255,255,255,255,255,255,255,255,255,120,255,194,249,162,249,254,255,255,255,255,255,255,255,255,255,255,255,228,249,194,249,242,254,255,255,255,255,255,255,221,255,195,249,195,249,40,250,255,255,255,255,255,255,107,250,195,249,73,250,255,255,255,255,
		141,250,195,249,195,249,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,20,255,195,249,161,249,187,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,53,255,194,249,161,249,255,255,255,255,255,255,255,255,255,255,
		255,255,106,250,195,249,6,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,209,254,195,249,194,249,221,255,255,255,255,255,229,249,194,249,209,254,255,255,255,255,255,255,222,255,195,249,195,249,40,250,255,255,255,255,255,255,
		162,249,195,249,242,254,255,255,255,255,255,255,255,255,255,255,161,249,174,250,255,255,255,255,255,255,255,255,255,255,229,249,194,249,242,254,255,255,221,255,162,249,195,249,18,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		228,249,195,249,107,250,255,255,255,255,255,255,255,255,108,250,195,249,228,249,196,249,195,249,195,249,195,249,228,249,194,249,242,254,255,255,255,255,120,255,194,249,228,249,196,249,196,249,196,249,196,249,196,249,195,249,73,250,255,255,255,255,255,255,
		255,255,255,255,73,250,195,249,228,249,196,249,195,249,228,249,228,249,228,249,154,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,73,250,195,249,107,250,255,255,255,255,255,255,255,255,255,255,255,255,196,249,194,249,242,254,255,255,255,255,
		255,255,255,255,255,255,255,255,108,250,195,249,228,249,196,249,195,249,195,249,195,249,228,249,194,249,242,254,255,255,155,255,161,249,195,249,54,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,194,249,195,249,174,250,255,255,
		255,255,53,255,141,250,140,250,255,255,255,255,255,255,255,255,255,255,255,255,39,250,255,255,255,255,255,255,255,255,255,255,220,255,140,250,140,250,187,255,255,255,174,250,141,250,242,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,174,250,141,250,241,254,255,255,255,255,255,255,255,255,255,255,154,255,141,250,7,250,162,249,194,249,6,250,107,250,120,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,221,255,209,254,39,250,195,249,194,249,228,249,74,250,
		53,255,255,255,255,255,255,255,255,255,20,255,141,250,207,250,255,255,255,255,255,255,255,255,255,255,255,255,254,255,141,250,141,250,154,255,255,255,255,255,255,255,53,255,141,250,174,250,255,255,255,255,255,255,254,255,140,250,140,250,155,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,107,250,120,255,255,255,255,255,255,255,19,255,141,250,174,250,174,250,174,250,174,250,174,250,174,250,141,250,154,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,120,255,141,250,140,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,241,254,141,250,174,250,174,250,174,250,174,250,174,250,174,250,140,250,188,255,255,255,255,255,154,255,141,250,140,250,255,255,255,255,255,255,255,255,
		255,255,174,250,141,250,141,250,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,187,255,140,250,174,250,174,250,174,250,174,250,174,250,108,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,20,255,141,250,207,250,
		255,255,255,255,255,255,255,255,209,254,141,250,19,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,86,255,141,250,255,255,255,255,255,255,242,254,141,250,174,250,174,250,174,250,207,250,53,255,188,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,120,255,74,250,195,249,162,249,229,249,140,250,187,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,208,250,6,250,162,249,195,249,74,250,120,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,154,255,141,250,140,250,255,255,255,255,255,255,255,255,255,255,255,255,174,250,141,250,86,255,255,255,255,255,255,255,255,255,54,255,141,250,140,250,119,255,255,255,255,255,242,254,141,250,241,254,255,255,255,255,
		141,250,141,250,52,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,140,250,141,250,20,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,121,255,141,250,140,250,255,255,255,255,255,255,255,255,255,255,
		254,255,140,250,141,250,86,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,140,250,141,250,86,255,255,255,255,255,174,250,141,250,86,255,255,255,255,255,255,255,255,255,86,255,141,250,140,250,87,255,255,255,221,255,
		140,250,141,250,154,255,255,255,255,255,255,255,255,255,255,255,107,250,187,255,255,255,255,255,255,255,255,255,255,255,208,250,141,250,19,255,255,255,53,255,141,250,140,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		53,255,141,250,141,250,254,255,255,255,255,255,255,255,255,255,153,255,141,250,7,250,162,249,162,249,6,250,140,250,121,255,255,255,255,255,255,255,187,255,140,250,174,250,174,250,174,250,174,250,174,250,174,250,141,250,242,254,255,255,255,255,255,255,
		255,255,255,255,255,255,52,255,73,250,162,249,162,249,6,250,174,250,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,241,254,141,250,242,254,255,255,255,255,255,255,255,255,255,255,255,255,141,250,141,250,87,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,153,255,141,250,7,250,162,249,162,249,6,250,108,250,121,255,255,255,255,255,19,255,141,250,141,250,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,19,255,141,250,174,250,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,221,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,222,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,254,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
		255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
};

static const Bitmap_header_t Bitmap_Header =
{
	.Format = ARGB1555,
	.Width = 480,
	.Height = 95,
	.Stride = 960,
	.Arrayoffset = 0,
	.Bitmap_RawData = (ft_int32_t)Bitmap_RawData
};

void Bitmap_Test( void )
{
	ft_uint32_t RawDataSize = (ft_uint32_t)Bitmap_Header.Stride * (ft_uint32_t)Bitmap_Header.Height;
	ft_uint8_t *pRawData = (ft_uint8_t *)(Bitmap_Header.Bitmap_RawData + Bitmap_Header.Arrayoffset);

	// load custom bitmap into GRAM, starting location RAM_G
	Ft_Gpu_Hal_WrMem( pHost, RAM_G, pRawData, RawDataSize);

	// view the bitmap
	Ft_Gpu_CoCmd_Dlstart(pHost);
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE

	// draw bitmap
	// specify the starting address of the bitmap in graphics RAM
	Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(RAM_G));
	// specify the bitmap format, linestride and height
	Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(Bitmap_Header.Format, Bitmap_Header.Stride, Bitmap_Header.Height));
	// set filtering, wrapping and on-screen size
	Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, Bitmap_Header.Width, Bitmap_Header.Height));
	// start drawing bitmap
	Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
	//Ft_App_WrCoCmd_Buffer(pHost,MACRO(0));
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( ((FT_DispWidth-Bitmap_Header.Width)/2)*16, ((FT_DispHeight-Bitmap_Header.Height)/2)*16 ) );

	Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
	Ft_Gpu_CoCmd_Swap( pHost );
	Ft_App_Flush_Co_Buffer( pHost );
	Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

	// end
	for( ; ; Wdog() )
	{
		Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
	}

}

#endif

#ifdef FONT_TEST

/*Command Line: fnt_cvt.exe -i arial.ttf -s 18 -u CharMap_Europe.txt -d 7848*/ 

/*118 characters has been converted */

/* 148 Metric Block Begin +++  */
/*('file properties ', 'format ', 'L1', ' stride ', 3, ' width ', 20, 'height', 21)*/ 
static const ft_uint8_t MetricBlock[] =
{
/* Widths */
0,5,5,6,12,10,16,12,3,6,6,8,11,5,7,5,5,10,10,10,10,11,10,10,10,10,10,5,6,11,11,11,10,19,14,12,13,13,11,10,14,12,5,9,12,10,14,12,14,11,14,13,12,12,12,12,17,12,12,12,5,5,5,8,10,6,10,10,10,10,10,7,10,10,4,5,9,4,15,10,10,10,10,7,9,6,10,9,13,9,9,10,7,5,7,11,14,7,11,13,10,14,5,14,12,12,10,5,10,9,10,13,13,12,12,10,10,9,10,0,0,0,0,0,0,0,0,0,
/* Format */
1,0,0,0,
/* Stride */
3,0,0,0,
/* Max Width */
20,0,0,0,
/* Max Height */
21,0,0,0,
/* Raw Data Address in Decimal: <7933> */ 
253,30,0,0,
};
/* 148 Metric Block End ---  */

/*Bitmap Raw Data begin +++*/
/*The expected raw bitmap size is 7434 Bytes */
static const ft_uint8_t FontBmpData[] =
{
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,
48,0,0,48,0,0,0,0,0,0,0,0,0,0,0,48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,108,0,0,108,0,0,108,0,0,72,0,0,72,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,12,64,0,12,64,0,8,192,0,8,192,0,127,224,0,25,128,0,24,128,0,17,128,0,17,128,
0,127,224,0,49,0,0,49,0,0,35,0,0,34,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,4,0,0,30,0,0,55,0,0,101,128,0,100,0,0,100,0,0,52,0,0,30,0,0,7,0,0,5,128,0,
5,128,0,101,128,0,101,128,0,55,0,0,30,0,0,4,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,56,16,0,108,32,0,102,32,0,102,64,0,102,64,0,100,128,0,60,128,0,1,28,0,1,54,0,2,
102,0,2,102,0,4,102,0,4,54,0,8,28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,15,0,0,27,128,0,49,128,0,49,128,0,27,0,0,31,0,0,30,0,0,54,0,0,99,96,0,97,224,
0,96,192,0,97,224,0,51,240,0,30,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,96,0,0,96,0,0,96,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,8,0,0,16,0,0,16,0,0,48,0,0,32,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,
0,0,96,0,0,96,0,0,32,0,0,48,0,0,16,0,0,16,0,0,8,0,0,0,0,0,0,0,0,0,0,0,
32,0,0,32,0,0,16,0,0,16,0,0,24,0,0,8,0,0,8,0,0,12,0,0,12,0,0,12,0,0,12,0,
0,8,0,0,8,0,0,24,0,0,16,0,0,16,0,0,32,0,0,32,0,0,0,0,0,0,0,0,0,0,0,24,
0,0,24,0,0,126,0,0,56,0,0,44,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,12,0,0,12,0,0,12,0,0,12,0,0,127,192,0,12,0,0,12,0,0,12,0,0,12,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,96,0,0,32,0,0,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,124,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,24,0,0,
16,0,0,16,0,0,16,0,0,48,0,0,32,0,0,32,0,0,96,0,0,64,0,0,64,0,0,64,0,0,192,0,
0,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,30,0,0,59,0,0,33,
128,0,96,128,0,96,128,0,96,192,0,96,192,0,96,192,0,96,192,0,96,128,0,96,128,0,33,128,0,59,0,0,
30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,0,6,0,0,30,0,
0,62,0,0,38,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,62,0,0,119,0,0,65,0,0,
193,128,0,1,128,0,3,0,0,3,0,0,6,0,0,12,0,0,24,0,0,48,0,0,96,0,0,224,0,0,255,128,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,30,0,0,51,0,0,97,0,0,97,
128,0,1,0,0,3,0,0,14,0,0,3,0,0,1,128,0,1,128,0,97,128,0,97,128,0,51,0,0,30,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,3,0,0,7,0,0,15,0,
0,11,0,0,27,0,0,19,0,0,35,0,0,99,0,0,127,192,0,3,0,0,3,0,0,3,0,0,3,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,31,128,0,48,0,0,48,0,0,48,0,0,
32,0,0,63,0,0,51,128,0,32,128,0,0,192,0,0,192,0,96,128,0,97,128,0,59,0,0,30,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,15,0,0,27,128,0,49,128,0,96,128,0,96,
0,0,111,0,0,115,128,0,96,128,0,96,192,0,96,192,0,96,128,0,33,128,0,59,128,0,14,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,127,128,0,1,128,0,3,0,0,3,0,0,6,0,
0,6,0,0,4,0,0,12,0,0,8,0,0,8,0,0,24,0,0,24,0,0,24,0,0,24,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,30,0,0,51,0,0,33,128,0,97,128,0,33,128,0,
51,0,0,31,0,0,51,128,0,96,128,0,96,192,0,96,192,0,96,128,0,59,128,0,30,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,30,0,0,59,0,0,97,128,0,96,128,0,96,128,0,96,
192,0,96,192,0,49,192,0,30,192,0,0,128,0,0,128,0,97,128,0,51,0,0,30,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,48,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,48,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,48,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,48,0,0,16,0,0,48,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,0,3,128,0,30,0,0,120,
0,0,96,0,0,56,0,0,15,0,0,3,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,127,192,0,0,0,0,0,0,
0,0,0,0,127,192,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,0,0,120,0,0,30,0,0,3,128,0,
1,192,0,7,128,0,28,0,0,112,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,30,0,0,51,0,0,97,128,0,65,128,0,1,128,0,1,128,0,3,0,0,6,
0,0,12,0,0,12,0,0,12,0,0,0,0,0,0,0,0,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,1,248,0,7,30,0,12,3,0,24,1,128,49,201,128,35,56,128,102,24,192,102,24,
192,100,24,192,108,16,128,108,49,128,100,51,0,103,254,0,51,156,0,48,0,192,28,3,128,15,15,0,1,252,0,0,
0,0,0,0,0,0,0,0,6,0,0,14,0,0,15,0,0,11,0,0,25,0,0,25,128,0,17,128,0,48,192,0,
48,192,0,63,192,0,96,96,0,96,96,0,192,96,0,192,48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,127,0,0,97,128,0,96,192,0,96,192,0,96,192,0,97,128,0,127,128,0,97,192,0,96,
64,0,96,96,0,96,96,0,96,64,0,97,192,0,127,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,7,128,0,28,224,0,48,96,0,32,48,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,
0,96,16,0,32,48,0,48,96,0,28,224,0,15,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,127,0,0,97,192,0,96,96,0,96,96,0,96,32,0,96,48,0,96,48,0,96,48,0,96,48,0,
96,32,0,96,96,0,96,96,0,97,192,0,127,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,127,224,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,127,192,0,96,0,0,96,0,0,96,
0,0,96,0,0,96,0,0,96,0,0,127,224,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,127,192,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,127,128,0,96,0,0,96,0,0,96,0,
0,96,0,0,96,0,0,96,0,0,96,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,7,192,0,28,112,0,48,48,0,32,24,0,96,0,0,96,0,0,96,0,0,97,248,0,96,24,0,96,24,0,
32,24,0,48,24,0,28,112,0,7,192,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,96,96,0,96,96,0,96,96,0,96,96,0,96,96,0,96,96,0,127,224,0,96,96,0,96,96,0,96,96,0,96,
96,0,96,96,0,96,96,0,96,96,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,
0,48,0,0,48,0,0,48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,
0,0,3,0,0,3,0,0,3,0,0,3,0,0,3,0,0,3,0,0,3,0,0,3,0,0,3,0,0,99,0,0,
99,0,0,54,0,0,28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,96,
0,96,192,0,97,128,0,99,0,0,102,0,0,108,0,0,124,0,0,118,0,0,99,0,0,99,128,0,97,128,0,96,
192,0,96,224,0,96,96,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,0,0,
96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,
0,127,128,0,127,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,112,24,0,112,
56,0,112,56,0,120,56,0,120,120,0,104,88,0,104,88,0,108,216,0,100,152,0,100,152,0,102,152,0,103,152,0,
99,24,0,99,24,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,96,0,112,96,
0,112,96,0,120,96,0,104,96,0,108,96,0,102,96,0,102,96,0,99,96,0,97,96,0,97,224,0,96,224,0,96,
224,0,96,96,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,128,0,28,224,0,
48,48,0,32,16,0,96,24,0,96,24,0,96,24,0,96,24,0,96,24,0,96,24,0,32,16,0,48,48,0,28,224,
0,7,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,127,128,0,97,192,0,96,
64,0,96,96,0,96,96,0,96,64,0,96,192,0,127,128,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,
96,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,128,0,28,224,0,48,48,
0,32,16,0,96,24,0,96,24,0,96,24,0,96,24,0,96,24,0,96,24,0,33,16,0,49,240,0,28,224,0,7,
248,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,127,128,0,96,192,0,96,96,0,
96,96,0,96,96,0,96,96,0,96,192,0,127,128,0,99,0,0,97,128,0,96,192,0,96,224,0,96,96,0,96,48,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,31,0,0,49,128,0,96,192,0,96,
64,0,96,0,0,120,0,0,63,0,0,7,192,0,0,192,0,64,96,0,64,96,0,96,64,0,57,192,0,31,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,127,224,0,6,0,0,6,0,0,6,0,
0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,96,0,96,96,0,96,96,0,96,96,0,
96,96,0,96,96,0,96,96,0,96,96,0,96,96,0,96,96,0,96,96,0,32,64,0,57,192,0,31,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,192,48,0,64,32,0,96,96,0,96,96,0,32,
64,0,48,192,0,48,192,0,16,128,0,25,128,0,25,0,0,9,0,0,15,0,0,6,0,0,6,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,193,193,128,65,193,0,97,193,0,97,67,0,99,99,
0,35,98,0,34,34,0,50,54,0,54,54,0,22,52,0,20,20,0,28,28,0,28,28,0,12,24,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,96,0,48,96,0,48,192,0,25,128,0,13,0,0,
15,0,0,6,0,0,7,0,0,15,0,0,25,128,0,48,192,0,48,192,0,96,96,0,192,112,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,192,96,0,96,96,0,48,192,0,48,192,0,25,128,0,25,
0,0,15,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,6,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,63,224,0,0,224,0,0,192,0,1,192,0,1,128,0,3,0,
0,6,0,0,6,0,0,12,0,0,24,0,0,56,0,0,48,0,0,112,0,0,127,224,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,120,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,
96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,
0,120,0,0,0,0,0,0,0,0,0,0,0,128,0,0,192,0,0,64,0,0,64,0,0,64,0,0,96,0,0,32,
0,0,32,0,0,48,0,0,16,0,0,16,0,0,16,0,0,24,0,0,8,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,240,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,
0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,240,
0,0,0,0,0,0,0,0,0,0,0,24,0,0,24,0,0,28,0,0,52,0,0,38,0,0,98,0,0,67,0,0,
64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,192,0,
0,0,0,0,0,0,0,0,0,96,0,0,48,0,0,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,31,0,0,51,128,0,97,128,0,1,128,0,
31,128,0,57,128,0,97,128,0,97,128,0,119,128,0,60,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,96,0,0,96,0,0,96,0,0,96,0,0,110,0,0,115,0,0,97,128,0,97,128,0,97,
128,0,97,128,0,97,128,0,97,128,0,115,0,0,126,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,30,0,0,59,0,0,97,128,0,96,0,0,96,0,
0,96,0,0,96,128,0,97,128,0,51,0,0,30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,1,128,0,1,128,0,1,128,0,1,128,0,29,128,0,51,128,0,97,128,0,97,128,0,97,128,0,
97,128,0,97,128,0,97,128,0,51,128,0,31,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,30,0,0,51,0,0,97,128,0,96,128,0,127,128,0,96,
0,0,96,0,0,97,128,0,59,128,0,30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,30,0,0,24,0,0,48,0,0,48,0,0,124,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,
0,48,0,0,48,0,0,48,0,0,48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,30,128,0,51,128,0,97,128,0,97,128,0,97,128,0,97,128,0,
97,128,0,97,128,0,51,128,0,31,128,0,1,128,0,97,128,0,51,0,0,30,0,0,0,0,0,0,0,0,0,0,
0,96,0,0,96,0,0,96,0,0,96,0,0,110,0,0,115,0,0,97,128,0,97,128,0,97,128,0,97,128,0,97,
128,0,97,128,0,97,128,0,97,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
96,0,0,0,0,0,0,0,0,0,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,
0,96,0,0,96,0,0,96,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,
0,0,0,0,0,0,0,0,0,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,
96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,192,0,0,192,0,0,0,0,0,0,0,0,0,0,0,96,0,
0,96,0,0,96,0,0,96,0,0,99,0,0,102,0,0,108,0,0,120,0,0,120,0,0,108,0,0,102,0,0,102,
0,0,99,0,0,97,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,0,0,
96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,
0,96,0,0,96,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,78,112,0,119,152,0,99,8,0,99,8,0,99,8,0,99,8,0,99,8,0,99,8,0,
99,8,0,99,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,78,0,0,115,0,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,97,
128,0,97,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,30,0,0,59,128,0,97,128,0,96,128,0,96,192,0,96,192,0,96,128,0,97,128,0,59,128,
0,30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,78,0,0,115,0,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,115,0,0,
126,0,0,96,0,0,96,0,0,96,0,0,96,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,28,128,0,51,128,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,51,128,0,31,
128,0,1,128,0,1,128,0,1,128,0,1,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,124,0,0,112,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,62,0,0,119,0,0,97,0,0,96,0,0,60,0,0,15,0,0,1,128,0,97,128,0,115,0,0,62,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,48,0,0,48,0,0,48,0,
0,120,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,24,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,119,128,0,61,128,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,193,
128,0,65,0,0,99,0,0,99,0,0,34,0,0,54,0,0,20,0,0,28,0,0,28,0,0,8,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,194,24,
0,199,16,0,71,16,0,101,48,0,101,48,0,45,160,0,41,160,0,56,224,0,56,192,0,24,192,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,99,0,0,
99,0,0,54,0,0,28,0,0,28,0,0,28,0,0,54,0,0,54,0,0,99,0,0,193,128,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,193,128,0,97,
0,0,99,0,0,35,0,0,50,0,0,54,0,0,22,0,0,28,0,0,28,0,0,12,0,0,8,0,0,24,0,0,
112,0,0,112,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,127,128,0,3,128,
0,3,0,0,6,0,0,12,0,0,8,0,0,24,0,0,48,0,0,96,0,0,127,128,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,24,0,0,24,0,0,24,0,0,24,0,0,24,0,0,
24,0,0,16,0,0,48,0,0,96,0,0,48,0,0,16,0,0,24,0,0,24,0,0,24,0,0,24,0,0,24,0,
0,14,0,0,0,0,0,0,0,0,0,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,
0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,
48,0,0,0,0,0,0,0,0,0,0,0,96,0,0,48,0,0,24,0,0,24,0,0,24,0,0,24,0,0,24,0,
0,24,0,0,8,0,0,6,0,0,12,0,0,24,0,0,24,0,0,24,0,0,24,0,0,24,0,0,48,0,0,112,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,124,64,0,
71,192,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,255,248,0,255,248,0,255,248,0,255,248,0,255,248,0,255,248,0,255,248,0,255,248,0,255,248,0,255,
248,0,255,248,0,255,248,0,255,248,0,255,248,0,255,248,0,255,248,0,255,248,0,255,248,0,255,248,0,255,248,0,
0,0,0,0,0,0,0,0,0,56,0,0,108,0,0,68,0,0,68,0,0,56,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,30,0,0,55,0,0,99,0,0,99,0,0,98,0,0,102,0,0,102,0,0,102,0,0,
99,128,0,97,192,0,96,192,0,104,192,0,109,192,0,103,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,7,192,0,28,224,0,48,48,0,32,48,0,96,0,0,96,0,0,96,0,0,96,0,0,96,
0,0,96,16,0,32,48,0,48,112,0,31,224,0,15,128,0,2,0,0,1,0,0,3,0,0,7,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,30,0,0,59,128,0,97,128,0,96,0,0,96,0,
0,96,0,0,96,128,0,97,128,0,59,128,0,30,0,0,12,0,0,6,0,0,6,0,0,28,0,0,4,64,0,3,
192,0,0,0,0,7,192,0,28,240,0,48,48,0,32,24,0,96,0,0,96,0,0,96,0,0,97,248,0,96,248,0,
96,24,0,32,24,0,48,24,0,28,112,0,7,192,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,48,0,
0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,
0,0,48,0,0,48,0,0,48,0,0,48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12,192,0,
0,0,0,7,128,0,28,224,0,48,48,0,32,16,0,96,24,0,96,24,0,96,24,0,96,24,0,96,24,0,96,24,
0,32,16,0,48,48,0,28,224,0,7,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,31,0,0,49,128,0,96,192,0,96,64,0,96,0,0,56,0,0,31,0,0,7,192,0,0,192,0,64,96,0,
64,96,0,96,192,0,63,192,0,31,0,0,4,0,0,2,0,0,6,0,0,14,0,0,0,0,0,25,128,0,0,0,
0,96,96,0,96,96,0,96,96,0,96,96,0,96,96,0,96,96,0,96,96,0,96,96,0,96,96,0,96,96,0,96,
96,0,32,64,0,57,192,0,31,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
17,0,0,18,0,0,14,0,0,0,0,0,30,128,0,51,128,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,
0,97,128,0,51,128,0,29,128,0,1,128,0,97,128,0,51,0,0,30,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,48,0,0,
48,0,0,48,0,0,48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,27,0,
0,0,0,0,0,0,0,0,0,0,30,0,0,59,128,0,97,128,0,96,128,0,96,192,0,96,192,0,96,128,0,97,
128,0,59,128,0,30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,62,0,0,119,0,0,97,0,0,96,0,0,60,0,0,15,0,0,1,128,0,97,128,
0,115,0,0,62,0,0,8,0,0,4,0,0,12,0,0,28,0,0,0,0,0,0,0,0,0,0,0,54,0,0,0,
0,0,0,0,0,0,0,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,97,128,0,
119,128,0,61,128,0,0,0,0,0,0,0,0,0,0,0,0,0,4,128,0,7,0,0,0,0,0,7,128,0,28,224,
0,48,96,0,32,48,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,16,0,32,48,0,48,96,0,28,
224,0,15,128,0,0,0,0,0,0,0,0,0,0,0,0,0,1,128,0,3,0,0,0,0,0,7,128,0,28,224,0,
48,96,0,32,48,0,96,0,0,96,0,0,96,0,0,96,0,0,96,0,0,96,16,0,32,48,0,48,96,0,28,224,
0,15,128,0,0,0,0,0,0,0,0,0,0,0,0,0,27,0,0,14,0,0,4,0,0,31,0,0,49,128,0,96,
192,0,96,64,0,96,0,0,120,0,0,63,0,0,7,192,0,0,192,0,64,96,0,64,96,0,96,64,0,57,192,0,
31,0,0,0,0,0,0,0,0,0,0,0,0,0,0,13,128,0,7,0,0,2,0,0,63,224,0,0,224,0,0,192,
0,1,192,0,1,128,0,3,0,0,6,0,0,6,0,0,12,0,0,24,0,0,56,0,0,48,0,0,112,0,0,127,
224,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,27,0,0,30,0,0,12,0,0,
0,0,0,30,0,0,59,0,0,97,128,0,96,0,0,96,0,0,96,0,0,96,128,0,97,128,0,51,0,0,30,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,0,4,0,0,12,0,0,0,
0,0,30,0,0,59,0,0,97,128,0,96,0,0,96,0,0,96,0,0,96,128,0,97,128,0,51,0,0,30,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,54,0,0,28,0,0,12,0,0,0,0,
0,62,0,0,119,0,0,97,0,0,96,0,0,60,0,0,15,0,0,1,128,0,97,128,0,115,0,0,62,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,27,0,0,30,0,0,12,0,0,0,0,0,
127,128,0,3,128,0,3,0,0,6,0,0,12,0,0,8,0,0,24,0,0,48,0,0,96,0,0,127,128,0,0,0,
0,0,0,0,0,0,0,0,0,0,
};
/*Bitmap Raw Data end ---*/

static Font_header_t fntHeader;

void Font_Test( void )
{
	Font_header_t * pfntHeader = &fntHeader;
	
	// load custom font into GRAM
	/*
	Wdog();
	Ft_Gpu_Hal_WrMem(pHost, RAM_G + 0,	MetricBlock,sizeof(MetricBlock));
	Wdog();
	Ft_Gpu_Hal_WrMem(pHost, RAM_G + 0 + sizeof(MetricBlock),FontBmpData,sizeof(FontBmpData));
	Wdog();
	*/
	Wdog();
	memcpy( &pfntHeader->FontTable, MetricBlock, sizeof(FT_Gpu_Fonts_t) );
	pfntHeader->Font_RawData = (unsigned long)FontBmpData;
	pfntHeader->Font_RawDataSize = sizeof(FontBmpData);
	pfntHeader->FontTableAddr = 7848;
	Ft_Gpu_Hal_WrMem(pHost, RAM_G+pfntHeader->FontTableAddr,	&pfntHeader->FontTable, sizeof(FT_Gpu_Fonts_t) );
	Wdog();
	Ft_Gpu_Hal_WrMem(pHost, RAM_G+pfntHeader->FontTableAddr+sizeof(FT_Gpu_Fonts_t), pfntHeader->Font_RawData, pfntHeader->Font_RawDataSize );
	Wdog();

	// register custom font with handle 7
	Ft_Gpu_CoCmd_Dlstart(pHost);
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE

	Ft_App_WrCoCmd_Buffer(pHost,BITMAP_HANDLE(7));
	Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(pfntHeader->FontTable.PointerToFontGraphicsData));
	Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(pfntHeader->FontTable.FontBitmapFormat, pfntHeader->FontTable.FontLineStride, pfntHeader->FontTable.FontHeightInPixels));
	Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, pfntHeader->FontTable.FontWidthInPixels, pfntHeader->FontTable.FontHeightInPixels));
	Ft_Gpu_CoCmd_SetFont(pHost, 7, RAM_G+pfntHeader->FontTableAddr);

	Ft_Gpu_CoCmd_Text( pHost, 0, 0, 28, 0, String2Font( "New font loaded", 28 ) );
	
	Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
	Ft_Gpu_CoCmd_Swap( pHost );
	Ft_App_Flush_Co_Buffer( pHost );
	Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

	Ft_Gpu_Hal_Sleep(2000);
		
	Ft_Gpu_CoCmd_Dlstart(pHost);
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
	
	Ft_Gpu_CoCmd_Text( pHost, 0, 60, 28, 0, String2Font( "ALEssandro 1963 \200  VA.BER.", 28 ) );
	Ft_Gpu_CoCmd_Text( pHost, 0, 90, 7, 0, String2Font( "ALEssandro 1963 \200  VA.BER.", 7 ) );
	
	Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
	Ft_Gpu_CoCmd_Swap( pHost );
	Ft_App_Flush_Co_Buffer( pHost );
	Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
	
	
	
	// end
	for( ; ; Wdog() )
	{
		Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
	}
	
}

#endif

#ifdef TOUCH_CALIBRATE

// touch calibration values
#ifdef LCD_INVERTED_LANDSCAPE
static const ft_uint32_t CalibTransMatrix[6] =
{
	0x00000007,		// A
	0xFFFEFDBF,		// B
	0x01E205FC,		// C
	0xFFFEF0FA,		// D
	0x0000044C,		// E
	0x010D9D1C,		// F
};
#else
static const ft_uint32_t CalibTransMatrix[6] =
{
	0x00000000,		// A
	0x00010000,		// B
	0x00010000,		// C
	0x0000FF36,		// D
	0xFFFFFF8F,		// E
	0xFFFF2AC0,		// F
};
#endif

ft_void_t user_Touch_Calibrate( void )
{
	ft_uint32_t TransMatrix[6];

	/*************************************************************************/
	/* Below code demonstrates the usage of calibrate function. Calibrate    */
	/* function will wait untill user presses all the three dots. Only way to*/
	/* come out of this api is to reset the coprocessor bit.                 */
	/*************************************************************************/
	
	/* Print the configured values */
	Ft_Gpu_Hal_RdMem(pHost,REG_TOUCH_TRANSFORM_A,(ft_uint8_t *)TransMatrix,4*6);//read all the 6 coefficients
#ifdef LOGGING
	myprintf("Touch screen transform initial values: \r\n\tA 0x%08X\r\n\tB 0x%08X\r\n\tC 0x%08X\r\n\tD 0x%08X\r\n\tE 0x%08X\r\n\tF 0x%08X\r\n",
		TransMatrix[0],TransMatrix[1],TransMatrix[2],TransMatrix[3],TransMatrix[4],TransMatrix[5]);
#endif
		
#ifdef TOUCH_CALIBRATION_AND_READ
	// Ft_Gpu_CoCmd_Dlstart(pHost);

	Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(64,64,64));
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xff,0xff,0xff));
	//Draw number at 0,0 location
	//Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(30));
	Ft_Gpu_CoCmd_Text(pHost,(FT_DispWidth/2), (FT_DispHeight/2), 27, OPT_CENTER, "Please Tap on the dot");
	Ft_Gpu_CoCmd_Calibrate(pHost,0);

	// Download the commands into FIFO
	Ft_App_Flush_Co_Buffer(pHost);
	// Wait till coprocessor completes the operation
	Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);
#else
	// write all the 6 coefficients
	Ft_Gpu_Hal_WrMem(pHost, REG_TOUCH_TRANSFORM_A, (ft_uint8_t *)CalibTransMatrix, 4*6);
#endif
	
	// Print the configured values
	Ft_Gpu_Hal_RdMem(pHost,REG_TOUCH_TRANSFORM_A,(ft_uint8_t *)TransMatrix,4*6);//read all the 6 coefficients
#ifdef LOGGING
	myprintf("Touch screen transform calibrated values: \r\n\tA 0x%08X\r\n\tB 0x%08X\r\n\tC 0x%08X\r\n\tD 0x%08X\r\n\tE 0x%08X\r\n\tF 0x%08X\r\n",
		TransMatrix[0],TransMatrix[1],TransMatrix[2],TransMatrix[3],TransMatrix[4],TransMatrix[5]);
#endif

}
#endif

#ifdef TOUCH_TEST
ft_void_t user_Touch( void )
{
	ft_int32_t wbutton,hbutton,tagval,tagoption;
	ft_char8_t StringArray[100];
	//ft_char8_t StringArray1[100];
	//ft_uint32_t ReadWord;
	ft_int16_t xvalue,yvalue;
	unsigned char TouchStatus = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;


	/*************************************************************************/
	/* Below code demonstrates the usage of touch function. Display info     */
	/* touch raw, touch screen, touch tag, raw adc and resistance values     */
	/*************************************************************************/
	wbutton = FT_DispWidth/8;
	hbutton = FT_DispHeight/8;
	// Ft_Gpu_Hal_Wr8(pHost,REG_CTOUCH_EXTENDED, CTOUCH_MODE_EXTENDED);
	tagval = 0xFFFF;
	xvalue = -1;
	yvalue = -1;
	while(1)
	{

		Ft_Gpu_CoCmd_Dlstart(pHost);
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(64,64,64));
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xff,0xff,0xff));
		//Ft_App_WrCoCmd_Buffer(pHost,TAG_MASK(0));

		// StringArray[0] = '\0';
		// strcat(StringArray,"Touch Screen XY0 (");
		strcpy(StringArray,"Touch Screen XY (");
		// ReadWord = Ft_Gpu_Hal_Rd32(pHost, REG_CTOUCH_TOUCH0_XY);
  		// yvalue = (ReadWord & 0xffff);
		// xvalue = (ReadWord>>16);
		Ft_Gpu_Hal_Dec2Ascii(StringArray,(ft_int32_t)xvalue);
		strcat(StringArray,",");
		Ft_Gpu_Hal_Dec2Ascii(StringArray,(ft_int32_t)yvalue);
		strcat(StringArray,")");
		Ft_Gpu_CoCmd_Text(pHost,FT_DispWidth/2, 50, 26, OPT_CENTER, StringArray);

		if( TouchStatus )
		{
			strcpy(StringArray,"Touched");
		}
		else
		{
			strcpy(StringArray,"Untouched");
		}
		Ft_Gpu_CoCmd_Text(pHost,FT_DispWidth/2, 70, 26, OPT_CENTER, StringArray);

		/*StringArray[0] = '\0';
		strcat(StringArray,"Touch Screen XY1 (");
		ReadWord = Ft_Gpu_Hal_Rd32(pHost, REG_CTOUCH_TOUCH1_XY);
  		yvalue = (ReadWord & 0xffff);
		xvalue = (ReadWord>>16);
		Ft_Gpu_Hal_Dec2Ascii(StringArray,(ft_int32_t)xvalue);
		strcat(StringArray,",");
		Ft_Gpu_Hal_Dec2Ascii(StringArray,(ft_int32_t)yvalue);
		strcat(StringArray,")");
		Ft_Gpu_CoCmd_Text(pHost,FT_DispWidth/2, 70, 26, OPT_CENTER, StringArray);

		StringArray[0] = '\0';
		strcat(StringArray,"Touch Screen XY2 (");
		ReadWord = Ft_Gpu_Hal_Rd32(pHost, REG_CTOUCH_TOUCH2_XY);
  		yvalue = (ReadWord & 0xffff);
		xvalue = (ReadWord>>16);
		Ft_Gpu_Hal_Dec2Ascii(StringArray,(ft_int32_t)xvalue);
		strcat(StringArray,",");
		Ft_Gpu_Hal_Dec2Ascii(StringArray,(ft_int32_t)yvalue);
		strcat(StringArray,")");
		Ft_Gpu_CoCmd_Text(pHost,FT_DispWidth/2, 90, 26, OPT_CENTER, StringArray);

		StringArray[0] = '\0';
		strcat(StringArray,"Touch Screen XY3 (");
		ReadWord = Ft_Gpu_Hal_Rd32(pHost, REG_CTOUCH_TOUCH3_XY);
  		yvalue = (ReadWord & 0xffff);
		xvalue = (ReadWord>>16);
		Ft_Gpu_Hal_Dec2Ascii(StringArray,(ft_int32_t)xvalue);
		strcat(StringArray,",");
		Ft_Gpu_Hal_Dec2Ascii(StringArray,(ft_int32_t)yvalue);
		strcat(StringArray,")");
		Ft_Gpu_CoCmd_Text(pHost,FT_DispWidth/2, 110, 26, OPT_CENTER, StringArray);

		StringArray[0] = '\0';
			StringArray1[0] = '\0';
		strcat(StringArray,"Touch Screen XY4 (");
		xvalue = Ft_Gpu_Hal_Rd16(pHost, REG_CTOUCH_TOUCH4_X);
		yvalue = Ft_Gpu_Hal_Rd16(pHost, REG_CTOUCH_TOUCH4_Y);
		Ft_Gpu_Hal_Dec2Ascii(StringArray,(ft_int32_t)xvalue);
		strcat(StringArray,",");
		Ft_Gpu_Hal_Dec2Ascii(StringArray1,(ft_int32_t)yvalue);
		strcat(StringArray1,")");
		strcat(StringArray,StringArray1);
		Ft_Gpu_CoCmd_Text(pHost,FT_DispWidth/2, 130, 26, OPT_CENTER, StringArray);

		StringArray[0] = '\0';
		strcat(StringArray,"Touch TAG (");
		ReadWord = Ft_Gpu_Hal_Rd8(pHost, REG_TOUCH_TAG);
		Ft_Gpu_Hal_Dec2Ascii(StringArray,ReadWord);
		strcat(StringArray,")");
		Ft_Gpu_CoCmd_Text(pHost,FT_DispWidth/2, 170, 26, OPT_CENTER, StringArray);
		tagval = ReadWord;*/

		strcpy(StringArray,"Touch TAG (");
		Ft_Gpu_Hal_Dec2Ascii(StringArray,tagval);
		strcat(StringArray,")");
		Ft_Gpu_CoCmd_Text(pHost,FT_DispWidth/2, 170, 26, OPT_CENTER, StringArray);


		Ft_Gpu_CoCmd_FgColor(pHost,0x008000);
		//Ft_App_WrCoCmd_Buffer(pHost,TAG_MASK(1));

		//Ft_App_WrCoCmd_Buffer(pHost,TAG(13));
		if(13 == tagval)
		{
			if( TouchStatus )
			{
				tagoption = OPT_FLAT;
			}
			else
			{
				tagoption = 0;
			}
		}
		Ft_Gpu_CoCmd_Button(pHost,(FT_DispWidth/2)- (wbutton/2) ,(FT_DispHeight*3/4) - (hbutton/2),wbutton,hbutton,26,tagoption,"Tag13");
		if( tagval == 0xFFFF )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();

			// button tag
			TagRegister( 13, (FT_DispWidth/2)- (wbutton/2), (FT_DispHeight*3/4) - (hbutton/2),
							wbutton, hbutton );

			// screen tag
			TagRegister( 0, 0, 0, FT_DispWidth, FT_DispHeight );
		}

		Ft_App_WrCoCmd_Buffer(pHost,DISPLAY());
		Ft_Gpu_CoCmd_Swap(pHost);

		/* Download the commands into fifo */
		Ft_App_Flush_Co_Buffer(pHost);

		/* Wait till coprocessor completes the operation */
		Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);
		//Ft_Gpu_Hal_Sleep(30);

		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, osWaitForever );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
				tagval = pt->tag;
				xvalue = pt->x;
				yvalue = pt->y;
				TouchStatus = pt->status;
			}
		} while( event.status != osEventMessage );

	}

	//Ft_Gpu_Hal_Wr8(pHost, REG_CTOUCH_EXTENDED, CTOUCH_MODE_COMPATABILITY);
	//Ft_Gpu_Hal_Sleep(30);
}
#endif
/*!
** \fn void User_Task( void const * argument )
** \brief User interface task
**/
void User_Task( void const * argument )
{	
	userStateHandler pf;
	
	if( !User_Init() )
	{
	#ifdef LOGGING
		myprintf("EVE Init error\r\n");
	#endif
	}

#ifdef TOUCH_CALIBRATE
	user_Touch_Calibrate();
#endif
	
	/* definition and creation of CTP */
	osThreadDef(CTP, CTP_Task, osPriorityNormal, 0, 1024);
	CTPHandle = osThreadCreate(osThread(CTP), NULL);

	Ft_Gpu_Hal_Sleep(10);
	
#ifdef BITMAP_TEST
	Bitmap_Test();
#endif
	
#ifdef FONT_TEST
	Font_Test();
#endif

#ifdef TOUCH_TEST
	user_Touch();
#endif

	AlarmInit();	// init alarm records handler

	userCurrentState = userVisResources();
	
	// task loop
	for( ; ; Wdog() )
	{
		switch( Check4StatusChange() )
		{
			case USER_STATE_LOW_POWER:
				// low-power
				userCurrentState = userVisLowPower();
			break;

			case USER_STATE_RESTART_OFF:
				// RESTART / SWITCH_OFF
				if( userCurrentState == USER_STATE_RESOURCES )
				{
					/*
					We cannot display the Restart/Off screen
					with the graphic resources unavailable.
					Power off in any case.
					*/
					userVisShutDown( "Power off...", 2000 );
					SystemOff();
					// ------> NON PASSA MAI DI QUI
				}
				else
				{
					userCurrentState = USER_STATE_RESTART_OFF;
				}
			break;

			case USER_STATE_PAUSE:
				// STANDBY
			#ifdef LOGGING
				if( UserRequest == USER_REQ_STANDBY )
				{
					myprintf( "Pausa in standby per comando utente\r\n" );
				}
				else if( cntPausa )
				{
					myprintf( "Pausa in standby per fine ciclo richieste\r\n" );
				}
			#endif
				if( (userCurrentState == USER_STATE_AVVIO)
					|| (userCurrentState == USER_STATE_AVVIO_ALLARME)
					)
				{
					ReleaseAllBitmapAvvio();
				}
				UserRequest = USER_REQ_NONE;
				userCurrentState = userVisPause();
			break;

			case USER_STATE_AVVIO:
				// WAKEUP
			#ifdef LOGGING
				myprintf( "Exit da pausa in standby per comando utente\r\n" );
			#endif
				userCurrentState = userExitPause();
			break;
			
			case USER_STATE_COLLAUDO:
				// test entry
				//userCurrentState = userCollaudoInit();
			break;
			
			case USER_STATE_INIT:
				// PH2 is disconnected
				userCurrentState = userVisInit();
			break;
			
			case USER_STATE_RESET:
				// PH2 is in reset
				//userCurrentState = userVisReset();
				userVisReset();
				OT_RestartRequests();
				// start waiting for logo and Tipo Stufa
				userCurrentState = USER_STATE_INIT;
			break;
		}
		
		// handle the current user status
		pf = userStateTable[userCurrentState];
		if( pf != NULL )
		{
			userCurrentState = pf();
		}
		else
		{
			Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
		}
	}
	
}

/*!
** \fn unsigned char Check4StatusChange( void )
** \brief Check for a mandatory status change event
** \return The next User Interface state, USER_MAX_STATES if no status change is required
**/
unsigned char Check4StatusChange( void )
{
	unsigned char RetVal = USER_MAX_STATES;
	BATT_STRUCT *pt = &VBatt_tab;
	
	/*!
	** Vengono verificati eventi a priorit� crescente
	**/

	// check if PH2 is in reset
	if(
		(userCurrentState >= USER_STATE_AVVIO) &&
		(userCurrentState <= USER_STATE_ALLARME) &&
		(OT_Stat == OT_STAT_LINKED) &&
		(STATO_STUFA == STUFASTAT_IN_RESET) &&
		(STATO_ALL_BLACK != PAN_NO_TIPO)
		)
	{
		// PH2 is in reset
		RetVal = USER_STATE_RESET;
	}
	
	// check if PH2 is disconnected
	//if( OT_Stat == OT_STAT_LINKED )
	if( OT_Stat >= OT_STAT_UNLINKED_WRITE_VERFW )	// at least verSW was received
	{
		// restart disconnection timeout
		starttimer( TDISCONNECTED, 10*T1SEC );
	}
	if(
		(userCurrentState > USER_STATE_INIT) &&
		(userCurrentState < USER_STATE_COLLAUDO) &&
		(userCurrentState != USER_STATE_LOCAL_SETTINGS) &&
		//(OT_Stat != OT_STAT_LINKED)
		(OT_Stat < OT_STAT_UNLINKED_WRITE_VERFW)	// verSW is being requested
		)
	{
		// restart user interface after 10s of disconnection
		if( checktimer( TDISCONNECTED ) != INCORSO )
		{
		#ifndef DEBUG_CONTINUE_IF_NO_LINK
			RetVal = USER_STATE_INIT;
		#endif
		}
	}

	// check livello minimo batteria
	if(
		(userCurrentState < USER_STATE_COLLAUDO) &&
		(OpeMode == MODE_FULL_POWER) &&
		!DIGIN_ALIM &&
		(VBatt < pt->VBattLow)
		)
	{
		// forzo ingresso in low-power
		RetVal = USER_STATE_LOW_POWER;
	}
	
	// check richiesta utente pausa STANDBY
	if( 
		(userCurrentState >= USER_STATE_INIT) &&
		(userCurrentState < USER_STATE_COLLAUDO) &&
		(	(UserRequest == USER_REQ_STANDBY)
			|| cntPausa
		)
		)
	{
		// ingresso in pausa STANDBY
		RetVal = USER_STATE_PAUSE;
	}
	
	// check richiesta utente WAKEUP
	if(
		(userCurrentState == USER_STATE_PAUSE) &&
		(UserRequest == USER_REQ_WAKEUP)
		)
	{
		// uscita da pausa STANDBY
		RetVal = USER_STATE_AVVIO;
	}

	// check richiesta utente RESTART / SWITCH_OFF
	if( UserRequest == USER_REQ_OFF )
	{
		// richiede conferma RESTART / SWITCH_OFF
		RetVal = USER_STATE_RESTART_OFF;
	}

	// check for test entry
	if( 
		(STATO_ALL_BLACK == PAN_COLLAUDO) &&
		(userCurrentState != USER_STATE_COLLAUDO)
		)
	{
		RetVal = USER_STATE_COLLAUDO;
	}
	
	return RetVal;
}

/*!
** \fn void userSetTempUM( void )
** \brief Set the measure unit for temperatures
** \return None
**/
void userSetTempUM( void )
{
	switch( SHOWTEMP )
	{
		default:
		case SHOW_TEMP_AUTO:
			// L'unit� di misura visualizzata dipende dalla versione della scheda
			// DA FARE!!! (per ora AUTO = CELSIUS)

		case SHOW_TEMP_CELSIUS:
			// Disabilito la temperatura visualizzata in Fahreneit
			TEMPFAHRENHEIT = 0;
		break;

		case SHOW_TEMP_FAHRENHEIT:
			// Abilito la temperatura visualizzata in Fahreneit
			TEMPFAHRENHEIT = 1;
		break;
	}
}

/*!
	\fn void userGetServicePassword( unsigned char *Lettera, unsigned char *Numero )
	\brief Ritorna la password di accesso assistenza
	\param Lettera password parte letterale
	\param Numero password parte numerica
	\return None
*/
void userGetServicePassword( unsigned char *Lettera, unsigned char *Numero )
{
	const char *s;

	/*
		La parte letterale e` la lettera iniziale in INGLESE del giorno corrente
	*/
	s = pSetMsg[LANG_ENG][sWeekDay_tab[DateTime.tm_wday]];
	*Lettera = s[0];

	/*
		La parte numerica e` la somma modulo 100 di giorno, mese, anno (ultime
		due cifre) della data corrente
	*/
	*Numero = DateTime.tm_mday;
	*Numero += DateTime.tm_mon;
	*Numero += (DateTime.tm_year % 100);
	*Numero %= 100;
}

/*!
** \fn void ShowWaitSetResultPopup( void )
** \brief Show the waiting setting result pop-up window
** \return None
** \note This function is based on the 'WaitSetResult' structure and has to be
** 		used only within a co-processor engine command list.
**/
void ShowWaitSetResultPopup( void )
{
	short i;
	BM_HEADER * p_bmhdr;
	
	// draw the pop-up window
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,0,96));	// background color
	Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
	Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 30*16, 60*16 ) );
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 450*16, 210*16 ) );
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
	Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
	Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 30*16, 60*16 ) );
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 450*16, 60*16 ) );
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 450*16, 210*16 ) );
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 30*16, 210*16 ) );
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 30*16, 60*16 ) );
	Ft_App_WrCoCmd_Buffer(pHost,END());
	Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (30+3)*16, (60+3)*16 ) );
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (450-3)*16, (60+3)*16 ) );
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (450-3)*16, (210-3)*16 ) );
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (30+3)*16, (210-3)*16 ) );
	Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (30+3)*16, (60+3)*16 ) );
	Ft_App_WrCoCmd_Buffer(pHost,END());
	
	// title
	if( WaitSetResult.sTitle != NULL )
	{
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 60+3+15, fontDesc, OPT_CENTER, String2Font(WaitSetResult.sTitle, fontDesc) );
	}

	// description
	if( WaitSetResult.sDesc != NULL )
	{
		if( (i = GetFont4StringWidth( (char *)WaitSetResult.sDesc, fontDesc, ((450-5)-(30+5)) )) > 0 )
		{
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 60+3+45, i, OPT_CENTER, String2Font(WaitSetResult.sDesc, i) );
		}
	}

	// setting result
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
	switch( WaitSetResult.result )
	{
		case OT_RESULT_RUN:
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2II( (FT_DispWidth/2-8), (164-12), FONT_MAX_DEF, charStDownload[StDownload] ) );
			if( ++StDownload >= NSTAT_DOWNLOAD )
				StDownload = 0;
		break;

		case OT_RESULT_OK:
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(255));
			Ft_App_WrCoCmd_Buffer(pHost, BLEND_FUNC( SRC_ALPHA, SRC_ALPHA ) );	// overlay
			// get bitmap OK into Graphic RAM
			p_bmhdr = res_bm_GetHeader( BITMAP_OK );
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( ((FT_DispWidth-p_bmhdr->bmhdr.Width)/2)*16, (164-p_bmhdr->bmhdr.Height/2)*16 ) );
			Ft_App_WrCoCmd_Buffer(pHost, BLEND_FUNC( SRC_ALPHA, ONE_MINUS_SRC_ALPHA ) );	// default blend
		break;
		
		case OT_RESULT_FAILED:
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(255));
			Ft_App_WrCoCmd_Buffer(pHost, BLEND_FUNC( SRC_ALPHA, SRC_ALPHA ) );	// overlay
			// get bitmap QUIT into Graphic RAM
			p_bmhdr = res_bm_GetHeader( BITMAP_QUIT );
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( ((FT_DispWidth-p_bmhdr->bmhdr.Width)/2)*16, (164-p_bmhdr->bmhdr.Height/2)*16 ) );
			Ft_App_WrCoCmd_Buffer(pHost, BLEND_FUNC( SRC_ALPHA, ONE_MINUS_SRC_ALPHA ) );	// default blend
		break;
	}
}



/*!
	\fn unsigned char ExecTipoStufa( void )
	\brief Esegue il set parametro P_TIPOSTUFA
	\return Il codice dello stato di transizione
*/
unsigned char ExecTipoStufa( void )
{
	userSetPar( P_TIPOSTUFA );

	// la stufa dovrebbe entrare in reset -> richiedo il suo stato
	OT_Send( (PKT_STRUCT *)&reqInputStato_Misure_1, SEND_MODE_ONE_SHOT, 0 );
	// reinvio dati pannello
	SendVerFW();

	return userCurrentState;
}

/*!
	\fn unsigned char ExecTipoDef( void )
   \brief Esegue il Ripristino Tipo Stufa al Default
	\return Il codice dello stato di transizione
*/
unsigned char ExecTipoDef( void )
{
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;

	sPkt.pktLen = 0;
	sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
	sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
	sPkt.pkt[sPkt.pktLen++] = MB_FUNC_TYPE_SET;	// cmd
	sPkt.pkt[sPkt.pktLen++] = TIPO_STUFA;	// tipo stufa
	sPkt.pkt[sPkt.pktLen++] = 2;	// param (tipo in FLASH)
	OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );

	// la stufa dovrebbe entrare in reset -> richiedo il suo stato
	OT_Send( (PKT_STRUCT *)&reqInputStato_Misure_1, SEND_MODE_ONE_SHOT, 0 );

	//return RetMenuGen();
	return userCurrentState;
}

/*!
	\fn unsigned char ExecUpdateLang( void )
   \brief Esegue al set nuova lingua
	\return Il codice dello stato di transizione
*/
unsigned char ExecUpdateLang( void )
{
	userSetPar( P_LINGUA );
	// Predispongo memorizzazione su RTC (fatta ogni 1s per tutta la struttura 'StatSec')
	StatSec.Language = iLanguage;
	return userCurrentState;
}

/*! 
	\fn unsigned char ExecSetTempUM( void )
   \brief Esegue al set nuova lingua
	\return Il codice dello stato di transizione
*/
unsigned char ExecSetTempUM( void )
{
	userSetPar( P_SHOWTEMP );
	userSetTempUM();	// Imposta l'unita' di misura delle temperature visualizzate
	return userCurrentState;
}

/*!
	\fn unsigned char ExecBypass( void )
   \brief Esegue la procedura di Bypass
	\return Il codice dello stato di transizione
*/
unsigned char ExecBypass( void )
{
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;

	sPkt.pktLen = 0;
	sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
	sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
	sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
	sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_BYPASS_ACC;
	sPkt.pkt[sPkt.pktLen++] = 0;
	sPkt.pkt[sPkt.pktLen++] = 0;
	sPkt.pkt[sPkt.pktLen++] = 0;
	sPkt.pkt[sPkt.pktLen++] = 0;
	OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );

	//return RetMenuCmd();
	return userCurrentState;
}

/*!
	\fn unsigned char ExecBypassTra( void )
   \brief Esegue la procedura di Bypass
	\return Il codice dello stato di transizione
*/
unsigned char ExecBypassTra( void )
{
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;

	sPkt.pktLen = 0;
	sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
	sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
	sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
	sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_BYPASS_ACC;
	sPkt.pkt[sPkt.pktLen++] = 0;
	sPkt.pkt[sPkt.pktLen++] = 0;
	sPkt.pkt[sPkt.pktLen++] = 0;
	sPkt.pkt[sPkt.pktLen++] = 0;
	OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );

	//return RetMenuParTra();
	return userCurrentState;
}

/*!
	\fn unsigned char ExecTaraTC( void )
   \brief Esegue la procedura di taratura termocoppia
	\return Il codice dello stato di transizione
*/
unsigned char ExecTaraTC( void )
{
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;

	sPkt.pktLen = 0;
	sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
	sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
	sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
	sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_CALIB_TC;
	sPkt.pkt[sPkt.pktLen++] = 1;
	sPkt.pkt[sPkt.pktLen++] = 0;
	sPkt.pkt[sPkt.pktLen++] = 0;
	sPkt.pkt[sPkt.pktLen++] = 0;
	OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );

	//return RetMenuCmd();
	return userCurrentState;
}

/*! \fn unsigned char ExecSetRFid( void )
    \brief Esegue il set param P_RFPANEL
	 \return Il codice dello stato di transizione
*/
unsigned char ExecSetRFid( void )
{
	// memorizzo in EEPROM
	ScriviRFid( tmpRFid );
	rfID = RFid;	// valore corrente per identificativo radio
	
	//OT_Init();	// reinizializzo processo comunicazione radio

	return userCurrentState;
}

/*! \fn unsigned char ExecLocalSetRFid( void )
    \brief Esegue il set param P_RFPANEL nel menu' Local Settings
	 \return Il codice dello stato di transizione
*/
unsigned char ExecLocalSetRFid( void )
{
	tmpRFid = userParValue.b[0];

	return ExecSetRFid();
}



/*!	\fn void userVisShutDown( char *s, unsigned long visTime )
    \brief Visualizzazione messaggio di shut-down
    \param s Il messaggio da visualizzare (max 21 caratteri)
    \param visTime Il tempo (ms) di visualizzazione del messaggio
	\return None
*/
void userVisShutDown( char *s, unsigned long visTime )
{
	Ft_Gpu_CoCmd_Dlstart(pHost);
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE

	Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, FT_DispHeight/2, FONT_TITLE_DEF, OPT_CENTER, s );

	Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
	Ft_Gpu_CoCmd_Swap( pHost );
	Ft_App_Flush_Co_Buffer( pHost );
	Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

	visTime /= USER_TIME_TICK;
	while( visTime )
	{
		Wdog();
		Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
		visTime--;
	}

}

/*! \fn unsigned char userVisLowPower( void )
    \brief Visualizzazione nello stato USER_STATE_LOW_POWER
	\return Il codice dello stato di transizione
*/
unsigned char userVisLowPower( void )
{
	SetBacklight( LCD_BACKLIGHT_MID );

	userVisShutDown( "Battery OFF...", 2000 );

	return USER_STATE_LOW_POWER;
}

/*!
** \fn unsigned char userVisPause( void )
** \brief Visualizzazione nello stato USER_STATE_PAUSE
** \return Il codice dello stato di transizione
**/
unsigned char userVisPause( void )
{
#ifdef DISCHARGE_BATTERY
	// annulla ingresso in pausa
	return userExitPause();
#endif
	if( userCurrentState != USER_STATE_PAUSE )
	{
		// black screen
		SetBacklight( 0 );
		userVisShutDown( "", 0 );
		
		// switch FT801 from Active to Sleep mode (clocks disabled, all registers retained)
		//Play mute sound to avoid pop sound
		Ft_Gpu_Hal_Wr16( pHost, REG_SOUND, 0x0060 );
		Ft_Gpu_Hal_Wr8( pHost, REG_PLAY, 0x01 );
		Ft_Gpu_PowerModeSwitch( pHost, FT_GPU_SLEEP_M );
		
		MX_SPI1_Init( CLK_SLOW );
	}
	
	// comando ingresso in pausa
	OpeMode = MODE_STANDBY;
	
	return USER_STATE_PAUSE;
}

/*! \fn unsigned char userPause( void )
    \brief Gestione dello stato USER_STATE_PAUSE
	 \return Il codice dello stato di transizione
*/
static unsigned char userPause( void )
{
	unsigned char RetVal = USER_STATE_PAUSE;
	unsigned char loop = 1;
	unsigned long logoTimeout = TIMEOUT_UNLINKED;

	OT_DL_Pausa = OT_DL_PAUSE;

	/*
	 * Reset STATO_STUFA e TIPO_ALLARME.
	 * Se stufa connessa, saranno aggiornati dal ciclo di richieste.
	 */
	STATO_STUFA = STUFASTAT_SPENTA;
	TIPO_ALLARME = NO_ALARM;

	while( loop )
	{
		Wdog();
		ALIM_INP = DIGIN_ALIM;
		Ft_Gpu_Hal_Sleep(USER_TIME_TICK);

		// connessione alimentazione esterna
		if( !ALIM_INP && DIGIN_ALIM )
		{
			// esce dalla pausa
			RetVal = userExitPause();
			cntExit4Alarm = 0;
			loop = 0;
		}
		else if( Check4StatusChange() != USER_MAX_STATES )
		{
			cntExit4Alarm = 0;
			loop = 0;
		}
		else if( OT_Stat == OT_STAT_LINKED )
		{
			logoTimeout = TIMEOUT_UNLINKED;
			// check stufa in allarme
			if( (TIPO_ALLARME != NO_ALARM)
				|| (STATO_STUFA == STUFASTAT_ALLARME)
				)
			{
				if( ++cntExit4Alarm <= MAX_NUM_EXIT4ALARM )
				{
					// esce dalla pausa
					RetVal = userExitPause();
					loop = 0;
				}
				else
				{
					cntExit4Alarm = MAX_NUM_EXIT4ALARM + 1;
					// comando la pausa lunga solo se non ci sono richieste pendenti
					if( (PeekPkt( SEND_MODE_ONE_SHOT ) == NULL)
						&& (iLimReq >= LIMITSREQ_NUM)
						)
					{
						userStartOTLongPause();
					}
				}
			}
			// check fine ciclo richieste
			else if( cntPausa )
			{
			#ifdef LOGGING
				if( OpeMode != MODE_STANDBY )
				{
					myprintf( "Ritorno in pausa standby per fine ciclo richieste\r\n" );
				}
			#endif
				// comando ingresso in pausa
				OpeMode = MODE_STANDBY;
			}
			// comando la pausa lunga solo se non ci sono richieste pendenti
			else if( (PeekPkt( SEND_MODE_ONE_SHOT ) == NULL)
					&& (iLimReq >= LIMITSREQ_NUM)
					)
			{
				userStartOTLongPause();
			}
		}
		else if( OT_Stat < OT_STAT_UNLINKED_WRITE_VERFW )
		{
			if( logoTimeout )
			{
				if( --logoTimeout == 0 )
				{
					// connection timeout
				#ifdef LOGGING
					myprintf( "Pausa in standby per timeout connessione\r\n" );
				#endif
					// comando ingresso in pausa
					OpeMode = MODE_STANDBY;
				}
			}
			else
			{
				// comando ingresso in pausa
				OpeMode = MODE_STANDBY;
			}
		}
		else
		{
			logoTimeout = TIMEOUT_UNLINKED;
		}
	}

	return RetVal;
}


/*!
** \fn unsigned char userExitPause( void )
** \brief Uscita dallo stato USER_STATE_PAUSE
** \return Il codice dello stato di transizione
**/
unsigned char userExitPause( void )
{
	if( userCurrentState == USER_STATE_PAUSE )
	{
		// switch FT801 from Sleep to Active mode
		Ft_Gpu_PowerModeSwitch( pHost, FT_GPU_ACTIVE_M );
		Ft_Gpu_Hal_Sleep(40);

		// Now FT800 can accept commands at up to 30MHz clock on SPI bus
		MX_SPI1_Init( CLK_FAST );
	}
	
	// flush the touch queue
	CTP_TagDeregisterAll();
	FlushTouchQueue();
	
	// ricarica timeout disconnessione
	starttimer( TDISCONNECTED, 100 );
	
	if( OT_NextStat == OT_STAT_LINKED )
	{
		// visualizzazione nello stato USER_STATE_AVVIO
		userCurrentState = userVisAvvio();
	}
	else
	{
		// PH2 is disconnected
		userCurrentState = userVisInit();
	}

	UserRequest = USER_REQ_NONE;
	OpeMode = MODE_FULL_POWER;
	
	return userCurrentState;
}

/*!
** \fn void userStartOTLongPause( void )
** \brief Comanda la pausa lunga al processo OT/+
** \return None
**/
void userStartOTLongPause( void )
{
#ifndef DISCHARGE_BATTERY
	if( !DIGIN_ALIM )	// mai automaticamente in pausa standby con alimentazione esterna presente
	{
		OT_DL_Pausa = OT_DL_LONG_PAUSE;
	}
#endif
}

/*!
**  \fn unsigned short GetVisPercVBatt( void )
**  \brief Rileva la percentuale di carica batteria da visualizzare
**  \return Il valore in 0.1% della percentuale di carica batteria da visualizzare
**/
unsigned short GetVisPercVBatt( void )
{
	static unsigned short prePercVBatt = 0;
	unsigned short RetVal = 0;
	
	/* Con alimentazione esterna presente il valore da visualizzare non puo'
		mai essere minore del valore visualizzato in precedenza */
	if( DIGIN_ALIM )
	{
		if( PercVBatt > prePercVBatt )
		{
			prePercVBatt = PercVBatt;
			RetVal = PercVBatt;
		}
		else
		{
			RetVal = prePercVBatt;
		}
	}
	else
	{
		prePercVBatt = 0;
		RetVal = PercVBatt;
	}
	
	return RetVal;
}




/*!
** \fn short userIsXXX( void )
** \brief Check a PH2 parameter
** \return TRUE or FALSE
**/
short IsToView_PreCarPellet( void )
{
	return (short)(STATO_STUFA == STUFASTAT_SPENTA);
}
short IsToView_Pulizia( void )
{
	return (short)(STATO_STUFA == STUFASTAT_SPENTA);
}
short IsToView_AttivaPompa( void )
{
	return (short)(F_IDRO_PAN && (STATO_STUFA == STUFASTAT_SPENTA));
}
short IsServiceReq( void )
{
	return (short)(WARNING2_SERVICEREQ);
}
short IsSensAriaFail( void )
{
	return (short)(WARNING2_SONDAARIAFAIL);
}
short IsSensH2OFail( void )
{
	return (short)(WARNING2_SONDAACQUAFAIL);
}
short IsPressostH2OFail( void )
{
	return (short)(WARNING2_PRESSACQUAFAIL);
}
short IsPressH2OAlarm( void )
{
	return (short)(WARNING2_PREALMPRESSIDRO);
}
short IsSensPortFail( void )
{
	return (short)(NOTIFICA_GUASTO_PORTATA);
}
short IsFinePellet( void )
{
	return (short)(WARNING1_PREALMPELLET);
}
short IsPortaAperta( void )
{
	return (short)(WARNING1_PORTAAPERTA);
}
short userIsCrono( void )
{
	return (short)(!WIFI_CLIENT);
}
short userIsSleep( void )
{
#ifdef DEBUG_SLEEP
	return 1;
#else
	return (short)(LOGIC_ONOFF);
#endif
}
short userIsIdro( void )
{
	return (short)(F_IDRO_PAN /*&& !F_EN_ACCUMULO*/);
}
short IsCtrlPortata( void )
{
	return (short)(F_PORTATA_PAN);
}
short IsCtrlLambda( void )
{
	return (short)(F_EN_LAMBDA);
}
short IsFan1Info( void )
{
	return (short)((F_MULTIFAN >= SINGLE_FAN) && !EN_PULIT_COCLEA2);
}
short IsFan2Info( void )
{
	return (short)(
						!EN_ESP2 &&
						(
							(F_MULTIFAN >= DOUBLE_FAN) ||
							((F_MULTIFAN == SINGLE_FAN) && EN_PULIT_COCLEA2)
						)
					);
}
short IsPressIdro( void )
{
	return (short)(F_PRESS_IDRO_PAN);
}
short IsWiFi( void )
{
#ifdef SIMUL_WIFI
	return 1;
#else
	return (short)(EN_IP_NETWORK);
#endif
}
short IsWiFiNet( void )
{
	return (short)(EN_IP_NETWORK && !WIFI_CLIENT);
}
short IsCtrlAria( void )
{
	return (short)(F_PORTATA_PAN || F_HALL_PAN);
}
short IsSensPellet( void )
{
	return (short)(F_PELLET_PAN);
}
short IsTermocoppia( void )
{
	return (short)(F_ACC_TERMOCOPPIA);
}
short IsTempoCoclea( void )
{
	return (short)(!EN_COCLEA_HALL);
}
short IsGiriCoclea( void )
{
	return (short)(EN_COCLEA_HALL);
}
short IsCoclea2( void )
{
	return (short)(EN_PULIT_COCLEA2);
}
short IsTOffCoclea( void )
{
	return (short)(!EN_COCLEA_HALL && !EN_PERIODO_COCLEA);
}
short IsPeriodoCoclea( void )
{
	return (short)(!EN_COCLEA_HALL && EN_PERIODO_COCLEA);
}
short IsTipoFrenCoclea( void )
{
	return (short)(!EN_COCLEA_HALL && F_FRENATA_COCLEA);
}
short IsEsp2( void )
{
	return (short)(EN_ESP2 && !EN_FAN2_HALL);
}
short IsEsp2Giri( void )
{
	return (short)(EN_ESP2 && EN_FAN2_HALL);
}
short IsTOffCoclea2( void )
{
	return (short)(EN_PULIT_COCLEA2 && !EN_PERIODO_FAN1);
}
short IsPeriodoCoclea2( void )
{
	return (short)(EN_PULIT_COCLEA2 && EN_PERIODO_FAN1);
}
short IsTipoFrenCoclea2( void )
{
	return (short)(EN_PULIT_COCLEA2 && F_FRENATA_FAN1_COCLEA2);
}
short IsVisSanit( void )
{
	return (short)(userIsIdro() && VIS_SANITARI);
}
short IsRipristino( void )
{
	return (short)(F_EN_RIPRISTINO);
}
short IsTMaxPreAlm( void )
{
	return (short)(!DISAB_CNTPALMFUMI);
}
short IsTermostato( void )
{
	return (short)(F_EN_TERMAMB /*|| F_EN_ACCUMULO*/);
}
short IsDTempEco( void )
{
	return (short)(!IsTermostato());
}
short IsTPreA06( void )
{
	return (short)(!DIS_A06);
}
short IsPot1( void )
{
	return (short)(MAX_POWER >= POT1LEV);
}
short IsPot2( void )
{
	return (short)(MAX_POWER >= POT2LEV);
}
short IsPot3( void )
{
	return (short)(MAX_POWER >= POT3LEV);
}
short IsPot4( void )
{
	return (short)(MAX_POWER >= POT4LEV);
}
short IsPot5( void )
{
	return (short)(MAX_POWER >= POT5LEV);
}
short IsPot6( void )
{
	return (short)(MAX_POWER >= POT6LEV);
}
short IsConsumo( void )
{
	return (short)(PesataPellet > 0);
}
short IsFotoresistenza( void )
{
	return (short)(F_ACC_FOTORES);
}
short IsCollaudoMan( void )
{
	return (short)(StatoLogico != STAT_TESTER_AUTO);
}
short IsCollaudoAuto( void )
{
	return (short)(StatoLogico != STAT_TESTER_MANU);
}
short IsFan1Test( void )
{
	return (short)(!EN_PULIT_COCLEA2);
}
short IsFan2Test( void )
{
	return (short)(!EN_ESP2);
}
short IsScuotitore( void )
{
	return (short)(F_EN_SCUOTITORE);
}
short IsCocleaItem( void )
{
	if( LivAccesso == LOGIN_MANUFACTURER )
		return 1;
	else	// LOGIN_SERVICE
		return (short)(IsTempoCoclea() || IsPeriodoCoclea());
}
short IsPreAccHotItem( void )
{
	if( LivAccesso == LOGIN_MANUFACTURER )
		return 1;
	else	// LOGIN_SERVICE
		return (short)(userIsIdro());
}
short IsAccBItem( void )
{
	if( LivAccesso == LOGIN_MANUFACTURER )
		return 1;
	else	// LOGIN_SERVICE
		return (short)(userIsIdro());
}
short IsWiFiIPvalid( void )
{
#ifdef WIFI_IP_VALID
	return 1;
#else
	short RetVal = 0;
	short i;
	
	// check for IP not null
	for( i=0; i<IP_SIZE; i++ )
	{
		if( Ip[i] )
		{
			RetVal = 1;
			break;
		}
	}
	
	return RetVal;
#endif
}


#ifdef SCREENSHOT

#define REG_BIST_EN				1058132UL	// REG_SCREENSHOT_READ
#define REG_BUSYBITS			1058008UL	// REG_SCREENSHOT_BUSY
#define RAM_SCREENSHOT			0x1C2000
#define RAM_SCREENSHOT_SIZE	2048	// 2KB
/*!
** \fn short Screen_Snapshot( void )
** \brief Screen capture
** \return 1: OK, 0: FAILED
**/
static short Screen_Snapshot( void )
{
	short RetVal = 0;
	int i;
	ft_uint8_t *linearray;
	ft_uint32_t busyflag;
	
	if( (linearray = (ft_uint8_t *)malloc( RAM_SCREENSHOT_SIZE )) != NULL )
	{
		//osThreadSuspendAll();	//!< Suspends the scheduler
		/*
		 * WARNING
		 * The suspended kernel is still runnning, but its timing is not working any more.
		 * Every osDelay() or osMutexWait() (like in LOCKprintf()) shall not be used until the kernel is resumed.
		 */

		// delete all the mutex
		//DeleteMutex();
			
		/*!
			Fermare il programma qui con un breakpoint e predisporre il
			terminale seriale per il log su file binario della ricezione.
		*/
		
		/* Take a snapshot of the DL */
		Ft_Gpu_Hal_Wr32(pHost,REG_RENDERMODE,1);	// REG_SCREENSHOT_EN = 1

		for( i=0; i<FT_DispHeight; i++ )
		{
			Ft_Gpu_Hal_Wr32(pHost,REG_SNAPY,i);	// set REG_SCREENSHOT_Y
			Ft_Gpu_Hal_Wr32(pHost,REG_SNAPSHOT,1);	// REG_SCREENSHOT_START = 1

			/* check for the completion */
			Wdog();
			do 
			{
				busyflag = Ft_Gpu_Hal_Rd32(pHost,REG_BUSYBITS) | Ft_Gpu_Hal_Rd32(pHost,REG_BUSYBITS + sizeof(long));
			} while( busyflag );
			Wdog();

			// Read 32 bit pixel values of the line from RAM_SCREENSHOT.
			// The pixel format is BGRA: Blue is in lowest address and Alpha
			// is in highest address.
			Ft_Gpu_Hal_Wr32(pHost,REG_BIST_EN,1);
			Ft_Gpu_Hal_RdMem(pHost,RAM_SCREENSHOT,linearray,RAM_SCREENSHOT_SIZE);
			Ft_Gpu_Hal_Wr32(pHost,REG_BIST_EN,0);
			Ft_Gpu_Hal_Wr32(pHost,REG_SNAPSHOT,0);	// REG_SCREENSHOT_START = 0

			Wdog();
			// send the read line 
			HAL_UART_Transmit( &huart6, (uint8_t *)linearray, FT_DispWidth*sizeof(long), HAL_MAX_DELAY );
			Wdog();
		}
		
		Ft_Gpu_Hal_Wr32(pHost,REG_RENDERMODE,0);	// REG_SCREENSHOT_EN = 0
		/* Make the bist mode to 1 */
		
		if( i >= FT_DispHeight )
		{
			// successfull
			RetVal = 1;
		}
			
		/*!
			Fermare il programma qui con un breakpoint e salvare il file binario
			ricevuto sul terminale seriale.
		*/
		
		// create all the mutex
		//CreateMutex();

		//osThreadResumeAll();	//!< Resume the scheduler

		free( linearray );
	}
	else
	{
	#ifdef LOGGING
		//myprintf( "Not enough memory\r\n" );
		HAL_UART_Transmit(&huart6, (uint8_t *)"Not enough memory\r\n", 19, HAL_MAX_DELAY);
	#endif
	}
	
	return RetVal;
}

static unsigned char bmapSShot[NUM_SSHOT/8+1];

/*!
** \fn void DoScreenshot( unsigned short iSShot )
** \brief Execute a screenshot
** \param iSShot Screenshot index
** \return None
**/
void DoScreenshot( unsigned short iSShot )
{
	uint8_t c = 0;
	
	if( iSShot < NUM_SSHOT )
	{
		// make the screenshot, if not already done before
		if( !GetBitBMap( bmapSShot, iSShot ) )
		{
		#ifdef LOGGING
			osThreadSuspendAll();	//!< Suspends the scheduler
			/*
			 * WARNING
			 * The suspended kernel is still runnning, but its timing is not working any more.
			 * Every osDelay() or osMutexWait() (like in LOCKprintf()) shall not be used until the kernel is resumed.
			 */

			// delete all the mutex
			DeleteMutex();

			/*!
				Predisporre il terminale seriale per il log su file binario della ricezione.
			*/

			// flush RX
			HAL_UART_Receive(&huart6, &c, 1, 10);
			//myprintf( "\r\n\nConfirm Screenshot %u [y/n]\r\n", iSShot );
			mysprintf( str, "\r\n\nConfirm Screenshot %u [y/n]\r\n", iSShot );
			HAL_UART_Transmit(&huart6, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
			while( 1 )
			{
				if( __HAL_UART_GET_FLAG(&huart6, UART_FLAG_RXNE) )
				{
					if( HAL_UART_Receive(&huart6, &c, 1, 10) == HAL_OK )
					{
						break;
					}
				}
				//Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
				Wdog();
			}
			if( c == 'y' )
		#endif
			{
				// make and send the screenshot BGRA file
				if( Screen_Snapshot() )	// may suspends the scheduler
				{
					// OK
					SetBitBMap( bmapSShot, iSShot );
				#ifdef LOGGING
					//myprintf( "\r\n\nSUCCESS: Screenshot sent\r\n\n" );
					//HAL_UART_Transmit(&huart6, (uint8_t *)"\r\n\nSUCCESS: Screenshot sent\r\n\n", 30, HAL_MAX_DELAY);
				#endif
				}
				else
				{
					// FAILED
				#ifdef LOGGING
					//myprintf( "\r\n\nERROR: Screenshot not sent\r\n\n" );
					HAL_UART_Transmit(&huart6, (uint8_t *)"\r\n\nERROR: Screenshot not sent\r\n\n", 32, HAL_MAX_DELAY);
				#endif
				}
			}
		#ifdef LOGGING
			else if( c == 'n' )
			{
				SetBitBMap( bmapSShot, iSShot );	// do not make this screenshot
			}
		#endif

			// flush the touch queue
			FlushTouchQueue();

		#ifdef LOGGING
			if( c == 'y' )
			{
				/*!
					Salvare il file binario ricevuto sul terminale seriale.
				*/

				// flush RX
				HAL_UART_Receive(&huart6, &c, 1, 10);
				while( 1 )
				{
					if( __HAL_UART_GET_FLAG(&huart6, UART_FLAG_RXNE) )
					{
						if( HAL_UART_Receive(&huart6, &c, 1, 10) == HAL_OK )
						{
							break;
						}
					}
					//Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
					Wdog();
				}
			}

			// create all the mutex
			CreateMutex();

			osThreadResumeAll();	//!< Resume the scheduler
		#endif
		}
	}
}

/*!
** \fn short IsGenScreeshot( void )
** \brief Check the general condition for a screenshot
** \return TRUE or FALSE
**/
short IsGenScreeshot( void )
{
	return (short)(
					(huart6.Instance == USART6)	// UART output channel is available
					&& !strncmp( "ENG", (char *)vector_get( &SigleStati, iLanguage ), 3 )	// only for one language
					);
}
#endif	// SCREENSHOT

