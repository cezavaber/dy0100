/*!
	\file OT_Plus_data_layer.c
	\brief Modulo OT_Plus_data_layer.c

	Gestione del livello dati di comunicazione MASTER su canale OT/+ .

*/

#include "main.h"
#include "timebase.h"
#include "mbcrc.h"
#include "OT_Plus_data_layer.h"


/*!
	\struct OneShotPkt_queue_struct
	\brief Struttura della coda dei pacchetti con unico invio
*/
struct OneShotPkt_queue_struct
{
	unsigned char pktNum;		//!< queue size
	unsigned char pktIns;		//!< write index
	unsigned char pktEst;		//!< read index
	PKT_STRUCT sPkt[MAX_NUM_ONE_SHOT_PKT];		//!< queue
};
/*!
	\typedef ONE_SHOT_PKT_QUEUE
	\brief Typedef definition for OneShotPkt_queue_struct
*/
typedef struct OneShotPkt_queue_struct ONE_SHOT_PKT_QUEUE;
/*!
	\var OneShotPkt_queue
	\brief Coda dei pacchetti con unico invio
*/
static ONE_SHOT_PKT_QUEUE OneShotPkt_queue;

/*!
	\struct OneShotExtPkt_queue_struct
	\brief Struttura della coda dei pacchetti con unico invio da client esterno
*/
struct OneShotExtPkt_queue_struct
{
	unsigned char pktNum;		//!< queue size
	unsigned char pktIns;		//!< write index
	unsigned char pktEst;		//!< read index
	PKT_STRUCT sPkt[MAX_NUM_ONE_SHOT_EXTERNAL_PKT];		//!< queue
};
/*!
	\typedef ONE_SHOT_EXTERNAL_PKT_QUEUE
	\brief Typedef definition for OneShotExtPkt_queue_struct
*/
typedef struct OneShotExtPkt_queue_struct ONE_SHOT_EXTERNAL_PKT_QUEUE;
/*!
	\var OneShotExtPkt_queue
	\brief Coda dei pacchetti con unico invio da client esterno
*/
static ONE_SHOT_EXTERNAL_PKT_QUEUE OneShotExtPkt_queue;

/*!
	\struct CyclePkt_queue_struct
	\brief Struttura della coda dei pacchetti con invio ciclico
*/
struct CyclePkt_queue_struct
{
	unsigned char pktNum;		//!< queue size
	unsigned char pktEst;		//!< read index
	PKT_STRUCT *sPkt[MAX_NUM_CYCLE_PKT];		//!< queue
};
/*!
	\typedef CYCLE_PKT_QUEUE
	\brief Typedef definition for CyclePkt_queue_struct
*/
typedef struct CyclePkt_queue_struct CYCLE_PKT_QUEUE;
/*!
	\var CyclePkt_queue
	\brief Coda dei pacchetti con invio ciclico
*/
static CYCLE_PKT_QUEUE CyclePkt_queue;

/*!
	\var OT_DL_Stat
	\brief Stato del gestore livello dati MASTER
*/
unsigned char OT_DL_Stat;


static OT_DL_RxPkt OTRx;		//!< rx call-back function pointer
static OT_DL_RxPkt OTExtRx;	//!< external rx call-back function pointer
static unsigned char *RxPkt;	//!< rx packet pointer
static unsigned short RxPktSize;	//!< rx packet size
unsigned char cntRetry;		//!< sending retry counter
static OT_DL_LimitReq OTLimitReq;	//!< Limiti/Step request call-back function pointer
/*!
	\def MAX_NUM_SEND_RETRY
	\brief Massimo numero di tentativi di trasmissione.
*/
#define MAX_NUM_SEND_RETRY		10	//3


#define OT_DL_RX_TIMEOUT	(T1SEC)		// 1s timeout ricezione
													/* deve comprendere sia il tempo di
														trasmissione che quello di ricezione */

/*!
	\var OT_DL_Pausa
	\brief Pausa (in ms) tra cicli di richieste cicliche
*/
unsigned short OT_DL_Pausa;														
unsigned char cntPausa;	//!< contatore di pausa

#ifdef TXRX_ONLY
unsigned long cntTxRx, cntTxRxpre;	// contatore transazioni tx-rx effettuate
unsigned long cntTxRxGood;	// contatore transazioni tx-rx effettuate con successo
#endif

/*!
	\var OT_DL_RFFilter
	\brief Abilitazione ricezione con filtro codice RF
*/
unsigned char OT_DL_RFFilter;														
														
														
														


static void OT_PH_RxPkt( unsigned char *pkt, unsigned short pktSize );
static void NextPkt( unsigned char mode );



/*!
	\fn void OT_PH_RxPkt( unsigned char *pkt, unsigned short pktSize );
	\brief Funzione call-back per ricezione pacchetto.
	\param pkt pacchetto ricevuto
	\param pktSize dimensione pacchetto
*/
static void OT_PH_RxPkt( unsigned char *pkt, unsigned short pktSize )
{
	RxPkt = pkt;		// segnala avvenuta ricezione se != NULL
	RxPktSize = pktSize;		// segnala avvenuta ricezione se > 0
}

/*!
	\fn PKT_STRUCT *PeekPkt( unsigned char mode )
   \brief Rileva un pacchetto dalla coda indicata, senza eliminarlo dalla coda.
	\param mode modo di invio (SEND_MODE_ONE_SHOT_xxx o SEND_MODE_CYCLE)
   \return Il puntatore al pacchetto, NULL se coda vuota.
*/
PKT_STRUCT *PeekPkt( unsigned char mode )
{
	PKT_STRUCT *pPkt = NULL;

	switch( mode )
	{
		case SEND_MODE_CYCLE:
			if( CyclePkt_queue.pktNum )
			{
				pPkt = CyclePkt_queue.sPkt[CyclePkt_queue.pktEst];
			}
		break;
		
		case SEND_MODE_ONE_SHOT:
			if( OneShotPkt_queue.pktNum )
			{
				pPkt = (PKT_STRUCT *)&OneShotPkt_queue.sPkt[OneShotPkt_queue.pktEst];
			}
		break;
		
		case SEND_MODE_ONE_SHOT_EXTERNAL:
			if( OneShotExtPkt_queue.pktNum )
			{
				pPkt = (PKT_STRUCT *)&OneShotExtPkt_queue.sPkt[OneShotExtPkt_queue.pktEst];
			}
		break;
	}

	return pPkt;
}

/*!
	\fn void NextPkt( unsigned char mode )
   \brief Passa al successivo pacchetto nella coda indicata
	\param mode modo di invio (SEND_MODE_ONE_SHOT o SEND_MODE_CYCLE)
*/
static void NextPkt( unsigned char mode )
{
	switch( mode )
	{
		case SEND_MODE_CYCLE:
			if( ++CyclePkt_queue.pktEst >= CyclePkt_queue.pktNum )
				CyclePkt_queue.pktEst = 0;
		break;
		
		case SEND_MODE_ONE_SHOT:
			if( ++OneShotPkt_queue.pktEst >= MAX_NUM_ONE_SHOT_PKT )
				OneShotPkt_queue.pktEst = 0;
			OneShotPkt_queue.pktNum--;
		break;
		
		case SEND_MODE_ONE_SHOT_EXTERNAL:
			if( ++OneShotExtPkt_queue.pktEst >= MAX_NUM_ONE_SHOT_EXTERNAL_PKT )
				OneShotExtPkt_queue.pktEst = 0;
			OneShotExtPkt_queue.pktNum--;
		break;
	}
}



/*!
	\fn short OT_DL_Init( OT_DL_RxPkt Rx_CallBack )
   \brief Inizializzazione data layer OT/+
	\param Rx_CallBack funzione call-back di ricezione
	\return 1: OK, 0: FAILED
*/
short OT_DL_Init( OT_DL_RxPkt Rx_CallBack )
{
	memset( &OneShotPkt_queue, 0, sizeof(OneShotPkt_queue) );
	memset( &OneShotExtPkt_queue, 0, sizeof(OneShotExtPkt_queue) );
	memset( &CyclePkt_queue, 0, sizeof(CyclePkt_queue) );
	OT_DL_Stat = OT_DL_STAT_IDLE;
	OTRx = Rx_CallBack;
	OTExtRx = NULL;
	OT_LINK = 0;
	RxPkt = NULL;
	RxPktSize = 0;
	starttimer( T_OT_DL, OT_DL_PAUSE );
	OTLimitReq = NULL;
	OT_DL_Pausa = OT_DL_PAUSE;
	cntPausa = 0;

#ifdef TXRX_ONLY
	cntTxRx = 0;
	cntTxRxpre = 0;
	cntTxRxGood = 0;
#endif
	
	return OT_PH_Init( OT_PH_RxPkt );
}

/*!
	\fn void OT_DL_SetLimitReqFunc( OT_DL_LimitReq LimReq_CallBack )
   \brief Imposta la funzione call-back per invio richiesta Limiti/Step
	\param LimReq_CallBack funzione call-back per invio richiesta Limiti/Step
*/
void OT_DL_SetLimitReqFunc( OT_DL_LimitReq LimReq_CallBack )
{
	OTLimitReq = LimReq_CallBack;
}

/*!
	\fn short OT_DL_Send( PKT_STRUCT *sPkt, unsigned char mode )
   \brief Invio di un pacchetto
	\param pkt struttura pacchetto da inviare
	\param mode modo di invio (SEND_MODE_ONE_SHOT_xxx o SEND_MODE_CYCLE)
	\return 1: OK, 0: FAILED
	\note I pacchetti con invio unico sono prioritari rispetto a quelli
			con invio ciclico.
			La struttura del pacchetto con invio unico viene ricopiata dalla
			funzione nella propria coda e puo` quindi essere allocata in modo
			dinamico.
			La struttura del pacchetto con invio ciclico viene registrata dalla
			funzione come puntatore nella propria coda e deve quindi essere
			allocata in modo statico.
*/
short OT_DL_Send( PKT_STRUCT *sPkt, unsigned char mode )
{
	short RetVal = 0;

	if( sPkt->pktLen )
	{
		switch( mode )
		{
			case SEND_MODE_CYCLE:
				// append del pacchetto alla coda con invio ciclico
				if( CyclePkt_queue.pktNum < MAX_NUM_CYCLE_PKT )
				{
					CyclePkt_queue.sPkt[CyclePkt_queue.pktNum] = sPkt;
					CyclePkt_queue.pktNum++;
					RetVal = 1;
				}
			break;
			
			case SEND_MODE_ONE_SHOT:
				// inserisce il pacchetto nella coda con invio unico
				if( OneShotPkt_queue.pktNum < MAX_NUM_ONE_SHOT_PKT )
				{
					/* OneShotPkt_queue.sPkt[OneShotPkt_queue.pktIns] = *sPkt;	ERRORE!!
					 * sPkt->pkt potrebbe avere dimensione inferiore a MAX_PACKET_SIZE
					 * e in quel caso esiste il rischio di accedere a zone proibite
					 * (es.: oltre lo stack) nella copia di tutti i MAX_PACKET_SIZE bytes.
					 */
					OneShotPkt_queue.sPkt[OneShotPkt_queue.pktIns].pktLen = sPkt->pktLen;
					memcpy( OneShotPkt_queue.sPkt[OneShotPkt_queue.pktIns].pkt, sPkt->pkt, sPkt->pktLen );
					if( ++OneShotPkt_queue.pktIns >= MAX_NUM_ONE_SHOT_PKT )
						OneShotPkt_queue.pktIns = 0;
					OneShotPkt_queue.pktNum++;
					RetVal = 1;
				}
			break;
			
			case SEND_MODE_ONE_SHOT_EXTERNAL:
				// inserisce il pacchetto nella coda con invio unico da client esterno
				if( OneShotExtPkt_queue.pktNum < MAX_NUM_ONE_SHOT_EXTERNAL_PKT )
				{
					/* OneShotExtPkt_queue.sPkt[OneShotExtPkt_queue.pktIns] = *sPkt;	ERRORE!!
					 * sPkt->pkt potrebbe avere dimensione inferiore a MAX_PACKET_SIZE
					 * e in quel caso esiste il rischio di accedere a zone proibite
					 * (es.: oltre lo stack) nella copia di tutti i MAX_PACKET_SIZE bytes.
					 */
					OneShotExtPkt_queue.sPkt[OneShotExtPkt_queue.pktIns].pktLen = sPkt->pktLen;
					memcpy( OneShotExtPkt_queue.sPkt[OneShotExtPkt_queue.pktIns].pkt, sPkt->pkt, sPkt->pktLen );
					if( ++OneShotExtPkt_queue.pktIns >= MAX_NUM_ONE_SHOT_EXTERNAL_PKT )
						OneShotExtPkt_queue.pktIns = 0;
					OneShotExtPkt_queue.pktNum++;
					RetVal = 1;
				}
			break;
		}
	}

	return RetVal;
}

/*!
	\fn void OT_DL_FlushOneShotQueue( void )
	\brief Svuota la coda dei pacchetti con invio unico
*/
void OT_DL_FlushOneShotQueue( void )
{
	OneShotPkt_queue.pktNum = 0;
	OneShotPkt_queue.pktIns = 0;
	OneShotPkt_queue.pktEst = 0;
}

/*!
	\fn void OT_DL_FlushOneShotExtQueue( void )
	\brief Svuota la coda dei pacchetti con invio unico da client esterno
*/
void OT_DL_FlushOneShotExtQueue( void )
{
	OneShotExtPkt_queue.pktNum = 0;
	OneShotExtPkt_queue.pktIns = 0;
	OneShotExtPkt_queue.pktEst = 0;
}

/*!
	\fn void OT_DL_FlushCycleQueue( void )
	\brief Svuota la coda dei pacchetti con invio ciclico
*/
void OT_DL_FlushCycleQueue( void )
{
	CyclePkt_queue.pktNum = 0;
	CyclePkt_queue.pktEst = 0;
}

/*!
	\fn void OT_DL_Restart( void )
	\brief Riavvia il gestore del livello dati MASTER della comunicazione OT/+
*/
void OT_DL_Restart( void )
{
	OT_DL_Stat = OT_DL_STAT_IDLE;
	starttimer( T_OT_DL, OT_DL_PAUSE );
	// reset TX e RX
	OT_ResetRXTX();
}

/*!
	\fn void OT_DL_Handler( void )
	\brief Gestore del livello dati MASTER della comunicazione OT/+
*/
void OT_DL_Handler( void )
{
	PKT_STRUCT *sPkt;
	unsigned short w, lwCheckCalc;
	unsigned long timer;

	switch( OT_DL_Stat )
	{
		case OT_DL_STAT_IDLE:
#ifdef RX_ONLY
			// attesa pacchetto
			if( RxPktSize >= 5 )	// come minimo: rfid|addr|cmd|crcLo|crcHi
			{
				// ricevuto pacchetto -> verifico CRC16
				w = MAKEWORD( RxPkt[RxPktSize-2], RxPkt[RxPktSize-1] );
				RxPktSize -= 2;
				lwCheckCalc = CalcCrcBuff( &RxPkt[1], RxPktSize-1 );
				if( lwCheckCalc == w )
				{
					// pacchetto OK
					if( OTRx != NULL )
					{
						// passo il pacchetto al livello applicativo
						OTRx( RxPkt, RxPktSize, RX_ERR_NONE, NULL );
					}
				}
			}
#else
			// pausa di invio
			if( OT_DL_Pausa != OT_DL_LONG_PAUSE )
			{
				// tempo di pausa breve deciso dal livello applicativo
				cntPausa = 0;
				timer = readtimer( T_OT_DL );
				if( timer != NOTIMER )
				{
					// siamo in pausa lunga?
					if( timer > OT_DL_PAUSE )
					{
						// forzo fine pausa lunga
						starttimer( T_OT_DL, 1 );
					}
				}
			}
			
			if( checktimer( T_OT_DL ) != INCORSO )
			{
				if( cntPausa )
					cntPausa--;
				if( cntPausa == 0 )
				{
					OT_PH_SetFullPower();	// radio in PWRUP
					RxPkt = NULL;
					RxPktSize = 0;
					cntRetry = MAX_NUM_SEND_RETRY + 1;
					if( (sPkt = PeekPkt( SEND_MODE_ONE_SHOT )) != NULL )
					{
						OT_DL_Stat = OT_DL_STAT_ONE_SHOT_BUSY;
					}
					else if( (sPkt = PeekPkt( SEND_MODE_ONE_SHOT_EXTERNAL )) != NULL )
					{
						cntRetry = 1 + 1;	// unico tentativo
						OT_DL_Stat = OT_DL_STAT_ONE_SHOT_EXTERNAL_BUSY;
					}
					else if( (sPkt = PeekPkt( SEND_MODE_CYCLE )) != NULL )
					{
						OT_DL_Stat = OT_DL_STAT_CYCLE_BUSY;
					}
					else if( OTLimitReq != NULL )
					{
						OTLimitReq();
						// invio subito l'eventuale richiesta Limiti/Step
						if( OneShotPkt_queue.pktNum )
						{
							OT_DL_Stat = OT_DL_STAT_ONE_SHOT_BUSY;
						}
						else
						{
							// nothing to do
							starttimer( T_OT_DL, OT_DL_Pausa );
							if( OT_DL_Pausa == OT_DL_LONG_PAUSE )
							{
							#ifdef OT_LOGGING
								myprintf( "OT_DL_LONG_PAUSE\r\n" );
							#endif
								OT_PH_SetLowPower();	// mi assicuro che la radio sia in PWRDN
								cntPausa = OT_DL_LONG_PAUSE_FACT;
							}
						}
					}
					else
					{
						// nothing to do
						starttimer( T_OT_DL, OT_DL_Pausa );
						if( OT_DL_Pausa == OT_DL_LONG_PAUSE )
						{
						#ifdef OT_LOGGING
							myprintf( "OT_DL_LONG_PAUSE\r\n" );
						#endif
							OT_PH_SetLowPower();	// mi assicuro che la radio sia in PWRDN
							cntPausa = OT_DL_LONG_PAUSE_FACT;
						}
					}
				}
				else
				{
					// continua la pausa
					starttimer( T_OT_DL, OT_DL_Pausa );
				}
			}
#endif	// RX_ONLY
		break;
		
		case OT_DL_STAT_ONE_SHOT_BUSY:
			// attesa risposta a pacchetto con invio unico
			if( RxPktSize >= 5 )	// come minimo: rfid|addr|cmd|crcLo|crcHi
			{
			#ifdef OT_LOGGING
				myprintf( "%u OT_RX: %s\r\n", TimeSec, VisHexMsg( RxPkt, RxPktSize ) );
			#endif
				// ricevuto risposta -> verifico CRC16 e RFID
				w = MAKEWORD( RxPkt[RxPktSize-2], RxPkt[RxPktSize-1] );
				RxPktSize -= 2;
				lwCheckCalc = CalcCrcBuff( &RxPkt[1], RxPktSize-1 );
				if( lwCheckCalc == w )
				{
					// pacchetto OK
					if( OT_DL_RFFilter && (RxPkt[0] != rfID) )
					{
						// scarta pacchetto ricevuto
						RxPkt = NULL;
						RxPktSize = 0;
					}
					else
					{
					#ifdef TXRX_ONLY
						cntTxRx++;
						cntTxRxGood++;
					#endif
					#ifdef OT_LOGGING
						if( !OT_LINK )
						{
							myprintf( "\tSLAVE OT/+ on-line\r\n" );
						}
						else
						{
							myprintf( "\tRX OK\r\n" );
						}
					#endif
						OT_LINK = 1;	// slave on-line
						OT_DL_Stat = OT_DL_STAT_IDLE;
						if( OTRx != NULL )
						{
							// passo il pacchetto al livello applicativo
							sPkt = PeekPkt( SEND_MODE_ONE_SHOT );
							OTRx( RxPkt, RxPktSize, RX_ERR_NONE, sPkt );
						}
						NextPkt( SEND_MODE_ONE_SHOT );
						starttimer( T_OT_DL, OT_DL_PAUSE );
					}
				}
				else
				{
					// scarta pacchetto ricevuto
					RxPkt = NULL;
					RxPktSize = 0;
				}
			}
			else if( checktimer( T_OT_DL ) != INCORSO )
			{
				if( --cntRetry == 0 )
				{
					// tentativi esauriti
				#ifdef TXRX_ONLY
					cntTxRx++;
				#endif
				#ifdef OT_LOGGING
					if( OT_LINK )
					{
						myprintf( "\tSLAVE OT/+ off-line\r\n" );
					}
				#endif
					OT_LINK = 0;	// slave off-line
					//PwrMode = PWR_MODE_LOW;		// start low-power mode
					OT_ResetRXTX();
					//starttimer( T_OT_DL, T1SEC );
					starttimer( T_OT_DL, OT_DL_PAUSE );
					OT_DL_Stat = OT_DL_STAT_IDLE;
					if( OTRx != NULL )
					{
						// segnala errore rx al livello applicativo
						sPkt = PeekPkt( SEND_MODE_ONE_SHOT );
						OTRx( NULL, 0, RX_ERR_ONE_SHOT, sPkt );
					}
					NextPkt( SEND_MODE_ONE_SHOT );
				}
				else if( (sPkt = PeekPkt( SEND_MODE_ONE_SHOT )) != NULL )
				{
					// nuovo tentativo
					RxPkt = NULL;
					RxPktSize = 0;
					OT_PH_Send( sPkt->pkt, sPkt->pktLen, 1 );
					starttimer( T_OT_DL, OT_DL_RX_TIMEOUT );
				#ifdef OT_LOGGING
					if( cntRetry < MAX_NUM_SEND_RETRY )
					{
						myprintf( "\tRetry %u\r\n", cntRetry );
					}
				#endif
				}
				else
				{
					// per sicurezza ...
					starttimer( T_OT_DL, OT_DL_PAUSE );
					OT_DL_Stat = OT_DL_STAT_IDLE;
				}
			}
		break;
		
		case OT_DL_STAT_ONE_SHOT_EXTERNAL_BUSY:
			// attesa risposta a pacchetto con invio unico da client esterno
			if( RxPktSize >= 5 )	// come minimo: rfid|addr|cmd|crcLo|crcHi
			{
			#ifdef OT_LOGGING
				myprintf( "%u OT_RX: %s\r\n", TimeSec, VisHexMsg( RxPkt, RxPktSize ) );
			#endif
				// ricevuto risposta -> verifico CRC16 e RFID
				w = MAKEWORD( RxPkt[RxPktSize-2], RxPkt[RxPktSize-1] );
				RxPktSize -= 2;
				lwCheckCalc = CalcCrcBuff( &RxPkt[1], RxPktSize-1 );
				if( lwCheckCalc == w )
				{
					// pacchetto OK
					if( OT_DL_RFFilter && (RxPkt[0] != rfID) )
					{
						// scarta pacchetto ricevuto
						RxPkt = NULL;
						RxPktSize = 0;
					}
					else
					{
					#ifdef TXRX_ONLY
						cntTxRx++;
						cntTxRxGood++;
					#endif
					#ifdef OT_LOGGING
						if( !OT_LINK )
						{
							myprintf( "\tSLAVE OT/+ on-line\r\n" );
						}
						else
						{
							myprintf( "\tRX OK\r\n" );
						}
					#endif
						OT_LINK = 1;	// slave on-line
						OT_DL_Stat = OT_DL_STAT_IDLE;
						if( OTExtRx != NULL )
						{
							// passo il pacchetto al livello applicativo
							sPkt = PeekPkt( SEND_MODE_ONE_SHOT_EXTERNAL );
							OTExtRx( RxPkt, RxPktSize+2, RX_ERR_NONE, sPkt );	// mantengo CRC nel pacchetto
						}
						NextPkt( SEND_MODE_ONE_SHOT_EXTERNAL );
						starttimer( T_OT_DL, OT_DL_PAUSE );
					}
				}
				else
				{
					// scarta pacchetto ricevuto
					RxPkt = NULL;
					RxPktSize = 0;
				}
			}
			else if( checktimer( T_OT_DL ) != INCORSO )
			{
				if( --cntRetry == 0 )
				{
					// tentativi esauriti
				#ifdef TXRX_ONLY
					cntTxRx++;
				#endif
					OT_ResetRXTX();
					//starttimer( T_OT_DL, T1SEC );
					starttimer( T_OT_DL, OT_DL_PAUSE );
					OT_DL_Stat = OT_DL_STAT_IDLE;
					if( OTExtRx != NULL )
					{
						// segnala errore rx al livello applicativo
						sPkt = PeekPkt( SEND_MODE_ONE_SHOT_EXTERNAL );
						OTExtRx( NULL, 0, RX_ERR_ONE_SHOT_EXTERNAL, sPkt );
					}
					NextPkt( SEND_MODE_ONE_SHOT_EXTERNAL );
				}
				else if( (sPkt = PeekPkt( SEND_MODE_ONE_SHOT_EXTERNAL )) != NULL )
				{
					// nuovo tentativo
					RxPkt = NULL;
					RxPktSize = 0;
					OT_PH_Send( sPkt->pkt, sPkt->pktLen, 0 );	// suppongo CRC gia' presente nel pacchetto
					starttimer( T_OT_DL, OT_DL_RX_TIMEOUT );
				#ifdef OT_LOGGING
					if( cntRetry < MAX_NUM_SEND_RETRY )
					{
						myprintf( "\tRetry %u\r\n", cntRetry );
					}
				#endif
				}
				else
				{
					// per sicurezza ...
					starttimer( T_OT_DL, OT_DL_PAUSE );
					OT_DL_Stat = OT_DL_STAT_IDLE;
				}
			}
		break;
		
		case OT_DL_STAT_CYCLE_BUSY:
			// attesa risposta a pacchetto con invio ciclico
			if( RxPktSize >= 5 )	// come minimo: rfid|addr|cmd|crcLo|crcHi
			{
			#ifdef OT_LOGGING
				myprintf( "%u OT_RX: %s\r\n", TimeSec, VisHexMsg( RxPkt, RxPktSize ) );
			#endif
				// ricevuto risposta -> verifico CRC16 e RFID
				w = MAKEWORD( RxPkt[RxPktSize-2], RxPkt[RxPktSize-1] );
				RxPktSize -= 2;
				lwCheckCalc = CalcCrcBuff( &RxPkt[1], RxPktSize-1 );
				if( lwCheckCalc == w )
				{
					// pacchetto OK
					if( OT_DL_RFFilter && (RxPkt[0] != rfID) )
					{
						// scarta pacchetto ricevuto
						RxPkt = NULL;
						RxPktSize = 0;
					}
					else
					{
					#ifdef TXRX_ONLY
						cntTxRx++;
						cntTxRxGood++;
					#endif
					#ifdef OT_LOGGING
						if( !OT_LINK )
						{
							myprintf( "\tSLAVE OT/+ on-line\r\n" );
						}
						else
						{
							myprintf( "\tRX OK\r\n" );
						}
					#endif
						OT_LINK = 1;	// slave on-line
						OT_DL_Stat = OT_DL_STAT_IDLE;
						if( OTRx != NULL )
						{
							// passo il pacchetto al livello applicativo
							sPkt = PeekPkt( SEND_MODE_CYCLE );
							OTRx( RxPkt, RxPktSize, RX_ERR_NONE, sPkt );
						}
						NextPkt( SEND_MODE_CYCLE );
						// eventuali azioni a fine ciclo
						if((OneShotPkt_queue.pktNum == 0) &&	// la coda ONE_SHOT e` vuota
							(
								(CyclePkt_queue.pktNum == 0) ||	// la coda CYCLE e` vuota
								(CyclePkt_queue.pktNum && (CyclePkt_queue.pktEst == 0))	// la coda CYCLE ha ricircolato
							)
							)
						{
							// eventuale richiesta Limiti/Step
							if( OTLimitReq != NULL )
							{
								// richiamo la call-back per invio richiesta Limiti/Step
								OTLimitReq();
								if( OneShotPkt_queue.pktNum )
								{
									// richiesta Limiti/Step accodata
									starttimer( T_OT_DL, OT_DL_PAUSE );
								}
								else
								{
									// imposto il tempo di pausa deciso dal livello applicativo
									starttimer( T_OT_DL, OT_DL_Pausa );
									if( OT_DL_Pausa == OT_DL_LONG_PAUSE )
									{
									#ifdef OT_LOGGING
										myprintf( "OT_DL_LONG_PAUSE\r\n" );
									#endif
										OT_PH_SetLowPower();	// mi assicuro che la radio sia in PWRDN
										cntPausa = OT_DL_LONG_PAUSE_FACT;
									}
								}
							}
							else
							{
								OT_ResetRXTX();
								// imposto il tempo di pausa deciso dal livello applicativo
								starttimer( T_OT_DL, OT_DL_Pausa );
								if( OT_DL_Pausa == OT_DL_LONG_PAUSE )
								{
								#ifdef OT_LOGGING
									myprintf( "OT_DL_LONG_PAUSE\r\n" );
								#endif
									OT_PH_SetLowPower();	// mi assicuro che la radio sia in PWRDN
									cntPausa = OT_DL_LONG_PAUSE_FACT;
								}
							}
						}
						else
						{
							starttimer( T_OT_DL, OT_DL_PAUSE );
						}
					}
				}
				else
				{
					// scarta pacchetto ricevuto
					RxPkt = NULL;
					RxPktSize = 0;
				}
			}
			else if( checktimer( T_OT_DL ) != INCORSO )
			{
				if( --cntRetry == 0 )
				{
					// tentativi esauriti
				#ifdef TXRX_ONLY
					cntTxRx++;
				#endif
				#ifdef OT_LOGGING
					if( OT_LINK )
					{
						myprintf( "\tSLAVE OT/+ off-line\r\n" );
					}
				#endif
					OT_LINK = 0;	// slave off-line
					OT_ResetRXTX();
					OT_DL_Stat = OT_DL_STAT_IDLE;
					if( OTRx != NULL )
					{
						// segnala errore rx al livello applicativo
						sPkt = PeekPkt( SEND_MODE_CYCLE );
						OTRx( NULL, 0, RX_ERR_CYCLE, sPkt );
					}
					NextPkt( SEND_MODE_CYCLE );
					// eventuali azioni a fine ciclo
					if((OneShotPkt_queue.pktNum == 0) &&	// la coda ONE_SHOT e` vuota
						(
							(CyclePkt_queue.pktNum == 0) ||	// la coda CYCLE e` vuota
							(CyclePkt_queue.pktNum && (CyclePkt_queue.pktEst == 0))	// la coda CYCLE ha ricircolato
						)
						)
					{
						// imposto il tempo di pausa deciso dal livello applicativo
						starttimer( T_OT_DL, OT_DL_Pausa );
						if( OT_DL_Pausa == OT_DL_LONG_PAUSE )
						{
							OT_PH_SetLowPower();	// mi assicuro che la radio sia in PWRDN
							cntPausa = OT_DL_LONG_PAUSE_FACT;
						}
					}
					else
					{
						//starttimer( T_OT_DL, T1SEC );
						starttimer( T_OT_DL, OT_DL_PAUSE );
					}
				}
				else if( (sPkt = PeekPkt( SEND_MODE_CYCLE )) != NULL )
				{
					// nuovo tentativo
					RxPkt = NULL;
					RxPktSize = 0;
					OT_PH_Send( sPkt->pkt, sPkt->pktLen, 2 );	// segnala richiesta ciclica
					starttimer( T_OT_DL, OT_DL_RX_TIMEOUT );
				#ifdef OT_LOGGING
					if( cntRetry < MAX_NUM_SEND_RETRY )
					{
						myprintf( "\tRetry %u\r\n", cntRetry );
					}
				#endif
				}
				else
				{
					// per sicurezza ...
					starttimer( T_OT_DL, OT_DL_PAUSE );
					OT_DL_Stat = OT_DL_STAT_IDLE;
				}
			}
		break;
	}

}

/*!
	\fn void OT_DL_SetExtRxFunc( OT_DL_RxPkt Rx_CallBack )
   \brief Imposta la funzione call-back per ricezione risposte a richieste da
			 client esterno.
	\param Rx_CallBack funzione call-back di ricezione
*/
void OT_DL_SetExtRxFunc( OT_DL_RxPkt Rx_CallBack )
{
	OTExtRx = Rx_CallBack;
}



