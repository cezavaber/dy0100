/*!
** \file TimeBase.c
** \author Alessandro Vagniluca
** \date 16/10/2015
** \brief Base Timer handler
** 
** \version 1.0
**/

#include "main.h"
#include "TimeBase.h"


/* defines */

/* variabili */

/*!
** Timers with 1 tick resolution
**/
static unsigned long lTimer[ NTIMER ];
static unsigned long Tstat;   /* max 32 timer */
static unsigned long lTattesa[ NTIMER ];





/* prototipi */




/*!
** \fn void starttimer( unsigned char quale, unsigned long attesa )
** \brief Start a software timer
** \param quale The timer index
** \param attesa The timer value (ticks)
**/
void starttimer( unsigned char quale, unsigned long attesa )
{
	unsigned long tmp = 1 << quale;

	lTimer[ quale ] = osKernelSysTick();
	Tstat |= tmp;
	lTattesa[ quale ] = attesa;
}



/*!
** \fn unsigned char checktimer( unsigned char quale )
** \brief Check a software timer
** \param quale The timer index
** \return The timer state:
** 			SCADUTO - elapsed
** 			INCORSO - not elapsed yet
** 			NON_ATTIVO - not active
** \note When SCADUTO is returned, the timer is automatically disabled
**/
unsigned char checktimer( unsigned char quale )
{
	unsigned long tmp = 1 << quale;
	unsigned long systimer = osKernelSysTick();

	if( !(Tstat & tmp) )
		return( NON_ATTIVO );

	if( (unsigned long)(systimer - lTimer[quale]) < lTattesa[quale] )
		return( INCORSO );
	else
	{
		Tstat &= ~tmp;
		return( SCADUTO );
	}
}


/*!
** \fn unsigned long readtimer( unsigned char quale )
** \brief Read the current value of a software timer
** \param quale The timer index
** \return The timer value (ticks) or NOTIMER if disabled
**/
unsigned long readtimer( unsigned char quale )
{
 	unsigned long tmp = 1 << quale;
	unsigned long systimer = osKernelSysTick();
	unsigned long time;

	if( !(Tstat & tmp) )
		return( NOTIMER );
	else
	{
		time = (unsigned long)(systimer - lTimer[quale]);
		if( time >= lTattesa[quale] )
		{
			Tstat &= ~tmp;
			return( NOTIMER );
		}
		else
			return( time );
	}
}


