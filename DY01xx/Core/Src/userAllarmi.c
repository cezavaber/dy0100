/*!
** \file userAllarmi.c
** \author Alessandro Vagniluca
** \date 22/07/2019
** \brief User Interface alarm list handler
** 
** \version 1.0
**/

//#define DEBUG_ALARM_REC		// abilita il debug della lista dei record allarmi

#include "user.h"
#include "User_Parametri.h"
#include "userMenu.h"
#include "userAllarmi.h"


/*!
** \enum ScreenRetStatesEnum
** \brief I possibili codici di ritorno delle schermate tipo
**/
enum ScreenRetStatesEnum
{
	SCREEN_ERROR = 0,
	SCREEN_STATUS_CHANGED,
	SCREEN_TIMEOUT,
	SCREEN_OK,
	SCREEN_EVENT, 
	PULSESC_PRESSED,
	PULSOK_PRESSED,
	PULSUP_PRESSED,
	PULSDOWN_PRESSED,
	PULSOPZ_PRESSED,
	LIST_UPDATE_UP,
	LIST_UPDATE_DOWN,

	
	SCREEN_RET_MAX_STATES
};

/*!
** \enum TouchTags
** \brief The used touch tags values
**/
#define TAG_VERT_SCROLL 	0		// riservato a questo TAG !!!!
#define TAG_PULSESC 			TAG_QUIT
#define TAG_PULSOK			TAG_OK



/*!
	\var AlarmRecord
	\brief The Alarm item structure list
*/
ALARM_RECORD AlarmRecord[MAX_ALARM_NUM_LIST];

static unsigned short head;	//!< first item index of the list
static unsigned short curr;	//!< current item index of the list
static unsigned short tail;	//!< last item index of the list
static unsigned short num;	//!< the item number in the list


static unsigned short visFirst;
static unsigned short visLast;




/*!
** \fn unsigned short EVE_WinList( unsigned short *Quale,
**                          char *title, char **msg_tab, unsigned short NumItem, 
**					        unsigned short iCurr,
**							short (*cbScrollUp)( unsigned short iVisFirst ),
**							short (*cbScrollDown)( unsigned short iVisLast ) )
** \brief LIST window handler
** \param Quale - ptr a indice voce in lista scelta (0,..) in caso di 
**                   return value PULSOK_PRESSED
** \param title The window's title string
** \param msg_tab The strings table for the listed items
** \param NumItem The number of items in the list
** \param iCurr The index of the current selected item
** \param cbScrollUp - call-back di scroll-up
** \param cbScrollDown - call-back di scroll-down
** \return Ritorna uno tra i seguenti codici :
**  			SCREEN_ERROR se errore, 
**				SCREEN_STATUS_CHANGED se richiesto cambiamento stato da esterno
**				SCREEN_TIMEOUT se timeout 
**				PULSESC_PRESSED se premuto ESC 
**         		PULSOK_PRESSED se premuto OK
**         		LIST_UPDATE_UP se richiesto aggiornamento lista per scroll up
**         		LIST_UPDATE_DOWN  se richiesto aggiornamento lista per scroll down
**/
unsigned short EVE_WinList( unsigned short *Quale, char *title, const char **msg_tab,
							unsigned short NumItem, unsigned short iCurr,
							short (*cbScrollUp)( unsigned short iVisFirst ),
							short (*cbScrollDown)( unsigned short iVisLast ) )
{
	#define NUM_ITEM_PAGE		(5)
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	unsigned long key_cnt = 0;
	unsigned char loop = 1;
	unsigned short RetVal = SCREEN_ERROR;
	//
	unsigned short FirstLine;
	unsigned char nMenuPage;
	ft_uint16_t But_opt;
	unsigned char scrollON = 0;
	unsigned short preYscroll = 0;
	short Dy = 0;
	//
	short i;

	// calcola il numero di pagine
	nMenuPage = NumItem/NUM_ITEM_PAGE;	// NUM_ITEM_PAGE item/page
	if( NumItem % NUM_ITEM_PAGE )
		nMenuPage++;

	// first item index to be shown
	i = (short)iCurr - (NUM_ITEM_PAGE-1);
	if( i < 0 )
		FirstLine = 0;	
	else
		FirstLine = (unsigned char)i;	
	
	*Quale = iCurr;
	
/********************************************************************************/

	userCounterTime = USER_TIMEOUT_SHOW_60;

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
		}

		if( !((tagval == TAG_VERT_SCROLL) && (scrollON == 1)) )
		{
			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE

			// list item
			for( i=0; (i<NUM_ITEM_PAGE)&&(FirstLine+i<NumItem); i++ )
			{
				if( tagval == TOUCH_TAG_INIT )
				{
					TagRegister( i+1, 0, 31+i*(26+21+1), 469, (26+21+1) );
				}
				if( (tagval == i+1) && (key_cnt >= VALID_TOUCHED_KEY) )
				{
					Ft_App_WrCoCmd_Buffer(pHost, COLOR_RGB(0xFF,0xFF,0xFF) );
					Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (31+i*(26+21+1))*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 469*16, (78+i*(26+21+1))*16 ) );
					// colore stringa evidenziata
					Ft_App_WrCoCmd_Buffer(pHost, Get_UserColorHex(UserColorId) );	// USER COLOR
				}
				else
				{
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				}
				Ft_Gpu_CoCmd_Text( pHost, 0, 31+13+i*(26+21+1), fontDesc, 0, String2Font((char *)msg_tab[FirstLine+i], fontDesc) );
				if( i != (NUM_ITEM_PAGE-1) )
				{
					// separator
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(16,16,16));	// GREY-BLACK
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (78+i*(26+21+1))*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 469*16, (78+i*(26+21+1))*16 ) );
				}
			}
		
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
					
			if( nMenuPage > 1 )
			{
				// vertical scroll bar
				Ft_Gpu_CoCmd_FgColor(pHost, Get_UserColorHex(UserColor_tab[UserColorId].addon));
				Ft_Gpu_CoCmd_BgColor(pHost, 0xFFFFFF);
				if( tagval == TOUCH_TAG_INIT )
				{
					TagRegister( TAG_VSCROLL_BASE, 472, 30, 8, (FT_DispHeight-30) );
				}
				But_opt = ( (tagval == TAG_VSCROLL_BASE) /*&& pt->status*/ ) ? OPT_FLAT : 0;
				Ft_Gpu_CoCmd_Scrollbar( pHost,
												(FT_DispWidth-8), (30+8/2), // consider radius of bar
												8, (FT_DispHeight-(30+8/2)-8/2), // consider radius of bar
												But_opt,
												FirstLine,
												NumItem/nMenuPage,
												NumItem);
					
				if( tagval == TOUCH_TAG_INIT )
				{
					// screen scroll tag
					TagRegister( TAG_VERT_SCROLL, 0, 31, FT_DispWidth-8, FT_DispHeight-30 );
				}
			}
		
			// draw QUIT bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(m_bmhdrQUIT->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(m_bmhdrQUIT->bmhdr.Format, m_bmhdrQUIT->bmhdr.Stride, m_bmhdrQUIT->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, m_bmhdrQUIT->bmhdr.Width, m_bmhdrQUIT->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, 0*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// QUIT bitmap tag
				TagRegister( TAG_QUIT, 0, 0, 30, 30 );
			}

			// Title
			/* Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_XY( 30, 0 ) );
			Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_SIZE( FT_DispWidth-30, 30 ) );
			Ft_Gpu_CoCmd_Gradient( pHost, 30, 0, 0x0000FF, FT_DispWidth-1, 0, 0x000000 ); */
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 30*16, (0)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(title, fontTitle) );
		
			Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
			Ft_Gpu_CoCmd_Swap( pHost );
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
		}
		
		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
				tagval = pt->tag;
			}
		} while( event.status == osOK );
			
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			RetVal = SCREEN_STATUS_CHANGED;
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			scrollON = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = TAG_VERT_SCROLL;
			scrollON = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					RetVal = SCREEN_TIMEOUT;
					loop = 0;	// exit
				}
			}
		}
		else if( (pt->tag == TAG_QUIT) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			RetVal = PULSESC_PRESSED;
			loop = 0;	// exit
		}
		else if( (tagval == TAG_VSCROLL_BASE) && pt->status )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// scroll bar touch
		}
		else if( tagval == TAG_VERT_SCROLL )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// screen vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON )
				{
				default:
				case 0:
					preYscroll = pt->y;
					scrollON = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll;
						if( Dy > 5 )
						{
							scrollON = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll = pt->y;
							if( FirstLine )
							{
								FirstLine--;
							}
							
							if( cbScrollUp != NULL )
							{
								if( cbScrollUp( FirstLine ) )
								{
									// exit with LIST_UPDATE
									//*Quale = iCurr;
									*Quale = FirstLine;
									RetVal = LIST_UPDATE_UP;
									loop = 0;	// exit
								}
							}
						}
						else if( Dy < -5 )
						{
							scrollON = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll = pt->y;
							if( FirstLine < (NumItem-NUM_ITEM_PAGE) )
							{
								FirstLine++;
							}
							
							if( cbScrollDown != NULL )
							{
								i = FirstLine + (NUM_ITEM_PAGE-1);
								if( i >= NumItem )
								{
									i = NumItem - 1;
								}
								if( cbScrollDown( (unsigned short)i ) )
								{
									// exit with LIST_UPDATE
									//*Quale = iCurr;
									*Quale = FirstLine;
									RetVal = LIST_UPDATE_DOWN;
									loop = 0;	// exit
								}
							}
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON = 0;
			}
		}
		else if( scrollON != 2 )	// tagval > 0 without scroll
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// item
			tagval = Read_Keypad( pt );	// read the keys
			if( tagval )
			{
				// touched
				if( Flag.Key_Detect )
				{
					// first touch
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
				else if( ++key_cnt >= VALID_TOUCHED_KEY )
				{
					// pressed item
				}
			}
			else if( pt->tag )
			{
				// untouched
				if( key_cnt >= VALID_TOUCHED_KEY )
				{
					// flush the touch queue
					FlushTouchQueue();

					// process the pressed item on touch release
					switch( pt->tag )
					{
					default:
						// process the pressed item on touch release
						iCurr = FirstLine + (pt->tag - 1);
						*Quale = iCurr;
						RetVal = PULSOK_PRESSED;
						loop = 0;	// exit
						break;
					}
				}
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}
		else
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// tagval > 0 with scroll
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
	}
	
	return RetVal;
}


/*!
** \fn void AlarmInit( void )
** \brief Alarm list init
** \return None
**/
void AlarmInit( void )
{
	memset( AlarmRecord, 0, sizeof(AlarmRecord) );
	head = 0;
	curr = 0;
	tail = 0;
	num = 0;

#ifdef DEBUG_ALARM_REC
	int i;
	ALARM_RECORD AlmRec;

	AlmRec.timestamp = rtcRd( NULL );
	AlmRec.TipoAllarme = NO_ALARM;

	for( i=0; i<MAX_ALARM_NUM_LIST; i++ )
	{
		AlmRec.timestamp += 4000;
		if( ++AlmRec.TipoAllarme >= NALARM )
		{
			AlmRec.TipoAllarme = NO_ALARM + 1;
		}
		AlarmPut( &AlmRec );
	}
#endif
}

/**
 *  \fn short AlarmPut( ALARM_RECORD *pAlmRec )
 *  \brief Add a new Alarm item structure in the list
 *  \param [in] pAlmRec The new Alarm item structure
 *  \return 1: OK, 0: FAILED
 */
short AlarmPut( ALARM_RECORD *pAlmRec )
{
	short RetVal = 0;
	
	// add the new alarm to the list
	AlarmRecord[tail] = *pAlmRec;
	if( ++tail >= MAX_ALARM_NUM_LIST )
	{
		tail = 0;
	}
	// update the oldest alarm in the list
	if( num < MAX_ALARM_NUM_LIST )
	{
		num++;
	}
	else if( ++head >= MAX_ALARM_NUM_LIST )
	{
		head = 0;
	}
	
	return RetVal;
}

/*!
** \fn ALARM_RECORD *AlarmGet( unsigned short index )
** \brief Return the Alarm item structure with the indicated index
** \param index The Alarm item structure index (0: the newest)
** \return The Alarm item structure, NULL if failed.
** \note The found Alarm is set as the current item of the list.
**/
ALARM_RECORD *AlarmGet( unsigned short index )
{
	ALARM_RECORD *p = NULL;
	unsigned short i;
	unsigned short j;
	
	if( index < MAX_ALARM_NUM_LIST )
	{
		// search the item from the newest to the oldest
		for( i=tail,j=0; j<=index; j++ )
		{
			if( i == 0 )
			{
				i = MAX_ALARM_NUM_LIST - 1;
			}
			else
			{
				i--;
			}
		}
		curr = i;
		p = &AlarmRecord[curr];
	}
	
	return p;
}

/*!
** \fn ALARM_RECORD *AlarmGetNext( void )
** \brief Return the next (older) Alarm item structure
** \return The Alarm item structure, NULL if failed.
** \note The current item of the list is updated.
**/
/* ALARM_RECORD *AlarmGetNext( void )
{
	ALARM_RECORD *p = NULL;
	
	// return the item older than the current
	if( curr == 0 )
	{
		curr = MAX_ALARM_NUM_LIST - 1;
	}
	else
	{
		curr--;
	}
	p = &AlarmRecord[curr];
	
	return p;
} */

/*!
** \fn short IsAlmRec( void )
** \brief Check if Alarm list not empty
** \return TRUE or FALSE
**/
short IsAlmRec( void )
{
	return (short)(num > 0);
}

/*!
	\fn unsigned short VisAlmRec( ALARM_RECORD *pAlmRec )
	\brief Visualizza il Record Allarme
	\param [in] pAlmRec Record Allarme da visualizzare
	\return Ritorna uno tra i seguenti codici :
 			SCREEN_ERROR se errore, 
			SCREEN_STATUS_CHANGED se richiesto cambiamento stato da esterno
			SCREEN_TIMEOUT se timeout 
			PULSESC_PRESSED se premuto ESC 
*/
static unsigned short VisAlmRec( ALARM_RECORD *pAlmRec )
{
	unsigned short RetVal = SCREEN_ERROR;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrALARM;
	char s[30];
	TM tm;
	unsigned long key_cnt = 0;
	unsigned char loop = 1;
	short i;
	const char *sAlarm;
	const char *sAlarmInfo;
	
	// push bitmap ALARM into Graphic RAM
	if( res_bm_Load( BITMAP_ALARM, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_ALARM );
	#endif
		return RetVal;
	}
	p_bmhdrALARM = res_bm_GetHeader( BITMAP_ALARM );

	userCounterTime = USER_TIMEOUT_SHOW_20;

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			// full LCD backlight
			SetBacklight( LCD_BACKLIGHT_MAX );
			starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );

			// screen tag
			TagRegister( 0, 0, 31, 407, FT_DispHeight );
		}

		Ft_Gpu_CoCmd_Dlstart(pHost);
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		// draw QUIT bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(m_bmhdrQUIT->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(m_bmhdrQUIT->bmhdr.Format, m_bmhdrQUIT->bmhdr.Stride, m_bmhdrQUIT->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, m_bmhdrQUIT->bmhdr.Width, m_bmhdrQUIT->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, 0*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// QUIT bitmap tag
			TagRegister( TAG_QUIT, 0, 0, 30, 30 );
		}
			
		// draw alarm date-time
		memset( s, cBLANK, 29 );
		s[29] = '\0';
		Sec2DateTime( pAlmRec->timestamp, &tm );
		rtcStrMakeDate( s, &tm );
		strcat( s, "  " );
		rtcStrMakeTime( &s[12], &tm );
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 31+13, fontDesc, OPT_CENTERX, String2Font(s, fontDesc) );
		
		// draw ALARM bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrALARM->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrALARM->bmhdr.Format, p_bmhdrALARM->bmhdr.Stride, p_bmhdrALARM->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrALARM->bmhdr.Width, p_bmhdrALARM->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 200*16, (128-p_bmhdrALARM->bmhdr.Height/2)*16 ) );
			
		// draw alarm code
		s[0] = Hex2Ascii( pAlmRec->TipoAllarme/10 );
		s[1] = Hex2Ascii( pAlmRec->TipoAllarme%10 );
		s[2] = '\0';
		Ft_Gpu_CoCmd_Text( pHost, (200+p_bmhdrALARM->bmhdr.Width+5), 128, fontMax, OPT_CENTERY, String2Font(s, fontMax) );

		// draw alarm description
		sAlarm = GetMsg( 2, NULL, iLanguage, ALARMSOURCE00+pAlmRec->TipoAllarme );
		if( (i = GetFont4StringWidth( (char *)sAlarm, fontDesc, FT_DispWidth )) > 0 )
		{
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 192, i, OPT_CENTERX, String2Font((char *)sAlarm, i) );
		}

		// draw alarm info
		sAlarmInfo = GetMsg( 3, NULL, iLanguage, ALARMRECOVERY00+pAlmRec->TipoAllarme );
		if( (i = GetFont4StringWidth( (char *)sAlarmInfo, fontDesc, FT_DispWidth )) > 0 )
		{
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 220, i, OPT_CENTERX, String2Font((char *)sAlarmInfo, i) );
		}
			
		// title
		GetMsg( 0, s, iLanguage, ALARM );
		/* Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_XY( 30, 0 ) );
		Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_SIZE( FT_DispWidth-30, 30 ) );
		Ft_Gpu_CoCmd_Gradient( pHost, 30, 0, 0x0000FF, FT_DispWidth-1, 0, 0x000000 ); */
		Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
		Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 30*16, (0)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(s, fontTitle) );

		Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
		Ft_Gpu_CoCmd_Swap( pHost );
		Ft_App_Flush_Co_Buffer( pHost );
		Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

		// alarm record window loop
		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
			}
		} while( event.status == osOK );
			
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			RetVal = SCREEN_STATUS_CHANGED;
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = TAG_VERT_SCROLL;
			Flag.Key_Detect = 0;
			key_cnt = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					RetVal = SCREEN_TIMEOUT;
					loop = 0;	// exit
				}
			}
		}
		else if( (pt->tag == TAG_QUIT) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			RetVal = PULSESC_PRESSED;
			loop = 0;	// exit
		}
		else
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// item
			tagval = Read_Keypad( pt );	// read the keys
			if( tagval )
			{
				// touched
				if( Flag.Key_Detect )
				{
					// first touch
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
				else if( ++key_cnt >= VALID_TOUCHED_KEY )
				{
					// pressed item
				}
			}
			else if( pt->tag )
			{
				// untouched
				if( key_cnt >= VALID_TOUCHED_KEY )
				{
					// flush the touch queue
					FlushTouchQueue();

					// process the pressed item on touch release
					switch( pt->tag )
					{
					default:
						// process the pressed item on touch release
						break;
					}
				}
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}
	}
	
	return RetVal;
}

/*!
	\fn unsigned char WinAlmRec( void )
	\brief Visualizza la lista dei Record Allarmi
	\return Il codice dello stato di transizione
*/
unsigned char WinAlmRec( void )
{
	unsigned char RetVal = userCurrentState;
	unsigned long loop = 1;
	unsigned short iItem = 0;
	unsigned short nItem = 0;
	unsigned short SelItem = 0;
	char *sItem[MAX_ALARM_NUM_LIST];
	unsigned short index = 0;
	ALARM_RECORD *ps;
	char s[30];
	TM tm;
	
	memset( sItem, 0, sizeof(sItem) );
	
	while( loop )
	{
		Wdog();

		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			RetVal = userExitMenu();	// exit
			loop = 0;	// exit
		}
		else
		{
			// make the visible portion of the list
			for( iItem=0,nItem=0; iItem<MAX_ALARM_NUM_LIST; iItem++ )
			{
				if( (ps = AlarmGet( index )) == NULL )
				{
					break;
				}
				else if( (ps->timestamp == 0) || (ps->TipoAllarme == NO_ALARM) )
				{
					break;
				}
				else
				{
					if( iItem == 0 )
					{
						visFirst = index;	// update the index of the first Alarm Record displayed
					}
					visLast = index;	// update the index of the last Alarm Record displayed
					if( sItem[iItem] == NULL )
					{
						sItem[iItem] = (char *)malloc( MENU_MAX_COL+1 );
					}
					if( sItem[iItem] != NULL )
					{
						// make the item string
						Sec2DateTime( ps->timestamp, &tm );
						rtcStrMakeDate( s, &tm );
						rtcStrMakeTime( &s[12], &tm );
						mysprintf( sItem[iItem], "A%02u: %s %s", ps->TipoAllarme, s, &s[12] );
						nItem++;
						index++;
					}
					else
					{
						break;
					}
				}
			}
			if( nItem  )
			{
				index = visFirst;	// now 'index' is the index of the first Alarm Record to be displayed
				// display the visible portion of the list
				iItem = SelItem;
				RetVal = EVE_WinList( &SelItem,
									(char *)GetMsg( 8, NULL, iLanguage, d_STORICO_ALLARMI ),
									(const char **)sItem,
									nItem,
									iItem,
									NULL,
									NULL
									);
				switch( RetVal )
				{
					case SCREEN_ERROR:
					case SCREEN_STATUS_CHANGED:
					case SCREEN_TIMEOUT:
						// abort
						RetVal = userExitMenu();
						loop = 0;	// exit
					break;
						
					case PULSESC_PRESSED:
						// exit
						RetVal = RetMenuInfo();
						loop = 0;	// exit
					break;
					
					/* case LIST_UPDATE_UP:
						// update the list for scroll up
						while( (ps = AlarmGet( visLast )) == NULL )
						{
							if( visFirst )
							{
								visFirst--;
							}
							if( visLast )
							{
								visLast--;
							}
							else
							{
								break;
							}
						}
						index = visFirst;
						// now 'index' is the index of the new first Alarm Record to be displayed
					break;
						
					case LIST_UPDATE_DOWN:
						// update the list for scroll down
						while( (ps = AlarmGet( visFirst )) == NULL )
						{
							visFirst++;
							//visLast++;
						}
						index = visFirst;
						// now 'index' is the index of the new first Alarm Record to be displayed
					break; */
					
					case PULSOK_PRESSED:
						// display the selected Alarm Record
						if( (ps = AlarmGet( visFirst + SelItem )) != NULL )
						{
						#ifdef DEBUG_ALARM_REC
							#ifdef LOGGING
								myprintf( "Item %u selected\r\n", (visFirst + SelItem) );
								myprintf( "\tDate-Time: %u\r\n", ps->timestamp );
								myprintf( "\tAlarm: %u\r\n", ps->TipoAllarme );
							#endif
						#endif
							RetVal = VisAlmRec( ps );
							switch( RetVal )
							{
								case SCREEN_ERROR:
								case SCREEN_STATUS_CHANGED:
								case SCREEN_TIMEOUT:
									// abort
									RetVal = userExitMenu();
									loop = 0;	// exit
								break;
									
								case PULSESC_PRESSED:
									// continue
								break;
							}
						}
					break;
				}
			}
			else
			{
				// exit
				RetVal = RetMenuInfo();
				loop = 0;	// exit
			}
		}
	}

	for( iItem=0; iItem<MAX_ALARM_NUM_LIST; iItem++ )
	{
		if( sItem[iItem] != NULL )
		{
			free( sItem[iItem] );
		}
	}
	
	return RetVal;
}



