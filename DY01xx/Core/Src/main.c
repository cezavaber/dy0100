/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "TimeBase.h"
#include "DecodInput.h"
#include "OT_Plus.h"
#include "nvram.h"
#include "rtc.h"
#include "Led.h"
#include "user.h"
#include "ctp.h"
#include "DataFlash.h"
#include "crono.h"
#include "i2cm_drv.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* Constants required to manipulate the core.  Registers first... */
#define portNVIC_SYSTICK_CTRL_REG			( * ( ( volatile uint32_t * ) 0xe000e010 ) )
#define portNVIC_SYSTICK_LOAD_REG			( * ( ( volatile uint32_t * ) 0xe000e014 ) )
#define portNVIC_SYSTICK_CURRENT_VALUE_REG	( * ( ( volatile uint32_t * ) 0xe000e018 ) )
#define portNVIC_SYSPRI2_REG				( * ( ( volatile uint32_t * ) 0xe000ed20 ) )
/* ...then bits in the registers. */
#define portNVIC_SYSTICK_INT_BIT			( 1UL << 1UL )
#define portNVIC_SYSTICK_ENABLE_BIT			( 1UL << 0UL )
#define portNVIC_SYSTICK_COUNT_FLAG_BIT		( 1UL << 16UL )
#define portNVIC_PENDSVCLEAR_BIT 			( 1UL << 27UL )
#define portNVIC_PEND_SYSTICK_CLEAR_BIT		( 1UL << 25UL )

#define portNVIC_PENDSV_PRI					( ( ( uint32_t ) configKERNEL_INTERRUPT_PRIORITY ) << 16UL )
#define portNVIC_SYSTICK_PRI				( ( ( uint32_t ) configKERNEL_INTERRUPT_PRIORITY ) << 24UL )

extern void vPortSetupTimerInterrupt( void );

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;

IWDG_HandleTypeDef hiwdg;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;

UART_HandleTypeDef huart2;
UART_HandleTypeDef huart6;

osThreadId MAINHandle;
osThreadId USERHandle;
osThreadId CTPHandle;
osTimerId LedTimerHandle;

osMutexDef(printfMutex);
osMutexId printfMutexHandle;

osMutexDef(EVEMutex);
osMutexId EVEMutexHandle;

osMutexDef(I2CMutex);
osMutexId I2CMutexHandle;

/* USER CODE BEGIN PV */

/*! \var SystemFlags
    \brief Bitmap flag di sistema
*/
TBitDWord SystemFlags;

#ifdef OT_LOGGING
unsigned long TimeSec;	// contatore 0.01s
#endif

/*!
 *  \var OpeMode
 *  \brief Modo operativo
 */
unsigned char OpeMode;

/*!
	\var userReq
	\brief Richiesta da pulsante utente
*/
unsigned char UserRequest;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_RTC_Init(void);
static void MX_USART6_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_ADC1_Init(void);
static void MX_SPI2_Init(void);
static void MX_I2C1_Init(void);
#ifdef WDOG
static void MX_IWDG_Init(void);
#endif
void Main_Task(void const * argument);
void User_Task(void const * argument);
void LedTimer_Callback(void const * argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/*!
** \fn void CreateMutex( void )
** \brief Create all the used mutex
** \return None
**/
void CreateMutex( void )
{
	printfMutexHandle = osMutexCreate(osMutex(printfMutex));
	EVEMutexHandle = osMutexCreate(osMutex(EVEMutex));
	I2CMutexHandle = osMutexCreate(osMutex(I2CMutex));
}

/*!
** \fn void DeleteMutex( void )
** \brief Delete all the used mutex
** \return None
**/
void DeleteMutex( void )
{
	osMutexDelete( printfMutexHandle );
	printfMutexHandle = NULL;
	osMutexDelete( EVEMutexHandle );
	EVEMutexHandle = NULL;
	osMutexDelete( I2CMutexHandle );
	I2CMutexHandle = NULL;
}

/*!
** \fn void SystemReset( void )
** \brief System reset
** \return None
**/
void SystemReset( void )
{
	/* disable all interrupts */
	__disable_irq();
	/* System Reset */
	HAL_NVIC_SystemReset();
	// ------> NON PASSA MAI DI QUI
}

/*!
** \fn void SystemRestart( void )
** \brief System restart
** \return None
**/
void SystemRestart( void )
{
#ifdef LOGGING
	myprintf("\r\n\nRESTART...\r\n\n");
#endif
	SetBacklight( 0 );

	if( osKernelRunning() )
	{
		osThreadSuspendAll();	//!< Suspends the scheduler
		vTaskEndScheduler();	//!< Ends the scheduler (disable all interrupts with portDISABLE_INTERRUPTS())
	}

	/* Stop the SysTick and disable its interrupt. */
	portNVIC_SYSTICK_CTRL_REG &= ~( portNVIC_SYSTICK_INT_BIT | portNVIC_SYSTICK_ENABLE_BIT );
	/* Clear the SysTick count flag and set the count value back to
	zero. */
	portNVIC_SYSTICK_CURRENT_VALUE_REG = 0UL;

	Wdog();

	// power off dataflash
	HAL_GPIO_WritePin( EN_FLASH_GPIO_Port, EN_FLASH_Pin, GPIO_PIN_SET );
	// power off VBATT OPA
	HAL_GPIO_WritePin(EN_V_BATT_GPIO_Port, EN_V_BATT_Pin, GPIO_PIN_RESET);
	// Radio-modem RCQ2-868 in low-power
	OT_PH_SetLowPower();
	// FT801 in low-power
	HAL_GPIO_WritePin(PD_FT801_GPIO_Port, PD_FT801_Pin, GPIO_PIN_RESET);
	// buzzer off
	BSP_BUZ_OnOff( BUZ_OFF );
	// switch-off all external peripherals
	HAL_GPIO_WritePin( EN_PWR_GPIO_Port, EN_PWR_Pin, GPIO_PIN_SET );
	
	/* if( DIGIN_PULS )
	{
		// PUSHB is pressed: enable bootloader on restart
		ENABLE_BOOT = 1;
	} */
	StatSec.LastNumSec = SecCounter;
	nvramUpdate();
	
	SystemReset();
	
}

/*!
** \fn void SystemOff( void )
** \brief System power-off
** \return None
**/
void SystemOff( void )
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	unsigned long NSec = 0;

#ifdef LOGGING
	myprintf("\r\n\nPOWER-OFF...\r\n\n");
#endif
	SetBacklight( 0 );

	if( osKernelRunning() )
	{
		osThreadSuspendAll();	//!< Suspends the scheduler
		vTaskEndScheduler();	//!< Ends the scheduler (disable all interrupts with portDISABLE_INTERRUPTS())
	}

	/* Stop the SysTick and disable its interrupt. */
	portNVIC_SYSTICK_CTRL_REG &= ~( portNVIC_SYSTICK_INT_BIT | portNVIC_SYSTICK_ENABLE_BIT );
	/* Clear the SysTick count flag and set the count value back to
	zero. */
	portNVIC_SYSTICK_CURRENT_VALUE_REG = 0UL;

	// disable all EXTI interrupts
	HAL_NVIC_DisableIRQ(EXTI1_IRQn);
	HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
	// also disable INT_FT801, if used

	Wdog();

	// power off dataflash
	HAL_GPIO_WritePin( EN_FLASH_GPIO_Port, EN_FLASH_Pin, GPIO_PIN_SET );
	// power off VBATT OPA
	HAL_GPIO_WritePin(EN_V_BATT_GPIO_Port, EN_V_BATT_Pin, GPIO_PIN_RESET);
	// Radio-modem RCQ2-868 in low-power
	OT_PH_SetLowPower();
	// FT801 in low-power
	HAL_GPIO_WritePin(PD_FT801_GPIO_Port, PD_FT801_Pin, GPIO_PIN_RESET);
	// buzzer off
	BSP_BUZ_OnOff( BUZ_OFF );
	// switch-off all external peripherals
	HAL_GPIO_WritePin( EN_PWR_GPIO_Port, EN_PWR_Pin, GPIO_PIN_SET );

	StatSec.LastNumSec = SecCounter;
	nvramUpdate();

	Wdog();

	/* stop and deinit all the used peripherals */
	HAL_UART_DeInit( &huart2 );
	HAL_I2C_DeInit( &hi2c1 );
	HAL_SPI_DeInit( &hspi1 );
	HAL_SPI_DeInit( &hspi2 );
	//HAL_RTC_DeInit( &hrtc );
	HAL_ADC_DeInit( &hadc1 );
	/* DMA controller clock disable */
	__HAL_RCC_DMA2_CLK_DISABLE();

	// To enter in STANDBY mode all the wake-up sources must be low
	if( DIGIN_PULS || DIGIN_ALIM )
	{
	#ifdef LOGGING
		if( DIGIN_PULS )
		{
			myprintf("PUSHB is pressed\r\n");
		}
		if( DIGIN_ALIM )
		{
			myprintf("SUPPLY is present\r\n");
		}
		myprintf("STOP Mode cycles: ");
	#endif
		if( !DIGIN_PULS )
		{
			NSec = 0;
		}

		// Configure all GPIO as analog to reduce current consumption on non used IOs
		/* GPIO Ports Clock Enable */
		__HAL_RCC_GPIOC_CLK_ENABLE();
		__HAL_RCC_GPIOH_CLK_ENABLE();
		__HAL_RCC_GPIOA_CLK_ENABLE();
		__HAL_RCC_GPIOB_CLK_ENABLE();
		__HAL_RCC_GPIOD_CLK_ENABLE();
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Pin = GPIO_PIN_All;
		HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
		HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	#ifdef LOGGING
		GPIO_InitStruct.Pin = GPIO_PIN_All & (~GPIO_PIN_6);	// keep active USART6_TX pin on GPIOC
	#endif
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
		GPIO_InitStruct.Pin = GPIO_PIN_All & (~(PRES_RETE_Pin | PULS_Pin));	// keep active PRES_RETE_Pin and PULS_Pin pins on GPIOB
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
		/* GPIO Ports Clock Disable */
		__HAL_RCC_GPIOD_CLK_DISABLE();
		__HAL_RCC_GPIOH_CLK_DISABLE();
		__HAL_RCC_GPIOA_CLK_DISABLE();
	#ifndef LOGGING
		__HAL_RCC_GPIOC_CLK_DISABLE();
	#endif
		//__HAL_RCC_GPIOB_CLK_DISABLE();
		
		// the RTC wake-up timer interrupt must not be disabled
		__HAL_RTC_WAKEUPTIMER_CLEAR_FLAG(&hrtc, RTC_FLAG_WUTF);
		HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);
		/* enable all interrupts */
		portENABLE_INTERRUPTS();	//__enable_irq();
		
		// enter in STOP mode for 1s and then check all the wake-up sources
		do
		{
			Wdog();
			PULS_INP = DIGIN_PULS;
			ALIM_INP = DIGIN_ALIM;
		#ifdef LOGGING
			outbyte( 'I' );
			HAL_UART_DeInit( &huart6 );
		#endif

			// Wake-up timer already enabled every 1s

			/* FLASH Deep Power Down Mode enabled */
			HAL_PWREx_EnableFlashPowerDown();

			/*## Enter Stop Mode #######################################################*/
			HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);

			/*## Exit from Stop Mode #######################################################*/
			Wdog();
			//SystemClock_Config();
		#ifdef LOGGING
			MX_USART6_UART_Init();	// HSI = system clock => bit rate is set to 19200
			outbyte( 'O' );
		#endif
			// check all the wake-up sources
			if( HAL_GPIO_ReadPin( PULS_GPIO_Port, PULS_Pin ) == GPIO_PIN_SET )
			{
				DIGIN_PULS = 1;
				NSec++;
			}
			else
			{
				DIGIN_PULS = 0;
				NSec = 0;
			}
			if( HAL_GPIO_ReadPin( PRES_RETE_GPIO_Port, PRES_RETE_Pin ) == GPIO_PIN_SET )
			{
				DIGIN_ALIM = 1;
			}
			else
			{
				DIGIN_ALIM = 0;
			}
			if( //(!PULS_INP && DIGIN_PULS)
				(NSec >= T_PULS_ON)	// PUSHB is pressed for the minimum T_PULS_ON time
				|| (!ALIM_INP && DIGIN_ALIM)	// SUPPLY was connected
				)
			{
				// simulate a wake-up event: restart
			#ifdef LOGGING
				outbyte( 'W' );
			#endif
				SystemReset();
			}

		} while( DIGIN_PULS || DIGIN_ALIM );
	}

#ifdef LOGGING
	myprintf("STANDBY Mode entry...\r\n");
#endif
	HAL_UART_DeInit( &huart6 );
	/* GPIO Ports Clock Disable */
	__HAL_RCC_GPIOC_CLK_DISABLE();
	__HAL_RCC_GPIOH_CLK_DISABLE();
	__HAL_RCC_GPIOA_CLK_DISABLE();
	__HAL_RCC_GPIOB_CLK_DISABLE();
	__HAL_RCC_GPIOD_CLK_DISABLE();

	/* disable all interrupts */
	__disable_irq();

	Wdog();
	
	/* Disable Wake-up timer */
	HAL_NVIC_DisableIRQ(RTC_WKUP_IRQn);	// Interrupt disable
	HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);

	/*## Clear all related wakeup flags ########################################*/

	/* Disable all used wakeup sources: Pin1(PA.0) */
	HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN1);
	/* Clear PWR wake up Flag */
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

	/* Clear RTC Wake Up timer Flag */
	__HAL_RTC_WAKEUPTIMER_CLEAR_FLAG(&hrtc, RTC_FLAG_WUTF);

	/* Enable the Backup SRAM low power Regulator */
	HAL_PWREx_EnableBkUpReg();

	/* Re-enable all used wakeup sources: Pin1(PA.0) */
	HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1);

	/*## Enter Standby Mode ####################################################*/
	/* Request to enter STANDBY mode  */
	HAL_PWR_EnterSTANDBYMode();
	// ------> NON PASSA MAI DI QUI
	
	/*!
	 * ATTENZIONE
	 * Il watchdog, se abilitato, rimane in funzione.
	 * In questo caso la cpu si risveglia per il reset da watchdog.
	 * Il bootloader all'avvio deve riconoscere il reset da watchdog avvenuto in
	 * Standby mode e ritornare subito in Standby mode senza abilitare il 
	 * watchdog.
	 */
	
	SystemReset();	// se fallisse l'ingresso in Standby mode...
}

/*!
** \fn void Check4StandbyPause( void )
** \brief Verifica se entrare in pausa standby
** \return None
**/
static void Check4StandbyPause( void )
{
	unsigned long NSec;
	unsigned long timeNow;
	unsigned long timeSleep;
	unsigned long timeCrono;
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	
#ifdef DISCHARGE_BATTERY
	// annulla pausa lunga
	OT_DL_Pausa = OT_DL_PAUSE;
	OpeMode = MODE_FULL_POWER;
	return;
#endif

	if( 
		(OpeMode == MODE_STANDBY)
		&& (userCurrentState == USER_STATE_PAUSE)
		)
	{
		UserRequest = USER_REQ_NONE;

		// stabilisce la durata della pausa standby
		if( OT_Stat != OT_STAT_LINKED )
		{
			// unlinked
			timeNow = STANDBY_TIME_UNLINKED;
			NSec = DateTime.tm_sec;
		}
		else
		{
			// linked
			
			// ora corrente in secondi
			timeNow = 3600L*DateTime.tm_hour + 60L*DateTime.tm_min + (unsigned long)DateTime.tm_sec;
			NSec = DateTime.tm_sec;
			
			if( Sleep.Ore != SLEEP_OFF )
			{
				// prossima ora di sleep in secondi
				timeSleep = 3600L*Sleep.Ore + 60L*Sleep.Minuti;
				// calcolo differenza con ora corrente
				if( timeSleep >= timeNow )
					timeSleep -= timeNow;
				else
					timeSleep = 0;
			}
			else
			{
				timeSleep = STANDBY_TIME;
			}
			
			if( Crono.CronoMode.BIT.Enab == CRONO_ON )
			{
				// prossima ora di crono in secondi (inizio di ogni quarto d'ora)
				timeCrono = 3600L*DateTime.tm_hour;
				if( (DateTime.tm_min == 0) || (DateTime.tm_min == 15) ||
					(DateTime.tm_min == 30) || (DateTime.tm_min == 45) )
				{
					timeCrono = 0;	// salta se siamo nel primo minuto di ogni quarto d'ora
				}
				else if( DateTime.tm_min < 15 )
					timeCrono += 900;
				else if( DateTime.tm_min < 30 )
					timeCrono += 1800;
				else if( DateTime.tm_min < 45 )
					timeCrono += 2700;
				else
					timeCrono += 3600;
				// calcolo differenza con ora corrente
				if( timeCrono >= timeNow )
					timeCrono -= timeNow;
				else
					timeCrono = 0;
			}
			else
			{
				timeCrono = STANDBY_TIME;
			}
			
			// in 'timeSleep' il minimo tra 'timeSleep' e 'timeCrono'
			if( timeSleep > timeCrono )
				timeSleep = timeCrono;
			
			// esce se inferiore al tempo minimo di pausa standby
			if( timeSleep < STANDBY_TIME_MIN )
			{
				// annulla pausa lunga
				OT_DL_Pausa = OT_DL_PAUSE;
				OpeMode = MODE_FULL_POWER;
				return;
			}
			
			// in 'timeNow' il tempo di pausa standby
			timeNow = timeSleep;
			if( timeNow > STANDBY_TIME )
				timeNow = STANDBY_TIME;
		}

	#ifdef LOGGING
		myprintf("\r\n\nPAUSE...\r\n\n");
	#endif
		if( osKernelRunning() )
		{
			osThreadSuspendAll();	//!< Suspends the scheduler
			/*
			 * WARNING
			 * The suspended kernel is still runnning, but its timing is not working any more.
			 * Every osDelay() or osMutexWait() (like in LOCKprintf()) shall not be used until the kernel is resumed.
			 */

			// delete all the mutex
			DeleteMutex();
		}
		
		while( (NSec != DateTime.tm_sec) && timeNow )
		{
			// dal calcolo del tempo di pausa standby i secondi possono essere cambiati
			if( ++NSec >= 60 )
				NSec = 0;
			timeNow--;
		}
		NSec = 0;

		// dataflash disable
		df_Deinit( EE_BANK_0 );
		// Radio-modem RCQ2-868 in low-power
		OT_PH_SetLowPower();
		// buzzer off
		led_Set( BUZ, LEDSTAT_OFF );
		BSP_BUZ_OnOff( BUZ_OFF );
		// power off VBATT OPA
		//HAL_GPIO_WritePin(EN_V_BATT_GPIO_Port, EN_V_BATT_Pin, GPIO_PIN_RESET);
		inpSuspend();

		// disable all EXTI interrupts
		HAL_NVIC_DisableIRQ(EXTI1_IRQn);
		//HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);

		/* disable all interrupts */
		__disable_irq();

		/* Stop the SysTick and disable its interrupt. */
		portNVIC_SYSTICK_CTRL_REG &= ~( portNVIC_SYSTICK_INT_BIT | portNVIC_SYSTICK_ENABLE_BIT );
		/* Clear the SysTick count flag and set the count value back to
		zero. */
		portNVIC_SYSTICK_CURRENT_VALUE_REG = 0UL;

		// Configure PULS_Pin as external falling interrupt
		GPIO_InitStruct.Pin = PULS_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(PULS_GPIO_Port, &GPIO_InitStruct);
		// enable PULS EXTI interrupt
		HAL_NVIC_SetPriority(EXTI4_IRQn, 5, 0);
		HAL_NVIC_EnableIRQ(EXTI4_IRQn);

		if( ACC_IRQ )
		{
			ACC_IRQ = 0;
			// clear INTx_ACC interrupt
			//INT1_Acc_irq();
			INT2_Acc_irq();
		}

		/* enable all interrupts */
		__enable_irq();

		Wdog();
		
	#ifdef LOGGING
		//myprintf("SLEEP Mode cycles: ");
		HAL_UART_Transmit(&huart6, (uint8_t *)"SLEEP Mode cycles: ", 18, HAL_MAX_DELAY);
	#endif
		PULS_IRQ = 0;
		do
		{
			PULS_INP = DIGIN_PULS;
			ALIM_INP = DIGIN_ALIM;
		#ifdef LOGGING
			outbyte( 'I' );
		#endif
			
				// Wake-up timer already enabled every 1s
				// Wake-up for pushbutton interrupt
				// Wake-up for INTx_ACC interrupt

				Wdog();
				if( !PULS_IRQ	// do not enter in SLEEP mode, if a pushbutton interrupt occurred
					&& !ACC_IRQ	// do not enter in SLEEP mode, if a INTx_ACC interrupt occurred
					)
				{
					/* Request to enter SLEEP mode */
					HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
				}
				Wdog();
			
		#ifdef LOGGING
			outbyte( 'O' );
		#endif

			// aggiorna il tempo trascorso in pausa standby
			if( RTC_TIME )
			{
				// procedure ogni 1s
				NSec++;
				if( NSec < timeNow )
				{
					RTC_TIME = 0;
				}
				else
				{
					// RTC_TIME = 1 per eventuale attivazione procedure esterne (Crono-Sleep) ogni 1 s
				}
			
				/* IN PAUSA STANDBY NON SI MISURA PIU' LA TEMPERATURA AMBIENTE
				// eventuale acquisizione temperatura ambiente
				if( OT_Stat == OT_STAT_LINKED )
				{
					// linked
					if( NSec && !(NSec % STANDBY_TAIN_SAMPLE) )
					{
						// acquisizione temperatura ambiente
					#ifdef LOGGING
						outbyte( 'T' );
					#endif
						ExecTAConv();
					}
				}
				*/
			}

			// acquisizione pulsante
			if( HAL_GPIO_ReadPin( PULS_GPIO_Port, PULS_Pin ) == GPIO_PIN_SET )
			{
				DIGIN_PULS = 1;
			}
			else
			{
				DIGIN_PULS = 0;
			}
			if( HAL_GPIO_ReadPin( PRES_RETE_GPIO_Port, PRES_RETE_Pin ) == GPIO_PIN_SET )
			{
				DIGIN_ALIM = 1;
			}
			else
			{
				DIGIN_ALIM = 0;
			}
			
			// check comando uscita da pausa standby
			if( 
				// comando utente (rilascio pulsante)
				(PULS_INP && !DIGIN_PULS) || PULS_IRQ
				// comando utente (accelerometro)
				|| ACC_IRQ
				// connessione alimentazione esterna
				|| (!ALIM_INP && DIGIN_ALIM)
				// COV temperatura ambiente
				//|| ((OT_Stat == OT_STAT_LINKED) && (PreTempAmbPAN != TEMP_AMB_PAN))	
				// fine tempo di pausa standby
				|| (NSec >= timeNow)		
				)
			{
				// comando uscita da low-power
				OpeMode = MODE_FULL_POWER;
			}
		} while( OpeMode == MODE_STANDBY );
			
		Wdog();
		
	#ifdef LOGGING
		outbyte( 'R' );
	#endif
		/* disable all interrupts */
		__disable_irq();
		
		// disable PULS EXTI interrupt
		HAL_NVIC_DisableIRQ(EXTI4_IRQn);
		// Configure PULS_Pin as GPIO input
		GPIO_InitStruct.Pin = PULS_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(PULS_GPIO_Port, &GPIO_InitStruct);
		
		// reset monitor di ricarica della batteria 
		CntSenseBat = 0;
		CntChargeBat = 0;
		
		/* Make PendSV and SysTick the lowest priority interrupts. */
		portNVIC_SYSPRI2_REG |= portNVIC_PENDSV_PRI;
		portNVIC_SYSPRI2_REG |= portNVIC_SYSTICK_PRI;
		/* Start the SysTick. */
		vPortSetupTimerInterrupt();

		/* enable all interrupts */
		__enable_irq();
		
		// dataflash enable
		df_Init( EE_BANK_0 );
		// restart OT/+ communication
		OT_ExitStandby();

		// enable all EXTI interrupts
		//HAL_NVIC_EnableIRQ(EXTI1_IRQn);
		//HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
		
		// allineamento data-ora a fine pausa standby
		rtcRd( NULL );

		if( osKernelRunning() )
		{
			// create all the mutex
			CreateMutex();

			osThreadResumeAll();
		}

	#ifdef LOGGING
		myprintf("\r\n\tRisveglio da:\r\n");
		// comando utente (rilascio pulsante)
		if( (PULS_INP && !DIGIN_PULS) || PULS_IRQ )
		{
			myprintf("\t\tUTENTE (pulsante)\r\n");
		}
		// comando utente (accelerometro)
		if( ACC_IRQ )
		{
			myprintf("\t\tUTENTE (accelerometro)\r\n");
		}
		// connessione alimentazione esterna
		if(!ALIM_INP && DIGIN_ALIM)
		{
			myprintf("\t\tALIM\r\n");
		}
		// COV temperatura ambiente
		/* if( ((OT_Stat == OT_STAT_LINKED) || (OT_Stat == OT_STAT_UNLINKED_STBY_WAIT))
			&& (PreTempAmbPAN != TEMP_AMB_PAN)
			)
		{
			myprintf("\t\tTEMP [%d => %d in 0.5�C\r\n", PreTempAmbPAN, TEMP_AMB_PAN);
		} */
		// fine tempo di pausa standby
		if(NSec >= timeNow)
		{
			myprintf("\t\tTIME\r\n");
		}
	#endif
		// eventuale riavvio interfaccia utente
		if( (PULS_INP && !DIGIN_PULS) || PULS_IRQ	// per comando utente (rilascio pulsante)
			|| ACC_IRQ	// per comando utente (accelerometro)
			|| (!ALIM_INP && DIGIN_ALIM)// per connessione alimentazione esterna
			)
		{
			// segnala richiesta utente WAKEUP
			UserRequest = USER_REQ_WAKEUP;
			cntExit4Alarm = 0;
		}
		// allineamento stato ingressi di wake-up
		if( (PULS_INP && !DIGIN_PULS) || PULS_IRQ )
		{
			// comando utente (rilascio pulsante)
			DIGIN_PULS = 0;	// per sicurezza forzo stato pulsante rilasciato
			PULS_LOCK = 1;
		}
		PULS_IRQ = 0;
		ALIM_INP = DIGIN_ALIM;
		PULS_INP = DIGIN_PULS;
		if( DIGIN_PULS )
		{
			starttimer( TKEYB, 3*T1SEC );
		}
		if( ACC_IRQ )
		{
			ACC_IRQ = 0;
			// clear INTx_ACC interrupt
			//INT1_Acc_irq();
			INT2_Acc_irq();
		}
		inpRestart();

		/* A fine pausa standby attendi assestamento alimentazione radio,
		 * per non inviare eventuali comandi Crono/Sleep prima che il modem sia
		 * pronto.
		 */
		osDelay(350);
	}
}

/*!
** \fn void SleepOnBatteryCharge( void )
** \brief System sleep on battery charging
** \return None
**/
void SleepOnBatteryCharge( void )
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

#ifdef DISCHARGE_BATTERY
	// annulla sleep on battery charging
	return;
#endif

#ifdef LOGGING
	myprintf("\r\n\nSLEEP (battery charging)...\r\n\n");
#endif
	if( osKernelRunning() )
	{
		osThreadSuspendAll();	//!< Suspends the scheduler
		/*
		 * WARNING
		 * The suspended kernel is still runnning, but its timing is not working any more.
		 * Every osDelay() or osMutexWait() (like in LOCKprintf()) shall not be used until the kernel is resumed.
		 */

		// delete all the mutex
		DeleteMutex();
	}

	// dataflash disable
	df_Deinit( EE_BANK_0 );
	// Radio-modem RCQ2-868 in low-power
	OT_PH_SetLowPower();
	// buzzer off
	led_Set( BUZ, LEDSTAT_OFF );
	BSP_BUZ_OnOff( BUZ_OFF );
	// power off VBATT OPA
	//HAL_GPIO_WritePin(EN_V_BATT_GPIO_Port, EN_V_BATT_Pin, GPIO_PIN_RESET);
	inpSuspend();

	// disable all EXTI interrupts
	HAL_NVIC_DisableIRQ(EXTI1_IRQn);
	HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);

	/* disable all interrupts */
	__disable_irq();

	/* Stop the SysTick and disable its interrupt. */
	portNVIC_SYSTICK_CTRL_REG &= ~( portNVIC_SYSTICK_INT_BIT | portNVIC_SYSTICK_ENABLE_BIT );
	/* Clear the SysTick count flag and set the count value back to
	zero. */
	portNVIC_SYSTICK_CURRENT_VALUE_REG = 0UL;

	// Configure PULS_Pin as external falling interrupt
	GPIO_InitStruct.Pin = PULS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(PULS_GPIO_Port, &GPIO_InitStruct);
	// enable PULS EXTI interrupt
	HAL_NVIC_SetPriority(EXTI4_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(EXTI4_IRQn);

	/* enable all interrupts */
	__enable_irq();

	Wdog();

#ifdef LOGGING
	//myprintf("SLEEP Mode cycles: ");
	HAL_UART_Transmit(&huart6, (uint8_t *)"SLEEP Mode cycles: ", 18, HAL_MAX_DELAY);
#endif
	PULS_IRQ = 0;
	while( 1 )
	{
		PULS_INP = DIGIN_PULS;
		ALIM_INP = DIGIN_ALIM;
	#ifdef LOGGING
		outbyte( 'I' );
	#endif

			// Wake-up timer already enabled every 1s
			// Wake-up for pushbutton interrupt

			Wdog();
			if( !PULS_IRQ )	// do not enter in SLEEP mode, if a pushbutton interrupt occurred
			{
				/* Request to enter SLEEP mode */
				HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
			}
			Wdog();

	#ifdef LOGGING
		outbyte( 'O' );
	#endif

		// aggiorna il tempo trascorso in sleep
		if( RTC_TIME )
		{
			// procedure ogni 1s
			RTC_TIME = 0;

		}

		// acquisizione pulsante
		if( HAL_GPIO_ReadPin( PULS_GPIO_Port, PULS_Pin ) == GPIO_PIN_SET )
		{
			DIGIN_PULS = 1;
		}
		else
		{
			DIGIN_PULS = 0;
		}
		if( HAL_GPIO_ReadPin( PRES_RETE_GPIO_Port, PRES_RETE_Pin ) == GPIO_PIN_SET )
		{
			DIGIN_ALIM = 1;
		}
		else
		{
			DIGIN_ALIM = 0;
		}

		// check comando uscita da sleep
		if(
			// comando utente (rilascio pulsante)
			(PULS_INP && !DIGIN_PULS) || PULS_IRQ
			// sconnessione alimentazione esterna
			|| (!DIGIN_ALIM)
			)
		{
			// comando uscita da sleep
			break;
		}
	}

	Wdog();

#ifdef LOGGING
	outbyte( 'R' );
#endif
	/* disable all interrupts */
	__disable_irq();

	// disable PULS EXTI interrupt
	HAL_NVIC_DisableIRQ(EXTI4_IRQn);
	// Configure PULS_Pin as GPIO input
	GPIO_InitStruct.Pin = PULS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(PULS_GPIO_Port, &GPIO_InitStruct);

	// reset monitor di ricarica della batteria 
	CntSenseBat = 0;
	CntChargeBat = 0;
	
	/* Make PendSV and SysTick the lowest priority interrupts. */
	portNVIC_SYSPRI2_REG |= portNVIC_PENDSV_PRI;
	portNVIC_SYSPRI2_REG |= portNVIC_SYSTICK_PRI;
	/* Start the SysTick. */
	vPortSetupTimerInterrupt();

	/* enable all interrupts */
	__enable_irq();

	// dataflash enable
	df_Init( EE_BANK_0 );

	// enable all EXTI interrupts
	__HAL_GPIO_EXTI_CLEAR_IT(INT1_ACC_Pin);
	//HAL_NVIC_EnableIRQ(EXTI1_IRQn);
	__HAL_GPIO_EXTI_CLEAR_IT(INT2_ACC_Pin);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	RTC_TIME = 0;
	rtcRd( NULL );

	if( osKernelRunning() )
	{
		// create all the mutex
		CreateMutex();

		osThreadResumeAll();
	}

#ifdef LOGGING
	myprintf("\r\n\tRisveglio da:\r\n");
	// comando utente (rilascio pulsante)
	if( (PULS_INP && !DIGIN_PULS) || PULS_IRQ )
	{
		myprintf("\t\tUTENTE\r\n");
	}
	// sconnessione alimentazione esterna
	if( !DIGIN_ALIM )
	{
		myprintf("\t\tALIM\r\n");
	}
#endif
	// allineamento stato ingressi di wake-up
	if( (PULS_INP && !DIGIN_PULS) || PULS_IRQ )
	{
		// comando utente (rilascio pulsante)
		DIGIN_PULS = 0;	// per sicurezza forzo stato pulsante rilasciato
		PULS_LOCK = 1;
	}
	PULS_IRQ = 0;
	ALIM_INP = DIGIN_ALIM;
	PULS_INP = DIGIN_PULS;
	if( DIGIN_PULS )
	{
		starttimer( TKEYB, 3*T1SEC );
	}
	ACC_IRQ = 0;
	// clear INTx_ACC interrupt
	//INT1_Acc_irq();
	INT2_Acc_irq();
	inpRestart();
	
	// attesa assestamento pulsante
	while( PULS_LOCK )
	{
		Wdog();
		osDelay(10);
	}

}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	
	FLAGS = 0;
	OpeMode = MODE_LOW_POWER;
	UserRequest = USER_REQ_NONE;

	/* enable all interrupts */
	__enable_irq();

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
#ifdef WDOG
  MX_IWDG_Init();
#endif
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_RTC_Init();
  MX_USART6_UART_Init();
  MX_USART2_UART_Init();
  MX_ADC1_Init();
  MX_SPI1_Init( CLK_SLOW );
  MX_SPI2_Init();
  //MX_I2C1_Init();	// da fare con il bus I2C alimentato

  /* USER CODE BEGIN 2 */

	Wdog();

#ifdef LOGGING
	myprintf("\r\n\n%s\r\n\n", sFW_VERSIONE);
#endif

	DWT_Delay_Init();
	led_Init();

	if( !rtcInit() )
	{
	#ifdef LOGGING
		myprintf("RTC-NVRAM default\r\n");
	#endif
	}
#ifdef LOGGING
	ScriviData( str );
	ScriviOra( &str[12] );
	myprintf("Data-Ora: %s %s\r\n", str, &str[12]);
#endif

	Wdog();

	// switch-on all external peripherals
	HAL_GPIO_WritePin( EN_PWR_GPIO_Port, EN_PWR_Pin, GPIO_PIN_RESET );
	HAL_Delay( 100 );
	// FT801 in low-power
	//HAL_GPIO_WritePin(PD_FT801_GPIO_Port, PD_FT801_Pin, GPIO_PIN_RESET);
	// Radio-modem RCQ2-868 in low-power
	//HAL_GPIO_WritePin(RESET_RF_GPIO_Port, RESET_RF_Pin, GPIO_PIN_SET);	// RADIO reset off
	//HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_SET);	// disable RADIO config
	//HAL_GPIO_WritePin(PD_RF_GPIO_Port, PD_RF_Pin, GPIO_PIN_RESET);	// RADIO power-down on

	// check data-flash bank #0
	if( !df_Init( EE_BANK_0 ) )
	{
	#ifdef LOGGING
		myprintf("DATAFLASH #0 Init error\r\n");
	#endif
	}
	//df_Deinit( EE_BANK_0 );

	// init inputs and I2C slaves
	MX_I2C1_Init();
	i2cInit();
	inpInit();

	Wdog();

  /* USER CODE END 2 */

  /* Create the mutex(es) */
  /* definition and creation of printfMutex */
  //osMutexDef(printfMutex);	spostata come definizione globale
  //printfMutexHandle = osMutexCreate(osMutex(printfMutex));

  /* definition and creation of EVEMutex */
  //osMutexDef(EVEMutex);	spostata come definizione globale
  //EVEMutexHandle = osMutexCreate(osMutex(EVEMutex));

  /* definition and creation of I2CMutex */
  //osMutexDef(I2CMutex);	spostata come definizione globale
  //I2CMutexHandle = osMutexCreate(osMutex(I2CMutex));
  
  CreateMutex();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of LedTimer */
  osTimerDef(LedTimer, LedTimer_Callback);
  LedTimerHandle = osTimerCreate(osTimer(LedTimer), osTimerPeriodic, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
	if( LedTimerHandle )
	{
		osTimerStart( LedTimerHandle, 50 );	// start 50ms led periodic timer
	}
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of MAIN */
  osThreadDef(MAIN, Main_Task, osPriorityNormal, 0, 1024);
  MAINHandle = osThreadCreate(osThread(MAIN), NULL);

  /* definition and creation of USER */
  osThreadDef(USER, User_Task, osPriorityIdle, 0, 1024);
  USERHandle = osThreadCreate(osThread(USER), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
 

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /**Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE
                              |RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 2;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_112CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_VREFINT;
  sConfig.Rank = 2;
  sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

#ifdef WDOG
/**
  * @brief IWDG Initialization Function
  * @param None
  * @retval None
  */
static void MX_IWDG_Init(void)
{

  /* USER CODE BEGIN IWDG_Init 0 */

  /* USER CODE END IWDG_Init 0 */

  /* USER CODE BEGIN IWDG_Init 1 */

  /* USER CODE END IWDG_Init 1 */
  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_16;
  hiwdg.Init.Reload = 4095;
  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN IWDG_Init 2 */

  /* USER CODE END IWDG_Init 2 */

}
#endif

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  //RTC_TimeTypeDef sTime = {0};
  //RTC_DateTypeDef sDate = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /**Initialize RTC Only 
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */
    
  /* USER CODE END Check_RTC_BKUP */

  /**Initialize RTC and set the Time and Date 
  *
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_TUESDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x19;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  } */

  /**Enable the WakeUp 
  */
  if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 2048, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/*!
** \fn void MX_SPI1_Init(unsigned char mode)
** \brief SPI1 init function
** \param mode The SPI clock configuration mode (CLK_SLOW or CLK_FAST)
** \return None
**/
void MX_SPI1_Init(unsigned char mode)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
	if( mode == CLK_FAST )
	{
		hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
	}
	else
	{
		hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
	}
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART6_UART_Init(void)
{

  /* USER CODE BEGIN USART6_Init 0 */

  /* USER CODE END USART6_Init 0 */

  /* USER CODE BEGIN USART6_Init 1 */

  /* USER CODE END USART6_Init 1 */
  huart6.Instance = USART6;
  huart6.Init.BaudRate = 115200;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart6) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART6_Init 2 */

  /* USER CODE END USART6_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, SPI1_NSS_Pin|USART2_RTS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, PD_FT801_Pin|EN_FLASH_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, SPI2_NSS_Pin|CONF_RF_Pin|EN_PWR_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, BUZZER_Pin|PD_RF_Pin|EN_V_BATT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(RESET_RF_GPIO_Port, RESET_RF_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(EN_V_BCKL_GPIO_Port, EN_V_BCKL_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : CHARGE_STAT_Pin */
  GPIO_InitStruct.Pin = CHARGE_STAT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(CHARGE_STAT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PC0 PC10 PC11 PC12 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : INT1_ACC_Pin INT2_ACC_Pin */
  GPIO_InitStruct.Pin = INT1_ACC_Pin|INT2_ACC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : USART2_CTS_Pin */
  GPIO_InitStruct.Pin = USART2_CTS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(USART2_CTS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : SPI1_NSS_Pin USART2_RTS_Pin EN_V_BCKL_Pin */
  GPIO_InitStruct.Pin = SPI1_NSS_Pin|USART2_RTS_Pin|EN_V_BCKL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PD_FT801_Pin EN_FLASH_Pin RESET_RF_Pin */
  GPIO_InitStruct.Pin = PD_FT801_Pin|EN_FLASH_Pin|RESET_RF_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : INT_FT801_Pin BOOT1_Pin PRES_RETE_Pin PULS_Pin */
  GPIO_InitStruct.Pin = INT_FT801_Pin|BOOT1_Pin|PRES_RETE_Pin|PULS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : SPI2_NSS_Pin BUZZER_Pin CONF_RF_Pin PD_RF_Pin 
                           EN_V_BATT_Pin EN_PWR_Pin */
  GPIO_InitStruct.Pin = SPI2_NSS_Pin|BUZZER_Pin|CONF_RF_Pin|PD_RF_Pin 
                          |EN_V_BATT_Pin|EN_PWR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PA9 PA11 PA12 PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PD2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PB8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI1_IRQn, 5, 0);
  //HAL_NVIC_EnableIRQ(EXTI1_IRQn);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 5, 0);
  //HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */

#ifdef LOGGING

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
int outbyte( int c )
{
  /* Place your implementation of fputc here *
  Wdog();
#if defined( LOGGING_UART6 )
  HAL_UART_Transmit(&huart6, (uint8_t *)&c, 1, 1000);
#endif
  Wdog(); */
	debug_out(c);

  return c;
}

/*!
**  \fn short LOCKprintf( void )
**  \brief Lock the printf() resource
**  \return 1: OK, 0: FAILED
**/
short LOCKprintf( void )
{
	short RetVal = 0;

	if( osKernelRunning()
		&& (printfMutexHandle != NULL)
		)
	{
		if( osMutexWait(printfMutexHandle, osWaitForever) == osOK )
		{
			RetVal = 1;
		}
	}

	return RetVal;
}

/*!
**  \fn short UNLOCKprintf( void )
**  \brief Unlock the printf() resource
**  \return 1: OK, 0: FAILED
**/
short UNLOCKprintf( void )
{
	short RetVal = 0;

	if( osKernelRunning()
		&& (printfMutexHandle != NULL)
		)
	{
		if( osMutexRelease(printfMutexHandle) == osOK )
		{
			RetVal = 1;
		}
	}

	return RetVal;
}

#endif	// LOGGING

/* USER CODE END 4 */

/* USER CODE BEGIN Header_Main_Task */
/**
  * @brief  Function implementing the MAIN thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_Main_Task */
void Main_Task(void const * argument)
{

  /* USER CODE BEGIN 5 */
	OT_STAT OT_StatPre = 255;
	char flag = 1;
	
	for( ; ; Wdog() )
	{
		osDelay(50);
#ifdef LOGGING
		if( flag && (DateTime.tm_year == MIN_YEAR_RTC) )
		{
			flag = 0;
			myprintf( "%u RTC ERROR!!!\r\n", TimeSec );
		}
#endif
		
		if( RTC_TIME )
		{
			// procedure ogni 1s
			RTC_TIME = 0;
		
			rtcRd( NULL );
			
			/*
				L'eventuale ingresso in standby deve essere sincronizzato
				al secondo con la gestione Crono-Sleep.
			*/
			Check4StandbyPause();	// ingresso in pausa standby?
		
			if(
				(
					(userCurrentState > USER_STATE_RESET) &&
					(userCurrentState < USER_STATE_COLLAUDO)
				)
				|| (userCurrentState == USER_STATE_PAUSE)
				)
			{
				// Gestione del crono.
				GestCronoSleep();
			}
			// aggiorna la NVRAM
			StatSec.LastNumSec = SecCounter;
			nvramUpdate();

			// OT link status
			if( OT_StatPre != OT_Stat )
			{
				OT_StatPre = OT_Stat;
				//if( OT_Stat == OT_STAT_UNLINKED )
				if( OT_Stat < OT_STAT_UNLINKED_WRITE_VERFW )	// verSW is being requested
				{
				#ifdef LOGGING
					myprintf( "SLAVE OT/+ not connected\r\n" );
				#endif
				}
				else if( OT_Stat == OT_STAT_LINKED )
				{
				#ifdef LOGGING
					myprintf( "SLAVE OT/+ connected\r\n" );
				#endif
				}
			}

			// monitor acquisizioni analogiche
			inpMonitor();
		}
	}
  /* USER CODE END 5 */ 
}

/* LedTimer_Callback function */
void LedTimer_Callback(void const * argument)
{
  /* USER CODE BEGIN LedTimer_Callback */
	static unsigned char cnt = 0;

	DecodSenseCharge();	//!< gestione stato batteria ogni 50ms
	if( ++cnt >= 2 )	// ogni 100ms...
	{
		cnt = 0;
		led_Handler();	//!< led handler
	}
  /* USER CODE END LedTimer_Callback */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
#ifdef LOGGING
	myprintf( "HAL Error\r\n" );
#endif
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
#ifdef LOGGING
	myprintf( "Wrong parameters value: file %s on line %d\r\n", file, line );
#endif
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
