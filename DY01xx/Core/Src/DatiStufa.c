/*!
	\file DatiStufa.c
	\brief Modulo DatiStufa.c

	Strutture dati della stufa.

*/

#include "DatiStufa.h"



/* variabili non inizializzate */
//#pragma SECTION bss DatiStufa_bss

PANENAB3 PanEnab3;			//!< abilitazioni 3

/*! 16 cifre numero telefonico assistenza.
	 Le cifre piu` a sx del numero corrispondono ai membri del
	 vettore con indice minore.
*/
SERVNUM ServNumber[SERVNUM_SIZE];

short iTempAmbPAN;
unsigned short TBlackout;
TBitWord CoilBMap0;
_STRUCT_CMD_MB_ gStrCmdMB;
unsigned	char gunTemp;
STRUCT_SPI_II StructSPI_2;
unsigned char NumFan;
DATASPI0 DataSPI0;
DATA_FAN3 DataFan3;
unsigned char gBySchedaCod;
unsigned char gBySchedaVers;
unsigned char gBySchedaRev;
unsigned char gBySchedaBuild;
unsigned char gBySicurezzaCod;
unsigned char gBySicurezzaVers;
unsigned char gBySicurezzaRev;
unsigned char gBySicurezzaBuild;
unsigned char StatoStufa;
unsigned char StatoLogico;
unsigned char StatoLogicoPassato;
unsigned char StatoAttuazioni;
unsigned char TipoAllarme;
short TempAmbMB;
short H2O_Temp;
unsigned char H2O_TSet;
unsigned char H2O_Flow;
unsigned char TSetOnPompa;
unsigned char H2O_TSetSan;
unsigned char TipoStufa;
unsigned char StatoAllBlack;
short RicPellet;	// ricetta pellet in potenza
short RicEsp;	// ricetta espulsore in potenza
unsigned char GradienteECO;
unsigned short TSwitchOnECO;
unsigned short TShutDownECO;
unsigned short TempFumi;
unsigned short VelocitaEspulsore;
unsigned short PortataAria;
unsigned long OreFunz;
unsigned short OreSATLimite;
unsigned short OreSAT;
short RicPelletAcc;	// ricetta pellet in accensione
short RicEspAcc;	// ricetta espulsore in accensione
unsigned char pByPassLettera;
unsigned char pByPassCodice;

unsigned char NumLanguage;
unsigned char iLanguage;
unsigned char LinguaDef;

unsigned short TPreAcc1;
unsigned short TPreAcc2;
unsigned short TPreAccCaldo;
unsigned short TWarmUp;
unsigned short TFireOn;
unsigned short TempPreAlarmFumi;
unsigned short TempAlarmFumi;
unsigned short TempOnFumi;
unsigned short TempOffFumi;
unsigned short TempSogliaFumi;
unsigned char GradienteAlmFumi;
unsigned short TempOnScamb;
unsigned short TempSogliaScamb;
unsigned char TipoMotoreEsp;
unsigned char TipoMotoreEsp2;
unsigned char ScuotNCicli;			
unsigned short ScuotDurataCiclo;	
unsigned short TWaitScuot;	
unsigned char PotMaxCoclea;
unsigned short PotMaxGiri;
unsigned short PotMaxPortata;
unsigned short TPreAlmPorta;
unsigned short TAlarmPorta;
unsigned char DeltaSanitari;
unsigned char IstTempAcqua;
short GainRisc;					
short GainSanitari;			
unsigned short TPreAlmAriaCombust;
unsigned char PressH2OMax;
unsigned char SetIdroPompaOff;
unsigned short TWaitPB;
unsigned short TPB;
unsigned char MinPowerPB;


unsigned short SetpSanitari;
TBitWord InpBMap21;
TBitWord InpBMap22;
TBitWord InpBMap23;
TBitWord InpBMap24;
TBitWord InpBMap25;
unsigned char ContaDurata1;
unsigned short ContaDurata2;
unsigned short ContaDurataEco;
unsigned char ContaPID;		
unsigned char PotCalc;		
unsigned char PotIdro;		
unsigned char PotAttuata;	
unsigned short TOnFan1Set;
unsigned short TOffFan1Set;
unsigned short TOnCocleaSet;
unsigned short TOffCocleaSet;
unsigned short PortataAriaSet;
unsigned short GiriMinEspSet;
unsigned short AttEsp;
unsigned short Fan1Set;
unsigned short Fan2Set;
unsigned short Fan3Set;
unsigned short LivFotores;
unsigned short Giri2Set;
unsigned short VelocitaEspulsore2;
unsigned char DriverCod;
unsigned char DriverVers;
unsigned char DriverRev;
unsigned char DriverBuild;
unsigned char SecOrologio;
unsigned char MinOrologio;
unsigned short TempoOn;
TBitWord InpBMap115;


TBitWord HoldBMap6;
TBitWord HoldBMap23;
unsigned char TOnAux;

TBitWord PreAcc1;
unsigned short PreAcc1_TOnCoclea;
unsigned short PreAcc1_TOffCoclea;
unsigned short PreAcc1_Portata;
unsigned short PreAcc1_Esp;
unsigned short PreAcc1_TOnFan1;
unsigned short PreAcc1_TOffFan1;
unsigned short PreAcc1_AttuaFan1;
unsigned short PreAcc1_AttuaFan2;
unsigned short PreAcc1_AttuaFan3;
unsigned short PreAcc1_Giri2;

TBitWord PreAcc2;
unsigned short PreAcc2_TOnCoclea;
unsigned short PreAcc2_TOffCoclea;
unsigned short PreAcc2_Portata;
unsigned short PreAcc2_Esp;
unsigned short PreAcc2_TOnFan1;
unsigned short PreAcc2_TOffFan1;
unsigned short PreAcc2_AttuaFan1;
unsigned short PreAcc2_AttuaFan2;
unsigned short PreAcc2_AttuaFan3;
unsigned short PreAcc2_Giri2;

TBitWord PreAccCaldo;
unsigned short PreAccCaldo_TOnCoclea;
unsigned short PreAccCaldo_TOffCoclea;
unsigned short PreAccCaldo_Portata;
unsigned short PreAccCaldo_Esp;
unsigned short PreAccCaldo_TOnFan1;
unsigned short PreAccCaldo_TOffFan1;
unsigned short PreAccCaldo_AttuaFan1;
unsigned short PreAccCaldo_AttuaFan2;
unsigned short PreAccCaldo_AttuaFan3;
unsigned short PreAccCaldo_Giri2;

TBitWord AccA;
unsigned short AccA_TOnCoclea;
unsigned short AccA_TOffCoclea;
unsigned short AccA_Portata;
unsigned short AccA_Esp;
unsigned short AccA_TOnFan1;
unsigned short AccA_TOffFan1;
unsigned short AccA_AttuaFan1;
unsigned short AccA_AttuaFan2;
unsigned short AccA_AttuaFan3;
unsigned short AccA_Giri2;

TBitWord AccB;
unsigned short AccB_TOnCoclea;
unsigned short AccB_TOffCoclea;
unsigned short AccB_Portata;
unsigned short AccB_Esp;
unsigned short AccB_TOnFan1;
unsigned short AccB_TOffFan1;
unsigned short AccB_AttuaFan1;
unsigned short AccB_AttuaFan2;
unsigned short AccB_AttuaFan3;
unsigned short AccB_Giri2;

TBitWord FireOnA;
unsigned short FireOnA_TOnCoclea;
unsigned short FireOnA_TOffCoclea;
unsigned short FireOnA_Portata;
unsigned short FireOnA_Esp;
unsigned short FireOnA_TOnFan1;
unsigned short FireOnA_TOffFan1;
unsigned short FireOnA_AttuaFan1;
unsigned short FireOnA_AttuaFan2;
unsigned short FireOnA_AttuaFan3;
unsigned short FireOnA_Giri2;

TBitWord FireOnB;
unsigned short FireOnB_TOnCoclea;
unsigned short FireOnB_TOffCoclea;
unsigned short FireOnB_Portata;
unsigned short FireOnB_Esp;
unsigned short FireOnB_TOnFan1;
unsigned short FireOnB_TOffFan1;
unsigned short FireOnB_AttuaFan1;
unsigned short FireOnB_AttuaFan2;
unsigned short FireOnB_AttuaFan3;
unsigned short FireOnB_Giri2;

TBitWord AccC;
unsigned short AccC_TOnCoclea;
unsigned short AccC_TOffCoclea;
unsigned short AccC_Portata;
unsigned short AccC_Esp;
unsigned short AccC_TOnFan1;
unsigned short AccC_TOffFan1;
unsigned short AccC_AttuaFan1;
unsigned short AccC_AttuaFan2;
unsigned short AccC_AttuaFan3;
unsigned short AccC_Giri2;

TBitWord Spenta;
unsigned short Spenta_TOnCoclea;
unsigned short Spenta_TOffCoclea;
unsigned short Spenta_Portata;
unsigned short Spenta_Esp;
unsigned short Spenta_TOnFan1;
unsigned short Spenta_TOffFan1;
unsigned short Spenta_AttuaFan1;
unsigned short Spenta_AttuaFan2;
unsigned short Spenta_AttuaFan3;
unsigned short Spenta_Giri2;

TBitWord SpeA;
unsigned short SpeA_TOnCoclea;
unsigned short SpeA_TOffCoclea;
unsigned short SpeA_Portata;
unsigned short SpeA_Esp;
unsigned short SpeA_TOnFan1;
unsigned short SpeA_TOffFan1;
unsigned short SpeA_AttuaFan1;
unsigned short SpeA_AttuaFan2;
unsigned short SpeA_AttuaFan3;
unsigned short SpeA_Giri2;

TBitWord SpeB;
unsigned short SpeB_TOnCoclea;
unsigned short SpeB_TOffCoclea;
unsigned short SpeB_Portata;
unsigned short SpeB_Esp;
unsigned short SpeB_TOnFan1;
unsigned short SpeB_TOffFan1;
unsigned short SpeB_AttuaFan1;
unsigned short SpeB_AttuaFan2;
unsigned short SpeB_AttuaFan3;
unsigned short SpeB_Giri2;

TBitWord RaffA;
unsigned short RaffA_TOnCoclea;
unsigned short RaffA_TOffCoclea;
unsigned short RaffA_Portata;
unsigned short RaffA_Esp;
unsigned short RaffA_TOnFan1;
unsigned short RaffA_TOffFan1;
unsigned short RaffA_AttuaFan1;
unsigned short RaffA_AttuaFan2;
unsigned short RaffA_AttuaFan3;
unsigned short RaffA_Giri2;

TBitWord RaffB;
unsigned short RaffB_TOnCoclea;
unsigned short RaffB_TOffCoclea;
unsigned short RaffB_Portata;
unsigned short RaffB_Esp;
unsigned short RaffB_TOnFan1;
unsigned short RaffB_TOffFan1;
unsigned short RaffB_AttuaFan1;
unsigned short RaffB_AttuaFan2;
unsigned short RaffB_AttuaFan3;
unsigned short RaffB_Giri2;

TBitWord SpeC;
unsigned short SpeC_TOnCoclea;
unsigned short SpeC_TOffCoclea;
unsigned short SpeC_Portata;
unsigned short SpeC_Esp;
unsigned short SpeC_TOnFan1;
unsigned short SpeC_TOffFan1;
unsigned short SpeC_AttuaFan1;
unsigned short SpeC_AttuaFan2;
unsigned short SpeC_AttuaFan3;
unsigned short SpeC_Giri2;

unsigned char DeltaTempCaldo;
unsigned short TAccB;
unsigned short TAccC;
unsigned short TFireOnB;
unsigned short TSpeA;
unsigned short TSpeB;
unsigned short TSpeC;
unsigned short TRaffA;
unsigned short TRaffB;
unsigned short PeriodoCoclea;
unsigned short PeriodoFan1;

TBitWord Pulizia;
unsigned short Pulizia_TOnCoclea;
unsigned short Pulizia_TOffCoclea;
unsigned short Pulizia_Portata;
unsigned short Pulizia_Esp;
unsigned short Pulizia_TOnFan1;
unsigned short Pulizia_TOffFan1;
unsigned short Pulizia_AttuaFan1;
unsigned short Pulizia_AttuaFan2;
unsigned short Pulizia_AttuaFan3;
unsigned short Pulizia_Giri2;

TBitWord Pot1;
unsigned short Pot1_TOnCoclea;
unsigned short Pot1_TOffCoclea;
unsigned short Pot1_Portata;
unsigned short Pot1_Esp;
unsigned short Pot1_TOnFan1;
unsigned short Pot1_TOffFan1;
unsigned short Pot1_AttuaFan1;
unsigned short Pot1_AttuaFan2;
unsigned short Pot1_AttuaFan3;
unsigned short Pot1_Giri2;

TBitWord Pot2;
unsigned short Pot2_TOnCoclea;
unsigned short Pot2_TOffCoclea;
unsigned short Pot2_Portata;
unsigned short Pot2_Esp;
unsigned short Pot2_TOnFan1;
unsigned short Pot2_TOffFan1;
unsigned short Pot2_AttuaFan1;
unsigned short Pot2_AttuaFan2;
unsigned short Pot2_AttuaFan3;
unsigned short Pot2_Giri2;

TBitWord Pot3;
unsigned short Pot3_TOnCoclea;
unsigned short Pot3_TOffCoclea;
unsigned short Pot3_Portata;
unsigned short Pot3_Esp;
unsigned short Pot3_TOnFan1;
unsigned short Pot3_TOffFan1;
unsigned short Pot3_AttuaFan1;
unsigned short Pot3_AttuaFan2;
unsigned short Pot3_AttuaFan3;
unsigned short Pot3_Giri2;

TBitWord Pot4;
unsigned short Pot4_TOnCoclea;
unsigned short Pot4_TOffCoclea;
unsigned short Pot4_Portata;
unsigned short Pot4_Esp;
unsigned short Pot4_TOnFan1;
unsigned short Pot4_TOffFan1;
unsigned short Pot4_AttuaFan1;
unsigned short Pot4_AttuaFan2;
unsigned short Pot4_AttuaFan3;
unsigned short Pot4_Giri2;

TBitWord Pot5;
unsigned short Pot5_TOnCoclea;
unsigned short Pot5_TOffCoclea;
unsigned short Pot5_Portata;
unsigned short Pot5_Esp;
unsigned short Pot5_TOnFan1;
unsigned short Pot5_TOffFan1;
unsigned short Pot5_AttuaFan1;
unsigned short Pot5_AttuaFan2;
unsigned short Pot5_AttuaFan3;
unsigned short Pot5_Giri2;

TBitWord Pot6;
unsigned short Pot6_TOnCoclea;
unsigned short Pot6_TOffCoclea;
unsigned short Pot6_Portata;
unsigned short Pot6_Esp;
unsigned short Pot6_TOnFan1;
unsigned short Pot6_TOffFan1;
unsigned short Pot6_AttuaFan1;
unsigned short Pot6_AttuaFan2;
unsigned short Pot6_AttuaFan3;
unsigned short Pot6_Giri2;

TBitWord Pot7;
unsigned short Pot7_TOnCoclea;
unsigned short Pot7_TOffCoclea;
unsigned short Pot7_Portata;
unsigned short Pot7_Esp;
unsigned short Pot7_TOnFan1;
unsigned short Pot7_TOffFan1;
unsigned short Pot7_AttuaFan1;
unsigned short Pot7_AttuaFan2;
unsigned short Pot7_AttuaFan3;
unsigned short Pot7_Giri2;

TBitWord Pot8;
unsigned short Pot8_TOnCoclea;
unsigned short Pot8_TOffCoclea;
unsigned short Pot8_Portata;
unsigned short Pot8_Esp;
unsigned short Pot8_TOnFan1;
unsigned short Pot8_TOffFan1;
unsigned short Pot8_AttuaFan1;
unsigned short Pot8_AttuaFan2;
unsigned short Pot8_AttuaFan3;
unsigned short Pot8_Giri2;

TBitWord PuliziaB;
unsigned short PuliziaB_TOnCoclea;
unsigned short PuliziaB_TOffCoclea;
unsigned short PuliziaB_Portata;
unsigned short PuliziaB_Esp;
unsigned short PuliziaB_TOnFan1;
unsigned short PuliziaB_TOffFan1;
unsigned short PuliziaB_AttuaFan1;
unsigned short PuliziaB_AttuaFan2;
unsigned short PuliziaB_AttuaFan3;
unsigned short PuliziaB_Giri2;

unsigned short EspRitardoInc;
unsigned short EspRitardoDec;
unsigned short PotRitardoInc;
unsigned short PotRitardoDec;
unsigned short TPBB;

TBitWord HoldBMap286;
unsigned short DurataMaxPreAllFumi;
unsigned short DurataPreAllPellet;
unsigned char DeltaTAssenzaFiamma;
unsigned short PortataCritica;
unsigned char DeltaPortata;
TBitWord HoldBMap305;
TBitWord HoldBMap306;
unsigned short DurataBlkoutRecovery;
unsigned char TipoFrenataCoclea;
unsigned char TipoFrenataFan1;


unsigned char IdRF;
TBitWord HoldBMap8194;
unsigned short PortaIP;
unsigned char Ip[IP_SIZE];
unsigned char Subnet[IP_SIZE];
unsigned char Gateway[IP_SIZE];
unsigned char Dns[IP_SIZE];
char Ssid[SSID_SIZE+1];
char CryptoKey[KEYW_SIZE+1];
char WPSPin[WPS_PIN_SIZE+1];


unsigned char Ossigeno;
unsigned char OssigenoResiduo;

unsigned short Consumo;
unsigned short PesataPellet;

unsigned short ParamCode; // codice/versione parametri (cod cliente)
unsigned short ParamRel; // rev/build parametri (report modifica)

unsigned short FotoresThresOn;
unsigned short FotoresThresOff;

unsigned char ConfigIdro;
unsigned char MinSetFan1;
unsigned char MinSetFan2;
unsigned char MinSetFan3;
unsigned char PreAcc1NumExec;
unsigned char PreAcc2NumExec;
unsigned char K_Sonda;

unsigned short NumAcc;
TBitWord HoldBMap8315;
TBitWord HoldBMap8316;
short TempP0;
short TempP1;
short TempP2;
short TempP3;
short TempP4;

char Local_ISO3166_1_Code[2+1];	// codice ISO3166-1 alpha 2 del Paese locale
unsigned char OTdelay;







