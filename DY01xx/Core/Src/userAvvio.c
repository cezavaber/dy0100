/*!
** \file userAvvio.c
** \author Alessandro Vagniluca
** \date 27/11/2015
** \brief User Interface USER_STATE_AVVIO and USER_STATE_AVVIO_ALLARME states handler
** 
** \version 1.0
**/

#include "user.h"
#include "User_Parametri.h"
#include "userMenu.h"
#include "userAvvio.h"
#include "userCrono.h"
#include "DecodInput.h"
#include "userInit.h"
#include "userResources.h"



// gruppi di 4 comandi di AVVIO
enum
{
	AVCMD_FIRE_TEMP_TERM,
	AVCMD_FAN,
	AVCMD_MODE_TEMP_TERM,
	AVCMD_H2O,
	NAVCMD1
};
enum
{
	AVCMD_SUMMER_WINTER,
	AVCMD_ESM,
	AVCMD_6,
	AVCMD_7,
	NAVCMD2
};
#define NAVCMD		NAVCMD1
enum
{
	AVCMDGRP_0,
	AVCMDGRP_1,
	NAVCMDGRP
};

/*!
** \enum TouchTags
** \brief The used touch tags values
**/
enum TouchTags
{
	TAG_HORIZ_SCROLL = 0,
	TAG_CHRONO = 1,
	TAG_SLEEP,
	TAG_DATE,
	TAG_TIME,
	TAG_ON_OFF_PLAY,
	TAG_MODEM,
	TAG_WARNING,
	
	// AVCMDGRP_0
	TAG_FIRE_TSETP,
	TAG_FAN,
	TAG_MODE_TSETP,
	TAG_IDRO_TEMP,
	
	// AVCMDGRP_1
	TAG_SUMMER_WINTER,
	TAG_ESM,
	TAG_CMDGRP_1_2,
	TAG_CMDGRP_1_3,
	
	TAG_MENU,
	TAG_INFO,
	
	TAG_CMD_HORIZ_SCROLL,	// per barra comandi a pagine scorrevoli orizzontalmente
	
	TAG_UP,
	TAG_DOWN,
	
	TAG_MUTE,
	TAG_STOVE_TYPE,
	
	TAG_RESTART,
	
	NUM_TAG
};

static const unsigned char AvGrpTags[NAVCMDGRP][NAVCMD] =
{
	// AVCMDGRP_0
	{
		TAG_FIRE_TSETP,
		TAG_FAN,
		TAG_MODE_TSETP,
		TAG_IDRO_TEMP,
	},
	// AVCMDGRP_1
	{
		TAG_SUMMER_WINTER,
		TAG_ESM,
		TAG_CMDGRP_1_2,
		TAG_CMDGRP_1_3,
	},
	
};

static unsigned short userCounterTempOra;	//!< room/water temperature viewing counter

/*!
** \var bmWinMain
** \brief Vettore degli indici delle bitmap visualizzate sulla Main Window
** 
** CARICAMENTO DELLE BITMAP PER LA MAIN WINDOW
** 
** Dalla main window vengono attivate tutte le altre schermate, che possono
** a loro volta dover caricare le proprie bitmap nella GRAM.
** Si gestisce ogni bitmap caricata come elemento di una lista linkata.
** Ogni elemento della lista � costituito da una struttura con i seguenti
** membri:
** 	a) puntatore alla struttura delle propriet� della bitmap 
** 	b) indirizzo nella GRAM
** 	c) handle della bitmap caricata
** All'ingresso in ogni schermata si caricano nella GRAM le bitmap che 
** servono in un certo ordine, registrandole nella lista linkata. Si usano 
** le bitmap caricate attraverso gli handle loro assegnati (con l'handle 
** si ottiene dalla lista linkata i suddetti parametri a) e b). All'uscita 
** dalla schermata si scaricano le bitmap dalla lista linkata nell'ordine 
** inverso. La schermata di ritorno non deve cos� ricaricare le proprie 
** bitmap nella GRAM.
** VANTAGGIO: si ottimizzano i tempi di esecuzione del codice.
** SVANTAGGIO: la finestra chiamata per ultima ha a disposizione la quantit�
** minore di GRAM per le sue bitmap (essendo occupata dalle bitmap usate 
** nelle finestre a livello superiore).
** POSSIBILE PROBLEMA: Le bitmap della main window sono numerose e verrebbero 
** caricate solo all'avvio e quindi aumenta la possibilit� che si possano 
** corrempere per qualche motivo.
**/
#define NUM_BM_WINMAIN		25
static const unsigned short bmWinMain[NUM_BM_WINMAIN] =
{
	BITMAP_CHRONO,
	BITMAP_SLEEP,
	BITMAP_TAP,
	BITMAP_MODEM,
	BITMAP_OFF_BUTTON,
	BITMAP_ON_BUTTON,
	BITMAP_PLAY,
	BITMAP_WARNING,
	BITMAP_DEG_C,
	BITMAP_DEG_F,
	BITMAP_ROOM_TEMP,
	BITMAP_WATER_TEMP,
	BITMAP_FLAMES,
	BITMAP_TSETP,
	BITMAP_FAN,
	BITMAP_MAN,
	BITMAP_AUTO,
	BITMAP_WATERTEMP,
	BITMAP_SETTINGS,
	BITMAP_INFO,
	BITMAP_SUMMER,
	BITMAP_WINTER,
	BITMAP_THERM,
	BITMAP_CHARGE,
	BITMAP_BATTERY,
};

/*!
** \var sStatoStufa_tab
** \brief Vettore degli indici alle stringhe di stato stufa
**/
static const WORD sStatoStufa_tab[NUMSTUFASTAT] =
{
	STUFA_RESET,				// STUFASTAT_IN_RESET,
	STUFA_SPENTA,           // STUFASTAT_SPENTA,
	STUFA_ACCENSIONE,       // STUFASTAT_IN_ACCENSIONE,
	STUFA_ACCESA,           // STUFASTAT_ACCESA,
	STUFA_SPEGNIMENTO,      // STUFASTAT_IN_SPEGNIMENTO,
	STUFA_ALLARME,          // STUFASTAT_ALLARME,
	STUFA_ACCRETE,          // STUFASTAT_IN_ACCENSIONE_RETE,
	STUFA_COLLAUDO,         // STUFASTAT_IN_COLLAUDO,
	//STUFA_SPERETE,          // STUFASTAT_IN_PULIZIA_RETE,
	STUFA_NONDEF            // STUFASTAT_NON_DEFINITA,
};


// schermate ESM
enum
{
	ESM_0,
	ESM_1,
	NUM_ESM
};

// tabella per visualizzazioni temperature ESM
enum
{
	TEMP_P0,			// J33 6-8
	TEMP_P1,			// IN1
	TEMP_P2,			// IN2
	TEMP_P3,			// IN3
	TEMP_P4,			// IN4
	NUM_TEMP_Px
};
static const PCSTR ESM_Px_Desc[NUM_TEMP_Px] =
{
	"J33 6-8: ",	// J33 6-8
	"IN1: ",			// IN1
	"IN2: ",			// IN2
	"IN3: ",			// IN3
	"IN4: ",			// IN4
};
typedef struct
{
	unsigned short iPar;		//!< indice parametro Px
	ft_int16_t x;				//!< coordinata X visualizzazione
	ft_int16_t y;				//!< coordinata Y visualizzazione
	ft_uint16_t options;		//!< opzioni visualizzazione testo
} ESM_Px_STRUCT;
static const ESM_Px_STRUCT ESM_Px_tab[NUM_TEMP_Px] =
{
	{ P_TEMP_P0, 240, 57, OPT_CENTER },			// J33 6-8
	{ P_TEMP_P1, 60, 111, OPT_CENTERY },		// IN1
	{ P_TEMP_P2, 300, 111, OPT_CENTERY },		// IN2
	{ P_TEMP_P3, 60, 165, OPT_CENTERY },		// IN3
	{ P_TEMP_P4, 300, 165, OPT_CENTERY },		// IN4
};

// tabella per visualizzazioni termostati ESM
enum
{
	TERM_0,			// THERM
	TERM_1,			// J33 4-7
	NUM_TERMx
};
static const PCSTR ESM_TERMx_Desc[NUM_TERMx] =
{
	"THERM: ",			// THERM
	"J33 4-7: ",		// J33 4-7
};
static const ESM_Px_STRUCT ESM_TERMx_tab[NUM_TERMx] =
{
	{ P_TERM1, 60, 219, OPT_CENTERY },			// THERM
	{ P_TERM2, 300, 219, OPT_CENTERY },			// J33 4-7
};

// tabella per visualizzazioni uscite ESM
enum
{
	OUTP_0,			// PUMP
	OUTP_1,			// 3-WAY
	OUTP_2,			// AUX1
	OUTP_3,			// AUX2
	OUTP_4,			// 3-WAY2
	NUM_OUTPx
};
static const PCSTR ESM_OUTPx_Desc[NUM_OUTPx] =
{
	"PUMP: ",			// PUMP
	"3-WAY: ",			// 3-WAY
	"AUX1: ",			// AUX1
	"AUX2: ",			// AUX2
	"3-WAY2: ",			// 3-WAY2
};
static const ESM_Px_STRUCT ESM_OUTPx_tab[NUM_OUTPx] =
{
	{ P_REL0, 60, 57, OPT_CENTERY },			// PUMP
	{ P_REL1, 60, 111, OPT_CENTERY },		// 3-WAY
	{ P_REL2, 300, 111, OPT_CENTERY },		// AUX1
	{ P_REL3, 300, 165, OPT_CENTERY },		// AUX2
	{ P_REL4, 60, 165, OPT_CENTERY },		// 3-WAY2
};



static void userVisSetpTemp( char *s );
static void userVisTemp( char *s );
static void ExecUnlock( void );
static short WinAlarm( void );
static short WinSetIdro( char *sTitle );
static short WinSetFan( char *sTitle );
static short Check4CmdGroup( unsigned char iAvCmdGrp );
static unsigned char userVisStandby( void );
static void WinESM( char *sTitle );



/*! 
	\fn void userVisSetpTemp( char *s )
	\brief Visualizzazione setpoint temperatura
	\param s Stringa destinazione valore setpoint temperatura
	\return None
*/
static void userVisSetpTemp( char *s )
{
	WORD w;
	BYTE lByApp = TEMP_PAN;

	// sicuramente 0 <= TEMP_PAN <= +50 �C
	if( TEMPFAHRENHEIT )
	{
		// Converto la temperatura in Fahrenheit
		w = (WORD)lByApp * 18;	// in decimi di grado
		w += 320;
		w += 5;	// arrotondamento
		w /= 10;	// in gradi
		lByApp = w;
		s[0] = cBLANK;
		s[1] = Hex2Ascii( lByApp / 10 );
		if( s[1] == '0' )
			s[1] = cBLANK;
		s[2] = Hex2Ascii( lByApp % 10 );
		s[3] = cDEG;
		s[4] = 'F';
		s[5] = '\0';
	}
	else
	{
		s[0] = cBLANK;
		s[1] = Hex2Ascii( lByApp / 10 );
		if( s[1] == '0' )
			s[1] = cBLANK;
		s[2] = Hex2Ascii( lByApp % 10 );
		s[3] = cDEG;
		s[4] = 'C';
		s[5] = '\0';
	}
}

/*! 
	\fn void userVisTemp( char *s )
	\brief Visualizzazione temperatura ambiente o idro
	\param s Stringa destinazione valore temperatura
	\return None
*/
static void userVisTemp( char *s )
{
	//BYTE dec;
	WORD w;
	short iTemp;
	DWORD d;

	// Cancella i valori su s.
	memset( s, 0, DISPLAY_MAX_COL );
	// campo di visualizzazione: 7 caratteri "+##.#�C" / "+ ###�F"
	memset( s, cBLANK, 7 );

	if(lByTogVisTH2O)
	{
		// Temperatura ambiente misurata tra -30 e 80 �C.
		iTemp = TEMP_AMB_MB;	// il valore e' espresso in step 0.5�C
		if( (iTemp < -60) || (iTemp > 160) )
		{
			/* La misura della scheda madre non e' valida.
				Prendo la misura locale. */
#if defined( TEMP_AMB_PAN_0_1_DEG )		
			// converto in step di 0.5�C con arrotondamento
			if( TempAmb >= 0 )
				iTemp = (TempAmb + 25)/50;
			else
				iTemp = (TempAmb - 25)/50;
#elif defined( TEMP_AMB_PAN_0_5_DEG )
			iTemp = TEMP_AMB_PAN;
#else
			#error Risoluzione TEMP_AMB_PAN non definita
#endif
		}
		//dec = iTemp & 0x01;	// unico bit decimale

		if( (iTemp < -60) || (iTemp > 160) )
		{
			// Valore di errore.
			s[1] = '-';
			s[2] = '-';
			s[3] = '-';
			s[4] = '-';
		}
		else if( TEMPFAHRENHEIT )
		{
			// valore in gradi Fahrenheit
			if( iTemp > 120 )
			{
				// Valore superiore ai 60�C.
				s[2] = 'H';
				s[3] = 'H';
				s[4] = 'H';
				s[5] = cDEG;
				s[6] = 'F';
			}
			else if( iTemp < -40 )
			{
				// Valore inferiore ai -20�C.
				s[2] = 'L';
				s[3] = 'L';
				s[4] = 'L';
				s[5] = cDEG;
				s[6] = 'F';
			}
			else
			{
				iTemp *= 5;	// converto in 0.1�C
				if( iTemp >= 0 )
				{
					w = iTemp;
				}
				else
				{
					w = (-iTemp);
					s[0] = '-';
				}
				// Converto la temperatura in Fahrenheit
				d = (DWORD)w * 18;	// in centesimi di grado
				d += 3200;
				// arrotondamento
				if( s[0] == cBLANK )
					d += 50;
				else
					d -= 50;
				d /= 100;	// in gradi
				w = (WORD)d;
				s[2] = Hex2Ascii( w / 100 );
				if( s[2] == '0' )
					s[2] = cBLANK;
				w %= 100;
				s[3] = Hex2Ascii( w / 10 );
				if( (s[2] == cBLANK) && (s[3] == '0') )
					s[3] = cBLANK;
				s[4] = Hex2Ascii( w % 10 );
				s[5] = cDEG;
				s[6] = 'F';
			}
		}
		else
		{
			// valore in gradi Celsius
			if( iTemp > 120 )
			{
				// Valore superiore ai 60�C.
				s[1] = 'H';
				s[2] = 'H';
				s[3] = '.';
				s[4] = 'H';
				s[5] = cDEG;
				s[6] = 'C';
			}
			else if( iTemp < -40 )
			{
				// Valore inferiore ai -20�C.
				s[1] = 'L';
				s[2] = 'L';
				s[3] = '.';
				s[4] = 'L';
				s[5] = cDEG;
				s[6] = 'C';
			}
			else
			{
				// valore assoluto
				iTemp *= 5;	// converto in 0.1�C
				if( iTemp >= 0 )
				{
					w = iTemp;
				}
				else
				{
					w = (-iTemp);
					s[0] = '-';
				}
				s[1] = Hex2Ascii( w/100 );
				if( s[1] == '0' )
					s[1] = cBLANK;
				w %= 100;
				s[2] = Hex2Ascii( w/10 );
				s[3] = '.';
				s[4] = Hex2Ascii( w%10 );
				s[5] = cDEG;
				s[6] = 'C';
			}
		}
	}
	else
	{
		// Temperatura acqua misurata tra 0 e 100 �C.
		iTemp = H2O_TEMP;	// il valore e' espresso in step 0.5�C
		if( TEMPFAHRENHEIT )
		{
			// valore in gradi Fahrenheit
			if( iTemp >= 200 )
			{
				// Valore superiore ai 100�C.
				s[2] = 'H';
				s[3] = 'H';
				s[4] = 'H';
				s[5] = cDEG;
				s[6] = 'F';
			}
			else if( iTemp < 0 )
			{
				// Valore inferiore a 0�C.
				s[2] = 'L';
				s[3] = 'L';
				s[4] = 'L';
				s[5] = cDEG;
				s[6] = 'F';
			}
			else
			{
				// Converto la temperatura in Fahrenheit
				iTemp *= 5;	// converto in 0.1�C
				if( iTemp >= 0 )
				{
					w = iTemp;
				}
				else
				{
					w = (-iTemp);
					s[0] = '-';
				}
				// Converto la temperatura in Fahrenheit
				d = (DWORD)w * 18;	// in centesimi di grado
				d += 3200;
				// arrotondamento
				if( s[0] == cBLANK )
					d += 50;
				else
					d -= 50;
				d /= 100;	// in gradi
				w = (WORD)d;
				s[2] = Hex2Ascii( w / 100 );
				if( s[2] == '0' )
					s[2] = cBLANK;
				w %= 100;
				s[3] = Hex2Ascii( w / 10 );
				if( (s[2] == cBLANK) && (s[3] == '0') )
					s[3] = cBLANK;
				s[4] = Hex2Ascii( w % 10 );
				s[5] = cDEG;
				s[6] = 'F';
			}
		}
		else
		{
			// valore in gradi Celsius
			if( iTemp >= 200 )
			{
				// Valore superiore ai 100�C.
				s[1] = 'H';
				s[2] = 'H';
				s[3] = '.';
				s[4] = 'H';
				s[5] = cDEG;
				s[6] = 'C';
			}
			else if( iTemp < 0 )
			{
				// Valore inferiore a 0�C.
				s[1] = 'L';
				s[2] = 'L';
				s[3] = '.';
				s[4] = 'L';
				s[5] = cDEG;
				s[6] = 'C';
			}
			else
			{
				// valore assoluto
				iTemp *= 5;	// converto in 0.1�C
				if( iTemp >= 0 )
				{
					w = iTemp;
				}
				else
				{
					w = (-iTemp);
					s[0] = '-';
				}
				s[1] = Hex2Ascii( w/100 );
				if( s[1] == '0' )
					s[1] = cBLANK;
				w %= 100;
				s[2] = Hex2Ascii( w/10 );
				s[3] = '.';
				s[4] = Hex2Ascii( w%10 );
				s[5] = cDEG;
				s[6] = 'C';
			}
		}
	}
	if( fontMax == FONT_MAX_DEF )
	{
		// no deg character
		s[5] = '\0';
	}
}

/*! 
** \fn void ExecUnlock( void )
** \brief Unlock processing
** \return None
**/
static void ExecUnlock( void )
{
	if( SBLOCCO_PAN )
	{
		SBLOCCO_PAN = 0;
		// riabilita suono allarme
		userParValue.b[0] = 0;
		userSetPar( P_ALARM_SOUND );

		// Uscendo dall'allarme attivo il buzzer.
		//led_Set( BUZ, LEDSTAT_ON );	no segnalazioni acustiche
		starttimer( T_BUZZER, 2*T1SEC );
		// comando il suono sulla stufa
		OT_ReqSound( BUZ_1LONG );
	}
}

/*!
** \fn unsigned char userVisAvvio( void )
** \brief Starts the USER_STATE_AVVIO and USER_STATE_AVVIO_ALLARME
** 		User Interface states handling
** \return The next User Interface state 
**/
unsigned char userVisAvvio( void )
{
	OpeMode = MODE_FULL_POWER;
	
	STATO_ALL_BLACK = PAN_ONLINE;

	/* reset MENU windows resources */
	LivAccesso = LOGIN_FREE;	// reset login
	iLivMenu = LIVMNENU_0;
	// pop bitmap UNCHECKED from Graphic RAM
	res_bm_Release( BITMAP_UNCHECKED );
	m_bmhdrUNCHECKED = NULL;
	// pop bitmap CHECKED from Graphic RAM
	res_bm_Release( BITMAP_CHECKED );
	m_bmhdrCHECKED = NULL;
	m_bmhdrQUIT = NULL;
	Flag.popup = 0;

	// reset gestione comando FAN da telecomando IR
	// TargetFAN = TARGET_FAN_1;
	// CommutaFan = 0;

	// comandi di AVVIO
	iMenu1Pag = AVCMDGRP_0;
	
	// predispone timer per gestione visualizzazione temp ambiente/temp idro
	userCounterTempOra = TIMEOUT_AVVIO_2SEC;

	userSubState = 0;
	userCounterTime = TIMEOUT_AVVIO_8SEC;
	userNextPointer = NULL;

	// avvio richieste cicliche di stato
	OT_Send_SetReq( SEND_REQ_SET_STATUS );
	
	return USER_STATE_AVVIO;
}

/*!
** \fn unsigned char userVisStandby( void )
** \brief Starts the USER_STATE_STANDBY User Interface states handling
** \return The next User Interface state 
**/
static unsigned char userVisStandby( void )
{
	// clear screen (to change the current Display List while all the bitmaps are loaded)
	Ft_Gpu_CoCmd_Dlstart(pHost);
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
	Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
	Ft_Gpu_CoCmd_Swap( pHost );
	Ft_App_Flush_Co_Buffer( pHost );
	Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
	
	lByTogVisTH2O = 1;	// view only air temperature
	
	return USER_STATE_STANDBY;
}

/*!
** \fn void ReleaseAllBitmapAvvio( void )
** \brief Release all the bitmaps used in the USER_STATE_AVVIO User Interface state
** \return None
**/
void ReleaseAllBitmapAvvio( void )
{
	short i;
	
	// pop bitmaps from Graphic RAM
	for( i=NUM_BM_WINMAIN-1; i>=0; i-- )
	{
		Wdog();
		res_bm_Release( bmWinMain[i] );
	}
}

/*!
** \fn short LoadAllBitmapAvvio( void )
** \brief Load all the bitmaps used in the USER_STATE_AVVIO User Interface state
** \return 1: OK, 0: FAILED
**/
short LoadAllBitmapAvvio( void )
{
	short RetVal = 1;
	short i;

	// waiting screen while all the bitmaps are loaded
	Ft_Gpu_CoCmd_Dlstart(pHost);
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,0xFF,0));	// GREEN
	Ft_Gpu_CoCmd_Spinner(pHost, (FT_DispWidth/2),(FT_DispHeight/2),0,1);
	Ft_App_Flush_Co_Buffer( pHost );
	Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

	// push bitmaps into Graphic RAM
	for( i=0; i<NUM_BM_WINMAIN; i++ )
	{
		Wdog();
		//Ft_Gpu_Hal_Sleep( 2 );
		if( res_bm_Load( bmWinMain[i], res_bm_GetGRAM() ) == 0 )
		{
			RetVal = 0;
		#ifdef LOGGING
			myprintf( sBitmapLoadError, bmWinMain[i] );
		#endif
			break;
		}
	}

	// Send the stop command
	Ft_Gpu_Hal_WrCmd32(pHost, CMD_STOP);
	Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);

	return RetVal;
}

/*!
** \def VBATT_Y
** \brief Calculate the battery level Y coordinate
** \param ys Y start value (upper: 100% battery level)
** \param ye Y end value (lower: 0% battery level)
** \param vb battery level (0.1%)
**/
#define VBATT_Y( ys, ye, vb )	( (ye - ys)*(1000-vb)/1000 + ys )

/*!
** \fn unsigned char userAvvio( void )
** \brief Handler function for the USER_STATE_AVVIO and USER_STATE_AVVIO_ALLARME
** 		User Interface states
** \return The next User Interface state 
**/
unsigned char userAvvio( void )
{
	unsigned char RetVal = userCurrentState;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdr;
	unsigned char scrollON = 0;
	unsigned short preXscroll = 0;
	short Dx = 0;
	unsigned char scrollONcmd = 0;
	unsigned short preXscrollCmd = 0;
	short DxCmd = 0;
	char s[DISPLAY_MAX_COL];
	unsigned long key_cnt = 0;
	unsigned long timer;
	unsigned char loop = 1;
	short i;
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;
	unsigned short visPercVBatt;

	// full LCD backlight
	SetBacklight( LCD_BACKLIGHT_MAX );
	starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
	
	if( LoadAllBitmapAvvio() )
	{
		// all the bitmaps were successfully copied

		while( loop )
		{
			if( tagval == TOUCH_TAG_INIT )
			{
				// reset all tags
				CTP_TagDeregisterAll();
				scrollONcmd = 0;
				scrollON = 0;
				Flag.Key_Detect = 0;
				key_cnt = 0;
				// full LCD backlight
				SetBacklight( LCD_BACKLIGHT_MAX );
				starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
				OT_DL_Pausa = OT_DL_PAUSE;
				if( userCurrentState == USER_STATE_AVVIO_ALLARME )
				{
					// start timer for main window in USER_STATE_AVVIO_ALLARME
					userCounterTime = TIMEOUT_AVVIO_10SEC;
				}
			}
			
			if( !((tagval == TAG_HORIZ_SCROLL) && (scrollON == 1)) &&
				!((tagval == TAG_CMD_HORIZ_SCROLL) && (scrollONcmd == 1)) )
			{
				if( tagval == TOUCH_TAG_INIT )
				{
					// screen horizontal scroll tags
					TagRegister( TAG_HORIZ_SCROLL, 0, 0, FT_DispWidth, 192 );
					TagRegister( TAG_CMD_HORIZ_SCROLL, 0, 192, 303, 80 );
				}
				
				Ft_Gpu_CoCmd_Dlstart(pHost);
				Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
				Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
				if( userIsCrono() )
				{
					// draw CHRONO bitmap
					p_bmhdr = res_bm_GetHeader( BITMAP_CHRONO );
					if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_CHRONO, 1 ) )
					{
						// CHRONO bitmap tag
						TagRegister( TAG_CHRONO, 12, 4, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
					}
					if( (tagval == TAG_CHRONO) && (key_cnt >= VALID_TOUCHED_KEY) )
					{
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
					}
					// specify the starting address of the bitmap in graphics RAM
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
					// specify the bitmap format, linestride and height
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
					// set filtering, wrapping and on-screen size
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
					// start drawing bitmap
					Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
					if( Crono.CronoMode.BIT.Enab == CRONO_OFF )
					{
						// Crono is disabled
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(32));
					}
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 12*16, 4*16 ) );

					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(255));
				}
				else
				{
					// disable tag
					CTP_SetTag( TAG_CHRONO, 0 );
				}
				
				// draw SLEEP bitmap
				if( userIsSleep() && (userCurrentState != USER_STATE_AVVIO_ALLARME) )
				{
					// draw SLEEP bitmap
					p_bmhdr = res_bm_GetHeader( BITMAP_SLEEP );
					if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_SLEEP, 1 ) )
					{
						// SLEEP bitmap tag
						TagRegister( TAG_SLEEP, 76, 4, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
					}
					if( (tagval == TAG_SLEEP) && (key_cnt >= VALID_TOUCHED_KEY) )
					{
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
					}
					// specify the starting address of the bitmap in graphics RAM
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
					// specify the bitmap format, linestride and height
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
					// set filtering, wrapping and on-screen size
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
					// start drawing bitmap
					Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
					if( Sleep.Ore == SLEEP_OFF )
					{
						// Sleep is disabled
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(32));
					}
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 76*16, 4*16 ) );

					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(255));
				}
				else
				{
					// disable tag
					CTP_SetTag( TAG_SLEEP, 0 );
				}

				if( STATO5_TREVIE )
				{
					// draw TAP bitmap
					p_bmhdr = res_bm_GetHeader( BITMAP_TAP );
					// specify the starting address of the bitmap in graphics RAM
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
					// specify the bitmap format, linestride and height
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
					// set filtering, wrapping and on-screen size
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
					// start drawing bitmap
					Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 8*16, 64*16 ) );
				}
				
				if( unCMD_SHOW_MODEM || IsWiFi() )
				{
					// draw MODEM bitmap
					p_bmhdr = res_bm_GetHeader( BITMAP_MODEM );
					if( IsWiFi() )
					{
						if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_MODEM, 1 ) )
						{
							// MODEM bitmap tag
							TagRegister( TAG_MODEM, 50, 64, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
						}
						if( (tagval == TAG_MODEM) && (key_cnt >= VALID_TOUCHED_KEY) )
						{
							//Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0xFF,0x00));	// GREEN
						}
						else if( IsWiFiIPvalid() )
						{
							//Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0xFF,0x00));	// GREEN
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
						}
						else
						{
							//Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0x00,0x00));	// RED
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(207, 216, 220));	// GREY
						}
					}
					else
					{
						// disable tag
						CTP_SetTag( TAG_MODEM, 0 );
					}
					// specify the starting address of the bitmap in graphics RAM
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
					// specify the bitmap format, linestride and height
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
					// set filtering, wrapping and on-screen size
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
					// start drawing bitmap
					Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 50*16, 64*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				}
				else
				{
					// disable tag
					CTP_SetTag( TAG_MODEM, 0 );
				}
				
				// draw BATTERY bitmap
				p_bmhdr = res_bm_GetHeader( BITMAP_BATTERY );
				// specify the starting address of the bitmap in graphics RAM
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
				// specify the bitmap format, linestride and height
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
				// set filtering, wrapping and on-screen size
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
				// start drawing bitmap
				Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 92*16, 64*16 ) );
				
				// draw BATTERY level
				visPercVBatt = GetVisPercVBatt();
				if( DIGIN_ALIM /*&& (BattUp == BATTERY_CHARGE)*/ )	// GREY color for charging battery
				{
					// charge battery level
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(207, 216, 220));
				}
				else if( visPercVBatt <= 100 )	// RED color for PercVBatt <= 10%
				{
					// alarm battery level
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(200,0,0));
				}
				else if( visPercVBatt <= 200 )	// ORANGE color for 10% < PercVBatt <= 20%
				{
					// warning battery level
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(200,128,0));
				}
				else	// GREEN color for 20% < PercVBatt <= 100%
				{
					// info battery level
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,200,0));
				}
				Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(2 * 16) );
				Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (92+8)*16, (64+48)*16 ) );
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (92+31)*16, VBATT_Y( (64+9), (64+48), visPercVBatt )*16 ) );

				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
				// draw BATTERY charge state
				if( DIGIN_ALIM /*&& (BattUp == BATTERY_CHARGE)*/ )
				{
					// battery charging
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(255));
					Ft_App_WrCoCmd_Buffer(pHost, BLEND_FUNC( SRC_ALPHA, SRC_ALPHA ) );	// overlay on BATTERY bitmap 40x56
					// draw CHARGE bitmap
					p_bmhdr = res_bm_GetHeader( BITMAP_CHARGE );
					// specify the starting address of the bitmap in graphics RAM
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
					// specify the bitmap format, linestride and height
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
					// set filtering, wrapping and on-screen size
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
					// start drawing bitmap
					Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (92+(40-12)/2)*16, (64+(56-20)/2)*16 ) );
					
					Ft_App_WrCoCmd_Buffer(pHost, BLEND_FUNC( SRC_ALPHA, ONE_MINUS_SRC_ALPHA ) );	// default blend
				}
				
				if( userIsWarning() )
				{
					// draw WARNING bitmap
					p_bmhdr = res_bm_GetHeader( BITMAP_WARNING );
					if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_WARNING, 1 ) )
					{
						// WARNING bitmap tag
						TagRegister( TAG_WARNING, 8, 128, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
					}
					if( (tagval == TAG_WARNING) && (key_cnt >= VALID_TOUCHED_KEY) )
					{
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
					}
					// specify the starting address of the bitmap in graphics RAM
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
					// specify the bitmap format, linestride and height
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
					// set filtering, wrapping and on-screen size
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
					// start drawing bitmap
					Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 8*16, 128*16 ) );

					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				}
				else
				{
					// disable tag
					CTP_SetTag( TAG_WARNING, 0 );
				}
				
				if( userCurrentState == USER_STATE_AVVIO_ALLARME )
				{
					// draw PLAY bitmap
					p_bmhdr = res_bm_GetHeader( BITMAP_PLAY );
				}
				else if( LOGIC_ONOFF )
				{
					// draw ON_BUTTON bitmap
					p_bmhdr = res_bm_GetHeader( BITMAP_ON_BUTTON );
				}
				else
				{
					// draw OFF_BUTTON bitmap
					p_bmhdr = res_bm_GetHeader( BITMAP_OFF_BUTTON );
				}
				if( tagval == TOUCH_TAG_INIT )
				{
					// ON-OFF_BUTTON bitmap tag
					TagRegister( TAG_ON_OFF_PLAY, 408, 0, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
				}
				if( (tagval == TAG_ON_OFF_PLAY) && (key_cnt >= VALID_TOUCHED_KEY) )
				{
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
				}
				// specify the starting address of the bitmap in graphics RAM
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
				// specify the bitmap format, linestride and height
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
				// set filtering, wrapping and on-screen size
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
				// start drawing bitmap
				Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 408*16, 0*16 ) );

				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
				// draw AIR/WATER bitmap
				if( lByTogVisTH2O )
				{
					// draw AIR bitmap
					p_bmhdr = res_bm_GetHeader( BITMAP_ROOM_TEMP );
				}
				else
				{
					// draw WATER bitmap
					p_bmhdr = res_bm_GetHeader( BITMAP_WATER_TEMP );
				}
				// specify the starting address of the bitmap in graphics RAM
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
				// specify the bitmap format, linestride and height
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
				// set filtering, wrapping and on-screen size
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
				// start drawing bitmap
				Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 352*16, 64*16 ) );
				
				if( fontMax == FONT_MAX_DEF )
				{
					/* draw DEG bitmap */
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0x00));	// YELLOW (the same color of temperature value text)
					if( TEMPFAHRENHEIT )
					{
						p_bmhdr = res_bm_GetHeader( BITMAP_DEG_F );
					}
					else
					{
						p_bmhdr = res_bm_GetHeader( BITMAP_DEG_C );
					}
					// specify the starting address of the bitmap in graphics RAM
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
					// specify the bitmap format, linestride and height
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
					// set filtering, wrapping and on-screen size
					//Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
					Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, 40, 40));	//scale to 40x40
					// start drawing bitmap
					Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
					Ft_Gpu_CoCmd_LoadIdentity(pHost);
					Ft_Gpu_CoCmd_Scale(pHost, 65536*40L/p_bmhdr->bmhdr.Width, 65536*40L/p_bmhdr->bmhdr.Height);	//scale to 40x40
					Ft_Gpu_CoCmd_SetMatrix(pHost );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 300*16, 71*16 ) );
				}

				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				Ft_Gpu_CoCmd_LoadIdentity(pHost);
				Ft_Gpu_CoCmd_Scale(pHost, 65536, 65536);	//scale to normal
				Ft_Gpu_CoCmd_SetMatrix(pHost );
				
				// draw the command bar
				/* Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,56,112));
				Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
				Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 8*16, 192*16 ) );
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 303*16, 271*16 ) ); */
				
				// draw the menu' bar
				/* Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,56,112));
				Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
				Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 320*16, 192*16 ) );
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 470*16, 271*16 ) ); */
				
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
				// command bar page
				if( iMenu1Pag == AVCMDGRP_0 )
				{
					// draw FLAMES/TSETP/TERM bitmap (first command position)
					if( EN_IBRIDO || !MODE_AUTO_MANU )
					{
						// draw FLAMES bitmap
						p_bmhdr = res_bm_GetHeader( BITMAP_FLAMES );
						if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_FIRE_TSETP, 1 ) )
						{
							// FLAMES/TSETP bitmap tag
							TagRegister( TAG_FIRE_TSETP, 16, 200, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
						}
						if( (tagval == TAG_FIRE_TSETP) && (key_cnt >= VALID_TOUCHED_KEY) )
						{
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
						}
						// specify the starting address of the bitmap in graphics RAM
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
						// specify the bitmap format, linestride and height
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
						// set filtering, wrapping and on-screen size
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
						// start drawing bitmap
						Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 16*16, 200*16 ) );
					
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE	
					}
					else if( !IsTermostato() )
					{
						// draw TSETP bitmap
						p_bmhdr = res_bm_GetHeader( BITMAP_TSETP );
						if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_FIRE_TSETP, 1 ) )
						{
							// FLAMES/TSETP bitmap tag
							TagRegister( TAG_FIRE_TSETP, 16, 200, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
						}
						if( (tagval == TAG_FIRE_TSETP) && (key_cnt >= VALID_TOUCHED_KEY) )
						{
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
						}
						// specify the starting address of the bitmap in graphics RAM
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
						// specify the bitmap format, linestride and height
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
						// set filtering, wrapping and on-screen size
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
						// start drawing bitmap
						Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 16*16, 200*16 ) );
						// display temperature setpoint
						userVisSetpTemp( s );
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
						Ft_Gpu_CoCmd_Text( pHost, 16+60, 200+32, fontValue, OPT_CENTERY|OPT_RIGHTX, String2Font(s, fontValue) );
					}
					else
					{
						// disable tag
						CTP_SetTag( TAG_FIRE_TSETP, 0 );
						// draw THERM bitmap
						p_bmhdr = res_bm_GetHeader( BITMAP_THERM );
						// specify the starting address of the bitmap in graphics RAM
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
						// specify the bitmap format, linestride and height
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
						// set filtering, wrapping and on-screen size
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
						// start drawing bitmap
						Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 16*16, 200*16 ) );
						// thermostat
						if( STATO4_TERMAMBACCUM )
						{
							// closed -> active
							//strcpy( s, sCLOSE );
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(255,0,0));
							Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
							Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
							Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (16+12)*16, (200+49)*16 ) );
							Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (16+54)*16, (200+55)*16 ) );
						}
						else
						{
							// opened -> not active
							//strcpy( s, sOPEN );
							//Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(170,170,170));
						}
						// sfrutto i primi 5 caratteri della stringa d_TERMAMB
						/* GetMsg( 0, s, iLanguage, d_TERMAMB );
						s[5] = '\0';
						Ft_Gpu_CoCmd_Text( pHost, 16+32, 200+32, fontTitle, OPT_CENTER, String2Font(s, fontTitle) ); */
						
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
					}
					
					// draw FAN bitmap (second command position)
					if( F_MULTIFAN != ZERO_FAN )
					{
						// draw FAN bitmap
						p_bmhdr = res_bm_GetHeader( BITMAP_FAN );
						if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_FAN, 1 ) )
						{
							// FAN bitmap tag
							TagRegister( TAG_FAN, 88, 200, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
						}
						if( (tagval == TAG_FAN) && (key_cnt >= VALID_TOUCHED_KEY) )
						{
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
						}
						// specify the starting address of the bitmap in graphics RAM
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
						// specify the bitmap format, linestride and height
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
						// set filtering, wrapping and on-screen size
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
						// start drawing bitmap
						Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 88*16, 200*16 ) );

						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
					}
					else
					{
						// disable tag
						CTP_SetTag( TAG_FAN, 0 );
					}
					
					// draw MODE/TSETP/TERM bitmap (third command position)
					if( !EN_IBRIDO )
					{
						// draw MAN/AUTO bitmap
						if( MODE_AUTO_MANU )
						{
							p_bmhdr = res_bm_GetHeader( BITMAP_AUTO );
						}
						else
						{
							p_bmhdr = res_bm_GetHeader( BITMAP_MAN );
						}
						if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_MODE_TSETP, 1 ) )
						{
							// MODE/TSETP bitmap tag
							TagRegister( TAG_MODE_TSETP, 160, 200, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
						}
						if( (tagval == TAG_MODE_TSETP) && (key_cnt >= VALID_TOUCHED_KEY) )
						{
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
						}
						// specify the starting address of the bitmap in graphics RAM
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
						// specify the bitmap format, linestride and height
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
						// set filtering, wrapping and on-screen size
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
						// start drawing bitmap
						Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 160*16, 200*16 ) );
						
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
					}
					else if( !IsTermostato() )
					{
						// draw TSETP bitmap
						p_bmhdr = res_bm_GetHeader( BITMAP_TSETP );
						if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_MODE_TSETP, 1 ) )
						{
							// MODE/TSETP bitmap tag
							TagRegister( TAG_MODE_TSETP, 160, 200, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
						}
						if( (tagval == TAG_MODE_TSETP) && (key_cnt >= VALID_TOUCHED_KEY) )
						{
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
						}
						// specify the starting address of the bitmap in graphics RAM
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
						// specify the bitmap format, linestride and height
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
						// set filtering, wrapping and on-screen size
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
						// start drawing bitmap
						Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 160*16, 200*16 ) );
						// display temperature setpoint
						userVisSetpTemp( s );
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
						Ft_Gpu_CoCmd_Text( pHost, 160+60, 200+32, fontValue, OPT_CENTERY|OPT_RIGHTX, String2Font(s, fontValue) );
					}
					else
					{
						// disable tag
						CTP_SetTag( TAG_MODE_TSETP, 0 );
						// draw THERM bitmap
						p_bmhdr = res_bm_GetHeader( BITMAP_THERM );
						// specify the starting address of the bitmap in graphics RAM
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
						// specify the bitmap format, linestride and height
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
						// set filtering, wrapping and on-screen size
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
						// start drawing bitmap
						Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 160*16, 200*16 ) );
						// thermostat
						if( STATO4_TERMAMBACCUM )
						{
							// closed -> active
							//strcpy( s, sCLOSE );
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(255,0,0));
							Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
							Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
							Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (160+12)*16, (200+49)*16 ) );
							Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (160+54)*16, (200+55)*16 ) );
						}
						else
						{
							// opened -> not active
							//strcpy( s, sOPEN );
							//Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(170,170,170));
						}
						// sfrutto i primi 5 caratteri della stringa d_TERMAMB
						/* GetMsg( 0, s, iLanguage, d_TERMAMB );
						s[5] = '\0';
						Ft_Gpu_CoCmd_Text( pHost, 16+32, 200+32, fontTitle, OPT_CENTER, String2Font(s, fontTitle) ); */
						
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
					}
					
					// draw WATERTEMP bitmap (fourth command position)
					if( userIsIdro() )
					{
						// draw WATERTEMP bitmap
						p_bmhdr = res_bm_GetHeader( BITMAP_WATERTEMP );
						if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_IDRO_TEMP, 1 ) )
						{
							// WATERTEMP bitmap tag
							TagRegister( TAG_IDRO_TEMP, 232, 200, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
						}
						if( (tagval == TAG_IDRO_TEMP) && (key_cnt >= VALID_TOUCHED_KEY) )
						{
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
						}
						// specify the starting address of the bitmap in graphics RAM
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
						// specify the bitmap format, linestride and height
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
						// set filtering, wrapping and on-screen size
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
						// start drawing bitmap
						Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 232*16, 200*16 ) );

						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
					}
					else
					{
						// disable tag
						CTP_SetTag( TAG_IDRO_TEMP, 0 );
					}
				}
				else if( iMenu1Pag == AVCMDGRP_1 )
				{
					// draw SUMMER/WINTER bitmap (5^ command position)
					if( userIsIdro() && userIsCrono() )
					{
						// draw SUMMER/WINTER bitmap
						if( Crono.CronoMode.BIT.Enab == CRONO_ON )
						{
							p_bmhdr = res_bm_GetHeader( BITMAP_WINTER );
						}
						else
						{
							p_bmhdr = res_bm_GetHeader( BITMAP_SUMMER );
						}
						if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_SUMMER_WINTER, 1 ) )
						{
							// TAG_SUMMER_WINTER bitmap tag
							TagRegister( TAG_SUMMER_WINTER, 16, 200, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
						}
						if( (tagval == TAG_SUMMER_WINTER) && (key_cnt >= VALID_TOUCHED_KEY) )
						{
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
						}
						// specify the starting address of the bitmap in graphics RAM
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
						// specify the bitmap format, linestride and height
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
						// set filtering, wrapping and on-screen size
						Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
						// start drawing bitmap
						Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
						Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 16*16, 200*16 ) );
						
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
					}
					else
					{
						// disable tag
						CTP_SetTag( TAG_SUMMER_WINTER, 0 );
					}
					
					// draw ESM bitmap (6^ command position)
					if( userIsIdro() && ConfigIdro )
					{
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0x00));	// YELLOW
						if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_ESM, 1 ) )
						{
							// TAG_ESM bitmap tag
							TagRegister( TAG_ESM, 88, 200, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
						}
						if( (tagval == TAG_ESM) && (key_cnt >= VALID_TOUCHED_KEY) )
						{
							Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
						}
						Ft_Gpu_CoCmd_Text( pHost, 88+32, 200+32, FONT_TITLE_DEF, OPT_CENTER, sESM );
						
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
					}
					else
					{
						// disable tag
						CTP_SetTag( TAG_ESM, 0 );
					}
					
					// draw CMDGRP_1_2 bitmap (7^ command position)
					if( 0 )
					{
						/* if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_CMDGRP_1_2, 1 ) )
						{
							// TAG_CMDGRP_1_2 bitmap tag
							TagRegister( TAG_CMDGRP_1_2, 160, 200, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
						} */
					}
					else
					{
						// disable tag
						CTP_SetTag( TAG_CMDGRP_1_2, 0 );
					}
					
					// draw CMDGRP_1_3 bitmap (8^ command position)
					if( 0 )
					{
						/* if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_CMDGRP_1_3, 1 ) )
						{
							// TAG_CMDGRP_1_3 bitmap tag
							TagRegister( TAG_CMDGRP_1_3, 232, 200, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
						} */
					}
					else
					{
						// disable tag
						CTP_SetTag( TAG_CMDGRP_1_3, 0 );
					}
					
				}
				
				
				// draw SETTINGS bitmap (first menu' position)
				p_bmhdr = res_bm_GetHeader( BITMAP_SETTINGS );
				if( tagval == TOUCH_TAG_INIT )
				{
					// SETTINGS bitmap tag
					TagRegister( TAG_MENU, 328, 200, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
				}
				if( (tagval == TAG_MENU) && (key_cnt >= VALID_TOUCHED_KEY) )
				{
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
				}
				// specify the starting address of the bitmap in graphics RAM
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
				// specify the bitmap format, linestride and height
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
				// set filtering, wrapping and on-screen size
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
				// start drawing bitmap
				Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 328*16, 200*16 ) );

				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
				// draw INFO bitmap (second menu' position)
				p_bmhdr = res_bm_GetHeader( BITMAP_INFO );
				if( tagval == TOUCH_TAG_INIT )
				{
					// INFO bitmap tag
					TagRegister( TAG_INFO, 400, 200, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height );
				}
				if( (tagval == TAG_INFO) && (key_cnt >= VALID_TOUCHED_KEY) )
				{
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
				}
				// specify the starting address of the bitmap in graphics RAM
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
				// specify the bitmap format, linestride and height
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
				// set filtering, wrapping and on-screen size
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
				// start drawing bitmap
				Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 400*16, 200*16 ) );

				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
				
				// draw DATE-TIME
				GetMsg( 0, s, iLanguage, sWeekDay_tab[DateTime.tm_wday] );
				//s[4] = '\0';	// only the first 3 characters for week day
				strcat( s, " " );
				ScriviOra( &s[strlen(s)] );
				s[strlen(s)-3] = '\0';	// do not display seconds
				if( tagval == TOUCH_TAG_INIT )
				{
					// SET TIME tag
					TagRegister( TAG_TIME, 136, 0, 272, 32 );
				}
				if( (tagval == TAG_TIME) && (key_cnt >= VALID_TOUCHED_KEY) )
				{
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,0,128));
					Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 136*16, 0*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 407*16, 31*16 ) );
				}
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				Ft_Gpu_CoCmd_Text( pHost, 272, 17, fontTitle, OPT_CENTER, String2Font(s, fontTitle) );
				ScriviData( s );
				if( tagval == TOUCH_TAG_INIT )
				{
					// SET DATE tag
					TagRegister( TAG_DATE, 136, 32, 272, 32 );
				}
				if( (tagval == TAG_DATE) && (key_cnt >= VALID_TOUCHED_KEY) )
				{
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,0,128));
					Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(RECTS) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 136*16, 32*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 407*16, 63*16 ) );
				}
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				Ft_Gpu_CoCmd_Text( pHost, 272, 47, fontTitle, OPT_CENTER, String2Font(s, fontTitle) );
				
				// draw TEMPERATURE
				userVisTemp( s );
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0x00));	// YELLOW
				Ft_Gpu_CoCmd_Text( pHost, 
										((fontMax == FONT_MAX_DEF) ? 296 : 340 ),
										96, fontMax, OPT_CENTERY|OPT_RIGHTX, String2Font(s, fontMax) );

				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
				// draw H2O or ECO shutdown
				if( userIsIdro() && unCMD_SHOW_SDH2O )	// Se IDRO abilitato (F700=1) e Spegnimento per Idro attivo (D033=1)
				{
					// H2O
					GetMsg( 0, s, iLanguage, sH2O );
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
					if( (i = GetFont4StringWidth( s, fontMax, (470-410) )) > 0 )
					{
						Ft_Gpu_CoCmd_Text( pHost, 440, 96, i, OPT_CENTER, String2Font(s, i) );
					}
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				}
				else if( ECO_ENB )	// Altrimenti, se ECO abilitato (F004=1)
				{
					// ECO
					if( SPE_ECO )	// Se Spegnimento per Eco attivo (D034=1)
					{
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0xFF,0x00));	// GREEN
					}
					else
					{
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(128,128,128));	// GREY
					}
					// sfrutto i primi 3 caratteri della stringa d_ECO
					GetMsg( 0, s, iLanguage, d_ECO );
					s[3] = '\0';
					Ft_Gpu_CoCmd_Text( pHost, 440, 96, fontMax, OPT_CENTER, String2Font(s, fontMax) );

					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				}
				
				// draw STATUS BOX
				Ft_App_WrCoCmd_Buffer(pHost, Get_UserColorHex(UserColor_tab[UserColorId].addon) );
				Ft_App_WrCoCmd_Buffer(pHost, LINE_WIDTH(1 * 16) );
				Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 76*16, 128*16 ) );
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 469*16, 128*16 ) );
				// Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 469*16, 190*16 ) );
				// Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 76*16, 190*16 ) );
				// Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 76*16, 128*16 ) );
				
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
				// draw STATUS
				GetMsg( 0, s, iLanguage, sStatoStufa_tab[STATO_STUFA] );
				// check the string length for the STATUS BOX horizontal size
				if( (i = GetFont4StringWidth( s, fontMax, (469-76) )) > 0 )
				{
					Ft_Gpu_CoCmd_Text( pHost, 272, 160, i, OPT_CENTER, String2Font(s, i) );
				}

				Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
				Ft_Gpu_CoCmd_Swap( pHost );
				Ft_App_Flush_Co_Buffer( pHost );
				Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
				
			#ifdef SCREENSHOT
				if( IsGenScreeshot() &&
					(userCurrentState != USER_STATE_AVVIO_ALLARME) &&	// not in alarm
					!EN_IBRIDO &&	// not IBRIDO
					userIsCrono() && (Crono.CronoMode.BIT.Enab == CRONO_ON) &&	// chrono on
					lByTogVisTH2O &&	// air temperature displayed
					( F_MULTIFAN >= DOUBLE_FAN ) &&	// 2 FAN
					userIsIdro()	// idro
					)
				{
					if( LOGIC_ONOFF && STATO5_TREVIE )
					{
						// ON con sanitari attivi
						i = SSHOT_Main_ON;
					}
					else if( iMenu1Pag == AVCMDGRP_0 )
					{
						// commands page 1 
						if( !MODE_AUTO_MANU )
						{
							// MANUAL
							i = SSHOT_Main_MAN;
						}
						else if( !IsTermostato() )
						{
							if( ECO_ENB )
							{
								// AUTO
								i = SSHOT_Main_AUTO;
							}
						}
						else
						{
							// TERMOSTATO
							i = SSHOT_Main_THERM;
						}
					}
					else if( iMenu1Pag == AVCMDGRP_1 )
					{
						if( ConfigIdro )
						{
							// COMMAND PAGE 2
							i = SSHOT_Main_CMD2;
						}
					}
				
					// screenshot
					DoScreenshot( i );
				}
			#endif	// SCREENSHOT
			}
		
			// main window loop
			do
			{
				Wdog();
				ALIM_INP = DIGIN_ALIM;
				// wait for a touch event
				event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

				// process the touch event
				if( event.status == osEventMessage )
				{
					pt = (TAG_STRUCT *)event.value.v;
					tagval = pt->tag;
				}
			} while( event.status == osOK );
			
			// eventuale fine beep buzzer
			if( checktimer( T_BUZZER ) != INCORSO )
			{
				// buzzer off
				led_Set( BUZ, LEDSTAT_OFF );
			}

			// connessione alimentazione esterna
			if( !ALIM_INP && DIGIN_ALIM )
			{
				// full LCD backlight
				SetBacklight( LCD_BACKLIGHT_MAX );
				starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
			}
			
			// event processing
			if( (i = Check4StatusChange()) != USER_MAX_STATES )
			{
				cntExit4Alarm = 0;
				// buzzer off
				led_Set( BUZ, LEDSTAT_OFF );
				/*
				 * Richiesta di pausa standby (rilascio pulsante) dopo timeout
				 * utente, con alimentazione esterna assente, ma con comunicazione
				 * OT/+ ancora attiva:
				 * => Riaccende solo il backlight display.
				 */
				if( (i == USER_STATE_PAUSE) && (UserRequest == USER_REQ_STANDBY)
					&& !DIGIN_ALIM
					&& StatSec.BackLightTime
					&& (readtimer( TUSER100 ) == NOTIMER)
					&& (
						(PeekPkt( SEND_MODE_ONE_SHOT ) != NULL)
						|| (iLimReq < LIMITSREQ_NUM)
						)
					)
				{
					UserRequest = USER_REQ_NONE;
					// full LCD backlight
					SetBacklight( LCD_BACKLIGHT_MAX );
					starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
				}
				else
				{
					loop = 0;	// exit
				}
			}
			/*
			 * Evento touch dopo timeout utente,
			 * con alimentazione esterna assente, ma con comunicazione
			 * OT/+ ancora attiva:
			 * => Non considerare, riaccensione backlight solo da rilascio pulsante.
			 */
			else if( (event.status == osEventMessage)
					&& !DIGIN_ALIM
					&& StatSec.BackLightTime
					&& (readtimer( TUSER100 ) == NOTIMER)
					&& (
						(PeekPkt( SEND_MODE_ONE_SHOT ) != NULL)
						|| (iLimReq < LIMITSREQ_NUM)
						)
					)
			{
				// tag reset
				tagval = 0;
				scrollONcmd = 0;
				scrollON = 0;
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
			else if( LoadFonts() > 0 )
			{
				// fonts updated -> restart window
				tagval = TOUCH_TAG_INIT;
				// scrollONcmd = 0;
				// scrollON = 0;
				// Flag.Key_Detect = 0;
				// key_cnt = 0;
			}
			else if( event.status == osEventTimeout )
			{
				// tag reset
				tagval = 0;
				scrollONcmd = 0;
				scrollON = 0;
				Flag.Key_Detect = 0;
				key_cnt = 0;

				// LCD backlight
				if( StatSec.BackLightTime )
				{
					timer = readtimer( TUSER100 );
					if( timer == NOTIMER )
					{
						//SetBacklight( LCD_BACKLIGHT_MIN );
						SetBacklight( 0 );
						// buzzer off
						led_Set( BUZ, LEDSTAT_OFF );
						// se alimentazione esterna presente, si visualizza la standby window
						if( DIGIN_ALIM )
						{
							// pop bitmaps from Graphic RAM
							ReleaseAllBitmapAvvio();
							// standby window
							RetVal = userVisStandby();
							//if( (RetVal = userVisStandby()) == USER_STATE_STANDBY )
							{
								loop = 0;	// exit
								break;	// assicura l'uscita dal while-loop subito da qui
							}
						}
						// altrimenti comando la pausa lunga solo se non ci sono richieste pendenti
						else if( (PeekPkt( SEND_MODE_ONE_SHOT ) == NULL)
							&& (iLimReq >= LIMITSREQ_NUM)
							)
						{
							userStartOTLongPause();
						}
					}
					else if( (timer >= (BUZZER_ALARM_ON + 5*T1SEC))
							&& (userCurrentState == USER_STATE_AVVIO_ALLARME)
							&& cntExit4Alarm
							)
					{
						/*
							Siamo usciti dalla pausa standby per allarme.
							Si rimane attivi solo per poco piu' del tempo
							necessario a far suonare il buzzer.
						*/
						SetBacklight( LCD_BACKLIGHT_MIN );
						// buzzer off
						led_Set( BUZ, LEDSTAT_OFF );
						userStartOTLongPause();
					}
					else if( timer >= USER_TIMEOUT_LIGHT_MID )
					{
						SetBacklight( LCD_BACKLIGHT_MID );
					}
				}
				
				// room/water temperature
				if(userCounterTempOra)
					userCounterTempOra--;
				if(!userCounterTempOra)
				{
					if(F_IDRO_PAN)
					{
						lByTogVisTH2O ^= 1;
					}
					if(!F_IDRO_PAN)
					{
						lByTogVisTH2O = 1;
					}
					if(lByTogVisTH2O)
					{
						userCounterTempOra = TIMEOUT_AVVIO_5SEC;
						// se WiFi abilitato
						// richiedi indirizzo IP a PH2 per aggiornare MODEM bitmap
						if( IsWiFi() )
						{
							userReqPar( P_WIFI_IP );
						}
					}
					else
					{
						userCounterTempOra = TIMEOUT_AVVIO_2SEC;
					}
				}

				// gestione comando FAN da telecomando IR

				// check alarms
				if( userCurrentState == USER_STATE_AVVIO )
				{
					if( (TIPO_ALLARME != NO_ALARM) || (STATO_STUFA == STUFASTAT_ALLARME) )
					{
						userCurrentState = USER_STATE_AVVIO_ALLARME;
						// buzzer on
						osTimerStop( LedTimerHandle );	// resincronizza led periodic timer per avere i buzzer beep precisi
						osTimerStart( LedTimerHandle, 50 );	// start 50ms led periodic timer
						//led_Set( BUZ, LEDSTAT_1_IMPULSE_LONG );	no segnalazioni acustiche
						starttimer( T_BUZZER, BUZZER_ALARM_ON );
						// pop bitmaps from Graphic RAM
						ReleaseAllBitmapAvvio();
						// start Alarm window
						SBLOCCO_PAN = 0;
						SetBacklight( LCD_BACKLIGHT_MAX );
						starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
						//OT_DL_Pausa = OT_DL_PAUSE;
						switch( WinAlarm() )
						{
						case 1:
							// alarm recovered
							cntExit4Alarm = 0;
							userCurrentState = USER_STATE_AVVIO;
							break;
						case -1:
							// status changed
							// buzzer off
							led_Set( BUZ, LEDSTAT_OFF );
							loop = 0;	// exit
							break;
						}
						if( !LoadAllBitmapAvvio() )
						{
							ReleaseAllBitmapAvvio();
							loop = 0;	// exit
						}
									
						// restart window
						tagval = TOUCH_TAG_INIT;
						// scrollONcmd = 0;
						// scrollON = 0;
						// Flag.Key_Detect = 0;
						// key_cnt = 0;
					}
				}
				else if( userCurrentState == USER_STATE_AVVIO_ALLARME )
				{
					if( userCounterTime )
						userCounterTime--;
					// check for alarm recovery
					if( (TIPO_ALLARME == NO_ALARM) && (STATO_STUFA != STUFASTAT_ALLARME) )
					{
						ExecUnlock();

						cntExit4Alarm = 0;
						userCurrentState = USER_STATE_AVVIO;
					}
					else if( userCounterTime == 0 )
					{
						// pop bitmaps from Graphic RAM
						ReleaseAllBitmapAvvio();
						// restart Alarm window
						SBLOCCO_PAN = 0;
						SetBacklight( LCD_BACKLIGHT_MAX );
						starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
						//OT_DL_Pausa = OT_DL_PAUSE;
						switch( WinAlarm() )
						{
						case 1:
							// alarm recovered
							cntExit4Alarm = 0;
							userCurrentState = USER_STATE_AVVIO;
							break;

						case -1:
							// status changed
							// buzzer off
							led_Set( BUZ, LEDSTAT_OFF );
							loop = 0;	// exit
							break;
						}
						if( !LoadAllBitmapAvvio() )
						{
							ReleaseAllBitmapAvvio();
							loop = 0;	// exit
						}
									
						// restart window
						tagval = TOUCH_TAG_INIT;
						// scrollONcmd = 0;
						// scrollON = 0;
						// Flag.Key_Detect = 0;
						// key_cnt = 0;
					}
				}
			}
			else if( tagval == TAG_HORIZ_SCROLL )
			//else if( pt->tag == TAG_HORIZ_SCROLL )
			{
				scrollONcmd = 0;	// reset the other horizontal scroll tag
				cntExit4Alarm = 0;
				// full LCD backlight
				SetBacklight( LCD_BACKLIGHT_MAX );
				starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
				// screen horizontal scroll
				if( pt->status )
				{
					// touched
					switch( scrollON )
					{
					default:
					case 0:
						preXscroll = pt->x;
						scrollON = 1;
						break;

					case 1:
					case 2:
						if( key_cnt < 5 )
						{
							Dx = pt->x - preXscroll;
							//if( Dx > 5 )
							if( Dx > 100 )
							{
								scrollON = 2;
								Flag.Key_Detect = 0;
								key_cnt = 0;
								// scroll right
								preXscroll = pt->x;
								
								if( userCurrentState == USER_STATE_AVVIO )
								{
									// buzzer off
									led_Set( BUZ, LEDSTAT_OFF );
									// pop bitmaps from Graphic RAM
									ReleaseAllBitmapAvvio();
									// standby window
									RetVal = userVisStandby();
									//if( (RetVal = userVisStandby()) == USER_STATE_STANDBY )
									{
										loop = 0;	// exit
									}
								}
								
								// restart window
								tagval = TOUCH_TAG_INIT;
								scrollON = 0;
								Flag.Key_Detect = 0;
								key_cnt = 0;
							}
							//else if( Dx < -5 )
							else if( Dx < -100 )
							{
								scrollON = 2;
								Flag.Key_Detect = 0;
								key_cnt = 0;
								// scroll left
								
								preXscroll = pt->x;
							}
						}
						break;
					}
				}
				else
				{
					// untouched
					scrollON = 0;
				}
			}
			else if( tagval == TAG_CMD_HORIZ_SCROLL )
			//else if( pt->tag == TAG_CMD_HORIZ_SCROLL )
			{
				scrollON = 0;	// reset the other horizontal scroll tag
				cntExit4Alarm = 0;
				// full LCD backlight
				SetBacklight( LCD_BACKLIGHT_MAX );
				starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
				// screen horizontal scroll
				if( pt->status )
				{
					// touched
					switch( scrollONcmd )
					{
					default:
					case 0:
						preXscrollCmd = pt->x;
						scrollONcmd = 1;
						break;

					case 1:
					case 2:
						if( key_cnt < 5 )
						{
							DxCmd = pt->x - preXscrollCmd;
							//if( DxCmd > 5 )
							if( DxCmd > 100 )
							{
								scrollONcmd = 2;
								Flag.Key_Detect = 0;
								key_cnt = 0;
								// scroll right
								preXscrollCmd = pt->x;
								// view the previous group on the command bar
								if( iMenu1Pag )
								{
									// disable current group tags
									for( i=0; i<NAVCMD; i++ )
									{
										CTP_SetTag( AvGrpTags[iMenu1Pag][i], 0 );
									}
									// previous group
									iMenu1Pag--;
									// restart window
									tagval = TOUCH_TAG_INIT;
								}
								//scrollONcmd = 0;
							}
							//else if( DxCmd < -5 )
							else if( DxCmd < -100 )
							{
								scrollONcmd = 2;
								Flag.Key_Detect = 0;
								key_cnt = 0;
								// scroll left
								preXscrollCmd = pt->x;
								// view the next group on the command bar
								if( (iMenu1Pag < NAVCMDGRP-1) && Check4CmdGroup(iMenu1Pag+1) )
								{
									// disable current group tags
									for( i=0; i<NAVCMD; i++ )
									{
										CTP_SetTag( AvGrpTags[iMenu1Pag][i], 0 );
									}
									// next group
									iMenu1Pag++;
									// restart window
									tagval = TOUCH_TAG_INIT;
								}
								//scrollONcmd = 0;
							}
						}
						break;
					}
				}
				else
				{
					// untouched
					scrollONcmd = 0;
				}
			}
			else if( (tagval != TAG_HORIZ_SCROLL) &&
						(scrollON != 2) && 
						(tagval != TAG_CMD_HORIZ_SCROLL) &&
						(scrollONcmd != 2) )	// tagval > 0 without scroll
			{
				cntExit4Alarm = 0;
				// full LCD backlight
				SetBacklight( LCD_BACKLIGHT_MAX );
				starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );

				/*!
				** \todo
				** 
				** Gestione telecomando IR (visto come touch tag particolari)
				** 
				**/
				
				
				// item
				tagval = Read_Keypad( pt );	// read the keys
				if( tagval )
				{
					// touched
					if( Flag.Key_Detect )
					{
						// first touch
						Flag.Key_Detect = 0;
						key_cnt = 0;
					}
					/* else if( ++key_cnt >= VALID_TOUCHED_KEY )
					{
						// pressed item
						key_cnt = VALID_TOUCHED_KEY;
					} */
					else if( ++key_cnt >= 100 )
					{
						// pressed item
						switch( tagval )
						{
							case TAG_ON_OFF_PLAY:
								// gestione ON_OFF_PLAY (tasto premuto per almeno 2s)
								if( userCurrentState == USER_STATE_AVVIO )
								{
									if( STATO_STUFA != STUFASTAT_IN_RESET )
									{
										// comando toggle on/off logico
										userParValue.b[0] = LOGIC_ONOFF ^ 1;
										userSetPar( P_LOGIC_ONOFF );

										// Comunicazione sonora per comando inviato.
										//led_Set( BUZ, LEDSTAT_ON );	no segnalazioni acustiche
										starttimer( T_BUZZER, 2*T1SEC );
										// comando il suono sulla stufa
										OT_ReqSound( BUZ_1LONG );

										// on/off command wait time
										Ft_Gpu_CoCmd_Dlstart(pHost);
										Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
										Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
										Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,0xFF,0));	// GREEN
										Ft_Gpu_CoCmd_Spinner(pHost, (FT_DispWidth/2),(FT_DispHeight/2),0,1);
										Ft_App_Flush_Co_Buffer( pHost );
										Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

										// wait 2s for the on/off command
										for( i=0; i<USER_TIMEOUT_SHOW_2; i++ )
										{
											Wdog();
											Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
											// flush the touch queue
											FlushTouchQueue();
										}

										// Send the stop command
										Ft_Gpu_Hal_WrCmd32(pHost, CMD_STOP);
										Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);
										
										// restart window
										tagval = TOUCH_TAG_INIT;
										// scrollONcmd = 0;
										// scrollON = 0;
										// Flag.Key_Detect = 0;
										// key_cnt = 0;
									}
								}
								else if( (userCurrentState == USER_STATE_AVVIO_ALLARME) && !SBLOCCO_PAN )
								{
									// buzzer off
									led_Set( BUZ, LEDSTAT_OFF );
									// send UNLOCK command
									SBLOCCO_PAN = 1;
									sPkt.pktLen = 0;
									sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
									sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
									sPkt.pkt[sPkt.pktLen++] = MB_FUNC_UNLOCK;	// cmd
									OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );

									// unlock wait time
									Ft_Gpu_CoCmd_Dlstart(pHost);
									Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
									Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
									Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,0xFF,0));	// GREEN
									Ft_Gpu_CoCmd_Spinner(pHost, (FT_DispWidth/2),(FT_DispHeight/2),0,1);
									Ft_App_Flush_Co_Buffer( pHost );
									Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

									// wait for the unlock result with 20s timeout
									for( i=0; i<USER_TIMEOUT_SHOW_20; i++ )
									{
										Wdog();
										Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
										// flush the touch queue
										FlushTouchQueue();
										// check for alarm recovery
										if( (TIPO_ALLARME == NO_ALARM) && (STATO_STUFA != STUFASTAT_ALLARME) )
										{
											ExecUnlock();
											userCurrentState = USER_STATE_AVVIO;
											break;
										}
									}

									// Send the stop command
									Ft_Gpu_Hal_WrCmd32(pHost, CMD_STOP);
									Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);
									
									// restart window
									tagval = TOUCH_TAG_INIT;
									// scrollONcmd = 0;
									// scrollON = 0;
									// Flag.Key_Detect = 0;
									// key_cnt = 0;
								}
							break;
							
							default:
							break;
							
							/*!
							** \todo
							** 
							** comando bypass accensione (per ora non si fa)
							**/
							
						}
					}
				}
				else if( pt->tag )
				{
					// untouched
					if( key_cnt >= VALID_TOUCHED_KEY )
					{
						// flush the touch queue
						FlushTouchQueue();
						// buzzer off
						led_Set( BUZ, LEDSTAT_OFF );

						// process the pressed item on touch release
						switch( pt->tag )
						{
							default:
								// restart window
								tagval = TOUCH_TAG_INIT;
							break;
							
							case TAG_CHRONO:
							#ifndef menu_CRONO
								if( userIsCrono() )
								{
									// pop bitmaps from Graphic RAM
									ReleaseAllBitmapAvvio();
									// set crono
									RetVal = userVisCrono();
									//if( (RetVal = userVisCrono()) == USER_STATE_CRONO )
									{
										loop = 0;	// exit
									}
									// restart window
									tagval = TOUCH_TAG_INIT;
								}
							#endif
							break;
							
							case TAG_SLEEP:
								if( userIsSleep() &&
									(userCurrentState != USER_STATE_AVVIO_ALLARME) )
								{
									// set sleep
									userSubState = 0;	// parameter view
									userGetPar( P_SLEEP );
									if( WinSetParam( 
														P_SLEEP,
														(char *)GetMsg( 1, NULL, iLanguage, MENU_SLEEP ),
														(char *)GetMsg( 2, NULL, iLanguage, MENU_SLEEP )
														) )
									{
										UpdateSleepTime();
									}
									// restart window
									tagval = TOUCH_TAG_INIT;
								}
							break;
							
							case TAG_TIME:
								// set time
								userSubState = 0;	// parameter view
								WinSetTime( (char *)GetMsg( 1, NULL, iLanguage, d_HOUR ) );
								// restart window
								tagval = TOUCH_TAG_INIT;
							break;
							
							case TAG_DATE:
								// set date
								userSubState = 0;	// parameter view
								WinSetDate( (char *)GetMsg( 1, NULL, iLanguage, d_DATE ) );
								// restart window
								tagval = TOUCH_TAG_INIT;
							break;
							
							case TAG_MODEM:
								if( IsWiFiNet() )
								{
									// pop bitmaps from Graphic RAM
									ReleaseAllBitmapAvvio();
									// set WiFi
									RetVal = EnterMenuNetwork();
									//if( (RetVal = EnterMenuNetwork()) == USER_STATE_MENU )
									{
										loop = 0;	// exit
									}
									// restart window
									tagval = TOUCH_TAG_INIT;
								}
							break;
							
							case TAG_WARNING:
								if( userIsWarning() )
								{
									// pop bitmaps from Graphic RAM
									ReleaseAllBitmapAvvio();
									// warnings list
									RetVal = EnterMenuWarn();
									//if( (RetVal = EnterMenuWarn()) == USER_STATE_MENU )
									{
										loop = 0;	// exit
									}
									// restart window
									tagval = TOUCH_TAG_INIT;
								}
							break;
							
							case TAG_FIRE_TSETP:
								userSubState = 0;	// parameter view
								if( EN_IBRIDO || !MODE_AUTO_MANU )
								{
									// set power level
									userGetPar( P_POWER );
									if( WinSetPower( (char *)GetMsg( 1, NULL, iLanguage, sFIRE ), P_POWER ) )
									{
										userSetPar( P_POWER );
									}
									// restart window
									tagval = TOUCH_TAG_INIT;
								}
								else if( !IsTermostato() )
								{
									// set room temperature setpoint
									userGetPar( P_TEMPERATURE );
									if( WinSetTemp( (char *)GetMsg( 1, NULL, iLanguage, sTEMP ), P_TEMPERATURE ) )
									{
										userSetPar( P_TEMPERATURE );
									}
									// restart window
									tagval = TOUCH_TAG_INIT;
								}
							break;
							
							case TAG_FAN:
								userSubState = 0;	// parameter view
								if( F_MULTIFAN != ZERO_FAN )
								{
									// set FAN
									WinSetFan( (char *)GetMsg( 1, NULL, iLanguage, sFAN ) );
									// restart window
									tagval = TOUCH_TAG_INIT;
								}
							break;
							
							case TAG_MODE_TSETP:
								userSubState = 0;	// parameter view
								if( !EN_IBRIDO )
								{
									// toggle MANU/AUTO mode
									userParValue.b[0] = MODE_AUTO_MANU ^ 1;
									userSetPar( P_MODE );
								}
								else if( !IsTermostato() )
								{
									// set room temperature setpoint
									userGetPar( P_TEMPERATURE );
									if( WinSetTemp( (char *)GetMsg( 1, NULL, iLanguage, sTEMP ), P_TEMPERATURE ) )
									{
										userSetPar( P_TEMPERATURE );
									}
									// restart window
									tagval = TOUCH_TAG_INIT;
								}
							break;
							
							case TAG_IDRO_TEMP:
								userSubState = 0;	// parameter view
								if( userIsIdro() )
								{
									// set IDRO temperatures setpoints
									WinSetIdro( (char *)GetMsg( 1, NULL, iLanguage, MENU_IDRO ) );
									// restart window
									tagval = TOUCH_TAG_INIT;
								}
							break;
							
							case TAG_MENU:
								// pop bitmaps from Graphic RAM
								ReleaseAllBitmapAvvio();
								// settings
								RetVal = EnterMainMenu();
								//if( (RetVal = EnterMainMenu() ) == USER_STATE_MENU )
								{
									loop = 0;	// exit
								}
								// restart window
								tagval = TOUCH_TAG_INIT;
							break;
							
							case TAG_INFO:
								// pop bitmaps from Graphic RAM
								ReleaseAllBitmapAvvio();
								// info
								RetVal = EnterMenuInfo();
								//if( (RetVal = EnterMenuInfo()) == USER_STATE_INFO )
								{
									loop = 0;	// exit
								}
								// restart window
								tagval = TOUCH_TAG_INIT;
							break;
							
							case TAG_SUMMER_WINTER:
								if( userIsIdro() && userIsCrono() )
								{
									// toggle enable/disable Chrono
									userToggleCronoEnable();
								}
							break;
							
							case TAG_ESM:
								if( userIsIdro() && ConfigIdro )
								{
									// ESM 
									WinESM( (char *)sESM );
									// restart window
									tagval = TOUCH_TAG_INIT;
								}
							break;
							
							case TAG_CMDGRP_1_2:
							case TAG_CMDGRP_1_3:
								// restart window
								tagval = TOUCH_TAG_INIT;
							break;
							
						}
					}
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
			}
			else
			{
				// tagval > 0 with scroll
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}
	}
	else
	{
		// reset bitmps GRAM
		res_bm_Reset();
	}


	if( (RetVal != USER_STATE_MENU) &&
		(RetVal != USER_STATE_INFO ) &&
		(RetVal != USER_STATE_STANDBY ) &&
		(RetVal != USER_STATE_CRONO ) )
	{
		// pop bitmaps from Graphic RAM
		ReleaseAllBitmapAvvio();
	}
	
	return RetVal;
}



/*!
** \fn short WinAlarm( void )
** \brief Alarm window handler
** \return 1: alarm recovered,
** 		   0: quit with alarm still present
** 		  -1: exit for status changed
**/
static short WinAlarm( void )
{
	short RetVal = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrPLAY;
	BM_HEADER * p_bmhdrALARM;
	BM_HEADER * p_bmhdrMUTE;
	char s[DISPLAY_MAX_COL];
	unsigned long key_cnt = 0;
	unsigned long timer;
	unsigned char loop = 1;
	short i;
	const char *sAlarm;
	const char *sAlarmInfo;
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	
	// push bitmap PLAY into Graphic RAM
	if( res_bm_Load( BITMAP_PLAY, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_PLAY );
	#endif
		return RetVal;
	}
	p_bmhdrPLAY = res_bm_GetHeader( BITMAP_PLAY );
	
	// push bitmap ALARM into Graphic RAM
	if( res_bm_Load( BITMAP_ALARM, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_ALARM );
	#endif
		return RetVal;
	}
	p_bmhdrALARM = res_bm_GetHeader( BITMAP_ALARM );
	
	// push bitmap MUTE into Graphic RAM
	if( res_bm_Load( BITMAP_MUTE, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_MUTE );
	#endif
		return RetVal;
	}
	p_bmhdrMUTE = res_bm_GetHeader( BITMAP_MUTE );

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			// full LCD backlight
			SetBacklight( LCD_BACKLIGHT_MAX );
			starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
		}

		Ft_Gpu_CoCmd_Dlstart(pHost);
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			
		// draw PLAY bitmap
		if( tagval == TOUCH_TAG_INIT )
		{
			// PLAY bitmap tag
			TagRegister( TAG_ON_OFF_PLAY, 416, 42, p_bmhdrPLAY->bmhdr.Width, p_bmhdrPLAY->bmhdr.Height );
		}
		if( (tagval == TAG_ON_OFF_PLAY) && (key_cnt >= VALID_TOUCHED_KEY) )
		{
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
		}
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrPLAY->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrPLAY->bmhdr.Format, p_bmhdrPLAY->bmhdr.Stride, p_bmhdrPLAY->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrPLAY->bmhdr.Width, p_bmhdrPLAY->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 416*16, 42*16 ) );

		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			
		// draw MUTE bitmap
		if( tagval == TOUCH_TAG_INIT )
		{
			// MUTE bitmap tag
			TagRegister( TAG_MUTE, 416, 118, p_bmhdrMUTE->bmhdr.Width, p_bmhdrMUTE->bmhdr.Height );
		}
		if( (tagval == TAG_MUTE) && (key_cnt >= VALID_TOUCHED_KEY) )
		{
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
		}
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrMUTE->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrMUTE->bmhdr.Format, p_bmhdrMUTE->bmhdr.Stride, p_bmhdrMUTE->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrMUTE->bmhdr.Width, p_bmhdrMUTE->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 416*16, 118*16 ) );

		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		// draw QUIT bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 416*16, 194*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// QUIT bitmap tag
			TagRegister( TAG_QUIT, 416, 194, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
		}
			
		// draw ALARM bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrALARM->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrALARM->bmhdr.Format, p_bmhdrALARM->bmhdr.Stride, p_bmhdrALARM->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrALARM->bmhdr.Width, p_bmhdrALARM->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 160*16, (115-p_bmhdrALARM->bmhdr.Height/2)*16 ) );
			
		// draw alarm code
		s[0] = Hex2Ascii( TIPO_ALLARME/10 );
		s[1] = Hex2Ascii( TIPO_ALLARME%10 );
		s[2] = '\0';
		Ft_Gpu_CoCmd_Text( pHost, (160+p_bmhdrALARM->bmhdr.Width+5), 115, fontMax, OPT_CENTERY, String2Font(s, fontMax) );

		// draw alarm description
		sAlarm = GetMsg( 2, NULL, iLanguage, ALARMSOURCE00+TIPO_ALLARME );
		if( (i = GetFont4StringWidth( (char *)sAlarm, fontDesc, FT_DispWidth )) > 0 )
		{
			Ft_Gpu_CoCmd_Text( pHost, (FT_DispWidth-p_bmhdrQUIT->bmhdr.Width)/2, 180, i, OPT_CENTERX, String2Font((char *)sAlarm, i) );
		}

		// draw alarm info
		sAlarmInfo = GetMsg( 3, NULL, iLanguage, ALARMRECOVERY00+TIPO_ALLARME );
		if( (i = GetFont4StringWidth( (char *)sAlarmInfo, fontDesc, FT_DispWidth )) > 0 )
		{
			Ft_Gpu_CoCmd_Text( pHost, (FT_DispWidth-p_bmhdrQUIT->bmhdr.Width)/2, 208, i, OPT_CENTERX, String2Font((char *)sAlarmInfo, i) );
		}
			
		// title
		GetMsg( 0, s, iLanguage, ALARM );
		Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
		Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(s, fontTitle) );

		Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
		Ft_Gpu_CoCmd_Swap( pHost );
		Ft_App_Flush_Co_Buffer( pHost );
		Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() )
		{
			// screenshot
			DoScreenshot( SSHOT_Alarm );
		}
	#endif	// SCREENSHOT

		// alarm window loop
		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
			}
		} while( event.status == osOK );
			
		// eventuale fine beep buzzer
		if( checktimer( T_BUZZER ) != INCORSO )
		{
			// buzzer off
			led_Set( BUZ, LEDSTAT_OFF );
		}
		
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			RetVal = -1;
			// buzzer off
			led_Set( BUZ, LEDSTAT_OFF );
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;

			// LCD backlight
			if( StatSec.BackLightTime )
			{
				timer = readtimer( TUSER100 );
				if( timer == NOTIMER )
				{
					SetBacklight( LCD_BACKLIGHT_MIN );
					// buzzer off
					led_Set( BUZ, LEDSTAT_OFF );
					// comando la pausa lunga solo se non ci sono richieste pendenti
					if( (PeekPkt( SEND_MODE_ONE_SHOT ) == NULL)
						&& (iLimReq >= LIMITSREQ_NUM)
						)
					{
						userStartOTLongPause();
					}
				}
				else if( (timer >= (BUZZER_ALARM_ON + 5*T1SEC))
						&& cntExit4Alarm
						)
				{
					/*
						Siamo usciti dalla pausa standby per allarme.
						Si rimane attivi solo per poco piu' del tempo
						necessario a far suonare il buzzer.
					*/
					SetBacklight( LCD_BACKLIGHT_MIN );
					// buzzer off
					led_Set( BUZ, LEDSTAT_OFF );
					userStartOTLongPause();
				}
				else if( timer >= USER_TIMEOUT_LIGHT_MID )
				{
					SetBacklight( LCD_BACKLIGHT_MID );
				}
			}
			
			// check for alarm recovery
			if( (TIPO_ALLARME == NO_ALARM) && (STATO_STUFA != STUFASTAT_ALLARME) )
			{
				ExecUnlock();
				
				RetVal = 1;
				loop = 0;	// exit
			}
		}
		else if( (pt->tag == TAG_QUIT) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			cntExit4Alarm = 0;
			loop = 0;	// exit
		}
		else
		{
			cntExit4Alarm = 0;
			// full LCD backlight
			SetBacklight( LCD_BACKLIGHT_MAX );
			starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
			// item
			tagval = Read_Keypad( pt );	// read the keys
			if( tagval )
			{
				// touched
				if( Flag.Key_Detect )
				{
					// first touch
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
				/* else if( ++key_cnt >= VALID_TOUCHED_KEY )
				{
					// pressed item
					key_cnt = VALID_TOUCHED_KEY;
				} */
				else if( ++key_cnt >= 100 )
				{
					// pressed item
					if( (tagval == TAG_ON_OFF_PLAY) && !SBLOCCO_PAN )
					{
						// buzzer off
						led_Set( BUZ, LEDSTAT_OFF );
						// Si invia il comando per fermare il buzzer sulla scheda.
						userParValue.b[0] = 1;
						userSetPar( P_ALARM_SOUND );
						// send UNLOCK command
						SBLOCCO_PAN = 1;
						sPkt.pktLen = 0;
						sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
						sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
						sPkt.pkt[sPkt.pktLen++] = MB_FUNC_UNLOCK;	// cmd
						OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );

						// unlock wait time
						Ft_Gpu_CoCmd_Dlstart(pHost);
						Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
						Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
						Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0,0xFF,0));	// GREEN
						Ft_Gpu_CoCmd_Spinner(pHost, (FT_DispWidth/2),(FT_DispHeight/2),0,1);
						Ft_App_Flush_Co_Buffer( pHost );
						Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

						// wait for the unlock result with 20s timeout
						for( i=0; i<USER_TIMEOUT_SHOW_20; i++ )
						{
							Wdog();
							Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
							// flush the touch queue
							FlushTouchQueue();
							// check for alarm recovery
							if( (TIPO_ALLARME == NO_ALARM) && (STATO_STUFA != STUFASTAT_ALLARME) )
							{
								ExecUnlock();
								
								RetVal = 1;
								loop = 0;	// exit
								break;
							}
						}
						SBLOCCO_PAN = 0;

						// Send the stop command
						Ft_Gpu_Hal_WrCmd32(pHost, CMD_STOP);
						Ft_Gpu_Hal_WaitCmdfifo_empty(pHost);
						
						// restart window
						tagval = TOUCH_TAG_INIT;
					}
				}
			}
			else if( pt->tag )
			{
				// untouched
				if( key_cnt >= VALID_TOUCHED_KEY )
				{
					// flush the touch queue
					FlushTouchQueue();

					// process the pressed item on touch release
					switch( pt->tag )
					{
						default:
							// restart window
							tagval = TOUCH_TAG_INIT;
						break;
						
						case TAG_MUTE:
							// buzzer off
							led_Set( BUZ, LEDSTAT_OFF );
							// Si invia il comando per fermare il buzzer sulla scheda.
							userParValue.b[0] = 1;
							userSetPar( P_ALARM_SOUND );
						break;
					}
				}
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}
	}

	// pop bitmap MUTE from Graphic RAM
	res_bm_Release( BITMAP_MUTE );
	// pop bitmap ALARM from Graphic RAM
	res_bm_Release( BITMAP_ALARM );
	// pop bitmap PLAY from Graphic RAM
	res_bm_Release( BITMAP_PLAY );
	
	return RetVal;
}

/*!
** \fn unsigned char userStandby( void )
** \brief Handler function for the USER_STATE_STANDBY User Interface states handling
** \return The next User Interface state
**/
unsigned char userStandby( void )
{
	unsigned char RetVal = USER_STATE_STANDBY;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdr;
#ifdef EN_WALLPAPER
	BM_HEADER * p_bmhdrLOGO_c0 = NULL;	// logo bitmap can be not valid
	volatile BM_HEADER * p_bmhdrLOGO_c1;
	BM_HEADER * p_bmhdrLOGO_b0;
	volatile BM_HEADER * p_bmhdrLOGO_b1;
#else
	BM_HEADER * p_bmhdrLOGO = NULL;	// logo bitmap can be not valid
#endif
	BM_HEADER * p_bmhdrROOM_TEMP;
	BM_HEADER * p_bmhdrSTOVE_TYPE;
	unsigned char scrollON = 0;
	unsigned short preXscroll = 0;
	short Dx = 0;
	unsigned char key_cnt = 0;
	unsigned long timer;
	unsigned char loop = 1;
	char s[DISPLAY_MAX_COL];
	int i;
	
	// push bitmap BITMAP_ROOM_TEMP into Graphic RAM
	if( res_bm_Load( BITMAP_ROOM_TEMP, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_ROOM_TEMP);
	#endif
	}
	p_bmhdrROOM_TEMP = res_bm_GetHeader( BITMAP_ROOM_TEMP );
	
	// push bitmap STOVE_TYPE into Graphic RAM
	if( res_bm_Load( BITMAP_STOVE_TYPE, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_STOVE_TYPE);
	#endif
	}
	p_bmhdrSTOVE_TYPE = res_bm_GetHeader( BITMAP_STOVE_TYPE );
	
	// push bitmap LOGO into Graphic RAM
#ifdef EN_WALLPAPER
	if( res_bm_Load( BITMAP_LOGO_c0, res_bm_GetGRAM() ) > 0 )
	{
		if( res_bm_Load( BITMAP_LOGO_c1, res_bm_GetGRAM() ) > 0 )
		{
			if( res_bm_Load( BITMAP_LOGO_b0, res_bm_GetGRAM() ) > 0 )
			{
				if( res_bm_Load( BITMAP_LOGO_b1, res_bm_GetGRAM() ) > 0 )
				{
					p_bmhdrLOGO_c0 = res_bm_GetHeader( BITMAP_LOGO_c0 );
					p_bmhdrLOGO_c1 = res_bm_GetHeader( BITMAP_LOGO_c1 );
					p_bmhdrLOGO_b0 = res_bm_GetHeader( BITMAP_LOGO_b0 );
					p_bmhdrLOGO_b1 = res_bm_GetHeader( BITMAP_LOGO_b1 );
				}
				else
				{
					// invalid logo bitmap
					p_bmhdrLOGO_c0 = NULL;
				#ifdef LOGGING
					myprintf( sBitmapLoadError, BITMAP_LOGO_b1 );
				#endif
				}
			}
			else
			{
				// invalid logo bitmap
				p_bmhdrLOGO_c0 = NULL;
			#ifdef LOGGING
				myprintf( sBitmapLoadError, BITMAP_LOGO_b0 );
			#endif
			}
		}
		else
		{
			// invalid logo bitmap
			p_bmhdrLOGO_c0 = NULL;
		#ifdef LOGGING
			myprintf( sBitmapLoadError, BITMAP_LOGO_c1 );
		#endif
		}
	}
	else
	{
		// invalid logo bitmap
		p_bmhdrLOGO_c0 = NULL;
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_LOGO_c0 );
	#endif
	}
#else
	if( res_bm_Load( BITMAP_LOGO, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_LOGO );
	#endif
	}
	else
	{
		p_bmhdrLOGO = res_bm_GetHeader( BITMAP_LOGO );
		if( (p_bmhdrLOGO->bmhdr.Width < BITMAP_LOGO_MIN_WIDTH)
			|| (p_bmhdrLOGO->bmhdr.Height < BITMAP_LOGO_MIN_HEIGHT) )
		{
			// invalid logo bitmap
			p_bmhdrLOGO = NULL;
		}
	}
#endif

	if( fontMax == FONT_MAX_DEF )
	{
		// push bitmap BITMAP_DEG_F into Graphic RAM
		if( res_bm_Load( BITMAP_DEG_F, res_bm_GetGRAM() ) == 0 )
		{
		#ifdef LOGGING
			myprintf( sBitmapLoadError, BITMAP_DEG_F);
		#endif
		}
		// push bitmap BITMAP_DEG_C into Graphic RAM
		if( res_bm_Load( BITMAP_DEG_C, res_bm_GetGRAM() ) == 0 )
		{
		#ifdef LOGGING
			myprintf( sBitmapLoadError, BITMAP_DEG_C);
		#endif
		}
	}
	

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// reset all tags
			CTP_TagDeregisterAll();
			// full LCD backlight
			SetBacklight( LCD_BACKLIGHT_MAX );
			starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
		}
		
		if( !((tagval == TAG_HORIZ_SCROLL) && (scrollON == 1)) )
		{
			if( tagval == TOUCH_TAG_INIT )
			{
				// screen horizontal scroll tag
				TagRegister( TAG_HORIZ_SCROLL, 0, 0, FT_DispWidth, FT_DispHeight );
			}
			
			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
				
		#ifdef EN_WALLPAPER
			if( p_bmhdrLOGO_c0 != NULL )
			{
				// draw LOGO bitmap
				
				Ft_App_WrCoCmd_Buffer(pHost,SAVE_CONTEXT());
				
				//B0&B1 Handle
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_HANDLE(11));	// handle 0,..,(NFONT_PER_GROUP-1) are for app custom fonts, NFONT_PER_GROUP,..,14 are free
				// specify the starting address of the bitmap in graphics RAM
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrLOGO_b0->GramAddr));
				// specify the bitmap format, linestride and height
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrLOGO_b0->bmhdr.Format, p_bmhdrLOGO_b0->bmhdr.Stride, p_bmhdrLOGO_b0->bmhdr.Height));
				// set filtering, wrapping and on-screen size
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, FT_DispWidth, FT_DispHeight));
				
				//C0&C1 Handle
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_HANDLE(13));	// handle 0,..,(NFONT_PER_GROUP-1) are for app custom fonts, NFONT_PER_GROUP,..,14 are free
				// specify the starting address of the bitmap in graphics RAM
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrLOGO_c0->GramAddr));
				// specify the bitmap format, linestride and height
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrLOGO_c0->bmhdr.Format, p_bmhdrLOGO_c0->bmhdr.Stride, p_bmhdrLOGO_c0->bmhdr.Height));
				// set filtering, wrapping and on-screen size
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, FT_DispWidth, FT_DispHeight));	//scale to 480x272

				// start drawing bitmap
				Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
				
				Ft_App_WrCoCmd_Buffer(pHost,BLEND_FUNC(ONE,ZERO));
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(0x55));
				Ft_App_WrCoCmd_Buffer(pHost,VERTEX2II(0, 0, 11, 0));
				Ft_App_WrCoCmd_Buffer(pHost,BLEND_FUNC(ONE,ONE));
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(0xAA));
				Ft_App_WrCoCmd_Buffer(pHost,VERTEX2II(0, 0, 11, 1));
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_MASK(1,1,1,0));
				Ft_Gpu_CoCmd_LoadIdentity(pHost);
				Ft_Gpu_CoCmd_Scale(pHost, 65536*FT_DispWidth/p_bmhdrLOGO_c0->bmhdr.Width, 65536*FT_DispHeight/p_bmhdrLOGO_c0->bmhdr.Height);	//scale to 480x272
				Ft_Gpu_CoCmd_SetMatrix(pHost);
				Ft_App_WrCoCmd_Buffer(pHost,BLEND_FUNC(DST_ALPHA,ZERO));
				Ft_App_WrCoCmd_Buffer(pHost,VERTEX2II(0, 0, 13, 1));
				Ft_App_WrCoCmd_Buffer(pHost,BLEND_FUNC(ONE_MINUS_DST_ALPHA,ONE));
				Ft_App_WrCoCmd_Buffer(pHost,VERTEX2II(0, 0, 13, 0));
				
				Ft_App_WrCoCmd_Buffer( pHost, END() );
				
				Ft_App_WrCoCmd_Buffer(pHost,RESTORE_CONTEXT());
			}
		#else
			if( p_bmhdrLOGO != NULL )
			{
				// draw LOGO bitmap
				// specify the starting address of the bitmap in graphics RAM
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrLOGO->GramAddr));
				// specify the bitmap format, linestride and height
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrLOGO->bmhdr.Format, p_bmhdrLOGO->bmhdr.Stride, p_bmhdrLOGO->bmhdr.Height));
				// set filtering, wrapping and on-screen size
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrLOGO->bmhdr.Width, p_bmhdrLOGO->bmhdr.Height));
				// start drawing bitmap
				Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( ((FT_DispWidth-p_bmhdrLOGO->bmhdr.Width)/2)*16, (192-p_bmhdrLOGO->bmhdr.Height/2)*16 ) );
			}
		#endif
			else
			{
				// draw LOGO string
				memcpy( s, pChLogo, LENGTH_LOGO );
				s[LENGTH_LOGO] = '\0';
				Ft_Gpu_CoCmd_Text(pHost,(FT_DispWidth/2), 192, fontMax, OPT_CENTERY, String2Font(s, fontMax));
			}

			// draw DATE-TIME
		#ifdef EN_WALLPAPER
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			/*
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(255));
			Ft_App_WrCoCmd_Buffer(pHost, BLEND_FUNC( SRC_ALPHA, SRC_ALPHA ) );	// overlay on background image
			*/
		#else
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		#endif
			GetMsg( 0, s, iLanguage, sWeekDay_tab[DateTime.tm_wday] );
			//s[4] = '\0';	// only the first 3 characters for week day
			strcat( s, " " );
			ScriviOra( &s[strlen(s)] );
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 17, fontTitle, OPT_CENTER, String2Font(s, fontTitle) );
			ScriviData( s );
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 47, fontTitle, OPT_CENTER, String2Font(s, fontTitle) );

			if( fontMax == FONT_MAX_DEF )
			{
				/* draw DEG bitmap */
			#ifdef EN_WALLPAPER
				//Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0x00));	// YELLOW (the same color of temperature value text)
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			#else
				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0x00));	// YELLOW (the same color of temperature value text)
			#endif
				if( TEMPFAHRENHEIT )
				{
					p_bmhdr = res_bm_GetHeader( BITMAP_DEG_F );
				}
				else
				{
					p_bmhdr = res_bm_GetHeader( BITMAP_DEG_C );
				}
				// specify the starting address of the bitmap in graphics RAM
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdr->GramAddr));
				// specify the bitmap format, linestride and height
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdr->bmhdr.Format, p_bmhdr->bmhdr.Stride, p_bmhdr->bmhdr.Height));
				// set filtering, wrapping and on-screen size
				//Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdr->bmhdr.Width, p_bmhdr->bmhdr.Height));
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, 40, 40));	//scale to 40x40
				// start drawing bitmap
				Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
				Ft_Gpu_CoCmd_LoadIdentity(pHost);
				Ft_Gpu_CoCmd_Scale(pHost, 65536*40L/p_bmhdr->bmhdr.Width, 65536*40L/p_bmhdr->bmhdr.Height);	//scale to 40x40
				Ft_Gpu_CoCmd_SetMatrix(pHost );
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 248*16, 71*16 ) );

				Ft_Gpu_CoCmd_LoadIdentity(pHost);
				Ft_Gpu_CoCmd_Scale(pHost, 65536, 65536);	//scale to normal
				Ft_Gpu_CoCmd_SetMatrix(pHost );
			}
				
			// draw TEMPERATURE
			userVisTemp( s );
		#ifdef EN_WALLPAPER
			//Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0x00));	// YELLOW
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		#else
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0x00));	// YELLOW
		#endif
			Ft_Gpu_CoCmd_Text( pHost, 
									((fontMax == FONT_MAX_DEF) ? 244 : 288 ),
									96, fontMax, OPT_CENTERY|OPT_RIGHTX, String2Font(s, fontMax) );

			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE

		#ifdef EN_WALLPAPER
			//Ft_App_WrCoCmd_Buffer(pHost, BLEND_FUNC( SRC_ALPHA, ONE_MINUS_SRC_ALPHA ) );	// default blend
		#endif

			// draw AIR bitmap
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrROOM_TEMP->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrROOM_TEMP->bmhdr.Format, p_bmhdrROOM_TEMP->bmhdr.Stride, p_bmhdrROOM_TEMP->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrROOM_TEMP->bmhdr.Width, p_bmhdrROOM_TEMP->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 300*16, 64*16 ) );
			
			if( VIS_TIPOSTUFA && (STATO_STUFA == STUFASTAT_SPENTA) )
			{
				// draw STOVE_TYPE bitmap
				if( (tagval == TOUCH_TAG_INIT) || !CTP_SetTag( TAG_STOVE_TYPE, 1 ) )
				{
					// STOVE_TYPE bitmap tag
					TagRegister( TAG_STOVE_TYPE, 0, 0, p_bmhdrSTOVE_TYPE->bmhdr.Width, p_bmhdrSTOVE_TYPE->bmhdr.Height );
				}
				if( (tagval == TAG_STOVE_TYPE) && (key_cnt >= VALID_TOUCHED_KEY) )
				{
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0x00,0x00,0xFF));	// BLUE
				}
				// specify the starting address of the bitmap in graphics RAM
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrSTOVE_TYPE->GramAddr));
				// specify the bitmap format, linestride and height
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrSTOVE_TYPE->bmhdr.Format, p_bmhdrSTOVE_TYPE->bmhdr.Stride, p_bmhdrSTOVE_TYPE->bmhdr.Height));
				// set filtering, wrapping and on-screen size
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrSTOVE_TYPE->bmhdr.Width, p_bmhdrSTOVE_TYPE->bmhdr.Height));
				// start drawing bitmap
				Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, 0*16 ) );

				Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			}
			else
			{
				// disable tag
				CTP_SetTag( TAG_STOVE_TYPE, 0 );
			}

			Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
			Ft_Gpu_CoCmd_Swap( pHost );
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
				
		#ifdef SCREENSHOT
			if( IsGenScreeshot() &&
				VIS_TIPOSTUFA && (STATO_STUFA == STUFASTAT_SPENTA)	// STOVE_TYPE bitmap enabled
				)
			{
				// screenshot
				DoScreenshot( SSHOT_Standby );
			}
		#endif	// SCREENSHOT
		}
		
		// standby window loop
		do
		{
			Wdog();
			ALIM_INP = DIGIN_ALIM;
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );
			
			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
				tagval = pt->tag;
			}
		} while( event.status == osOK );
		
		// eventuale fine beep buzzer
		if( checktimer( T_BUZZER ) != INCORSO )
		{
			// buzzer off
			led_Set( BUZ, LEDSTAT_OFF );
		}

		// connessione alimentazione esterna
		if( !ALIM_INP && DIGIN_ALIM )
		{
			// full LCD backlight
			SetBacklight( LCD_BACKLIGHT_MAX );
			starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
		}

		// event processing
		if( (i = Check4StatusChange()) != USER_MAX_STATES )
		{
			cntExit4Alarm = 0;
			// buzzer off
			led_Set( BUZ, LEDSTAT_OFF );
			/*
			 * Richiesta di pausa standby (rilascio pulsante) dopo timeout
			 * utente, con alimentazione esterna assente, ma con comunicazione
			 * OT/+ ancora attiva:
			 * => Riaccende solo il backlight display.
			 */
			if( (i == USER_STATE_PAUSE) && (UserRequest == USER_REQ_STANDBY)
				&& !DIGIN_ALIM
				&& StatSec.BackLightTime
				&& (readtimer( TUSER100 ) == NOTIMER)
				&& (
					(PeekPkt( SEND_MODE_ONE_SHOT ) != NULL)
					|| (iLimReq < LIMITSREQ_NUM)
					)
				)
			{
				UserRequest = USER_REQ_NONE;
				// full LCD backlight
				SetBacklight( LCD_BACKLIGHT_MAX );
				starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
			}
			else
			{
				loop = 0;	// exit
			}
		}
		/*
		 * Evento touch dopo timeout utente,
		 * con alimentazione esterna assente, ma con comunicazione
		 * OT/+ ancora attiva:
		 * => Non considerare, riaccensione backlight solo da rilascio pulsante.
		 */
		else if( (event.status == osEventMessage)
				&& !DIGIN_ALIM
				&& StatSec.BackLightTime
				&& (readtimer( TUSER100 ) == NOTIMER)
				&& (
					(PeekPkt( SEND_MODE_ONE_SHOT ) != NULL)
					|| (iLimReq < LIMITSREQ_NUM)
					)
				)
		{
			// flush the touch queue
			FlushTouchQueue();
			// tag reset
			tagval = 0;
			scrollON = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			scrollON = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			scrollON = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;

			// LCD backlight
			if( StatSec.BackLightTime )
			{
				timer = readtimer( TUSER100 );
				if( timer == NOTIMER )
				{
					if( DIGIN_ALIM )
					{
						SetBacklight( LCD_BACKLIGHT_MIN );
					}
					else
					{
						SetBacklight( 0 );
						// se alimentazione esterna assente,
						// comando la pausa lunga solo se non ci sono richieste pendenti
						if( (PeekPkt( SEND_MODE_ONE_SHOT ) == NULL)
							&& (iLimReq >= LIMITSREQ_NUM)
							)
						{
							userStartOTLongPause();
						}
					}
				}
				else if( timer >= USER_TIMEOUT_LIGHT_MID )
				{
					SetBacklight( LCD_BACKLIGHT_MID );
				}

				// check alarms
				if( (TIPO_ALLARME != NO_ALARM) || (STATO_STUFA == STUFASTAT_ALLARME) )
				{
					// return to the main screen
					RetVal = userVisAvvio();
					//if( (RetVal = userVisAvvio() ) == USER_STATE_AVVIO )
					{
						loop = 0;	// exit
					}
				}
			}
		}
		else if( tagval == TAG_HORIZ_SCROLL )
		//else if( pt->tag == TAG_HORIZ_SCROLL )
		{
			// full LCD backlight
			SetBacklight( LCD_BACKLIGHT_MAX );
			starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
			// screen horizontal scroll
			if( pt->status )
			{
				// touched
				switch( scrollON )
				{
				default:
				case 0:
					preXscroll = pt->x;
					scrollON = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dx = pt->x - preXscroll;
						//if( Dx > 5 )
						if( Dx > 100 )
						{
							scrollON = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll left
							
							preXscroll = pt->x;
						}
						//else if( Dx < -5 )
						else if( Dx < -100 )
						{
							scrollON = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll right
							
							// return to the main screen
							RetVal = userVisAvvio();
							//if( (RetVal = userVisAvvio() ) == USER_STATE_AVVIO )
							{
								loop = 0;	// exit
							}
							
							preXscroll = pt->x;
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON = 0;
			}
		}
		else if( scrollON != 2 )	// tagval > 0 without scroll
		{
			// full LCD backlight
			SetBacklight( LCD_BACKLIGHT_MAX );
			starttimer( TUSER100, USER_TIMEOUT_LIGHT_OFF );
			// item
			tagval = Read_Keypad( pt );	// read the keys
			if( tagval )
			{
				// touched
				if( Flag.Key_Detect )
				{
					// first touch
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
				else if( ++key_cnt >= VALID_TOUCHED_KEY )
				{
					// pressed item
					key_cnt = VALID_TOUCHED_KEY;
				}
				/* else if( ++key_cnt >= 100 )
				{
					// pressed item
					switch( tagval )
					{
					}
				} */
			}
			else if( pt->tag )
			{
				// untouched
				if( key_cnt >= VALID_TOUCHED_KEY )
				{
					// flush the touch queue
					FlushTouchQueue();

					// process the pressed item on touch release
					switch( pt->tag )
					{
						default:
							// restart window
							tagval = TOUCH_TAG_INIT;
						break;
						
						case TAG_STOVE_TYPE:
							if( VIS_TIPOSTUFA && (STATO_STUFA == STUFASTAT_SPENTA) )
							{
								// set Tipo Stufa
								userSubState = 0;	// parameter view
								// parameter code as title
								userGetParCode( P_TIPOSTUFA );
								strcpy( s, str2 );
								userGetPar( P_TIPOSTUFA );
								if( WinSetParam(		// oppure WinSetList() ????
														P_TIPOSTUFA,
														s,
														(char *)GetMsg( 2, NULL, iLanguage, d_TIPOSTUFA )
														) )
								{
									ExecTipoStufa();
									STATO_ALL_BLACK = PAN_ONLINE;
								}
								// restart window
								tagval = TOUCH_TAG_INIT;
							}
						break;
					}
				}
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}
		else
		{
			// tagval > 0 with scroll
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		
	}

	if( fontMax == FONT_MAX_DEF )
	{
		// pop bitmap BITMAP_DEG_C from Graphic RAM
		res_bm_Release( BITMAP_DEG_C );
		// pop bitmap BITMAP_DEG_F from Graphic RAM
		res_bm_Release( BITMAP_DEG_F );
	}
	// pop bitmap LOGO from Graphic RAM
#ifdef EN_WALLPAPER
	res_bm_Release( BITMAP_LOGO_b1 );
	res_bm_Release( BITMAP_LOGO_b0 );
	res_bm_Release( BITMAP_LOGO_c1 );
	res_bm_Release( BITMAP_LOGO_c0 );
#else
	res_bm_Release( BITMAP_LOGO );
#endif
	// pop bitmap STOVE_TYPE from Graphic RAM
	res_bm_Release( BITMAP_STOVE_TYPE );
	// pop bitmap BITMAP_ROOM_TEMP from Graphic RAM
	res_bm_Release( BITMAP_ROOM_TEMP );

	return RetVal;
}

/*!
** \fn short WinSetPower( char *sTitle, unsigned short iPar )
** \brief SET POWER window handler
** \param sTitle The window's title string
** \param iPar Parameter index
**	\return 1: SET, 0: QUIT
**/
short WinSetPower( char *sTitle, unsigned short iPar )
{
	short RetVal = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	BM_HEADER * p_bmhdrFLAMES;
	ft_uint16_t But_opt;
	unsigned char loop = 1;
	long l;
	char s[20];
	BYTE_UNION vMax;
	BYTE_UNION vMin;
	unsigned char step;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );
	// get bitmap FLAMES into Graphic RAM
	p_bmhdrFLAMES = res_bm_GetHeader( BITMAP_FLAMES );
	

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
			// recupera valore e limiti parametro
			userReqPar( iPar );
			userGetMinMaxStep( iPar, &vMin, &vMax, &step );
		}

		Ft_Gpu_CoCmd_Dlstart(pHost);
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		/* Draw horizontal slider bar */	
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));
		Ft_Gpu_CoCmd_FgColor(pHost,Get_UserColorHex(UserColor_tab[UserColorId].addon));
		Ft_Gpu_CoCmd_BgColor(pHost, 0xFFFFFF);
		if( tagval == TOUCH_TAG_INIT )
		{
			// a larger area for the touch
			TagRegister( TAG_HSCROLL_BASE, 
							30+p_bmhdrFLAMES->bmhdr.Width+20-5, 
							87+p_bmhdrFLAMES->bmhdr.Height-30, 
							FT_DispWidth-(30+p_bmhdrFLAMES->bmhdr.Width+20+30)+2*5, 
							30 );
		}
		But_opt = ( (tagval == TAG_HSCROLL_BASE) && pt->status ) ? OPT_FLAT : 0;
		l = 1000*(userParValue.b[0] - vMin.b[0]);
		l /= (vMax.b[0] - vMin.b[0]);
		if( l > 0 )
			l = (l + 5)/10;
		else
			l = (l - 5)/10;
		Ft_Gpu_CoCmd_Slider( pHost,
							30+p_bmhdrFLAMES->bmhdr.Width+20, 
							87+p_bmhdrFLAMES->bmhdr.Height-10, 
							FT_DispWidth-(30+p_bmhdrFLAMES->bmhdr.Width+20+30), 
							10, 
							But_opt, 
							(ft_uint16_t)l, 
							100 );

		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		// draw FLAMES bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrFLAMES->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrFLAMES->bmhdr.Format, p_bmhdrFLAMES->bmhdr.Stride, p_bmhdrFLAMES->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrFLAMES->bmhdr.Width, p_bmhdrFLAMES->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 30*16, 87*16 ) );
		
		// draw QUIT bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 128*16, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height)*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// QUIT bitmap tag
			TagRegister( TAG_QUIT, 128, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height), p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
		}
		
		// draw OK bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-128-p_bmhdrOK->bmhdr.Width)*16, (FT_DispHeight-p_bmhdrOK->bmhdr.Height)*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// OK bitmap tag
			TagRegister( TAG_OK, (FT_DispWidth-128-p_bmhdrOK->bmhdr.Width), (FT_DispHeight-p_bmhdrOK->bmhdr.Height), p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height );
		}
			
		// title
		Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
		Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );
		
		// draw the power level
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		mysprintf( s, "%d", userParValue.b[0] );
		Ft_Gpu_CoCmd_Text( pHost, 
							30+p_bmhdrFLAMES->bmhdr.Width+20 + (FT_DispWidth-(30+p_bmhdrFLAMES->bmhdr.Width+20+30))/2, 
							87+40/2, 
							fontTitle, OPT_CENTER, String2Font(s, fontTitle) );

		Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
		Ft_Gpu_CoCmd_Swap( pHost );
		Ft_App_Flush_Co_Buffer( pHost );
		Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() )
		{
			// screenshot
			DoScreenshot( SSHOT_SetFire );
		}
	#endif	// SCREENSHOT

		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
				tagval = pt->tag;
			}
		} while( event.status == osOK );
		
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			Flag.Key_Detect = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					if( userSubState == 1 )
					{
						// modifica
						// ???
					}
					loop = 0;	// exit
				}
				else
				{
					if( userSubState == 1 )
					{
						// modifica
						// ???
					}
					else
					{
						// visualizzazione
						if( !(userCounterTime % USER_TIMEOUT_SHOW_1) )
						{
							// recupera valore e limiti parametro
							userReqPar( iPar );
							if( iPar == P_POWER )
							{
								userGetPar( iPar );
							}
							userGetMinMaxStep( iPar, &vMin, &vMax, &step );
						}
					}
				}
			}
		}
		else if( (tagval == TAG_HSCROLL_BASE) && pt->status )
		{
			userCounterTime = USER_TIMEOUT_SHOW_60;
			userSubState = 1;	// parameter modify
			// horizontal slider touch
			l = 10*(pt->x - (30+p_bmhdrFLAMES->bmhdr.Width+20));
			if( l <= 0 )
			{
				l = 0;
				userParValue.b[0] = vMin.b[0];
			}
			else
			{
				l = (l * (vMax.b[0] - vMin.b[0]))/(FT_DispWidth-(30+p_bmhdrFLAMES->bmhdr.Width+20+30));
				l = (l + 5)/10;
				userParValue.b[0] = vMin.b[0] + l;
				if( userParValue.b[0] > vMax.b[0] )
					userParValue.b[0] = vMax.b[0];
			}
		}
		else if( ((pt->tag == TAG_QUIT) || (pt->tag == TAG_OK)) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			if( (pt->tag == TAG_OK) && (userSubState == 1) )
			{
				RetVal = 1;
			}
			
			loop = 0;	// exit
		}
		else
		{
			tagval = 0;
		}
		
		
	}

	return RetVal;
}

/*!
** \fn short WinSetTemp( char *sTitle, unsigned short iPar )
** \brief SET TEMP window handler
** \param sTitle The window's title string
** \param iPar Parameter index
** \return 1: SET, 0: QUIT
**/
short WinSetTemp( char *sTitle, unsigned short iPar )
{
	short RetVal = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	BM_HEADER * p_bmhdrROOM_TEMP;
	ft_uint16_t But_opt;
	unsigned char loop = 1;
	long l;
	char s[20];
	BYTE_UNION vMax;
	BYTE_UNION vMin;
	unsigned char step;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );
	if( iPar == P_CRONO_P1_SETTIM_TEMP_H2O )
	{
		// get bitmap WATER_TEMP into Graphic RAM
		p_bmhdrROOM_TEMP = res_bm_GetHeader( BITMAP_WATER_TEMP );
	}
	else
	{
		// get bitmap ROOM_TEMP into Graphic RAM
		p_bmhdrROOM_TEMP = res_bm_GetHeader( BITMAP_ROOM_TEMP );
	}
	

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
			// recupera valore e limiti parametro
			userReqPar( iPar );
			userGetMinMaxStep( iPar, &vMin, &vMax, &step );
		}

		Ft_Gpu_CoCmd_Dlstart(pHost);
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		/* Draw horizontal slider bar */	
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));
		Ft_Gpu_CoCmd_FgColor(pHost,Get_UserColorHex(UserColor_tab[UserColorId].addon));
		Ft_Gpu_CoCmd_BgColor(pHost, 0XFFFFFF);
		if( tagval == TOUCH_TAG_INIT )
		{
			// a larger area for the touch
			TagRegister( TAG_HSCROLL_BASE, 
							30+p_bmhdrROOM_TEMP->bmhdr.Width+20-5, 
							87+p_bmhdrROOM_TEMP->bmhdr.Height-30, 
							FT_DispWidth-(30+p_bmhdrROOM_TEMP->bmhdr.Width+20+30)+2*5, 
							30 );
		}
		But_opt = ( (tagval == TAG_HSCROLL_BASE) && pt->status ) ? OPT_FLAT : 0;
		l = 1000*(userParValue.b[0] - vMin.b[0]);
		l /= (vMax.b[0] - vMin.b[0]);
		if( l > 0 )
			l = (l + 5)/10;
		else
			l = (l - 5)/10;
		Ft_Gpu_CoCmd_Slider( pHost,
							30+p_bmhdrROOM_TEMP->bmhdr.Width+20, 
							87+p_bmhdrROOM_TEMP->bmhdr.Height-10, 
							FT_DispWidth-(30+p_bmhdrROOM_TEMP->bmhdr.Width+20+30), 
							10, 
							But_opt, 
							(ft_uint16_t)l, 
							100 );

		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		// draw ROOM_TEMP bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrROOM_TEMP->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrROOM_TEMP->bmhdr.Format, p_bmhdrROOM_TEMP->bmhdr.Stride, p_bmhdrROOM_TEMP->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrROOM_TEMP->bmhdr.Width, p_bmhdrROOM_TEMP->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 30*16, 87*16 ) );
		
		// draw QUIT bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 128*16, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height)*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// QUIT bitmap tag
			TagRegister( TAG_QUIT, 128, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height), p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
		}
		
		// draw OK bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-128-p_bmhdrOK->bmhdr.Width)*16, (FT_DispHeight-p_bmhdrOK->bmhdr.Height)*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// OK bitmap tag
			TagRegister( TAG_OK, (FT_DispWidth-128-p_bmhdrOK->bmhdr.Width), (FT_DispHeight-p_bmhdrOK->bmhdr.Height), p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height );
		}
			
		// title
		Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
		Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );
		
		// draw the temperature value
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		if( TEMPFAHRENHEIT )
		{
			// Converto la temperatura in Fahrenheit
			l = (long)userParValue.b[0] * 18;	// in decimi di grado
			l += 320;
			l += 5;	// arrotondamento
			l /= 10;	// in gradi
		}
		else
		{
			l = userParValue.b[0];
		}
		mysprintf( s, "%d%c%c", l, cDEG, (TEMPFAHRENHEIT ? 'F' : 'C') );
		Ft_Gpu_CoCmd_Text( pHost, 
							30+p_bmhdrROOM_TEMP->bmhdr.Width+20 + (FT_DispWidth-(30+p_bmhdrROOM_TEMP->bmhdr.Width+20+30))/2, 
							87+40/2, 
							fontTitle, OPT_CENTER, String2Font(s, fontTitle) );

		Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
		Ft_Gpu_CoCmd_Swap( pHost );
		Ft_App_Flush_Co_Buffer( pHost );
		Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() )
		{
			// screenshot
			DoScreenshot( SSHOT_SetTemp );
		}
	#endif	// SCREENSHOT

		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
				tagval = pt->tag;
			}
		} while( event.status == osOK );
		
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			Flag.Key_Detect = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					if( userSubState == 1 )
					{
						// modifica
						// ???
					}
					loop = 0;	// exit
				}
				else
				{
					if( userSubState == 1 )
					{
						// modifica
						// ???
					}
					else
					{
						// visualizzazione
						if( !(userCounterTime % USER_TIMEOUT_SHOW_1) )
						{
							// recupera valore e limiti parametro
							userReqPar( iPar );
							if( iPar == P_TEMPERATURE )
							{
								userGetPar( iPar );
							}
							userGetMinMaxStep( iPar, &vMin, &vMax, &step );
						}
					}
				}
			}
		}
		else if( (tagval == TAG_HSCROLL_BASE) && pt->status )
		{
			userCounterTime = USER_TIMEOUT_SHOW_60;
			userSubState = 1;	// parameter modify
			// horizontal slider touch
			l = 10*(pt->x - (30+p_bmhdrROOM_TEMP->bmhdr.Width+20));
			if( l <= 0 )
			{
				l = 0;
				userParValue.b[0] = vMin.b[0];
			}
			else
			{
				l = (l * (vMax.b[0] - vMin.b[0]))/(FT_DispWidth-(30+p_bmhdrROOM_TEMP->bmhdr.Width+20+30));
				l = (l + 5)/10;
				userParValue.b[0] = vMin.b[0] + l;
				if( userParValue.b[0] > vMax.b[0] )
					userParValue.b[0] = vMax.b[0];
			}
		}
		else if( ((pt->tag == TAG_QUIT) || (pt->tag == TAG_OK)) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			if( (pt->tag == TAG_OK) && (userSubState == 1) )
			{
				RetVal = 1;
			}
			
			loop = 0;	// exit
		}
		else
		{
			tagval = 0;
		}
		
		
	}

	return RetVal;
}

/*!
** \fn short WinSetIdro( char *sTitle )
** \brief Set IDRO temperatures window handler
** \param sTitle The window's title string
** \return 1: SET, 0: QUIT
**/
static short WinSetIdro( char *sTitle )
{
	short RetVal = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	BM_HEADER * p_bmhdrTAP;
	BM_HEADER * p_bmhdrRADIATOR;
	ft_uint16_t But_opt;
	unsigned char loop = 1;
	unsigned char valTRisc = 0;
	unsigned char valTSan = 0;
	long l;
	char s[20];
	unsigned short iParTRisc = P_SET_TRISC;
	BYTE_UNION vMaxTRisc;
	BYTE_UNION vMinTRisc;
	unsigned char stepTRisc;
	unsigned short iParTSan = P_SET_TSANIT;
	BYTE_UNION vMaxTSan;
	BYTE_UNION vMinTSan;
	unsigned char stepTSan;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );
	// get bitmap TAP into Graphic RAM
	p_bmhdrTAP = res_bm_GetHeader( BITMAP_TAP );
	
	// push bitmap RADIATOR into Graphic RAM
	if( res_bm_Load( BITMAP_RADIATOR, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_RADIATOR );
	#endif
		return RetVal;
	}
	p_bmhdrRADIATOR = res_bm_GetHeader( BITMAP_RADIATOR );
	

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
			// recupera valore e limiti parametro
			userReqPar( iParTRisc );
			userGetPar( iParTRisc );
			valTRisc = userParValue.b[0];
			userGetMinMaxStep( iParTRisc, &vMinTRisc, &vMaxTRisc, &stepTRisc );
			if( VIS_SANITARI )
			{
				userReqPar( iParTSan );
				userGetPar( iParTSan );
				valTSan = userParValue.b[0];
				userGetMinMaxStep( iParTSan, &vMinTSan, &vMaxTSan, &stepTSan );
			}
		}

		Ft_Gpu_CoCmd_Dlstart(pHost);
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		/* Draw TRISC horizontal slider bar */	
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));
		Ft_Gpu_CoCmd_FgColor(pHost,Get_UserColorHex(UserColor_tab[UserColorId].addon));
		Ft_Gpu_CoCmd_BgColor(pHost, 0xFFFFFF);
		if( tagval == TOUCH_TAG_INIT )
		{
			// a larger area for the touch
			TagRegister( TAG_HSCROLL_BASE, 
							30+p_bmhdrRADIATOR->bmhdr.Width+20-5, 
							52+p_bmhdrRADIATOR->bmhdr.Height-40,
							FT_DispWidth-(30+p_bmhdrRADIATOR->bmhdr.Width+20+30)+2*5, 
							30 );
		}
		But_opt = ( (tagval == TAG_HSCROLL_BASE) && pt->status ) ? OPT_FLAT : 0;
		l = 1000*(valTRisc - vMinTRisc.b[0]);
		l /= (vMaxTRisc.b[0] - vMinTRisc.b[0]);
		if( l > 0 )
			l = (l + 5)/10;
		else
			l = (l - 5)/10;
		Ft_Gpu_CoCmd_Slider( pHost,
							30+p_bmhdrRADIATOR->bmhdr.Width+20, 
							52+p_bmhdrRADIATOR->bmhdr.Height-20,
							FT_DispWidth-(30+p_bmhdrRADIATOR->bmhdr.Width+20+30), 
							10, 
							But_opt, 
							(ft_uint16_t)l, 
							100 );
		
		if( VIS_SANITARI )
		{
			/* Draw TSAN horizontal slider bar */	
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));
			Ft_Gpu_CoCmd_FgColor(pHost,Get_UserColorHex(UserColor_tab[UserColorId].addon));
			Ft_Gpu_CoCmd_BgColor(pHost, 0xFFFFFF);
			if( tagval == TOUCH_TAG_INIT )
			{
				// a larger area for the touch
				TagRegister( TAG_HSCROLL_BASE+1, 
							30+p_bmhdrRADIATOR->bmhdr.Width+20-5, 
							52+p_bmhdrRADIATOR->bmhdr.Height+22+p_bmhdrTAP->bmhdr.Height-40,
							FT_DispWidth-(30+p_bmhdrRADIATOR->bmhdr.Width+20+30)+2*5, 
							30 );
			}
			But_opt = ( (tagval == TAG_HSCROLL_BASE+1) && pt->status ) ? OPT_FLAT : 0;
			l = 1000*(valTSan - vMinTSan.b[0]);
			l /= (vMaxTSan.b[0] - vMinTSan.b[0]);
			if( l > 0 )
				l = (l + 5)/10;
			else
				l = (l - 5)/10;
			Ft_Gpu_CoCmd_Slider( pHost,
								30+p_bmhdrRADIATOR->bmhdr.Width+20, 
								52+p_bmhdrRADIATOR->bmhdr.Height+22+p_bmhdrTAP->bmhdr.Height-20,
								FT_DispWidth-(30+p_bmhdrRADIATOR->bmhdr.Width+20+30), 
								10, 
								But_opt, 
								(ft_uint16_t)l, 
								100 );
		}

		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		// draw RADIATOR bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrRADIATOR->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrRADIATOR->bmhdr.Format, p_bmhdrRADIATOR->bmhdr.Stride, p_bmhdrRADIATOR->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrRADIATOR->bmhdr.Width, p_bmhdrRADIATOR->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 30*16, 52*16 ) );
		
		if( VIS_SANITARI )
		{
			// draw TAP bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrTAP->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrTAP->bmhdr.Format, p_bmhdrTAP->bmhdr.Stride, p_bmhdrTAP->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrTAP->bmhdr.Width, p_bmhdrTAP->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (30+18)*16, (52+p_bmhdrRADIATOR->bmhdr.Height+22)*16 ) );
		}
		
		// draw QUIT bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 128*16, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height)*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// QUIT bitmap tag
			TagRegister( TAG_QUIT, 128, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height), p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
		}
		
		// draw OK bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-128-p_bmhdrOK->bmhdr.Width)*16, (FT_DispHeight-p_bmhdrOK->bmhdr.Height)*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// OK bitmap tag
			TagRegister( TAG_OK, (FT_DispWidth-128-p_bmhdrOK->bmhdr.Width), (FT_DispHeight-p_bmhdrOK->bmhdr.Height), p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height );
		}
		
		// title
		Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
		Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );
		
		// draw the TRISC temperature value
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		if( TEMPFAHRENHEIT )
		{
			// Converto la temperatura in Fahrenheit
			l = (long)valTRisc * 18;	// in decimi di grado
			l += 320;
			l += 5;	// arrotondamento
			l /= 10;	// in gradi
		}
		else
		{
			l = valTRisc;
		}
		mysprintf( s, "%d%c%c", l, cDEG, (TEMPFAHRENHEIT ? 'F' : 'C') );
		Ft_Gpu_CoCmd_Text( pHost, 
							30+p_bmhdrRADIATOR->bmhdr.Width+20 + (FT_DispWidth-(30+p_bmhdrRADIATOR->bmhdr.Width+20+30))/2, 
							52+40/2, 
							fontTitle, OPT_CENTER, String2Font(s, fontTitle) );
		
		if( VIS_SANITARI )
		{
			// draw the TSAN temperature value
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			if( TEMPFAHRENHEIT )
			{
				// Converto la temperatura in Fahrenheit
				l = (long)valTSan * 18;	// in decimi di grado
				l += 320;
				l += 5;	// arrotondamento
				l /= 10;	// in gradi
			}
			else
			{
				l = valTSan;
			}
			mysprintf( s, "%d%c%c", l, cDEG, (TEMPFAHRENHEIT ? 'F' : 'C') );
			Ft_Gpu_CoCmd_Text( pHost, 
								30+p_bmhdrRADIATOR->bmhdr.Width+20 + (FT_DispWidth-(30+p_bmhdrRADIATOR->bmhdr.Width+20+30))/2, 
								52+p_bmhdrRADIATOR->bmhdr.Height+22+10,
								fontTitle, OPT_CENTER, String2Font(s, fontTitle) );
		}

		Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
		Ft_Gpu_CoCmd_Swap( pHost );
		Ft_App_Flush_Co_Buffer( pHost );
		Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() &&
			VIS_SANITARI	// DHW enabled
			)
		{
			// screenshot
			DoScreenshot( SSHOT_SetIdro );
		}
	#endif	// SCREENSHOT

		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
				tagval = pt->tag;
			}
		} while( event.status == osOK );
		
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			Flag.Key_Detect = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					if( userSubState == 1 )
					{
						// modifica
						// ???
					}
					loop = 0;	// exit
				}
				else
				{
					if( userSubState == 1 )
					{
						// modifica
						// ???
					}
					else
					{
						// visualizzazione
						if( !(userCounterTime % USER_TIMEOUT_SHOW_1) )
						{
							// recupera valore e limiti parametro
							userReqPar( iParTRisc );
							userGetPar( iParTRisc );
							valTRisc = userParValue.b[0];
							userGetMinMaxStep( iParTRisc, &vMinTRisc, &vMaxTRisc, &stepTRisc );
							if( VIS_SANITARI )
							{
								userReqPar( iParTSan );
								userGetPar( iParTSan );
								valTSan = userParValue.b[0];
								userGetMinMaxStep( iParTSan, &vMinTSan, &vMaxTSan, &stepTSan );
							}
						}
					}
				}
			}
		}
		else if( (tagval == TAG_HSCROLL_BASE) && pt->status )
		{
			userCounterTime = USER_TIMEOUT_SHOW_60;
			userSubState = 1;	// parameter modify
			// TRISC horizontal slider touch
			l = 10*(pt->x - (30+p_bmhdrRADIATOR->bmhdr.Width+20));
			if( l <= 0 )
			{
				l = 0;
				valTRisc = vMinTRisc.b[0];
			}
			else
			{
				l = (l * (vMaxTRisc.b[0] - vMinTRisc.b[0]))/(FT_DispWidth-(30+p_bmhdrRADIATOR->bmhdr.Width+20+30));
				l = (l + 5)/10;
				valTRisc = vMinTRisc.b[0] + l;
				if( valTRisc > vMaxTRisc.b[0] )
					valTRisc = vMaxTRisc.b[0];
			}
		}
		else if( VIS_SANITARI && (tagval == TAG_HSCROLL_BASE+1) && pt->status )
		{
			userCounterTime = USER_TIMEOUT_SHOW_60;
			userSubState = 1;	// parameter modify
			// TSAN horizontal slider touch
			l = 10*(pt->x - (30+p_bmhdrRADIATOR->bmhdr.Width+20));
			if( l <= 0 )
			{
				l = 0;
				valTSan = vMinTSan.b[0];
			}
			else
			{
				l = (l * (vMaxTSan.b[0] - vMinTSan.b[0]))/(FT_DispWidth-(30+p_bmhdrRADIATOR->bmhdr.Width+20+30));
				l = (l + 5)/10;
				valTSan = vMinTSan.b[0] + l;
				if( valTSan > vMaxTSan.b[0] )
					valTSan = vMaxTSan.b[0];
			}
		}
		else if( ((pt->tag == TAG_QUIT) || (pt->tag == TAG_OK)) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			if( (pt->tag == TAG_OK) && (userSubState == 1) )
			{
				userParValue.b[0] = valTRisc;
				userSetPar( P_SET_TRISC );
				if( VIS_SANITARI )
				{
					userParValue.b[0] = valTSan;
					userSetPar( P_SET_TSANIT );
				}
				RetVal = 1;	// set
			}
			
			loop = 0;	// exit
		}
		else
		{
			tagval = 0;
		}
		
		
	}

	// pop bitmap RADIATOR from Graphic RAM
	res_bm_Release( BITMAP_RADIATOR );
	
	return RetVal;
}

/*!
** \fn short WinSetFan( char *sTitle )
** \brief Set FAN window handler
** \param sTitle The window's title string
** \return 1: SET, 0: QUIT
**/
static short WinSetFan( char *sTitle )
{
	short RetVal = 0;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	BM_HEADER * p_bmhdrFAN1;
	BM_HEADER * p_bmhdrFAN2;
	ft_uint16_t But_opt;
	unsigned char loop = 1;
	unsigned char valFan1 = 0;
	unsigned char valFan2 = 0;
	long l;
	char s[10];
	unsigned short iParFan1 = P_FAN;
	BYTE_UNION vMaxFan1;
	BYTE_UNION vMinFan1;
	unsigned char stepFan1;
	unsigned short iParFan2 = P_FAN_2;
	BYTE_UNION vMaxFan2;
	BYTE_UNION vMinFan2;
	unsigned char stepFan2;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );
	
	// push bitmap FAN1 into Graphic RAM
	if( res_bm_Load( BITMAP_FAN1, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_FAN1 );
	#endif
		return RetVal;
	}
	p_bmhdrFAN1 = res_bm_GetHeader( BITMAP_FAN1 );

	// push bitmap FAN2 into Graphic RAM
	if( res_bm_Load( BITMAP_FAN2, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_FAN2 );
	#endif
		return RetVal;
	}
	p_bmhdrFAN2 = res_bm_GetHeader( BITMAP_FAN2 );

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
			// recupera valore e limiti parametro
			userReqPar( iParFan1 );
			userGetPar( iParFan1 );
			valFan1 = userParValue.b[0];
			userGetMinMaxStep( iParFan1, &vMinFan1, &vMaxFan1, &stepFan1 );
			if( F_MULTIFAN >= DOUBLE_FAN )
			{
				userReqPar( iParFan2 );
				userGetPar( iParFan2 );
				valFan2 = userParValue.b[0];
				userGetMinMaxStep( iParFan2, &vMinFan2, &vMaxFan2, &stepFan2 );
			}
		}

		Ft_Gpu_CoCmd_Dlstart(pHost);
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		// draw FAN1 bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrFAN1->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrFAN1->bmhdr.Format, p_bmhdrFAN1->bmhdr.Stride, p_bmhdrFAN1->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrFAN1->bmhdr.Width, p_bmhdrFAN1->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 30*16, 52*16 ) );
		/* // draw slider description
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		GetMsg( 0, s, iLanguage, sFAN1 );
		Ft_Gpu_CoCmd_Text( pHost, 1, 78, fontTitle, 0, String2Font(s, fontTitle) ); */

		/* Draw FAN1 horizontal slider bar */	
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));
		Ft_Gpu_CoCmd_FgColor(pHost,Get_UserColorHex(UserColor_tab[UserColorId].addon));
		Ft_Gpu_CoCmd_BgColor(pHost, 0xFFFFFF);
		if( tagval == TOUCH_TAG_INIT )
		{
			// a larger area for the touch
			TagRegister( TAG_HSCROLL_BASE, 
							30+p_bmhdrFAN1->bmhdr.Width+20-5, 
							52+p_bmhdrFAN1->bmhdr.Height-30, 
							FT_DispWidth-(30+p_bmhdrFAN1->bmhdr.Width+20+30)+2*5, 
							30 );
		}
		But_opt = ( (tagval == TAG_HSCROLL_BASE) && pt->status ) ? OPT_FLAT : 0;
		/* max + AUTO + TURBO
		l = 1000*(valFan1 - vMinFan1.b[0]);
		l /= (vMaxFan1.b[0]+1 - vMinFan1.b[0]);
		*/
		l = 1000*(valFan1 - vMinFan1.b[0]);
		l /= (vMaxFan1.b[0] - vMinFan1.b[0]);
		if( l > 0 )
			l = (l + 5)/10;
		else
			l = (l - 5)/10;
		Ft_Gpu_CoCmd_Slider( pHost,
							30+p_bmhdrFAN1->bmhdr.Width+20, 
							52+p_bmhdrFAN1->bmhdr.Height-10, 
							FT_DispWidth-(30+p_bmhdrFAN1->bmhdr.Width+20+30), 
							10, 
							But_opt, 
							(ft_uint16_t)l, 
							100 );
			
		if( F_MULTIFAN >= DOUBLE_FAN )
		{
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			// draw FAN2 bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrFAN2->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrFAN2->bmhdr.Format, p_bmhdrFAN2->bmhdr.Stride, p_bmhdrFAN2->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrFAN2->bmhdr.Width, p_bmhdrFAN2->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 30*16, (52+p_bmhdrFAN1->bmhdr.Height+22)*16 ) );
			/* // draw slider description
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			GetMsg( 0, s, iLanguage, sFAN2 );
			Ft_Gpu_CoCmd_Text( pHost, 1, (78+p_bmhdrFAN1->bmhdr.Height+22), fontTitle, 0, String2Font(s, fontTitle) ); */

			/* Draw FAN2 horizontal slider bar */	
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));
			Ft_Gpu_CoCmd_FgColor(pHost,Get_UserColorHex(UserColor_tab[UserColorId].addon));
			Ft_Gpu_CoCmd_BgColor(pHost, 0xFFFFFF);
			if( tagval == TOUCH_TAG_INIT )
			{
				// a larger area for the touch
				TagRegister( TAG_HSCROLL_BASE+1, 
								30+p_bmhdrFAN2->bmhdr.Width+20-5, 
								52+p_bmhdrFAN1->bmhdr.Height+22+p_bmhdrFAN2->bmhdr.Height-30, 
								FT_DispWidth-(30+p_bmhdrFAN2->bmhdr.Width+20+30)+2*5, 
								30 );
			}
			But_opt = ( (tagval == TAG_HSCROLL_BASE+1) && pt->status ) ? OPT_FLAT : 0;
			/* max + AUTO + TURBO
			l = 1000*(valFan2 - vMinFan2.b[0]);
			l /= (vMaxFan2.b[0]+1 - vMinFan2.b[0]);
			*/
			l = 1000*(valFan2 - vMinFan2.b[0]);
			l /= (vMaxFan2.b[0] - vMinFan2.b[0]);
			if( l > 0 )
				l = (l + 5)/10;
			else
				l = (l - 5)/10;
			Ft_Gpu_CoCmd_Slider( pHost,
								30+p_bmhdrFAN2->bmhdr.Width+20, 
								52+p_bmhdrFAN1->bmhdr.Height+22+p_bmhdrFAN2->bmhdr.Height-10, 
								FT_DispWidth-(30+p_bmhdrFAN2->bmhdr.Width+20+30), 
								10, 
								But_opt, 
								(ft_uint16_t)l, 
								100 );
		}

		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		// draw QUIT bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 128*16, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height)*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// QUIT bitmap tag
			TagRegister( TAG_QUIT, 128, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height), p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
		}
		
		// draw OK bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-128-p_bmhdrOK->bmhdr.Width)*16, (FT_DispHeight-p_bmhdrOK->bmhdr.Height)*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// OK bitmap tag
			TagRegister( TAG_OK, (FT_DispWidth-128-p_bmhdrOK->bmhdr.Width), (FT_DispHeight-p_bmhdrOK->bmhdr.Height), p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height );
		}
			
		// title
		Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
		Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );
		
		// draw the FAN1 value
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		if( valFan1 == FANAUTO )
			s[0] = 'A';
		/*else if( valFan1 == FANTURBO )
			s[0] = 'T';*/
		else
			s[0] = Hex2Ascii( valFan1 );
		s[1] = '\0';
		Ft_Gpu_CoCmd_Text( pHost, 
							30+p_bmhdrFAN1->bmhdr.Width+20 + (FT_DispWidth-(30+p_bmhdrFAN1->bmhdr.Width+20+30))/2, 
							52+40/2, 
							fontTitle, OPT_CENTER, String2Font(s, fontTitle) );
		
		if( F_MULTIFAN >= DOUBLE_FAN )
		{
			// draw the FAN2 value
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			if( valFan2 == FANAUTO )
				s[0] = 'A';
			/*else if( valFan2 == FANTURBO )
				s[0] = 'T';*/
			else
				s[0] = Hex2Ascii( valFan2 );
			s[1] = '\0';
			Ft_Gpu_CoCmd_Text( pHost, 
								30+p_bmhdrFAN2->bmhdr.Width+20 + (FT_DispWidth-(30+p_bmhdrFAN2->bmhdr.Width+20+30))/2, 
								52+p_bmhdrFAN1->bmhdr.Height+22+40/2, 
								fontTitle, OPT_CENTER, String2Font(s, fontTitle) );
		}
		
		Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
		Ft_Gpu_CoCmd_Swap( pHost );
		Ft_App_Flush_Co_Buffer( pHost );
		Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() && 
			(F_MULTIFAN >= DOUBLE_FAN)	// 2 fans at least
			)
		{
			// screenshot
			DoScreenshot( SSHOT_SetFan );
		}
	#endif	// SCREENSHOT

		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
				tagval = pt->tag;
			}
		} while( event.status == osOK );
		
		
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			Flag.Key_Detect = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					if( userSubState == 1 )
					{
						// modifica
						// ???
					}
					loop = 0;	// exit
				}
				else
				{
					if( userSubState == 1 )
					{
						// modifica
						// ???
					}
					else
					{
						// visualizzazione
						if( !(userCounterTime % USER_TIMEOUT_SHOW_1) )
						{
							// recupera valore e limiti parametro
							userReqPar( iParFan1 );
							userGetPar( iParFan1 );
							valFan1 = userParValue.b[0];
							userGetMinMaxStep( iParFan1, &vMinFan1, &vMaxFan1, &stepFan1 );
							if( F_MULTIFAN >= DOUBLE_FAN )
							{
								userReqPar( iParFan2 );
								userGetPar( iParFan2 );
								valFan2 = userParValue.b[0];
								userGetMinMaxStep( iParFan2, &vMinFan2, &vMaxFan2, &stepFan2 );
							}
						}
					}
				}
			}
		}
		else if( (tagval == TAG_HSCROLL_BASE) && pt->status )
		{
			userCounterTime = USER_TIMEOUT_SHOW_60;
			userSubState = 1;	// parameter modify
			// FAN1 horizontal slider touch
			l = 10*(pt->x - (30+p_bmhdrFAN1->bmhdr.Width+20));
			if( l <= 0 )
			{
				l = 0;
				valFan1 = vMinFan1.b[0];
			}
			else
			{
				/* max + AUTO + TURBO
				l = (l * (vMaxFan1.b[0]+1 - vMinFan1.b[0]))/(FT_DispWidth-(30+p_bmhdrFAN1->bmhdr.Width+20+30));
				l = (l + 5)/10;
				valFan1 = vMinFan1.b[0] + l;
				if( valFan1 > vMaxFan1.b[0]+1 )
					valTFan1 = vMaxFan1.b[0]+1;
				*/
				l = (l * (vMaxFan1.b[0] - vMinFan1.b[0]))/(FT_DispWidth-(30+p_bmhdrFAN1->bmhdr.Width+20+30));
				l = (l + 5)/10;
				valFan1 = vMinFan1.b[0] + l;
				if( valFan1 > vMaxFan1.b[0] )
					valFan1 = vMaxFan1.b[0];
			}
		}
		else if( (F_MULTIFAN >= DOUBLE_FAN) && (tagval == TAG_HSCROLL_BASE+1) && pt->status )
		{
			userCounterTime = USER_TIMEOUT_SHOW_60;
			userSubState = 1;	// parameter modify
			// FAN2 horizontal slider touch
			l = 10*(pt->x - (30+p_bmhdrFAN2->bmhdr.Width+20));
			if( l <= 0 )
			{
				l = 0;
				valFan2 = vMinFan2.b[0];
			}
			else
			{
				/* max + AUTO + TURBO
				l = (l * (vMaxFan2.b[0]+1 - vMinFan2.b[0]))/(FT_DispWidth-(30+p_bmhdrFAN2->bmhdr.Width+20+30));
				l = (l + 5)/10;
				valFan2 = vMinFan2.b[0] + l;
				if( valFan2 > vMaxFan2.b[0]+1 )
					valTFan2 = vMaxFan2.b[0]+1;
				*/
				l = (l * (vMaxFan2.b[0] - vMinFan2.b[0]))/(FT_DispWidth-(30+p_bmhdrFAN2->bmhdr.Width+20+30));
				l = (l + 5)/10;
				valFan2 = vMinFan2.b[0] + l;
				if( valFan2 > vMaxFan2.b[0] )
					valFan2 = vMaxFan2.b[0];
			}
		}
		else if( ((pt->tag == TAG_QUIT) || (pt->tag == TAG_OK)) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			if( (pt->tag == TAG_OK) && (userSubState == 1) )
			{
				userParValue.b[0] = valFan1;
				userSetPar( P_FAN );
				if( F_MULTIFAN >= DOUBLE_FAN )
				{
					userParValue.b[0] = valFan2;
					userSetPar( P_FAN_2 );
				}
				RetVal = 1;	// set
			}
			
			loop = 0;	// exit
		}
		else
		{
			tagval = 0;
		}
		
	}

	// pop bitmap FAN2 from Graphic RAM
	res_bm_Release( BITMAP_FAN2 );
	// pop bitmap FAN1 from Graphic RAM
	res_bm_Release( BITMAP_FAN1 );

	return RetVal;
}

/*!
** \fn short Check4CmdGroup( unsigned char iAvCmdGrp )
** \brief Check to view a command group in the command bar
** \param iAvCmdGrp The command group index
** \return TRUE or FALSE
**/
static short Check4CmdGroup( unsigned char iAvCmdGrp )
{
	short RetVal = 0;
	
	switch( iAvCmdGrp )
	{
		case AVCMDGRP_0:
			// view always
			RetVal = 1;
		break;
		
		case AVCMDGRP_1:
			// check AVCMD_SUMMER_WINTER
			if( userIsIdro() && userIsCrono() )
			{
				RetVal = 1;
			}
			// check AVCMD_ESM
			else if( userIsIdro() && ConfigIdro )
			{
				RetVal = 1;
			}
			/* else if( ... )
			{
				RetVal = 1;
			} */
		break;
	}
	
	return RetVal;
}


/*!
** \fn short WinESM( char *sTitle )
** \brief ESM window handler
** \param sTitle The window's title string
** \return None
**/
static void WinESM( char *sTitle )
{
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	unsigned char scrollON = 0;
	unsigned short preXscroll = 0;
	short Dx = 0;
	unsigned long key_cnt = 0;
	unsigned char loop = 1;
	unsigned char page = 0;
	const ESM_Px_STRUCT *pESM_Px_tab;
	short i,k;
	char s[DISPLAY_MAX_COL];
	unsigned char iItemReq = 0;	//!< indice item per richieste valori
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			scrollON = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
			// reset indice item per richieste valori
			iItemReq = 0;
			userCounterTime = USER_TIMEOUT_SHOW_20 + 1;	// forza, senza aspettare, l'aggiornamento dei parametri visualizzati
		}	

		if( !((tagval == TAG_HORIZ_SCROLL) && (scrollON == 1)) )
		{
			if( tagval == TOUCH_TAG_INIT )
			{
				// screen horizontal scroll tag
				TagRegister( TAG_HORIZ_SCROLL, 0, 0, FT_DispWidth, FT_DispHeight );
			}
			
			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			
			// draw QUIT bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, 30, 30));	//scale to 30x30
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_Gpu_CoCmd_LoadIdentity(pHost);
			Ft_Gpu_CoCmd_Scale(pHost, 65536*30L/p_bmhdrQUIT->bmhdr.Width, 65536*30L/p_bmhdrQUIT->bmhdr.Height);	//scale to 30x30
			Ft_Gpu_CoCmd_SetMatrix(pHost );
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, 0*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// QUIT bitmap tag
				TagRegister( TAG_QUIT, 0, 0, 30, 30 );
			}

			Ft_Gpu_CoCmd_LoadIdentity(pHost);
			Ft_Gpu_CoCmd_Scale(pHost, 65536, 65536);	//scale to normal
			Ft_Gpu_CoCmd_SetMatrix(pHost );
			
			switch( page )
			{
				case ESM_0:
					// temperature
					for( k=0; k<NUM_TEMP_Px; k++ )
					{
						// formato di visualizzazione: ##.#�C / ###�F
						pESM_Px_tab = &ESM_Px_tab[k];
						userGetPar( pESM_Px_tab->iPar );
						// il valore e' espresso in step 0.5�C
						i = 5 * userParValue.i[0];	// converto in 0.1�C
						// range ammesso: 0 - 99.9�C
						if( (i < 0) || (i >= 1000) )
						{
							strcpy( str2, " ----" );
						}
						else
						{
							userVisPar( pESM_Px_tab->iPar );
							// no deg character
							i = strlen(str2);
							str2[i-2] = cBLANK;
						}
						strcpy( s, ESM_Px_Desc[k] );
						strcat( s, &str2[1] );	// salta il segno positivo
						Ft_Gpu_CoCmd_Text( pHost,
												pESM_Px_tab->x, pESM_Px_tab->y,
												FONT_DESC_DEF, pESM_Px_tab->options, s );
					}
							
					// stato termostati
					for( k=0; k<NUM_TERMx; k++ )
					{
						pESM_Px_tab = &ESM_TERMx_tab[k];
						userGetPar( pESM_Px_tab->iPar );
						strcpy( s, ESM_TERMx_Desc[k] );
						if( userParValue.b[0] )
						{
							// sCLOSE
							strcat( s, sCLOSE );
						}
						else
						{
							// sOPEN
							strcat( s, sOPEN );
						}
						Ft_Gpu_CoCmd_Text( pHost,
												pESM_Px_tab->x, pESM_Px_tab->y,
												FONT_DESC_DEF, pESM_Px_tab->options, s );
					}

					// separators
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(16,16,16));	// GREY-BLACK
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth/2)*16, (30+54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth/2)*16, (30+4*54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (8)*16, (30+54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-8)*16, (30+54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (8)*16, (30+2*54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-8)*16, (30+2*54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (8)*16, (30+3*54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-8)*16, (30+3*54)*16 ) );
				break;
				
				case ESM_1:
					// stato uscite
					for( k=0; k<NUM_OUTPx; k++ )
					{
						pESM_Px_tab = &ESM_OUTPx_tab[k];
						userGetPar( pESM_Px_tab->iPar );
						strcpy( s, ESM_OUTPx_Desc[k] );
						if( userParValue.b[0] )
						{
							// sON
							strcat( s, (char *)GetMsg( 1, NULL, LANG_ENG, sON ) );
						}
						else
						{
							// sOFF
							strcat( s, (char *)GetMsg( 1, NULL, LANG_ENG, sOFF ) );
						}
						Ft_Gpu_CoCmd_Text( pHost,
												pESM_Px_tab->x, pESM_Px_tab->y,
												FONT_DESC_DEF, pESM_Px_tab->options, s );
					}

					// separators
					Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(16,16,16));	// GREY-BLACK
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth/2)*16, (30+0)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth/2)*16, (30+4*54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (8)*16, (30+54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-8)*16, (30+54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (8)*16, (30+2*54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-8)*16, (30+2*54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, BEGIN(LINE_STRIP) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (8)*16, (30+3*54)*16 ) );
					Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-8)*16, (30+3*54)*16 ) );
				break;
			}
			
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE

			// horizontal scroll bar
			Ft_Gpu_CoCmd_FgColor(pHost, Get_UserColorHex(UserColor_tab[UserColorId].addon));
			Ft_Gpu_CoCmd_BgColor(pHost, 0xFFFFFF);
			Ft_Gpu_CoCmd_Scrollbar( pHost,
											10/2, (FT_DispHeight-10), // consider radius of bar
											(FT_DispWidth-10), 10, // consider radius of bar
											0,
											page,
											1,
											NUM_ESM);
			
			// title
			/* Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_XY( 30, 0 ) );
			Ft_App_WrCoCmd_Buffer( pHost, SCISSOR_SIZE( FT_DispWidth-30-30, 30 ) );
			Ft_Gpu_CoCmd_Gradient( pHost, 30, 0, 0x0000FF, FT_DispWidth-30-1, 0, 0x000000 ); */
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 30*16, (0)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-30-1)*16, (30)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, FONT_TITLE_DEF, OPT_CENTER, sTitle );

			Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
			Ft_Gpu_CoCmd_Swap( pHost );
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
				
		#ifdef SCREENSHOT
			if( IsGenScreeshot() )
			{
				// make the BGRA file name
				switch( page )
				{
					default:
						i = NUM_SSHOT;
					break;
					
					case ESM_0:
						i = SSHOT_ESM1;
					break;
					
					case ESM_1:
						i = SSHOT_ESM2;
					break;
				}
				// screenshot
				DoScreenshot( i );
			}
		#endif	// SCREENSHOT
		}

		// ESM window loop
		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
				tagval = pt->tag;
			}
		} while( event.status == osOK );
		
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			scrollON = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			scrollON = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		
			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					loop = 0;	// exit
				}
				else
				{
					if( !(userCounterTime % (200/USER_TIME_TICK)) )
					{
						//invio richiesta valori dei parametri visualizzati
						switch( iItemReq )
						{
							case 0:
								// richiedo temperature
								OT_Send( (PKT_STRUCT *)&reqTempPx, SEND_MODE_ONE_SHOT, 0 );
							break;
						
							case 1:
								// richiedo stato uscite e termostati
								OT_Send( (PKT_STRUCT *)&reqReleTermBmap, SEND_MODE_ONE_SHOT, 0 );
							break;
						}
						if( ++iItemReq >= 2 )
							iItemReq = 0;
					}
				}
			}
		}
		else if( tagval == TAG_HORIZ_SCROLL )
		//else if( pt->tag == TAG_HORIZ_SCROLL )
		{
			// screen horizontal scroll
			if( pt->status )
			{
				// touched
				switch( scrollON )
				{
				default:
				case 0:
					preXscroll = pt->x;
					scrollON = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dx = pt->x - preXscroll;
						//if( Dx > 5 )
						if( Dx > 100 )
						{
							scrollON = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll right
							preXscroll = pt->x;
							
							if( page )
								page--;
							
							// restart window
							tagval = TOUCH_TAG_INIT;
							scrollON = 0;
							Flag.Key_Detect = 0;
							key_cnt = 0;
						}
						//else if( Dx < -5 )
						else if( Dx < -100 )
						{
							scrollON = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll left
							preXscroll = pt->x;
							
							if( page < NUM_ESM-1 )
								page++;
							
							// restart window
							tagval = TOUCH_TAG_INIT;
							scrollON = 0;
							Flag.Key_Detect = 0;
							key_cnt = 0;
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON = 0;
			}
		}
		else if( (pt->tag == TAG_QUIT) && !pt->status )
		{
			// exit touch
			
			// flush the touch queue
			FlushTouchQueue();
			
			loop = 0;	// exit
		}
		else
		{
			tagval = Read_Keypad( pt );	// read the keys
			if( tagval )
			{
				// touched
				userCounterTime = USER_TIMEOUT_SHOW_20;
				if( Flag.Key_Detect )
				{
					// first touch
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
				else if( ++key_cnt >= VALID_TOUCHED_KEY )
				{
					// pressed item
					key_cnt = VALID_TOUCHED_KEY;
				}
				/* else if( ++key_cnt >= 100 )
				{
					// pressed item
					if( !(key_cnt % 5) )
					{
						// process the pressed item on continuous touch
						switch( tagval )
						{
						}
					}
				} */
			}
			else if( pt->tag )
			{
				// untouched
				if( key_cnt >= VALID_TOUCHED_KEY )
				{
					// flush the touch queue
					FlushTouchQueue();

					// process the pressed item on touch release
					switch( pt->tag )
					{
						default:
						break;
					}
				}
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}

	}		
	
}

/*!
** \fn unsigned char userRestartOff( void )
** \brief User RESTART or SWITCH_OFF confirmation window handler
** \return The next User Interface state
**/
unsigned char userRestartOff( void )
{
	unsigned char RetVal = USER_STATE_RESTART_OFF;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOFF;
	BM_HEADER * p_bmhdrREBOOT;
	//unsigned long key_cnt = 0;
	unsigned char loop = 1;
	char *sTitle = (char *)GetMsg( 8, NULL, iLanguage, REQCONF );
	
	// push bitmap ESC into Graphic RAM
	if( res_bm_Load( BITMAP_ESC, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_ESC );
	#endif
		return userVisAvvio();
	}
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_ESC );
	
	// push bitmap REBOOT into Graphic RAM
	if( res_bm_Load( BITMAP_REBOOT, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_REBOOT );
	#endif
		return userVisAvvio();
	}
	p_bmhdrREBOOT = res_bm_GetHeader( BITMAP_REBOOT );
	
	// push bitmap OFF_BUTTON into Graphic RAM
	if( res_bm_Load( BITMAP_OFF_BUTTON, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_OFF_BUTTON );
	#endif
		return userVisAvvio();
	}
	p_bmhdrOFF = res_bm_GetHeader( BITMAP_OFF_BUTTON );
	

	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
			// full LCD backlight
			SetBacklight( LCD_BACKLIGHT_MAX );
		}

		Ft_Gpu_CoCmd_Dlstart(pHost);
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
		Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
		
		// draw QUIT bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, 0*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// QUIT bitmap tag
			TagRegister( TAG_QUIT, 0, 0, 30, 30 );
		}
		
		// draw REBOOT bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrREBOOT->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrREBOOT->bmhdr.Format, p_bmhdrREBOOT->bmhdr.Stride, p_bmhdrREBOOT->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrREBOOT->bmhdr.Width, p_bmhdrREBOOT->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 117*16, 119*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// REBOOT bitmap tag
			TagRegister( TAG_RESTART, 117, 119, p_bmhdrREBOOT->bmhdr.Width, p_bmhdrREBOOT->bmhdr.Height );
		}
		
		// draw OFF_BUTTON bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOFF->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOFF->bmhdr.Format, p_bmhdrOFF->bmhdr.Stride, p_bmhdrOFF->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrOFF->bmhdr.Width, p_bmhdrOFF->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (117+p_bmhdrREBOOT->bmhdr.Width+117)*16, 119*16 ) );
		if( tagval == TOUCH_TAG_INIT )
		{
			// OFF_BUTTON bitmap tag
			TagRegister( TAG_ON_OFF_PLAY, (117+p_bmhdrREBOOT->bmhdr.Width+117), 119, p_bmhdrOFF->bmhdr.Width, p_bmhdrOFF->bmhdr.Height );
		}
			
		// title
		Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
		Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 30*16, (0)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
		Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
		Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );

		Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
		Ft_Gpu_CoCmd_Swap( pHost );
		Ft_App_Flush_Co_Buffer( pHost );
		Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() )
		{
			// screenshot
			DoScreenshot( SSHOT_Restart_Off );
		}
	#endif	// SCREENSHOT

		do
		{
			Wdog();
			// wait for a touch event
			event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

			// process the touch event
			if( event.status == osEventMessage )
			{
				pt = (TAG_STRUCT *)event.value.v;
				tagval = pt->tag;
			}
		} while( event.status == osOK );
		
		// event processing
		/* if( Check4StatusChange() != USER_MAX_STATES )
		{
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			Flag.Key_Detect = 0;
		}
		else */ if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			Flag.Key_Detect = 0;
			//key_cnt = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					// pop bitmaps from Graphic RAM
					res_bm_Release( BITMAP_OFF_BUTTON );
					res_bm_Release( BITMAP_REBOOT );
					res_bm_Release( BITMAP_ESC );
					RetVal = userVisAvvio();
					loop = 0;	// exit
				}
			}
		}
		else if( !pt->status && ((tagval == TAG_QUIT)
								|| (tagval == TAG_RESTART)
								|| (tagval == TAG_ON_OFF_PLAY)
								)
				)
		{
			// flush the touch queue
			FlushTouchQueue();
			// pop bitmaps from Graphic RAM
			res_bm_Release( BITMAP_OFF_BUTTON );
			res_bm_Release( BITMAP_REBOOT );
			res_bm_Release( BITMAP_ESC );
			
			if( tagval == TAG_QUIT )
			{
				RetVal = userVisAvvio();
			}
			else if( tagval == TAG_RESTART )
			{
				userVisShutDown( "Restart...", 2000 );
				// if battery in charge, restart from USER_STATE_INIT
				if( DIGIN_ALIM )
				{
					// black screen
					SetBacklight( 0 );
					userVisShutDown( "", 0 );
					// Radio-modem RCQ2-868 in low-power
					OT_PH_SetLowPower();
					OpeMode = MODE_LOW_POWER;

					Ft_Gpu_Hal_Sleep(USER_TIME_TICK);

					UserRequest = USER_REQ_NONE;
					RetVal = userVisInit();
				}
				else
				{
					SystemRestart();
					// ------> NON PASSA MAI DI QUI
				}
			}
			else if( tagval == TAG_ON_OFF_PLAY )
			{
				userVisShutDown( "Power off...", 2000 );

				// if battery in charge, restart from USER_STATE_BATTERY
				if( DIGIN_ALIM )
				{
					// black screen
					SetBacklight( 0 );
					userVisShutDown( "", 0 );
					// Radio-modem RCQ2-868 in low-power
					OT_PH_SetLowPower();
					OpeMode = MODE_LOW_POWER;

					Ft_Gpu_Hal_Sleep(USER_TIME_TICK);

					UserRequest = USER_REQ_NONE;
					RetVal = userVisBattery();
				}
				else
				{
					SystemOff();
					// ------> NON PASSA MAI DI QUI
				}
			}
			
			loop = 0;	// exit
		}
	}
	
	UserRequest = USER_REQ_NONE;
	
	return RetVal;
}



