/*!
** \file stringhe_ENG.c
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief ENG strings set
** 
** \version 1.0
**/

#include "stringhe_ENG.h"

//!< string set descriptor for each font group (ISO 3166-1 apha-3)
const char DescMsg_ENG[NUM_FONT_GROUP][MSGDESCLEN] =
{
//	 12345678 (MSGDESCLEN)
	"ENG\0\0\0\0\0",		// FONT_GROUP_EUROPE	(sarebbe "GBR")
	"AHF\0\0\0\0\0",		// FONT_GROUP_CYRILLIC
	"\0\0\0\0\0\0\0\0",		// FONT_GROUP_2
	"\0\0\0\0\0\0\0\0",		// FONT_GROUP_3
};


// week days
static const char sDomenica_ENG[]				= "Sunday";
static const char sLunedi_ENG[]					= "Monday";
static const char sMartedi_ENG[]					= "Tuesday";
static const char sMercoledi_ENG[]				= "Wednesday";
static const char sGiovedi_ENG[]					= "Thursday";
static const char sVenerdi_ENG[]					= "Friday";
static const char sSabato_ENG[]					= "Saturday";

static const char sOff_ENG[]						= "Off";
static const char sOn_ENG[]						= "On ";
static const char sAuto_ENG[]						= "Auto";
static const char sDegC_ENG[]						= "\200C";
static const char sDegF_ENG[]						= "\200F";
static const char sH2O_ENG[]						= "H2O";

// stati della stufa
static const char Stufa_Accensione_ENG[] 		= "IGNITION";
static const char Stufa_Accesa_ENG[]			= "ON";
static const char Stufa_Spegnimento_ENG[]		= "SHUT DOWN";
static const char Stufa_MCZ_ENG[]				= "OFF";
static const char Stufa_Allarme_ENG[]			= "ALARM";
static const char Stufa_SpeRete_ENG[]			= "SHUT DOWN AFTER BLACKOUT";
static const char Stufa_AccRete_ENG[]			= "IGNITION AFTER BLACKOUT";
static const char Stufa_Reset_ENG[]				= "RESET";
static const char Stufa_Collaudo_ENG[]			= "TESTING";
static const char Stufa_NonDef_ENG[]			= " ";

// allarme
static const char Alarm_ENG[]						= "ALARM";

// descrizione allarme
static const char AlarmSource00_ENG[]			= "";
static const char AlarmSource01_ENG[]			= "Ignition Failed";
static const char AlarmSource02_ENG[]			= "Flame Shut Down";
static const char AlarmSource03_ENG[]			= "Pellet Tank Over-Temperature";
static const char AlarmSource04_ENG[]			= "Fumes Over-Temperature";
static const char AlarmSource05_ENG[]			= "Fumes Pressure Switch Alarm";
static const char AlarmSource06_ENG[]			= "Combustion Airflow Alarm";
static const char AlarmSource07_ENG[]			= "Door Open";
static const char AlarmSource08_ENG[]			= "Fumes Extractor Error";
static const char AlarmSource09_ENG[]			= "Fumes Temp. Sensor Error";
static const char AlarmSource10_ENG[]			= "Pellet Igniter Error";
static const char AlarmSource11_ENG[]			= "Pellet Feeder Error";
#define AlarmSource12_ENG		AlarmSource00_ENG
static const char AlarmSource13_ENG[]			= "Electronic MotherBoard Error";
#define AlarmSource14_ENG		AlarmSource00_ENG
static const char AlarmSource15_ENG[]			= "Pellet Level Alarm";
static const char AlarmSource16_ENG[]			= "Water Pressure Out of Range";
static const char AlarmSource17_ENG[]			= "Pellet Tank Door Open";
static const char AlarmSource18_ENG[]			= "Water Tank Over-Temperature";

// ripristino allarme
static const char AlarmRecovery00_ENG[]			= "";
static const char AlarmRecovery01_ENG[]			= "Clean Brazier and Restart";
static const char AlarmRecovery02_ENG[]			= "Fill Pellet Tank";
static const char AlarmRecovery03_ENG[]			= "See Instruction Manual";
#define AlarmRecovery04_ENG		AlarmRecovery03_ENG
static const char AlarmRecovery05_ENG[]			= "Remove a Possible Obstruction";
static const char AlarmRecovery06_ENG[]			= "Check Brazier Cleaning/Air Intake/Chimney";
static const char AlarmRecovery07_ENG[]			= "Check if Door is Locked";
static const char AlarmRecovery08_ENG[]			= "Call Assistance";
#define AlarmRecovery09_ENG		AlarmRecovery08_ENG
#define AlarmRecovery10_ENG		AlarmRecovery08_ENG
#define AlarmRecovery11_ENG		AlarmRecovery08_ENG
#define AlarmRecovery12_ENG		AlarmRecovery08_ENG
#define AlarmRecovery13_ENG		AlarmRecovery08_ENG
#define AlarmRecovery14_ENG		AlarmRecovery08_ENG
static const char AlarmRecovery15_ENG[]			= "Check Pellet Level";
static const char AlarmRecovery16_ENG[]			= "Restore the Correct Water Circuit Pressure";
static const char AlarmRecovery17_ENG[]			= "Check if Pellet Tank Door is Closed";
#define AlarmRecovery18_ENG		AlarmRecovery03_ENG

// sleep
static const char Menu_Sleep_ENG[]				= "Sleep";

// avvio
static const char Settings_Time_ENG[]			= "Time";
static const char Menu_DataOra_ENG[]				= "Date";
static const char Potenza_ENG[]					= "Fire";
static const char Temperatura_ENG[]				= "Temperature";
static const char Menu_Idro_ENG[]					= "Water Temperature";
static const char Fan_ENG[]							= "Fan";
	
// network
static const char Menu_Network_ENG[]				= "Network";
//static const char WiFi_ENG[]						= "Wi-Fi";
static const char Ssid_ENG[]						= "SSID";
static const char Keyword_ENG[]					= "Keyword";
//static const char TCPPort_ENG[]					= "TCP Port";
//static const char WPSPin_ENG[]						= "WPS PIN";

// anomalie
static const char Menu_Anomalie_ENG[]				= "Warnings";
static const char RichiestaService_ENG[]			= "Service";
static const char GuastoSondaTempAria_ENG[]		= "Air Temp. Sensor Failure";
static const char GuastoSondaTempAcqua_ENG[]		= "Water Temp. Sensor Failure";
static const char GuastoPressostAcqua_ENG[]		= "Water Pressure Switch Failure";
static const char SovraPressAcqua_ENG[]				= "Water Pressure Out of Range";
static const char GuastoSensorePortata_ENG[]		= "Airflow Sensor Failure";
static const char PelletEsaurito_ENG[]				= "Pellet Low Level";
static const char PortaAperta_ENG[]					= "Door Open";

// info
static const char Menu_Infos_ENG[]				= "User Info";
static const char CodiceScheda_ENG[]				= "Control Board Code";
static const char CodiceSicurezza_ENG[]			= "Security Code";
static const char CodicePannello_ENG[]			= "Display Code";
static const char CodiceParametri_ENG[]			= "Parameters Code";
static const char OreFunzionamento_ENG[]		= "Functioning Time";
static const char OreSat_ENG[]						= "Service Time";
static const char Service_ENG[]					= "Service";
static const char VentilatoreFumi_ENG[]			= "Extractor 1 Rpm";
static const char PortataAriaMisurata_ENG[]	= "Measured Airflow";
static const char OssigenoResiduo_ENG[]			= "Residual Oxigen";
static const char Consumo_ENG[]					= "Consumption Pellet";
static const char TemperaturaFumi_ENG[]			= "Fumes Temperature";
static const char FotoResistenza_ENG[]			= "Photoresistor";
static const char TempoCoclea_ENG[]				= "Pellet Feeder 1 Time";
static const char GiriCoclea_ENG[]				= "Pellet Feeder 1 RPM";
static const char MotoreScambiatore_ENG[] 		= "Activation Fan1";
static const char MotoreScambiatore2_ENG[] 	= "Activation Fan2";
static const char IdroPress_ENG[]					= "Water Pressure";
static const char NumAccensioni_ENG[]			= "Ignitions Number";
static const char IPAddress_ENG[]					= "IP Address";
static const char StoricoAllarmi_ENG[]			= "Alarm History";

// menu principale
static const char MainMenu_ENG[]					= "Main Menu";
static const char Menu_Impostazioni_ENG[]		= "Settings";
static const char Menu_Tecnico_ENG[]				= "Technical Menu";

// settings
static const char Lingua_ENG[]						= "Language";
static const char EcoMode_ENG[]					= "Eco";
static const char Settings_Backlight_ENG[]		= "Display";
static const char Settings_ShowTemp_ENG[]		= "\200C / \200F";
static const char Precarica_Pellet_ENG[]    	= "Charge Pellet";
static const char Attiva_Pulizia_ENG[]			= "Cleaning";
static const char Attiva_Pompa_ENG[]				= "Start Pump";
static const char RFPanel_ENG[]					= "Radio ID";

// ricette
static const char Ricette_ENG[]						= "Combustion Recipes";
static const char OffsetEspulsore_ENG[]				= "Airflow";
static const char RicettaPelletON_ENG[]				= "Pellet";
static const char Ossigeno_ENG[]						= "Oxigen";

// temp H2O
static const char TempH2O_SetRisc_ENG[]			= "Set Heating Temperature";
static const char TempH2O_SetSan_ENG[]			= "Set Sanitary Temperature";

// menu nascosto
static const char HidPassword_ENG[]				= "Password";

// menu tecnico - livello 1					012345678901234567890
static const char ConfigSystem_ENG[]				= "Configuration";
static const char Controllo_ENG[]					= "Control";
static const char MenuIdro_ENG[]					= "Hydro";
static const char AttuaTransitorie_ENG[]		= "Ignition/Shutdown";
static const char AttuaPotenza_ENG[]				= "Power";
static const char GestAllarmi_ENG[]				= "Alarms Management";
static const char Collaudo_ENG[]					= "Testing";
static const char RicercaCodParam_ENG[]			= "Search by Code";

// menu tecnico - livello 2							012345678901234567890
static const char MenuParamGen_ENG[]						= "Settings";
static const char Blackout_ENG[]							= "Blackout";
static const char MenuCoclea_ENG[]						= "Pellet Feeder";
static const char MenuScuotitore_ENG[]					= "Ash / Brazier Clean";
static const char FanAmbiente_ENG[]						= "Fan";
static const char Fan1_ENG[]								= "Fan 1";
static const char Fan2_ENG[]								= "Fan 2";
static const char Fan3_ENG[]								= "Fan 3";

static const char MenuEco_ENG[]							= "Eco Function";
static const char TempiFunz_ENG[]							= "Working Time";

static const char ParamTransitori_ENG[]					= "Settings";
static const char Preacc1_ENG[]							= "Pre-Startup 1";
static const char Preacc2_ENG[]							= "Pre-Startup 2";
static const char PreaccCaldo_ENG[]						= "Warm Pre-Startup";
static const char AccA_ENG[]								= "Ignition A";
static const char AccB_ENG[]								= "Ignition B";
static const char FireON_ENG[]								= "Fire ON";
static const char SpeA_ENG[]								= "Shut Down A";
static const char SpeB_ENG[]								= "Shut Down B";
static const char Raff_ENG[]								= "Cooling";

static const char ParamPotenza_ENG[]						= "Power Parameters";
static const char MenuPB_ENG[]								= "Cleaning Cycle";
static const char RitardoAttuaEsp_ENG[]					= "Extractor Delay";
static const char RitardoStepPot_ENG[]					= "Power Step Delay";
static const char Pot1_ENG[]								= "Power 1";
static const char Pot2_ENG[]								= "Power 2";
static const char Pot3_ENG[]								= "Power 3";
static const char Pot4_ENG[]								= "Power 4";
static const char Pot5_ENG[]								= "Power 5";
static const char Pot6_ENG[]								= "Power 6";
static const char Pulizia_ENG[]							= "Cleaning";
static const char Interpolazione_ENG[]					= "Interpolation";

static const char AllarmeFumi_ENG[]						= "Fumes Alarm";
static const char SensPellet_ENG[]						= "Pellet Sensor";
// static const char AssenzaFiamma_ENG[]					= "No Flame";
static const char SensAria_ENG[]							= "Airflow";

static const char Comandi_ENG[]							= "Commands";
static const char Carichi_ENG[]							= "Motors";
static const char Attuazioni_ENG[]						= "Activations";
	
// parametri generali
static const char TipoStufa_ENG[]						= "Stove Type";
static const char TipoStufaDef_ENG[]					= "Restore Default Stove Type";
static const char MotoreEspulsore_ENG[]				= "Motor Type 1";
static const char MotoreEspulsore2_ENG[]			= "Motor Type 2";
static const char Multifan_ENG[]						= "Number of Fans";
static const char IdroMode_ENG[]						= "Hydro Enabled";
static const char VisSanitari_ENG[]					= "Viewing Domestic Hot Water";
static const char VisTipoStufa_ENG[]					= "Viewing Stove Type";
static const char SensorePortataAria_ENG[]			= "Airflow Control";
static const char SensoreHall_ENG[]					= "RPM Control";
static const char ControlloLambda_ENG[]				= "Lambda Control";
static const char LimGiriMinimi_ENG[]				= "Low RPM Limiter";
static const char AccTermocoppia_ENG[]				= "Thermocouple";
static const char AccFotoResistenza_ENG[]			= "Photoresistor";
static const char InibAccB_ENG[]						= "Startup B Inhibition";
//static const char RampaCandeletta_ENG[]				= "Igniter Ramp";
static const char Termostato_Ambiente_ENG[] 		= "Thermostat";
static const char GestioneIbrida_ENG[]		 		= "Fire/Temp. Mode";
//static const char GestioneLegna_ENG[]		 		= "Wood/Pellet Control";
static const char FScuotitore_ENG[]					= "Mechanical Cleaning";
static const char Coclea2_ENG[]						= "Pellet Feeder 2 / Ash Cleaner";
static const char Esp2Enable_ENG[]					= "Extractor 2 Enabled";
static const char SensoreHall_2_ENG[]				= "Extractor 2 RPM Control";
static const char SensorePellet_ENG[]				= "Pellet Level Sensor";

// parametri blackout
static const char BlackoutRipristino_ENG[]			= "Restore Enabled";
static const char BlackoutDurata_ENG[]				= "Restore Duration";

// parametri coclea
static const char FrenataCoclea_ENG[]				= "Pellet Feeder 1 Brake";
static const char TipoFrenataCoclea_ENG[]			= "Pellet Feeder 1 Brake Type";
static const char FrenataCoclea_2_ENG[]				= "Pellet Feeder 2 Brake";
static const char TipoFrenataCoclea_2_ENG[]		= "Pellet Feeder 2 Brake Type";
static const char CtrlGiriCoclea_ENG[]				= "Pellet Feeder 1 RPM Control";
static const char PeriodoCoclea_ENG[]				= "Pellet Feeder 1 Period";
static const char PeriodoCoclea2_ENG[]				= "Pellet Feeder 2 Period";
static const char AbilPeriodoCoclea_ENG[]			= "Enable Pellet Feeder 1 Period";
static const char AbilPeriodoCoclea2_ENG[]			= "Enable Pellet Feeder 2 Period";

// parametri scuotitore
static const char ScuotDurata_ENG[]					= "Half-Cycle Duration";
static const char ScuotNCicli_ENG[]					= "Number of Cycles";
//static const char ScuotPeriodo_ENG[]					= "Cycles Interval";

// parametri fan ambiente
static const char FanCtrlTempFumi_ENG[]				= "Control by Fumes Temperature";
static const char FanTempFumiON_ENG[]				= "Temp. On Fan";
static const char FanTempFumiOFF_ENG[]				= "Fan Min. Temp.";

// parametri fan 
static const char FanAttuaL1_ENG[]					= "Level 1";
static const char FanAttuaL2_ENG[]					= "Level 2";
static const char FanAttuaL3_ENG[]					= "Level 3";
static const char FanAttuaL4_ENG[]					= "Level 4";
static const char FanAttuaL5_ENG[]					= "Level 5";

// parametri ricette

// parametri eco
static const char EcoStop_ENG[]						= "Ecostop";
static const char EcoAttesaAccensione_ENG[]		= "Waiting On";
static const char Settings_TShutdown_Eco_ENG[] 	= "Waiting Off";
static const char EcoDeltaTemperatura_ENG[]		= "Delta Temp.";

// parametri tempi funzionamento

// parametri idro
static const char IdroIndipendente_ENG[]			= "Hydro Independent";
static const char SpegnimentoIdro_ENG[]				= "Hydro Shutdown";
//static const char FAccumulo_ENG[]						= "Accumulator";
static const char InibSensing_ENG[]					= "Pump Sensing Inhibition";
static const char PompaModulante_ENG[]				= "Modulating Pump";
static const char FPressIdro_ENG[]					= "Water Pressostat";
//static const char FlussostatoDigitale_ENG[] 		= "Secondary Fluxmeter";
static const char GainRisc_ENG[]						= "Heating Gain";
static const char IsteresiTempAcqua_ENG[]			= "Water Temp. Hysteresis";
static const char DeltaSanitari_ENG[]				= "Sanitary Delta";
static const char GainSanit_ENG[]						= "Sanitary Gain";
static const char TempAux_ENG[]						= "AUX 2 Activation Temp.";
static const char MaxPressH2O_ENG[]					= "Max Water Pression";
static const char TSETON_Pompa_ENG[]					= "Temp. On Pump";
static const char TSETOFF_Pompa_ENG[]				= "Temp. Off Pump";

// parametri transitori
static const char AccTempoPreacc_ENG[]				= "Pre-Startup 1 Duration";
static const char AccTempoPreLoad_ENG[]				= "Pre-Startup 2 Duration";
static const char AccTempoPreacc2_ENG[]				= "Warm Pre-Startup Duration";
static const char AccTempOnFumi_ENG[]				= "Fumes Temp. On";
static const char AccTempOffFumi_ENG[]				= "Fumes Temp. Off";
static const char AccTempSogliaFumi_ENG[]			= "Wood Threshold";
static const char AccTempoInc_ENG[]					= "Warm Pre-Startup Duration";
static const char AccDeltaTempCaldo_ENG[]			= "Warm Delta Temp.";
static const char AccMaxTimeWarmUp_ENG[]			= "Max. Startup Duration";
static const char AccMaxTimeFireOn_ENG[]			= "Fire On Duration";
static const char AccMaxTimeSpe_ENG[]				= "Shutdown Duration";
	
// parametri di attuazione
static const char AttuaTempoOnCoclea1_ENG[]		= "Pellet Feeder 1 On Time";
static const char AttuaTempoOffCoclea1_ENG[]		= "Pellet Feeder 1 Off Time";
static const char AttuaPortata_ENG[]					= "Airflow";
static const char AttuaGiriEsp_ENG[]					= "Extractor 1 Rpm";
static const char AttuaGiriEsp2_ENG[]				= "Extractor 2 Rpm";
static const char AttuaTempoOnCoclea2_ENG[]		= "Pellet Feeder 2 On Time";
static const char AttuaTempoOffCoclea2_ENG[]		= "Pellet Feeder 2 Off Time";
static const char AttuaEspulsore2_ENG[]				= "Extractor 2";
	
// parametri di potenza
static const char LivPotenzaMax_ENG[]				= "Power Levels";
	
// parametri pulizia braciere
static const char AttesaPB_ENG[]						= "Interval";
static const char DurataPB_ENG[]						= "Duration";
static const char MinPowerPB_ENG[]					= "Threshold";
	
// parametri ritardo espulsore
static const char RitardoEspInc_ENG[]				= "Speed Up Delay Time";
static const char RitardoEspDec_ENG[]				= "Speed Down Delay Time";
	
// parametri ritardo step potenza
static const char RitardoPotInc_ENG[]				= "Power Level Step Up";
static const char RitardoPotDec_ENG[]				= "Power Level Step Down";
	
// parametri allarme fumi
static const char AlmFumi_InibPreAlm_ENG[]			= "Pre-Alarm Inhibition";
static const char AlmFumi_TempPreAlm_ENG[]			= "Fumes Temp. Pre-Alarm";
static const char AlmFumi_DurataPreAlm_ENG[]		= "Max. Fumes Pre-Alarm Duration";
static const char AlmFumi_GradientePreAlm_ENG[]	= "Pre-Alarm Hysteresis";
static const char AlmFumi_TempAlm_ENG[]				= "Fumes Temp. Alarm";
	
// parametri sensore pellet
static const char SensPellet_DurataPreAlm_ENG[]	= "Pre-Alarm Duration";
	
// parametri assenza fiamma
static const char NoFiamma_DeltaTemp_ENG[]			= "Delta Fumes Off";
	
// parametri sensore portata aria
static const char Aria_PortataCritica_ENG[]			= "Door Open Threshold";
static const char Aria_TempoAlmPorta_ENG[]				= "Door Open Duration";
static const char Aria_TempoPreAlmPorta_ENG[]			= "Pre-Alarm Door Open Duration";
static const char Aria_InibAlmAriaCombust_ENG[]		= "Disable Combustion Airflow Alarm";
static const char Aria_TempoPreAlmAriaCombust_ENG[]	= "Combustion Airflow Pre-Alarm Duration";
static const char Aria_DeltaPortAriaCombust_ENG[]	= "Combustion Airflow Delta";
	
// comandi
static const char Test_StartStop_ENG[]				= "Manual Testing";
static const char Test_Sequenza_ENG[]				= "Automatic Testing";
static const char Test_Bypass_ENG[]					= "Bypass";
static const char Test_CalibTC_ENG[]					= "Thermocouple Calibration";
static const char Test_CalibFotoresOn_ENG[]		= "Photoresistor Calibration On";
static const char Test_CalibFotoresOff_ENG[]		= "Photoresistor Calibration Off";
static const char Test_DurataStep_ENG[]				= "Testing Step Duration";
	
// carichi
static const char Test_Coclea_ENG[]					= "Pellet Feeder 1";
static const char Test_Candeletta_ENG[]				= "Igniter";
static const char Test_Fan1_ENG[]						= "Fan 1";
static const char Test_Fan2_ENG[]						= "Fan 2";
static const char Test_Fan3_ENG[]						= "Fan 3";
static const char Test_Espulsore_ENG[]				= "Extractor 1";
static const char Test_Pompa_ENG[]					= "Pump";
static const char Test_3Vie_ENG[]						= "3-Way";
static const char Test_Aux1_ENG[]						= "Aux 1";
static const char Test_Aux2_ENG[]						= "Aux 2";
static const char Test_AuxA_ENG[]						= "Aux A";
	
// attuazioni
static const char AttuaFan1_ENG[]						= "Activation Fan 1";
static const char AttuaFan2_ENG[]						= "Activation Fan 2";
static const char AttuaFan3_ENG[]						= "Activation Fan 3";

// richieste di conferma
static const char ReqConf_ENG[]						= "Confirm?";
static const char TC_ReqConf_ENG[]					= "Short Circuit JE Terminal and then Enable";

// Crono
static const char Menu_Crono_ENG[]					= "Chrono";
static const char Abilitazione_ENG[]					= "Enable";
static const char CaricaProfilo_ENG[]				= "Load Profile";
static const char Azzera_ENG[]							= "Reset";

// prog. crono settimanale
static const char PrgSettimanale_ENG[]				= "Program";
static const char PrgAbilita_ENG[]					= "Enable";
static const char PrgStart_ENG[]						= "Start";
static const char PrgStop_ENG[]						= "Stop";
static const char PrgTempAria_ENG[]					= "Air Temperature";
static const char PrgTempH2O_ENG[]					= "Water Temperature";
static const char PrgPotenza_ENG[]					= "Fire";
	
// sigle fan
//													   12
static const char sF1_ENG[]							= "F1";
static const char sF2_ENG[]							= "F2";

// stati di attuazione
//													   1234
static const char sAttuaStat00_ENG[]				= " OFF";
static const char sAttuaStat01_ENG[]				= "PSU1";
static const char sAttuaStat02_ENG[]				= "PSU2";
static const char sAttuaStat03_ENG[]				= "WPSU";
static const char sAttuaStat04_ENG[]				= "SU A";
static const char sAttuaStat05_ENG[]				= "SU B";
static const char sAttuaStat06_ENG[]				= "SU C";
static const char sAttuaStat07_ENG[]				= "FONA";
static const char sAttuaStat08_ENG[]				= "FONB";
static const char sAttuaStat09_ENG[]				= "SD A";
static const char sAttuaStat10_ENG[]				= "SD B";
static const char sAttuaStat11_ENG[]				= "SD C";
static const char sAttuaStat12_ENG[]				= "CD A";
static const char sAttuaStat13_ENG[]				= "CD B";
static const char sAttuaStat14_ENG[]				= "BCLA";
static const char sAttuaStat15_ENG[]				= "BCLB";
static const char sAttuaStat16_ENG[]				= "PL 1";
static const char sAttuaStat17_ENG[]				= "PL 2";
static const char sAttuaStat18_ENG[]				= "PL 3";
static const char sAttuaStat19_ENG[]				= "PL 4";
static const char sAttuaStat20_ENG[]				= "PL 5";
static const char sAttuaStat21_ENG[]				= "PL 6";
static const char sAttuaStat22_ENG[]				= "PL 7";
static const char sAttuaStat23_ENG[]				= "PL 8";
static const char sAttuaStat24_ENG[]				= "PL 9";
static const char sAttuaStat25_ENG[]				= "PLSA";


// ENG strings set
const PCSTR pStr_ENG[NUMMSG] =
{
	// week days
	sDomenica_ENG,
	sLunedi_ENG,
	sMartedi_ENG,
	sMercoledi_ENG,
	sGiovedi_ENG,
	sVenerdi_ENG,
	sSabato_ENG,

	sOff_ENG,
	sOn_ENG,
	sAuto_ENG,
	sDegC_ENG,
	sDegF_ENG,
	sH2O_ENG,

	// stati della stufa
	Stufa_Accensione_ENG,
	Stufa_Accesa_ENG,
	Stufa_Spegnimento_ENG,
	Stufa_MCZ_ENG,
	Stufa_Allarme_ENG,
	Stufa_SpeRete_ENG,
	Stufa_AccRete_ENG,
	Stufa_Reset_ENG,
	Stufa_Collaudo_ENG,
	Stufa_NonDef_ENG,

	// allarme
	Alarm_ENG,

	// descrizione allarme
	AlarmSource00_ENG,
	AlarmSource01_ENG,
	AlarmSource02_ENG,
	AlarmSource03_ENG,
	AlarmSource04_ENG,
	AlarmSource05_ENG,
	AlarmSource06_ENG,
	AlarmSource07_ENG,
	AlarmSource08_ENG,
	AlarmSource09_ENG,
	AlarmSource10_ENG,
	AlarmSource11_ENG,
	AlarmSource12_ENG,
	AlarmSource13_ENG,
	AlarmSource14_ENG,
	AlarmSource15_ENG,
	AlarmSource16_ENG,
	AlarmSource17_ENG,
	AlarmSource18_ENG,

	// ripristino allarme
	AlarmRecovery00_ENG,
	AlarmRecovery01_ENG,
	AlarmRecovery02_ENG,
	AlarmRecovery03_ENG,
	AlarmRecovery04_ENG,
	AlarmRecovery05_ENG,
	AlarmRecovery06_ENG,
	AlarmRecovery07_ENG,
	AlarmRecovery08_ENG,
	AlarmRecovery09_ENG,
	AlarmRecovery10_ENG,
	AlarmRecovery11_ENG,
	AlarmRecovery12_ENG,
	AlarmRecovery13_ENG,
	AlarmRecovery14_ENG,
	AlarmRecovery15_ENG,
	AlarmRecovery16_ENG,
	AlarmRecovery17_ENG,
	AlarmRecovery18_ENG,

	// sleep
	Menu_Sleep_ENG,

	// avvio
	Settings_Time_ENG,
	Menu_DataOra_ENG,
	Potenza_ENG,
	Temperatura_ENG,
	Menu_Idro_ENG,
	Fan_ENG,
	
	// network
	Menu_Network_ENG,
	//WiFi_ENG,
	Ssid_ENG,
	Keyword_ENG,
	//TCPPort_ENG,
	//WPSPin_ENG,
	
	// anomalie
	Menu_Anomalie_ENG,
	RichiestaService_ENG,
	GuastoSondaTempAria_ENG,	
	GuastoSondaTempAcqua_ENG,	
	GuastoPressostAcqua_ENG,	
	SovraPressAcqua_ENG,	
	GuastoSensorePortata_ENG,
	PelletEsaurito_ENG,
	PortaAperta_ENG,

	// info
	Menu_Infos_ENG,
	CodiceScheda_ENG,
	CodiceSicurezza_ENG,
	CodicePannello_ENG,
	CodiceParametri_ENG,
	OreFunzionamento_ENG,
	OreSat_ENG,
	Service_ENG,
	VentilatoreFumi_ENG,
	PortataAriaMisurata_ENG,
	OssigenoResiduo_ENG,
	Consumo_ENG,
	TemperaturaFumi_ENG,
	FotoResistenza_ENG,
	TempoCoclea_ENG,
	GiriCoclea_ENG,
	MotoreScambiatore_ENG,
	MotoreScambiatore2_ENG,
	IdroPress_ENG,
	NumAccensioni_ENG,
	IPAddress_ENG,
	StoricoAllarmi_ENG,

	// menu principale
	MainMenu_ENG,
	Menu_Impostazioni_ENG,
	Menu_Tecnico_ENG,

	// settings
	Lingua_ENG,
	EcoMode_ENG,
	Settings_Backlight_ENG,
	Settings_ShowTemp_ENG,
	Precarica_Pellet_ENG,
	Attiva_Pulizia_ENG,
	Attiva_Pompa_ENG,
	RFPanel_ENG,

	// ricette
	Ricette_ENG,
	OffsetEspulsore_ENG,
	RicettaPelletON_ENG,
	Ossigeno_ENG,

	// temp H2O
	TempH2O_SetRisc_ENG,
	TempH2O_SetSan_ENG,

	// menu nascosto
	HidPassword_ENG,

	// menu tecnico - livello 1
	ConfigSystem_ENG,
	Controllo_ENG,
	MenuIdro_ENG,
	AttuaTransitorie_ENG,
	AttuaPotenza_ENG,
	GestAllarmi_ENG,
	Collaudo_ENG,
	RicercaCodParam_ENG,
	
	// menu tecnico - livello 2
	MenuParamGen_ENG,
	Blackout_ENG,
	MenuCoclea_ENG,
	MenuScuotitore_ENG,
	FanAmbiente_ENG,
	Fan1_ENG,
	Fan2_ENG,
	Fan3_ENG,
	
	MenuEco_ENG,
	TempiFunz_ENG,
	
	ParamTransitori_ENG,
	Preacc1_ENG,
	Preacc2_ENG,
	PreaccCaldo_ENG,
	AccA_ENG,
	AccB_ENG,
	FireON_ENG,
	SpeA_ENG,
	SpeB_ENG,
	Raff_ENG,
	
	ParamPotenza_ENG,
	MenuPB_ENG,
	RitardoAttuaEsp_ENG,
	RitardoStepPot_ENG,
	Pot1_ENG,
	Pot2_ENG,
	Pot3_ENG,
	Pot4_ENG,
	Pot5_ENG,
	Pot6_ENG,
	Pulizia_ENG,
	Interpolazione_ENG,
	
	AllarmeFumi_ENG,
	SensPellet_ENG,
	// AssenzaFiamma_ENG,
	SensAria_ENG,
	
	Comandi_ENG,
	Carichi_ENG,
	Attuazioni_ENG,
	
	// parametri generali
	TipoStufa_ENG,
	TipoStufaDef_ENG,
	MotoreEspulsore_ENG,
	MotoreEspulsore2_ENG,
	Multifan_ENG,
	IdroMode_ENG,
	VisSanitari_ENG,
	VisTipoStufa_ENG,
	SensorePortataAria_ENG,
	SensoreHall_ENG,
	ControlloLambda_ENG,
	LimGiriMinimi_ENG,
	AccTermocoppia_ENG,
	AccFotoResistenza_ENG,
	InibAccB_ENG,
	//RampaCandeletta_ENG,
	Termostato_Ambiente_ENG,
	GestioneIbrida_ENG,
	//GestioneLegna_ENG,
	FScuotitore_ENG,
	Coclea2_ENG,
	Esp2Enable_ENG,
	SensoreHall_2_ENG,
	SensorePellet_ENG,
	
	// parametri blackout
	BlackoutRipristino_ENG,
	BlackoutDurata_ENG,

	// parametri coclea
	FrenataCoclea_ENG,
	TipoFrenataCoclea_ENG,
	FrenataCoclea_2_ENG,
	TipoFrenataCoclea_2_ENG,
	CtrlGiriCoclea_ENG,
	PeriodoCoclea_ENG,
	PeriodoCoclea2_ENG,
	AbilPeriodoCoclea_ENG,
	AbilPeriodoCoclea2_ENG,

	// parametri scuotitore
	ScuotDurata_ENG,
	ScuotNCicli_ENG,
	//ScuotPeriodo_ENG,

	// parametri fan ambiente
	FanCtrlTempFumi_ENG,
	FanTempFumiON_ENG,
	FanTempFumiOFF_ENG,

	// parametri fan 
	FanAttuaL1_ENG,
	FanAttuaL2_ENG,
	FanAttuaL3_ENG,
	FanAttuaL4_ENG,
	FanAttuaL5_ENG,

	// parametri ricette

	// parametri eco
	EcoStop_ENG,
	EcoAttesaAccensione_ENG,
	Settings_TShutdown_Eco_ENG,
	EcoDeltaTemperatura_ENG,

	// parametri tempi funzionamento

	// parametri idro
	IdroIndipendente_ENG,	
	SpegnimentoIdro_ENG,	
	//FAccumulo_ENG,
	InibSensing_ENG,
	PompaModulante_ENG,
	FPressIdro_ENG,
	//FlussostatoDigitale_ENG,
	GainRisc_ENG,			
	IsteresiTempAcqua_ENG,
	DeltaSanitari_ENG,		
	GainSanit_ENG,			
	TempAux_ENG,
	MaxPressH2O_ENG,
	TSETON_Pompa_ENG,
	TSETOFF_Pompa_ENG,

	// parametri transitori
	AccTempoPreacc_ENG,
	AccTempoPreLoad_ENG,
	AccTempoPreacc2_ENG,
	AccTempOnFumi_ENG,
	AccTempOffFumi_ENG,
	AccTempSogliaFumi_ENG,
	AccTempoInc_ENG,
	AccDeltaTempCaldo_ENG,
	AccMaxTimeWarmUp_ENG,
	AccMaxTimeFireOn_ENG,
	AccMaxTimeSpe_ENG,
	
	// parametri di attuazione
	AttuaTempoOnCoclea1_ENG,	
	AttuaTempoOffCoclea1_ENG,
	AttuaPortata_ENG,			
	AttuaGiriEsp_ENG,			
	AttuaGiriEsp2_ENG,			
	AttuaTempoOnCoclea2_ENG,	
	AttuaTempoOffCoclea2_ENG,
	AttuaEspulsore2_ENG,
	
	// parametri di potenza
	LivPotenzaMax_ENG,
	
	// parametri pulizia braciere
	AttesaPB_ENG,
	DurataPB_ENG,
	MinPowerPB_ENG,
	
	// parametri ritardo espulsore
	RitardoEspInc_ENG,
	RitardoEspDec_ENG,
	
	// parametri ritardo step potenza
	RitardoPotInc_ENG,
	RitardoPotDec_ENG,
	
	// parametri allarme fumi
	AlmFumi_InibPreAlm_ENG,
	AlmFumi_TempPreAlm_ENG,
	AlmFumi_DurataPreAlm_ENG,
	AlmFumi_GradientePreAlm_ENG,
	AlmFumi_TempAlm_ENG,
	
	// parametri sensore pellet
	SensPellet_DurataPreAlm_ENG,
	
	// parametri assenza fiamma
	NoFiamma_DeltaTemp_ENG,
	
	// parametri sensore portata aria
	Aria_PortataCritica_ENG,
	Aria_TempoAlmPorta_ENG,
	Aria_TempoPreAlmPorta_ENG,
	Aria_InibAlmAriaCombust_ENG,
	Aria_TempoPreAlmAriaCombust_ENG,
	Aria_DeltaPortAriaCombust_ENG,
	
	// comandi
	Test_StartStop_ENG,
	Test_Sequenza_ENG,
	Test_Bypass_ENG,
	Test_CalibTC_ENG,
	Test_CalibFotoresOn_ENG,
	Test_CalibFotoresOff_ENG,
	Test_DurataStep_ENG,
	
	// carichi
	Test_Coclea_ENG,
	Test_Candeletta_ENG,	
	Test_Fan1_ENG,
	Test_Fan2_ENG,
	Test_Fan3_ENG,
	Test_Espulsore_ENG,
	Test_Pompa_ENG,
	Test_3Vie_ENG,
	Test_Aux1_ENG,
	Test_Aux2_ENG,
	Test_AuxA_ENG,
	
	// attuazioni
	AttuaFan1_ENG,
	AttuaFan2_ENG,
	AttuaFan3_ENG,

	// richieste di conferma
	ReqConf_ENG,
	TC_ReqConf_ENG,
	
	// Crono
	Menu_Crono_ENG,
	Abilitazione_ENG,
	CaricaProfilo_ENG,
	Azzera_ENG,	

	// prog. crono settimanale
	PrgSettimanale_ENG,
	PrgAbilita_ENG,
	PrgStart_ENG,	
	PrgStop_ENG,	
	PrgTempAria_ENG,	
	PrgTempH2O_ENG,	
	PrgPotenza_ENG,	
	
	// sigle fan
	sF1_ENG,
	sF2_ENG,

	// stati di attuazione
	sAttuaStat00_ENG,
	sAttuaStat01_ENG,
	sAttuaStat02_ENG,
	sAttuaStat03_ENG,
	sAttuaStat04_ENG,
	sAttuaStat05_ENG,
	sAttuaStat06_ENG,
	sAttuaStat07_ENG,
	sAttuaStat08_ENG,
	sAttuaStat09_ENG,
	sAttuaStat10_ENG,
	sAttuaStat11_ENG,
	sAttuaStat12_ENG,
	sAttuaStat13_ENG,
	sAttuaStat14_ENG,
	sAttuaStat15_ENG,
	sAttuaStat16_ENG,
	sAttuaStat17_ENG,
	sAttuaStat18_ENG,
	sAttuaStat19_ENG,
	sAttuaStat20_ENG,
	sAttuaStat21_ENG,
	sAttuaStat22_ENG,
	sAttuaStat23_ENG,
	sAttuaStat24_ENG,
	sAttuaStat25_ENG,

	
};

